package org.slasoi.gslam.core.negotiation;

import java.util.ArrayList;
import java.util.List;

import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The <code>INegotiation</code> interface provides methods for conducting negotiation between parties. A party
 * negotiates through its SLA-Manager.
 * <p>
 * An implementation of this interface is obtained from the <code>ProtocolEngine</code> interface or
 * <code>NegotiationClient</code> of the SLA-Manager. The former is used for intra SLA-Manager and latter for inter
 * SLA-Manager interactions.
 * 
 * @author Edwin Yaqub
 * @version 1.0-SNAPSHOT
 */
public interface INegotiation {

    /**
     * Initiates a negotiation session, using either the protocol of current SLA-Manager if present or the default one.
     * 
     * @param slaTemplate
     *            the slaTemplate used to setup negotiation session
     * @return String representing the id of the negotiation session established
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String initiateNegotiation(SLATemplate slaTemplate) throws OperationNotPossibleException;

    /**
     * Negotiates with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing an offer
     * @return SLATemplate array representing counter offers returned
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLATemplate[] negotiate(String negotiationID, SLATemplate slaTemplate) throws OperationInProgressException,
            OperationNotPossibleException, InvalidNegotiationIDException;

    /**
     * Creates agreement with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param slaTemplate
     *            the slaTemplate representing the final offer
     * @return SLA created, if at all
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws SLACreationException
     *             if sla could not be created
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public SLA createAgreement(String negotiationID, SLATemplate slaTemplate) throws OperationInProgressException,
            SLACreationException, InvalidNegotiationIDException;

    /**
     * Cancels ongoing negotiation with the SLA-Manager.
     * 
     * @param negotiationID
     *            the string representing the id of the negotiation session
     * @param cancellationReason
     *            list of cancellation reasons that must be provided
     * @return boolean value showing if negotiation could be cancelled or not
     * @throws OperationInProgressException
     *             if an operation is already in progress
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     * @throws InvalidNegotiationIDException
     *             if invalid negotiation session id is provided
     */
    public boolean cancelNegotiation(String negotiationID, List<CancellationReason> cancellationReason)
            throws OperationInProgressException, OperationNotPossibleException, InvalidNegotiationIDException;

    /**
     * Renegotiates with the SLA-Manager.
     * 
     * @param slaID
     *            the uuid of the sla to renegotiate over
     * @return String representing the id of the negotiation session established
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     * @throws OperationNotPossibleException
     *             if the operation is not possible at all
     */
    public String renegotiate(UUID slaID) throws SLANotFoundException, OperationNotPossibleException;

    /**
     * Terminates SLA with the SLA-Manager.
     * 
     * @param slaID
     *            the uuid of the sla to terminate
     * @param terminationReason
     *            list of termination reasons that must be provided
     * @return boolean value showing if termination was successful or not
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     */
    public boolean terminate(UUID slaID, List<TerminationReason> terminationReason) throws SLANotFoundException;

    /**
     * This method is here for legacy reasons. It is not to be used. Provisioning is related to <code>SLARegistry</code>
     * and <code>ProvisioningAdjustment</code>.
     * 
     * @param slaID
     *            the uuid of the sla to provision
     * @return SLA with provisioning information, if at all
     * @throws SLANotFoundException
     *             if sla is not found in slaregistry of the SLA-Manager
     * @throws ProvisioningException
     *             if provisioning was not possible
     * @see org.slasoi.gslam.core.poc.PlanningOptimization.INotification.activate(SLA sla)
     * @see org.slasoi.gslam.core.pac.ProvisioningAdjustment
     */
    public SLA provision(UUID slaID) throws SLANotFoundException, ProvisioningException;

    /**
     * Defines various reasons for terminating an agreement.
     * 
     * @author Edwin Yaqub
     */
    public enum TerminationReason {
        DEMAND_INCREASED,
        DEMAND_DECREASED,
        FOUND_BETTER_PROVIDER,
        UNSATISFACTORY_QUALITY,
        COST,
        STATUS_COMPROMISED, // e.g. if provider cannot keep up with agreements due to resource shortage or failure.
        FOUND_BETTER_CUSTOMER,
        BUSINESS_DECISION,
        CUSTOMER_INITIATED,
        PROVIDER_INITIATED
    }

    /**
     * Defines various reasons for cancelling an ongoing negotiation.
     * 
     * @author Edwin Yaqub
     */
    public enum CancellationReason {
        STATUS_COMPROMISED, // could mean "under load", "priorities changed" or other internal matters prompted
        // cancellation.
        PARALLEL_NEGOTIATION_SUCCEEDED,
        LOST_CONVERGENCE_HOPE,
        WILL_RETURN_LATER,
    }

    /**
     * Base level class for negotiation related exceptions.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */
    public class NegotiationException extends Exception {
        private static final long serialVersionUID = 1L;

        public NegotiationException(String message) {
            super(message);
        }

        public void setCause(Throwable throwable) {
            super.initCause(throwable);
        }
    }

    /**
     * Exception raised if SLA could not be created.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */
    public class SLACreationException extends NegotiationException {
        private static final long serialVersionUID = 2L;

        public SLACreationException(String message) {
            super(message);
        }
    }

    /**
     * Exception raised if a negotiation operation is not possible.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */
    public class OperationNotPossibleException extends NegotiationException {
        private static final long serialVersionUID = 3L;

        public OperationNotPossibleException(String message) {
            super(message);
        }
    }

    /**
     * Exception raised if the negotiation session is currently processing another operation.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */
    public class OperationInProgressException extends NegotiationException {
        private static final long serialVersionUID = 4L;

        public OperationInProgressException(String message) {
            super(message);
        }
    }

    /**
     * Exception raised if an SLA is not found in the <code>SLARegistry</code>.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */
    public class SLANotFoundException extends NegotiationException {
        private static final long serialVersionUID = 5L;

        public SLANotFoundException(String message) {
            super(message);
        }
    }

    /**
     * Exception raised if an an invalid SLA is encountered.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */
    public class InvalidSLAException extends NegotiationException {
        private static final long serialVersionUID = 6L;

        public InvalidSLAException(String message) {
            super(message);
        }
    }

    /**
     * Exception raised if an <code>SLATemplate</code> is not found in the <code>SLATemplateRegistry</code>.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */
    public class SLATemplateNotFoundException extends NegotiationException {
        private static final long serialVersionUID = 7L;

        public SLATemplateNotFoundException(String message) {
            super(message);
        }
    }

    /**
     * Exception raised if the provided NegotiationID is not recognized by the SLA-Manager.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */
    public class InvalidNegotiationIDException extends NegotiationException {
        private static final long serialVersionUID = 8L;

        public InvalidNegotiationIDException(String message) {
            super(message);
        }
    }

    /**
     * Exception raised if the provisioning process encountered a problem.
     * 
     * @author Edwin Yaqub
     * @version 1.0-SNAPSHOT
     */

    public class ProvisioningException extends NegotiationException {
        private static final long serialVersionUID = 9L;

        public ProvisioningException(String message) {
            super(message);
        }
    }

    /************* Methods below this line are related to Year 3 features ***************/

    /**
     * This enumeration is used in ProtocolParameters class.
     */
    public enum CRITIQUE {
        INCREASE,
        DECREASE,
        UNRECOGNIZED,
        CHANGE,
        ACCEPTED
    }

    /**
     * This enumeration is used in Customization class.
     */
    public enum RESULT {
        AGREED,
        DISAGREED
    }

    public class NegotiationGlossary {
        public static final String CREDENTIALS = "CREDENTIALS";
        public static final String CUSTOMIZATION_ROUNDS = "CUSTOMIZATION_ROUNDS";
        public static final String PROCESS_TIMEOUT = "PROCESS_TIMEOUT";
        public static final String NEGOTIATION_ROUNDS = "NEGOTIATION_ROUNDS";
        public static final String MAX_COUNTER_OFFERS = "MAX_COUNTER_OFFERS";
        public static final String OPTIONAL_CRITIQUE_ON_QOS = "OPTIONAL_CRITIQUE_ON_QOS";
        public static final String IS_SEALED = "IS_SEALED";
    }

    /**
     * This abstract class will represent Protocol Parameters that are customized during the new Customization phase
     * (pre-negotiation). Concrete forms of this class are used in Customization class.
     */
    public abstract class ProtocolParameters {
        // name of this parameter
        private String name;
        // value of this parameter
        private String value;
        // opinion on this parameter by the sending party. Initiator would always set it to ACCEPTED in the start but
        // during customization rounds, it may change.
        private CRITIQUE critique;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public CRITIQUE getCritique() {
            return critique;
        }

        public void setCritique(CRITIQUE critique) {
            this.critique = critique;
        }
    }

    /**
     * This class is of fundamental interest for Customization mechanism. Instances of this class would represent
     * protocol parameters that would be customized and fixed mutually among negotiating parties.
     * 
     * Example :
     * 
     * NegotiableParameters negotiableParameter1 = new NegotiableParameters(NegotiationGlossary.NEGOTIATION_ROUNDS);
     * negotiableParameter1.setValue(5+""); //5 rounds negotiableParameter1.setCritique(CRITIQUE.ACCEPTED);
     * 
     * NegotiableParameters negotiableParameter2 = new NegotiableParameters(NegotiationGlossary.PROCESS_TIMEOUT);
     * negotiableParameter2.setValue((1000 *60*10)+""); //10 minutes
     * negotiableParameter2.setCritique(CRITIQUE.ACCEPTED);
     * 
     * Customization customization = new Customization(); List<NegotiableParameters> listOfNegotiableParameters = new
     * ArrayList<NegotiableParameters>(); listOfNegotiableParameters.add(negotiableParameter1);
     * listOfNegotiableParameters.add(negotiableParameter2);
     * customization.setListOfNegotiableParameters(listOfNegotiableParameters);
     * 
     */
    public class NegotiableParameters extends ProtocolParameters {
        // suggested counter value incase super.ProtocolParameters.value is not ACCEPTED to this party.
        private String counterValue;

        public NegotiableParameters(String name) {
            super.setName(name);
        }

        public String getCounterValue() {
            return counterValue;
        }

        public void setCounterValue(String counterValue) {
            this.counterValue = counterValue;
        }
    }

    /**
     * This class is used to convey non-negotiable but informative parameters to the other party. It is kept here as a
     * place-holder as its not of immediate interest.
     */
    public class InformativeParameters extends ProtocolParameters {

    }

    public class QualityParameters extends InformativeParameters {

    }

    /**
     * This class will represent an instance of Customization settings that would contain e.g., negotiable protocol
     * parameters that negotiating parties exchange (during customization phase) in order to reach consensus on
     * parameters that determine protocol behaviour in the following negotiation phase. In case of no consensus within
     * customizationRounds, negotiation is not allowed.
     */
    public class Customization {

        private List<NegotiableParameters> listOfNegotiableParameters; // of fundamental interest to PE.
        private List<InformativeParameters> listOfInformativeParameters;
        private List<QualityParameters> listOfQualityParameters;
        private RESULT result = RESULT.AGREED; // default value

        public List<NegotiableParameters> getListOfNegotiableParameters() {
            return listOfNegotiableParameters;
        }

        public void setListOfNegotiableParameters(List<NegotiableParameters> listOfNegotiableParameters) {
            this.listOfNegotiableParameters = listOfNegotiableParameters;
        }

        public List<InformativeParameters> getListOfInformativeParameters() {
            return listOfInformativeParameters;
        }

        public void setListOfInformativeParameters(List<InformativeParameters> listOfInformativeParameters) {
            this.listOfInformativeParameters = listOfInformativeParameters;
        }

        public List<QualityParameters> getListOfQualityParameters() {
            return listOfQualityParameters;
        }

        public void setListOfQualityParameters(List<QualityParameters> listOfQualityParameters) {
            this.listOfQualityParameters = listOfQualityParameters;
        }

        /**
         * Auto-compute end result of this customization (end result is DISAGREED if any single NegotiableParameter is
         * not set to ACCEPTED by the sending party. Intentionally, no direct setter is provided for this.result
         * 
         */
        public RESULT getResult() {
            if (listOfNegotiableParameters != null && listOfNegotiableParameters.size() > 0) {
                for (NegotiableParameters negotiableParameter : listOfNegotiableParameters) {
                    if (!negotiableParameter.getCritique().equals(CRITIQUE.ACCEPTED)) {
                        result = RESULT.DISAGREED;
                    }
                }
            }
            return result;
        }

        @Override
        public String toString() {
            if (listOfNegotiableParameters != null && listOfNegotiableParameters.size() > 0) {
                for (NegotiableParameters negotiableParameter : listOfNegotiableParameters) {
                    System.out.println(negotiableParameter.getName() + "=" + negotiableParameter.getValue() + "->"
                            + negotiableParameter.getCritique() + "[CounterValue="
                            + negotiableParameter.getCounterValue() + "]" + "\n");
                }
            }
            return super.toString();
        }
    }

    /**
     * This is the new method needed for Y3 Customization mechanism. It extends the Y2 Bilateral Negotiation Protocol
     * behaviour by introducing a new state called Customize. The Customize state may span several rounds during which
     * negotiating parties exchange Customize object in an attempt to fix protocol parameters.
     * 
     * Important to note is that the semantics of this operation make the returned object and passed in parameter
     * irrelevant for the client programs (e.g., POCs, test bundles or manager bundles). This is because the Protocol
     * Engine uses these in collaboration with protocol rules to enforce customization mechanism.
     * 
     * Basic Event Flow is illustrated below:
     * 
     * Customer Provider 1 SendCustomizationEvent 3 CustomizationArrivedEvent 1.1 Initialize Customize State 3.1 Set
     * param values in rule considering own policies and customer's values 2 Post to Provider 3.2 return own params 4
     * Receive Provider's params. 4.1 CustomizationArrivedEvent 4.2 Set param values in rule considering own policies
     * and providers's values 5 Return updated own params back to client program.
     * 
     * @param negotiationID
     * @param customization
     * @return
     * @throws NegotiationException
     */
    public Customization customize(String negotiationID, Customization customization) throws NegotiationException;

}
