package org.slasoi.gslam.core.negotiation;

import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.tools.Validator.Warning;

/**
 * Provides the methods for parsing and renderings SLAs and SLATs.
 * 
 * @author Peter A. Chronz
 *
 */
public interface ISyntaxConverter {

    /**
     * Parse an SLA object from a specific string-based representation.
     * @param slaObject The string representation of the SLA.
     * @return The parsed SLA object.
     * @throws Exception 
     */
    public abstract Object parseSLA(String slaObject) throws Exception;

    /**
     * Parse an SLAT object from a specific string-based representation.
     * @param slaTemplateObject The string representation of the SLAT.
     * @returnThe parsed SLAT object.
     * @throws Exception
     */
    public abstract Object parseSLATemplate(String slaTemplateObject) throws Exception;

    /**
     * Render an interface specification to a WSDL 2.0 string.
     * @param interfaceSpec The interface specification to render.
     * @return A string representation of the provided interface specification in WSDL 2.0 formatting.
     * @throws Exception
     */
    public abstract String renderWSDL(Interface.Specification interfaceSpec) throws Exception;

    /**
     * Parse a WSDL 2.0 string to an interface specification.
     * @param wsdlString A string containing a WSDL 2.0 document.
     * @return The parsed interface specification.
     * @throws Exception
     */
    public abstract Interface.Specification[] parseWSDL(String wsdlString) throws Exception;
    
    /**
     * Renders an SLA object to a string representation.
     * @param sla The SLA object to render.
     * @return String representation of the SLA, formatting depends on the specific implementation.
     * @throws Exception
     */
    public abstract String renderSLA( SLA sla ) throws Exception;
    
    /**
     * Renders an SLA object to a string representation.
     * @param slaTemplate The SLAT object to render.
     * @return String representation of the template, formatting depends on the specific implementation.
     * @throws Exception
     */
    public abstract String renderSLATemplate( SLATemplate slaTemplate ) throws Exception;
    
    /**
     * Provides a negotiation client for a given EPR. 
     * @param epr The EPR to contact to participate in a negotiation.
     * @return A web service client implementing the INegotiation interface.
     * @throws Exception
     */
    public abstract INegotiation getNegotiationClient(String epr) throws Exception;
    
    /**
     * Provides a negotiation client for direct access to the syntaxconverter. 
     * @throws Exception
     */
    public abstract INegotiation getNegotiation() throws Exception;
    
    /**
     * Parses a constraint expression from string to ConstraintExpr.
     * 
     * @param constraint The string representation of the constraint expression.
     * @return ConstraintExpr object representing the provided constraint expression.
     * @throws Exception
     */
    public ConstraintExpr parseConstraintExpr(String constraint) throws Exception;
    
    /**
     * Renders the SLAMinimumInfo object to a string.
     * @param minInfo
     * @return
     * @throws Exception
     */
    public String renderMinInfo(SLAMinimumInfo minInfo) throws Exception;
    
    /**
     * Defines different kinds of syntax-converters.
     * 
     * @author Peter A. Chronz
     *
     */
    public enum SyntaxConverterType
    {
        //WSAGSyntaxConverter  ,
        SLASOISyntaxConverter;
    }
    
    /**    
     * Defines the operations of the SLA template registry made available as a web service.
     * 
     * @author Peter A. Chronz
     *
     */
    public interface ISLATemplateRegistryClient {
        public Warning[] addSLATemplate(SLATemplate slat, Metadata metadata) throws Exception;

        public UUID[] query(ConstraintExpr metadataQuery) throws Exception;

        public ResultSet query(SLATemplate query, ConstraintExpr metadataConstraint) throws Exception;

        public void removeSLATemplate(UUID templateId) throws Exception;
    }
}
