/* 
SVN FILE: $Id: ORC_templates.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/test/java/org/slasoi/gslam/templateregistry/ORC_templates.java $

*/

package org.slasoi.gslam.templateregistry;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl.Mode;
import org.slasoi.gslam.templateregistry.orc.SLAT_bronze;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.xsd;

import junit.framework.TestCase;

public class ORC_templates extends TestCase{
	
	static Metadata metadata = null;

	private void x_REGISTER(SLATemplate slat, boolean verbose) throws java.lang.Exception{
    	try{
            SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
            reg.setReferenceResolver(new _TEST_REF_RESOLVER());
    		if (metadata == null) metadata = _examples.p_METADATA_1();
    		reg.addSLATemplate(slat, metadata);
    	}catch(java.lang.Exception e){
    	    if (verbose) System.out.println("<< "+e.getMessage());
    	    throw e;
    	}
	}
    
    public void test_ORC_SLAT_BRONZE(){
        try{
            x_REGISTER(new SLAT_bronze(), false);
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
    
    static CONST one_kg = new CONST("1", units.kg);
    static CONST one = new CONST("1", null);
    static CONST one_point_five = new CONST("1.5", null);
    static CONST abc_str = new CONST("abc", xsd.string);

}
