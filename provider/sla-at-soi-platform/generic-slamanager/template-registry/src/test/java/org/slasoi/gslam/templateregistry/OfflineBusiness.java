/* 
SVN FILE: $Id: OfflineBusiness.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/test/java/org/slasoi/gslam/templateregistry/OfflineBusiness.java $

*/

package org.slasoi.gslam.templateregistry;

import java.util.GregorianCalendar;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl.Mode;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Operation.Property;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.Penalty;
import org.slasoi.slamodel.sla.business.PriceModification;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.sla.business.Termination;
import org.slasoi.slamodel.sla.business.TerminationClause;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.xsd;

import junit.framework.TestCase;

public class OfflineBusiness extends TestCase{
	
	static Metadata metadata = null;

	private void x_REGISTER(SLATemplateRegistry reg, SLATemplate slat, String msg, boolean verbose){
    	try{
    		if (metadata == null) metadata = _examples.p_METADATA_1();
    		reg.addSLATemplate(slat, metadata); // note "reg" uses a fake _REF_RESOLVER so can't use static validation !!
            if (msg != null){
                if (verbose) System.out.println("EXPECTED ---->> "+msg);
                fail("Expected exception: " + msg);
            }
    	}catch(java.lang.Exception e){
    		if (msg == null){
    		    e.printStackTrace();
    		    fail("Unexpected exception !");
    		}else{
        		if (verbose) System.out.println("<< "+e.getMessage());
    			assertEquals(msg, e.getMessage());
        		//if (verbose) System.out.println("---->> "+msg);
    		}
    	}
	}
	
	public void testBadContent_PENALTY(){
		try{
			boolean verbose = false;
			
        	SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
        	reg.setReferenceResolver(new _TEST_REF_RESOLVER());
        	
        	SLATemplate slat = x_BASIC_TEMPLATE();
        	
        	Penalty penalty = new Penalty(
        	    new CONST("4", units.Hz) // must be a price
        	);
        	slat.setAgreementTerms(new AgreementTerm[]{ x_AGREEMENT_TERM("TERM-1",penalty) });
        	
        	x_REGISTER(reg, slat, "PENALTY: invalid price: \"4\" Hz", verbose);
        	
        	penalty.setPrice(new CONST("4", units.EUR));
            x_REGISTER(reg, slat, null, verbose);
        	
		}catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
	}
    
    public void testBadContent_TERMINATIONS(){
        try{
            boolean verbose = false;
            
            SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
            reg.setReferenceResolver(new _TEST_REF_RESOLVER());
            
            SLATemplate slat = x_BASIC_TEMPLATE();
            
            // TERMINATION CLAUSE
            
            TerminationClause tc = new TerminationClause(
                new ID("TERMINATION_CLAUSE_1"),
                sla.email, // INVALID: should be an agreement role
                "'type' goes here", // not validated
                "'clause' goes here", // not validated
                sla.forbidden, // (notification method) INVALID: should be a communication protocol
                new CONST("1", units.Hz), // (notification period) INVALID: should be a duration
                new CONST("1", units.Hz) // (fees) INVALID: should be a price
            );
            slat.setAgreementTerms(new AgreementTerm[]{ x_AGREEMENT_TERM("TERM-1",tc) });
            
            x_REGISTER(reg, slat, "TERMINATION_CLAUSE: Invalid Initiator: http://www.slaatsoi.org/slamodel#email", verbose);
            
            tc.setInitiator(sla.provider);
            x_REGISTER(reg, slat, "TERMINATION_CLAUSE: Invalid Notification Method: http://www.slaatsoi.org/slamodel#forbidden", verbose);
            
            tc.setNotificationMethod(sla.email);
            x_REGISTER(reg, slat, "TERMINATION_CLAUSE: Invalid Notification Period: \"1\" Hz", verbose);
            
            tc.setNotificationPeriod(new CONST("1", units.day));
            x_REGISTER(reg, slat, "TERMINATION_CLAUSE: Invalid Fees: \"1\" Hz", verbose);

            tc.setFees(new CONST("1", units.EUR));
            x_REGISTER(reg, slat, null, verbose);
            
            // TERMINATION ACTION
            
            Termination ta = new Termination(
                "not-validated", // name
                new ID("invalid-clause-id") // termination clause id
            );
            // 1. NO TERMINATION-CLAUSES HAVE BEEN DEFINED
            slat.setUuid(new UUID("a different slat uuid")); // the SLAT above was successfully registered :)
            slat.setAgreementTerms(new AgreementTerm[]{ x_AGREEMENT_TERM("TERM-2",ta) });
            x_REGISTER(reg, slat, "TERMINATION_ACTION: Invalid TerminationClause ID: invalid-clause-id", verbose);
            
            // 2. ADD THE TERMINATION CLAUSE (referenced ID is still wrong though)
            slat.setAgreementTerms(new AgreementTerm[]{ x_AGREEMENT_TERM("TERM-2",ta), x_AGREEMENT_TERM("TERM-1",tc) });
            x_REGISTER(reg, slat, "TERMINATION_ACTION: Invalid TerminationClause ID: invalid-clause-id", verbose);
            
            // 3. correct the ID
            ta.setTerminationClauseId(tc.getId());
            x_REGISTER(reg, slat, null, verbose);
            
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }

    public void testBadContent_PRODUCT_OFFERING_PRICE(){
        try{
            boolean verbose = false;
            
            SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
            reg.setReferenceResolver(new _TEST_REF_RESOLVER());
            
            SLATemplate slat = x_BASIC_TEMPLATE();
            
            PriceModification pm = new PriceModification(
                business.one_time_charge, // INVALID : must be 'discount' or 'increment'     
                new CONST("1", units.Hz) // INVALID : must be either a price or a percentage/ratio
            );
            ComponentProductOfferingPrice cpop = new ComponentProductOfferingPrice(
                new ID("CPOP-1"),
                business.discount, // (price type) INVALID : must be a price-type
                new CONST("1", units.Hz), // (price) INVALID : must be a cost
                new CONST("1", units.Hz), // (quantity) INVALID : must be a quantity 
                new PriceModification[]{ pm } // TODO PriceModification is still not fully specified
            );
            ProductOfferingPrice pop = new ProductOfferingPrice(
                new ID("POP_1"),    
                "unvalidated name",
                new TIME(new GregorianCalendar(2010, 1, 1, 12, 00)), 
                new TIME(new GregorianCalendar(2009, 1, 1, 12, 00)), // INVALID : 'until' is before 'from'
                business.discount, // (billing freq.) INVALID : must be a 'billing frequency'
                null, // the product 
                new ComponentProductOfferingPrice[]{ cpop }
            );
            slat.setAgreementTerms(new AgreementTerm[]{ x_AGREEMENT_TERM("TERM-1",pop) });
            
            x_REGISTER(reg, slat, "PRODUCT OFFERING PRICE: Invalid 'validFrom'-'validUntil' range", verbose);
            
            pop.setValidUntil(new TIME(new GregorianCalendar(2011, 1, 1, 12, 00)));
            x_REGISTER(reg, slat, "PRODUCT OFFERING PRICE: Invalid 'billing frequency': http://www.slaatsoi.org/business#discount", verbose);
            
            pop.setBillingFrequency(business.per_request);
            x_REGISTER(reg, slat, "COMPONENT PRODUCT OFFERING PRICE: Invalid 'price type': http://www.slaatsoi.org/business#discount", verbose);
            
            cpop.setPriceType(business.one_time_charge);
            x_REGISTER(reg, slat, "COMPONENT PRODUCT OFFERING PRICE: Invalid 'price': \"1\" Hz", verbose);
            
            cpop.setPrice(new CONST("1", units.EUR));
            x_REGISTER(reg, slat, "COMPONENT PRODUCT OFFERING PRICE: Invalid 'quantity': \"1\" Hz", verbose);
            
            cpop.setQuantity(new CONST("1", null));
            x_REGISTER(reg, slat, "PRICE MODIFICATION: Invalid 'type': http://www.slaatsoi.org/business#one_time_charge", verbose);
            
            pm.setType(business.discount);
            x_REGISTER(reg, slat, "PRICE MODIFICATION: Invalid 'value': \"1\" Hz", verbose);
            
            cpop.setQuantity(new CONST("1", xsd.integer)); // integer type should also be recognised as a 'count'
            x_REGISTER(reg, slat, "PRICE MODIFICATION: Invalid 'value': \"1\" Hz", verbose);
            
            pm.setValue(new CONST("1", units.EUR));
            x_REGISTER(reg, slat, null, verbose);
            
            slat.setUuid(new UUID("another uuid"));
            pm.setValue(new CONST("1", units.percentage));
            x_REGISTER(reg, slat, null, verbose);
            
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
	
	private AgreementTerm x_AGREEMENT_TERM(String id, Guaranteed.Action.Defn defn){
	    return new AgreementTerm(
	        new ID(id),
	        null, // precondition, 
	        null, // variable declarations
	        new Guaranteed[]{
	            new Guaranteed.Action(
	                new ID("ACTION_1"), 
	                sla.provider, 
	                sla.mandatory, 
	                new EventExpr(core.periodic, new Expr[]{ new CONST("1", units.day) }), 
	                defn
	            )
	        }
	    );
	}
	
	private SLATemplate x_BASIC_TEMPLATE(){
    	SLATemplate slat = new SLATemplate();
    	slat.setUuid(new UUID("template1"));
    	slat.setParties(new Party[]{
    	    new Party(new ID("p1"), sla.provider)
    	});
    	slat.setInterfaceDeclrs(new InterfaceDeclr[]{
    	    new InterfaceDeclr(new ID("idec1"), sla.provider, x_ISPEC())
    	});
		return slat;
	}
	
	private Interface.Specification x_ISPEC(){
		Interface.Specification spec = new Interface.Specification("iface_spec1");
		Operation op1 = new Operation(new ID("op1"));
		Property in1 = new Property(new ID("in1"));
		op1.setInputs(new Property[]{ in1 });
		spec.setOperations(new Operation[]{ op1 });
		return spec;
	}

}
