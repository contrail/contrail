/* 
SVN FILE: $Id: OfflineRegister2.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/test/java/org/slasoi/gslam/templateregistry/OfflineRegister2.java $

*/

package org.slasoi.gslam.templateregistry;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl.Mode;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.ResourceType;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.xsd;

import junit.framework.TestCase;

public class OfflineRegister2 extends TestCase{
	
	static Metadata metadata = null;

	private void x_REGISTER(SLATemplateRegistry reg, SLATemplate slat, String msg, boolean verbose){
    	try{
    		if (metadata == null) metadata = _examples.p_METADATA_1();
    		reg.addSLATemplate(slat, metadata); // note "reg" uses a fake _REF_RESOLVER so can't use static validation !!
    		if (msg != null){
                if (verbose) System.out.println("EXPECTED ---->> "+msg);
                fail("Expected exception: " + msg);
    		}
    	}catch(java.lang.Exception e){
    		if (msg == null) e.printStackTrace();
    		else{
        		if (verbose) System.out.println("<< "+e.getMessage());
    			assertEquals(msg, e.getMessage());
        		//if (verbose) System.out.println("---->> "+msg);
    		}
    	}
	}
    
    public void test_SUBSET_OF_RESOURCE_TYPE(){
        try{
            boolean verbose = false;
            
            SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
            reg.setReferenceResolver(new _TEST_REF_RESOLVER());
            
            SLATemplate slat = new SLATemplate();
            slat.setUuid(new UUID("my-template"));
            slat.setParties(new Party[]{
               new Party(new ID("party1"),sla.provider)
            });
            InterfaceDeclr IF1 = new InterfaceDeclr(new ID("IF1"), sla.provider, new ResourceType("VM_Access_Point"));
            IF1.setEndpoints(new Endpoint[]{
                new Endpoint(new ID("EP1"), sla.email)
            });
            ID VM_X = new ID("VM_X");
            slat.setInterfaceDeclrs(new InterfaceDeclr[]{ IF1 });
            slat.setAgreementTerms(new AgreementTerm[]{
                new AgreementTerm(
                    new ID("autogen"),
                    null,
                    new VariableDeclr[]{
                        new VariableDeclr(
                            VM_X,
                            new FunctionalExpr(core.subset_of, new ValueExpr[]{ new ID("IF1") })
                        ) ,
                        new VariableDeclr(
                            new ID("TEMP"),
                            new CONST("90", units.percentage)
                        )     
                    }, 
                    new Guaranteed[]{
                        new Guaranteed.State(
                            new ID("GS1"), 
                            new TypeConstraintExpr(
                                new FunctionalExpr(core.count, new ValueExpr[]{ VM_X }), 
                                new SimpleDomainExpr(new CONST("1", null), core.equals)
                            )
                        ),
                        new Guaranteed.State(
                            new ID("GS2"), 
                            new TypeConstraintExpr(
                                new FunctionalExpr(core.day_is, new ValueExpr[]{}), 
                                new SimpleDomainExpr(new CONST("MONDAY", xsd.gDay), core.equals)
                            )
                        ),
                        new Guaranteed.State(
                            new ID("GS2b"), 
                            new TypeConstraintExpr(
                                new FunctionalExpr(core.day_is, new ValueExpr[]{}), 
                                new SimpleDomainExpr(new CONST("2", xsd.gDay), core.equals)
                            )
                        ),
                        new Guaranteed.State(
                            new ID("GS2c"), 
                            new TypeConstraintExpr(
                                new FunctionalExpr(core.day_is, new ValueExpr[]{}), 
                                new SimpleDomainExpr(new CONST("TUESDAY", null), core.equals)
                            )
                        ),
                        new Guaranteed.State(
                            new ID("GS3"), 
                            new TypeConstraintExpr(
                                new FunctionalExpr(core.month_is, new ValueExpr[]{}), 
                                new SimpleDomainExpr(new CONST("JANUARY", xsd.gMonth), core.equals)
                            )
                        )
                    }
                ),
                new AgreementTerm(
                    new ID("autogen2"),
                    null,
                    new VariableDeclr[]{
                        new VariableDeclr(
                            VM_X,
                            new FunctionalExpr(core.subset_of, new ValueExpr[]{ new ID("IF1") })
                        )     
                    }, 
                    new Guaranteed[]{
                        new Guaranteed.State(
                            new ID("GS1"), 
                            new TypeConstraintExpr(
                                new FunctionalExpr(core.count, new ValueExpr[]{ new ID("autogen2/VM_X") }), 
                                new SimpleDomainExpr(new CONST("1", null), core.equals)
                            )
                        )
                    }
                )    
            });

            x_REGISTER(reg, slat, null, verbose);
            
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
    
    static CONST one_kg = new CONST("1", units.kg);
    static CONST one = new CONST("1", null);
    static CONST one_point_five = new CONST("1.5", null);
    static CONST abc_str = new CONST("abc", xsd.string);

}
