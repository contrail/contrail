/* 
SVN FILE: $Id: OfflineQuery.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/test/java/org/slasoi/gslam/templateregistry/OfflineQuery.java $

*/

package org.slasoi.gslam.templateregistry;

import java.util.ArrayList;
import java.util.List;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet.Result;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.InterfaceRef;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.sla;

import junit.framework.TestCase;

public class OfflineQuery extends TestCase{

	static boolean trace = false;
	
    public void test(){
        try{
        	SLATemplate simple1 = _examples.p_SLAT_simple1();
        	SLATemplate simple2 = _examples.p_SLAT_simple2();
        	SLATemplate composite1 = _examples.p_SLAT_composite1();
        	Metadata slam_provider_meta = _examples.p_METADATA_1();
        	Metadata third_provider_meta = _examples.p_METADATA_2();
        	
        	SLATemplateRegistryImpl reg = _examples.p_SLATREG(null, false);
        	reg.addSLATemplate(simple1, third_provider_meta);
        	reg.addSLATemplate(simple2, third_provider_meta);
        	reg.addSLATemplate(composite1, slam_provider_meta);

        	if (trace) System.out.println("------------------------------");
        	// everything null -> all available templates
        	ResultSet resultset = reg.query(null, null);
        	if (!x_TEST_Q_RESULTS(resultset, new UUID[]{
	        		_examples.template_composite1_id,
	        		_examples.template_simple1_id,
	        		_examples.template_simple2_id
	        	})){ fail("Query 1"); }

        	if (trace) System.out.println("------------------------------");
        	// only third party providers
        	SimpleDomainExpr SDE1 = new SimpleDomainExpr(new ID(_examples.$another_provider_id), core.equals);
        	TypeConstraintExpr TCE1 = new TypeConstraintExpr(Metadata.provider_uuid, SDE1);
        	resultset = reg.query(null, TCE1);
        	if (!x_TEST_Q_RESULTS(resultset, new UUID[]{
            		_examples.template_simple1_id,
            		_examples.template_simple2_id
            	})){ fail("Query 2"); }

        	if (trace) System.out.println("------------------------------");
        	// only third parties providing simple2 ..
        	SLATemplate query = new SLATemplate();
        	query.setParties(new Party[]{
        	    new Party(
        	        new ID("p1"),
        	        sla.provider
        	    )
            });
        	query.setInterfaceDeclrs(new InterfaceDeclr[]{
                new InterfaceDeclr(
                	new ID("x"), 
                	sla.provider, 
                	new InterfaceRef(new UUID(_examples.$i_simple2))
                )
        	});
        	resultset = reg.query(query, TCE1);
        	if (!x_TEST_Q_RESULTS(resultset, new UUID[]{
            		_examples.template_simple2_id
            	})){ fail("Query 3"); }
        	
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
    
    private boolean x_TEST_Q_RESULTS(ResultSet resultset, UUID[] expected){
    	List<UUID> uuids = new ArrayList<UUID>();
    	for (Result r : resultset.results){
    		uuids.add(r.getUuid());
    		if (trace) System.out.println(r.toString());
    	}
    	if (uuids.size() != expected.length) return false;
    	List<UUID> list = new ArrayList<UUID>();
    	for (UUID uuid : uuids) list.add(uuid);
    	for (UUID x : expected){
    		if (list.contains(x)) list.remove(x);
    		else return false;
    	}
    	return true;
    }

}
