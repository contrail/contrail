/* 
SVN FILE: $Id: OfflineExtensions.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/test/java/org/slasoi/gslam/templateregistry/OfflineExtensions.java $

*/

package org.slasoi.gslam.templateregistry;

import junit.framework.TestCase;

import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl.Mode;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.xsd;
import org.slasoi.slamodel.vocab.ext.Category;
import org.slasoi.slamodel.vocab.ext.Extensions;
import org.slasoi.slamodel.vocab.ext.Functional;
import org.slasoi.slamodel.vocab.ext.Nominal;
import org.slasoi.slamodel.vocab.ext.Nominal.Role;

public class OfflineExtensions extends TestCase{
	
	private static class _EXT implements Extensions{
		public String[] getDays(){ return new String[0]; }
		public String[] getMonths(){ return new String[0]; }
		public Nominal[] getNominals(){ return new Nominal[0]; }
		public Functional[] getFunctionals(){ return new Functional[0]; }
		public UUID getUuid(){ return new UUID("MyDefns"); }
	}
	
	public void testBadExtensions(){
		try{
        	SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
        	
			// can't remove built-in extensions ..
        	try{
        		reg.removeExtensions(new common());
        		fail("1");
        	}catch(Exception e){
        		assertEquals("Built-in definitions can't be removed : http://www.slaatsoi.org/commonTerms#", e.getMessage());
            }
        	try{
        		reg.removeExtensions(new core());
                fail("2");
        	}catch(Exception e){
        		assertEquals("Built-in definitions can't be removed : http://www.slaatsoi.org/coremodel#", e.getMessage());
            }
        	try{
        		reg.removeExtensions(new xsd()); // OK to remove XSD !!
        		reg.removeExtensions(new units()); // so should get exception here instead
                fail("3");
        	}catch(Exception e){
        		assertEquals("Built-in definitions can't be removed : http://www.slaatsoi.org/coremodel/units#", e.getMessage());
            }
    		reg.addExtensions(new xsd()); // we removed it in last test !!
        	try{
        		Extensions ext = new _EXT(){
					public Nominal[] getNominals(){ return new Nominal[]{
						new Nominal(new STND("my-logical-op"), meta.BOOLEAN, Role.LOGICAL_OP), // illegal
					};}
        		};
        		reg.addExtensions(ext);
                fail("4");
        	}catch(Exception e){
        		assertEquals("Domain-specific logical/comparison operators are not supported: NOMINAL::my-logical-op::BOOLEAN::[LOGICAL_OP]", e.getMessage());
            }
        	try{
        		Extensions ext = new _EXT(){
					public Nominal[] getNominals(){ return new Nominal[]{
						new Nominal(new STND("my-comparison-op"), meta.BOOLEAN, Role.COMPARISON_OP), // illegal
					};}
        		};
        		reg.addExtensions(ext);
                fail("5");
        	}catch(Exception e){
        		assertEquals("Domain-specific logical/comparison operators are not supported: NOMINAL::my-comparison-op::BOOLEAN::[COMPARISON_OP]", e.getMessage());
            }
        	try{
        		Extensions ext = new _EXT(){
					public Nominal[] getNominals(){ return new Nominal[]{
						new Nominal(sla.endpoint, meta.ENDPOINT, Role.OPERATION_PROPERTY) // duplicate
					};}
        		};
        		reg.addExtensions(ext);
                fail("5");
        	}catch(Exception e){
        		assertEquals("NOMINAL::endpoint::ENDPOINT::[OPERATION_PROPERTY] conflicts with existing nominal", e.getMessage());
            }
        	try{
        		Extensions ext = new _EXT(){
					public Functional[] getFunctionals(){ return new Functional[]{
						new Functional(new STND("evaluator"), new Category[]{}, meta.NUMBER, true), // "true" = eval
					}; }
        		};
        		reg.addExtensions(ext);
                fail("6");
        	}catch(Exception e){
        		assertEquals("Domain-specific evaluation functions are not supported: FUNCTIONAL::evaluator/0::NUMBER", e.getMessage());
            }
        	try{
        		Extensions ext = new _EXT(){
					public Functional[] getFunctionals(){ return new Functional[]{ // conflict with existing
						new Functional(common.availability, new Category[]{ meta.SERVICE }, meta.RATIO, false),
					}; }
        		};
        		reg.addExtensions(ext);
                fail("7");
        	}catch(Exception e){
        	    assertEquals("FUNCTIONAL::availability/1::RATIO conflicts with existing functional", e.getMessage());
            }
		}catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
	}

}
