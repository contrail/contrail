/* 
SVN FILE: $Id: _TEST_REF_RESOLVER.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/test/java/org/slasoi/gslam/templateregistry/_TEST_REF_RESOLVER.java $

*/

package org.slasoi.gslam.templateregistry;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ReferenceResolver;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.service.Interface.Operation;

class _TEST_REF_RESOLVER implements ReferenceResolver{

	static final String $OPX = "opX";
	static final String $A = "A";
	static final String $B_exts_A = "B_exts_A";
	static final String $B_no_ops = "B_no_ops";
	static final String $B_opX = "B_opX";

	public Specification getInterfaceSpecification(UUID location){
		//System.out.println("_TEST_REF_RESOLVER ? "+location.getValue());
		if (location.getValue().equals($B_exts_A)) return x_B_EXTS_A();
		if (location.getValue().equals($B_no_ops)) return x_B_NO_OPS();
		if (location.getValue().equals($B_opX)) return x_B_OPX();
		return null;
	}
	
	private Specification x_B_EXTS_A(){
		Interface.Specification spec = new Interface.Specification($B_exts_A);
		spec.setExtended(new ID[]{ new ID($A) });
		return spec;
	}
	
	private Specification x_B_NO_OPS(){
		Interface.Specification spec = new Interface.Specification($B_no_ops);
		return spec;
	}
	
	private Specification x_B_OPX(){
		Interface.Specification spec = new Interface.Specification($B_opX);
		Operation opX = new Operation(new ID($OPX));
		spec.setOperations(new Operation[]{ opX });
		return spec;
	}

}
