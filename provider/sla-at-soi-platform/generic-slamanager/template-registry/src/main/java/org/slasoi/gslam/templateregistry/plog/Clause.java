/* 
SVN FILE: $Id: Clause.java 1944 2011-05-31 14:36:04Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1944 $
@lastrevision   $Date: 2011-05-31 16:36:04 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/Clause.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.*;


/**
 * Class encapsulating atomic & compound Prolog terms.
 * @author Keven T. Kearney
 */
public class Clause extends Term{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static boolean isUnquotedAtom(String s){
		return _m_parser.isUnquotedAtom(s);
	}
	
	private List<Term> args = Collections.synchronizedList(new ArrayList<Term>());
	protected String quoted = null;
	_m_wrapper context = null;
	
	/* -------------------------------------------------------------
	 * CONSTRUCTORS
	 * ------------------------------------------------------------- */
	
	/**
	 * Construct a clause conforming to the predicate-indicator
	 * ({@link PI PI}) <code>pi</code> &amp; with the given arguments.
	 */
	public Clause(PI pi, Object... args) throws Error{
		this(pi);
		add(args);
		if (arity() != pi.arity){
			throw Error.syntax("Invalid number of arguments for " + pi.toString());
		}
	}
	
	/**
	 * Construct a clause whose name is taken from the 
	 * predicate-indicator ({@link PI PI}) <code>pi</code>.
	 */
	public Clause(PI iso){
		super(iso.name, PI.atom_0);
		try{ 
			init(iso.name); 
		}catch(Error e){
			throw new IllegalStateException("This should never happen");
		}
	}
	
	/**
	 * Construct an integer atom.
	 */
	public Clause(int i){
		super("" + i, PI.integer_0);
		quoted = this.name;
	}
	
	/**
	 * Construct a floating-point atom.
	 */
	public Clause(float f){
		super("" + f, PI.float_0);
		quoted = this.name;
	}
		
	/**
	 * Construct a clause with the given name &amp; arguments.
	 */
	public Clause(String name, Object... args) throws Error{
		this(name);
		add(args);
	}
	
	/**
	 * Construct a clause with the specified name.
	 */
	public Clause(String name) throws Error{
		super(name, PI.atom_0);
		init(name);
		try{
			int i = _m_parser.convertToInteger(name); // handles different bases
			this.name = new String("" + i);
			quoted = this.name;
			type = PI.integer_0;
		}catch(Exception e){
			try{
				float f = _m_parser.convertToFloat(name);
				this.name = new String("" + f);
				quoted = this.name;
				type = PI.float_0;
			}catch(Exception fe){}
		}
		if (type == PI.atom_0 && !_m_parser.isAtomic(name)){
			throw Error.syntax("Illegal clause name \"" + name + "\"");
		}
	}
	
	private void init(String name) throws Error{
		if (_m_parser.isQuotedAtom(name)){
			this.name = name.substring(1, name.length()-1).replaceAll("''", "'");
		}
		if (_m_parser.isUnquotedAtom(this.name)){
			quoted = this.name;
		}else{
			quoted = "'" + this.name.replaceAll("'", "''") + "'";
		}
//		quoted = name;
	}
	
	/* -------------------------------------------------------------
	 * CLAUSE INFO
	 * ------------------------------------------------------------- */
	
	/**
	 * Retrieve the name of this clause - with single-quote 
	 * delimiters if required by the Prolog syntax.
	 */
	public String quoted(){
		return this.quoted;
	}
	
	/**
	 * Retrieve the predicate indicator (<code>PI</code>) for this clause 
	 * - where <code>PI</code> is a clause of the form <code>Name/Arity</code>.  
	 */
	public PI pi(){
		try{
			Clause c = this.is(PI.rule_n)? this.arg(0).asClause(): this;
			String name = c.name; 
			if (!_m_parser.isUnquotedAtom(name)) name = "'" + name + "'";
			return new PI(name, c.arity());
		}catch(Exception e){
			throw new IllegalStateException("This should never happen");
		}
	}
	
	boolean ispi(){
		if (this.is(PI.divide_2)){
			Term name = this.arg(0);
			Term arity = this.arg(1);
			return 
			(name.type == PI.var_0 || name.type == PI.atom_0)
			&& 
			(arity.type == PI.var_0 || arity.type == PI.integer_0);
		}
		return false;
	}
	
	/* -------------------------------------------------------------
	 * ARGUMENTS
	 * ------------------------------------------------------------- */
	
	/**
	 * Add the specified arguments to this Clause.
	 */
	public void add(Object... args) throws Error{
		for (Object a : args){
			if (a instanceof Term) add((Term)a);
			else if (a instanceof PI) add(new Clause((PI)a));
			else if (a instanceof String) add(new Clause((String)a));
			else if (a instanceof Integer) add(new Clause((Integer)a));
			else if (a instanceof Float) add(new Clause((Float)a));
			else if (a instanceof Boolean){
				PI pi = ((Boolean)a).booleanValue()?
					PI.true_0:
					PI.false_0;
				add(new Clause(pi));
			}
			else{
				throw Error.syntax("Invalid argument type: " + a.getClass().getName());
			}
		}
	}
	
	/**
	 * Retrieve the arity of this clause (atoms have zero arity).
	 */
	public final int arity(){
		return args.size();
	}
	
	/**
	 * Add the term <code>arg</code> as a child of this clause - this clauses's 
	 * arity is increased by 1, and it's type is set to {@link PI#compound_0 BI#compound_0}.
	 */
	public final void add(Term arg){
		if (this.type == PI.atom_0 || this.type == PI.compound_0){
			args.add(arg);
			this.type = PI.compound_0;
		}else{
			String s = this.type == PI.integer_0? "an integer": 
					   this.type == PI.float_0? "a float":
					   "type " + type;
			throw new IllegalStateException("Can't add arguments to " + s);
		}
	}
	
	/**
	 * Retrieve the list of this clauses's arguments (children) as a {@link Term Term[]}.
	 */
	public final Term[] args(){
		return args.toArray(new Term[0]);
	}
	
	/**
	 * Retrieve the {@link Term Term} argument (child) at index <code>i</code>
	 */
	public final Term arg(int i){
		if (i < 0 || i >= args.size()){
			throw new IllegalArgumentException("Index '" + i + "' is out of bounds (arity = " + arity() + ")");
		}
		return args.get(i);
	}
	
	/* -------------------------------------------------------------
	 * CONVERSIONS
	 * ------------------------------------------------------------- */
	
	/**
	 * Retrieve the integer value represented by this clause (if applicable).
	 */
	public final int asInteger() throws Error{
		if (isType(PI.integer_0)) return new Integer(name);
		throw Error.type(PI.integer_0, this);
	}
	
	/**
	 * Retrieve the floating-point value represented by this clause (if applicable).
	 */
	public final float asFloat() throws Error{
		if (isType(PI.number_0)) return new Float(name);
		throw Error.type(PI.number_0, this);
	}
	
	/**
	 * Retrieve the character value represented by this clause (if applicable).
	 */
	public char asChar() throws Error{
		if (!isType(PI.atom_0) || name.length() != 1){
			throw Error.representation(PI.character_0);
		}
		return name.charAt(0);
	}
	
	/* -------------------------------------------------------------
	 * OBJECT OVER-RIDES
	 * ------------------------------------------------------------- */
	
	public final int compareTo(Term o){
		if (o instanceof Var){
			return +1;
		}else if (o instanceof Clause){
			Clause c = (Clause)o;
			if (this.type == PI.compound_0){
				if (c.type == PI.compound_0){
					if (this.arity() < c.arity()) return -1;
					else if (this.arity() > c.arity()) return 1;
					else {
						int a = this.name.compareTo(c.name);
						if (a != 0) return a;
						for (int i=0; i<this.arity(); i++){
							a = this.arg(i).compareTo(c.arg(i));
							if (a != 0) return a;
						}
						return 0;
					}
				}else{
					return +1;
				}
			}else if (this.type == PI.float_0){
				return c.type == PI.float_0?
					new Float(this.name).compareTo(new Float(c.name)):
					-1;
			}else if (this.type == PI.integer_0){
				return c.type == PI.float_0? +1: 
					c.type == PI.integer_0?
						new Integer(this.name).compareTo(new Integer(c.name)):
					-1;
			}else if (this.type == PI.atom_0){
				return (c.type == PI.float_0 || c.type == PI.integer_0)?
					+1:
					c.type == PI.atom_0?
					this.name.compareTo(c.name):
					-1;
			}
		}
		throw new ClassCastException("it.eng.iso.lang.Term required"); 
	}
	
	public boolean equals(Object obj){
		if (this == obj) return true;
		if (obj instanceof Clause) return this.toString().equals(obj.toString());
		return false;
	}
	
	/* -------------------------------------------------------------
	 * PACKAGE RESTRICTED
	 * ------------------------------------------------------------- */
	
	boolean entails(Term x){
		if (this == x) return true;
		for (int i=0; i<args.size(); i++){
			Term c = args.get(i);
			if (c.entails(x)) return true;
		}
		return false;
	}
	
	final String uniformSyntax(boolean quoted, boolean numbervars, _m_parser parser) throws Error{
		String s = quoted? this.quoted: name;
		boolean hash = name.equals("#");
		if (arity() > 0){
			if (!hash) s += "(";
			for (int i=0; i<arity(); i++){
				s += parser != null?
						parser.write(arg(i), quoted, numbervars):
							arg(i).uniformSyntax(quoted, numbervars, null);
						if (i<arity()-1) s += ",";
			}
			if (!hash) s += ")";
		}
		return s;
	}
	
	/* -------------------------------------------------------------
	 * ITERATORS
	 * ------------------------------------------------------------- */

	/**
	 * An minimal iterator over {@link Clause Clauses}.
	 * @author Keven T. Kearney
	 */
	public static abstract class Iterator {
		
		boolean fail_after_success = false;
		public Iterator(){ this(false); }
		Iterator(boolean fail_after_success){ this.fail_after_success = fail_after_success; }
		
		/**
		 * Return the next {@link Clause Clause} in this iterator
		 * (<code>null</code> if there are no more clauses).
		 */
		public abstract Clause next() throws Error;
		
	}
	
	public static class OneShot extends Clause.Iterator{
		
		Clause c = null;
		boolean done = false;
		
		public OneShot(Clause c){
			this.c = c;
			if (c == null) done = true;
		}
		
		public Clause next() throws Error{
			if (done) return null;
			done = true;
			return once();
		}
		
		protected Clause once() throws Error{
			return c;
		}

	}

}
