/* 
SVN FILE: $Id: SLATemplateRegistryImpl.java 1946 2011-05-31 14:59:34Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1946 $
@lastrevision   $Date: 2011-05-31 16:59:34 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/SLATemplateRegistryImpl.java $

*/

package org.slasoi.gslam.templateregistry;

import java.util.ArrayList;
import java.util.Collections;   
import java.util.List;

import org.slasoi.gslam.core.context.SLAMContextAware; 
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.tools.Validator;
import org.slasoi.slamodel.sla.tools.Validator.Report;
import org.slasoi.slamodel.sla.tools.Validator.Warning;
import org.slasoi.slamodel.vocab.ext.Extensions;

public class SLATemplateRegistryImpl implements SLATemplateRegistry, SLAMContextAware{
	
	/**
	 * "runtime operation mode" flags.
	 */
    public static enum Mode{
		OFFLINE_TEST,
		ONLINE_TEST_CLEAR,
        ONLINE_TEST,
		ONLINE
	}
    
    private SLAManagerContext context = null; 
    
    private Mode mode = Mode.OFFLINE_TEST;
	private ___storage storage = null;
	private ____plog plog = null;
	private Validator validator = null;
	private __is_resolver is_resolver = null;
    
    public SLATemplateRegistryImpl(){
    	this(Mode.ONLINE);
    }
    
    // Injection method. called by OSGi
    public void setSLAManagerContext(SLAManagerContext context){
        this.context = context;
    }

    public void shutdown(){ x_SHUTDOWN(); }
    
    /**
     * This constructor is provided for testing purposes only. It should not be used :)
     * @param mode one of <code>OFFLINE_TEST</code>, <code>ONLINE_TEST</code> or <code>ONLINE</code>
     */
    public SLATemplateRegistryImpl(Mode mode){
        this.mode = mode;
        try{
            x_CHECK_BACKEND();
        	UUID[] uuids = storage.p_GET_ALL_SLAT_UUIDS();
        	System.out.println("EXISTING SLA(T) UUIDS:");
        	for (UUID uuid : uuids){
        		System.out.println("* "+uuid.toString());
        	}
        }catch(java.lang.Exception e){
        }
    }
    
    public void openConsole() throws SLATemplateRegistry.Exception{
    	x_CHECK_BACKEND();
    	plog.p_OPEN_CONSOLE();
    }
    
    protected void finalize(){ x_SHUTDOWN(); }
    
    private void x_SHUTDOWN(){
        if (storage != null) try{
            storage.p_DISCONNECT();
        }catch(java.lang.Exception e){
            e.printStackTrace();
        }finally{
            _notifications.ext_loggers.clear();
            _notifications.message("[S] closed");
        }
    }
    
	public void addListener(Listener q){
		if (!_notifications.ext_loggers.contains(q)) _notifications.ext_loggers.add(q);
	}
    
	public void removeListener(Listener q){
		_notifications.ext_loggers.remove(q);
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // VALIDATION
    /*-------------------------------------------------------------------------------------------------------------------------*/
    
    /**
     * Validates SLATemplate content<br/>
     * "offline" = does not have access to previously registered templates (or interface specifications).<br/>
     * Note that this may produce invalid results wrt:
     * <ul>
     *      <li>template UUID conflicts (i.e. can't determine if a template has already been registered)</li>
     *      <li>validity of interface specifications (i.e. may not have access to referred/extended interfaces)</li> 
     * </ul>
     * <b>THIS METHOD IS NOT YET COMPLETE (WORK IN PROGRESS)</b>
     * @param slat
     * @throws Exception if any illegal content is encountered
     */
    public static Warning[] offlineValidate(SLATemplate slat) throws SLATemplateRegistry.Exception{
        SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
        return reg.validateSLATemplate(slat);
    }
    
    public Warning[] validateSLATemplate(SLATemplate slat) throws SLATemplateRegistry.Exception{
        x_CHECK_BACKEND();
        x_VALIDATE_SLAT_UUID(slat);
    	try{
            Report res = validator.validate(slat);
            return res.warnings.toArray(new Warning[0]);
    	}catch(Validator.Exception ve){
    		throw new SLATemplateRegistry.Exception(ve.getMessage(), ve.sourceEntity());
    	}
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // DOMAIN-SPECIFIC EXTENSIONS
    /*-------------------------------------------------------------------------------------------------------------------------*/

    public void setReferenceResolver(ReferenceResolver resolver) throws SLATemplateRegistry.Exception{
    	x_CHECK_BACKEND();
        is_resolver.p_SET_REF_RESOLVER(resolver);
    } 

    public void addExtensions(Extensions exts) throws SLATemplateRegistry.Exception{
    	x_CHECK_BACKEND();
        try{
    		validator.addExtensions(exts);
    	}catch(Validator.Exception ve){
    		throw new SLATemplateRegistry.Exception(ve.getMessage(), ve.sourceEntity());
    	}
    }

    public void removeExtensions(Extensions exts) throws SLATemplateRegistry.Exception{
    	x_CHECK_BACKEND();
        try{
    		validator.removeExtensions(exts);
    	}catch(Validator.Exception ve){
    		throw new SLATemplateRegistry.Exception(ve.getMessage(), ve.sourceEntity());
    	}
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // METADATA-QUERY
    /*-------------------------------------------------------------------------------------------------------------------------*/

    public UUID[] query(ConstraintExpr metadata_query) throws SLATemplateRegistry.Exception{
        x_CHECK_BACKEND();
        if (metadata_query == null) try{
        	return storage.p_GET_ALL_SLAT_UUIDS();
        }catch(java.lang.Exception e){
        	throw new SLATemplateRegistry.Exception(e);
        }else return plog.p_METADATA_QUERY(metadata_query); // never returns null ()
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // TEMPLATE+METADATA QUERY
    /*-------------------------------------------------------------------------------------------------------------------------*/

    public ResultSet query(SLATemplate query, ConstraintExpr metadata_constraint){
    	ResultSet resultset = new ResultSet();
    	try{
    		x_CHECK_BACKEND();
    		// 1. validate the query !!
    		Report report = null;
    		if (query != null){
                report = validator.validate(query);
                for (Warning w : report.warnings) resultset.warnings.add(w);
    		}
    		
    		// 2. perform the metadata query ...
            UUID[] templateIds = query(metadata_constraint);
            
            // 3. perform the template matching ...
            if (templateIds.length==0){
				resultset.warnings.add(new Warning("No templates found with matching metadata",query));
            }else{
                plog.p_TEMPLATE_QUERY(query, templateIds, resultset, report);
                Collections.sort(resultset.results);
            }
    	}catch(java.lang.Exception e){
    	    e.printStackTrace();
    		resultset.errors.add(e);
    	}
        return resultset;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // METADATA
    /*-------------------------------------------------------------------------------------------------------------------------*/

    public void setMetadataProperty(UUID templateId, STND key, String value) throws SLATemplateRegistry.Exception{
    	if (key == null) throw new IllegalArgumentException("No key specified");
        if (value == null) throw new IllegalArgumentException("No value specified");
        String $mod = x_GET_SLAT_MODULE_ID(templateId); // guaranteed result or exception
        plog.p_SET_METADATA_PROPERTY($mod, key, value);
    }
    
    public String getMetadataProperty(UUID templateId, STND key) throws SLATemplateRegistry.Exception{
        if (key == null) throw new IllegalArgumentException("No key specified");
        String $mod = x_GET_SLAT_MODULE_ID(templateId); // guaranteed result or exception
        return plog.p_GET_METADATA_PROPERTY($mod, key);
    }
    
    public Metadata getMetadata(UUID templateId) throws Exception{
        String $mod = x_GET_SLAT_MODULE_ID(templateId); // guaranteed result or exception
        return plog.p_GET_METADATA($mod);
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // GET ISPECS
    /*-------------------------------------------------------------------------------------------------------------------------*/

    public Specification getInterfaceSpecification(String interfaceName) throws SLATemplateRegistry.Exception{
        x_CHECK_BACKEND();
        try{
            Specification spec = storage.p_GET_SPEC(interfaceName);
            if (spec != null) return spec;
            throw new SLATemplateRegistry.Exception("No Interface.Specification exists with name: "+interfaceName, interfaceName);
        }catch(java.lang.Exception e){
            throw new SLATemplateRegistry.Exception(e);
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // GET TEMPLATE
    /*-------------------------------------------------------------------------------------------------------------------------*/

    public SLATemplate getSLATemplate(UUID templateId) throws SLATemplateRegistry.Exception{
        x_CHECK_BACKEND();
        try{
    		SLATemplate slat = storage.p_GET_SLAT(templateId);
    		if (slat != null) return slat;
        	throw new SLATemplateRegistry.Exception("No SLAT exists with UUID: "+templateId, templateId);
        }catch(java.lang.Exception e){
        	throw new SLATemplateRegistry.Exception(e);
        }
    }

    
    public UUID[] getSLATemplateIds() throws SLATemplateRegistry.Exception{
        x_CHECK_BACKEND();
        try{
    		return storage.p_GET_ALL_SLAT_UUIDS();
        }catch(java.lang.Exception e){
        	throw new SLATemplateRegistry.Exception(e);
        }
    }
    

    public SLATemplate[] getSLATemplates() throws SLATemplateRegistry.Exception {
        x_CHECK_BACKEND();
        List<SLATemplate> tlist = new ArrayList<SLATemplate>();
        try{
        	UUID[] ids = this.getSLATemplateIds();
        	if (ids == null) return null;
        	for (UUID id : ids) {
        		tlist.add(this.getSLATemplate(id));
        	}
        	return tlist.toArray(new SLATemplate[0]);
        } catch(java.lang.Exception e){
        	throw new SLATemplateRegistry.Exception(e);
        }
    }
    
   
    
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // ADD TEMPLATE
    /*-------------------------------------------------------------------------------------------------------------------------*/

    public Warning[] addSLATemplate(SLATemplate slat, SLATemplateRegistry.Metadata metadata) throws SLATemplateRegistry.Exception{
        try{
        	x_CHECK_BACKEND();
            // if (slat instanceof SLA){
            //    throw new SLATemplateRegistry.Exception("REG: SLAs can not be stored in the SLATemplateRegistry :)", slat);
            //}
            UUID templateId = x_VALIDATE_SLAT_UUID(slat);
            x_VALIDATE_SLAT_METADATA(metadata, templateId);
        	Report res = validator.validate(slat);
            plog.p_REGISTER(res, metadata);
        	_notifications.templateRegistered(templateId);
            return res.warnings.toArray(new Warning[0]);
    	}catch(Validator.Exception ve){
    		SLATemplateRegistry.Exception e = new SLATemplateRegistry.Exception(ve.getMessage(), ve.sourceEntity());
        	_notifications.registerTemplateFailed(slat, e);
    		throw e;
    	}catch(SLATemplateRegistry.Exception e){
        	_notifications.registerTemplateFailed(slat, e);
    		throw e;
    	}catch(java.lang.Exception e){
        	_notifications.registerTemplateFailed(slat, e);
    		throw new SLATemplateRegistry.Exception(e);
    	}
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    // REMOVE TEMPLATE
    /*-------------------------------------------------------------------------------------------------------------------------*/

    public void removeSLATemplate(UUID templateId) throws SLATemplateRegistry.Exception{
        // NOT IMPLEMENTED
    }
    
/*----PRIVATE HELPER--------------------------------------------------------------------------------------------------------*/
    
    private UUID x_VALIDATE_SLAT_UUID(SLATemplate slat) throws SLATemplateRegistry.Exception{
        if (slat == null) throw new SLATemplateRegistry.Exception("REG: No SLATemplate specified", slat);
        UUID templateId = slat.getUuid();
        if (templateId == null) throw new SLATemplateRegistry.Exception("REG: No template UUID specified", slat);
        try{
            if (storage.p_GET_SLAT_MODULE_ID(templateId) != null){
                throw new SLATemplateRegistry.Exception("REG: SLATemplate already exists (duplicate template UUID)", templateId);
            }
            return templateId;
        }catch(java.lang.Exception e){
        	throw new SLATemplateRegistry.Exception(e);
        }
    }
    
    private void x_VALIDATE_SLAT_METADATA(SLATemplateRegistry.Metadata metadata, UUID templateId) throws SLATemplateRegistry.Exception{
    	if (metadata == null) throw new SLATemplateRegistry.Exception("No Metadata specified", null);
        String $provider_uuid = metadata.getPropertyValue(Metadata.provider_uuid);
        if ($provider_uuid == null) throw new SLATemplateRegistry.Exception("Missing 'provider_uuid' in Metadata", metadata);
        String $template_uuid = metadata.getPropertyValue(Metadata.template_uuid);
        if ($template_uuid == null){
            metadata.setPropertyValue(Metadata.template_uuid, templateId.getValue());
        }
        String $registrar_id = metadata.getPropertyValue(Metadata.registrar_id);
        if ($registrar_id == null) throw new SLATemplateRegistry.Exception("Missing 'registrar_id' in Metadata", metadata);
    }
    
    private String x_GET_SLAT_MODULE_ID(UUID templateId) throws SLATemplateRegistry.Exception{
        if (templateId == null) throw new SLATemplateRegistry.Exception("No templateId specified", null);
    	x_CHECK_BACKEND();
        try{
        	String moduleId = storage.p_GET_SLAT_MODULE_ID(templateId);
        	if (moduleId != null) return moduleId;
        	throw new SLATemplateRegistry.Exception("No SLAT exists with UUID: "+templateId, templateId);
        }catch(java.lang.Exception e){
        	throw new SLATemplateRegistry.Exception(e);
        }
    }
    
    private void x_CHECK_BACKEND() throws SLATemplateRegistry.Exception{
        if (mode != Mode.OFFLINE_TEST && context == null) throw new SLATemplateRegistry.Exception("No SLAManager context available", null);
        try{ 
        	if (storage == null){
            	//System.out.println("SLAT_REG_INIT ...");
        		switch (mode){
		            case OFFLINE_TEST: storage = new ___in_memory_storage(); break;
		            case ONLINE_TEST_CLEAR: x_SETUP_JDBC_STORAGE(true, true); break;
                    case ONLINE_TEST: x_SETUP_JDBC_STORAGE(true, false); break;
		            default: x_SETUP_JDBC_STORAGE(false, false); break;
	        	}
            	is_resolver = new __is_resolver(context, plog, storage);
            	validator = new Validator(is_resolver);
            	plog = new ____plog(storage, validator.getMetaModel());
            	_notifications.message("initialised ("+mode+")");
            }else{
                storage.p_CHECK_CONNECTION();
            }
        }catch(java.lang.Exception e){
            throw new SLATemplateRegistry.Exception(e);
        }
    }
    
    private void x_SETUP_JDBC_STORAGE(boolean test, boolean delete_previous){
    	try{
        	storage = new ___jdbc_storage(context, test, delete_previous);
    	}catch(java.lang.Exception e){
    	    e.printStackTrace();
        	storage = new ___in_memory_storage();
        	_notifications.message("Unable to connect to database; no persistence available");
    	}
    }

}