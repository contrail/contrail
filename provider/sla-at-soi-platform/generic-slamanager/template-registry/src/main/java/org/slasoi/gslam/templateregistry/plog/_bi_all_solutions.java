/* 
SVN FILE: $Id: _bi_all_solutions.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/_bi_all_solutions.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

class _bi_all_solutions {

	static Clause.Iterator solve(final Engine eng, final _m_wrapper m, final Clause c){
		return new Clause.Iterator(){
			boolean done = false;
			Term template = null; 
			Clause goal = null;
			_solution ALL = null;
			List<Clause> keys = null;
			public Clause next() throws Error{
				if (done) return null;
				if (ALL == null){
					template = c.arg(0);
					goal = c.arg(1).asCallable();
					Term list = c.arg(2);
					if (!list.isType(PI.var_0)){
						Term.fromPrologList(list.asClause());
					}
					ALL = findall(m, eng, template, goal, c);
					keys = ALL.keys();
				}
				Clause rlist = null;
				if (c.is(PI.findall_3)){
					rlist = Term.toPrologList(ALL.solutions.get(c.pi().asClause().toString()));
					done = true;
				}else if (!keys.isEmpty()){
					goal = keys.remove(0);
					rlist = Term.toPrologList(ALL.solutions.get(goal.toString()));
				}else{
					done = true;
					return null;
				}
				Clause x = new Clause(c.name);
					x.add(template);
					x.add(goal);
					x.add(rlist);
				return x;
			}
		};
	}
	
	private static _solution findall(_m_wrapper m, Engine eng, Term template, Clause goal, Clause caller) throws Error{
		List<Var> free_vars = new ArrayList<Var>();
		Var[] vs = new Var[0];
		if (template.isType(PI.var_0)) vs = new Var[]{(Var)template};
		else vs = new Var.Accessor(template.asClause()).variables().toArray(new Var[0]);
		for (Var v : vs) if (!free_vars.contains(v)) free_vars.add(v);
		Clause query = goal;
		if (goal.is(PI.hat_2)){
			query = goal.arg(1).asCallable();
			Term t = goal.arg(0);
			if (t.isType(PI.var_0)) vs = new Var[]{(Var)t};
			else vs = new Var.Accessor(t.asClause()).variables().toArray(new Var[0]);
			for (Var v : vs) if (!free_vars.contains(v)) free_vars.add(v);
		}
		Session	session = Session.create(eng, m, query);
		_solution result = new _solution(caller.is(PI.setof_3));
		while (session.solveNext()){
			Term solution = m.parse("x("+template.uniformSyntax(true, false, null)+").").arg(0);
			for (Var v : free_vars) v.unbind(); // don't want free vars used in key
			Clause key = caller.is(PI.findall_3)?
				caller.pi().asClause(): // the same key is used for all solutions !!
				(Clause)instantiate(goal);
			result.add(key, solution);
		}
		return result;
	}

	private static Term instantiate(Term x) throws Error{
		if (x.isType(PI.var_0)) {
			Var v = (Var)((Var)x).binding();
			return !v.isAnonymous() && v.name.startsWith("_")? new Var(x.name) : v;
		}else{
			Clause X = x.asClause();
			Clause c = new Clause(X.quoted);
			for (int i=0; i<X.arity(); i++) c.add(instantiate(X.arg(i)));
			return c;
		}
	}
	
	private static class _solution{
		
		private List<String> keys = new ArrayList<String>();
		Map<String, List<Term>> solutions = new Hashtable<String, List<Term>>();
		private Map<String, Clause> goals = new Hashtable<String, Clause>();
		private boolean sorted = false;
		
		_solution(boolean sorted){
			this.sorted = sorted;
		}
		
		void add(Clause key, Term solution){
			String $key = key.toString();
			List<Term> list = solutions.get($key);
			if (list == null){
				keys.add($key);
				goals.put($key, key);
				list = new ArrayList<Term>();
				solutions.put($key, list);
			}
			if (!sorted || !list.contains(solution)){
				list.add(solution);
				if (sorted) Collections.sort(list);
			}
		} 
		
		List<Clause> keys(){
			List<Clause> list = new ArrayList<Clause>();
			for (String key : keys) list.add(goals.get(key));
			if (sorted) Collections.sort(list);
			return list;
		}
		
	}
	
}
