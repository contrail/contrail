/* 
SVN FILE: $Id: ConsolePanel.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/gui/ConsolePanel.java $

*/

package org.slasoi.gslam.templateregistry.plog.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.Reader;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;

import org.slasoi.gslam.templateregistry.plog.Console;

public class ConsolePanel extends JPanel{

	private static final long serialVersionUID = 1L;

	private static final Font CONSOLE_FONT = new Font("Courier", Font.BOLD, 13);
	
	private ____OUTPUT output = null;
	private ____INPUT input = null;
	private ____CONSOLE_TEXT_AREA textArea = null;
	
	public ConsolePanel(Console console){
		if (console == null) throw new IllegalArgumentException("No Console specified");
		setLayout(new BorderLayout());
		final ____CONSOLE_DOC doc = new ____CONSOLE_DOC("?- ");
		textArea = new ____CONSOLE_TEXT_AREA(doc);
		add(textArea, BorderLayout.CENTER);
		this.output = new ____OUTPUT(textArea);
		this.input = new ____INPUT(textArea);
			textArea.addKeyListener(this.input);
		console.setOutput(this.output);
		console.setReader(new BufferedReader(this.input));
		new Thread(console).start();
		doc.clear();
	}
	
	/*--------------------------------------------------------------
	 * CONSOLE DOCUMENT
	 *--------------------------------------------------------------*/
	
	private class ____CONSOLE_DOC extends DefaultStyledDocument{

		private static final long serialVersionUID = 1L;
		int pos = 0;
		String def = "";
		
		____CONSOLE_DOC(String def){
			this.def = def;
		}

		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException{
			if (offs >= pos) super.insertString(offs, str, a);
		}
		
		public void remove(int offs, int len) throws BadLocationException{
			if (offs >= pos) super.remove(offs, len);
		}
		
		void clear(){
			if (pos > 0) try{
				super.remove(0, getLength());
				super.insertString(0, def, null);
				pos = getLength();
			}catch(BadLocationException e){
				e.printStackTrace();
			}
		}
		/*
		void clearCurrent(){
			try{ //clear only from pos onwards
				remove(pos, getLength()-pos);
				pos = getLength();
			}catch(BadLocationException e){
				e.printStackTrace();
			}
		}
		*/
		
	}
	
	/*--------------------------------------------------------------
	 * TEXT AREA
	 *--------------------------------------------------------------*/

	private class ____CONSOLE_TEXT_AREA extends JTextArea{
		
		private static final long serialVersionUID = 1L;
		____CONSOLE_DOC doc;
		
		____CONSOLE_TEXT_AREA(____CONSOLE_DOC doc){
			super(doc);
			this.doc = doc;
			setBorder(new EmptyBorder(5,10,5,5));
			setDocument(doc);
			setFont(CONSOLE_FONT);
			setBackground(Color.blue);
			setForeground(Color.white);
			setCaretColor(Color.white);
			setOpaque(true);
		}
		
		public void setCaretPosition(int position){
			if (position >= doc.pos) super.setCaretPosition(position);
		}
		
	}
	
	/*--------------------------------------------------------------
	 * INPUT
	 *--------------------------------------------------------------*/

	private class ____INPUT extends Reader implements KeyListener{
		
		____CONSOLE_TEXT_AREA text;
		____CONSOLE_DOC doc;
		PrintStream err = System.err;
		boolean ready = false;
		boolean first = true;
		
		____INPUT(____CONSOLE_TEXT_AREA text){
			this.text = text;
			this.doc = (____CONSOLE_DOC)text.getDocument();
		}
		
		//KEY LISTENER
		public void keyPressed(KeyEvent e){}
		
		public void keyReleased(KeyEvent e){}
		
		public synchronized void keyTyped(KeyEvent e){
			invokeKey(e.getKeyChar());
		}
		
		synchronized void invokeKey(int c){
			if (c == KeyEvent.VK_ENTER){
				ready = true;
				err.flush();
				notifyAll();
			}else if (c == KeyEvent.VK_ESCAPE){
				boolean more = text.getText().endsWith("more y/n? ");
				doc.clear();
				if (more){
					text.append("more y/n? ");
					doc.pos = doc.getLength();
				}
			}
		}
		
		//READER
		
		public void close(){}
		
		public synchronized int read(char[] cbuf, int off, int len){
			if (first){
				doc.clear();
				first = false;
			}
			doc.pos = doc.getLength();
			text.setCaretPosition(doc.pos);
			text.repaint(); //caret is often drawn badly
	    	while (!ready) try{ wait(); }catch(Exception e){}
	    	ready = false;
	    	String s = "(error)";
	    	try{
	    		s = doc.getText(doc.pos, doc.getLength() - doc.pos);
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	if (s.length() > len) s = s.substring(0,len);
	    	char[] c = s.toCharArray();
	    	System.arraycopy(c, 0, cbuf, off, c.length);
	    	return c.length;
		}
		
		public synchronized boolean ready(){
			return ready;
		}
	    
	}
	
	/*--------------------------------------------------------------
	 * OUTPUT
	 *--------------------------------------------------------------*/

	private class ____OUTPUT extends PrintStream{
		
		____CONSOLE_TEXT_AREA text = null;
		____CONSOLE_DOC doc;
		boolean suppressed = false;
		
		____OUTPUT(____CONSOLE_TEXT_AREA text){ 
	   		super(System.out); 
	   		this.text = text;
	   		this.doc = (____CONSOLE_DOC)text.getDocument();
	   	}
	    	
	   	public void print(String s){
	   		if (!suppressed) text.append(s);
	   	}
	    	
	   	public void println(String s){ //title only !!
	   		if (!suppressed) text.append(s + "\n");
	   	}
	   	
	   	public void flush(){
	   		doc.clear();
	   	}
	    	
	}
	
}
