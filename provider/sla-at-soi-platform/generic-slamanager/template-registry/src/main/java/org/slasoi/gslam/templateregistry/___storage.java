/* 
SVN FILE: $Id: ___storage.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/___storage.java $

*/

package org.slasoi.gslam.templateregistry;

import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.SLATemplate;

import org.slasoi.gslam.templateregistry.plog.Clause;

interface ___storage{
	
	public void p_CHECK_CONNECTION();

    public Clause[] p_GET_PREDICATES(String moduleId, String predicateSig) throws Exception;
    
    public void p_ADD_PREDICATE(Clause clause, String moduleId, String predicateSig, boolean append) throws Exception;
    
    public boolean p_REMOVE_PREDICATE(Clause clause, String moduleId, String predicateSig) throws Exception;
    
    public String[] p_GET_PREDICATE_SIGS(String moduleId) throws Exception;
    
    // SLATemplates ...
    
    public String[] p_GET_ALL_SLAT_MODULE_IDS() throws Exception;
    
    public UUID[] p_GET_ALL_SLAT_UUIDS() throws Exception;
	
	public String p_GET_SLAT_MODULE_ID(UUID templateUuid) throws Exception;
	
	public UUID p_GET_SLAT_UUID(String moduleId) throws Exception;
    
    public SLATemplate p_GET_SLAT(UUID templateUuid) throws Exception;
    
    public void p_ADD_SLAT(SLATemplate slat, String moduleId) throws Exception;
    
    // Specifications ...
	
	public boolean p_EXISTS_SPEC(String name) throws Exception;
    
    public Specification p_GET_SPEC(String name) throws Exception;
    
    public void p_ADD_SPEC(Specification spec) throws Exception;
	
	public void p_DISCONNECT() throws Exception;

}
