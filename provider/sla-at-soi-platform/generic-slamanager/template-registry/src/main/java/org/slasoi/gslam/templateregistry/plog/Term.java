/* 
SVN FILE: $Id: Term.java 1944 2011-05-31 14:36:04Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1944 $
@lastrevision   $Date: 2011-05-31 16:36:04 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/Term.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Abstract class encapsulating all Prolog terms.
 * @author Keven T. Kearney
 */
public abstract class Term implements Comparable<Term>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String name = null;
	protected PI type = PI.atom_0;
	
	protected Term(String name, PI type){
		if (name == null) throw new IllegalArgumentException("No name specified");
		this.name = name;
		this.type = type;
	}
	
	/**
	 * Returns <code>true</code> if the type of this term matches the argument.<br>
	 * Terms are classified according to the following types:
	 * <ul>
	 *  <li>{@link PI#var_0 BI#var_0} - an unbound variable,
	 *  <li>{@link PI#atom_0 BI#atom_0} - an atom (arity is 0, and name is a character sequence),
	 *  <li>{@link PI#integer_0 BI#integer_0} - an integer (arity is 0, and name evaluates to an integer value),
	 *  <li>{@link PI#float_0 BI#float_0} - a floating-point (arity is 0, and name evaluates to a floating-point value),
	 *  <li>{@link PI#number_0 BI#number_0} - either an integer or floating-point,
	 *  <li>{@link PI#atomic_0 BI#atomic_0} - either an atom or number,
	 *  <li>{@link PI#compound_0 BI#compound_0} - neither a variable nor atomic,
	 *  <li>{@link PI#nonvar_0 BI#nonvar_0} - not an unbound variable.
	 * </ul>
	 */
	public boolean isType(PI type){
		if (type == PI.nonvar_1) return !isType(PI.var_1);
		else if (this.type == type) return true;
		else if (type == PI.atomic_0){
			return this.type == PI.atom_0 || 
				this.type == PI.integer_0 ||
				this.type == PI.float_0;
		}else if (type == PI.number_0){
			return this.type == PI.integer_0 || this.type == PI.float_0;
		}
		return false;
	}
	
	/**
	 * Retrieve the (unquoted) name of this term.
	 * @see Clause#quoted() for retrieving
	 * qoted names.
	 */
	public String name(){
		return name;
	}
	
	abstract boolean entails(Term x);
	
	/**
	 * Retrieve <code>true</code> if this term matches the predicate-indicator given
	 * by <code>iso</code>. If this term is a variable, the binding of the variable 
	 * is tested.
	 */
	public boolean is(PI iso){
		if (this instanceof Clause && iso != null){
			Clause c = (Clause)this;
			int a = c.arity();
			return iso.name().equals(c.quoted) && 
				(a == iso.arity || 
				(iso.arity < 0 && a >= Math.abs(iso.arity)) // for arity = 'n'
				);
		}
		return false;
	}
	
	/**
	 * Retrieve this term as a {@link Clause Clause} object (if this term is 
	 * a bound variable, it's bound clause is returned).
	 * @throws Error - <code>instantiation_error</code> 
	 * if this term is an unbound variable.
	 */
	public Clause asClause() throws Error{
		Term t = this;
		if (t instanceof Var) t = ((Var)t).binding();
		if (t instanceof Clause) return (Clause)t;
		throw Error.instantiation();
	}
	
	/**
	 * Retrieve this term as a callable {@link Clause Clause} object - i.e. the same 
	 * as <code>asClause()</code>, but with throws an exception if this term is
	 * not callable.
	 * @throws Error - <code>type_error(callable, this)</code>
	 * if this term is not callable.
	 */
	public Clause asCallable() throws Error{
		Term t = this;
		if (t instanceof Var) t = ((Var)t).binding();
		if (t instanceof Clause){
			Clause c = (Clause)t;
			if ((c.type == PI.atom_0 || c.type == PI.compound_0) &&
				!c.is(PI.emptylist_0) && 
				!c.is(PI.list_2)); // OK -> callable
			else{
				throw Error.type(PI.callable_0, c);
			}
			return c;
		}else{
			throw Error.instantiation();
		}
	}
	
	/**
	 * Used to implement the ISO defined term comparison predicates.
	 */
	public abstract int compareTo(Term o); 
	
	public int hashCode(){
		return name.hashCode();
	}
	
	abstract String uniformSyntax(boolean quoted, boolean numbervars, _m_parser parser) throws Error;
	
	/**
	 * Returns the uniform-syntax representation of this term (if this term is a 
	 * bound variable, then the uniform-syntax representation of the 
	 * bound clause is returned).
	 */
	public String toString(){
		try{
			return uniformSyntax(true, false, null);
		}catch(Exception e){
			throw new IllegalStateException("This should never happen");
		}
	}
	
	/**
	 * Returns the uniform-syntax representation of this term (if this term is a 
	 * bound variable, then the uniform-syntax representation of the 
	 * bound clause is returned).
	 */
	public String toString(_m_parser parser, boolean quoted){
		try{
			return uniformSyntax(quoted, false, parser);
		}catch(Exception e){
			throw new IllegalStateException("This should never happen");
		}
	}
	
	/* -------------------------------------------------------------
	 * STATIC UTILITY
	 * ------------------------------------------------------------- */
	
	public static Clause toPrologList(List<Term> tlist){
		if (tlist == null || tlist.size() == 0) return new Clause(PI.emptylist_0);
		Clause c = new Clause(PI.list_2);
		c.add(tlist.get(0));
		List<Term> list = new ArrayList<Term>();
		for (int i=1; i<tlist.size(); i++) list.add(tlist.get(i));
		Clause tail = toPrologList(list);
		c.add(tail);
		return c;
	}
	
	/**
	 * Retrieve the specified Prolog list <code>t</code> as a list of {@link Term Terms}.
	 * @throws Error
	 */
	public static List<Term> fromPrologList(Clause t) throws Error{
		if (!isPrologList(t)){
			throw Error.type(PI.list_0, t);
		}
		List<Term> list = new ArrayList<Term>();
		if (t.is(PI.emptylist_0)) return list;
		list.add(t.arg(0));
		try{
			Clause ctail = t.arg(1).asClause(); // --> instantiation error
			List<Term> rest = fromPrologList(ctail);
			for (Term u : rest) list.add(u);
		}catch(Exception e){
			list.add(t.arg(1));
		}
		return list;
	}
	
	/**
	 * Retrieve <code>true</code> if the {@link Term Term} <code>t</code>
	 * can be instantiated to a Prolog list (includes the empty-list).
	 */
	public static boolean isPrologList(Term t) {
		try{
			Clause c = t.asClause();
			return (c.is(PI.emptylist_0)) || (c.is(PI.list_2));
		}catch(Exception e){
			e.printStackTrace();
		}
		return false; 
	}
	
	/**
	 * Retrieve a representation of the specified {@link Term Term} list
	 * as a (nested) Prolog conjunction - i.e. <code>','/2</code> (if the list 
	 * has a single element, then that element is returned). 
	 */
	public static Term toPrologConjunction(List<Term> tlist){
		if (tlist == null || tlist.size() == 0) return null;
		if (tlist.size() == 1) return tlist.get(0); 
		Clause c = new Clause(PI.conjunction_2);
			c.add(tlist.get(0));
		List<Term> list = new ArrayList<Term>();
		for (int i=1; i<tlist.size(); i++) list.add(tlist.get(i));
		Term tail = toPrologConjunction(list);
		c.add(tail);
		return c;
	}
	
	/**
	 * Retrieve the specified Prolog conjunction <code>t</code> as a list of {@link Term Terms}.
	 * @throws Error
	 */
	public static List<Term> fromPrologConjunction(Term t) throws Error{
		List<Term> list = new ArrayList<Term>();
		if (t.is(PI.conjunction_2)){
			Clause c = t.asClause();
			list.addAll(fromPrologConjunction(c.arg(0)));
			list.addAll(fromPrologConjunction(c.arg(1)));	
		}else{
			list.add(t);
		}
		return list;
	}
	
	/* -------------------------------------------------------------
	 * PACKAGE RESTRICTED
	 * ------------------------------------------------------------- */
	
	static Clause instantiate(Clause x, boolean autogen_vars) throws Error{
		return (Clause)instantiate(x, new Hashtable<String, Term>(), autogen_vars);
	}
	
	private static Term instantiate(Term x, Hashtable<String, Term> h, boolean autogen_vars) throws Error{
		if (x.isType(PI.var_0)) {
			Var v = ((Var)x).isAnonymous()? 
				autogen_vars? Var.autogen(): new Var("_"): 
				(Var)h.get(x.name);
			if (v == null){
				v = autogen_vars? Var.autogen(): (Var)x;
				h.put(x.name, v);
			}
			return v;
		}else{
			Clause X = x.asClause();
			Clause c = new Clause(X.quoted);
			for (int i=0; i<X.arity(); i++){
				c.add(instantiate(X.arg(i), h, autogen_vars));
			}
			return c;
		}
	}
	
}
