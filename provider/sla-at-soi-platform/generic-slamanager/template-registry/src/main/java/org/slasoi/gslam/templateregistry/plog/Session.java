/* 
SVN FILE: $Id: Session.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/Session.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.*;


/**
 * Encapsulates a Prolog query "Session".
 * <p>
 * <i>Example:</i>
 * <pre>
 *     Engine engine = new Engine();
 *     Session session = engine.parse("member(X,[a,b,c])");
 *     while (session.solveNext()){
 *         Var v = session.var("X");
 *         Term result = v.binding();
 *         System.out.println("X = " + result.toString());
 *     }
 * </pre>
 * @author Keven T. Kearney
 */
public final class Session extends Var.Accessor{
	
	static final int STACK_MAX = 10000;
	static final int SUCCESS = -50;
		
	Engine eng = null;
	private List<_goal> goals = Collections.synchronizedList(new ArrayList<_goal>());
	private int current = 0;
	private int depth = 0;
	private boolean bk = false;
	private boolean halted = false;
	private Clause query = null;
	
	static Session create(Engine eng, _m_wrapper module, Clause c) throws Error{
		if (c == null) throw new IllegalArgumentException("No Clause specified");
		if (!c.is(PI.query_n)){
			Clause q = new Clause(PI.query_n);
			q.add(c);
			c = q;
		}
		return new Session(eng, module, c);
	}
	
	private Session(Engine eng, _m_wrapper module, Clause c) throws Error{
		super(c);
		this.eng = eng;
		this.query = c;
		List<Term> list = Collections.synchronizedList(new ArrayList<Term>());
		for (int i=0; i<c.arity(); i++) list.add(c.arg(i));
		addGoals(module, list.toArray(new Term[0]), true);
		current = 0;
	}
	
	/**
	 * Retrieve the query which this session is solving.
	 */
	public Clause query(){
		return query;
	}
	
	//private enum Port{ Call, Exit, Redo, Fail }
	/*
	void trace(Port port, _goal g, int depth){
		try{
			String s = "";
			for (int i=0; i<depth; i++) s += "  ";
			s += port.toString() + ": " + g.toString();
			eng.out.println(s);
		}catch(Exception e){
			eng.__ERROR(e);
		}
	}*/
	
	/**
	 * Returns <code>true</code> if there is another solution to the query
	 * in this session (any variables in the query will be instantiated).
	 * @throws Error
	 */
	public boolean solveNext() throws Error{
		if (halted) return false;
		int next = current;
		_goal x = null;
		//boolean debug = false;
		while(!halted){
			if (goals.size()>STACK_MAX){
				throw new IllegalStateException("Out of Stack");
			}
			if (next == SUCCESS) {
				//if (debug) trace(Port.Exit, x, depth);
				bk = true; //next solve is a backtrack !
				return true; //no change to current (for next solve)!
			}else if (next < 0){
				//if (debug) trace(Port.Fail, x, depth);
				return false;
			}
			current = next;
			if (current >= goals.size()) return false;
			x = goals.get(current);
			_m_wrapper context = x.context(eng);
			//debug = context.getFlag(new Clause(PI.debug_0)).is(PI.on_0);
			//if (debug) trace(bk? Port.Redo: Port.Call, x, depth);
			if (x.is(PI.halt_0) || x.is(PI.halt_1)){
				Clause halt = x.term().asClause();
				if (halt.is(PI.halt_1)){
					halt.arg(0).asClause().asInteger(); // -> exception
				}
				halted = true;
			}else if (x.is(PI.fail_0)){
				next = backtrack(x);
			}else if (x.is(PI.true_0) || x.is(PI.cut_0)){
				next = bk? backtrack(x): contpt(x , true);
			}else if (x.is(PI.unify_2)){
				Clause c = x.term().asClause();
				if (!bk && unify(x, c.arg(0), c.arg(1))) next = contpt(x, true);
				else next = backtrack(x);
			}else if (x.is(PI.not_unify_2)){
				Clause c = x.term().asClause();
				if (!bk && !unify(x, c.arg(0), c.arg(1))) next = contpt(x, true);
				else next = backtrack(x);
			}else{
				if (!x.isAllocated()){
					Clause c = x.callable();
					Clause.Iterator ci = eng.__CANDIDATES(context, c);
					if (ci == null){
						boolean exit = true;
						if (c.is(PI.op_3)){
							// do nothing
						}else{
							//Clause unkn = context.getFlag(new Clause(PI.unknown_0));
							//if (unkn.is(PI.error_0)){
							//	throw Error.existence(PI.procedure_0, c.pi().asClause());
							//}else if (unkn.is(PI.warning_0)){
							//	eng.__WARN("unknown procedure: " + c.toString());
							//}else{
								exit = false;
							//}
						}
						//if (debug) trace(Port.Fail, x, depth);
						if (exit) return false;
					}
					x.allocate(ci);
				}
				Term[] subgoals = next(x); // -> PrologException
				if (subgoals != null) {
					if (subgoals.length == 0) next = contpt(x, false); //fact
					else {
						boolean b_old = bk;
						try{
							bk = false;
							next = addGoals(context, subgoals, false); //rule
							depth++;
						}catch(Exception e){
							bk = b_old;
							next = contpt(x, false);
						}
					}
				}else{
					next = backtrack(x);
				}
			}
		}
		return false; // when halted !!
	}
	
	private int addGoals(_m_wrapper m, Term[] newgoals, boolean first) throws Error{
		int n = goals.size();
		int len = newgoals.length;
		if (len == 0) return current;
		_goal caller = first? null: goals.get(current);
		int callercp = first? SUCCESS: caller.cp;
		int bk = first? -1: current;
		for (int i=0; i<len; i++){
			Term t = newgoals[i];
			_goal g = new _goal(m, eng, t);
			boolean cut = g.is(PI.cut_0);
			if (i == 0) g.cap = true;
			goals.add(g);
			if (cut) g.bk = bkForCut(caller);//(caller != null)? caller.bk: -1;
			else g.bk = (i == 0)? bk: goals.size()-2; //on backtrack
			g.cp = (i == len-1)? callercp: goals.size();
			if (callercp >= 0 && i == len-1){
				_goal z = goals.get(callercp); //set bk of continuation goal
				z.pushbk(/*cut? g.bk:*/ goals.size()-1);
			}
		}
		return n;
	}
	
	private int bkForCut(_goal caller){
		while (caller != null && caller.transparentToCuts()){
			int bk = caller.bk;
			caller = bk >= 0? goals.get(caller.bk): null;
		}
		return caller != null? caller.bk: -1;
	}
	
	private Term[] next(_goal g) throws Error{
		Clause match = g.next();
		while (match != null){ 
			if (g.is(PI.retract_1)){
				return new Term[0]; // just for effieciency
			}
			match = Term.instantiate(match, true); // replace vars with autogen vars
			if (match.is(PI.halt_0) || match.is(PI.halt_1)){
				this.halted = true;
				return null;
			}
			Clause head = match.is(PI.rule_n)? 
				match.arg(0).asClause():
				match;
			if (unify(g, g.term(), head)){
				List<Term> subgoals = new ArrayList<Term>();
				if (match.is(PI.rule_n)){
					for (int i=1; i<match.arity(); i++){
						subgoals.add(match.arg(i));
					}
				}
				return subgoals.toArray(new Term[0]); //empty subgoals -> fact
			}else{
				match = g.next();
			}
		}
		return null;
	}
	
	private int contpt(_goal x, boolean inc_depth){
		bk = false; //not backtracking
		if (x.cp != SUCCESS) goals.get(x.cp).deallocate();
		if (inc_depth) depth++;
		return x.cp;
	}
	
	private int backtrack(_goal x){
		bk = true; //backtracking
		x.deallocate();
		if (x.cap){ //backtracking from subgoal head
			_goal z = goals.get(goals.size()-1);
			if (z.cp >= 0){
				z = goals.get(z.cp);
				z.popbk();
			}
			while (goals.size()>current) {
				z = goals.remove(goals.size()-1);
				z.deallocate();
			}
		}
		depth--;
		if (current == x.bk) return -1;
		return x.bk;
	}
	
	private boolean unify(_goal goal, Term THIS, Term THAT){
		Hashtable<Var, Term> bindings = new Hashtable<Var, Term>();
		//if (unify(matched_head, goal.term(), bindings)) {
		if (unify(THIS, THAT, bindings)) { // <- required for CUT test a(X) binding order problem !!
			for (Var v : bindings.keySet()){
				try{
					goal.bind(v, bindings.get(v));
				}catch(Exception ex){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	static boolean unify(Term THIS, Term THAT, Hashtable<Var, Term> bindings){
		if (THIS instanceof Var) THIS = ((Var)THIS).binding();
		if (THAT instanceof Var) THAT = ((Var)THAT).binding();
		if (THIS == THAT) return true;	
		if (THIS instanceof Var) {
			Term binding = bindings.get(THIS); //candidate binding
			if (binding == null){
				bindings.put((Var)THIS, THAT); //bind THIS to THAT
			}else {
				if (binding == THAT) return true;
				else return unify(binding, THAT, bindings);
			}
		}else if (THAT instanceof Var){
			return unify(THAT, THIS, bindings);
		}else {
			Clause x = (Clause)THIS;
			Clause y = (Clause)THAT;
			if (!x.name.equals(y.name)) return false;
			if (x.arity() != y.arity()) return false;
			if (x.arity() > 0){
				for (int i=0; i<x.arity(); i++){
					try{
						Term a = x.arg(i);
						Term b = y.arg(i);
						if (!unify(a, b, bindings)) return false;
					}catch(Exception e){ 
						return false; 
					}
				}	
			}
		}
		return true;
	}
	
}
