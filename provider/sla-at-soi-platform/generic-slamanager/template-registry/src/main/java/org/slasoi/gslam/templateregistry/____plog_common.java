/* 
SVN FILE: $Id: ____plog_common.java 1964 2011-06-01 11:23:50Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1964 $
@lastrevision   $Date: 2011-06-01 13:23:50 +0200 (sre, 01 jun 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/____plog_common.java $

*/

package org.slasoi.gslam.templateregistry;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.Party.Operative;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;

import org.slasoi.gslam.templateregistry.plog.Clause;
import org.slasoi.gslam.templateregistry.plog.Error;
import org.slasoi.gslam.templateregistry.plog.PI;
import org.slasoi.gslam.templateregistry.plog.Var;

class ____plog_common {
	
	// SLATs
	static final String $slat = "slat";
    static final String $uuid = "uuid";
    static final String $party = "party";
    static final String $operative = "operative";
	// ISPECs & RESOURCE-TYPES
	static final String $iface = "iface";
    static final String $_functional = "functional";
    static final String $_resource = "resource";
    static final String $_monthly_tariff_usage = "monthly_tariff_usage";
    
    static final String $separator = "###";
	static final SimpleDateFormat sdf = new SimpleDateFormat(units.$date_format);
    
    static String x_KEY(PI pi){ return pi.name()+$separator+pi.arity(); }
    
    static PI x_PI(String key){
    	String[] s = key.split(____plog_common.$separator);
    	return new PI(s[0],Integer.parseInt(s[1]));
    }
    
    static String x_TIME(Calendar cal){ return sdf.format(cal.getTime()); }
	
	static String p_TEMPLATE_UUID(UUID uuid){
		StringBuffer buf = new StringBuffer();
		buf.append($uuid+"('");
		buf.append(uuid.getValue());
		buf.append("')");
		return buf.toString();
	}
	
	static String p_IFACE(String name, STND role, boolean is_res_type){
		StringBuffer buf = new StringBuffer();
		buf.append($iface+"('");
		buf.append(name);
		buf.append("','");
		buf.append(role.getValue());
        buf.append("',");
        buf.append(is_res_type? $_resource: $_functional);
		buf.append(")");
		return buf.toString();
	}
	
	static String p_PARTY(Party p){
		StringBuffer buf = new StringBuffer();
		buf.append($party+"('");
		buf.append(p.getId().getValue());
		buf.append("','");
		buf.append(p.getAgreementRole().getValue());
		buf.append("')");
		return buf.toString();
	}
	
	static String p_PARTY_OPERATIVE(Party p, Operative o){
		StringBuffer buf = new StringBuffer();
		buf.append($operative+"('");
		buf.append(p.getId().getValue());
		buf.append("','");
		buf.append(o.getId().getValue());
		buf.append("')");
		return buf.toString();
	}
	
	static void p_PARSE_METADATA(Metadata metadata, List<String> clauses) throws Error{
		STND[] keys = metadata.getPropertyKeys();
		for (STND key : keys){
			String $val = metadata.getPropertyValue(key);
			String s = p_PARSE_METADATA_PROPERTY(key, $val);
			clauses.add(s);
		}
	}
	
	static String p_PARSE_METADATA_PROPERTY(STND key, String value) throws Error{
		StringBuffer buf = new StringBuffer();
		buf.append("'");
		buf.append(key.getValue());
		buf.append("'('");
		buf.append(value);
		buf.append("')");
		return buf.toString();
	}
	
	static Clause p_EXTRACT_MONTHLY_TARIFF_USAGE_PRICE(SLATemplate slat, Var constrained_var) throws Error{
		if (slat != null){
	        AgreementTerm[] ats = slat.getAgreementTerms();
	        for (AgreementTerm at : ats){
	        	Guaranteed[] gs = at.getGuarantees();
	        	for (Guaranteed g : gs){
	        		if (g instanceof Action){
	        			Action ga = (Action)g;
	        			String $actor = ga.getActorRef().getValue();
	        			if ($actor.equals(sla.$provider)){
		        			Action.Defn post = ga.getPostcondition();
		        			if (post instanceof ProductOfferingPrice){
		        				ComponentProductOfferingPrice[] cpops = ((ProductOfferingPrice)post).getComponentProductOfferingPrices();
		        				for (ComponentProductOfferingPrice cpop : cpops){
		        					String s = cpop.getPriceType().getValue();
		        					if (s.equals(business.$tariff_usage)){
		        						CONST price = cpop.getPrice();
		        						String $value = price.getValue();
		        						VariableDeclr vd = x_GET_VARIABLE_DECLR(slat, $value);
		        						if (vd != null){
		        							if (vd instanceof Customisable) try{
			        							DomainExpr de = (DomainExpr)((Customisable)vd).getExpr(); // guaranteed
			        							if (constrained_var != null){
				        							return x_DOMAIN_TO_CONSTRAINT_CLAUSE(de, constrained_var);
			        							}else if (de instanceof SimpleDomainExpr){
			        								// 
			        								// TEMP ONLY
			        								// 
			        								ValueExpr ve = ((SimpleDomainExpr)de).getValue();
			        								if (ve instanceof CONST){
			        									$value = ((CONST)ve).getValue();
			        								}
			        								//
			        							}
		        							}catch(Error e){
		        								e.printStackTrace();
		        								throw new IllegalStateException("");
		        							}else{
		        								// just a standard macro
		        								$value = vd.getExpr().toString();
		        							}
		        						}
		        						return new Clause($value);
		        					}
		        				}
		        			}
	        			}
	        		}
	        	}
	        }
		}
		return null;
	}
	
	private static VariableDeclr x_GET_VARIABLE_DECLR(SLATemplate slat, String name){
		VariableDeclr[] vds = slat.getVariableDeclrs();
		for (VariableDeclr vd : vds){
			String var = vd.getVar().getValue();
			if (var.equals(name)) return (Customisable)vd;
		}
		return null;
	}
	
	private static Clause x_DOMAIN_TO_CONSTRAINT_CLAUSE(DomainExpr de, Var constrained_var) throws Error{
		if (de instanceof CompoundDomainExpr) return null; // NOT SUPPORTED
		if (de instanceof SimpleDomainExpr) return x_SIMPLE_DOMAIN_TO_CONSTRAINT_CLAUSE((SimpleDomainExpr)de, constrained_var);
		return null;
	}
	
	private static Clause x_SIMPLE_DOMAIN_TO_CONSTRAINT_CLAUSE(SimpleDomainExpr sde, Var constrained_var) throws Error{
		String $op = sde.getComparisonOp().getValue();
		PI pi = x_OP_TO_PI($op);
		if (pi != null){
			ValueExpr ve = sde.getValue();
			if (ve instanceof CONST){
				String $value = ((CONST)ve).getValue(); // TEMP ignores UNITS !!!!
				return new Clause(pi, constrained_var, $value);
			}
		}
		return null;
	}
	
	private static PI x_OP_TO_PI(String op){
		if (op.equals(core.$equals)){
			return PI.equal_2;
		}else if (op.equals(core.$not_equals)){
			return PI.not_equal_2;
		}else if (op.equals(core.$less_than)){
			return PI.lt_2;
		}else if (op.equals(core.$less_than_or_equals)){
			return PI.lte_2;
		}else if (op.equals(core.$greater_than)){
			return PI.gt_2;
		}else if (op.equals(core.$greater_than_or_equals)){
			return PI.gte_2;
		}
		return null;
	}

}
