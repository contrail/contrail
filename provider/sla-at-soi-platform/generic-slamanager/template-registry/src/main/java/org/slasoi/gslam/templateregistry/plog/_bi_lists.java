/* 
SVN FILE: $Id: _bi_lists.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/_bi_lists.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.ArrayList;
//import java.util.Collections;
import java.util.List;

class _bi_lists {

	static Clause.Iterator concat(final Clause c){
		return new Clause.Iterator(){
			Term _1 = null;
			Term _2 = null;
			Term _3 = null;
			List<Term> x = null;
			List<Term> y = null;
			List<Term> z = null;
			boolean done = false;
			public Clause next() throws Error{
				if (done) return null;
				if (_1 == null){
					_1 = c.arg(0);
					_2 = c.arg(1);
					_3 = c.arg(2);
					if (!_1.isType(PI.var_0)){
						x = Term.fromPrologList(_1.asClause());
					}
					if (!_2.isType(PI.var_0)){
						y = Term.fromPrologList(_2.asClause());
					}
					if (!_3.isType(PI.var_0)){
						z = Term.fromPrologList(_3.asClause());
					}
					if (z == null){
						if (x == null && y == null){
							throw Error.type(PI.list_0, _3);
						}else if (x == null){
							throw Error.type(PI.list_0, _1);
						}else if (y == null){
							throw Error.type(PI.list_0, _2);
						}
					}else{
						x = new ArrayList<Term>();
						y = new ArrayList<Term>();
						y.add(null);
						y.addAll(z);
					}
				}
				Clause res = new Clause(PI.conc_3);
				if (z == null){
					res.add(_1);
					res.add(_2);
					x.addAll(y);
					res.add(Term.toPrologList(x));
					done = true;
					return res;
				}else if (!y.isEmpty()){
					Term t = y.remove(0);
					if (t != null) x.add(t);
					res.add(Term.toPrologList(x));
					res.add(Term.toPrologList(y));
					res.add(_3);
					return res;
				}
				return null;
			}
		};
	}

	static Clause.Iterator member(final Clause c){
		return new Clause.Iterator(){
			List<Term> terms = null;
			public Clause next() throws Error{
				if (terms == null){
					terms = Term.fromPrologList(c.arg(1).asClause());
					/*
					terms = Collections.synchronizedList(new ArrayList<Term>());
					List<Term> list = Term.fromPrologList(c.arg(1).asClause());
					Term a = c.arg(0);
					boolean var = a.isType(PI.var_0);
					for (Term t : list){
						if (var || t.isType(PI.var_0) || t.equals(a)) terms.add(t);
					}
					*/
				}
				if (!terms.isEmpty()){
					Clause res = new Clause(PI.member_2);
					res.add(terms.remove(0));
					res.add(c.arg(1));
					return res;
				}
				return null;
			}
		};
	}
	
}
