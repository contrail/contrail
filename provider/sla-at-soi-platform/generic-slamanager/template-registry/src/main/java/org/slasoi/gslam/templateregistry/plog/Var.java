/* 
SVN FILE: $Id: Var.java 1944 2011-05-31 14:36:04Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1944 $
@lastrevision   $Date: 2011-05-31 16:36:04 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/Var.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Class encapsulating Prolog variables.
 * @author Keven T. Kearney
 */
public final class Var extends Term {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Term binding = null;
	private static int anon_count = 1;
	private Integer hashcode = null;
	
	static Var autogen(){
		try{
			return new Var("_" + (anon_count++));
		}catch(Error e){
			throw new IllegalStateException("This should never happen !");
		}
	}
	
	public int hashCode(){
		return hashcode != null? hashcode: super.hashCode();
	}
	
	/**
	 * Construct a variable with the specified name.
	 * @throws Error - <code>syntax_error(Msg)</code> if
	 * <code>name</code> is an invalid variable name.
	 */
	public Var(String name) throws Error{
		super(name, PI.var_0);
		if (!_m_parser.isVar(name)){
			throw Error.syntax("Illegal variable name \"" + name + "\"");
		}
		if (isAnonymous()) hashcode = anon_count++;
	}
	
	public boolean isType(PI type){
		if (binding != null) return binding.isType(type);
		return type == PI.var_0;
	}
	
	/**
	 * @return <code>true</code> if this variable is anonymous
	 * (i.e. has a name equal to '_').
	 */
	public boolean isAnonymous(){
		return name.equals("_");
	}
	
	/**
	 * Retrieve the binding of this variable - i.e.:
	 * <ul>
	 *  <li> if this variable, V, is unbound, then V is returned,
	 *  <li> if this variable, V, is bound to another variable, W, 
	 *  then the binding of W is returned,   
	 *  <li> if this variable is bound to a {@link Clause Clause}, C,
	 * then C is returned.
	 * </ul>
	 */
	public Term binding(){
		if (binding == null) return this;
		else if (binding instanceof Clause) return (Clause)binding;
		else return ((Var)binding).binding();
	} 
	
	boolean bindto(Term x){
		if (x == null || binding != null || x.entails(this)) return false;
		binding = x;
		return true;
	}
	
	void unbind(){
		binding = null;
	}
	
	boolean entails(Term x){
		Term d = binding();
		boolean b = (x == this || //SELF REFERENCE
			(d != null && d != this && d.entails(x)) );
		return b;
	}
	
	public int compareTo(Term o){
		if (o instanceof Var){
			if (name.equals("_") && o.name.equals("_")){
				int a = hashCode();
				int b = o.hashCode();
				return a>b? 1: a<b? -1: 0;
			}
			return name.compareTo(o.name);
		}else if (o instanceof Clause){
			return -1;
		}
		throw new ClassCastException("it.eng.iso.lang.Term required"); 
	}
	
	final String uniformSyntax(boolean quoted, boolean numbervars, _m_parser parser) throws Error{
		// TODO numbervars ?'
		Term b = binding();
		if (b instanceof Var) return b.name;
		else return b.uniformSyntax(quoted, numbervars, parser);
	}
	
	public boolean equals(Object obj){
		if (this == obj) return true;
		if (obj instanceof Var) return this.compareTo((Var)obj) == 0;
		return false;
	}
	
	
	public static class Accessor {
		
		private Map<String, Var> vars = Collections.synchronizedMap(new Hashtable<String, Var>());
		private Map<String, Integer> count = Collections.synchronizedMap(new Hashtable<String, Integer>());
		
		/**
		 * Create a VarAccessor for the compound Prolog term <code>c</code>.
		 */
		public Accessor(Clause c){
			build(c);
		}
		
		private void build(Clause c){
			Term[] args = c.args();
			for (Term t : args){
				if (t instanceof Var && !((Var)t).isAnonymous()){
					int count = count((Var)t); 
					put((Var)t, count+1);
				}else if (t instanceof Clause){
					build((Clause)t);
				}
			}
		}
		
		/**
		 * Retrieve the variable with name <code>var_name</code>.
		 */
		public Var var(String var_name){
			return vars.get(var_name);
		}
		
		void put(Var v, int i){
			vars.put(v.name, v);
			count.put(v.name, i);
		}
		
		int count(Var v){
			Integer i = count.get(v.name);
			return i != null? i: 0; 
		}
		
		/**
		 * Retrieve a list of embedded variables.
		 */
		public List<Var> variables(){
			List<Var> list = Collections.synchronizedList(new ArrayList<Var>());
			Var[] vs = vars.values().toArray(new Var[0]);
			for (Var v : vs) list.add(v);
			return list;
		}
		
		/**
		 * Retrieve a list of embedded singleton variables.
		 */
		public List<Var> singletons(){
			List<Var> list = new ArrayList<Var>();
			Iterator<Var> it = vars.values().iterator();
			while (it.hasNext()){
				Var v = it.next();
				if (count(v) == 1) list.add(v);
			}
			return list;
		} 

	}

}
