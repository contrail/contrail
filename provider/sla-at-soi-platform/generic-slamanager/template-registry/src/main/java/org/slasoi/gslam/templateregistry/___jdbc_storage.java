/* 
SVN FILE: $Id: ___jdbc_storage.java 1713 2011-05-13 16:29:51Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1713 $
@lastrevision   $Date: 2011-05-13 18:29:51 +0200 (pet, 13 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/___jdbc_storage.java $

*/

package org.slasoi.gslam.templateregistry;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.rowset.serial.SerialBlob;

import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.templateregistry.plog.Clause;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.SLATemplate;

/*
 * see "_IN_MEMORY_STORAGE" for a (non-persistent) reference implementation of _STORAGE
 */
class ___jdbc_storage implements ___storage{
	
	boolean test = false;
    private Connection DB_CONNECTION = null;
    
    ___jdbc_storage(SLAManagerContext context, boolean test, boolean delete_previous) throws Exception{
    	this.test = test;
        DB_CONNECTION = x_DATASOURCE(context);
        x_CREATE_TABLES(test, delete_previous);
    }
    
    // Connections ...
	
	public void p_CHECK_CONNECTION(){
		// TODO
	}
	
	public void p_DISCONNECT() throws Exception{
        DB_CONNECTION.close();
	}
	
	// Predicates ...

    public Clause[] p_GET_PREDICATES(String moduleId, String predicateSig) throws Exception{
        StringBuffer buf = new StringBuffer();
    	buf.append("SELECT ");
    	buf.append($predicate_list);
    	buf.append(" FROM ");
    	buf.append($predicates_table);
    	buf.append(" WHERE ");
    	buf.append($moduleId);
    	buf.append("=\"");
    	buf.append(moduleId);
    	buf.append("\" AND ");
    	buf.append($predicateSig);
    	buf.append("=\"");
    	buf.append(predicateSig);
    	buf.append("\"");
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        if (rs.first()){
            InputStream is = rs.getBlob(1).getBinaryStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            return (Clause[])ois.readObject();
        }
        return new Clause[0];
    }

    public void p_ADD_PREDICATE(Clause predicate, String moduleId, String predicateSig, boolean append) throws Exception{
    	Clause[] preds = p_GET_PREDICATES(moduleId, predicateSig);
        List<Clause> list = new ArrayList<Clause>();
        if (!append) list.add(predicate);
        for (Clause o : preds) list.add(o);
        if (append) list.add(predicate);
        StringBuffer buf = new StringBuffer();
        if (preds.length==0){
        	buf.append("INSERT INTO ");
        	buf.append($predicates_table);
        	buf.append(" (");
        	buf.append($moduleId);
        	buf.append(",");
        	buf.append($predicateSig);
        	buf.append(",");
        	buf.append($predicate_list);
        	buf.append(") VALUES(\"");
        	buf.append(moduleId);
        	buf.append("\",\"");
        	buf.append(predicateSig);
        	buf.append("\",?)");
        }else{
        	buf.append("UPDATE ");
        	buf.append($predicates_table);
        	buf.append(" SET ");
        	buf.append($predicate_list);
        	buf.append("=? WHERE ");
        	buf.append($moduleId);
        	buf.append("=\"");
        	buf.append(moduleId);
        	buf.append("\" AND ");
        	buf.append($predicateSig);
        	buf.append("=\"");
        	buf.append(predicateSig);
        	buf.append("\"");
        }
        PreparedStatement ps = DB_CONNECTION.prepareStatement(buf.toString());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos); 
        oos.writeObject(list.toArray(new Clause[0]));
        oos.close();
        SerialBlob sblob = new SerialBlob(baos.toByteArray());
        ps.setBlob(1,sblob);
        ps.execute();
    }
    
    public boolean p_REMOVE_PREDICATE(Clause clause, String moduleId, String predicateSig) throws Exception{
    	Clause[] preds = p_GET_PREDICATES(moduleId, predicateSig);
    	if (preds != null && preds.length > 0){
            List<Clause> list = new ArrayList<Clause>();
            for (Clause c : preds){
            	if (!c.equals(clause)) list.add(c);
            }
            if (list.size() < preds.length){
                StringBuffer buf = new StringBuffer();
            	buf.append("UPDATE ");
            	buf.append($predicates_table);
            	buf.append(" SET ");
            	buf.append($predicate_list);
            	buf.append("=? WHERE ");
            	buf.append($moduleId);
            	buf.append("=\"");
            	buf.append(moduleId);
            	buf.append("\" AND ");
            	buf.append($predicateSig);
            	buf.append("=\"");
            	buf.append(predicateSig);
            	buf.append("\"");
                PreparedStatement ps = DB_CONNECTION.prepareStatement(buf.toString());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos); 
                oos.writeObject(list.toArray(new Clause[0]));
                oos.close();
                SerialBlob sblob = new SerialBlob(baos.toByteArray());
                ps.setBlob(1,sblob);
                ps.execute();
                return true;
            }
    	}
    	return false;
    }

    public String[] p_GET_PREDICATE_SIGS(String moduleId) throws Exception{
        StringBuffer buf = new StringBuffer();
        buf.append("SELECT ");
    	buf.append($predicateSig);
    	buf.append(" FROM ");
    	buf.append($predicates_table);
    	buf.append(" WHERE ");
    	buf.append($moduleId);
    	buf.append("=\"");
    	buf.append(moduleId);
    	buf.append("\"");
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        final List<String> list = new ArrayList<String>();
        if (rs.first()) do{
            String s = rs.getString(1);
            if (s.startsWith("\"")) s = s.substring(1);
            if (s.endsWith("\"")) s = s.substring(0,s.length()-2);
            list.add(s);
        }while (rs.next());
        return list.toArray(new String[0]);
    }
    
    // SLATemplates ...
    
    public String[] p_GET_ALL_SLAT_MODULE_IDS() throws Exception{
        List<String> list = new ArrayList<String>();
        StringBuffer buf = new StringBuffer();
        buf.append("SELECT ");
        buf.append($moduleId);
        buf.append(" FROM ");
        buf.append($templates_table);
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        if (rs.first()) do{
            list.add(rs.getString(1));
        }while (rs.next());
        return list.toArray(new String[0]);
    }
    
    public UUID[] p_GET_ALL_SLAT_UUIDS() throws Exception{
    	List<UUID> uuids = new ArrayList<UUID>();
        StringBuffer buf = new StringBuffer();
        buf.append("SELECT ");
    	buf.append($uuid);
    	buf.append(" FROM ");
    	buf.append($templates_table);
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        if (rs.first()){
        	do{
        		String s = rs.getString(1);
        		uuids.add(new UUID(s));
        	}while (rs.next());
        }
    	return uuids.toArray(new UUID[0]);
    }
	
	public String p_GET_SLAT_MODULE_ID(UUID templateUuid) throws Exception{
    	StringBuffer buf = new StringBuffer();
    	buf.append("SELECT ");
    	buf.append($moduleId);
    	buf.append(" FROM ");
    	buf.append($templates_table);
    	buf.append(" WHERE ");
    	buf.append($uuid);
    	buf.append("=\"");
    	buf.append(templateUuid.getValue());
    	buf.append("\"");
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        if (rs.first()) return rs.getString(1);
        return null;
	}
	
	public UUID p_GET_SLAT_UUID(String moduleId) throws Exception{
    	StringBuffer buf = new StringBuffer();
    	buf.append("SELECT ");
    	buf.append($uuid);
    	buf.append(" FROM ");
    	buf.append($templates_table);
    	buf.append(" WHERE ");
    	buf.append($moduleId);
    	buf.append("=\"");
    	buf.append(moduleId);
    	buf.append("\"");
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        if (rs.first()){
        	String s = rs.getString(1);
        	return new UUID(s);
        }
        return null;
	}

    public SLATemplate p_GET_SLAT(UUID templateUuid) throws Exception{
        SLATemplate slat = null;
        StringBuffer buf = new StringBuffer();
        buf.append("SELECT ");
    	buf.append($stored_template);
    	buf.append(" FROM ");
    	buf.append($templates_table);
    	buf.append(" WHERE ");
    	buf.append($uuid);
    	buf.append("=\"");
    	buf.append(templateUuid.getValue());
    	buf.append("\"");
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        if (rs.first()){
            Blob blob = rs.getBlob(1);
            InputStream is = blob.getBinaryStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            slat = (SLATemplate)ois.readObject();
        }
        return slat;
    }
    
    public void p_ADD_SLAT(SLATemplate slat, String moduleId) throws Exception{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos); 
        oos.writeObject(slat);
        oos.close();
        SerialBlob sblob = new SerialBlob(baos.toByteArray());
        StringBuffer buf = new StringBuffer();
        buf.append("INSERT INTO ");
    	buf.append($templates_table);
    	buf.append("(");
    	buf.append($moduleId);
    	buf.append(",");
    	buf.append($uuid);
    	buf.append(",");
    	buf.append($stored_template);
    	buf.append(") VALUES(?,?,?)");
        PreparedStatement ps = DB_CONNECTION.prepareStatement(buf.toString());
        ps.setString(1,moduleId);
        ps.setString(2,slat.getUuid().getValue());
        ps.setBlob(3,sblob);
        ps.execute();
    }
    
    // Specifications ...
	
	public boolean p_EXISTS_SPEC(String name) throws Exception{
    	StringBuffer buf = new StringBuffer();
    	buf.append("SELECT ");
    	buf.append($name);
    	buf.append(" FROM ");
    	buf.append($ispecs_table);
    	buf.append(" WHERE ");
    	buf.append($name);
    	buf.append("=\"");
    	buf.append(name);
    	buf.append("\"");
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        return rs.first();
	}
    
    public Specification p_GET_SPEC(String name) throws Exception{
    	Specification ispec = null;
        StringBuffer buf = new StringBuffer();
        buf.append("SELECT ");
    	buf.append($stored_ispec);
    	buf.append(" FROM ");
    	buf.append($ispecs_table);
    	buf.append(" WHERE ");
    	buf.append($name);
    	buf.append("=\"");
    	buf.append(name);
    	buf.append("\"");
        Statement stmt = DB_CONNECTION.createStatement();
        ResultSet rs = stmt.executeQuery(buf.toString());
        if (rs.first()){
            Blob blob = rs.getBlob(1);
            InputStream is = blob.getBinaryStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            ispec = (Specification)ois.readObject();
        }
        return ispec;
    }
    
    public void p_ADD_SPEC(Specification spec) throws Exception{
    	String $spec_name = spec.getName();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos); 
        oos.writeObject(spec);
        oos.close();
        SerialBlob sblob = new SerialBlob(baos.toByteArray());
        StringBuffer buf = new StringBuffer();
    	if (p_EXISTS_SPEC($spec_name)){
        	buf.append("UPDATE ");
        	buf.append($ispecs_table);
        	buf.append(" SET ");
        	buf.append($stored_ispec);
        	buf.append("=? WHERE ");
        	buf.append($name);
        	buf.append("=\"");
        	buf.append($spec_name);
        	buf.append("\"");
    	}else{
            buf.append("INSERT INTO ");
        	buf.append($ispecs_table);
        	buf.append("(");
        	buf.append($name);
        	buf.append(",");
        	buf.append($stored_ispec);
        	buf.append(") VALUES(\"");
        	buf.append($spec_name);
        	buf.append("\",?)");
    	}
        PreparedStatement ps = DB_CONNECTION.prepareStatement(buf.toString());
        ps.setBlob(1,sblob);
        ps.execute();
    }
    
    /*-------------------------------------------------------------------------------*/
    // __CREATE_TABLES
    /*-------------------------------------------------------------------------------*/

	private static final String $STRING = "CHAR(255)";
	private static final String $BLOB = "BLOB";
	private static final String $TEXT = "TEXT";

	private static final String $PREDICATES = "predicates";
	private static final String $TEMPLATES = "templates";
	private static final String $ISPECS = "ispecs";

    private String $slatreg_ref = "slatreg_ref";
    //private String $db_ref_table = "db_ref_table";
    //private String $db_name = "db_name";
    //private String $slam_id = "slam_id";
    
	private String $predicates_table = null;
	private String $templates_table = null;
	private String $ispecs_table = null;
    
	private static final String $moduleId = "moduleId";
	private static final String $uuid = "uuid";
	private static final String $name = "name";
	private static final String $predicateSig = "predicateSig";
	private static final String $predicate_list = "predicate_list";
	private static final String $stored_template = "stored_template";
	private static final String $stored_ispec = "stored_ispec";
    
    private void x_CREATE_TABLES(boolean test, boolean delete_previous) throws java.lang.Exception{
    	try{
            $predicates_table = $PREDICATES;
            $templates_table = $TEMPLATES;
            $ispecs_table = $ISPECS;
            if (test){
                String $test = "X";
                $predicates_table += $test;
                $templates_table += $test;
                $ispecs_table += $test;
                if (delete_previous){
                    Statement DELETE_PREVIOUS_TEST_TABLES = DB_CONNECTION.createStatement();
                    DELETE_PREVIOUS_TEST_TABLES.execute("DROP TABLE IF EXISTS "+$predicates_table);
                    DELETE_PREVIOUS_TEST_TABLES.execute("DROP TABLE IF EXISTS "+$templates_table);
                    DELETE_PREVIOUS_TEST_TABLES.execute("DROP TABLE IF EXISTS "+$ispecs_table);
                }
            }
         // OBJECTS TABLE
            StringBuffer buf = new StringBuffer();
            buf.append("CREATE TABLE IF NOT EXISTS ");
            buf.append($predicates_table);
            buf.append("(");
            buf.append($moduleId);
            buf.append(" ");
            buf.append($STRING);
            buf.append(", ");
            buf.append($predicateSig);
            buf.append(" ");
            buf.append($TEXT);
            buf.append(", ");
            buf.append($predicate_list);
            buf.append(" ");
            buf.append($BLOB);
            buf.append(")");
            Statement s = DB_CONNECTION.createStatement();
            s.execute(buf.toString());
         // TEMPLATES TABLE
            buf = new StringBuffer();
            buf.append("CREATE TABLE IF NOT EXISTS ");
            buf.append($templates_table);
            buf.append("(");
            buf.append($moduleId);
            buf.append(" ");
            buf.append($STRING);
            buf.append(", ");
            buf.append($uuid);
            buf.append(" ");
            buf.append($TEXT);
            buf.append(", ");
            buf.append($stored_template);
            buf.append(" ");
            buf.append($BLOB);
            buf.append(")");
            s = DB_CONNECTION.createStatement();
            s.execute(buf.toString());
         // ISPECS TABLE
            buf = new StringBuffer();
            buf.append("CREATE TABLE IF NOT EXISTS ");
            buf.append($ispecs_table);
            buf.append("(");
            buf.append($name);
            buf.append(" ");
            buf.append($TEXT);
            buf.append(", ");
            buf.append($stored_ispec);
            buf.append(" ");
            buf.append($BLOB);
            buf.append(")");
            s = DB_CONNECTION.createStatement();
            s.execute(buf.toString());
    	}catch(Exception e){
    	    e.printStackTrace();
    		throw e;
    	}
    }
    
    private static final String AUTO_RECONNECT = "?autoReconnect=true"; 
    
    private Connection x_DATASOURCE(SLAManagerContext context) throws Exception{
        File file = null;
        StringBuffer buf = new StringBuffer();
        try{
            buf.append(System.getenv("SLASOI_HOME"));
            buf.append(File.separator);
            buf.append("generic-slamanager");
            buf.append(File.separator);
            buf.append("template-registry");
            buf.append(File.separator);
            buf.append("db.properties");
            file = new File(buf.toString());
        }catch(Exception e){
            // DO NOTHING ...
        }
        if (file == null || !file.exists()){
            buf = new StringBuffer(System.getProperty("user.dir"));
            buf.append(File.separator);
            buf.append("dev_local_config");
            buf.append(File.separator);
            buf.append("db.properties");
            file = new File(buf.toString());
        }
        if (file.exists()){
            Properties p = new Properties();
            p.load(new FileInputStream(file));
            Class.forName(p.getProperty("db.driver")).newInstance();
            String url = p.getProperty("db.url");

		boolean auto_reconnect = url.endsWith(AUTO_RECONNECT);
            // WARNING: ONLY "autoReconnect" OPTION IS PERMITTED !!!
            // OTHER OPTIONS APPENDED TO URL WILL CREATE PROBLEMS
            //
            if (auto_reconnect){
                url = url.substring(0,url.length() - AUTO_RECONNECT.length());
            }            

		if (!url.endsWith("/")) url += "/"; 
            String user = p.getProperty("db.user");
            String pword = p.getProperty("db.pwd");
            // 1. connect to the server ..
            Connection server = DriverManager.getConnection(url, user, pword);
            // 2. if needed, create the DB_NAME-to-SLAM_ID mapping database ..
            Statement server_stmt = server.createStatement();
            server_stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS "+$slatreg_ref);
            // 3. get a connection to the DB_NAME-to-SLAM_ID mapping database .. 
            Connection db = DriverManager.getConnection(url + $slatreg_ref, user, pword);
            // 4. if needed, create the mapping table
            // x_CREATE_DB_TO_SLAM_MAPPING_TABLE(db);
            // 5. query the database name for this SLAM (creates a new name if not registered ..)
            String db_name = x_GET_DB_NAME(context.getSLAManagerID(), db);
            server_stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS "+db_name);
            // 6. finally, get a connection to the DB for this SLAM ...
		String $url = url + db_name + (auto_reconnect? AUTO_RECONNECT: "");
            db = DriverManager.getConnection($url, user, pword);
            System.out.println("connected to DB : "+url + db_name);
            return db;
        }else{
            throw new FileNotFoundException("Unable to find database properties file");
        }
    }
    
    private String x_GET_DB_NAME(String slam_id, Connection con) throws Exception{
        try{
            StringBuffer buf = new StringBuffer();
            /*
            buf.append("SELECT ");
            buf.append($db_name);
            buf.append(" FROM ");
            buf.append($db_ref_table);
            buf.append(" WHERE ");
            buf.append($slam_id);
            buf.append("=\"");
            buf.append(slam_id);
            buf.append("\"");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(buf.toString());
            if (rs.first()) return rs.getString(1);
            buf = new StringBuffer();
            */
            char[] cs = slam_id.toCharArray();
            for (char c : cs){
                if (x_LEGAL_CHAR(c)) buf.append(c);
                else buf.append((int)c);
            }
            String db_name = buf.toString().toLowerCase();
            /*
            buf = new StringBuffer();
            buf.append("INSERT INTO ");
            buf.append($db_ref_table);
            buf.append("(");
            buf.append($db_name);
            buf.append(",");
            buf.append($slam_id);
            buf.append(") VALUES(\"");
            buf.append(db_name);
            buf.append("\",\"");
            buf.append(slam_id);
            buf.append("\")");
            con.prepareStatement(buf.toString()).execute();
            */
            return db_name;
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }
    }
    
    private static final String $legal_chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final char[] legal_chars = $legal_chars.toCharArray();
    
    private boolean x_LEGAL_CHAR(char c){
        for (char z : legal_chars) if (z == c) return true;
        return false;
    }
    /*
    private void x_CREATE_DB_TO_SLAM_MAPPING_TABLE(Connection con) throws Exception{
        try{
            StringBuffer buf = new StringBuffer();
            buf.append("CREATE TABLE IF NOT EXISTS ");
            buf.append($db_ref_table);
            buf.append("(");
            buf.append($db_name);
            buf.append(" ");
            buf.append($STRING);
            buf.append(", ");
            buf.append($slam_id);
            buf.append(" ");
            buf.append($TEXT);
            buf.append(")");
            Statement s = con.createStatement();
            s.execute(buf.toString());
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }
    }
    */

}
