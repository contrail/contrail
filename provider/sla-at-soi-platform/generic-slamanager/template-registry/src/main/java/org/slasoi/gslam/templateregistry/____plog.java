/* 
SVN FILE: $Id: ____plog.java 1946 2011-05-31 14:59:34Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1946 $
@lastrevision   $Date: 2011-05-31 16:59:34 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/____plog.java $

*/

package org.slasoi.gslam.templateregistry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet.Result;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.ResourceType;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.tools.MetaModel;
import org.slasoi.slamodel.sla.tools.Validator.Report;
import org.slasoi.slamodel.sla.tools.Validator.Warning;

import org.slasoi.gslam.templateregistry.plog.Clause;
import org.slasoi.gslam.templateregistry.plog.Engine;
import org.slasoi.gslam.templateregistry.plog.Session;
import org.slasoi.gslam.templateregistry.plog.gui.ConsoleApp;

class ____plog extends Engine{
	
	private ___storage storage = null;
	// private MetaModel metamodel = null;
	
	static String[] _code = new String[]{
		"slat(M) :- current_module(M,slat)."
	};
	
	____plog(___storage storage, MetaModel metamodel){
		this.storage = storage;
		// this.metamodel = metamodel;
		try{
			for (String s : _code) consult("user",parse(s));
			String[] modules = storage.p_GET_ALL_SLAT_MODULE_IDS();
			for (String s : modules) this.addModule(new ____plog_mod(storage, s, ____plog_common.$slat));
		}catch(Exception e){
			throw new IllegalStateException(e);
		}
	}
    
    public void p_OPEN_CONSOLE(){
    	new ConsoleApp(this).setVisible(true);
    }
	
	String p_REGISTER(Report report, Metadata metadata) throws SLATemplateRegistry.Exception{
		____plog_mod mod = null;
		try{
			for (Map<String,Specification> map : report.ispecs.values()){
				for (Specification spec : map.values()){
					String $name = spec.getName();
					if (!storage.p_EXISTS_SPEC($name)){
						storage.p_ADD_SPEC(spec);
					}
				}
			}
			mod = x_STORE_SLAT(report.slat, metadata, report);
			return mod.name();
		}catch(Exception e){
			e.printStackTrace();
			if (mod != null) try{
				this.removeModule(mod);
			}catch(org.slasoi.gslam.templateregistry.plog.Error z){
				z.printStackTrace();
			}
			throw new SLATemplateRegistry.Exception(e);
		}
	}
	
	UUID[] p_METADATA_QUERY(ConstraintExpr metadata_query) throws SLATemplateRegistry.Exception{
		List<UUID> uuids = new ArrayList<UUID>();
		try{
			StringBuffer buf = new StringBuffer();
			buf.append("?- current_module(M,");
			buf.append(____plog_common.$slat);
			buf.append("),");
			buf.append(____plog_metadata_query.p_CONSTRAINT_TO_PLOG_STRING(metadata_query,"M", new int[]{1}));
			buf.append(".");
			Session ss = this.session(buf.toString());
			while (ss.solveNext()){
				String mod = ss.var("M").binding().name();
				// System.out.println("M = "+mod);
				UUID uuid = storage.p_GET_SLAT_UUID(mod);
				if (uuid != null && !uuids.contains(uuid)) uuids.add(uuid); // can uuid be null ??
			}
		}catch(Exception e){
			throw new SLATemplateRegistry.Exception(e);
		}
		return uuids.toArray(new UUID[0]);
	}
	
	private static boolean DEBUG = false;
	static final String MOD____ID = "MOD____ID";
	
	// UUID[] templateIds = result of metadata-based query
	void p_TEMPLATE_QUERY(SLATemplate query, UUID[] templateIds, ResultSet resultset, Report report){
		try{
            if (DEBUG) System.out.println("p_TEMPLATE_QUERY .. templateId count = " + templateIds.length);
		    // BUILD THE FUNCTIONAL-PROPERTIES QUERY 
		    // = operates over interface names (according to agreement role of the party offering the interface) 
			String $functional_query_suffix = null;
			if (query != null && report != null){
			    StringBuffer buf = new StringBuffer();
			    // INTERFACE SPECS
		        for (String id : report.ispecs.keySet()){
		            Map<String,Specification> map = report.ispecs.get(id);
		            if (map != null) for (String s : map.keySet()){
		                STND role = report.ispec_providers.get(s);
		                if (buf == null) buf = new StringBuffer();
                        buf.append(",M:");
                        buf.append(____plog_common.p_IFACE(s, role, false));
		            }
		        }
		        // RESOURCE TYPES
		        for (String id : report.res_types.keySet()){
		            Map<String,ResourceType> map = report.res_types.get(id);
		            if (map != null) for (String s : map.keySet()){
		                STND role = report.res_type_providers.get(s);
                        if (buf == null) buf = new StringBuffer();
                        buf.append(",M:");
                        buf.append(____plog_common.p_IFACE(s, role, true));
		            }
		        }
                if (buf != null){
                    $functional_query_suffix = buf.toString();
                    if (DEBUG) System.out.println("FQS = "+buf.toString());
                }
			}else if (DEBUG) System.out.println("Null query and/or report (defaulting to metadata query)");
            Map<String,UUID> modules = new HashMap<String,UUID>();
            for (UUID uuid : templateIds){
                try{
                    String $mod = storage.p_GET_SLAT_MODULE_ID(uuid);
                    if ($functional_query_suffix != null){
                        StringBuffer buf = new StringBuffer("?- =(M,");
                        buf.append($mod);
                        buf.append(")");
                        buf.append($functional_query_suffix);
                        buf.append(".");
                        Session ss = session(buf.toString());
                        if (DEBUG) System.out.println("SOLVING: "+buf.toString());
                        if (ss.solveNext()){
                            modules.put($mod,uuid);
                        }
                    }else{
                        modules.put($mod,uuid); // if no ifaces are specified add all modules
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    resultset.errors.add(e);
                }
            }
            if (DEBUG) System.out.println("functional match: modules count = " + modules.size());
			if (modules.isEmpty()){
				resultset.warnings.add(new Warning("No templates found with matching interfaces",query));
			}else if (query != null){
	            if (DEBUG) System.out.println("performing non-functional match (not yet implemented)");
			    String $query = ____plog_query_parser.p_PARSE(this, query);
			    
			    
			    if (DEBUG) System.out.println("Generated Query:\n"+$query);
			    
				for (String $mod : modules.keySet()){ // use ? M is <$mod>, <$query> ..
				    String $mod_query = $query.replaceAll(MOD____ID, $mod);
				    
					UUID uuid = modules.get($mod);
					if (DEBUG) System.out.println("QUERYING SLAT WITH UUID = " + uuid.getValue());
					float confidence = 0.0f;
						
					StringBuffer Q = new StringBuffer();
					Q.append("?- ");
					Q.append($mod_query);
					Q.append(".");
					if (DEBUG) System.out.println("QUERY = \n"+Q.toString());
					try{
						Session session = this.session(Q.toString());
						if (session.solveNext()){
							if (DEBUG) System.out.println(" >> SUCCESS");
							Result r = new Result(uuid, confidence);
							resultset.results.add(r);
						}else{
							if (DEBUG) System.out.println(" >> FAIL");
						}
					}catch(Exception x){
						x.printStackTrace();
					}
				}
			}else{
			    // defaults to standard metadata query ..
			    // = just add all modules for templateIds 
				for (UUID uuid : modules.values()){
					Result r = new Result(uuid, 1.0f);
					resultset.results.add(r);
				}
			}
		}catch(Exception e){
			resultset.errors.add(e);
			return;
		}
	}
	
	// templateId = guaranteed
	Metadata p_GET_METADATA(String moduleId) throws SLATemplateRegistry.Exception{
		Metadata m = new Metadata();
		for (STND key : Metadata.__stnds){
			String value = p_GET_METADATA_PROPERTY(moduleId, key);
			if (value != null) m.setPropertyValue(key, value);
		}
		return m;
	}

	// templateId & key = guaranteed
	String p_GET_METADATA_PROPERTY(String moduleId, STND key) throws SLATemplateRegistry.Exception{
		try{
	        StringBuffer buf = new StringBuffer();
	        buf.append("?- ");
	    	buf.append(moduleId);
	    	buf.append(":'");
	    	buf.append(key.getValue());
	    	buf.append("'(V).");
	    	Session ss = session(buf.toString());
	    	if (ss.solveNext()){
	    		return ss.var("V").binding().name();
	    	}
        }catch(java.lang.Exception e){
        	throw new SLATemplateRegistry.Exception(e);
        }
        return null;
	}
	
	// templateId & key & value = guaranteed
	// value may be NULL !!!
	void p_SET_METADATA_PROPERTY(String moduleId, STND key, String value) throws SLATemplateRegistry.Exception{
    	String v = p_GET_METADATA_PROPERTY(moduleId, key);
		if ((v != null && !v.equals(value)) || (v== null || value != null)) try{
	    	if (v != null){
				String s = ____plog_common.p_PARSE_METADATA_PROPERTY(key, v);
				Clause c = parse(s+".");
				storage.p_REMOVE_PREDICATE(c, moduleId, ____plog_common.x_KEY(c.pi()));
			}
			if (value != null){
				String s = ____plog_common.p_PARSE_METADATA_PROPERTY(key, value);
				Clause c = parse(s+".");
				storage.p_ADD_PREDICATE(c, moduleId, ____plog_common.x_KEY(c.pi()), true);
			}
		}catch(java.lang.Exception e){
        	throw new SLATemplateRegistry.Exception(e);
        }
	}
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SLATS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	private static int counter = 0; 
	
	private ____plog_mod x_STORE_SLAT(SLATemplate slat, Metadata metadata, Report rep) throws Exception{
		String mod_name = ____plog_common.$slat + System.currentTimeMillis()+ (counter++);
		//System.out.println("--SLAT---"+mod_name+"--------------------------------------------");
		____plog_mod mod = new ____plog_mod(storage, mod_name, ____plog_common.$slat);
		this.addModule(mod);
		List<Clause> clauses = ____plog_slat_parser.p_PARSE(this, slat, metadata, rep);
		for (Clause c : clauses){
			consult(mod_name, c);
			//System.out.println(c.toString());
		}
        storage.p_ADD_SLAT(slat, mod_name);
		return mod;
	}

}
