/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/plan/src/test/java/org/slasoi/gslam/commons/plan/DAGTest.java $
 */

/**
 *
 */
package org.slasoi.gslam.commons.plan;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.slasoi.gslam.commons.plan.graph.DirectedAcyclicGraph;
import org.slasoi.gslam.commons.plan.graph.EdgeFoundException;
import org.slasoi.gslam.commons.plan.graph.EdgeNotFoundException;
import org.slasoi.gslam.commons.plan.graph.VertexFoundException;
import org.slasoi.gslam.commons.plan.graph.VertexNotFoundException;

/**
 * @author Beatriz Fuentes
 * 
 */
public class DAGTest {
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(DAGTest.class.getName());

    /**
     * Maximum number of nodes in the DAG.
     */
    private static final int MAX_NODES = 10;

    /**
     * Head vertices of the edges.
     */
    private static final Integer[] EDGES_IN = { 1, 2, 3, 4, 5 };

    /**
     * Tail vertices of the edges.
     */
    private static final Integer[] EDGES_OUT = { 6, 7, 8, 9, 0 };

    /**
     * Method that creates a DAG and plays with it.
     */
    @Test
    public final void testDAG() {
        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        try {
            for (int i = 0; i < MAX_NODES; i++) {
                dag.addVertex(new Integer(i));
            }

            for (int i = 0; i < EDGES_IN.length; i++) {
                dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
            }

            // Each node in EDGES_OUT must have inDegree = 0 and outDegree = 1
            assertEquals(dag.getInDegree(EDGES_OUT[0]), 1);
            assertEquals(dag.getIncomingEdges(EDGES_OUT[0]).size(), 1);
            assertEquals(dag.getOutDegree(EDGES_OUT[0]), 0);
            assertEquals(dag.getOutgoingEdges(EDGES_OUT[0]).size(), 0);

        }
        catch (VertexFoundException e) {
            e.printStackTrace();
        }
        catch (VertexNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (EdgeFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Test that fills a DAG and then tries to insert the same edge twice.
     * 
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     */
    @Test(expected = EdgeFoundException.class)
    public final void edgeFoundException() throws EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");
        try {
            for (int i = 0; i < MAX_NODES; i++) {
                dag.addVertex(new Integer(i));
            }

            for (int i = 0; i < EDGES_IN.length; i++) {
                dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
            }

            // Now, insert another edge again
            dag.addEdge(EDGES_IN[0], EDGES_OUT[0]);

        }
        catch (VertexFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (VertexNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Test that fills a DAG and then tries to remove a not-existing edge.
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     * @throws EdgeNotFoundException
     *             if an edge is not found in the DAG
     */
    @Test(expected = EdgeNotFoundException.class)
    public final void edgeNotFoundException() throws VertexFoundException, VertexNotFoundException, EdgeFoundException,
            EdgeNotFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 0; i < MAX_NODES; i++) {
            dag.addVertex(new Integer(i));
        }

        for (int i = 0; i < EDGES_IN.length; i++) {
            dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
        }

        // Now, try to remove a non-existing edge
        dag.removeEdge(EDGES_OUT[0], EDGES_IN[0]);
    }

    /**
     * Test that fills a DAG and then tries to remove a vertex.
     * 
     * @throws VertexFoundException
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     */
    @Test
    public final void removeVerticesTest() throws VertexFoundException, VertexNotFoundException, EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 0; i < MAX_NODES; i++) {
            dag.addVertex(new Integer(i));
        }

        for (int i = 0; i < EDGES_IN.length; i++) {
            dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
        }

        int verticesBefore = dag.getVertices().size();

        // Now, try to remove a vertex
        dag.removeVertex(new Integer(1));

        int verticesAfter = dag.getVertices().size();

        assertEquals(verticesBefore, verticesAfter + 1);
    }

    /**
     * Test that fills a DAG and then tries to get info about a not-existing vertex.
     * 
     * @throws VertexFoundException
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     */
    @Test(expected = VertexNotFoundException.class)
    public final void getInDegreesExceptionTest() throws VertexFoundException, VertexNotFoundException,
            EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 0; i < MAX_NODES; i++) {
            dag.addVertex(new Integer(i));
        }

        for (int i = 0; i < EDGES_IN.length; i++) {
            dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
        }

        // Now, try to remove a vertex
        dag.getInDegree(new Integer(MAX_NODES + 1));

    }

    /**
     * Test that fills a DAG and then tries to get info about a not-existing vertex.
     * 
     * @throws VertexFoundException
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     */
    @Test(expected = VertexNotFoundException.class)
    public final void getOutDegreesExceptionTest() throws VertexFoundException, VertexNotFoundException,
            EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 0; i < MAX_NODES; i++) {
            dag.addVertex(new Integer(i));
        }

        for (int i = 0; i < EDGES_IN.length; i++) {
            dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
        }

        // Now, try to remove a vertex
        dag.getOutDegree(new Integer(MAX_NODES + 1));

    }

    /**
     * Test that fills a DAG and then tries to get info about a not-existing vertex.
     * 
     * @throws VertexFoundException
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     */
    @Test(expected = VertexNotFoundException.class)
    public final void getIncomingEdgesExceptionTest() throws VertexFoundException, VertexNotFoundException,
            EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 0; i < MAX_NODES; i++) {
            dag.addVertex(new Integer(i));
        }

        for (int i = 0; i < EDGES_IN.length; i++) {
            dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
        }

        // Now, try to remove a vertex
        dag.getIncomingEdges(new Integer(MAX_NODES + 1));

    }

    /**
     * Test that fills a DAG and then tries to get info about a not-existing vertex.
     * 
     * @throws VertexFoundException
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     */
    @Test(expected = VertexNotFoundException.class)
    public final void getOutgoingEdgesExceptionTest() throws VertexFoundException, VertexNotFoundException,
            EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 0; i < MAX_NODES; i++) {
            dag.addVertex(new Integer(i));
        }

        for (int i = 0; i < EDGES_IN.length; i++) {
            dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
        }

        // Now, try to remove a vertex
        dag.getOutgoingEdges(new Integer(MAX_NODES + 1));

    }

    /**
     * Test that fills a DAG and then tries add a cyclical edge (head = tail).
     * 
     * @throws VertexFoundException
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     */
    @Test(expected = IllegalArgumentException.class)
    public final void addEdgeExceptionTest() throws VertexFoundException, VertexNotFoundException, EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 0; i < MAX_NODES; i++) {
            dag.addVertex(new Integer(i));
        }

        for (int i = 0; i < EDGES_IN.length; i++) {
            dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
        }

        // Now, try to add a not-allowed edge
        dag.addEdge(EDGES_IN[0], EDGES_IN[0]);

    }

    /**
     * Test that fills a DAG and then tries to get a non-existing edge.
     * 
     * @throws VertexFoundException
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     */
    @Test(expected = VertexNotFoundException.class)
    public final void getEdgeExceptionTest() throws VertexFoundException, VertexNotFoundException, EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 1; i < MAX_NODES - 1; i++) {
            dag.addVertex(new Integer(i));
        }

        // Now, try to get a not-existing edge
        dag.addEdge(new Integer(0), new Integer(MAX_NODES - 1));

    }

    /**
     * Test that fills a DAG and then test the getPredecessors method.
     * 
     * @throws VertexFoundException
     * 
     * @throws VertexFoundException
     *             if a vertex has already been inserted
     * @throws EdgeFoundException
     *             if an edge has already been inserted
     * @throws VertexNotFoundException
     *             if a vertex is not found in the DAG.
     */
    @Test
    public final void getPredecessorsTest() throws VertexFoundException, VertexNotFoundException, EdgeFoundException {

        DirectedAcyclicGraph<Integer> dag = new DirectedAcyclicGraph<Integer>();

        logger.info("testDAG ");

        for (int i = 0; i < MAX_NODES; i++) {
            dag.addVertex(new Integer(i));
        }

        for (int i = 0; i < EDGES_IN.length; i++) {
            dag.addEdge(EDGES_IN[i], EDGES_OUT[i]);
        }

        // Now, get predecessors
        assertEquals(dag.getPredecessors(EDGES_OUT[0]).size(), 1);

    }

}
