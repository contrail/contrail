/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/plan/src/main/java/org/slasoi/gslam/commons/plan/graph/Graph.java $
 */

/**
 * Package Graph
 */
package org.slasoi.gslam.commons.plan.graph;

import java.util.Collection;
import java.util.Set;

/**
 * Generic Interface for a Graph. A graph is defined as a set of vertices and a set of edges. An edge e=(v1, v2)
 * connects vertex v1 and vertex v2.
 * 
 * @param <V>
 *            vertex
 * @param <E>
 *            edge
 * 
 * @author Beatriz Fuentes
 * 
 */
public interface Graph<V, E> {

    /**
     * Adds the specified vertex to this graph if not already present.
     * 
     * @param v
     *            vertex to be added to this graph.
     * @throws VertexFoundException
     *             if the vertex already exists in the graph
     * 
     */
    void addVertex(V v) throws VertexFoundException;

    /**
     * Creates a new edge in this graph, going from the head vertex to the tail vertex, and returns the created edge.
     * 
     * @param v1
     *            head vertex of the edge.
     * @param v2
     *            tail vertex of the edge.
     * 
     * @return The newly created edge if added to the graph, otherwise <code>null</code>.
     * 
     * @throws VertexNotFoundException
     *             if one or both of the vertices are not found in the graph.
     * @throws EdgeFoundException
     *             if the same edge is already present in the graph
     * 
     */
    E addEdge(V v1, V v2) throws VertexNotFoundException, EdgeFoundException;

    /**
     * Checks if the graph contains an edge going from vertex v1 to vertex v2.
     * 
     * @param v1
     *            head vertex of the edge.
     * @param v2
     *            tail vertex of the edge.
     * @return <tt>true</tt> if the graph contains the specified edge.
     * @throws VertexNotFoundException
     *             if one of the vertices is not contained in the graph
     */
    boolean containsEdge(V v1, V v2) throws VertexNotFoundException;

    /**
     * Checks if the graph contains the specified edge.
     * 
     * @param e
     *            edge to be checked.
     * @return <code>true</code> if the graph contains the specified edge.
     */
    boolean containsEdge(E e);

    /**
     * Checks if the graph contains the specified vertex.
     * 
     * @param v
     *            vertex to be checked.
     * 
     * @return <code>true</code> if the graph contains the specified vertex.
     */
    boolean containsVertex(V v);

    /**
     * Returns a set of the edges connecting vertex v1 to vertex v2 if such vertices exist in this graph. If any of the
     * vertices does not exist throws a VertexNotFoundException. If both vertices exist but no edges found between them,
     * returns an empty set.
     * 
     * 
     * @param v1
     *            head vertex of the edge.
     * @param v2
     *            tail vertex of the edge.
     * @throws VertexNotFoundException
     *             exception when one of the vertices does not exist in the graph
     * 
     * @return the set of edges connecting vertex v1 to vertex v2.
     */
    Set<E> getEdges(V v1, V v2) throws VertexNotFoundException;

    /**
     * Returns a set of all edges touching (going from or to) the specified vertex. If no edges are touching the
     * specified vertex returns an empty set.
     * 
     * @param vertex
     *            the vertex for which a set of touching edges is to be returned.
     * 
     * @return the set of edges touching the specified vertex.
     * 
     * @throws VertexNotFoundException
     *             if vertex is not found in the graph.
     */
    Set<E> getEdges(V vertex) throws VertexNotFoundException;

    /**
     * Returns the vertices contained in this graph.
     * 
     * @return set of the vertices contained in this graph.
     */
    Set<V> getVertices();

    /**
     * Returns the degree of the specified vertex.The degree of a vertex is the number of edges that connect to it.
     * 
     * 
     * @param v
     *            vertex whose degree is going to be calculated
     * @return degree the degree of the specified vertex
     * @throws VertexNotFoundException
     *             if the specified vertex is not in the graph
     * 
     */
    int getDegree(V v) throws VertexNotFoundException;

    /**
     * Removes an edge going from head vertex to tail vertex, and returns the removed edge. If there is no edge between
     * v1 and v2, returns null. or <code>null</code> otherwise.
     * 
     * @param v1
     *            head vertex of the edge.
     * @param v2
     *            tail vertex of the edge.
     * 
     * @return The removed edge, or <code>null</code> if no such edge exists.
     * @throws VertexNotFoundException
     *             if one of the vertices does not exist in the graph
     * @throws EdgeNotFoundException
     *             if such an edge does not exist in the graph
     */
    E removeEdge(V v1, V v2) throws EdgeNotFoundException, VertexNotFoundException;

    /**
     * Removes the specified edge from the graph. Removes the specified edge from this graph if it is present. More
     * formally, removes an edge <code>
     * e2</code> such that <code>e2.equals(e)</code>, if the graph contains such edge. Returns
     * <tt>true</tt> if the graph contained the specified edge. (The graph will not contain the specified edge once the
     * call returns).
     * 
     * <p>
     * If the specified edge is <code>null</code> returns <code>
     * false</code>.
     * </p>
     * 
     * @param e
     *            edge to be removed from this graph, if present.
     * 
     * @throws EdgeNotFoundException
     *             if the specified edge does not exist in the graph.
     * @throws VertexNotFoundException
     *             if the specified vertex does not exist in the graph.
     */
    void removeEdge(E e) throws EdgeNotFoundException, VertexNotFoundException;

    /**
     * Removes the specified edges.
     * 
     * @param edges
     *            edges to be removed from this graph.
     * 
     */
    void removeEdges(Collection<E> edges);

    /**
     * Removes all the edges going from vertex v1 to vertex v2. Returns a set of all removed edges. If both vertices
     * exist but no edge is found, returns an empty set.
     * 
     * @param v1
     *            head vertex of the edge.
     * @param v2
     *            tail vertex of the edge.
     * 
     * @return the removed edges
     * @throws VertexNotFoundException
     *             if one of the specified vertices does not exist in the graph
     */
    Set<E> removeEdges(V v1, V v2) throws VertexNotFoundException;

    /**
     * Removes the specified vertex from this graph including all the edges going from or to the vertex.
     * 
     * @param v
     *            vertex to be removed.
     * 
     * @throws VertexNotFoundException
     *             if the specified vertex is not in the graph
     */
    void removeVertex(V v) throws VertexNotFoundException;

    /**
     * Removes the specified vertices.
     * 
     * @param vertices
     *            vertices to be removed from this graph.
     * 
     */
    void removeVertices(Collection<V> vertices);
}
