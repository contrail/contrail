/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/plan/src/main/java/org/slasoi/gslam/commons/plan/Plan.java $
 */

/**
 * 
 */
package org.slasoi.gslam.commons.plan;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.gslam.commons.plan.graph.DirectedAcyclicGraph;
import org.slasoi.gslam.commons.plan.graph.EdgeFoundException;
import org.slasoi.gslam.commons.plan.graph.VertexFoundException;
import org.slasoi.gslam.commons.plan.graph.VertexNotFoundException;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;

/**
 * A Plan is defined as a DAG (Directed Acyclic Graph) with only one entry point (root).
 * 
 * In order to assure the existence and uniqueness of the entry point,
 * 
 * the Plan is defined as a root element + a graph, instead of extending
 * 
 * the DirectedAcyclicGraph class.
 * 
 * @author Beatriz Fuentes
 * 
 */
public class Plan implements ProvisioningAdjustment.Plan {

    /**
     * the logger.
     */
    private static Logger logger = Logger.getLogger(Plan.class.getName());

    /**
     * the identifier of this plan.
     */
    private String planId;

    /**
     * the unique entry point of the plan, the rootTask.
     */
    private Task rootTask;

    /**
     * the plan represented as a directed acyclic graph.
     */
    private DirectedAcyclicGraph<Task> dag;

    /**
     * Constructor.
     * 
     * @param planId
     *            the identifier for this plan
     */
    public Plan(String planId) {
        this.planId = planId;
        rootTask = null;
        dag = new DirectedAcyclicGraph<Task>();
    }

    /**
     * sets the planId.
     * 
     * @param planId
     *            the identifier for this plan
     */
    public void setPlanId(String planId) {
        this.planId = planId;
    }

    /**
     * gets the planId.
     * 
     * @return the plan identifier
     */
    public String getPlanId() {
        return (planId);
    }

    /**
     * gets the root task.
     * 
     * @return the root task
     */
    public Task getRootTask() {
        return (rootTask);
    }

    /**
     * sets a given task as the root task (the unique entry point).
     * 
     * @param root
     *            task that will be the unique entry point of the Plan
     * @throws RootFoundException
     *             if there is a root task already in the plan
     * @throws TaskFoundException
     *             if the task already exists in the plan
     */
    public void setRoot(Task root) throws TaskFoundException, RootFoundException {
        if (rootTask != null) {
            throw new RootFoundException();
        }

        rootTask = root;
        try {
            dag.addVertex(root);
        }
        catch (VertexFoundException e) {
            throw new TaskFoundException();
        }
    }

    /**
     * add task to the plan.
     * 
     * @param node
     *            task to be added to the plan
     * @throws TaskFoundException
     *             if the task is already in the plan
     */
    public void addNode(Task node) throws TaskFoundException {

        try {
            if (dag != null) {
                dag.addVertex(node);
            }
        }
        catch (VertexFoundException e) {
            throw new TaskFoundException();
        }
    }

    /**
     * adds an edge from task parent to task child.
     * 
     * Both tasks must exist already in the plan.
     * 
     * @param parent
     *            task that will be the origin of the edge
     * @param child
     *            task that will be the end of the edge
     * @throws TaskNotFoundException
     *             if one of the tasks does not exist in the plan
     * @throws EdgeFoundException
     *             if the edge already exists in the plan
     */
    public void addEdge(Task parent, Task child) throws TaskNotFoundException, EdgeFoundException {
        try {
            dag.addEdge(parent, child);
        }
        catch (VertexNotFoundException e) {
            throw new TaskNotFoundException();
        }
    }

    /**
     * add a list of tasks as children of the parent task.
     * 
     * Parent task must exist in the plan.
     * 
     * If the children ones are not present, they are added to the plan.
     * 
     * @param parent
     *            task that will be the parent of the tasks in the second argument
     * @param children
     *            tasks that will be set as children of the task in the first argument
     */
    public void addChildren(Task parent, List<Task> children) {
        // First, add children to the plan, if they do not exist yet
        for (Task t : children) {
            try {
                dag.addVertex(t);
            }
            catch (VertexFoundException e) {
                // child already exits, it's ok
                logger.debug("Node already exists, but ok");
            }
            finally {
                // Add edge
                try {
                    dag.addEdge(parent, t);
                }
                catch (VertexNotFoundException e) {
                    // cannot happen, since we have just added the node
                    e.printStackTrace();
                }
                catch (EdgeFoundException e) {
                    // Edge already in the plan, ok
                }
            }
        }

    }

    /**
     * gets the list of children tasks of the task in the argument.
     * 
     * @param task
     *            task whose children will be returned
     * @return children children tasks of the task passed as argument
     * @throws TaskNotFoundException
     *             when the task is not found
     */
    public Set<Task> getChildren(Task task) throws TaskNotFoundException {
        try {
            return dag.getSuccessors(task);
        }
        catch (VertexNotFoundException e) {
            throw new TaskNotFoundException();
        }
    }

    /**
     * gets the list of parent tasks of the task in the argument.
     * 
     * @param task
     *            task whose parent will be returned
     * @return parent parent tasks of the task passed as argument
     */
    public Set<Task> getParents(Task task) throws TaskNotFoundException {
        try {
            return dag.getPredecessors(task);
        }
        catch (VertexNotFoundException e) {
            throw new TaskNotFoundException();
        }
    }

    /**
     * gets the total number of nodes in the plan.
     * 
     * @return the total number of nodes in the plan
     */
    public int getNumberNodes() {
        int numberNodes = 0;

        Task root = getRootTask();

        // List to store the not-yet-expanded nodes
        ArrayList<Task> tasks = new ArrayList<Task>();
        tasks.add(root);
        numberNodes++;

        Task current = null;
        while (tasks.size() > 0) {
            // Take the first task and deletes it from the list
            current = tasks.get(0);

            // Expands the node and add the children to the list if it is not already there
            try {
                Set<Task> children = getChildren(current);
                for (Task t : children) {
                    if (!tasks.contains(t)) {
                        tasks.add(t);
                        numberNodes++;
                    }
                }
            }
            catch (TaskNotFoundException e) {
                e.printStackTrace();
            }

            tasks.remove(current);
        }

        return numberNodes;
    }

    /**
     * toString method.
     * 
     * @return a String representation of a Plan
     */
    public String toString() {
        // TO-DO: better output
        String output = "Plan " + planId + "\n";
        output = output + "Root task = " + rootTask + "\n";
        output = output + dag;
        return output;
    }

}
