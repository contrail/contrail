/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/plan/src/main/java/org/slasoi/gslam/commons/plan/graph/VertexDirectedEdgesCollection.java $
 */

/**
 * 
 */
package org.slasoi.gslam.commons.plan.graph;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Class representing the collection of edges connected to a given vertex in a directed graph. Each vertex is
 * characterized for a set of incoming edges and a set of outgoing edges.
 * 
 * @param <E>
 *            an edge
 * @author Beatriz Fuentes
 * 
 */
public class VertexDirectedEdgesCollection<E> {

    /**
     * Set of incoming edges.
     */
    private Set<E> incomingEdges;

    /**
     * Set of outgoing edges.
     */
    private Set<E> outgoingEdges;

    /**
     * Constructor.
     */

    public VertexDirectedEdgesCollection() {
        incomingEdges = new LinkedHashSet<E>();
        outgoingEdges = new LinkedHashSet<E>();
    }

    /**
     * get the incoming edge set.
     * 
     * @return incomingEdges the set of incoming edges
     */
    public Set<E> getIncomingEdges() {
        return incomingEdges;
    }

    /**
     * get the outgoing edge set.
     * 
     * @return outgoingEdges the set of outgoing edges
     */
    public Set<E> getOutgoingEdges() {
        return outgoingEdges;
    }

    /**
     * add an incoming edge.
     * 
     * @param edge
     *            the incoming edge to be added
     */
    public void addIncomingEdge(E edge) {
        incomingEdges.add(edge);
    }

    /**
     * add an outgoing edge.
     * 
     * @param edge
     *            the outgoing edge to be added
     */
    public void addOutgoingEdge(E edge) {
        outgoingEdges.add(edge);
    }

    /**
     * check if the edge is contained in the collections.
     * 
     * @param edge
     *            the edge to be checked
     * @return a boolean indicating if the edge is already attached to this vertex
     */
    public boolean containsEdge(E edge) {
        // return (incomingEdges.contains(edge) || outgoingEdges.contains(edge));
        return (containsEdge(incomingEdges, edge) || containsEdge(outgoingEdges, edge));
    }

    /**
     * check if the edge is contained in the collections.
     * 
     * @param edge
     *            the edge to be checked
     * @return a boolean indicating if the edge is already attached to this vertex
     */
    public boolean containsEdge(Set<E> collection, E edge) {
        boolean contains = false;

        Iterator<E> iter = collection.iterator();
        while (iter.hasNext() && !contains) {
            E temp = iter.next();
            contains = edge.equals(temp);
        }

        return contains;
    }

    /**
     * remove an incoming edge.
     * 
     * @param edge
     *            the edge to be removed from the set of incoming edges
     * @throws EdgeNotFoundException
     *             is the edge does not exist
     */
    public void removeIncomingEdge(E edge) throws EdgeNotFoundException {
        boolean result = incomingEdges.remove(edge);
        if (!result) {
            throw new EdgeNotFoundException();
        }
    }

    /**
     * Remove an outgoing edge.
     * 
     * @param edge
     *            the edge to be removed from the set of outgoing edges
     * @throws EdgeNotFoundException
     *             is the edge does not exist
     */
    public void removeOutgoingEdge(E edge) throws EdgeNotFoundException {
        boolean result = outgoingEdges.remove(edge);
        if (!result) {
            throw new EdgeNotFoundException();
        }
    }

    /**
     * toString method.
     * 
     * @return a string representation of and a collection of edges
     */
    public String toString() {
        String output = "Incoming edges:  \n";
        for (E e : incomingEdges) {
            output = output + e + "  ";
        }

        output = output + "\nOutgoing edges: \n";
        for (E e : outgoingEdges) {
            output = output + e + "  ";
        }
        output = output + "\n";

        return output;
    }
}
