/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/plan/src/main/java/org/slasoi/gslam/commons/plan/graph/DirectedAcyclicGraph.java $
 */

package org.slasoi.gslam.commons.plan.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * Class implementing a Directed Acyclic Graph (DAG). A DAG is a directed graph without loops.
 * 
 * @param <V>: vertex
 * 
 * @author Beatriz Fuentes
 * 
 */

public class DirectedAcyclicGraph<V> extends AbstractGraph<V, Edge<V>> implements DirectedGraph<V, Edge<V>> {

    /**
     * A map to store the vertex and the edges.
     */
    private Map<V, VertexDirectedEdgesCollection<Edge<V>>> vertexEdgesSet = null;

    /**
     * EDGE_SAME_VERTEX constant.
     */
    private final String EDGE_SAME_VERTEX = "Edges can not join one vertex with itself";

    /**
     * Constructor.
     */
    public DirectedAcyclicGraph() {
        vertexEdgesSet = new LinkedHashMap<V, VertexDirectedEdgesCollection<Edge<V>>>();
    }

    /**
     * get the incoming degree of the vertex (number of edges having vertex as tail).
     * 
     * @param vertex
     *            vertex for which the inner degree will be calculated
     * @return inDegree inner degree of the vertex
     * @throws VertexNotFoundException
     *             if the vertex does not exist in the graph
     * 
     * @see org.slasoi.gslam.commons.plan.graph.DirectedGraph#getInDegree(java.lang.Object)
     */
    public int getInDegree(V vertex) throws VertexNotFoundException {
        if (!vertexEdgesSet.containsKey(vertex)) {
            throw new VertexNotFoundException();
        }

        return getEdgeCollection(vertex).getIncomingEdges().size();
    }

    /**
     * get the incoming edges to the vertex (edges having vertex as tail).
     * 
     * @param vertex
     *            vertex for which the inner edges will be returned
     * @return edges inner edges of the vertex
     * @throws VertexNotFoundException
     *             if the vertex does not exist in the graph
     * 
     * @see org.slasoi.gslam.commons.plan.graph.DirectedGraph#getIncomingEdges(java.lang.Object)
     */
    public Set<Edge<V>> getIncomingEdges(V vertex) throws VertexNotFoundException {
        if (!vertexEdgesSet.containsKey(vertex)) {
            throw new VertexNotFoundException();
        }

        return Collections.unmodifiableSet(getEdgeCollection(vertex).getIncomingEdges());
    }

    /**
     * get the outgoing degree of the vertex (number of edges having vertex as head).
     * 
     * @param vertex
     *            vertex for which the outer degree will be calculated
     * @return outDegree outer degree of the vertex
     * @throws VertexNotFoundException
     *             if the vertex does not exist in the graph
     * 
     * @see org.slasoi.gslam.commons.plan.graph.DirectedGraph#getOutDegree(java.lang.Object)
     */
    public int getOutDegree(V vertex) throws VertexNotFoundException {
        if (!vertexEdgesSet.containsKey(vertex)) {
            throw new VertexNotFoundException();
        }

        return getEdgeCollection(vertex).getOutgoingEdges().size();
    }

    /**
     * get the outgoing edges to the vertex (edges having vertex as head).
     * 
     * @param vertex
     *            vertex for which the outer edges will be returned
     * @return edges outer edges of the vertex
     * @throws VertexNotFoundException
     *             if the vertex does not exist in the graph
     * 
     * @see org.slasoi.gslam.commons.plan.graph.DirectedGraph#getOutgoingEdges(java.lang.Object)
     */
    public Set<Edge<V>> getOutgoingEdges(V vertex) throws VertexNotFoundException {
        if (!vertexEdgesSet.containsKey(vertex)) {
            throw new VertexNotFoundException();
        }

        return Collections.unmodifiableSet(getEdgeCollection(vertex).getOutgoingEdges());
    }

    /**
     * Returns the set of vertex that are predecessors of the specified vertex, that is, there is an edge with a
     * predecessor as head and the specified vertex as tail.
     * 
     * @param vertex
     *            the vertex whose predecessors are going to be returned
     * @return the set of predecessors
     * @throws VertexNotFoundException
     *             if the specified vertex is not contained in the graph
     */
    public Set<V> getPredecessors(V vertex) throws VertexNotFoundException {
        Set<V> predecessors = new HashSet<V>();

        // Gets the edges connected to this vertex and selects the incoming ones
        VertexDirectedEdgesCollection<Edge<V>> edges = getEdgeCollection(vertex);
        Set<Edge<V>> incoming = edges.getIncomingEdges();

        // Gets the head of the incoming edges and add them to predecessors
        for (Edge<V> e : incoming) {
            predecessors.add(e.getHead());
        }
        return Collections.unmodifiableSet(predecessors);
    }

    /**
     * Returns the set of vertex that are successors of the specified vertex, that is, there is an edge with a specified
     * vertex as head and the successor as tail.
     * 
     * @param vertex
     *            the vertex whose successors are going to be returned
     * @return the set of successors
     * @throws VertexNotFoundException
     *             if the specified vertex is not contained in the graph
     */
    public Set<V> getSuccessors(V vertex) throws VertexNotFoundException {
        Set<V> sucessors = new HashSet<V>();

        // Gets the edges connected to this vertex and selects the outgoing ones
        VertexDirectedEdgesCollection<Edge<V>> edges = getEdgeCollection(vertex);
        Set<Edge<V>> outgoing = edges.getOutgoingEdges();

        // Gets the tail of the outgoing edges and add them to predecessors
        for (Edge<V> e : outgoing) {
            sucessors.add(e.getTail());
        }

        return Collections.unmodifiableSet(sucessors);
    }

    /**
     * add an edge to the graph between vertices v1 and v2. Both vertices must already exist in the graph.
     * 
     * @param v1
     *            head of the edge
     * @param v2
     *            tail of the edge
     * @throws VertexNotFoundException
     *             if one of the vertices does not exist in the graph
     * @throws EdgeFoundException
     *             if an edge already exists between v1 and v2
     * @return edge the edge that has been added to the graph
     * 
     */
    public Edge<V> addEdge(V v1, V v2) throws VertexNotFoundException, EdgeFoundException {
        Edge<V> e = new Edge<V>(v1, v2);
        addEdge(e);

        return e;
    }

    /**
     * add a given edge to the graph. Vertices must exist in the graph.
     * 
     * @param e
     *            edge to be added
     * @throws VertexNotFoundException
     *             if the vertex is not found in the DAG
     * @throws EdgeFoundException
     *             is the vertex is found in the DAG
     * 
     */
    public void addEdge(Edge<V> e) throws VertexNotFoundException, EdgeFoundException {
        V head = (V) e.getHead();
        V tail = (V) e.getTail();

        // check that the edge is not a loop
        if (head.equals(tail)) {
            throw new IllegalArgumentException(EDGE_SAME_VERTEX);
        }

        // check that an edge between vertices does not exist
        if (containsEdge(e)) {
            throw new EdgeFoundException();
        }

        // Vertices already exist in the graph.
        // Otherwise, containsEdge would have thrown an exception.
        getEdgeCollection(head).addOutgoingEdge(e);
        getEdgeCollection(tail).addIncomingEdge(e);
    }

    /**
     * add a vertex to the graph.
     * 
     * @param v
     *            to be added
     * @throws VertexFoundException
     *             if the vertex is already present in the DAG
     * 
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#addVertex(java.lang.Object)
     */
    public void addVertex(V v) throws VertexFoundException {
        if (containsVertex(v)) {
            throw new VertexFoundException();
        }
        else {
            vertexEdgesSet.put(v, new VertexDirectedEdgesCollection<Edge<V>>());
        }
    }

    /**
     * checks whether the graph contains the given edge.
     * 
     * @param e
     *            edge to be checked
     * @return boolean indicating if the graph contains the edge or not
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#containsEdge(java.lang.Object)
     */
    public boolean containsEdge(Edge<V> e) {
        boolean found = false;

        // takes the collection of edges attached to the head of the edge
        V head = (V) e.getHead();

        try {
            VertexDirectedEdgesCollection<Edge<V>> collection = getEdgeCollection(head);

            found = collection.containsEdge(e);
        }
        catch (VertexNotFoundException e1) {
            found = false;
        }

        return found;
    }

    /**
     * check if the vertex already exists in the graph.
     * 
     * @param v
     *            vertex to be checked
     * @return boolean indicating if the vertex already exist
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#containsVertex(java.lang.Object)
     */
    public boolean containsVertex(V v) {
        return vertexEdgesSet.containsKey(v);
    }

    /**
     * returns the degree of the vertex. Degree is the number of edges touching a vertex.
     * 
     * @param v
     *            vertex for which the degree is going to be calculated
     * 
     * @return the degree of the vertex
     * @throws VertexNotFoundException
     *             when the vertex is not found in the graph.
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#getDegree(java.lang.Object)
     */
    public int getDegree(V v) throws VertexNotFoundException {
        return getInDegree(v) + getOutDegree(v);
    }

    /**
     * get an edge with head v1 and tail v2.
     * 
     * @param v1
     *            head of the edge
     * @param v2
     *            tail of the edge
     * @throws VertexNotFoundException
     *             if one of the vertices does not exist in the graph
     * @return edges edge with head v1 and tail v2
     * 
     */
    public Edge<V> getEdge(V v1, V v2) throws VertexNotFoundException {

        Edge<V> edge = null;

        if (!containsVertex(v1) || !containsVertex(v2)) {
            throw new VertexNotFoundException();
        }

        VertexDirectedEdgesCollection<Edge<V>> collection = getEdgeCollection(v1);

        for (Edge<V> e : collection.getOutgoingEdges()) {
            if (e.getHead().equals(v2)) {
                edge = e;
            }
        }

        return edge;
    }

    /**
     * get the set of edges with head v1 and tail v2.
     * 
     * @param v1
     *            head of the edge
     * @param v2
     *            tail of the edge
     * @throws VertexNotFoundException
     *             if one of the vertices does not exist in the graph
     * @return edges set of edges with head v1 and tail v2
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#getEdges(java.lang.Object, java.lang.Object)
     */
    public Set<Edge<V>> getEdges(V v1, V v2) throws VertexNotFoundException {
        Set<Edge<V>> edges = null;

        if (containsVertex(v1) && containsVertex(v2)) {
            edges = new LinkedHashSet<Edge<V>>();

            VertexDirectedEdgesCollection<Edge<V>> collection = getEdgeCollection(v1);

            for (Edge<V> e : collection.getOutgoingEdges()) {
                if (e.getTail().equals(v2)) {
                    edges.add(e);
                }
            }
        }
        else {
            throw new VertexNotFoundException();
        }

        return edges;
    }

    /**
     * A lazy build of edge container for specified vertex.
     * 
     * @param vertex
     *            a vertex in this graph.
     * 
     * @return EdgeContainer
     * 
     * @throws VertexNotFoundException
     *             if the vertex is not found in the DAG
     */
    private VertexDirectedEdgesCollection<Edge<V>> getEdgeCollection(V vertex) throws VertexNotFoundException {
        if (!vertexEdgesSet.containsKey(vertex)) {
            throw new VertexNotFoundException();
        }

        return vertexEdgesSet.get(vertex);
    }

    /**
     * get the edges attached to the vertex (incoming + outgoing).
     * 
     * @param vertex
     *            vertex whose edges will be returned
     * @return set of edges linked to this vertex
     * 
     * @throws VertexNotFoundException
     *             if the vertex is not found in the DAG
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#getEdges(java.lang.Object)
     */
    public Set<Edge<V>> getEdges(V vertex) throws VertexNotFoundException {
        HashSet<Edge<V>> inAndOut = new HashSet<Edge<V>>(getEdgeCollection(vertex).getIncomingEdges());
        inAndOut.addAll(getEdgeCollection(vertex).getOutgoingEdges());

        return Collections.unmodifiableSet(inAndOut);
    }

    /**
     * Returns the set of vertices of the graph.
     * 
     * @return the set of vertices of the graph
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#getVertices()
     */
    public Set<V> getVertices() {
        return Collections.unmodifiableSet(vertexEdgesSet.keySet());
    }

    /**
     * removes the edge between v1 and v2.
     * 
     * @param v1
     *            head of the edge to be removed
     * @param v2
     *            tail of the edge to be removed
     * 
     * @return the removed edge
     * 
     * @throws VertexNotFoundException
     *             if any of the two vertices does not exist
     * @throws EdgeNotFoundException
     *             if such an edge does not exist
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#removeEdge(java.lang.Object, java.lang.Object)
     */
    public Edge<V> removeEdge(V v1, V v2) throws EdgeNotFoundException, VertexNotFoundException {
        Edge<V> e = getEdge(v1, v2);

        if (e != null) {
            removeEdge(e);
        }
        else {
            throw new EdgeNotFoundException();
        }
        return e;
    }

    /**
     * Remove the edge from the graph.
     * 
     * @param e
     *            the edge to be removed.
     * @throws EdgeNotFoundException
     *             if the edge does not exist in the graph
     * @throws VertexNotFoundException
     *             if one of the vertices of the edge does not exist in the graph.
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#removeEdge(java.lang.Object)
     */
    public void removeEdge(Edge<V> e) throws EdgeNotFoundException, VertexNotFoundException {
        V head = (V) e.getHead();
        V tail = (V) e.getTail();

        getEdgeCollection(head).removeOutgoingEdge(e);
        getEdgeCollection(tail).removeIncomingEdge(e);
    }

    /**
     * Remove a vertex from the graph.
     * 
     * @param v
     *            vertex to be removed
     * @throws VertexNotFoundException
     *             if the vertex does not exist in the graph.
     * 
     * @see org.slasoi.gslam.commons.plan.graph.Graph#removeVertex(java.lang.Object)
     */
    public void removeVertex(V v) throws VertexNotFoundException {
        // Remove the edges connected to this vertex
        Set<Edge<V>> edges = getEdges(v);
        removeEdges(new ArrayList<Edge<V>>(edges));

        // And now, remove the vertex itself
        vertexEdgesSet.remove(v);
    }

    /**
     * Returns a string representing the dag.
     * 
     * @return a String representation of the DAG
     */
    public String toString() {
        String output = "Directed Acyclic Graph: \n";
        for (V vertex : vertexEdgesSet.keySet()) {
            output = output + "Vertex = " + vertex.toString() + "\n";
            output = output + vertexEdgesSet.get(vertex).toString();
        }
        return output;
    }

}
