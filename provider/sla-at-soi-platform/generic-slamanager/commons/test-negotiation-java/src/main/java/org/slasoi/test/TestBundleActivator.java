package org.slasoi.test;

import java.util.Calendar;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils.GenericSLAManagerUtilsException;
import org.slasoi.gslam.core.negotiation.INegotiation;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.syntaxconverter.SyntaxConverter;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.sla.Party.Operative;


public class TestBundleActivator implements BundleActivator
{
	private BundleContext context;
	private ServiceTracker tracker;
	
	public static SLA createSLA() {
        UUID[] uuids = new UUID[1];
        uuids[0] = new UUID("42");
        
        SLA sla = new SLA();
        sla.setUuid(uuids[0]);
        Party[] parties = new Party[1];
        parties[0] = new Party(new ID("24"), new STND("provider"));
        Operative[] operative = new Operative[1];
        operative[0] = new Operative(new ID("23"));
        parties[0].setOperatives(operative);
        sla.setAgreedAt(new TIME(Calendar.getInstance()));
        sla.setEffectiveFrom(new TIME(Calendar.getInstance()));
        sla.setEffectiveUntil(new TIME(Calendar.getInstance()));
        sla.setParties(parties);
        sla.setTemplateId(uuids[0]);
        InterfaceDeclr[] ifaces = new InterfaceDeclr[1];
        Interface iface = new Interface.Specification("huhu");
        ID[] ids = new ID[1];
        ids[0] = new ID("123");
        ((Interface.Specification)iface).setExtended(ids);
        Operation[] operations = new Operation[1];
        operations[0] = new Operation(ids[0]);
        ((Interface.Specification)iface).setOperations(operations);
        ifaces[0] = new InterfaceDeclr(new ID("24"), new ID("25"), iface);
        Endpoint[] endpoints = new Endpoint[1];
        endpoints[0] = new Endpoint(new ID("26"), new STND("over the ether"));
        endpoints[0].setLocation(new UUID("176"));
        ifaces[0].setEndpoints(endpoints);
        sla.setInterfaceDeclrs(ifaces);
        SimpleDomainExpr domain = new SimpleDomainExpr(ids[0], new STND("equals"));
        ConstraintExpr constraint = new TypeConstraintExpr(ids[0], domain);
        State state = new Guaranteed.State(ids[0], constraint);
        Guaranteed[] guarantees = new Guaranteed[1];
        guarantees[0] = state;
        AgreementTerm[] terms = new AgreementTerm[1];
        terms[0] = new AgreementTerm(ids[0], constraint, null, guarantees);
        sla.setAgreementTerms(terms);
        
        return sla;
    }
	
	public static SLATemplate createSLAT() {
        UUID[] uuids = new UUID[1];
        uuids[0] = new UUID("42");
        
        SLATemplate slat = new SLATemplate();
        slat.setUuid(uuids[0]);
        Party[] parties = new Party[2];
        
        parties[0] = new Party(new ID("24"), org.slasoi.slamodel.vocab.sla.customer);
        Operative[] operative = new Operative[1];
        operative[0] = new Operative(new ID("23"));
        parties[0].setOperatives(operative);
        parties[0].setPropertyValue(org.slasoi.slamodel.vocab.sla.gslam_epr, "http://localhost:9596/services/");
        
        parties[1] = new Party(new ID("25"), org.slasoi.slamodel.vocab.sla.provider);
        Operative[] operative2 = new Operative[1];
        operative2[0] = new Operative(new ID("24"));
        parties[1].setOperatives(operative2);
        parties[1].setPropertyValue(org.slasoi.slamodel.vocab.sla.gslam_epr, "http://localhost:9595/services/");
        
        slat.setParties(parties);
        
        InterfaceDeclr[] ifaces = new InterfaceDeclr[1];
        Interface iface = new Interface.Specification("huhu");
        ID[] ids = new ID[1];
        ids[0] = new ID("123");
        ((Interface.Specification)iface).setExtended(ids);
        Operation[] operations = new Operation[1];
        operations[0] = new Operation(ids[0]);
        ((Interface.Specification)iface).setOperations(operations);
        ifaces[0] = new InterfaceDeclr(new ID("24"), new ID("25"), iface);
        Endpoint[] endpoints = new Endpoint[1];
        endpoints[0] = new Endpoint(new ID("26"), new STND("over the ether"));
        endpoints[0].setLocation(new UUID("176"));
        ifaces[0].setEndpoints(endpoints);
        slat.setInterfaceDeclrs(ifaces);
        SimpleDomainExpr domain = new SimpleDomainExpr(ids[0], new STND("equals"));
        ConstraintExpr constraint = new TypeConstraintExpr(ids[0], domain);
        State state = new Guaranteed.State(ids[0], constraint);
        Guaranteed[] guarantees = new Guaranteed[1];
        guarantees[0] = state;
        AgreementTerm[] terms = new AgreementTerm[1];
        terms[0] = new AgreementTerm(ids[0], constraint, null, guarantees);
        slat.setAgreementTerms(terms);
        
        return slat;
    }
	
    public void start( BundleContext context ) throws Exception
    {   
       System.out.println("**************** TestBundleActivator: BUNDLE STARTED ******************");
       this.context = context;
       
       System.out.println("**************** TestBundleActivator: Getting Service Reference ******************");
       /*
       ServiceReference serviceReference = this.context.getServiceReference("org.slasoi.gslam.core.context.GenericSLAManagerUtils");
       System.out.println("**************** TestBundleActivator: Service Reference obtained ******************");
       GenericSLAManagerUtils genericSLAManagerUtils = (GenericSLAManagerUtils)this.context.getService( serviceReference );
       System.out.println("**************** TestBundleActivator: Service obtained ******************");
       */
       
       //ServiceReference sr = context.getServiceReference(GenericSLAManagerUtils.class.getName());       
       //GenericSLAManagerUtils genericSLAManagerUtils = (GenericSLAManagerUtils) context.getService(sr);
	   //
       this.tracker = new ServiceTracker(context, GenericSLAManagerUtils.class.getName(), null);
	   tracker.open();
	   GenericSLAManagerUtils genericSLAManagerUtils = (GenericSLAManagerUtils) tracker.waitForService(1000);		
	   System.out.println("*************** TestBundleActivator: genericSLAManagerUtils = "+genericSLAManagerUtils );
	   
       try {
			if(genericSLAManagerUtils != null){
				System.out.println("********************  TestBundleActivator: genericSLAManagerUtils!=null *********************");
				System.out.println("********************  TestBundleActivator: genericSLAManagerUtils "+ genericSLAManagerUtils.toString());
				
				SLAManagerContext[] slaManagerContexts = genericSLAManagerUtils.getContextSet("GLOBAL");
				if(slaManagerContexts!=null){
					System.out.println("*********1***********  TestBundleActivator: slaManagerContexts.length = "+slaManagerContexts.length);
					System.out.println("********************  TestBundleActivator: slaManagerContexts[0].getSLAManagerID() = "+slaManagerContexts[0].getSLAManagerID());
					System.out.println("********************  TestBundleActivator: slaManagerContexts[0].getGroupID() = "+slaManagerContexts[0].getGroupID() );
					if(slaManagerContexts.length>1){
						System.out.println("********************  TestBundleActivator: slaManagerContexts[1].getSLAManagerID() = "+slaManagerContexts[1].getSLAManagerID());
						System.out.println("********************  TestBundleActivator: slaManagerContexts[1].getGroupID() = "+slaManagerContexts[1].getGroupID() );
					}
					
					
					SLATemplate slaTemplate = createSLAT();
					
					ISyntaxConverter sc = ((Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter>)slaManagerContexts[1].getSyntaxConverters()).get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);
					INegotiation iNegotiation = ((SyntaxConverter)sc).getJavaNegotiationClient("http://localhost:9596/services/");
					String negId = iNegotiation.initiateNegotiation(slaTemplate);
					System.out.println("*********  TestBundleActivator: Obtained NegId = "+negId);
					
					
					//3 round negotiation via syntaxConverter:
					System.out.println("*********  TestBundleActivator: NEGOTIATE 1 ");
					SLATemplate [] counterOffers = iNegotiation.negotiate(negId, slaTemplate);
					System.out.println("*********  TestBundleActivator: NEGOTIATE 2 ");
					counterOffers = iNegotiation.negotiate(negId, slaTemplate);			
					System.out.println("*********  TestBundleActivator: NEGOTIATE 3 ");
					counterOffers = iNegotiation.negotiate(negId, slaTemplate);
					System.out.println("*********  TestBundleActivator: NEGOTIATE 4 ");
					counterOffers = iNegotiation.negotiate(negId, slaTemplate);					
					//SLA sla = iNegotiation.createAgreement(negId, counterOffers[0] );
					//System.out.println("*********  TestBundleActivator: NEGOTIATE 4 ");
					
					//String ob = sc.renderSLATemplate(slaTemplate);
					//System.out.println("*********  TestBundleActivator: returned = "+ob);
					
				}else{
					System.out.println("*********2***********  TestBundleActivator: slaManagerContexts = null ");
				}
			}else{
				System.out.println("********************  TestBundleActivator: genericSLAManagerUtils = null *********************");
			}
		} catch (GenericSLAManagerUtilsException e) {
			e.printStackTrace();
		}
	
    }
    
    public void stop( BundleContext context ) throws Exception
    {
    	System.out.println("**************** TestBundleActivator BUNDLE STOPPED ******************");
    	tracker.close();
    }
    
}