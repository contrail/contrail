package org.slasoi.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.apache.log4j.Logger;

import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils.GenericSLAManagerUtilsException;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.INegotiation;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ProtocolEngine;
import org.slasoi.gslam.core.negotiation.INegotiation.CancellationReason;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.gslam.core.poc.PlanningOptimization.IAssessmentAndCustomize;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.bnf;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLARegistry;

import org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.NegotiationClient;

public class TestActivator implements BundleActivator {
    private BundleContext context;
    private ServiceTracker tracker;
    private static final String COMMENT = "#";
    private static final String ORC_HOME = "{SLASOI_ORC_HOME}";
    private static final String SLASOI_HOME = System.getenv("SLASOI_HOME");
    private static final String SLASOI_ORC_HOME = System.getenv("SLASOI_ORC_HOME");
    private static final Logger LOGGER = Logger.getLogger(TestActivator.class);

    public void start(BundleContext context) throws Exception {

        LOGGER.info("TestActivator : BUNDLE STARTED");
        this.context = context;
        LOGGER.info("TestActivator : Getting Service Reference");
        this.tracker = new ServiceTracker(context, GenericSLAManagerUtils.class.getName(), null);
        tracker.open();
        GenericSLAManagerUtils genericSLAManagerUtils = (GenericSLAManagerUtils) tracker.waitForService(1000);
        LOGGER.info("TestActivator : genericSLAManagerUtils = " + genericSLAManagerUtils);
        try {
            if (genericSLAManagerUtils != null) {
                LOGGER.info("TestActivator : genericSLAManagerUtils!=null ");
                LOGGER.info("TestActivator : genericSLAManagerUtils " + genericSLAManagerUtils.toString());

                SLAManagerContext[] slaManagerContexts = genericSLAManagerUtils.getContextSet("GLOBAL");
                if (slaManagerContexts != null) {
                    LOGGER.info("TestActivator : slaManagerContexts.length = " + slaManagerContexts.length);
                    LOGGER.info("TestActivator : slaManagerContexts[0].getSLAManagerID() = "
                            + slaManagerContexts[0].getSLAManagerID());
                    LOGGER.info("TestActivator : slaManagerContexts[0].getGroupID() = "
                            + slaManagerContexts[0].getGroupID());
                    String filePath =
                            SLASOI_HOME + System.getProperty("file.separator") + "generic-slamanager"
                                    + System.getProperty("file.separator") + "commons"
                                    + System.getProperty("file.separator") + "test-templates"
                                    + System.getProperty("file.separator") + "templates.conf";
                    LineIterator pathLines = null;
                    try {
                        pathLines = FileUtils.lineIterator(new File(filePath));
                    }
                    catch (IOException e) {
                        LOGGER.error("TestActivator : ERROR OPENING file = " + filePath);
                        e.printStackTrace();
                    }
                    if (pathLines != null) {
                        while (pathLines.hasNext()) {
                            String pathLine = pathLines.nextLine();
                            if (pathLine == null || pathLine.equals("") || pathLine.startsWith(COMMENT)) {
                                continue;
                            }
                            if (pathLine.contains(ORC_HOME)) {
                                pathLine = pathLine.replace(ORC_HOME, SLASOI_ORC_HOME);
                            }
                            LOGGER.info("TestActivator : ABSOLUTE FILE PATH = " + pathLine);
                            try {
                                File file = new File(pathLine);
                                String fileName = file.getName().substring(0, file.getName().lastIndexOf(".xml"));
                                String slaTemplateXml = FileUtils.readFileToString(file);

                                ISyntaxConverter sc =
                                        ((Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter>) slaManagerContexts[0]
                                                .getSyntaxConverters())
                                                .get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);
                                // LOGGER.info("**************slaTemplate = "+slaTemplateXml);
                                LOGGER.info("TestActivator : CONTACTING SYNTAX CONVERTER FOR PARSING");
                                SLATemplate slaTemplate = (SLATemplate) sc.parseSLATemplate(slaTemplateXml);
                                // Create BNF rendering file
                                String htmlRendering = bnf.render(slaTemplate, true);
                                DataOutputStream dos =
                                        new DataOutputStream(new FileOutputStream(new File(fileName + "-parsing.html")));
                                dos.writeChars(htmlRendering);
                                dos.close();
                                SLATemplateRegistry slaTemplateRegistry =
                                        slaManagerContexts[0].getSLATemplateRegistry();
                                LOGGER.info("TestActivator : CONTACTING SLATR");
                                org.slasoi.slamodel.sla.tools.Validator.Warning[] warnings =
                                        slaTemplateRegistry.validateSLATemplate(slaTemplate);
                                LOGGER.info("TestActivator : SLATR warnings = " + warnings.length);
                                for (org.slasoi.slamodel.sla.tools.Validator.Warning warning : warnings) {
                                    LOGGER.info("TestActivator : SLATR warning = " + warning.message());
                                }
                            }
                            catch (SLAManagerContextException e) {
                                e.printStackTrace();
                            }
                            catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                            catch (IOException e) {
                                e.printStackTrace();
                            }
                            catch (java.lang.Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                else {
                    LOGGER.info("TestActivator : slaManagerContexts = null ");
                }
            }
            else {
                LOGGER.info("TestActivator : genericSLAManagerUtils = null");
            }
        }
        catch (GenericSLAManagerUtilsException e) {
            e.printStackTrace();
        }
    }

    public void stop(BundleContext context) throws Exception {
        LOGGER.info("TestActivator : BUNDLE STOPPED ");
        tracker.close();
    }

}
