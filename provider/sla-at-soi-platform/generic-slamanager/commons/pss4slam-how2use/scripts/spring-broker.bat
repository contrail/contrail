@echo off
cd ..
set MAIN_CLASS="-Dexec.mainClass=org.slasoi.slam.negotiation.pss4slam.internal.spring.SpringBroker"
set LOG="-Dlog4j.configuration=file:src/main/java/log4j.properties"
set SPRING="-Dexec.args="classpath:/META-INF/spring/spring-2.0.xml"
mvn exec:java %MAIN_CLASS% %LOG% %SPRING% -e
cd spring
cls