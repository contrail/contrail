/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 1025 $
 * @lastrevision   $Date: 2011-03-22 20:45:15 +0100 (tor, 22 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/pss4slam-how2use/src/main/java/org/slasoi/gslam/commons/how2use/pss4slam/Publisher.java $
 */

package org.slasoi.gslam.commons.how2use.pss4slam;

import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.Template;

public class Publisher
{
    public Publisher( ServiceAdvertisement service )
    {
        this.service = service;
    }
    
    public void run()
    {
        try
        {
            for ( int i = 0; i < 5; i++ )
            {
                long ser = (long)(Math.random() * System.currentTimeMillis());
                Template<String>[] templates = new Template[]{ 
                    new Template<String>( "A-::" + ser ), 
                    new Template<String>( "C-::" + ser ) };
                
                service.getIPublishable().publish( templates, ServiceAdvertisementChannel.SLAMODEL );
                
                try
                {
                    Thread.sleep( 500 );
                }
                catch ( Exception e )
                {
                }
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
    
    protected ServiceAdvertisement service;
}

