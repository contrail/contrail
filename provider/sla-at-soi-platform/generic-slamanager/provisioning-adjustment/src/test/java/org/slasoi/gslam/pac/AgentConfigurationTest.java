/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 892 $
 * @lastrevision   $Date: 2011-03-07 16:49:07 +0100 (pon, 07 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/test/java/org/slasoi/gslam/pac/AgentConfigurationTest.java $
 */

package org.slasoi.gslam.pac;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.slasoi.gslam.pac.config.AgentConfiguration;
import org.slasoi.gslam.pac.config.TaskConfiguration;

import com.thoughtworks.xstream.XStream;

public class AgentConfigurationTest {

    private static final Logger logger = Logger.getLogger(AgentConfigurationTest.class.getName());

    @Test
    public void testSerialization() {
        // creates an agentConfiguration object, serializes it, deserializes the output and checks the new object is
        // equal to the original one

        AgentConfiguration config = createConfiguration();
        String str = serialize(config);
        AgentConfiguration configAfter = deserialize(str);

        assertEquals(config.toString(), configAfter.toString());
    }

    public AgentConfiguration createConfiguration() {
        logger.info("Creating new AgentConfiguration...");

        AgentConfiguration config = new AgentConfiguration();
        config.setAgentId("Manager");

        TaskConfiguration task1 = new TaskConfiguration("Analysis", "org.slasoi.gslam.pac.AnalysisTask", null, "GPAC");
        config.addTaskConfiguration(task1);

        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("MAX_NUMBER_CONCURRENT_PLANS", "10");
        parameters.put("RULES_FILE", "/org/slasoi/gslam/pac/manage_T_services.drl");
        TaskConfiguration task2 =
                new TaskConfiguration("Provisioning", "org.slasoi.gslam.pac.PlanCoordinationTask", parameters, "GPAC");
        config.addTaskConfiguration(task2);

        TaskConfiguration task3 = new TaskConfiguration("Execution", "org.slasoi.gslam.pac.ActionExecutionTask", null, "GPAC");
        config.addTaskConfiguration(task3);

        return config;
    }

    public String serialize(AgentConfiguration config) {
        logger.info("Serializing...");

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.config.AgentConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.AgentConfiguration.class);
        xstream.alias(org.slasoi.gslam.pac.config.TaskConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.TaskConfiguration.class);
        String configStr = xstream.toXML(config);

        logger.info(configStr);

        return (configStr);
    }

    public AgentConfiguration deserialize(String configStr) {
        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.config.AgentConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.AgentConfiguration.class);
        xstream.alias(org.slasoi.gslam.pac.config.TaskConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.TaskConfiguration.class);

        AgentConfiguration agentConfig = new AgentConfiguration();

        try {
            agentConfig = (AgentConfiguration) xstream.fromXML(configStr);
        }

        catch (Exception e) {
            logger.info("Unable to deseserialised Agent Configuration. \n" + e.getMessage());
            e.printStackTrace();
        }

        logger.info(agentConfig);
        return (agentConfig);
    }
}
