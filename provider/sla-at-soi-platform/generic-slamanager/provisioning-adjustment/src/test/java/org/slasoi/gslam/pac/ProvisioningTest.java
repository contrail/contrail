/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/test/java/org/slasoi/gslam/pac/ProvisioningTest.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.gslam.commons.plan.Plan;
import org.slasoi.gslam.commons.plan.Task;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment.PlanFormatException;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment.PlanFoundException;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class ProvisioningTest {
    private static final Logger logger = Logger.getLogger(ProvisioningTest.class.getName());

    private static final String propertiesFile =
            "generic-slamanager" + System.getProperty("file.separator") + "provisioning-adjustment"
                    + System.getProperty("file.separator") + "provisioning_adjustment.properties";

    private static final String PLAN_ID = "MyPlan";
    private static final String TASK_ID = "MyTask";
    private static final String SLA_ID = "MySLA";
    private static final String ACTION_NAME = "MyAction";
    private static final String SERVICE_MANAGER_ID = "MyServiceManager";

    private static int planId = -2;
    private static int nodeId = 0;

    @Before
    public void init() {
        planId++;
        nodeId = 0; // resets node id
        logger.info("Waiting to give time to the bus for disconnecting");
        try {
            Thread.sleep(10000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void executePlan1() {
        logger.info("Executing plan with only one node...");
        ProvisioningAndAdjustment pac = new ProvisioningAndAdjustment(propertiesFile);

        try {
            pac.executePlan(createPlanOneNode());
        }
        catch (PlanFoundException e) {
            e.printStackTrace();
        }
        catch (PlanFormatException e) {
            e.printStackTrace();
        }
        logger.debug("ProvisioningTest: plan execution finished");

        // assert (true);

        pac.stop();
    }

    @Test
    public void executePlan2() {
        logger.info("Executing plan with two nodes...");
        ProvisioningAndAdjustment pac = new ProvisioningAndAdjustment();
        pac.setConfigurationFile(propertiesFile);

        try {
            pac.executePlan(createPlanTwoLevels());
        }
        catch (PlanFoundException e) {
            e.printStackTrace();
        }
        catch (PlanFormatException e) {
            e.printStackTrace();
        }
        logger.debug("ProvisioningTest: plan execution finished");

        // assert (true);

        pac.finalize();
    }

    private Plan createPlanOneNode() {
        Plan plan = new Plan(getPlanId());
        Task node = new Task(getTaskId(), getSlaId(), getActionName(), getServiceManagerId());
        try {
            plan.setRoot(node);
        }
        catch (Exception e) {
            // Shouldn't happen, it's the first task we add to the plan
            e.printStackTrace();
        }

        return plan;
    }

    private Plan createPlanTwoLevels() {
        Plan plan = new Plan("Myplan_" + planId);
        Task root = new Task(getTaskId(), getSlaId(), getActionName(), getServiceManagerId());
        try {
            plan.setRoot(root);

            // Add three children
            List<Task> children = new ArrayList<Task>();
            for (int i = 0; i < 3; i++) {
                Task child = new Task(getTaskId(), getSlaId(), getActionName(), getServiceManagerId());
                children.add(child);
            }
            plan.addChildren(root, children);
        }
        catch (Exception e) {
            // Shouldn't happen, it's the first task we add to the plan
            e.printStackTrace();
        }

        return plan;
    }

    private String getPlanId() {
        planId++;
        return (PLAN_ID + "_" + planId);
    }

    private String getTaskId() {
        nodeId++;
        return (TASK_ID + "_" + nodeId);
    }

    private String getSlaId() {
        return (SLA_ID + "_" + nodeId);
    }

    private String getActionName() {
        return (ACTION_NAME + "_" + nodeId);
    }

    private String getServiceManagerId() {
        return (SERVICE_MANAGER_ID + "_" + nodeId);
    }
}
