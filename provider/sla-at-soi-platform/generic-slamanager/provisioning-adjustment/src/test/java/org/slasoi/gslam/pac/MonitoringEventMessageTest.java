/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 2776 $
 * @lastrevision   $Date: 2011-07-22 18:10:18 +0200 (pet, 22 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/test/java/org/slasoi/gslam/pac/MonitoringEventMessageTest.java $
 */

package org.slasoi.gslam.pac;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.slasoi.gslam.pac.events.Message;

public class MonitoringEventMessageTest {

    private static final Logger logger = Logger.getLogger(MonitoringEventMessageTest.class.getName());

    private static final String CONFIG_PATH =
            System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "generic-slamanager"
                    + System.getProperty("file.separator") + "provisioning-adjustment";
    private static final String TEST_FILE =
            CONFIG_PATH + System.getProperty("file.separator") + "test" + System.getProperty("file.separator")
                    + "MonitoringEvent.xml";

    public void testSerialization() {
        // creates an monitoringevent object, serializes it, deserializes the output and checks the new object is
        // equal to the original one

        String message = null;
        try {
            message = readMessage(TEST_FILE);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        MonitoringEventMessageTranslator translator = new MonitoringEventMessageTranslator();

        Message monitoringEvent = translator.fromXML(message);
        String messageAfter = translator.toXML(monitoringEvent);

        logger.debug(message);
        logger.debug("original message length = " + message.length());

        // TODO: why the lengths seem to be different?
        logger.debug(messageAfter);
        logger.debug("final message length = " + messageAfter.length());
        // assertEquals(message, messageAfter);
        assert (true);
    }

    public String readMessage(String filename) throws IOException {
        logger.info("Creating new MonitoringEventMessage...");

        String lineSep = System.getProperty("line.separator");
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String nextLine = "";
        StringBuffer sb = new StringBuffer();
        while ((nextLine = br.readLine()) != null) {
            sb.append(nextLine);
            sb.append(lineSep);
        }
        return sb.toString();
    }

}
