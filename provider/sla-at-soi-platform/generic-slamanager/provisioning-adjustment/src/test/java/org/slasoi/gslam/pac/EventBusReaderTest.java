/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/test/java/org/slasoi/gslam/pac/EventBusReaderTest.java $
 */

package org.slasoi.gslam.pac;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.gslam.pac.events.ManageabilityAgentMessage;
import org.slasoi.gslam.pac.mockups.AgentMockup;

public class EventBusReaderTest {

    private static final Logger logger = Logger.getLogger(EventBusReaderTest.class.getName());
    private static final String CONFIG_PATH =
            System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "generic-slamanager"
                    + System.getProperty("file.separator") + "provisioning-adjustment";

    private static final String AGENT_CONFIG_FILE =
            CONFIG_PATH + System.getProperty("file.separator") + "test" + System.getProperty("file.separator")
                    + "Configuration_BusTest.xml";

    private static final String BUS_PROPERTIES_FILE =
            CONFIG_PATH + System.getProperty("file.separator") + "test" + System.getProperty("file.separator")
                    + "eventbus.properties";

    private static final String CHANNEL = "TEST_EVENT_CHANNEL";

    private PubSubManager pubSubManager = null;
    private String channel = CHANNEL;

    AgentMockup agent = null;

    @Before
    public void init() {
        agent = new AgentMockup(AGENT_CONFIG_FILE);
        configureMessaging(BUS_PROPERTIES_FILE, channel);
    }

    @Test
    public void test() {

        // Build the message
        ManageabilityAgentMessage message = createMessage();

        // Send the message through the event bus
        PubSubMessage pubsubMessage = new PubSubMessage(channel, message.toXML());
        try {
            if (pubSubManager != null)
                pubSubManager.publish(pubsubMessage);
        }
        catch (MessagingException e) {
            logger.debug("Error when publishing a ManageabilityAgent message");
            e.printStackTrace();
        }

        // wait until the agent has configured itself, the message is sent and analyzed
        try {
            Thread.sleep(10000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        logger.debug("Checking reception of event");
        assert (agent.isEventReceived());

    }

    @After
    public void stop() {
        agent.stop();

        // agent needs some time to completely stop
        try {
            Thread.sleep(15000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void configureMessaging(String propertiesFile, String channel) {

        // Create manager instance
        try {
            pubSubManager = PubSubFactory.createPubSubManager(propertiesFile);
        }
        catch (MessagingException e) {
            logger.error("PubSubManager could not be created" + e.getMessage());
            return;
        }
        catch (FileNotFoundException e) {
            logger.error("Properties file " + propertiesFile + " not found");
        }
        catch (IOException e) {
            logger.error("EventBusHandlingTask, IO error in configureMessaging");
            logger.error(e.getMessage());
        }

        logger.debug("EventBusHandlingTask::configureMessaging.PubSubManager created");

        // Channel creation
        try {
            if (!pubSubManager.isChannel(channel)) {
                pubSubManager.createChannel(new Channel(channel));
                logger.debug("Channel " + channel + " created");
            }
        }
        catch (MessagingException e) {
            logger.error("Error subscribing to channel " + channel);
            logger.error(e.getMessage());
        }

    }

    private ManageabilityAgentMessage createMessage() {
        logger.info("Creating new ManageabilityAgentMessage...");

        ManageabilityAgentMessage message = new ManageabilityAgentMessage();
        message.setTimestamp(System.currentTimeMillis());
        message.setEventType("Provision_Event");
        message.setValue("Id", "ID");
        message.setValue("ProvisioningStatus", "READY");

        logger.debug(message);

        return message;
    }

}
