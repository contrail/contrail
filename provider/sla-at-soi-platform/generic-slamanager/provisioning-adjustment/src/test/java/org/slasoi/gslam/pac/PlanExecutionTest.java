/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 892 $
 * @lastrevision   $Date: 2011-03-07 16:49:07 +0100 (pon, 07 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/test/java/org/slasoi/gslam/pac/PlanExecutionTest.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.gslam.commons.plan.Plan;
import org.slasoi.gslam.commons.plan.Task;
import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.slasoi.gslam.pac.mockups.AgentMockup;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class PlanExecutionTest {
    private static final Logger logger = Logger.getLogger(PlanExecutionTest.class.getName());

    private static final String PLAN_ID = "MyPlan";
    private static final String TASK_ID = "MyTask";
    private static final String TASK_NAME = "PlanExecutionTask";
    private static final String SLA_ID = "MySLA";
    private static final String ACTION_NAME = "MyAction";
    private static final String SERVICE_MANAGER_ID = "INFRA_SERVICE_MANAGER_ID";
    private static final String RULES_FILE_KEY = "RULES_FILE";
    private static final String ACTION_EXECUTION_TASK_KEY = "ACTION_EXECUTION_TASK";
    private static final String CONFIG_PATH =
            System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "generic-slamanager"
                    + System.getProperty("file.separator") + "provisioning-adjustment";
    private static final String RULES_FILE =
            System.getProperty("file.separator") + "rules" + System.getProperty("file.separator") + "org"
                    + System.getProperty("file.separator") + "slasoi" + System.getProperty("file.separator") + "gslam"
                    + System.getProperty("file.separator") + "pac" + System.getProperty("file.separator")
                    + "manage_T_services.drl";
    private static final String ACTION_EXECUTION_TASK = "org.slasoi.gslam.pac.ActionExecutionTask";
    private static final String AGENT_CONFIG_FILE =
            CONFIG_PATH + System.getProperty("file.separator") + "test" + System.getProperty("file.separator")
                    + "Configuration_PlanExecutionTest.xml";

    private static int planId = -2;
    private static int nodeId = 0;
    Agent agent = null;

    private TaskConfiguration config = null;

    @Before
    public void init() {
        if (config == null) {
            HashMap<String, String> parameters = new HashMap<String, String>();
            parameters.put(RULES_FILE_KEY, RULES_FILE);
            parameters.put(ACTION_EXECUTION_TASK_KEY, ACTION_EXECUTION_TASK);
            config = new TaskConfiguration(TASK_ID, TASK_NAME, parameters, "GPAC");
            SharedKnowledgePlane.getInstance("GPAC").initKnowledgeBase();
        }

        agent = new AgentMockup(AGENT_CONFIG_FILE);
        agent.addManagedElement(SERVICE_MANAGER_ID, null);

        planId++;
        nodeId = 0; // resets node id
    }

    @After
    public void finalize() {
        agent.stop();

        // agent needs some time to completely stop
        try {
            Thread.sleep(15000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void executePlan1() {
        logger.info("Executing plan with only one node...");
        PlanExecutionTask task = new PlanExecutionTask(config, createPlanOneNode());
        task.setAgent(agent);
        task.start();

        assert (true);
    }

    @Test
    public void executePlan2() {
        logger.info("\nExecuting plan with two levels...");
        logger.info("First, root should be triggered");
        logger.info("Once root is finished, children must be triggered in parallel");
        PlanExecutionTask task = new PlanExecutionTask(config, createPlanTwoLevels());
        task.setAgent(agent);
        task.start();

        assert (true);
    }

    @Test
    public void executePlan3() {
        logger.info("\nExecuting plan with three levels...");
        logger.info("First, root should be triggered");
        logger.info("Once one node is finished, children must be triggered in parallel");
        PlanExecutionTask task = new PlanExecutionTask(config, createPlanThreeLevels());
        task.setAgent(agent);
        task.start();

        assert (true);
    }

    @Test
    public void executePlan4() {
        logger.info("\nExecuting plan where synchronization is required...");
        logger.info("First, root should be triggered");
        logger.info("Once one node is finished, children must be triggered in parallel");
        logger.info("One node can not start before all its parents finishes");
        PlanExecutionTask task = new PlanExecutionTask(config, createComplexPlan());
        task.setAgent(agent);
        task.start();

        assert (true);
    }

    private Plan createPlanOneNode() {
        Plan plan = new Plan(getPlanId());
        Task node = new Task(getTaskId(), getSlaId(), getActionName(), getServiceManagerId());
        try {
            plan.setRoot(node);
        }
        catch (Exception e) {
            // Shouldn't happen, it's the first task we add to the plan
            e.printStackTrace();
        }

        return plan;
    }

    private Plan createPlanTwoLevels() {
        Plan plan = new Plan("Myplan_" + planId);
        Task root = new Task(getTaskId(), getSlaId(), getActionName(), getServiceManagerId());
        try {
            plan.setRoot(root);

            // Add three children
            List<Task> children = new ArrayList<Task>();
            for (int i = 0; i < 3; i++) {
                Task child = new Task(getTaskId(), getSlaId(), getActionName(), getServiceManagerId());
                children.add(child);
            }
            plan.addChildren(root, children);
        }
        catch (Exception e) {
            // Shouldn't happen, it's the first task we add to the plan
            e.printStackTrace();
        }

        return plan;
    }

    private Plan createPlanThreeLevels() {
        Plan plan = new Plan("Myplan_" + planId);
        int numberTasks = 9;

        Task[] tasks = new Task[numberTasks];
        for (int i = 0; i < numberTasks; i++)
            tasks[i] = new Task(getTaskId(), getSlaId(), getActionName(), getServiceManagerId());

        try {
            plan.setRoot(tasks[0]);

            // add 1, 2, 3 as children
            List<Task> children = new ArrayList<Task>();
            children.add(tasks[1]);
            children.add(tasks[2]);
            children.add(tasks[3]);
            plan.addChildren(tasks[0], children);

            // add 4,5 as 1's children
            children.clear();
            children.add(tasks[4]);
            children.add(tasks[5]);
            plan.addChildren(tasks[1], children);

            // add 6 as 2's children
            children.clear();
            children.add(tasks[6]);
            plan.addChildren(tasks[2], children);

            // add 7,8 as 3's children
            children.clear();
            children.add(tasks[7]);
            children.add(tasks[8]);
            plan.addChildren(tasks[3], children);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return plan;
    }

    private Plan createComplexPlan() {
        Plan plan = new Plan("Myplan_" + planId);
        int numberTasks = 11;

        Task[] tasks = new Task[numberTasks];
        for (int i = 0; i < numberTasks; i++)
            tasks[i] = new Task(getTaskId(), getSlaId(), getActionName(), getServiceManagerId());

        try {
            plan.setRoot(tasks[0]);

            // add 1, 2, 3, 4 as children
            List<Task> children = new ArrayList<Task>();
            children.add(tasks[1]);
            children.add(tasks[2]);
            children.add(tasks[3]);
            children.add(tasks[4]);
            plan.addChildren(tasks[0], children);

            // add 5,6 as 1's children
            children.clear();
            children.add(tasks[5]);
            children.add(tasks[6]);
            plan.addChildren(tasks[1], children);

            // add 7,8 as 4's children
            children.clear();
            children.add(tasks[7]);
            children.add(tasks[8]);
            plan.addChildren(tasks[4], children);

            // add an edge between 3 and 7
            plan.addEdge(tasks[3], tasks[7]);

            // add 9,10 as 8's children
            children.clear();
            children.add(tasks[9]);
            children.add(tasks[10]);
            plan.addChildren(tasks[8], children);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return plan;
    }

    private String getPlanId() {
        planId++;
        return (PLAN_ID + "_" + planId);
    }

    private String getTaskId() {
        nodeId++;
        return (TASK_ID + "_" + nodeId);
    }

    private String getSlaId() {
        return (SLA_ID + "_" + nodeId);
    }

    private String getActionName() {
        return (ACTION_NAME + "_" + nodeId);
    }

    private String getServiceManagerId() {
        return (SERVICE_MANAGER_ID);
    }
}
