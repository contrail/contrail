/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/Task.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.gslam.pac.config.TaskConfiguration;

/**
 * Abstract class implementing a task.
 * 
 * @author Beatriz Fuentes
 * 
 */
public abstract class Task implements Runnable, ITask {
    /**
     * Logger for this class
     */
    protected static Logger logger = Logger.getLogger(Task.class);

    /**
     * The task identifier
     */
    protected String taskId;

    protected TaskConfiguration config = null;

    protected boolean running = true;

    /**
     * Agent owner of this task
     */
    protected Agent agent;

    /**
     * list of event types a task can manage
     */
    protected List<EventType> eventTypes = null;

    /**
     * Constructor
     * 
     * @param taskConfig
     *            object with the task configuration information
     */
    public Task(TaskConfiguration config) {
        this.config = config;
        eventTypes = new ArrayList<EventType>();
        configure();
    }

    /**
     * Default constructor. Does nothing, but it is needed for RTTI purposes.
     */
    public Task() {
        eventTypes = new ArrayList<EventType>();
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Agent getAgent() {
        return agent;
    }

    public void configure(TaskConfiguration config) {
        this.config = config;
        configure();
    }

    public void setTaskId(String id) {
        taskId = id;
    }

    public String getTaskId() {
        return taskId;
    }

    /**
     * 
     * 
     * 
     */
    public void configure() {
        taskId = config.getTaskId();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    public void run() {
        logger.info(this.getClass().getName() + " running");
        running = true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.pac.ITask#start()
     */
    public void start() {
        run();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.pac.ITask#stop()
     */
    public void stop() {
        running = false;
    }

    public void setConfig(TaskConfiguration config) {
        this.config = config;
    }

    public TaskConfiguration getConfig() {
        return config;
    }

    public void addEventType(EventType type) {
        eventTypes.add(type);
    }

    public List<EventType> getListEventTypes() {
        return eventTypes;
    }

    public boolean canProcessEvent(EventType type) {
        return (eventTypes.contains(type));
    }

    public boolean canProcessEvent(Event event) {
        return (eventTypes.contains(event.getType()));
    }

    /**
     * Used for parent-child synchronization
     */
    public void notify(Task child) {
    }

    /**
     * process an event sent by another agent to the agent owning this task
     * 
     * @param event
     */
    public void notifyEvent(Event event) {
    }

    public Event query(Event event) {
        return null;
    }
}
