/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 1118 $
 * @lastrevision   $Date: 2011-03-29 02:12:27 +0200 (tor, 29 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/config/Configuration.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac.config;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * @author Beatriz Fuentes
 * 
 */
public class Configuration {
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(Configuration.class.getName());

    /**
     * Name of the file with the agent configuration
     */
    private String configurationFile = null;
    private AgentConfiguration agentConfig = null;

    /**
     * Constructor
     * 
     * @param configFile
     *            The configuration file
     */
    public Configuration(String configFile) {
        logger.debug("Configuration created from " + configFile);
        configurationFile = configFile;
        configure();
    }

    /**
     * Constructor
     * 
     * @param configFile
     *            The configuration file
     */
    public Configuration(InputStream is) {
        logger.debug("Configuration created from supplied InputStream");
        configure(is);
    }
    
    /**
     * Get the agent configuration
     */
    public AgentConfiguration getAgentConfiguration() {
        return (agentConfig);
    }
    
    /*
     * escamez
     * 
     * added new configuration parameters
     */
    /**
     * Read the configuration file and deserialized it
     */
    private void configure(InputStream is) {
        
        logger.debug("Configuration::configure");
        
        if (is==null){
            logger.info("InputStream IS NULL for Agent Configuration");
            return;
        }
        
        XStream xstream = new XStream(new DomDriver());
        xstream.alias(org.slasoi.gslam.pac.config.AgentConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.AgentConfiguration.class);
        xstream.alias(org.slasoi.gslam.pac.config.TaskConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.TaskConfiguration.class);

        agentConfig = new AgentConfiguration();

        try {          
            agentConfig = (AgentConfiguration) xstream.fromXML(is);
            
            logger.info("AgentConfig: " +agentConfig.toString());
        }
        catch (Exception e) {
            logger.info("Unable to deseserialise Agent Configuration. \n" + e.getMessage());
        }

    }


    /**
     * Read the configuration file and deserialized it
     */
    private void configure() {
        logger.debug("Configuration::configure");
        XStream xstream = new XStream(new DomDriver());
        xstream.alias(org.slasoi.gslam.pac.config.AgentConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.AgentConfiguration.class);
        xstream.alias(org.slasoi.gslam.pac.config.TaskConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.TaskConfiguration.class);

        agentConfig = new AgentConfiguration();

        try {
            String fileContent = readFile(configurationFile);
            agentConfig = (AgentConfiguration) xstream.fromXML(fileContent);
            logger.info(agentConfig.toString());
        }
        catch (Exception e) {
            logger.info("Unable to deseserialised Agent Configuration. \n" + e.getMessage());
        }

    }

    private String readFile(String fileName) {
        String content = null;
        try {
            FileInputStream file = new FileInputStream(fileName);
            byte[] b = new byte[file.available()];
            file.read(b);
            file.close();
            content = new String(b);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return content;
    }

}
