/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/ActionExecutionTask.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.slasoi.gslam.pac.events.ManageabilityAgentMessage;

/**
 * Class in charge of executing a given action that in turn means an invocation to some external component (typically,
 * Service Manager). The action needed to provision a service are domain specific. Therefore, this kind of task need to
 * be reimplemented in the domain-specific PACs (sw and infrastructure). This is included here as an example and
 * guideline.
 * 
 * @author Beatriz Fuentes (TID)
 * 
 */
public class ActionExecutionTask extends Task {

    /*
     * Identifier for the message to be sent by the manageability agent in order to notify that the provisioned service
     * is up and running.
     */
    protected static final String MA_EVENT_IDENTIFIER = "Id";

    /**
     * For testing purposes, we configure if the PAC should wait for a message from the MA or not.
     */
    protected static String WAIT_MANAGEABILITY_AGENT_MESSAGE = "WAIT_MANAGEABILITY_AGENT_MESSAGE";

    /**
     * Value of the previous parameter (TRUE or FALSE).
     */
    protected static String TRUE = "TRUE";

    /**
     * action of the plan to be executed.
     */
    protected org.slasoi.gslam.commons.plan.Task action;

    /**
     * Parent task of this, which will be a PlanExecutionTask.
     */
    protected Task parent;

    /**
     * Constructor.
     * 
     * @param config
     *            configuration of this task
     * @param action
     *            node of the plan to be executed by this task
     * @param parent
     *            the parent task of this ActionExecutionTask
     */
    public ActionExecutionTask(TaskConfiguration config, org.slasoi.gslam.commons.plan.Task action, Task parent) {
        this.action = action;
        this.parent = parent;
        logger.debug("Creating thread for the action " + action.getActionName() + " to ServiceManager "
                + action.getServiceManagerId());
        configure(config);
    }

    /*
     * gets the parent of this task.
     * 
     * @returns the parent of this task
     */
    public Task getParent() {
        return parent;
    }

    /*
     * sets the parent of this task.
     * 
     * @parent the parent of this task
     */
    public void setParent(Task parent) {
        this.parent = parent;
    }

    /**
     * Default constructor. Needed for RTTI purposes.
     */
    public ActionExecutionTask() {

    }

    /**
     * gets the action to be executed by this task.
     * 
     * @return the action executed by this task.
     */
    public org.slasoi.gslam.commons.plan.Task getAction() {
        return (action);
    }

    /**
     * sets the action to be executed by this tasks.
     * 
     * @param action
     *            the action executed by this task.
     */
    public void setAction(org.slasoi.gslam.commons.plan.Task action) {
        this.action = action;
        logger.debug(toStringTask(action));
    }

    /**
     * Needed when instantiating the class using RTTI.
     * 
     * @param config
     *            the configuration for this task.
     */
    public void configure(TaskConfiguration config) {
        super.configure(config);
    }

    /**
     * run method. In short, it triggers commands to the Service Manager (one or more, this is domain-specific), and
     * waits till the message from the Manageability Agent arrives.
     */
    public void run() {

        if (action != null) {
            logger.info("Triggering action " + action.getActionName() + " to ServiceManager "
                    + action.getServiceManagerId());

            action.setStatus(TaskStatus.CREATED.toString());

            ManagedElement serviceManager = agent.getManagedElement(action.getServiceManagerId());

            if (serviceManager != null) {
                Object element = serviceManager.getElement();

                // do whatever

                // later on, a message from the ManageabilityAgent will arrive to notify
                // the actual end of the provisioning
                // then, we need to set as status an identifier to match the message
                // with the node
                action.setStatus(action.getTaskId());
            }
        }

        // First, wait for the ManageabilityAgentMessage to arrive before
        // notifying the parent task
        action.setStatus(TaskStatus.PROVISIONED_OK.toString());

        parent.notify(this);
    }

    public boolean notifyMessage(ManageabilityAgentMessage message) {
        logger.debug("ActionExecutionTask of node" + action.getTaskId() + ", notifyMessage");
        boolean mine = false;
        String id = message.getValue(MA_EVENT_IDENTIFIER);

        mine = (id.equals(action.getStatus()));

        if (mine) {
            synchronized (this) {
                this.notify();
            }
        }

        return mine;
    }

    protected String toStringTask(org.slasoi.gslam.commons.plan.Task task) {
        String output = "Task id = " + task.getTaskId() + "\n";
        output = output + "SLA_ID = " + task.getSlaId() + "\n";
        output = output + "Action name = " + task.getActionName() + "\n";
        output = output + "Service Manager Id = " + task.getServiceManagerId() + "\n";
        output = output + "Status = " + task.getStatus() + "\n";
        output = output + "Time out = " + task.getTimeout() + "\n";
        return output;
    }

}
