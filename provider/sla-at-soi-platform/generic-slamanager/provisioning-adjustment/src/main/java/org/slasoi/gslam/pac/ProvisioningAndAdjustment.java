/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 1315 $
 * @lastrevision   $Date: 2011-04-12 19:27:47 +0200 (tor, 12 apr 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/ProvisioningAndAdjustment.java $
 */

package org.slasoi.gslam.pac;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.drools.builder.ResourceType;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.control.Policy;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;

/**
 * This class implements the interface for a generic Provisioning and Adjustment Component. An object of this class is
 * instantiated at startup. It is in charge of reading an agent configuration file, and of creating the agents. It is
 * also a proxy between the rest of the components of the SLAM and the agents.
 * 
 * @author Beatriz Fuentes (TID)
 * 
 */
public class ProvisioningAndAdjustment implements ProvisioningAdjustment, SLAMContextAware {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(ProvisioningAndAdjustment.class.getName());

    /**
     * Identifier for the messages sent by/to this object.
     */
    protected static Identifier myId = Identifier.PAC;

    /**
     * Name of the property indicating the file for configuring the agents.
     */
    protected static String AGENT_CONFIGURATION_FILE = "agent_configuration_file";

    /**
     * A default properties file, in case none is provided at start time.
     */
    protected static String DEFAULT_PROPERTIES_FILE =
            "generic-slamanager" + System.getProperty("file.separator") + "provisioning-adjustment"
                    + System.getProperty("file.separator") + "provisioning_adjustment.properties";

    /**
     * File with rules for the basic functioning of the system
     */
    protected static String INITIAL_RULES_FILE = "initial_rules_file";

    /**
     * Name of the property that specifies the log4j configuration file.
     */
    protected static String LOG4J_FILE = "log4j_file";

    /**
     * Name of the property that specifies the logging mode for drools (info/debug).
     */
    protected static String LOG_MODE_DROOLS = "log_mode_drools";

    /**
     * Name of the base dir where to look for the configuration files.
     */
    protected String configurationFilesPath = System.getenv("SLASOI_HOME") + System.getProperty("file.separator");

    /**
     * Properties of this class.
     */
    protected Properties properties = new Properties();

    /**
     * Flag indicating if the PAC is being stopped or not.
     */

    protected boolean stopping = false;

    /**
     * Name of the actual configuration file.
     */

    protected String configurationFile = null;

    /**
     * Context of the SLAM where this PAC belongs to.
     */
    protected static SLAManagerContext sLAManagerContext;

    /**
     * Agent with the role of manager.
     */
    protected Agent manager = null;
    
    protected String pacID = null;
    
    /**
     * Constructor.
     * 
     * @param configurationFile
     *            path to the configuration file
     */
    public ProvisioningAndAdjustment(String configurationFile) {
        logger.info("Creating generic PAC...");
        this.configurationFile = configurationFile;
        init();
    }

    /**
     * Constructor to be invoked from a spring context file. The init() method will be invoked by spring, using
     * init-method.
     */
    public ProvisioningAndAdjustment() {
        logger.info("Creating generic PAC...");
        configurationFile = DEFAULT_PROPERTIES_FILE;
        // init();
    }

    /**
     * Setter method for the configuration file.
     * 
     * @param configurationFile
     *            the filename of the configuration file.
     */
    public void setConfigurationFile(String configurationFile) {
        this.configurationFile = configurationFile;
        logger.debug("Configuration file = " + configurationFile);
    }

    /**
     * Gets the configuration filename.
     * 
     * @return the name of the configuration file.
     */
    public String getConfigurationFile() {
        return configurationFile;
    }

    /**
     * Sets the policies that will govern the behaviour of the adjustment.
     * 
     * @param policyClassType
     *            the type of policies (Adjustment)
     * @param policies
     *            the policies to be set.
     */
    public int setPolicies(String policyClassType, Policy[] policies) {
        logger.info("Generic PAC, setting policies: " + policies);
        int result = SharedKnowledgePlane.getInstance(this.pacID).setPolicies(policies);
        return result;
    }

    /**
     * Gets the policies used for adjustment.
     * 
     * @param policyClassType
     *            the type of policies (Adjustment)
     * @return the policies used for adjustment.
     */
    public Policy[] getPolicies(String policyClassType) {
        logger.info("Generic PAC, getting policies ");
        Policy[] list = SharedKnowledgePlane.getInstance(this.pacID).getPolicies();
        return (list);
    }

    /**
     * Performs initialization actions:
     * 
     * - load properties
     * 
     * - create agents
     * 
     * - initialize drools knowledge base.
     */
    public void init() {

        logger.info("ProvisioningAndAdjustment, init() method");
        try {
            properties.load(new FileInputStream(configurationFilesPath + configurationFile));
            PropertyConfigurator.configure(configurationFilesPath + properties.getProperty(LOG4J_FILE));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("Properties file loaded...");

        SharedKnowledgePlane.getInstance(this.pacID).initKnowledgeBase(properties.getProperty(LOG_MODE_DROOLS));
        createAgents();
        SharedKnowledgePlane.getInstance(this.pacID).getStatefulSession().insert(this);
        SharedKnowledgePlane.getInstance(this.pacID).addRulesFile(properties.getProperty(INITIAL_RULES_FILE), ResourceType.DRL);
    }

    /**
     * Cancels the execution of a provisioning plan.
     * 
     * @param planId
     *            identifier of the plan to be cancelled
     * @throws PlanNotFoundException
     *             if the plan is not being provisioned.
     */
    public void cancelExecution(String planId) throws PlanNotFoundException {
        logger.info("Cancelling execution of plan " + planId);
        sendEvent(new Event(EventType.PlanCancelationRequest, myId, planId));
    }

    /**
     * execute the plan received from the POC. There is no return value, the status of the execution will be
     * communicated asynchronously.
     * 
     * The plan is inserted into the shared knowledge plane, so an agent with the task PlanExecution can picked it up.
     * 
     * @param plan
     *            the plan to be executed
     * @throws PlanFoundException
     *             if the plan is already being executed
     */
    public void executePlan(ProvisioningAdjustment.Plan plan) throws PlanFoundException, PlanFormatException {
        logger.info("Executing plan " + plan.getPlanId());
        Event event = new Event(EventType.PlanExecutionRequest, myId, plan);
        sendEvent(event);
    }

    /**
     * Used by the POC to inform the PAC about any decission taken at higher level.
     * 
     * @param planId
     *            identifier of the plan being affected by the decission.
     * @param task
     *            action that is taken place.
     * @param timeEstimation
     *            estimation of the time needed to perform the action.
     * 
     */
    public void ongoingAction(String planId, Task task, long timeEstimation) throws PlanNotFoundException {
        // TODO Auto-generated method stub
        logger.info("Action " + task.getActionName() + " taken for plan " + planId);

    }

    /**
     * Gets the status of a given plan.
     * 
     * @param planId
     *            identifier of the plan.
     * @throws PlanNotFoundException
     *             if the plan is not managed by this PAC.
     * @return the status of the plan.
     */
    public Status getPlanStatus(String planId) throws PlanNotFoundException {
        // TODO Auto-generated method stub
        logger.info("Querying status of plan " + planId);
        Event planStatus = manager.query(new Event(EventType.PlanStatusRequest, myId, planId));
        return (Status) planStatus.getEventContent();
    }

    /**
     * Method to query the Monitoring Database. It is domain specific, therefore not implemented in the generic PAC.
     * 
     * @param serviceManagerId
     *            identifier of the service manager whose monitoring data are requested.
     * @param query
     *            query to be triggered
     * @return the result of the query.
     */
    public String queryMonitoringDatabase(String serviceManagerId, String query) {
        // TODO Auto-generated method stub
        logger.info("Querying monitoring data");
        return "Query ok";
    }

    /**
     * Setter of the SLAM context.
     * 
     * @param context
     *            the context to be set.
     */
    public void setSLAManagerContext(SLAManagerContext context) {
        ProvisioningAndAdjustment.sLAManagerContext = context;
    }

    /**
     * Getter of the SLAM context.
     * 
     * @return the context of the SLAM to which this PAC belongs.
     */
    public static SLAManagerContext getSLAManagerContext() {
        return sLAManagerContext;
    }

    public String getPacID() {
        return pacID;
    }

    public void setPacID(String pacID) {
        this.pacID = pacID;
    }

    
    /**
     * At the moment we only create one agent, the Manager.
     * 
     * The idea is to have a community of agents working in cooperation. To achieve this, agents are inserted into the
     * knowledge plane, so they can pick up messages when they arrive. Knowledge plane is used as a communication
     * mechanism between agents.
     */
    private void createAgents() {
        // At the moment, only one agent is created
        manager = new Agent(configurationFilesPath + properties.getProperty(AGENT_CONFIGURATION_FILE));
        SharedKnowledgePlane.getInstance(this.pacID).getStatefulSession().insert(manager);
        addManagedElements(manager);
        new Thread(manager).start();
    }

    /**
     * Service Managers that will be managed by this PAC.
     * 
     * The Service Managers are domain specific, therefore this method will be overload in domain-specific PACs.
     * 
     * @param agent
     */
    protected void addManagedElements(Agent agent) {
        logger.debug("Generic PAC, addManagedElements");
    }

    /**
     * Sends an event to be picked up by the agent in charge of the task. In this case, Drools will be used as the
     * messaging mechanism.
     * 
     * @param event
     *            event to be sent
     */
    protected void sendEvent(Event event) {
        logger.debug("Sending event... ");
        logger.debug(event);
        SharedKnowledgePlane.getInstance(this.pacID).getStatefulSession().insert(event);
        SharedKnowledgePlane.getInstance(this.pacID).getStatefulSession().fireAllRules();
    }

    /**
     * Method that will be invoked when an agent sends an event to this PAC.
     * 
     * @param event
     *            event sent by an agent to this PAC.
     */
    public void notifyEvent(Event event) {
        EventType type = event.getType();
        logger.debug("PAC::notifyEvent " + type.toString());

        if (type.equals(EventType.PlanExecutionResponse)) {
            // notify status to POC
            logger.debug("PlanExecutionResponse");
            // sLAManagerContext.getPlanningOptimization()....
        }
    }

    /**
     * finalize method.
     */
    public void finalize() {
        logger.info("Finalizing PAC ");
        stop();
    }

    /**
     * Method to be invoked to stop the component.
     * 
     * A message is sent to all the agents, so they stop themselves and their tasks.
     * 
     */
    public void stop() {
        if (!stopping) {
            logger.info("Stopping PAC ");
            Event event = new Event(EventType.StopAgentsEvent, myId, null);
            sendEvent(event);
            stopping = true;
        }
    }

}
