/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 892 $
 * @lastrevision   $Date: 2011-03-07 16:49:07 +0100 (pon, 07 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/PlanExecutionTask.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.slasoi.gslam.commons.plan.Plan;
import org.slasoi.gslam.commons.plan.TaskNotFoundException;
import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.slasoi.gslam.pac.events.ManageabilityAgentMessage;

/**
 * Task in charge of executing a given plan. Agent will create a new PlanExecutionTask each time the POC send a Plan.
 * 
 * @author Beatriz Fuentes (TID)
 * 
 */
public class PlanExecutionTask extends Task {

    private final static String CHILDREN_NAME = "ActionExecutionTask";
    private final static String CHILD_PARAM_NAME = "ACTION_EXECUTION_TASK";

    private Plan plan = null;

    private boolean finished = false;

    private Task parent = null;

    /**
     * actions currently being executed
     */
    private HashMap<String, ActionExecutionTask> actions = null;

    /**
     * last nodes of the plan that have notified its completion
     */
    private Queue<org.slasoi.gslam.commons.plan.Task> currentTaskPool = null;

    private String actionExecutionTaskClassName = null;
    
    private String slamId;

    /**
     * @param config
     */
    public PlanExecutionTask(TaskConfiguration config) {
        super(config);
        actions = new HashMap<String, ActionExecutionTask>();
        currentTaskPool = new LinkedList<org.slasoi.gslam.commons.plan.Task>();
        actionExecutionTaskClassName = config.getValue(CHILD_PARAM_NAME);
        slamId = config.getSlamId();
        logger.debug(config.toString());
        logger.debug("actionExecutionTaskClassName = " + actionExecutionTaskClassName);
    }

    public PlanExecutionTask(TaskConfiguration config, Plan plan) {
        super(config);
        logger.debug("PlanExecutionTask constructor");
        this.plan = plan;
        actions = new HashMap<String, ActionExecutionTask>();
        currentTaskPool = new LinkedList<org.slasoi.gslam.commons.plan.Task>();
        actionExecutionTaskClassName = config.getValue(CHILD_PARAM_NAME);
        slamId = config.getSlamId();
        logger.debug(config.toString());
        logger.debug("actionExecutionTaskClassName = " + actionExecutionTaskClassName);
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public Plan getPlan() {
        return plan;
    }

    public String getPlanId() {
        return plan.getPlanId();
    }

    public void setParent(Task parent) {
        this.parent = parent;
    }

    public Task getParent() {
        return parent;
    }

    public void run() {
        // actual execution of the plan

        // List of tasks that can be triggered simultaneously
        // When starting the execution, only the root node can be executed
        ArrayList<org.slasoi.gslam.commons.plan.Task> tasksToTrigger =
                new ArrayList<org.slasoi.gslam.commons.plan.Task>();
        tasksToTrigger.add(plan.getRootTask());
        synchronized (this) {

            while (!finished && running) {
                // trigger the actions
                for (org.slasoi.gslam.commons.plan.Task t : tasksToTrigger) {
                    ActionExecutionTask actionTask = createActionExecutionTask(t);

                    synchronized (actions) {
                        actions.put(t.getTaskId(), actionTask);
                    }
                    new Thread(actionTask).start();
                }

                try {
                    logger.debug("Waiting for the actions to complete...");
                    wait();
                }
                catch (InterruptedException e) {
                    logger.debug("Someone has interrupted my dreams...");
                }
                finally {
                    logger.debug("Awaiking...");
                    tasksToTrigger = next();
                    logger.debug("Actions in execution = " + actions.size());
                    // This task has no children to execute, look if the plan has finished
                    finished = ((tasksToTrigger.size() == 0) && (actions.size() == 0));
                }
            }
        }
        logger.debug("Plan " + plan.getPlanId() + ", execution finished");
        if (parent != null)
            parent.notify(this);

        PlanStatus status = new PlanStatus(plan.getPlanId(), Status.PROVISIONED_OK);
        Event event = new Event(EventType.PlanExecutionResponse, Identifier.PlanExecutionTask, status);
        SharedKnowledgePlane.getInstance(this.slamId).getStatefulSession().insert(event);
        SharedKnowledgePlane.getInstance(this.slamId).getStatefulSession().fireAllRules();
        /*
         * // Now, wait until the messages from the ManageabilityAgents arrive // We should receive one message per
         * Plan's node int messagesPending = plan.getNumberNodes(); while (running && messagesPending > 0) { try {
         * logger.debug("Waiting for the messages to arrive..."); wait(); } catch (InterruptedException e) {
         * logger.debug("Someone has interrupted my dreams..."); } finally { logger.debug("Awaiking..."); synchronized
         * (this) { // TODO: match the received message with the actual node and update its status messagesPending--; }
         * logger.debug("Messages pending = " + messagesPending); } }
         * 
         * logger.debug("All the Manageability Agent messages have arrived. Provisioning completed");
         */
    }

    public ActionExecutionTask createActionExecutionTask(org.slasoi.gslam.commons.plan.Task action) {
        logger.debug("createActionExecutionTask...");
        Object task = null;

        TaskConfiguration configuration =
                new TaskConfiguration(CHILDREN_NAME, action.getTaskId(), config.getParameters(), this.slamId);

        try {
            // Create the task instance, using RTTI
            Class<?> taskClass = Class.forName(actionExecutionTaskClassName);
            task = taskClass.newInstance();
            ((ActionExecutionTask) task).setAction(action);
            ((ActionExecutionTask) task).configure(configuration);
            ((ActionExecutionTask) task).setAgent(agent);
            ((ActionExecutionTask) task).setParent(this);
        }
        catch (InstantiationException e) {
            logger.info("Error when instantiating the task:  " + e.getMessage());
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            logger.info("Cannot access the class: " + e.getMessage());
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            logger.info("Cannot find class " + actionExecutionTaskClassName);
            logger.info(e.getMessage());
            e.printStackTrace();
        }

        return ((ActionExecutionTask) task);
    }

    /**
     * Look at the children of tasks in currentTaskPool, to see which of them can be triggered.
     * 
     * @return
     */
    synchronized private ArrayList<org.slasoi.gslam.commons.plan.Task> next() {
        ArrayList<org.slasoi.gslam.commons.plan.Task> next = new ArrayList<org.slasoi.gslam.commons.plan.Task>();

        Set<org.slasoi.gslam.commons.plan.Task> children = null;
        while (!currentTaskPool.isEmpty()) {
            org.slasoi.gslam.commons.plan.Task currentNode;
            synchronized (currentTaskPool) {
                currentNode = currentTaskPool.poll();
            }

            synchronized (actions) {
                actions.remove(currentNode.getTaskId());
            }

            try {
                children = plan.getChildren(currentNode);
            }
            catch (TaskNotFoundException e) {
                // Should not happen, the task was previously in the plan
                e.printStackTrace();
            }

            // Now, for each child, look if it can be triggered: one node can be triggered only if all its parents are
            // done
            for (org.slasoi.gslam.commons.plan.Task child : children) {
                Set<org.slasoi.gslam.commons.plan.Task> parents = null;
                boolean done = true;

                try {
                    parents = plan.getParents(child);
                }
                catch (TaskNotFoundException e) {
                    // Should not happen, the task was previously in the plan
                    e.printStackTrace();
                }

                Iterator<org.slasoi.gslam.commons.plan.Task> iter = parents.iterator();
                while (iter.hasNext() && done) {
                    org.slasoi.gslam.commons.plan.Task father = (org.slasoi.gslam.commons.plan.Task) iter.next();
                    if (father.getStatus() != Status.PROVISIONED_OK.toString())
                        done = false;
                }

                // all the parents have been provisioned, so we can proceed with the child
                if (done)
                    next.add(child);

            }
        }

        logger.debug("Number of  tasks to be triggered? = " + next.size());

        return next;
    }

    public synchronized void notify(Task child) {
        // a child has completed its work
        // A provisioning action has been completed. Proceed with the execution.
        logger.debug("PlanExecutionTask, notification from child " + child.getTaskId() + " received");
        if (child instanceof ActionExecutionTask) {
            org.slasoi.gslam.commons.plan.Task currentNode = ((ActionExecutionTask) child).getAction();
            synchronized (currentTaskPool) {
                currentTaskPool.add(currentNode);
            }

            logger.info("Task " + currentNode.getTaskId() + " completed. Status = " + currentNode.getStatus());

            // wake up the interrupted thread
            synchronized (this) {
                this.notify();
            }
        }
    }

    /**
     * 
     */
    public void notifyEvent(Event event) {
        logger.debug("PlanExecutionTask, notifyEvent: " + event);

        synchronized (this) {
            this.notify();
        }

    }

    public boolean notifyMessage(ManageabilityAgentMessage message) {
        logger.debug("PlanExecutionTask planid = " + plan.getPlanId() + ", notifyMessage");

        boolean found = false;

        // Look in the ActionExecutionTasks
        Iterator<String> iter = actions.keySet().iterator();
        while (iter.hasNext() && !found) {
            String aux = (String) iter.next();
            ActionExecutionTask task = actions.get(aux);
            found = task.notifyMessage(message);
        }

        // Find the node corresponding to id
        /*
         * org.slasoi.gslam.commons.plan.Task node = findNode(id); if (node != null) { mine = true;
         * node.setStatus(TaskStatus.PROVISIONED_OK.toString());
         * 
         * synchronized (this) { this.notify(); } }
         */

        return found;
    }

    private org.slasoi.gslam.commons.plan.Task findNode(String idStatus) {
        org.slasoi.gslam.commons.plan.Task node = null;
        boolean found = false;

        node = plan.getRootTask(); // List to store the not-yet-expanded nodes
        ArrayList<org.slasoi.gslam.commons.plan.Task> tasks = new ArrayList<org.slasoi.gslam.commons.plan.Task>();
        tasks.add(node);

        org.slasoi.gslam.commons.plan.Task current = null;
        while (tasks.size() > 0 && !found) {
            // Take the first task and deletes it from the list
            current = tasks.get(0);

            logger.info("Node " + current.getTaskId() + " visited");

            if (current.getStatus().equals(idStatus)) {
                found = true;
                node = current;
            }
            else {
                node = null;
                // Expands the node and add the children to the list if it is not already there
                try {
                    Set<org.slasoi.gslam.commons.plan.Task> children = plan.getChildren(current);
                    for (org.slasoi.gslam.commons.plan.Task t : children) {
                        if (!tasks.contains(t))
                            tasks.add(t);
                    }
                }
                catch (TaskNotFoundException e) {
                    e.printStackTrace();
                }

                tasks.remove(current);
            }
        }

        return node;
    }
}
