/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/EventTranslationTask.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.slasoi.gslam.pac.config.TaskConfiguration;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class EventTranslationTask extends Task {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(EventTranslationTask.class.getName());
    private HashMap<String, EventTranslator> listTranslators = new HashMap<String, EventTranslator>();

    public EventTranslationTask() {

    }

    /**
     * @param config
     */
    public EventTranslationTask(TaskConfiguration config) {
        super(config);

    }

    public void configure(TaskConfiguration config) {
        logger.debug("EventTranslationTask::configure");

        // List of parameters is a hash with EventType, EventTranslator class
        HashMap<String, String> translators = config.getParameters();
        for (String name : translators.keySet()) {
            String className = translators.get(name);

            logger.debug("Configuring message type " + name + " for class " + className);

            // Creates the EventTranslator class using RTTI
            Class<?> translatorClass;
            try {
                translatorClass = Class.forName(className);
                EventTranslator translator = (EventTranslator) translatorClass.newInstance();

                listTranslators.put(name, translator);
            }
            catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (InstantiationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

    public EventTranslator getEventTranslator(EventType type) {
        logger.debug("getEventTranslator( " + type.toString() + ")");
        EventTranslator translator = listTranslators.get(type.toString());
        logger.debug("returning " + translator);
        return translator;
    }

    public HashMap<String, EventTranslator> getEventTranslators() {
        return listTranslators;
    }

    public void run() {
        logger.debug("EventTranslationTask running...");
        while (running) {
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                logger.debug("EventTranslationTask. InterruptedException");
                stop();
                return;
            }
        }
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer("Task EventTranslationTask");
        buffer.append("List of events: \n");

        for (String name : listTranslators.keySet())
            buffer.append(name);

        return (buffer.toString());
    }
}
