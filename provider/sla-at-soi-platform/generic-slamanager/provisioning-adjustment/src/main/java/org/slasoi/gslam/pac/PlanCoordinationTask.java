/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 892 $
 * @lastrevision   $Date: 2011-03-07 16:49:07 +0100 (pon, 07 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/PlanCoordinationTask.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.slasoi.gslam.commons.plan.Plan;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment.PlanFoundException;
import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.slasoi.gslam.pac.events.ManageabilityAgentMessage;

/**
 * Task in charge of coordinating the execution of plans provided by the POC. It keeps a in-memory registry of the plans
 * being executed, and creates a PlanExecutionTask for each one.
 * 
 * TODO: add a control on the number of plans that can be executed simultaneously
 * 
 * @author Beatriz Fuentes (TID)
 * 
 */
public class PlanCoordinationTask extends Task {

    private final String CHILDREN_NAME = "PlanExecutionTask";
    private final String MAX_NUMBER_CONCURRENT_PLANS = "MAX_NUMBER_CONCURRENT_PLANS";
    private final int DEFAULT_MAX_NUMBER_CONCURRENT_PLANS = 20;
    private int maximumNumberConcurrentPlans;
    
    private String slamId;

    private HashMap<String, Plan> plans = null;
    private HashMap<String, PlanExecutionTask> tasks = null;
    private Queue<Plan> queuedPlans = null;

    /**
     * @param config
     */
    public PlanCoordinationTask(TaskConfiguration config) {
        super(config);
    	logger.info("Instantiating PlanExecutionTask with the specific constructor...");
        plans = new HashMap<String, Plan>();
        tasks = new HashMap<String, PlanExecutionTask>();
        queuedPlans = new LinkedList<Plan>();
        addEventTypes();
        configure();
        this.slamId = config.getSlamId();
    }

    public PlanCoordinationTask() {
    	super();
    	logger.info("Instantiating PlanExecutionTask with default constructor...");
        plans = new HashMap<String, Plan>();
        tasks = new HashMap<String, PlanExecutionTask>();
        queuedPlans = new LinkedList<Plan>();
        addEventTypes();
    }
    
    public void setSLAMId(String slamId) {
    	this.slamId = slamId;
    }

    private void addEventTypes() {
        eventTypes.add(EventType.PlanExecutionRequest);
        eventTypes.add(EventType.PlanCancelationRequest);
        eventTypes.add(EventType.ManageabilityAgentMessage);
        eventTypes.add(EventType.PlanExecutionRequest);
    }

    /**
     * 
     * 
     * 
     */
    public void configure() {
        logger.debug("PlanCoordinationTask::configure");
        // Sets the maximum number of plans that can be executed simultaneously
        String maxPlans = config.getParameters().get(MAX_NUMBER_CONCURRENT_PLANS);

        if (maxPlans == null) {
            // No parameter defined, stick to the default value
            setMaximumNumberConcurrentPlans(DEFAULT_MAX_NUMBER_CONCURRENT_PLANS);
        }
        else {
            Integer aux = new Integer(maxPlans);
            setMaximumNumberConcurrentPlans(aux.intValue());
        }

    }

    public void execute(Plan plan) throws ProvisioningAdjustment.PlanFoundException {
        logger.debug("PlanCoordinationTask, execute plan");
        logger.debug(plan);

        // check if the plan already exists
        String planId = plan.getPlanId();
        if (containsPlan(planId))
            throw new ProvisioningAdjustment.PlanFoundException();

        // checks if the plan can be executed now
        if (canExecuteNewPlan()) {
            // creates a new PlanExecutionTask
            // with name "PlanExecutionTask", id = planId and own config parameters
            TaskConfiguration conf = new TaskConfiguration(CHILDREN_NAME, planId, config.getParameters(), this.slamId);
            PlanExecutionTask task = new PlanExecutionTask(conf, plan);
            task.setAgent(agent);
            task.setParent(this);
            plans.put(planId, plan);
            tasks.put(planId, task);

            task.start();
        }
        else {
            // maximum number of concurrent plans has been reached. Add to queue
            queuedPlans.add(plan);
        }
    }

    private boolean containsPlan(String planId) {
        boolean found = false;

        // first, check the plan currently in execution
        found = plans.containsKey(planId);

        if (!found) {
            // check plans in queue
            Iterator<Plan> iter = queuedPlans.iterator();
            while (iter.hasNext() && !found) {
                Plan aux = (Plan) iter.next();
                found = (aux.getPlanId() == planId);
            }
        }
        return found;
    }

    private boolean canExecuteNewPlan() {
        // checks if the maximum number of concurrent plans has been reached
        logger.debug("can execute new plan? ");
        logger.debug("number of plans in execution " + plans.size());
        logger.debug("maximum number of concurrent plans " + maximumNumberConcurrentPlans);

        boolean newplan = false;
        if (plans.size() < maximumNumberConcurrentPlans)
            newplan = true;

        return (newplan);
    }

    public void setMaximumNumberConcurrentPlans(int maximumNumberConcurrentPlans) {
        this.maximumNumberConcurrentPlans = maximumNumberConcurrentPlans;
    }

    public int getMaximumNumberConcurrentPlans() {
        return maximumNumberConcurrentPlans;
    }

    /**
     * process an event sent by another agent to the agent owning this task
     * 
     * @param event
     */
    public void notifyEvent(Event event) {

        // if it is a PlanExecutionRequest, then the content of the event is a plan
        logger.debug("PlanCoordinationTask, notifyEvent");
        if (event.getType() == EventType.PlanExecutionRequest) {
            Object object = event.getEventContent();
            if (object instanceof Plan) {
                try {
                    execute((Plan) object);
                }
                catch (PlanFoundException e) {
                    // TODO: look how to handle exceptions
                    e.printStackTrace();
                }
            }
        }
        else if (event.getType() == EventType.PlanCancelationRequest) {
            Object planId = event.getEventContent();
            if (planId instanceof String) {
                Task target = tasks.get(planId);
                if (target != null)
                    target.stop();
            }
        }
        else if (event.getType() == EventType.ManageabilityAgentMessage) {
            // Find plan and node for this message
            boolean found = false;
            Iterator<String> iter = tasks.keySet().iterator();
            while (iter.hasNext() && !found) {
                String aux = (String) iter.next();
                PlanExecutionTask task = tasks.get(aux);
                found = task.notifyMessage((ManageabilityAgentMessage) event.getEventContent());
            }

        }
        else {
            logger.debug("Unexpected event type");
        }
    }

    /**
     * A child task (PlanExecutionTask) notifies the finalization of the job
     */
    public void notify(Task child) {
        // Delete task and plan from lists
        String planId = ((PlanExecutionTask) child).getPlanId();
        plans.remove(planId);
        tasks.remove(planId);
        logger.debug("PlanCoordinationTask, plan " + planId + " completed");
    }

    public Event query(Event event) {
        Event result = null;

        if (event.getType().equals(EventType.PlanExecutionRequest)) {
            Task target = tasks.get(event.getEventContent());
            Status status;
            if (target != null) {
                // Still being provisioned
                status = Status.PROVISIONING;
            }
            else {
                // TODO: query database to retrieve old plan data. If not there, then plan hasn't ever exist
                status = Status.PROVISIONED_OK;
            }

            result = new Event(EventType.PlanStatusResponse, Identifier.PlanCoordinationTask, status);
        }

        return result;
    }
}
