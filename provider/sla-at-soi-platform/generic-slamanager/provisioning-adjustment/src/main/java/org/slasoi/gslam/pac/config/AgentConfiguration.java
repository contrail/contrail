/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/config/AgentConfiguration.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac.config;

import java.util.ArrayList;
import java.util.List;

/**
 * Configuration for an agent: name and list of task configurations.
 * 
 * @author Beatriz Fuentes
 * 
 */

public class AgentConfiguration {
    /**
     * Agent identifier
     */
    private String agentId;

    /**
     * List of task configuration attached to this agent
     */
    private List<TaskConfiguration> tasks = new ArrayList<TaskConfiguration>();

    /**
     * Set agent id
     * 
     * @param name
     *            Agent id
     */
    public void setAgentId(String id) {
        this.agentId = id;
    }

    /**
     * Get agent id
     * 
     * @return agent id
     */
    public String getAgentId() {
        return (agentId);
    }

    /**
     * Set the task configuration list.
     * 
     * @param tasks
     *            the task configuration list to set
     */
    public final void setTaskConfigurationList(final List<TaskConfiguration> tasks) {
        this.tasks = tasks;
    }

    /**
     * Get the task configuration list.
     * 
     * @return the task configuration list
     */
    public final List<TaskConfiguration> getTaskConfigurationList() {
        return tasks;
    }

    /**
     * Add a task configuration object
     * 
     * @param task
     *            The task configuration object
     */
    public final void addTaskConfiguration(final TaskConfiguration task) {
        tasks.add(task);
    }

    /**
     * Print the agent name and task configuration list to a string.
     * 
     * @return The agent configuration information in a string.
     */
    public final String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Agent configuration object\n");
        sb.append("Agent id: " + agentId + "\n");
        sb.append("Task Configuration: " + "\n");
        for (TaskConfiguration task : tasks) {
            sb.append(task.toString() + "\n");
        }
        return sb.toString();
    }

}
