/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/MonitoringEventMessageTranslator.java $
 */

package org.slasoi.gslam.pac;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.ObjectFactory;
import org.slasoi.gslam.pac.events.Message;
import org.slasoi.gslam.pac.events.MonitoringEventMessage;

public class MonitoringEventMessageTranslator extends EventTranslator {
    private static final Logger logger = Logger.getLogger(MonitoringEventMessageTranslator.class.getName());

    private static final String CONTEXT_NAME = "org.slasoi.common.monitoringevent";
    private static JAXBContext context;
    private static Unmarshaller unmarshaller;
    private static Marshaller marshaller;

    public MonitoringEventMessageTranslator() {
        try {
            // ClassLoader cl = org.slasoi.common.monitoringevent.ObjectFactory.class.getClassLoader();
            // context = JAXBContext.newInstance(CONTEXT_NAME, cl);
            context = JAXBContext.newInstance(org.slasoi.common.eventschema.EventInstance.class);
            unmarshaller = context.createUnmarshaller();
            marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        }
        catch (JAXBException e) {
            // // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public Message fromXML(String messageStr) {

        logger.debug("MonitoringEventMessage from XML");
        logger.debug(messageStr);

        JAXBElement<EventInstance> event = null;
        try {
            // context = JAXBContext.newInstance(CONTEXT_NAME);
            // unmarshaller = context.createUnmarshaller();
            // marshaller = context.createMarshaller();
            // marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            event = (JAXBElement<EventInstance>) unmarshaller.unmarshal(new StringReader(messageStr));
        }
        catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        EventInstance instance = event.getValue();
        MonitoringEventMessage message = new MonitoringEventMessage(instance);

        return (message);
    }

    public String toXML(Message message) {
        logger.debug("MonitoringEventMessage to XML");

        String messageStr = null;

        if (message instanceof MonitoringEventMessage) {
            EventInstance event = ((MonitoringEventMessage) message).getEventInstance();
            JAXBElement<EventInstance> instance = (new ObjectFactory()).createEvent(event);

            StringWriter output = new StringWriter();
            try {
                marshaller.marshal(instance, output);
            }
            catch (JAXBException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            messageStr = output.toString();
        }

        logger.debug(messageStr);

        return (messageStr);
    }

}
