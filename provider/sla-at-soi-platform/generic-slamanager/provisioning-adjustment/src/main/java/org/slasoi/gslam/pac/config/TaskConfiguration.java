/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 892 $
 * @lastrevision   $Date: 2011-03-07 16:49:07 +0100 (pon, 07 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/config/TaskConfiguration.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac.config;

import java.util.HashMap;

/**
 * @author Beatriz Fuentes
 * 
 */
public class TaskConfiguration {
    /**
     * The task identifier
     */
    private String taskId;

    /**
     * The task name
     */
    private String className;

    /**
     * The task configuration parameters
     */
    private HashMap<String, String> parameters = new HashMap<String, String>();
    
    private String slamId;

    public TaskConfiguration() {
        // needed for RTTI (xstream) under pax-runner??
    }

    /**
     * Constructor
     */
    public TaskConfiguration(String taskId, String className, HashMap<String, String> parameters, String slamId) {
        this.taskId = taskId;
        this.className = className;
        this.parameters = parameters;
        this.slamId = slamId;
    }

    /**
     * Copy constructor
     */
    public TaskConfiguration(TaskConfiguration config, String slamId) {
        taskId = config.getTaskId();
        className = config.getClassName();
        parameters = config.getParameters();
        this.slamId = slamId;
    }

    /**
     * Set task id
     * 
     * @param id
     *            Task identifier
     */
    public void setTaskId(String id) {
        this.taskId = id;
    }

    /**
     * Get task id
     * 
     * @return task id
     */
    public String getTaskId() {
        return (taskId);
    }

    /**
     * Set task name
     * 
     * @param name
     *            Task name
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * Get task name
     * 
     * @return task name
     */
    public String getClassName() {
        return (className);
    }

    /**
     * Set the task configuration parameters.
     * 
     * @param parameters
     *            the parameters to set
     */
    public final void setParameters(final HashMap<String, String> parameters) {
        this.parameters = parameters;
    }

    /**
     * Get the task configuration parameters.
     * 
     * @return the parameters
     */
    public final HashMap<String, String> getParameters() {
        return parameters;
    }

    /**
     * Return the value of the parameter. Return an empty string if not found.
     * 
     * @param name
     *            the parameter name.
     * @return the value of the parameter with the given name
     */

    public final String getValue(final String name) {
        String value = "";
        if (parameters.containsKey(name)) {
            value = parameters.get(name);
        }
        return value;
    }

    /**
     * Set the value of a parameter. If a parameter already exists, overwrites it.
     * 
     * @param name
     *            The name of the parameter
     * @param value
     *            The value of the parameter
     */
    public final void setValue(final String name, final String value) {
        if (parameters.containsKey(name)) {
            parameters.remove(name);
        }

        parameters.put(name, value);
    }

    /**
     * Print the task name and task configuration parameters to a string.
     * 
     * @return The task configuration information in a string.
     */
    public final String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("Task class name: " + className + "\n");
        sb.append("Task id: " + taskId + "\n");
        sb.append("Task Configuration Parameters: " + "\n");
        if (parameters != null) {
            for (String name : parameters.keySet())
                sb.append("\r\n\tParameter " + name + " = " + parameters.get(name) + "\n");
        }
        return sb.toString();
    }
    
    public String getSlamId() {
    	return this.slamId;
    }

}
