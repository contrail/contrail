/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 892 $
 * @lastrevision   $Date: 2011-03-07 16:49:07 +0100 (pon, 07 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/AnalysisTask.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.drools.builder.ResourceType;
//import org.ejml.data.SimpleMatrix;
import org.slasoi.gslam.pac.config.TaskConfiguration;
//import org.slasoi.gslam.pac.ml.MLP;
//import org.slasoi.gslam.pac.ml.NFP;
//import org.slasoi.gslam.pac.ml.NeuralOutputs;
//import org.slasoi.gslam.pac.ml.ServiceState;
//import org.slasoi.gslam.pac.ml.ServiceStateFactory;
//import org.slasoi.gslam.pac.ml.Weights;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class AnalysisTask extends Task {
	
	protected String slamId;

	protected static final String RULES_FILE = "RULES_FILE";
	protected String rulesFile = null;

//	protected ServiceStateFactory ssf = null;

	// how many values should be regarded for training the network
	protected final static int TRAINING_WINDOW_SIZE = 100;
	// how many steps into the future are we trying to predict?
	protected final static int DELTA_T = 1;
	// accumulated service states
//	protected List<ServiceState> serviceStateHistory = new ArrayList<ServiceState>(
//			TRAINING_WINDOW_SIZE);
	// current weights for the MLP
//	protected Weights weights;

	/*
	 * default constructor for RTTI
	 */
	public AnalysisTask(String slamId) {
		eventTypes.add(EventType.MonitoringEventMessage);
		this.slamId = slamId;
	}

	/**
	 * @param config
	 */
	public AnalysisTask(TaskConfiguration config) {
		super(config);
		eventTypes.add(EventType.MonitoringEventMessage);
		this.slamId = config.getSlamId();
	}

	public void configure(TaskConfiguration config) {
		logger.debug("AnalysisTask::configure");

		rulesFile = config.getValue(RULES_FILE);

		if (rulesFile != null) {
			logger.debug("Configuring rules file: " + rulesFile);
			SharedKnowledgePlane.getInstance(this.slamId).addRulesFile(rulesFile,
					ResourceType.DRL);
		}
	}

	public void run() {
		logger.debug("AnalysisTask running...");
		while (running) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.debug("AnalysisTask. InterruptedException");
				stop();
				return;
			}
		}
	}

	public void notifyEvent(Event event) {
//		if (event.getType() == EventType.MonitoringEventMessage) {
//			// parse the event for the measured qos term and for the source VM
//			ServiceState serviceState = this.ssf.addNFP(extractNFP(event));
//			// TODO extend this for an arbitrary number of VMs
//			if (serviceState != null) {
//				if (this.serviceStateHistory.size() < TRAINING_WINDOW_SIZE) {
//					this.serviceStateHistory.add(serviceState);
//				}
//				if (this.serviceStateHistory.size() == TRAINING_WINDOW_SIZE) {
//					// TODO train the network
//					// TODO do it asynchronously
//
//					SimpleMatrix[] trainingData = preprocessTrainingData();
//					SimpleMatrix X = trainingData[0];
//					SimpleMatrix T = trainingData[1];
//
//					this.weights = MLP.trainNetwork(1000, 10, 0.0001, 0.0001,
//							X, T);
//				}
//				// TODO get a prediction from the network
//				// extract the last state
//				SimpleMatrix lastState = extractTargetVector(this.serviceStateHistory.get(this.serviceStateHistory.size() - 1).getNFPs());
//				NeuralOutputs predictedState = MLP.computeOutputs(lastState, weights.getWeightsLayer1(), weights.getWeightsLayer2());
//				// inject the predicted state into Drools and let it take appropriate actions
//				SharedKnowledgePlane.getInstance("IPAC").getStatefulSession().insert(predictedState);
//			}
//		}
		System.out.println(event.toString());
		SharedKnowledgePlane.getInstance(this.slamId).getStatefulSession().insert(event);
		SharedKnowledgePlane.getInstance(this.slamId).getStatefulSession().fireAllRules();
	}

	// TODO overload this method in subclass
//	protected NFP extractNFP(Event event) {
//		// TODO implement once the monitoring messages are available
//		return null;
//	}
//
//	protected SimpleMatrix[] preprocessTrainingData() {
//		int distinctNFPs = this.serviceStateHistory.get(0).getNFPs().size();
//
//		// prepare the input vector
//		// all the service states and the changes for all of them
//		SimpleMatrix X = new SimpleMatrix(2 * distinctNFPs,
//				TRAINING_WINDOW_SIZE);
//
//		// prepare the target vector
//		// all of the service states
//		SimpleMatrix T = new SimpleMatrix(distinctNFPs, TRAINING_WINDOW_SIZE);
//
//		// iterate over the gathered data starting with the second value
//		// TODO allow for variable step size
//		for (int i = 2; i < this.serviceStateHistory.size(); i++) {
//			Set<NFP> nfPsGradientInput = this.serviceStateHistory.get(i-2).getNFPs();
//			Set<NFP> nfPsInput = this.serviceStateHistory.get(i-1).getNFPs();
//			SimpleMatrix x = extractInputVector(nfPsInput, nfPsGradientInput);
//			Set<NFP> nfPsTarget = this.serviceStateHistory.get(i).getNFPs();
//			SimpleMatrix t = extractTargetVector(nfPsTarget);
//			X.insertIntoThis(0, i, x);
//			T.insertIntoThis(0, i, t);
//			i++;
//		}
//
//		SimpleMatrix[] trainingData = new SimpleMatrix[2];
//		trainingData[0] = X;
//		trainingData[1] = T;
//		return trainingData;
//	}
//
//
//	// TODO the following method needs to be overloaded by the domain specific
//	// adjustment component
//	private SimpleMatrix extractTargetVector(Set<NFP> nfPs) {
//		return new SimpleMatrix(nfPs.size(), 1);
//	}
//
//	// TODO the following method needs to be overloaded by the domain specific
//	// adjustment component
//	protected SimpleMatrix extractInputVector(Set<NFP> nfPsInput, Set<NFP> nfPsGradientInput) {
//		return new SimpleMatrix(2 * nfPsInput.size(), 1);
//	}
}
