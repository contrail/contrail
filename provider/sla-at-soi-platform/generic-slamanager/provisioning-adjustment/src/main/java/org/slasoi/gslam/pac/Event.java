/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/Event.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.sql.Timestamp;

/**
 * Event to be used in the communication between agents.
 * 
 * @author Beatriz Fuentes
 * 
 */
public class Event {
    /**
     * Type of the event: PlanExecutionRequest, PlanCancelationRequest...
     */
    private EventType type;

    /**
     * id of the sender of the event
     */
    private Identifier sender;

    private Identifier recipient;

    private Timestamp generationDate;

    private EventStatus status;

    /**
     * content of the event: it can be a Plan to be executed, an id of the plan to be cancelled, a notification...
     */
    private Object eventContent;

    /**
     * Constructor
     * 
     * @param type
     *            type of the event
     * @param sender
     *            agent that sends the event
     * @param eventContent
     *            actual content of the event. Its actual type depends on the event type.
     */

    public Event(EventType type, Identifier sender, Object eventContent) {
        this.setType(type);
        this.setSender(sender);
        this.setEventContent(eventContent);
        this.status = EventStatus.CREATED;
        setGenerationDate(new Timestamp(System.currentTimeMillis()));
    }

    public Event(EventType type, Identifier sender, Identifier recipient, Object eventContent) {
        this.setType(type);
        this.setSender(sender);
        this.setRecipient(recipient);
        this.setEventContent(eventContent);
        this.status = EventStatus.CREATED;
        setGenerationDate(new Timestamp(System.currentTimeMillis()));
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public EventType getType() {
        return type;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    public EventStatus getStatus() {
        return status;
    }

    public void setSender(Identifier sender) {
        this.sender = sender;
    }

    public Identifier getSender() {
        return sender;
    }

    public void setGenerationDate(Timestamp generationDate) {
        this.generationDate = generationDate;
    }

    public Timestamp getGenerationDate() {
        return generationDate;
    }

    public void setEventContent(Object eventContent) {
        this.eventContent = eventContent;
    }

    public Object getEventContent() {
        return eventContent;
    }

    public Identifier getRecipient() {
        return recipient;
    }

    public void setRecipient(Identifier recipient) {
        this.recipient = recipient;
    }

    public String toString() {
        String output = "Type: " + type + "\n";
        output = output + "Sender: " + sender + "\n";
        output = output + "Status: " + status + "\n";
        return output;
    }

}
