package org.slasoi.isslam.poc.test.provision;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment.Plan;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment.PlanFormatException;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment.PlanFoundException;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.ism.occi.IsmOcciServiceImpl;
import org.slasoi.isslam.pac.InfrastructureProvisioningAdjustment;
import org.slasoi.isslam.poc.impl.PlanningOptimizationImpl;
import org.slasoi.isslam.poc.planhandler.PlanHandler;
import org.slasoi.isslam.poc.planhandler.exceptions.InvalidSLAFormatException;
import org.slasoi.isslam.poc.planhandler.impl.PlanHandlerImpl;
import org.slasoi.isslam.poc.runner.impl.RunnerImpl;
import org.slasoi.isslam.poc.servicesmanager.impl.ServiceManagerHandlerImpl;
import org.slasoi.isslam.poc.test.negotiation.NegotiationTest;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

import junit.framework.TestCase;

/**
 * This class is for testing the functionalities of provision.
 * 
 * @author Kuan Lu
 * 
 */
public class ProvisionTest extends TestCase {

    private static final Logger LOGGER = Logger.getLogger(NegotiationTest.class);

    /**
     * Tests the runner.
     */
    public void testRunner() {
        InfrastructureProvisioningAdjustment pac = new InfrastructureProvisioningAdjustment();
        LOGGER.info(ServiceManagerHandlerImpl.getInstance().getInfraServiceManager());
        FileReader read;
        try {

            // SLA With one 1 vm, wrong version (null feedback from ISM, means we can't fulfill the request)

            {
                LOGGER
                        .info("***SLA With one 1 vm, wrong version (null feedback from ISM, means we can't fulfill the request)***");
                read =
                        new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                                + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                                + "A4_SLA-1-VM-Correct.xml");
                BufferedReader br = new BufferedReader(read);
                StringBuffer sb = new StringBuffer();
                String row;
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                SLASOIParser slasoiParser = new SLASOIParser();
                SLA sla = slasoiParser.parseSLA(sb.toString());
                assertNotNull(sla);
                try {
                    PlanHandler planHandler;
                    planHandler = new PlanHandlerImpl(sla);
                    Plan plan = planHandler.planMaker();
                    assertNotNull(plan);

                }
                catch (InvalidSLAFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
