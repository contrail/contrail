package org.slasoi.isslam.poc.test.negotiation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.isslam.poc.runner.impl.RunnerImpl;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

import junit.framework.TestCase;

/**
 * This class is for testing the functionalities of runner.
 * 
 * @author Kuan Lu
 * 
 */
public class NegotiationTest extends TestCase {
    private static final Logger LOGGER = Logger.getLogger(NegotiationTest.class);
    private SLATemplate iTemplate;

    /**
     * Tests the runner.
     */
    public void testRunner() {
        FileReader read;
        try {
            // SLAT With one 1 vm, correct version (not null feedback from ISM, means we can fulfill the request)
            LOGGER
                    .info("***SLA With one 1 vm, correct version (not null feedback from ISM, means we can fulfill the request)***");
            /*read =
                    new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                            + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                            + "A4_SLATemplate_1_VM_1.xml");*/
            
            read =
                new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                        + File.separator + "planning-optimization" + File.separator + "A4" + File.separator+"Chris"+ File.separator
                        + "ORCInfrastructure_SLATemplateA4Final.xml");

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }
            
            SLASOITemplateParser slasoieTemplatParser = new SLASOITemplateParser();
            //SLATemplate slat;
            iTemplate = slasoieTemplatParser.parseTemplate(sb.toString());            
            
            //this.initializeCustomerRequest();

            
            RunnerImpl run = new RunnerImpl(iTemplate, null);
            assertNotNull(run.run());

            // SLA With one 1 vm, wrong version (null feedback from ISM, means we can't fulfill the request)

            {
                LOGGER
                        .info("***SLA With one 1 vm, wrong version (null feedback from ISM, means we can't fulfill the request)***");
                read =
                        new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                                + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                                + "A4_SLA-1-VM-Wrong.xml");
                br = new BufferedReader(read);
                sb = new StringBuffer();
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                SLASOIParser slasoiParser = new SLASOIParser();
                SLA sla = slasoiParser.parseSLA(sb.toString());
                run = new RunnerImpl(sla, null);
                assertNotNull(run.run());
            }

            // SLA With one 2 vm, correct version (not null feedback from ISM, means we can fulfill the request)

            {
                LOGGER
                        .info("***SLA With one 2 vm, correct version (not null feedback from ISM, means we can fulfill the request)***");
                read =
                        new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                                + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                                + "A4_SLA-2-VM-Correct.xml");
                br = new BufferedReader(read);
                sb = new StringBuffer();
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                SLASOIParser slasoiParser = new SLASOIParser();
                SLA sla = slasoiParser.parseSLA(sb.toString());
                run = new RunnerImpl(sla, null);
                assertNotNull(run.run());
            }

            // SLA With one 2 vm, wrong version (null feedback from ISM, means we can't fulfill the request)

            {
                LOGGER
                        .info("***SLA With one 2 vm, wrong version (null feedback from ISM, means we can't fulfill the request)***");
                read =
                        new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                                + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                                + "A4_SLA-2-VM-Wrong.xml");
                br = new BufferedReader(read);
                sb = new StringBuffer();
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                SLASOIParser slasoiParser = new SLASOIParser();
                SLA sla = slasoiParser.parseSLA(sb.toString());
                run = new RunnerImpl(sla, null);
                assertNotNull(run.run());
            }

            // SLA With one 2 vm, wrong version (null feedback from ISM, means we can't fulfill the request), no SLAT
            // contains required VM types.

            {
                LOGGER
                        .info("***SLA With one 2 vm, wrong version (null feedback from ISM, means we can't fulfill the request)***");
                read =
                        new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                                + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                                + "A4_SLA-2-VM-Wrong-No-SLAT.xml");
                br = new BufferedReader(read);
                sb = new StringBuffer();
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                SLASOIParser slasoiParser = new SLASOIParser();
                SLA sla = slasoiParser.parseSLA(sb.toString());
                run = new RunnerImpl(sla, null);
                assertNotNull(run.run());
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    
    public void initializeCustomerRequest()
    {
        Hashtable<String, String> params = new Hashtable<String, String>();
        
        params.put( "VMTYPES", "VM_DEVELOPERS,VM_DEVELOPERS_LOW,VM_OFFICE,VM_DESIGN,VM_RESEARCH");
        //VM_DEVELOPERS
        params.put( "VM_DEVELOPERS.VM_QUANTITY_VAR", "5" );
        params.put( "VM_DEVELOPERS.VM_CORES_VAR", "2" );
        params.put( "VM_DEVELOPERS.VM_CPU_SPEED_VAR", "2.8" );
        params.put( "VM_DEVELOPERS.VM_MEMORY_SIZE_VAR", "1024" );
        params.put( "VM_DEVELOPERS.VM_HARDDISK_SIZE_VAR", "11" );
        params.put( "VM_DEVELOPERS.START_TIME_VAR", "22/02/2011 12:00" );
        params.put( "VM_DEVELOPERS.END_TIME_VAR"  , "22/02/2011 18:00" );
        params.put( "VM_DEVELOPERS.VM_ISOLATION_VAR", "true" );
        params.put( "VM_DEVELOPERS.VM_PERSISTENCE_VAR", "false" );
        params.put( "VM_DEVELOPERS.VM_IMAGE_VAR", "http://my.image.VM_DEVELOPERS" );
        //VM_DEVELOPERS_LOW
        params.put( "VM_DEVELOPERS_LOW.VM_QUANTITY_VAR", "2" );
        params.put( "VM_DEVELOPERS_LOW.VM_CORES_VAR", "1" );
        params.put( "VM_DEVELOPERS_LOW.VM_CPU_SPEED_VAR", "2.6" );
        params.put( "VM_DEVELOPERS_LOW.VM_MEMORY_SIZE_VAR", "512" );
        params.put( "VM_DEVELOPERS_LOW.VM_HARDDISK_SIZE_VAR", "6" );
        params.put( "VM_DEVELOPERS_LOW.START_TIME_VAR", "22/02/2011 12:00" );
        params.put( "VM_DEVELOPERS_LOW.END_TIME_VAR"  , "22/02/2011 18:00" );
        params.put( "VM_DEVELOPERS_LOW.VM_ISOLATION_VAR", "false" );
        params.put( "VM_DEVELOPERS_LOW.VM_PERSISTENCE_VAR", "false" );
        params.put( "VM_DEVELOPERS_LOW.VM_IMAGE_VAR", "http://my.image.VM_DEVELOPERS_LOW" );
        //VM_OFFICE
        params.put( "VM_OFFICE.VM_QUANTITY_VAR", "1" );
        params.put( "VM_OFFICE.VM_CORES_VAR", "3" );
        params.put( "VM_OFFICE.VM_CPU_SPEED_VAR", "3.0" );
        params.put( "VM_OFFICE.VM_MEMORY_SIZE_VAR", "2048" );
        params.put( "VM_OFFICE.VM_HARDDISK_SIZE_VAR", "20" );
        params.put( "VM_OFFICE.START_TIME_VAR", "22/02/2011 12:00" );
        params.put( "VM_OFFICE.END_TIME_VAR"  , "22/02/2011 18:00" );
        params.put( "VM_OFFICE.VM_ISOLATION_VAR", "true" );
        params.put( "VM_OFFICE.VM_PERSISTENCE_VAR", "true" );
        params.put( "VM_OFFICE.VM_IMAGE_VAR", "http://my.image.VM_OFFICE" );
        //VM_DESIGN
        params.put( "VM_DESIGN.VM_QUANTITY_VAR", "0" );
        params.put( "VM_DESIGN.VM_CORES_VAR", "2" );
        params.put( "VM_DESIGN.VM_CPU_SPEED_VAR", "2.8" );
        params.put( "VM_DESIGN.VM_MEMORY_SIZE_VAR", "1024" );
        params.put( "VM_DESIGN.VM_HARDDISK_SIZE_VAR", "11" );
        params.put( "VM_DESIGN.START_TIME_VAR", "22/02/2011 12:00" );
        params.put( "VM_DESIGN.END_TIME_VAR"  , "22/02/2011 18:00" );
        params.put( "VM_DESIGN.VM_ISOLATION_VAR", "true" );
        params.put( "VM_DESIGN.VM_PERSISTENCE_VAR", "false" );
        params.put( "VM_DESIGN.VM_IMAGE_VAR", "http://my.image.VM_DESIGN" );
        //VM_RESEARCH
        params.put( "VM_RESEARCH.VM_QUANTITY_VAR", "1" );
        params.put( "VM_RESEARCH.VM_CORES_VAR", "2" );
        params.put( "VM_RESEARCH.VM_CPU_SPEED_VAR", "2.8" );
        params.put( "VM_RESEARCH.VM_MEMORY_SIZE_VAR", "1024" );
        params.put( "VM_RESEARCH.VM_HARDDISK_SIZE_VAR", "11" );
        params.put( "VM_RESEARCH.START_TIME_VAR", "22/02/2011 12:00" );
        params.put( "VM_RESEARCH.END_TIME_VAR"  , "22/02/2011 18:00" );
        params.put( "VM_RESEARCH.VM_ISOLATION_VAR", "true" );
        params.put( "VM_RESEARCH.VM_PERSISTENCE_VAR", "false" );
        params.put( "VM_RESEARCH.VM_IMAGE_VAR", "http://my.image.VM_RESEARCH" );
        
        this.setCustomerRequest(params);
    }
    
    public SLATemplate setCustomerRequest( Hashtable<String, String> params )
    {
                
        String vms[] = params.get( "VMTYPES" ).split( "," );
        
        for( String vm : vms )
        {
            update( vm, params );
        }

        return iTemplate;
    }
    
    protected void update( String vm, Hashtable<String, String> params )
    {
        AgreementTerm agreementTerm = iTemplate.getAgreementTerm( vm );
        if(agreementTerm!=null){
            ((Customisable)agreementTerm.getVariableDeclr( "VM_QUANTITY_VAR" )).getValue().setValue( params.get( vm + ".VM_QUANTITY_VAR" ) );
            ((Customisable)agreementTerm.getVariableDeclr( "VM_CORES_VAR" )).getValue().setValue(params.get( vm + ".VM_CORES_VAR" ));
            ((Customisable)agreementTerm.getVariableDeclr( "VM_CPU_SPEED_VAR" )).getValue().setValue(params.get( vm + ".VM_CPU_SPEED_VAR" ));
            ((Customisable)agreementTerm.getVariableDeclr( "VM_MEMORY_SIZE_VAR" )).getValue().setValue(params.get( vm + ".VM_MEMORY_SIZE_VAR" ));
            ((Customisable)agreementTerm.getVariableDeclr( "VM_HARDDISK_SIZE_VAR" )).getValue().setValue(params.get( vm + ".VM_HARDDISK_SIZE_VAR" ));
            ((Customisable)agreementTerm.getVariableDeclr( "START_TIME_VAR" )).getValue().setValue(params.get( vm + ".START_TIME_VAR" ));
            ((Customisable)agreementTerm.getVariableDeclr( "END_TIME_VAR" )).getValue().setValue(params.get( vm + ".END_TIME_VAR" ));
            ((Customisable)agreementTerm.getVariableDeclr( "VM_ISOLATION_VAR" )).getValue().setValue(params.get( vm + ".VM_ISOLATION_VAR" ));
            ((Customisable)agreementTerm.getVariableDeclr( "VM_PERSISTENCE_VAR" )).getValue().setValue(params.get( vm + ".VM_PERSISTENCE_VAR" ));
            ((Customisable)agreementTerm.getVariableDeclr( "VM_IMAGE_VAR" )).getValue().setValue(params.get( vm + ".VM_IMAGE_VAR" ));
        }
        else System.out.println("Unsupported VM Type :: "+vm);
    }
}
