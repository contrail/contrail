package org.slasoi.isslam.poc.test.monitorconfig;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import junit.framework.TestCase;

import org.slasoi.gslam.monitoring.manager.impl.MonitoringManager;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.isslam.poc.servicesmanager.impl.EverestTestMonitoringFeatures;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.slasoi.monitoring.common.features.impl.FeaturesFactoryImpl;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;
import org.slasoi.slamodel.vocab.units;

public class MonitoringConfigTest extends TestCase {
    SLA sla;

    public void testSlaCreation() {

        // generating SLA
        FileReader read;

        try {
            read =
                    new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                            + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                            + "A4_SLA-1-VM-Correct.xml");

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }

            SLASOIParser slasoiParser = new SLASOIParser();
            // SLATemplate slat;
            sla = slasoiParser.parseSLA(sb.toString());
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void testGetMonitoringConfig() {
        // generate the features
        this.testSlaCreation();
        ComponentMonitoringFeatures[] features = this.buildTest();
        MonitoringManager mm = new MonitoringManager();
        assertNotNull(mm.checkMonitorability(sla, features));

    }

    /**
     * Constructs a set of ComponentMonitoringFeatures.
     * 
     * @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures[]
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public ComponentMonitoringFeatures[] buildTest() {
        FeaturesFactoryImpl ffi = new FeaturesFactoryImpl();
        ComponentMonitoringFeatures[] cmfeatures = new ComponentMonitoringFeatures[2];
        EverestTestMonitoringFeatures everestFeatures = new EverestTestMonitoringFeatures();
        cmfeatures = everestFeatures.buildTest();

        try {
            // ////////////////////////////////////////////////////////////////////////////////
            // Extension component for B4 Sensors
            // ////////////////////////////////////////////////////////////////////////////////
            cmfeatures[2] = ffi.createComponentMonitoringFeatures();
            cmfeatures[2].setUuid("555e8400-sss2-41d4-a716-406075043333");
            cmfeatures[2].setType("SENSOR");

            LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

            // ////////////////////////////////////////////////////////////////////////////////
            // B4 Extension Features
            // ////////////////////////////////////////////////////////////////////////////////
            String type = "";
            type = "http://www.slaatsoi.org/commonTerms#persistence";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#memory";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#cpu_speed";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#vm_cores";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            /*
             * type = common.$location; mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
             * //disabled in order to compile, because $location was removed from common
             */
            type = common.$isolation;
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#vm_image";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "http://www.slaatsoi.org/commonTerms#VM_Access_Point";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "RESPONSE"));
            type = "autogen/VM_X";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "RESPONSE"));
            type = "REQUEST";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
            type = "RESPONSE";
            mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));

            // greater_than reasoners and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$greater_than,
                    "greater_than xsd:integer Reasoner", "xsd:integer", "xsd:integer", meta.$BOOLEAN));
            // isa reasoner and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$isa, "isa xsd:boolean Reasoner",
                    "xsd:boolean", "xsd:boolean", meta.$BOOLEAN));
            // member_of reasoner and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$member_of, "member_of xsd:string Reasoner",
                    "xsd:string", "xsd:string", meta.$BOOLEAN));
            // member_of reasoner and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$member_of, "member_of units#GHz Reasoner",
                    units.$GHz, units.$GHz, meta.$BOOLEAN));
            // member_of reasoner and primitives
            mflist.add(EverestTestMonitoringFeatures.buildFunction(core.$member_of, "member_of units#GHz Reasoner",
                    "xsd:anyURI", "xsd:anyURI", meta.$BOOLEAN));

            cmfeatures[2].setMonitoringFeatures(EverestTestMonitoringFeatures.mfListToArray(mflist));

        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        return cmfeatures;
    }

}
