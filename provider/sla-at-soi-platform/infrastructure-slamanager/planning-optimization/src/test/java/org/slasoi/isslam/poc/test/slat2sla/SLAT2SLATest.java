package org.slasoi.isslam.poc.test.slat2sla;

import org.slasoi.isslam.poc.slat2sla.impl.SLAT2SLAImpl;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import junit.framework.TestCase;

public class SLAT2SLATest extends TestCase {
    /**
     * This class is for testing the transformation from SLA template to be SLA.
     * 
     * @author Kuan Lu
     * 
     */
    public void testSLAT2SLATest() {
        SLATemplate slat = new SLATemplate();
        SLAT2SLAImpl slat2slat = new SLAT2SLAImpl(slat);
        SLA sla = slat2slat.transfer();
        assertNotNull(sla);
    }
}
