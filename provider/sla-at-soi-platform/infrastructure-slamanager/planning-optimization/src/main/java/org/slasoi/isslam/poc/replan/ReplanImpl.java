package org.slasoi.isslam.poc.replan;

import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLANotFoundException;
import org.slasoi.isslam.poc.planhandler.impl.PlanHandlerImpl;
import org.slasoi.slamodel.primitives.UUID;

/**
 * The implementation of ReplanImpl.
 * 
 * @author Kuan Lu
 */
public class ReplanImpl {
    public void rePlan(UUID uuid){
        try {
            String message = PlanHandlerImpl.context.getProtocolEngine().getINegotiation().renegotiate(uuid);
            // what does message mean?
        }
        catch (SLANotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLAManagerContextException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
