package org.slasoi.isslam.poc.resourceplanneroptimizer.impl;

import org.slasoi.isslam.poc.utils.Constant;

/**
 * <code>Harddisk</code> represents Harddisk in resource request.
 * 
 * @see org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Resource
 * @author Kuan Lu
 */
public class Harddisk extends Resource {

    public Harddisk() {
        this.setResourceName(Constant.Harddisk);
    }
}
