/**
 * 
 */
package org.slasoi.isslam.poc.servicesmanager;

import java.util.Collection;
import java.util.LinkedHashMap;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ReservationResponseType;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Request;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;

/**
 * Inreface <code>ServiceManagerHandler</code> interacts with infrastucture service manager.
 * 
 * @author Kuan Lu
 * 
 */
public interface ServiceManagerHandler {
    /**
     * Gets monitoring features
     */
    public ComponentMonitoringFeatures[] getMonitoringFeature();

    /**
     * Queries the resources capacity.
     * 
     * @param provisioningRequest
     */
    public LinkedHashMap<Request, Integer> query(Collection<Request> requestList);

    /**
     * Prepares the resource.
     */
    public boolean prepare();

    /**
     * Reserves the resource.
     * 
     * @param provisioningRequest
     */
    public ReservationResponseType reserve(ProvisionRequestType provisioningRequest);
}
