/**
 * 
 */
package org.slasoi.isslam.poc.outresourcer;

import java.util.ArrayList;
import java.util.Collection;

import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Request;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The <code>OutResource</code> interface represents out-sourcing processes.
 * 
 * @author Kuan Lu
 */
public interface OutResource {
    /**
     * Starts negotiation with other SLA Managers.
     * 
     * @param slatArray
     */
    public ArrayList<SLATemplate> negotiation(ArrayList<SLATemplate> slatArray);

    /**
     * Gets the proper SLA template from SLA template registry.
     * 
     * @param requestList
     */
    public ArrayList<SLATemplate> getProperSLATemplates(Collection<Request> requestList);

    /**
     * Creates an SLA based on the given SLA template.
     * 
     * @param slat
     */
    public SLA createSLA(SLATemplate slat);
}
