package org.slasoi.isslam.poc.slatcreator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * This class is used for getting a proper SLA template when out-sourcing.
 * @author Kuan Lu
 */
public class SLATCreator {

    private static final Logger LOGGER = Logger.getLogger(SLATCreator.class);
    public static String Infrastructure_SLA_Manager_Path_2 =
            System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager" + File.separator
                    + "planning-optimization" + File.separator + "A4" + File.separator + "A4_SLATemplate_1_VM_1.xml";
    public static String Infrastructure_SLA_Manager_Path_3 =
            System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager" + File.separator
                    + "planning-optimization" + File.separator + "A4" + File.separator + "A4_SLATemplate_2_VM_1_2.xml";
    public static String Infrastructure_SLA_Manager_Path_4 =
            System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager" + File.separator
                    + "planning-optimization" + File.separator + "A4" + File.separator + "A4_SLATemplate_2_VM_3_4.xml";

    public SLATCreator() {
    }

    public SLATemplate start(String path) {
        if (path.equals(SLATCreator.Infrastructure_SLA_Manager_Path_2)
                | path.equals(SLATCreator.Infrastructure_SLA_Manager_Path_3)
                | path.equals(SLATCreator.Infrastructure_SLA_Manager_Path_4)) {
            FileReader read;
            try {
                read = new FileReader(path);
                BufferedReader br = new BufferedReader(read);
                StringBuffer sb = new StringBuffer();
                String row;
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                SLASOITemplateParser slasoieTemplatParser = new SLASOITemplateParser();
                SLATemplate slat;
                slat = slasoieTemplatParser.parseTemplate(sb.toString());
                return slat;
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
            catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }
        else
            return null;
    }
}
