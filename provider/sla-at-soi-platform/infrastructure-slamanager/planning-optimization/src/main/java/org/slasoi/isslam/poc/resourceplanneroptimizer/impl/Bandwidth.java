package org.slasoi.isslam.poc.resourceplanneroptimizer.impl;

import org.slasoi.isslam.poc.utils.Constant;

/**
 * <code>Bandwidth</code> represents Bandwidth in resource request.
 * 
 * @see org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Resource
 * @author Kuan Lu
 */
public class Bandwidth extends Resource {
    public Bandwidth() {
        this.setResourceName(Constant.Bandwidth);
    }
}
