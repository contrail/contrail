package org.slasoi.isslam.poc.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils.GenericSLAManagerUtilsException;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The class that will be operated while deploying the bundle into OSGi environment.
 * 
 * @author Kuan Lu
 */
public class PlanningOptimizationActivator implements BundleActivator {
    private static final Logger LOGGER = Logger.getLogger(PlanningOptimizationActivator.class);
    public static SLAManagerContext context;
    //private BundleContext context;
    //private ServiceTracker tracker;

	 /**
      * The method that starts the bundle in OSGi environment.
      */
    public void start(BundleContext context) throws Exception {
        /*LOGGER.info("************ PlanningOptimizationActivator - start() ***************************");
        methodExecuted();
        if(PlanningOptimizationActivator.context!=null){
            try {
                LOGGER.info("CONTEXT : " + PlanningOptimizationActivator.context.getSlamUtils().getContextSet("GLOBAL"));
                LOGGER.info("**************************************************************************");
                LOGGER.info("**************************NEGOTIATION PHASE*******************************");
                LOGGER.info("**************************************************************************");
                FileReader read;
                read =
                    new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                            + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                            + "a4-infrastructure-template.xml");
                BufferedReader br = new BufferedReader(read);
                StringBuffer sb = new StringBuffer();
                String row;
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                // SLASOITemplateParser slasoieTemplatParser = new SLASOITemplateParser();
                SLATemplate slat;
                Iterator<ISyntaxConverter> it = PlanningOptimizationActivator.context.getSyntaxConverters().values().iterator();
                ISyntaxConverter sc = it.next();
                slat = (SLATemplate) (sc.parseSLATemplate(sb.toString()));
                PlanningOptimizationActivator.context.getPlanningOptimization().getIAssessmentAndCustomize().negotiate("", slat);
                // run = new RunnerImpl(slat, null);
                /*LOGGER.info("**************************************************************************");
                LOGGER.info("************************PLAN PROVISIONING PHASE***************************");
                LOGGER.info("**************************************************************************");
                FileReader read1 =
                        new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                                + File.separator + "planning-optimization" + File.separator + "A4" + File.separator + "A4_SLA-2-VM-Correct.xml");

                BufferedReader br1 = new BufferedReader(read1);
                StringBuffer sb1 = new StringBuffer();
                String row1;
                while ((row1 = br1.readLine()) != null) {
                    sb1.append(row1);
                }
                SLA sla;
                LOGGER.info("SC : "+sc);
                sla = (SLA) (sc.parseSLA(sb1.toString()));
                this.context.getPlanningOptimization().getINotification().activate(sla);
            }
            catch (GenericSLAManagerUtilsException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            LOGGER.info("context is null...");
        }*/
    }
    
    private void methodExecuted() {
        System.out.println("execute method methodExecuted()");
    }

    private void methodNotExecuted() {
        System.out.println("execute method methodNotExecuted()");
    }
	
	 /**
      * The method that stops the bundle in OSGi environment.
      */
    public void stop(BundleContext arg0) throws Exception {
        try {
            String className = "net.sourceforge.cobertura.coveragedata.ProjectData";
            String methodName = "saveGlobalProjectData";
            Class saveClass = Class.forName(className);
            java.lang.reflect.Method saveMethod = saveClass.getDeclaredMethod(
                    methodName, new Class[0]);
            saveMethod.invoke(null, new Object[0]);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
