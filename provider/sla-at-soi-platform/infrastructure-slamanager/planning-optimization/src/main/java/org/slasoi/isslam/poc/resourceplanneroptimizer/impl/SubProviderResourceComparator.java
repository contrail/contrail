/**
 * 
 */
package org.slasoi.isslam.poc.resourceplanneroptimizer.impl;

import java.util.Comparator;

/**
 * Sorts the sequence of sub-provider according to the number of specific resources
 * 
 * @author Kuan Lu
 */
public class SubProviderResourceComparator implements Comparator<RequestProcessor> {

    private String resourceName;

    /**
     * Compares the sub-provides.
     */
    public SubProviderResourceComparator(String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     * Starts to compare.
     */
    public int compare(RequestProcessor o1, RequestProcessor o2) {
        // TODO Auto-generated method stub
        return 0;
    }

    /*
     * public int compare(RequestProcessor o1, RequestProcessor o2) { if(o1.getResourceByName(resourceName)!=null){
     * if(o2.getResourceByName(resourceName)==null){ return -1; }
     * if(o1.getResourceByName(resourceName).getAmount()>o2.getResourceByName(resourceName).getAmount()){ return -1; }
     * else if(o1.getResourceByName(resourceName).getAmount()==o2.getResourceByName(resourceName).getAmount()){ return
     * 0; } else return 1; } else return 1; }
     */

}
