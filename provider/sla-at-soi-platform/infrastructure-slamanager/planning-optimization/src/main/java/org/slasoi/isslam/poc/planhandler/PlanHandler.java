/**
 * 
 */
package org.slasoi.isslam.poc.planhandler;

import org.slasoi.gslam.commons.plan.Plan;

/**
 * The <code>PlanHandler</code> interface represents creation of Plan instance for executing a Plan.
 * 
 * @author Kuan Lu
 */
public interface PlanHandler {
    /**
     * Creates a Plan instance.
     */
    public Plan planMaker();
}
