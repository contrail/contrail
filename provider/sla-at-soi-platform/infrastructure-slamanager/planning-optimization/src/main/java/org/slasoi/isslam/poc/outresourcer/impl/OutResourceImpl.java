package org.slasoi.isslam.poc.outresourcer.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.INegotiation.InvalidNegotiationIDException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationInProgressException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLACreationException;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Exception;
import org.slasoi.isslam.poc.outresourcer.OutResource;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Request;
import org.slasoi.isslam.poc.runner.impl.RunnerImpl;
import org.slasoi.isslam.poc.slatcreator.SLATCreator;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The implementation class of <code>OutResource</code> interface.
 * 
 * @see org.slasoi.isslam.poc.outresourcer.OutResource
 * @author Kuan Lu
 */
public class OutResourceImpl implements OutResource {
    private static final Logger LOGGER = Logger.getLogger(OutResourceImpl.class);
    private LinkedHashMap<SLATemplate, String> negoIDSLAT = new LinkedHashMap<SLATemplate, String>();

    public OutResourceImpl() {
    }

    /**
     * Gets the proper SLA templates.
     */
    public ArrayList<SLATemplate> getProperSLATemplates(Collection<Request> requestList) {
        try {
            ArrayList<SLATemplate> slatArray = new ArrayList<SLATemplate>();
            if (RunnerImpl.context != null) {
                UUID[] uuidSet = RunnerImpl.context.getSLATemplateRegistry().query(null);

                for (int i = 0; i < uuidSet.length; i++) {
                    slatArray.add(RunnerImpl.context.getSLATemplateRegistry().getSLATemplate(uuidSet[i]));
                }
                return slatArray;
            }
            else {
                SLATCreator slatCreator = new SLATCreator();
                slatArray.add(slatCreator.start(SLATCreator.Infrastructure_SLA_Manager_Path_2));
                slatArray.add(slatCreator.start(SLATCreator.Infrastructure_SLA_Manager_Path_3));
                slatArray.add(slatCreator.start(SLATCreator.Infrastructure_SLA_Manager_Path_4));
                return slatArray;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Starts the negotiation with its sub-contractors.
     */
    public ArrayList<SLATemplate> negotiation(ArrayList<SLATemplate> slatArray) {
        if (RunnerImpl.context != null) {
            ArrayList<SLATemplate> returnSlatArray = new ArrayList<SLATemplate>();
            for (int i = 0; i < slatArray.size(); i++) {
                try {
                    String negotiationID =
                            RunnerImpl.context.getProtocolEngine().getINegotiation().initiateNegotiation(
                                    slatArray.get(i));
                    SLATemplate[] temp =
                            RunnerImpl.context.getProtocolEngine().getINegotiation().negotiate(negotiationID,
                                    slatArray.get(i));
                    if (temp != null) {
                        returnSlatArray.add(temp[0]);
                        negoIDSLAT.put(temp[0], negotiationID);
                    }
                    else {
                        LOGGER.error("No feedback received while outsourcing.");
                        return slatArray;
                    }
                }
                catch (OperationInProgressException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (InvalidNegotiationIDException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (OperationNotPossibleException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SLAManagerContextException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return returnSlatArray;
        }
        else {
            return slatArray;
        }
    }

    /**
     * Starts to create an SLA with its sub-contractor.
     */
    public SLA createSLA(SLATemplate slat) {
        if (RunnerImpl.context != null) {
            String negotiationID = this.negoIDSLAT.get(slat);
            SLA temp;
            try {
                temp = RunnerImpl.context.getProtocolEngine().getINegotiation().createAgreement(negotiationID, slat);
                if (temp != null) {
                    return temp;
                }
                else {
                    LOGGER.error("No feedback received while outsourcing.");
                    return temp;
                }
            }
            catch (OperationInProgressException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
            catch (SLACreationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
            catch (InvalidNegotiationIDException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }
        else {
            return new SLA();
        }
    }

}
