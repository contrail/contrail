package org.slasoi.isslam.poc.resourceplanneroptimizer.impl;

/**
 * The general QoS (availability) for VM access point.
 * 
 * @author Kuan Lu
 */
public class VMAccessPoint {
    /**
     * Availability
     */
    private double availability;

    /**
     * Gets availability.
     */
    public double getAvailability() {
        return availability;
    }

    /**
     * Sets availability.
     */
    public void setAvailability(double availability) {
        this.availability = availability;
    }
}
