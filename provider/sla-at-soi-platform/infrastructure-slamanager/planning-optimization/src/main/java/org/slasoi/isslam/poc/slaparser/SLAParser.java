package org.slasoi.isslam.poc.slaparser;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.apache.log4j.Logger;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Bandwidth;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.CPU;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Harddisk;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Memory;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Request;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Resource;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.VMAccessPoint;
import org.slasoi.isslam.poc.slaparser.exceptions.InvalidSLATemplateFormatException;
import org.slasoi.isslam.poc.slaparser.exceptions.MoreThanOneInterfaceDefinedException;
import org.slasoi.isslam.poc.slaparser.exceptions.NoVMSpecifiedException;
import org.slasoi.isslam.poc.utils.Constant;
import org.slasoi.slamodel.core.CompoundConstraintExpr;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.BOOL;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.ResourceType;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.vocab.B4Terms;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.resources;
import org.slasoi.slamodel.vocab.units;

/**
 * The class <code>SLAParser</code> parsers the coming SLA / SLA template.
 * 
 * @author Kuan Lu
 */
public class SLAParser {
    private SLATemplate slat;
    private ValueExpr value;
    private static final Logger LOGGER = Logger.getLogger(SLAParser.class);
    private LinkedHashMap<String, Request> resourceRequest;
    private LinkedHashMap<String, VMAccessPoint> vmAccessPointList;
    private SimpleDomainExpr sde;
    private CONST newValue;
    private LinkedHashMap<String, CONST> variableDeclr = new LinkedHashMap<String, CONST>();
    private float price = 0;
    private String priceType = "";

    /**
     * Starts parser.
     */
    public SLAParser(SLATemplate slat) {
        try {
            this.resourceRequest = new LinkedHashMap<String, Request>();
            this.vmAccessPointList = new LinkedHashMap<String, VMAccessPoint>();
            if (slat instanceof SLA) {
                this.slat = slat;
                LOGGER.info("The file is an SLA.");
                this.render();
            }
            else if (slat instanceof SLATemplate) {
                this.slat = slat;
                LOGGER.info("The file is an SLA template.");
                this.render();
            }
            this.requestCleaner();
        }
        catch (NoVMSpecifiedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (MoreThanOneInterfaceDefinedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidSLATemplateFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void render() throws NoVMSpecifiedException, MoreThanOneInterfaceDefinedException,
            InvalidSLATemplateFormatException {
        if (this.slat == null)
            throw new IllegalArgumentException("No SLA(T) specified");
        if (this.slat instanceof SLA) {
            this.x_SLA();
        }
        else if (this.slat instanceof SLATemplate) {
            this.x_SLA();
        }
    }

    // ----PUBLIC-------------------------------------------------------------------------------------------

    // ----ANNOTATION-------------------------------------------------------------------------------------------

    // ----SLA-------------------------------------------------------------------------------------------

    private void x_SLA() throws NoVMSpecifiedException, MoreThanOneInterfaceDefinedException,
            InvalidSLATemplateFormatException {
        if (slat == null)
            throw new IllegalArgumentException("No SLA(T) specified");
        x_SLA_CONTENT();
    }

    // ----SLAT CONTENT-------------------------------------------------------------------------------------------

    private void x_SLA_CONTENT() throws NoVMSpecifiedException, MoreThanOneInterfaceDefinedException,
            InvalidSLATemplateFormatException {
        if (this.slat == null)
            throw new IllegalArgumentException("No SLA specified");
        // ---- PARTIES -------
        /*
         * Party[] parties = this.slat.getParties(); for (Party p : parties){
         * if(p.getAgreementRole().toString().equalsIgnoreCase(sla.customer.toString())){ try { if(RunnerImpl.context !=
         * null){ p.setPropertyValue(sla.gslam_epr, RunnerImpl.context.getEPR());
         * LOGGER.info("OSGi: Set the endpoint to be "+p.getPropertyValue(sla.gslam_epr)); } /*else { // just for
         * testing p.setPropertyValue(sla.gslam_epr, "129.217.211.212:7070");
         * LOGGER.info("Local: Set the endpoint to be "+p.getPropertyValue(sla.gslam_epr)); } } catch
         * (SLAManagerContextException e) { // TODO Auto-generated catch block e.printStackTrace(); } } else continue; }
         */
        LOGGER.info("Start to parse the SLA(T) content.");
        InterfaceDeclr[] idecs = this.slat.getInterfaceDeclrs();
        // ---- INTERFACE DECLARATIONS-------------------------------------------------
        // TODO: the client type should be defined somewhere.
        if (idecs != null) {
            for (InterfaceDeclr idec : idecs) {
                this.x_RENDER(idec);
            }
        }

        // ---- AGREEMENT TERMS--------------------------------------------------------
        AgreementTerm[] terms = this.slat.getAgreementTerms();
        if (terms != null)
            for (AgreementTerm t : terms) {
                x_RENDER(t);
                this.variableDeclr.clear();
            }
        // this.setAvailability(-1);
        // this.interfaceDeclrArray = "";
    }

    // ----PARTY OPERATIVE-------------------------------------------------------------------------------------------

    // ----INTERFACE DECLR -------------------------------------------------------------------------------------------
    private void x_RENDER(InterfaceDeclr idec) {
        LOGGER.info("Start to parse the SLA(T) INTERFACE DECLR");
        // Endpoint[] eps = idec.getEndpoints();
        Interface iface = idec.getInterface();
        if (iface instanceof ResourceType) {
            if (((ResourceType) iface).getName().equalsIgnoreCase("VirtualMachine")
                    || ((ResourceType) iface).getName().equalsIgnoreCase("VirtualMachines")
                    || ((ResourceType) iface).getName().equalsIgnoreCase("VM")
                    || ((ResourceType) iface).getName().equalsIgnoreCase("VMs")) {
                this.vmAccessPointList.put(idec.getId().getValue(), new VMAccessPoint());
            }
        }
    }

    // ----ENDPOINT-------------------------------------------------------------------------------------------

    // ----INTERFACE REF-------------------------------------------------------------------------------------------

    // ----INTERFACE SPEC-------------------------------------------------------------------------------------------

    // ----INTERFACE
    // RESOURCETYPE-------------------------------------------------------------------------------------------

    // ----OPERATION-------------------------------------------------------------------------------------------

    // ----OPERATION PROPERTY-------------------------------------------------------------------------------------------

    // ----SERVICE REF------------------------------------------------------------------------------------------

    // ----VAR-------------------------------------------------------------------------------------------

    // ----AGREEMENT TERM-------------------------------------------------------------------------------------------

    private void x_RENDER(AgreementTerm term) throws NoVMSpecifiedException, InvalidSLATemplateFormatException {
        LOGGER.info("Start to parse the SLA(T) AGREEMENT TERM");
        VariableDeclr[] vdecs = term.getVariableDeclrs();
        if (vdecs != null)
            for (VariableDeclr v : vdecs) {
                x_RENDER(v);
            }

        Guaranteed[] gs = term.getGuarantees();
        for (Guaranteed g : gs) {
            if (g instanceof Guaranteed.State)
                x_RENDER((Guaranteed.State) g);
            else if (g instanceof Guaranteed.Action)
                x_RENDER((Guaranteed.Action) g);
        }
    }

    // ----VAR-------------------------------------------------------------------------------------------

    private void x_RENDER(VariableDeclr vdec) throws InvalidSLATemplateFormatException {
        LOGGER.info("Start to parse the SLA(T) VARIABLE DECLR");
        if (vdec instanceof Customisable) {
            Customisable c = (Customisable) vdec;
            // ID for Var
            ID var = c.getVar();
            // CONST for Value
            CONST valu = c.getValue();
            this.variableDeclr.put(var.getValue(), valu);
        }
        // create VM
        else if (!(vdec instanceof Customisable)) {
            String term = "";
            String var = vdec.getVar().getValue();
            Expr exp = vdec.getExpr();
            if (exp instanceof FunctionalExpr) {
                term = ((FunctionalExpr) exp).getOperator().toString();
                ValueExpr[] parameter = ((FunctionalExpr) exp).getParameters();
                // one VM points to only one VM-Access-point
                if ((parameter[0] instanceof ID) & term.equalsIgnoreCase(core.subset_of.toString())) {
                    Request request = new Request(Constant.Client_Type_Gold);
                    this.copyVMAccessPoint2Request(request, this.vmAccessPointList.get(((ID) parameter[0]).getValue()));
                    request.setVmName(var);
                    this.resourceRequest.put(var, request);
                }
                // SLA without customisable
                else if ((parameter[0] instanceof CONST) & !term.equalsIgnoreCase(core.subset_of.toString())) {
                    this.variableDeclr.put(var, (CONST) parameter[0]);
                }
            }

            else if (exp instanceof CompoundDomainExpr) {
                LOGGER.error("Sorry, POC dose not support more than one constraints to a specific term.");
                throw new InvalidSLATemplateFormatException();
            }
        }
    }

    private void copyVMAccessPoint2Request(Request request, VMAccessPoint vmAccessPoint) {
        request.setAvailability(vmAccessPoint.getAvailability());
    }

    // ----STATE-------------------------------------------------------------------------------------------

    private void x_RENDER(Guaranteed.State gs) throws NoVMSpecifiedException {
        LOGGER.info("Start to parse the SLA(T) GUARANTEED STATE");
        ConstraintExpr state = gs.getState();
        x_RENDER(state);
    }

    // ----ACTION-------------------------------------------------------------------------------------------

    private void x_RENDER(Guaranteed.Action ga) {
        LOGGER.info("Start to parse the SLA(T) GUARANTEED ACTION");
        x_RENDER(ga.getPostcondition());
    }

    // ----ACTION DEFN-------------------------------------------------------------------------------------------

    private void x_RENDER(Guaranteed.Action.Defn gad) {
        if (gad instanceof ProductOfferingPrice) {
            x_RENDER_PRODUCT_OFFERING_PRICE((ProductOfferingPrice) gad);
        }
    }

    // ----PRODUCT OFFERING
    // PRICE--------------------------------------------------------------------------------------------------------
    private void x_RENDER_PRODUCT_OFFERING_PRICE(ProductOfferingPrice pop) {
        ComponentProductOfferingPrice[] cpops = pop.getComponentProductOfferingPrices();
        if (cpops.length >= 1) {
            this.priceType = cpops[0].getPrice().getDatatype().getValue();
            this.price = Float.valueOf(cpops[0].getPrice().getValue().toString());
        }
    }

    // ----CONSTRAINT EXPR------------------------------------------------------------------------------------------

    private void x_RENDER(ConstraintExpr ce) throws NoVMSpecifiedException {
        String term = "";
        // ArrayList<String> valueList = new ArrayList<String>();
        ArrayList<String> VMList = new ArrayList<String>();
        if (ce instanceof TypeConstraintExpr) {
            // TypeConstraintExpr
            TypeConstraintExpr tce = (TypeConstraintExpr) ce;
            ValueExpr valu = tce.getValue();
            if (valu instanceof FunctionalExpr) {
                // LOGGER.info("The operator is : " + ((FunctionalExpr) valu).getOperator().toString());
                term = ((FunctionalExpr) valu).getOperator().toString();
                ValueExpr[] parameter = ((FunctionalExpr) valu).getParameters();
                for (ValueExpr v : parameter) {
                    if (v instanceof ID) {
                        String id = ((ID) v).getValue();
                        if (id.equalsIgnoreCase("")) {
                            LOGGER.error("No interface information specified in agreement term : " + term);
                            throw new NoVMSpecifiedException();
                        }
                        VMList.add(id);
                    }
                }
            }
            // SimpleDomainExpr
            DomainExpr de = tce.getDomain();
            if (de instanceof SimpleDomainExpr) {
                sde = (SimpleDomainExpr) de;
                value = sde.getValue();
                /*
                 * if (val instanceof CONST) { value = ((CONST) val).getValue(); } else if (val instanceof LIST){
                 * valueList = }
                 */
            }
            else if (de instanceof CompoundDomainExpr) {
                LOGGER.error("Sorry, POC dose not support more than one constraints to a specific term.");
                return;
            }

            for (String vm : VMList) {
                VMAccessPoint request;
                if (this.resourceRequest.containsKey(vm)) {
                    request = this.resourceRequest.get(vm);
                }
                else if (this.vmAccessPointList.containsKey(vm)) {
                    request = this.vmAccessPointList.get(vm);
                }
                else {
                    LOGGER.error("The required VM: " + vm + " is either not in VM list nor in VM Access Point list.");
                    return;
                }

                if (request instanceof Request) {
                    // set starting time
                    if (this.variableDeclr.containsKey(Constant.startTtime)) {
                        ((Request) request).setStartTime(((CONST) this.variableDeclr.get("START_TIME_VAR")).getValue());
                    }

                    // set ending time
                    if (this.variableDeclr.containsKey(Constant.endTime)) {
                        ((Request) request).setFreeAt(((CONST) this.variableDeclr.get("END_TIME_VAR")).getValue());
                    }
                }

                if (term.equalsIgnoreCase(common.availability.toString())) {
                    if (value instanceof CONST) {
                        double value_trans = Double.parseDouble(((CONST) value).getValue());
                        if (value_trans >= Constant.basicAvailability) {
                            request.setAvailability(value_trans);
                        }
                        else {
                            request.setAvailability(Constant.basicAvailability);
                            newValue = new CONST(String.valueOf(Constant.basicAvailability), units.percentage);
                            sde.setValue(newValue);
                        }
                        continue;
                    }
                    // point to variable
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            double value_trans = Double.parseDouble(((CONST) temp).getValue());
                            if (value_trans >= Constant.basicAvailability) {
                                request.setAvailability(value_trans);
                            }
                            else
                                request.setAvailability(Constant.basicAvailability);
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(availability)");
                        return;
                    }
                }
                else if (term.equalsIgnoreCase(common.isolation.toString()) & (request instanceof Request)) {
                    if (value instanceof BOOL) {
                        ((Request) request).setIsolation(((BOOL) value).getValue());
                        continue;
                    }
                    // point to variable
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            ((Request) request).setIsolation(Boolean.parseBoolean(temp.getValue()));
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be BOOL.(isolation)");
                        return;
                    }
                }

                else if (term.equalsIgnoreCase(resources.vm_cores.toString()) & (request instanceof Request)) {
                    if (value instanceof CONST) {
                        ((Request) request).setCpuNr((CONST) value);
                        int value_trans = Integer.parseInt(((CONST) value).getValue());
                        LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                        /*
                         * I will change the code later, because current ISM can not give me the detail number of core.
                         * So now I accept whatever user requires. I will also create a SLA Template parser, when POC
                         * starts, it will get all the limitation of each resorces.
                         */
                        // //////////////////////////////////////////////////////////////////////////////////////
                        if (resource.containsKey(Constant.CPU) & resource.get(Constant.CPU) instanceof CPU) {
                            if (value_trans >= Constant.CPU_Core_Mini_Nr_VM0
                                    & value_trans <= Constant.CPU_Core_Max_Nr_VM0) {
                                Resource cpu = resource.get(Constant.CPU);
                                cpu.setAmount(value_trans);
                            }
                            else {
                                LOGGER.error("Unsupported CPU core number for VM: " + value_trans
                                        + ". The lowest CPU core number setting is : " + Constant.CPU_Core_Mini_Nr_VM0
                                        + ". The highest CPU core number setting is : " + Constant.CPU_Core_Max_Nr_VM0);
                                return;
                            }
                        }
                        else {
                            if (value_trans >= Constant.CPU_Core_Mini_Nr_VM0
                                    & value_trans <= Constant.CPU_Core_Max_Nr_VM0) {
                                CPU cpu = new CPU();
                                cpu.setAmount(value_trans);
                                resource.put(Constant.CPU, cpu);
                            }
                            else {
                                LOGGER.error("Unsupported CPU core number for VM: " + value_trans
                                        + ". The lowest CPU core number setting is : " + Constant.CPU_Core_Mini_Nr_VM0
                                        + ". The highest CPU core number setting is : " + Constant.CPU_Core_Max_Nr_VM0);
                                return;
                            }
                        }
                        continue;
                        // ////////////////////////////////////////////////////////////////////////////////////////
                        /*
                         * if (vm.equalsIgnoreCase(Constant.VM0)) { if (resource.containsKey(Constant.CPU) &
                         * resource.get(Constant.CPU) instanceof CPU) { if (value_trans >= Constant.CPU_Core_Mini_Nr_VM0
                         * & value_trans <= Constant.CPU_Core_Max_Nr_VM0) { Resource cpu = resource.get(Constant.CPU);
                         * cpu.setAmount(value_trans); } else { LOGGER.error("Unsupported CPU core number for VM0: " +
                         * value_trans + ". The lowest CPU core number setting is : " + Constant.CPU_Core_Mini_Nr_VM0 +
                         * ". The highest CPU core number setting is : " + Constant.CPU_Core_Max_Nr_VM0); return; } }
                         * else { if (value_trans >= Constant.CPU_Core_Mini_Nr_VM0 & value_trans <=
                         * Constant.CPU_Core_Max_Nr_VM0) { CPU cpu = new CPU(); cpu.setAmount(value_trans);
                         * resource.put(Constant.CPU, cpu); } else {
                         * LOGGER.error("Unsupported CPU core number for VM0: " + value_trans +
                         * ". The lowest CPU core number setting is : " + Constant.CPU_Core_Mini_Nr_VM0 +
                         * ". The highest CPU core number setting is : " + Constant.CPU_Core_Max_Nr_VM0); return; } }
                         * continue; } else if (vm.equalsIgnoreCase(Constant.VM1)) { if
                         * (resource.containsKey(Constant.CPU) & resource.get(Constant.CPU) instanceof CPU) { if
                         * (value_trans >= Constant.CPU_Core_Mini_Nr_VM1 & value_trans <= Constant.CPU_Core_Max_Nr_VM1)
                         * { Resource cpu = resource.get(Constant.CPU); cpu.setAmount(value_trans); } else {
                         * LOGGER.error("Unsupported CPU core number for VM1: " + value_trans +
                         * ". The lowest CPU core number setting is : " + Constant.CPU_Core_Mini_Nr_VM1 +
                         * ". The highest CPU core number setting is : " + Constant.CPU_Core_Max_Nr_VM1); return; }
                         * 
                         * } else { if (value_trans >= Constant.CPU_Core_Mini_Nr_VM1 & value_trans <=
                         * Constant.CPU_Core_Max_Nr_VM1) { CPU cpu = new CPU(); cpu.setAmount(value_trans);
                         * resource.put(Constant.CPU, cpu); } else {
                         * LOGGER.error("Unsupported CPU core number for VM1: " + value_trans +
                         * ". The lowest CPU core number setting is : " + Constant.CPU_Core_Mini_Nr_VM1 +
                         * ". The highest CPU core number setting is : " + Constant.CPU_Core_Max_Nr_VM1); return; }
                         * 
                         * } continue; }
                         */
                    }
                    // point to variable
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            ((Request) request).setCpuNr(temp);
                            int value_trans = Integer.parseInt(temp.getValue());
                            LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                            if (resource.containsKey(Constant.CPU) & resource.get(Constant.CPU) instanceof CPU) {
                                if (value_trans >= Constant.CPU_Core_Mini_Nr_VM0
                                        & value_trans <= Constant.CPU_Core_Max_Nr_VM0) {
                                    Resource cpu = resource.get(Constant.CPU);
                                    cpu.setAmount(value_trans);
                                }
                                else {
                                    LOGGER.error("Unsupported CPU core number for VM: " + value_trans
                                            + ". The lowest CPU core number setting is : "
                                            + Constant.CPU_Core_Mini_Nr_VM0
                                            + ". The highest CPU core number setting is : "
                                            + Constant.CPU_Core_Max_Nr_VM0);
                                    return;
                                }
                            }
                            else {
                                if (value_trans >= Constant.CPU_Core_Mini_Nr_VM0
                                        & value_trans <= Constant.CPU_Core_Max_Nr_VM0) {
                                    CPU cpu = new CPU();
                                    cpu.setAmount(value_trans);
                                    resource.put(Constant.CPU, cpu);
                                }
                                else {
                                    LOGGER.error("Unsupported CPU core number for VM: " + value_trans
                                            + ". The lowest CPU core number setting is : "
                                            + Constant.CPU_Core_Mini_Nr_VM0
                                            + ". The highest CPU core number setting is : "
                                            + Constant.CPU_Core_Max_Nr_VM0);
                                    return;
                                }
                            }
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(CPU_cores)");
                        return;
                    }
                }
                else if (term.equalsIgnoreCase(resources.cpu_speed.toString()) & (request instanceof Request)) {
                    if (value instanceof CONST) {
                        float value_trans = Float.parseFloat(((CONST) value).getValue());
                        LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                        // ////////////////////////////////////////////////////////////////////////////////////////
                        if (resource.containsKey(Constant.CPU) & resource.get(Constant.CPU) instanceof CPU) {
                            if (value_trans >= Constant.CPU_Mini_Speed_VM0 & value_trans <= Constant.CPU_Max_Speed_VM0) {
                                CPU cpu = (CPU) resource.get(Constant.CPU);
                                double price =
                                        Constant.CPU_Basic_Price + (value_trans - Constant.CPU_Mini_Speed_VM0) * 10
                                                * Constant.CPU_Price_Increasement;
                                // LOGGER.info("The price for CPU with " + value_trans + " GHz from VM0 is " +
                                // price);
                                cpu.setPrice(price);
                                cpu.setSpeed(value_trans);
                            }
                            else {
                                LOGGER.error("Unsupported CPU speed for VM: " + value_trans
                                        + ". The lowest CPU speed setting is " + Constant.CPU_Mini_Speed_VM0
                                        + " GHz. The highest CPU speed setting is " + Constant.CPU_Max_Speed_VM0
                                        + " GHz.");
                                return;
                            }
                        }
                        else {
                            if (value_trans >= Constant.CPU_Mini_Speed_VM0 & value_trans <= Constant.CPU_Max_Speed_VM0) {
                                CPU cpu = new CPU();
                                double price =
                                        Constant.CPU_Basic_Price + (value_trans - Constant.CPU_Mini_Speed_VM0) * 10
                                                * Constant.CPU_Price_Increasement;
                                // LOGGER.info("The price for CPU with " + value_trans + " GHz from VM0 is " +
                                // price);
                                cpu.setPrice(price);
                                cpu.setSpeed(value_trans);
                                resource.put(Constant.CPU, cpu);
                            }
                            else {
                                LOGGER.error("Unsupported CPU speed for VM: " + value_trans
                                        + ". The lowest CPU speed setting is " + Constant.CPU_Mini_Speed_VM0
                                        + " GHz. The highest CPU speed setting is " + Constant.CPU_Max_Speed_VM0
                                        + " GHz.");
                                return;
                            }
                        }
                        continue;
                        // ////////////////////////////////////////////////////////////////////////////////////////
                        /*
                         * if (vm.equalsIgnoreCase(Constant.VM0)) { if (resource.containsKey(Constant.CPU) &
                         * resource.get(Constant.CPU) instanceof CPU) { if (value_trans >= Constant.CPU_Mini_Speed_VM0 &
                         * value_trans <= Constant.CPU_Max_Speed_VM0) { CPU cpu = (CPU) resource.get(Constant.CPU);
                         * double price = Constant.CPU_Basic_Price + (value_trans - Constant.CPU_Mini_Speed_VM0) 10 *
                         * Constant.CPU_Price_Increasement; // LOGGER.info("The price for CPU with " + value_trans +
                         * " GHz from VM0 is " + // price); cpu.setPrice(price); cpu.setSpeed(value_trans); } else {
                         * LOGGER.error("Unsupported CPU speed for VM0: " + value_trans +
                         * ". The lowest CPU speed setting is " + Constant.CPU_Mini_Speed_VM0 +
                         * " GHz. The highest CPU speed setting is " + Constant.CPU_Max_Speed_VM0 + " GHz."); return; }
                         * } else { if (value_trans >= Constant.CPU_Mini_Speed_VM0 & value_trans <=
                         * Constant.CPU_Max_Speed_VM0) { CPU cpu = new CPU(); double price = Constant.CPU_Basic_Price +
                         * (value_trans - Constant.CPU_Mini_Speed_VM0) 10 * Constant.CPU_Price_Increasement; //
                         * LOGGER.info("The price for CPU with " + value_trans + " GHz from VM0 is " + // price);
                         * cpu.setPrice(price); cpu.setSpeed(value_trans); resource.put(Constant.CPU, cpu); } else {
                         * LOGGER.error("Unsupported CPU speed for VM0: " + value_trans +
                         * ". The lowest CPU speed setting is " + Constant.CPU_Mini_Speed_VM0 +
                         * " GHz. The highest CPU speed setting is " + Constant.CPU_Max_Speed_VM0 + " GHz."); return; }
                         * } continue; } else if (vm.equalsIgnoreCase(Constant.VM1)) { if
                         * (resource.containsKey(Constant.CPU) & resource.get(Constant.CPU) instanceof CPU) { if
                         * (value_trans >= Constant.CPU_Mini_Speed_VM1 & value_trans <= Constant.CPU_Max_Speed_VM1) {
                         * CPU cpu = (CPU) resource.get(Constant.CPU); double price = Constant.CPU_Basic_Price +
                         * (value_trans - Constant.CPU_Mini_Speed_VM1) 10 * Constant.CPU_Price_Increasement; //
                         * LOGGER.info("The price for CPU with " + value_trans + " GHz from VM1 is " + // price);
                         * cpu.setPrice(price); cpu.setSpeed(value_trans); } else {
                         * LOGGER.error("Unsupported CPU speed for VM1: " + value_trans +
                         * ". The lowest CPU speed setting is " + Constant.CPU_Mini_Speed_VM1 +
                         * " GHz. The highest CPU speed setting is " + Constant.CPU_Max_Speed_VM1 + " GHz."); return; }
                         * } else { if (value_trans >= Constant.CPU_Mini_Speed_VM1 & value_trans <=
                         * Constant.CPU_Max_Speed_VM1) { CPU cpu = new CPU(); double price = Constant.CPU_Basic_Price +
                         * (value_trans - Constant.CPU_Mini_Speed_VM1) 10 * Constant.CPU_Price_Increasement; //
                         * LOGGER.info("The price for CPU with " + value_trans + " GHz from VM1 is " + // price);
                         * cpu.setPrice(price); cpu.setSpeed(value_trans); resource.put(Constant.CPU, cpu); } else {
                         * LOGGER.error("Unsupported CPU speed for VM1: " + value_trans +
                         * ". The lowest CPU speed setting is " + Constant.CPU_Mini_Speed_VM1 +
                         * " GHz. The highest CPU speed setting is " + Constant.CPU_Max_Speed_VM1 + " GHz."); return; }
                         * } continue; }
                         */

                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            float value_trans = Float.parseFloat(temp.getValue());
                            LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                            // ////////////////////////////////////////////////////////////////////////////////////////
                            if (resource.containsKey(Constant.CPU) & resource.get(Constant.CPU) instanceof CPU) {
                                if (value_trans >= Constant.CPU_Mini_Speed_VM0
                                        & value_trans <= Constant.CPU_Max_Speed_VM0) {
                                    CPU cpu = (CPU) resource.get(Constant.CPU);
                                    double price =
                                            Constant.CPU_Basic_Price + (value_trans - Constant.CPU_Mini_Speed_VM0) * 10
                                                    * Constant.CPU_Price_Increasement;
                                    // LOGGER.info("The price for CPU with " + value_trans + " GHz from VM0 is "
                                    // +
                                    // price);
                                    cpu.setPrice(price);
                                    cpu.setSpeed(value_trans);
                                }
                                else {
                                    LOGGER.error("Unsupported CPU speed for VM: " + value_trans
                                            + ". The lowest CPU speed setting is " + Constant.CPU_Mini_Speed_VM0
                                            + " GHz. The highest CPU speed setting is " + Constant.CPU_Max_Speed_VM0
                                            + " GHz.");
                                    return;
                                }
                            }
                            else {
                                if (value_trans >= Constant.CPU_Mini_Speed_VM0
                                        & value_trans <= Constant.CPU_Max_Speed_VM0) {
                                    CPU cpu = new CPU();
                                    double price =
                                            Constant.CPU_Basic_Price + (value_trans - Constant.CPU_Mini_Speed_VM0) * 10
                                                    * Constant.CPU_Price_Increasement;
                                    // LOGGER.info("The price for CPU with " + value_trans + " GHz from VM0 is "
                                    // +
                                    // price);
                                    cpu.setPrice(price);
                                    cpu.setSpeed(value_trans);
                                    resource.put(Constant.CPU, cpu);
                                }
                                else {
                                    LOGGER.error("Unsupported CPU speed for VM: " + value_trans
                                            + ". The lowest CPU speed setting is " + Constant.CPU_Mini_Speed_VM0
                                            + " GHz. The highest CPU speed setting is " + Constant.CPU_Max_Speed_VM0
                                            + " GHz.");
                                    return;
                                }
                            }
                            continue;

                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(CPU_speed)");
                        return;
                    }
                }
                else if (term.equalsIgnoreCase(resources.memory.toString()) & (request instanceof Request)) {
                    if (value instanceof CONST) {
                        ((Request) request).setMemoryNr((CONST) value);
                        int value_trans = Integer.parseInt(((CONST) value).getValue());
                        LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                        if (resource.containsKey(Constant.Memory) & resource.get(Constant.Memory) instanceof Memory) {
                            Memory memory = (Memory) resource.get(Constant.Memory);
                            /*
                             * if (value_trans >= Constant.Memory_Mini_Size & value_trans <= Constant.Memory_Max_Size) {
                             * if ((value_trans % Constant.Memory_Mini_Size) > 0.0) {
                             * LOGGER.error("Unsupported momory size " + value_trans +
                             * "M, it should be multiple of 128M."); return; }
                             */
                            double amount = value_trans / Constant.Memory_Mini_Size;
                            memory.setPrice(Constant.Memory_Basic_Price);
                            memory.setAmount((int) amount);
                            /*
                             * } else { LOGGER.error("Unsupported momory size : " + value_trans +
                             * ". The lowest memory setting is " + Constant.Memory_Mini_Size +
                             * " M. The highest memory setting is " + Constant.Memory_Max_Size + "M."); return; }
                             */
                        }
                        else {
                            Memory memory = new Memory();
                            /*
                             * if (value_trans >= Constant.Memory_Mini_Size & value_trans <= Constant.Memory_Max_Size) {
                             * if ((value_trans % Constant.Memory_Mini_Size) > 0.0) {
                             * LOGGER.error("Unsupported momory size " + value_trans +
                             * "M, it should be multiple of 128M."); return; }
                             */
                            double amount = value_trans / Constant.Memory_Mini_Size;
                            memory.setPrice(Constant.Memory_Basic_Price);
                            memory.setAmount((int) amount);
                            resource.put(Constant.Memory, memory);
                            /*
                             * } else { LOGGER.error("Unsupported momory size : " + value_trans +
                             * ". The lowest memory setting is " + Constant.Memory_Mini_Size +
                             * " M. The highest memory setting is " + Constant.Memory_Max_Size + "M."); return; }
                             */

                        }
                        continue;
                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            ((Request) request).setMemoryNr(temp);
                            int value_trans = Integer.parseInt(temp.getValue());
                            LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                            if (resource.containsKey(Constant.Memory) & resource.get(Constant.Memory) instanceof Memory) {
                                Memory memory = (Memory) resource.get(Constant.Memory);
                                /*
                                 * if (value_trans >= Constant.Memory_Mini_Size & value_trans <=
                                 * Constant.Memory_Max_Size) { if ((value_trans % Constant.Memory_Mini_Size) > 0.0) {
                                 * LOGGER.error("Unsupported momory size " + value_trans +
                                 * "M, it should be multiple of 128M."); return; }
                                 */
                                double amount = value_trans / Constant.Memory_Mini_Size;
                                memory.setPrice(Constant.Memory_Basic_Price);
                                memory.setAmount((int) amount);
                                /*
                                 * } else { LOGGER.error("Unsupported momory size : " + value_trans +
                                 * ". The lowest memory setting is " + Constant.Memory_Mini_Size +
                                 * " M. The highest memory setting is " + Constant.Memory_Max_Size + "M."); return; }
                                 */
                            }
                            else {
                                Memory memory = new Memory();
                                /*
                                 * if (value_trans >= Constant.Memory_Mini_Size & value_trans <=
                                 * Constant.Memory_Max_Size) { if ((value_trans % Constant.Memory_Mini_Size) > 0.0) {
                                 * LOGGER.error("Unsupported momory size " + value_trans +
                                 * "M, it should be multiple of 128M."); return; }
                                 */
                                double amount = value_trans / Constant.Memory_Mini_Size;
                                memory.setPrice(Constant.Memory_Basic_Price);
                                memory.setAmount((int) amount);
                                resource.put(Constant.Memory, memory);
                                /*
                                 * } else { LOGGER.error("Unsupported momory size : " + value_trans +
                                 * ". The lowest memory setting is " + Constant.Memory_Mini_Size +
                                 * " M. The highest memory setting is " + Constant.Memory_Max_Size + "M."); return; }
                                 */

                            }
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(memory)");
                        return;
                    }
                }
                // here accuracy is hard disk
                else if (term.equalsIgnoreCase(common.accuracy.toString()) & (request instanceof Request)) {
                    if (value instanceof CONST) {
                        ((Request) request).setHarddiskNr((CONST) value);
                        int value_trans = Integer.parseInt(((CONST) value).getValue());
                        LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                        if (resource.containsKey(Constant.Harddisk)
                                & resource.get(Constant.Harddisk) instanceof Harddisk) {
                            Harddisk disk = (Harddisk) resource.get(Constant.Harddisk);
                            double amount = value_trans;
                            disk.setPrice(Constant.Harddisk_Basic_Price);
                            disk.setAmount((int) amount);
                        }
                        else {
                            Harddisk disk = new Harddisk();
                            double amount = value_trans;
                            disk.setPrice(Constant.Harddisk_Basic_Price);
                            disk.setAmount((int) amount);
                            resource.put(Constant.Harddisk, disk);

                        }
                        continue;
                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            ((Request) request).setHarddiskNr(temp);
                            int value_trans = Integer.parseInt(temp.getValue());
                            LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                            if (resource.containsKey(Constant.Harddisk)
                                    & resource.get(Constant.Harddisk) instanceof Harddisk) {
                                Harddisk disk = (Harddisk) resource.get(Constant.Harddisk);
                                double amount = value_trans;
                                disk.setPrice(Constant.Harddisk_Basic_Price);
                                disk.setAmount((int) amount);
                            }
                            else {
                                Harddisk disk = new Harddisk();
                                double amount = value_trans;
                                disk.setPrice(Constant.Harddisk_Basic_Price);
                                disk.setAmount((int) amount);
                                resource.put(Constant.Harddisk, disk);

                            }
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(harddisk)");
                        return;
                    }
                }

                // bandwidth
                else if (term.equalsIgnoreCase(B4Terms.vm_network_bandwidth.toString()) & (request instanceof Request)) {
                    if (value instanceof CONST) {
                        ((Request) request).setBandwidth((CONST) value);
                        int value_trans = Integer.parseInt(((CONST) value).getValue());
                        LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                        if (resource.containsKey(Constant.Bandwidth)
                                & resource.get(Constant.Bandwidth) instanceof Bandwidth) {
                            Bandwidth bandwidth = (Bandwidth) resource.get(Constant.Bandwidth);
                            double amount = value_trans;
                            bandwidth.setPrice(Constant.Bandwidth_Basic_Price);
                            bandwidth.setAmount((int) amount);
                        }
                        else {
                            Bandwidth bandwidth = new Bandwidth();
                            double amount = value_trans;
                            bandwidth.setPrice(Constant.Bandwidth_Basic_Price);
                            bandwidth.setAmount((int) amount);
                            resource.put(Constant.Bandwidth, bandwidth);

                        }
                        continue;
                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            ((Request) request).setBandwidth(temp);
                            int value_trans = Integer.parseInt(temp.getValue());
                            LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                            if (resource.containsKey(Constant.Bandwidth)
                                    & resource.get(Constant.Bandwidth) instanceof Bandwidth) {
                                Bandwidth bandwidth = (Bandwidth) resource.get(Constant.Bandwidth);
                                double amount = value_trans;
                                bandwidth.setPrice(Constant.Bandwidth_Basic_Price);
                                bandwidth.setAmount((int) amount);
                            }
                            else {
                                Bandwidth bandwidth = new Bandwidth();
                                double amount = value_trans;
                                bandwidth.setPrice(Constant.Bandwidth_Basic_Price);
                                bandwidth.setAmount((int) amount);
                                resource.put(Constant.Bandwidth, bandwidth);

                            }
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(bandwidth)");
                        return;
                    }
                }
                else if (term.equalsIgnoreCase(resources.persistence.toString()) & (request instanceof Request)) {
                    if (value instanceof BOOL) {
                        ((Request) request).setPersistence(((BOOL) value).getValue());
                        continue;
                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            ((Request) request).setPersistence(Boolean.parseBoolean(temp.getValue()));
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be BOOL.(persistence)");
                        return;
                    }
                }
                else if (term.equalsIgnoreCase(core.count.toString()) & (request instanceof Request)) {
                    if (value instanceof CONST) {
                        ((Request) request).setVmNumber((CONST) value);
                        continue;
                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            ((Request) request).setVmNumber(temp);
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(vm_number)");
                        return;
                    }
                }
                else if (term.equalsIgnoreCase(resources.vm_image.toString()) & (request instanceof Request)) {
                    if (value instanceof CONST) {
                        ((Request) request).setImage(((CONST) value).getValue());
                        continue;
                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            ((Request) request).setImage(temp.getValue());
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(vm_image)");
                        return;
                    }
                }
                /*
                 * else if (term.equalsIgnoreCase(common.location.toString()) & (request instanceof Request)) { if
                 * (value instanceof CONST) { ((Request) request).setLocation(((CONST) value).getValue()); continue; }
                 * else if (value instanceof ID) { CONST temp = this.variableDeclr.get(((ID) value).getValue()); if
                 * (temp != null) { ((Request) request).setLocation(temp.getValue()); continue; } else {
                 * LOGGER.error("Unable to get the value of Variable of " + value.toString() + "."); return; } } else {
                 * LOGGER.error("Invalid datastructue format of value that should be CONST.(location)"); return; } }
                 */
                // TODO: architecture should be in the SLA model later
                else if (term.equalsIgnoreCase(Constant.$cup_architecture) & (request instanceof Request)) {
                    if (value instanceof CONST) {
                        LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                        if (resource.containsKey(Constant.CPU) & resource.get(Constant.CPU) instanceof CPU) {
                            CPU cpu = (CPU) resource.get(Constant.CPU);
                            cpu.setArchitecture(((CONST) value).getValue());
                        }
                        else {
                            CPU cpu = new CPU();
                            cpu.setArchitecture(((CONST) value).getValue());
                            resource.put(Constant.CPU, cpu);
                        }
                        continue;
                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                            if (resource.containsKey(Constant.CPU) & resource.get(Constant.CPU) instanceof CPU) {
                                CPU cpu = (CPU) resource.get(Constant.CPU);
                                cpu.setArchitecture(temp.getValue());
                            }
                            else {
                                CPU cpu = new CPU();
                                cpu.setArchitecture(temp.getValue());
                                resource.put(Constant.CPU, cpu);
                            }
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(architecture)");
                        return;
                    }
                }
                else if (term.equalsIgnoreCase(Constant.$memory_redundancy) & (request instanceof Request)) {
                    if (value instanceof BOOL) {
                        LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                        if (resource.containsKey(Constant.Memory) & resource.get(Constant.Memory) instanceof Memory) {
                            Memory memory = (Memory) resource.get(Constant.Memory);
                            memory.setMemory_redundancy(((BOOL) value).getValue());
                        }
                        else {
                            Memory memory = new Memory();
                            memory.setMemory_redundancy(((BOOL) value).getValue());
                            resource.put(Constant.Memory, memory);
                        }
                        continue;
                    }
                    else if (value instanceof ID) {
                        CONST temp = this.variableDeclr.get(((ID) value).getValue());
                        if (temp != null) {
                            LinkedHashMap<String, Resource> resource = ((Request) request).getResource();
                            if (resource.containsKey(Constant.Memory) & resource.get(Constant.Memory) instanceof Memory) {
                                Memory memory = (Memory) resource.get(Constant.Memory);
                                memory.setMemory_redundancy(Boolean.parseBoolean(temp.getValue()));
                            }
                            else {
                                Memory memory = new Memory();
                                memory.setMemory_redundancy(Boolean.parseBoolean(temp.getValue()));
                                resource.put(Constant.Memory, memory);
                            }
                            continue;
                        }
                        else {
                            LOGGER.error("Unable to get the value of Variable of " + value.toString() + ".");
                            return;
                        }
                    }
                    else {
                        LOGGER.error("Invalid datastructue format of value that should be CONST.(memory_redundancy)");
                        return;
                    }
                }

            }

        }
        else if (ce instanceof CompoundConstraintExpr) {
            /*
             * CompoundConstraintExpr cce = (CompoundConstraintExpr) ce; STND op = cce.getLogicalOp(); int OP =
             * op.equals(core.not) ? 1 : op.equals(core.and) ? 2 : op.equals(core.or) ? 3 : 0; ConstraintExpr[] sub =
             * cce.getSubExpressions(); switch (OP) { case 1: if (sub.length == 1) { x_RENDER(sub[0]); } break; case 2:
             * case 3: if (sub.length >= 2) { for (int i = 0; i < sub.length; i++) { x_RENDER(sub[i]); } } break; }
             */
            LOGGER.error("Sorry, POC dose not support more than one constraint to a specific term.");
            return;
        }
    }

    /**
     * Gets the resource request.
     */
    public LinkedHashMap<String, Request> getResourceRequest() {
        return resourceRequest;
    }

    /**
     * Clean the request with 0 VM
     */
    public void requestCleaner() {
        ArrayList<String> revomeList = new ArrayList<String>();
        for (String key : this.resourceRequest.keySet()) {
            if (Integer.valueOf(this.resourceRequest.get(key).getVmNumber().getValue()) == 0) {
                revomeList.add(key);
            }
            else
                continue;
        }
        for (String key : revomeList) {
            this.resourceRequest.remove(key);
        }
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
