package org.slasoi.isslam.poc;

import org.slasoi.gslam.core.builder.PlanningOptimizationBuilder;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.isslam.poc.impl.PlanningOptimizationImpl;

/**
 * The <code>PlanningOptimizationServices</code> class represents creating of <code>PlanningOptimizationImpl</code>
 * instance.
 * 
 * @author Kuan Lu
 */
public class PlanningOptimizationServices implements PlanningOptimizationBuilder {
    /**
     * Initializes a newly created <code>PlanningOptimizationImpl</code> object.
     */
    public PlanningOptimization create() {
        return new PlanningOptimizationImpl();
    }
}
