package org.slasoi.isslam.poc.planhandler.exceptions;

/**
 * The <code>InvalidSLAFormatException</code> class represents an exception that an SLA is with an incorrect format.
 * 
 * @author Kuan Lu
 */
public class InvalidSLAFormatException extends Exception {

    private static final long serialVersionUID = 2741556597805356115L;

    public InvalidSLAFormatException() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public InvalidSLAFormatException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public InvalidSLAFormatException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public InvalidSLAFormatException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
