/**
 * 
 */
package org.slasoi.isslam.poc.resourceplanneroptimizer;

/**
 * The <code>ResourcePlannerOptimizer</code> interface represents resource planning and optimization processes.
 * 
 * @author Kuan Lu
 */
public interface ResourcePlannerOptimizer {
    /**
     * Makes plan for each input OCCI
     * 
     */
    public boolean /* OCCI */getPlanForEachOCCI(/* OCCI */);

    /**
     * Selects the best option
     */
    public boolean /* OCCI */getBestOCCI(/* List<OCCI> */);
}
