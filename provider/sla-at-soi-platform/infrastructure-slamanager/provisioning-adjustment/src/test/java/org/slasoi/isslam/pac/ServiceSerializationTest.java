package org.slasoi.isslam.pac;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.slasoi.isslam.pac.types.MetricTimestamp;
import org.slasoi.isslam.pac.types.Service;

public class ServiceSerializationTest {
    private static final Logger logger = Logger.getLogger(ServiceSerializationTest.class.getName());
    private static final String TIMESTAMP_1 = "1282984362492";
    private static final String METRIC_NAME_1 = "cpuusagehistorylastMinute";
    private static final String METRIC_VALUE_1 = "32";
    private static final String TIMESTAMP_2 = "1282984392491";
    private static final String METRIC_NAME_2 = "cpuusagehistorylastMinute";
    private static final String METRIC_VALUE_2 = "320";

    @Test
    public void testSerialization() {

        Service service = createService();
        String str = service.toXML();
        logger.info(str);

        Service serviceAfter = Service.fromXML(str);

        // assertEquals(reporting.toString(), reportingAfter.toString());
    }

    public Service createService() {
        logger.info("Creating new Service...");

        Service service = new Service();
        service.addMetric(new MetricTimestamp(TIMESTAMP_1, METRIC_NAME_1, METRIC_VALUE_1));
        service.addMetric(new MetricTimestamp(TIMESTAMP_2, METRIC_NAME_2, METRIC_VALUE_2));
        logger.info(service);

        return service;
    }

}
