package org.slasoi.isslam.pac;

//Detects a violation indicating that the availability of a given VM does not fulfill the SLA, and restarts it.
rule "Violation on Availability"
@id(11000)
@name("Violation on Availability")
@rulebody(rule "Violation on Availability" 
    when
        $event: Event(type == EventType.MonitoringEventMessage, $sla : event.eventPayload.monitoringResultEvent.monitoredSLA);
        
    	$violation : SLAViolationEvent( type == "availability", $infrastructureId : infrastructureId, $availability : value);
		$ism : InfrastructureServiceManager();
    then
		$ism.restart($infrastructureId);
        retract($violation);
end)
    when
        $event: Event(type == EventType.MonitoringEventMessage, $sla : event.eventPayload.monitoringResultEvent.monitoredSLA);
        
    	$violation : SLAViolationEvent( type == "availability", $infrastructureId : infrastructureId, $availability : value);
		$ism : InfrastructureServiceManager();
    then
		$ism.restart($infrastructureId);
        retract($violation);
end


//Detects a violation of the guaranteed memory, loads the original ProvisionRequest, resizes its memory, and triggers a reprovision
rule "Violation on average memory"
@id(11001)
@name("Violation on average memory")
@rulebody(rule "Violation on average memory" 
    when
    	$violation : SLAViolationEvent( type == GTType.MemoryLastHour, $slaId : slaId, $memory : value);
		InfrastructureSLA(slaId == $slaId, $infrastructureID : infrastructureID);
		$provision : ProvisionRequest(infrastructureID == $infrastructureID);
		$ism : InfrastructureServiceManager();
    then
        modify($provision){setMemory($memory)};
		$ism.reprovision($provision);
        retract($violation);
end)
    when
    	$violation : SLAViolationEvent( type == GTType.MemoryLastHour, $slaId : slaId, $memory : value);
		InfrastructureSLA(slaId == $slaId, $infrastructureID : infrastructureID);
		$provision : ProvisionRequest(infrastructureID == $infrastructureID);
		$ism : InfrastructureServiceManager();
    then
        modify($provision){setMemory($memory)};
		$ism.reprovision($provision);
        retract($violation);
end

//Detects a violation of the guaranteed CPU, loads the original ProvisionRequest, resizes it, and triggers a reprovision
rule "Violation on CPU"
@id(11002)
@name("Violation on CPU")
@rulebody(rule "Violation on CPU" 
    when
    	$violation : SLAViolationEvent( type == GTType.CPULastHour, $slaId : slaId, $cpu : value);
		InfrastructureSLA(slaId == $slaId, $infrastructureID : infrastructureID);
		$provision : ProvisionRequest(infrastructureID == $infrastructureID);
		$ism : InfrastructureServiceManager();
    then
        modify($provision){setMemory(1.5*$memory)};
		$ism.reprovision($provision);
        retract($violation);
end)
    when
    	$violation : SLAViolationEvent( type == GTType.CPULastHour, $slaId : slaId, $cpu : value);
		InfrastructureSLA(slaId == $slaId, $infrastructureID : infrastructureID);
		$provision : ProvisionRequest(infrastructureID == $infrastructureID);
		$ism : InfrastructureServiceManager();
    then
        modify($provision){setMemory(1.5*$memory)};
		$ism.reprovision($provision);
        retract($violation);
end


//If more than 5 CPU warnings arrive in 10 min, loads the original ProvisionRequest, resizes its memory, and triggers a reprovision
rule "Adjustment for CPU warnings"
@id(11003)
@name("Adjustment for CPU warnings")
@rulebody(rule "Adjustment for CPU warnings" 
    when
		InfrastructureSLA($infrastructureID : infrastructureID);
		Number($counter : longValue > 5) from accumulate(
		    $warning: SLAWarningEvent(type == "average cpu", infrastructureId == infrastructureID) over window:time( 10m ) ,
			count( $warning ));
		$provision : ProvisionRequest(infrastructureID == $infrastructureID, $memory : memory);
		$ism : InfrastructureServiceManager();
    then
        modify($provision){setMemory(2*$memory)};
		$ism.reprovision($provision);
end)
    when
		InfrastructureSLA($infrastructureID : infrastructureID);
		Number($counter : longValue > 5) from accumulate(
		    $warning: SLAWarningEvent(type == "average cpu", infrastructureId == infrastructureID) over window:time( 10m ) ,
			count( $warning ));
		$provision : ProvisionRequest(infrastructureID == $infrastructureID, $memory : memory);
		$ism : InfrastructureServiceManager();
    then
        modify($provision){setMemory(2*$memory)};
		$ism.reprovision($provision);
end

//If more than 5 memory warnings arrive in 10 min, loads the original ProvisionRequest, resizes its memory, and triggers a reprovision
rule "Adjustment for memory warnings"
@id(11004)
@name("Adjustment for memory warnings")
@rulebody(rule "Adjustment for memory warnings" 
    when
    when
		InfrastructureSLA($infrastructureID : infrastructureID);
		Number($counter : longValue > 5) from accumulate(
		    $warning: SLAWarningEvent(type == "average memory", infrastructureId == infrastructureID) over window:time( 10m ) ,
			count( $warning ));
		$provision : ProvisionRequest(infrastructureID == $infrastructureID, $memory : memory);
		$ism : InfrastructureServiceManager();
    then
        modify($provision){setMemory(2*$memory)};
		$ism.reprovision($provision);
end)
    when
		InfrastructureSLA($infrastructureID : infrastructureID);
		Number($counter : longValue > 5) from accumulate(
		    $warning: SLAWarningEvent(type == "average memory", infrastructureId == infrastructureID) over window:time( 10m ) ,
			count( $warning ));
		$provision : ProvisionRequest(infrastructureID == $infrastructureID, $memory : memory);
		$ism : InfrastructureServiceManager();
    then
        modify($provision){setMemory(2*$memory)};
		$ism.reprovision($provision);
end