package org.slasoi.isslam.pac;

import org.apache.log4j.Logger;
import org.slasoi.gslam.pac.Agent;
import org.slasoi.gslam.pac.ProvisioningAndAdjustment;
import org.slasoi.infrastructure.servicemanager.Infrastructure;

public class InfrastructureProvisioningAdjustment extends ProvisioningAndAdjustment {

    protected static Logger logger = Logger.getLogger(InfrastructureProvisioningAdjustment.class.getName());
    protected Infrastructure iServiceManager = null;

    protected static String INFRA_SERVICE_MANAGER_ID = "INFRA_SERVICE_MANAGER_ID"; // Only one ServiceManager in Y2
    
    protected String pacID = "IPAC";

    public InfrastructureProvisioningAdjustment() {
        super();
        logger.info("Creating infrastructure PAC (default)...");
    }

    public InfrastructureProvisioningAdjustment(String configurationFile) {
        super(configurationFile);
        logger.info("Creating infrastructure PAC (file)...");
    }

    public void setInfrastructureServiceManager(Infrastructure iServiceManager) {
        this.iServiceManager = iServiceManager;
    }

    public Infrastructure getInfrastructureServiceManager() {
        return iServiceManager;
    }

    protected void addManagedElements(Agent agent) {
        logger.info(String.format( "InfrastructurePAC, adding Service Manager (id=%s,instance=%s)", INFRA_SERVICE_MANAGER_ID,iServiceManager));
        agent.addManagedElement(INFRA_SERVICE_MANAGER_ID, iServiceManager);
    }
    
    public String toString()
    {
        return String.format( "%s (id=%s, ism=%s)", super.toString(), pacID, iServiceManager );
    }

}
