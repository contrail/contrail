package org.slasoi.isslam.pac;

public enum InfrastructureAdjustmentType {
    REPROVISION,
    START_RESOURCE,
    STOP_RESOURCE,
    STOP,
    GET_DETAILS
}
