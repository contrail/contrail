/**
 * 
 */
package org.slasoi.isslam.pac;

import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;

/**
 * @author fuentes
 * 
 */
public class InfrastructureAdjustmentAction {

    private InfrastructureAdjustmentType type = null;
    private String serviceManagerId = null;
    private String infrastructureID = null;
    private String resourceID = null;
    private ProvisionRequestType provisioningRequest = null;

    public ProvisionRequestType getProvisioningRequest() {
        return provisioningRequest;
    }

    public void setProvisioningRequest(ProvisionRequestType provisioningRequest) {
        this.provisioningRequest = provisioningRequest;
    }

    public String getInfrastructureID() {
        return infrastructureID;
    }

    public void setInfrastructureID(String infrastructureID) {
        this.infrastructureID = infrastructureID;
    }

    public String getServiceManagerId() {
        return serviceManagerId;
    }

    public void setServiceManagerId(String serviceManagerId) {
        this.serviceManagerId = serviceManagerId;
    }

    InfrastructureAdjustmentAction(InfrastructureAdjustmentType type) {
        this.type = type;
    }

    public InfrastructureAdjustmentType getType() {
        return type;
    }

    public void setType(InfrastructureAdjustmentType type) {
        this.type = type;
    }

    public void setResourceID(String resourceID) {
        this.resourceID = resourceID;
    }

    public String getResourceID() {
        return resourceID;
    }

}
