/**
 * 
 */
package org.slasoi.isslam.pac.types;

import org.slasoi.infrastructure.servicemanager.types.Host;

/**
 * @author Beatriz
 * 
 */
public class ISMHost extends Host {
    private String serviceManagerId;

    public static enum Status {
        NEW,
        STORED
    };

    public Status status = Status.NEW;

    public ISMHost(Host host) {
        this.setAddress(host.getAddress());
        this.setId(host.getId());
        this.setState(host.getState());
        this.setTimestamp(host.getTimestamp());
    }

    public ISMHost(Host host, String serviceManagerId) {
        this.setAddress(host.getAddress());
        this.setId(host.getId());
        this.setState(host.getState());
        this.setTimestamp(host.getTimestamp());
        this.serviceManagerId = serviceManagerId;
    }

    public String getServiceManagerId() {
        return serviceManagerId;
    }

    public void setServiceManagerId(String serviceManagerId) {
        this.serviceManagerId = serviceManagerId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String toString() {
        String str =
                "\n" + "id = " + getId() + "\n" + "address = " + getAddress() + "\n" + "timestamp = " + getTimestamp()
                        + "\n" + "state = " + getState().toString() + "\n" + "serviceManagerId = " + serviceManagerId
                        + "\n" + "status = " + status.toString() + "\n";
        return str;
    }
}
