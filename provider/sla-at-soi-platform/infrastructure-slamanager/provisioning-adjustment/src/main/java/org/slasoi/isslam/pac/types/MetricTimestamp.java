/**
 * 
 */
package org.slasoi.isslam.pac.types;

/**
 * @author Beatriz
 * 
 */
public class MetricTimestamp {
    private String value;
    private Metric metric;

    public MetricTimestamp() {

    }

    public MetricTimestamp(String timestamp, Metric metric) {
        this.value = timestamp;
        this.metric = metric;
    }

    public MetricTimestamp(String timestamp, String metricName, String metricValue) {
        this.value = timestamp;
        this.metric = new Metric(metricName, metricValue);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

}
