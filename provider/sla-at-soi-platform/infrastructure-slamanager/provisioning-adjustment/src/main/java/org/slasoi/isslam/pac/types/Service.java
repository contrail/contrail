/**
 * 
 */
package org.slasoi.isslam.pac.types;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;

/**
 * @author Beatriz
 * 
 */
public class Service {
    private static final Logger logger = Logger.getLogger(Service.class.getName());
    private static final String NAME = "name";
    private static final String VALUE = "value";
    private static final String METRICS = "metrics";
    private static final String TIMESTAMP = "timestamp";

    private List<MetricTimestamp> metrics = new ArrayList<MetricTimestamp>();

    public List<MetricTimestamp> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<MetricTimestamp> metrics) {
        this.metrics = metrics;
    }

    public void addMetric(MetricTimestamp metric) {
        metrics.add(metric);
    }

    public static Service fromXML(String messageStr) {
        logger.debug("Service from XML");
        logger.debug(messageStr);

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.isslam.pac.types.Service.class.getSimpleName(),
                org.slasoi.isslam.pac.types.Service.class);
        xstream.alias(org.slasoi.isslam.pac.types.MetricTimestamp.class.getSimpleName(),
                org.slasoi.isslam.pac.types.MetricTimestamp.class);
        xstream.alias(org.slasoi.isslam.pac.types.Metric.class.getSimpleName(),
                org.slasoi.isslam.pac.types.Metric.class);

        xstream.useAttributeFor(MetricTimestamp.class, VALUE);
        xstream.useAttributeFor(Metric.class, NAME);
        xstream.useAttributeFor(Metric.class, VALUE);
        xstream.addImplicitCollection(Service.class, METRICS);
        xstream.alias(TIMESTAMP, org.slasoi.isslam.pac.types.MetricTimestamp.class);

        Service service = new Service();

        service = (Service) xstream.fromXML(messageStr);

        return (service);

    }

    public String toXML() {
        logger.debug("Service to XML");
        logger.debug(this);

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.isslam.pac.types.Service.class.getSimpleName(),
                org.slasoi.isslam.pac.types.Service.class);
        xstream.alias(org.slasoi.isslam.pac.types.MetricTimestamp.class.getSimpleName(),
                org.slasoi.isslam.pac.types.MetricTimestamp.class);
        xstream.alias(org.slasoi.isslam.pac.types.Metric.class.getSimpleName(),
                org.slasoi.isslam.pac.types.Metric.class);

        xstream.useAttributeFor(MetricTimestamp.class, VALUE);
        xstream.useAttributeFor(Metric.class, NAME);
        xstream.useAttributeFor(Metric.class, VALUE);
        xstream.addImplicitCollection(Service.class, METRICS);
        xstream.alias(TIMESTAMP, org.slasoi.isslam.pac.types.MetricTimestamp.class);

        String messageStr = xstream.toXML(this);
        logger.debug(messageStr);

        return (messageStr);
    }
}
