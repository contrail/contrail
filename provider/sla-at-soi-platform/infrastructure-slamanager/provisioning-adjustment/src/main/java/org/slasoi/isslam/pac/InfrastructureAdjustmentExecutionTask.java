/**
 * 
 */
package org.slasoi.isslam.pac;

import org.slasoi.gslam.pac.ManagedElement;
import org.slasoi.gslam.pac.Task;
import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.slasoi.infrastructure.servicemanager.Infrastructure;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.ProvisionException;
import org.slasoi.infrastructure.servicemanager.exceptions.StopException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;

/**
 * Class in charge of executing a given action that in turn means an invocation to some external component (typically,
 * Service Manager)
 * 
 * @author Beatriz Fuentes (TID)
 * 
 */
public class InfrastructureAdjustmentExecutionTask extends Task {

    protected InfrastructureAdjustmentAction action = null;

    public InfrastructureAdjustmentAction getAction() {
        return action;
    }

    public void setAction(InfrastructureAdjustmentAction action) {
        this.action = action;
    }

    /**
     * @param config
     */
    public InfrastructureAdjustmentExecutionTask(TaskConfiguration config, Task parent) {
        super(config);
    }

    /**
     * Needed for RTTI purposes
     */
    public InfrastructureAdjustmentExecutionTask() {
        logger.debug("Creating infrastructure InfrastructureAdjustmentExecutionTask");
    }

    public void run() {

        if (action != null) {
            logger.info("Triggering action " + action.getType() + " to ServiceManager " + action.getServiceManagerId());

            ManagedElement serviceManager = agent.getManagedElement(action.getServiceManagerId());

            if (serviceManager != null) {
                Object element = serviceManager.getElement();
                InfrastructureAdjustmentType type = action.getType();

                if (type == InfrastructureAdjustmentType.GET_DETAILS) {
                    logger.debug("Invoking getDetails on Infrastructure Service Manager...");
                    try {
                        ProvisionResponseType response =
                                ((Infrastructure) element).getDetails(action.getInfrastructureID());
                    }
                    catch (UnknownIdException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                else if (type == InfrastructureAdjustmentType.REPROVISION) {
                    logger.debug("Invoking reprovision on Infrastructure Service Manager...");
                    try {
                        ProvisionResponseType response =
                                ((Infrastructure) element).reprovision(action.getInfrastructureID(), action
                                        .getProvisioningRequest());
                    }
                    catch (DescriptorException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (UnknownIdException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (ProvisionException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                else if (type == InfrastructureAdjustmentType.START_RESOURCE) {
                    logger.debug("Invoking start resource on Infrastructure Service Manager...");
                    try {
                        ((Infrastructure) element).startResource(action.getResourceID());
                    }
                    catch (UnknownIdException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                else if (type == InfrastructureAdjustmentType.STOP_RESOURCE) {
                    logger.debug("Invoking stop resource on Infrastructure Service Manager...");
                    try {
                        ((Infrastructure) element).stopResource(action.getResourceID());
                    }
                    catch (UnknownIdException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                else if (type == InfrastructureAdjustmentType.STOP) {
                    logger.debug("Invoking stop on Infrastructure Service Manager...");
                    try {
                        ((Infrastructure) element).stop(action.getInfrastructureID());
                    }
                    catch (UnknownIdException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (StopException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
