package org.slasoi.isslam.main.beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.slasoi.gslam.core.builder.PlanningOptimizationBuilder;
import org.slasoi.gslam.core.context.GenericSLAManagerServices;
import org.slasoi.gslam.core.context.GenericSLAManagerServices.SLAMConfiguration;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.ism.occi.IsmOcciService;
import org.slasoi.isslam.core.ism.OCCIAware;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.B4Terms;
import org.springframework.osgi.context.BundleContextAware;
import org.springframework.osgi.extensions.annotation.ServiceReference;

/**
 * This class creates infrastructure domain specific SLA manager context by importing generic SLA manager,
 * infrastructure POC, infrastructure PAC and infrastructure service manager.
 * 
 * @author Kuan Lu
 * 
 */
public class IsSLAMBean implements BundleContextAware {
    /*
     * Starts the infrastructure SLA manager.
     */
    public void start() {
        try {
            INSTANCES++;
            SLAMConfiguration isConfig = gslamServices.loadConfigurationFrom("infraslam.instance1.cfg");

            isContext =
                    gslamServices.createContext(osgiContext, isConfig.name + "-" + INSTANCES, isConfig.epr,
                            isConfig.group, isConfig.wsPrefix);
            
            isContext.setProperties( isConfig.properties );
            
            // Inject the ISM into isPOC
            injectOCCI(this.isPOC);

            // Inject the PAC into the isContext
            isContext.setProvisioningAdjustment(isPAC);
            injectIntoContext(isPAC, isContext);

            // Inject the POC into the isContext
            isContext.setPlanningOptimization(isPOC);
            injectIntoContext(isPOC, isContext);
            
            // Inject B4 terms
            isContext.getSLATemplateRegistry().addExtensions(new B4Terms());

            LOGGER.info("\n\n \t*** :: start :: gslamServices >> \n" + gslamServices);
            LOGGER.info("\n\n \t*** :: start :: IsSLAMBean >> \n" + this.isContext);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stops the infrastructure SLA manager.
     */
    public void stop() {
        // FIXME: PENDING: release resources obtained from 'gslamServices.createContext'
    }

    /**
     * Sets the OSGi context of infrastructure SLA manager.
     */
    public void setBundleContext(BundleContext osgiContext) {
        assert (osgiContext != null) : "The OSGi context of infrastructure SLA manager != null.";
        this.osgiContext = osgiContext;

        IsSLAMTracer tracer = new IsSLAMTracer();
        osgiContext.registerService(tracer.getClass().getName(), tracer, null);
    }

    /**
     * Injects the domain specific components into SLA manager context. e.g., IPOC and IPAC.
     */
    protected void injectIntoContext(Object obj, SLAManagerContext context) {
        assert (context != null && obj != null) : "The context of infrastructure SLA manager != null and the object that is injecting into SLAM context !=null.";
        if (obj instanceof SLAMContextAware) {
            ((SLAMContextAware) obj).setSLAManagerContext(context);
        }
    }

    /**
     * Injects the infrastructure service manager into SLA manager context.
     */
    protected void injectOCCI(Object obj) {
        assert (obj != null) : "The OCCI (ISM) context that is injecting into infrastructure SLA manager context != null.";
        if (obj instanceof OCCIAware) {
            ((OCCIAware) obj).setOCCI(iServiceManager);
        }
    }

    /**
     * Sets the generic SLA manager services.
     */
    @ServiceReference
    public void setGslamServices(GenericSLAManagerServices gslamServices) {
        assert (gslamServices != null) : "Generic SLAM services != null.";
        LOGGER.info("generic-slam injected successfully into is-slam");
        this.gslamServices = gslamServices;
    }

    /**
     * Sets the infrastructure service manager.
     */
    @ServiceReference
    public void setInfrastructureServiceManager(IsmOcciService iServiceManager) {
        LOGGER.info("ISM injected successfully into is-slam.");
        assert (iServiceManager != null) : "Infrastructure Service Manager != null.";
        this.iServiceManager = iServiceManager;
    }

    /**
     * Sets IPOC.
     */
    @ServiceReference(filter = "(proxy=is-poc)")
    public void setPOC(PlanningOptimizationBuilder builder) {
        LOGGER.info("is-POC injected successfully into is-slam");
        assert (builder != null) : "PlanningOptimizationBuilder instance != null.";
        this.isPOC = builder.create();
    }

    /**
     * Sets IPAC.
     */
    @ServiceReference(filter = "(proxy=is-pac)")
    public void setPAC(ProvisioningAdjustment isPAC) {
        LOGGER.info("is-PAC injected successfully into is-slam");
        assert (isPAC != null) : "ProvisioningAdjustment instance != null.";
        this.isPAC = isPAC;
    }

    protected SLAManagerContext isContext;

    protected PlanningOptimization isPOC;
    protected ProvisioningAdjustment isPAC;
    protected IsmOcciService iServiceManager;

    protected GenericSLAManagerServices gslamServices;

    protected BundleContext osgiContext;

    private static int INSTANCES = 0;

    private static final Logger LOGGER = Logger.getLogger(IsSLAMBean.class);

    public class IsSLAMTracer {
        /**
         * method to be invoked from osgi-console via 'echo' command
         */
        public void context() {
            System.out.println(isContext);
        }

        public void ism() {
            System.out.println(iServiceManager);
        }

        /**
         * method to be invoked from osgi-console via 'echo' command
         */
        public void slamID() {
            try {
                System.out.println("\t\t isSLAM-ID = " + isContext.getSLAManagerID());
            }
            catch (Exception e) {
            }
        }

        /**
         * method to be invoked from osgi-console via 'invoke' command
         */
        public void values(Hashtable<String, String> params) {
            assert (params != null) : "Infrastructure SLAM-params != null.";
            System.out.println("\t\t isSLAM-params = " + params);
        }

        /**
         * method to test IPOC
         */
        public void ipocNegotiationTesting() {
            LOGGER.info("**************************************************************************");
            LOGGER.info("**************************NEGOTIATION PHASE*******************************");
            LOGGER.info("**************************************************************************");
            FileReader read;
            try {
                read =
                        new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                                + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                                + "a4-infrastructure-template.xml");

                BufferedReader br = new BufferedReader(read);
                StringBuffer sb = new StringBuffer();
                String row;
                while ((row = br.readLine()) != null) {
                    sb.append(row);
                }
                // SLASOITemplateParser slasoieTemplatParser = new SLASOITemplateParser();
                SLATemplate slat;
                Iterator<ISyntaxConverter> it = isContext.getSyntaxConverters().values().iterator();
                ISyntaxConverter sc = it.next();
                slat = (SLATemplate) (sc.parseSLATemplate(sb.toString()));
                isContext.getPlanningOptimization().getIAssessmentAndCustomize().negotiate("", slat);
            }
            catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        /**
         * method to test IPOC
         */
        public void ipocProvisionTesting() {
            LOGGER.info("**************************************************************************");
            LOGGER.info("**************************PROVISIONING PHASE*******************************");
            LOGGER.info("**************************************************************************");
            FileReader read;
            try {
                read =
                        new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                                + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                                + "A4_SLA-1-VM-Correct.xml");
            

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }

            Iterator<ISyntaxConverter> it = isContext.getSyntaxConverters().values().iterator();
            ISyntaxConverter sc = it.next();

            SLA sla = (SLA) sc.parseSLA(sb.toString());
            isContext.getPlanningOptimization().getINotification().activate(sla);
            }
            catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        /**
         * method to test IPOC
         */
        public void ipocViolationTesting() {
            LOGGER.info("**************************************************************************");
            LOGGER.info("***************************VIOLATION PHASE********************************");
            LOGGER.info("**************************************************************************");
            FileReader read;
            try {
                read =
                    new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                            + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                            + "a4-infrastructure-template.xml");

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }
            
            Iterator<ISyntaxConverter> it = isContext.getSyntaxConverters().values().iterator();
            ISyntaxConverter sc = it.next();

            SLATemplate slat = (SLATemplate) sc.parseSLATemplate(sb.toString());
            
            LOGGER.info("***************************Start to create an agreement********************************");
            SLA sla = isContext.getPlanningOptimization().getIAssessmentAndCustomize().createAgreement("69e2639f-cab6-4770-a061-146806be1128", slat);
            LOGGER.info("***************************Agreement is created********************************");
            LOGGER.info("***************************SLA is service is provisioning********************************");
            isContext.getPlanningOptimization().getINotification().activate(sla);
            LOGGER.info("***************************SLA is service is provisioned********************************");
            LOGGER.info("***************************Violation happens********************************");
            // isContext.getProvisioningAdjustment().renegotiation();
            }
            catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }
}
