/* 
SVN FILE: $Id: $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: $
@version        $Rev: $
@lastrevision   $Date: $
@filesource     $URL: $

*/

package org.slasoi.slamodel.vocab;

import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.vocab.ext.Category;
import org.slasoi.slamodel.vocab.ext.Extensions;
import org.slasoi.slamodel.vocab.ext.Functional;
import org.slasoi.slamodel.vocab.ext.Nominal;

public class resources implements Extensions{
    
    public static final String $base = "http://www.slaatsoi.org/resources#";

    // STRING VERSIONS
    public static final String $vm_cores = $base + "vm_cores";
    public static final String $cpu_speed = $base + "cpu_speed";
    public static final String $memory = $base + "memory";
    public static final String $persistence = $base + "persistence";
    public static final String $vm_image = $base + "vm_image";
    public static final String $vs_location = $base + "vs_location";
    public static final String $storage_location = $base + "storage_location";
    public static final String $vm_cpu_load = $base + "vm_cpu_load";
    public static final String $reserve = $base + "reserve";
    public static final String $co_location_rack = $base + "co_location_rack";
    public static final String $not_co_location_host = $base + "not_co_location_host";
    public static final String $availability = $base + "availability";
    public static final String $reliability = $base + "reliability";
    public static final String $minimum_loa = $base + "minimum_loa";
    
    // STND VERSIONS
    public static final STND vm_cores = new STND($vm_cores);
    public static final STND cpu_speed = new STND($cpu_speed);
    public static final STND memory = new STND($memory);
    public static final STND persistence = new STND($persistence);
    public static final STND vm_image = new STND($vm_image);
    public static final STND vs_location = new STND($vs_location);
    public static final STND storage_location = new STND($storage_location);
    public static final STND vm_cpu_load = new STND($vm_cpu_load);
    public static final STND reserve = new STND($reserve);
    public static final STND co_location_rack = new STND($co_location_rack);
    public static final STND not_co_location_host = new STND($not_co_location_host);
    public static final STND availability = new STND($availability);
    public static final STND reliability = new STND($reliability);
    public static final STND minimum_loa = new STND($minimum_loa);
    
    /*--------- EXTENSIONS IMPLEMENTATION ---------------------------------------------------------------*/
    
    private UUID uuid = new UUID($base);
    private static final Nominal[] _nominals = new Nominal[]{
        // NOMINAL DEFINITIONS GO HERE
    };
    private static final Functional[] _predicates = new Functional[]{
        new Functional(vm_cores, new Category[]{ meta.SERVICE }, meta.COUNT, false),
        new Functional(cpu_speed, new Category[]{ meta.SERVICE }, meta.FREQUENCY, false),
        new Functional(memory, new Category[]{ meta.SERVICE }, meta.QUANTITY, false),
        new Functional(persistence, new Category[]{ meta.SERVICE }, meta.BOOLEAN, false),
        new Functional(vm_image, new Category[]{ meta.SERVICE }, meta.UUID, false),
        new Functional(vs_location, new Category[]{ meta.SERVICE }, meta.TEXT, false),
        new Functional(storage_location, new Category[]{ meta.SERVICE }, meta.TEXT, false),
        new Functional(vm_cpu_load, new Category[]{ meta.SERVICE }, meta.NUMBER, false),
        new Functional(reserve, new Category[]{ meta.SERVICE }, meta.NUMBER, false),
        new Functional(co_location_rack, new Category[]{ meta.SERVICE }, meta.BOOLEAN, false),
        new Functional(not_co_location_host, new Category[]{ meta.SERVICE }, meta.BOOLEAN, false),
        new Functional(availability, new Category[]{ meta.SERVICE }, meta.NUMBER, false),
        new Functional(reliability, new Category[]{ meta.SERVICE }, meta.NUMBER, false),
        new Functional(minimum_loa, new Category[]{ meta.SERVICE }, meta.NUMBER, false),
    };
    public UUID getUuid(){ return uuid; }
    public Nominal[] getNominals(){ return _nominals; }
    public Functional[] getFunctionals(){ return _predicates; }
    public String[] getDays(){ return new String[0]; }
    public String[] getMonths(){ return new String[0]; }

}
