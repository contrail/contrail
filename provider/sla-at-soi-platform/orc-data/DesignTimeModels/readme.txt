
NOTE: 
As a user of the framework, you usually don't need to bother about these models since they 
are already integrated into the ServiceEvaluation component. 
Still, for interested, here's a quick intro:

The models can be seen as internal system representation and serve as the central basis for 
design time prediction of QoS. They are created when a service's quality is to be evaluated. 
For that, terms from the SLA(T) are extracted and fed into the model. 

It is important to understand that the overall system design is supported before negotiation. 
At negotiation time, the model is calibrated according to the actual SLA QoS offering / demand. 
Then, terms are predicted and passed back.



=============================================================
Dipl.-Inform. Martin K�ster
Software Engineering (SE)
Tel.: +49-721-9654-634
Fax: +49-721-9654-635
E-Mail: kuester@fzi.de
=============================================================
FZI Forschungszentrum Informatik an der Universit�t Karlsruhe
Haid-und-Neu-Str. 10-14, D-76131 Karlsruhe
Tel.: +49-721-9654-0, 
Fax: +49-721-9654-959
Stiftung des b�rgerlichen Rechts
Stiftung Az: 14-0563.1 Regierungspr�sidium Karlsruhe

Vorstand: 
Prof. Dr.-Ing. R�diger Dillmann
Dipl. Wi.-Ing. Michael Flor
Prof. Dr. rer. nat. Dr. h.c. Wolffried Stucky
Prof. Dr. rer. nat. Rudi Studer	

Vorsitzender des Kuratoriums: 
Ministerialdirigent G�nther Le�nerkraus
=============================================================
