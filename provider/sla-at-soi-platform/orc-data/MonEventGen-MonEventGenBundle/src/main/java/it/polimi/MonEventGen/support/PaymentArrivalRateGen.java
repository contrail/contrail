package it.polimi.MonEventGen.support;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;


public class PaymentArrivalRateGen implements Runnable {

	private String channel;
	private PubSubManager manager;
	
	
	private int runs;
	private long[] ts;
	private int tsLength;
	private String PaymentServiceProcessName;
	private String PaymentServiceOperationName;
	private String PaymentServiceIp;
	private String PaymentServiceEpr;
	
	public PaymentArrivalRateGen(PubSubManager manager, String channel, int runsPerRunner, long[] timestamp) {
		super();
		runs = runsPerRunner;
		ts = timestamp;
		tsLength = ts.length;
		this.channel = channel;
		this.manager = manager;
	}

	public void run() {
		
		EventConfiguration config = new EventConfiguration();
		
		config.setNotifier("sla@soi_bpel_instrumentation");
		config.setServicename(PaymentServiceProcessName);
		config.setOpName(PaymentServiceOperationName);
		
		config.setSourceName("AnonymousName");
		config.setSourceEpr("AnonymousEpr");
		config.setSourceIp("AnonymousIp");
		
		config.setDestName(PaymentServiceProcessName);
		config.setDestIp(PaymentServiceIp);
		config.setDestEpr(PaymentServiceEpr);
		
		for (int i=0; i< runs; i++) {

			config.setTimestamp(ts[i%tsLength]);
			
			String receiveMsg = EventFactory.createReceiveEvent(config);
			
			PubSubMessage message = new PubSubMessage(channel, receiveMsg);
			try {
				manager.publish(message);
				System.out.println("[GEN] Payment arrival event has been sent.");
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
	

	
	

}
