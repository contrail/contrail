package it.polimi.MonEventGen.support;

public class EventConfiguration {
	
	private long timestamp;
	private String notifier;
	
	private String sourceIp;
	private String sourceEpr;
	private String sourceName;
	
	private String destIp;
	private String destEpr;
	private String destName;
	
	private String servicename; 
	private String opName;
	
	
	
	
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public String getDestName() {
		return destName;
	}
	public void setDestName(String destName) {
		this.destName = destName;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getNotifier() {
		return notifier;
	}
	public void setNotifier(String notifier) {
		this.notifier = notifier;
	}
	public String getSourceEpr() {
		return sourceEpr;
	}
	public void setSourceEpr(String sourceEpr) {
		this.sourceEpr = sourceEpr;
	}
	public String getDestEpr() {
		return destEpr;
	}
	public void setDestEpr(String destEpr) {
		this.destEpr = destEpr;
	}

	public String getOpName() {
		return opName;
	}
	public void setOpName(String opName) {
		this.opName = opName;
	}
	public String getSourceIp() {
		return sourceIp;
	}
	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}
	public String getDestIp() {
		return destIp;
	}
	public void setDestIp(String destIp) {
		this.destIp = destIp;
	}
	public String getServicename() {
		return servicename;
	}
	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	
	

}
