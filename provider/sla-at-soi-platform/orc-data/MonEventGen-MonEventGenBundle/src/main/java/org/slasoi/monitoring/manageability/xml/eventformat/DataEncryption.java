//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0.3-b24-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.05.05 at 10:58:30 AM BST 
//


package org.slasoi.monitoring.manageability.xml.eventformat;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataEncryption complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataEncryption">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="encryptionDef" type="{http://www.slaatsoi.org/commonTerms}encryptionArtefact"/>
 *         &lt;element name="resourceDef" type="{http://www.slaatsoi.org/commonTerms}resourceType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataEncryption", propOrder = {
    "encryptionDefAndResourceDef"
})
public class DataEncryption {

    @XmlElements({
        @XmlElement(name = "encryptionDef", required = true, type = EncryptionArtefact.class),
        @XmlElement(name = "resourceDef", required = true, type = String.class)
    })
    protected List<Object> encryptionDefAndResourceDef;

    /**
     * Gets the value of the encryptionDefAndResourceDef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the encryptionDefAndResourceDef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEncryptionDefAndResourceDef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EncryptionArtefact }
     * {@link String }
     * 
     * 
     */
    public List<Object> getEncryptionDefAndResourceDef() {
        if (encryptionDefAndResourceDef == null) {
            encryptionDefAndResourceDef = new ArrayList<Object>();
        }
        return this.encryptionDefAndResourceDef;
    }

}
