//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0.3-b24-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.05.05 at 10:58:30 AM BST 
//


package org.slasoi.monitoring.manageability.xml.eventformat;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for DataRateUnitKind.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DataRateUnitKind">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="b_per_s"/>
 *     &lt;enumeration value="Kb_per_s"/>
 *     &lt;enumeration value="Mb_per_s"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum DataRateUnitKind {

    @XmlEnumValue("b_per_s")
    B_PER_S("b_per_s"),
    @XmlEnumValue("Kb_per_s")
    KB_PER_S("Kb_per_s"),
    @XmlEnumValue("Mb_per_s")
    MB_PER_S("Mb_per_s");
    private final String value;

    DataRateUnitKind(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DataRateUnitKind fromValue(String v) {
        for (DataRateUnitKind c: DataRateUnitKind.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
