//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0.3-b24-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.05.05 at 10:58:30 AM BST 
//


package org.slasoi.monitoring.manageability.xml.eventformat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SimpleArgument complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SimpleArgument">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ArgType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Direction" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}anySimpleType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleArgument", namespace = "http://slasoi.org/monitoring/xml/eventformat", propOrder = {
    "argName",
    "argType",
    "direction",
    "value"
})
public class SimpleArgument {

    @XmlElement(name = "ArgName", namespace = "http://slasoi.org/monitoring/xml/eventformat", required = true)
    protected String argName;
    @XmlElement(name = "ArgType", namespace = "http://slasoi.org/monitoring/xml/eventformat", required = true)
    protected String argType;
    @XmlElement(name = "Direction", namespace = "http://slasoi.org/monitoring/xml/eventformat", required = true)
    protected String direction;
    @XmlElement(name = "Value", namespace = "http://slasoi.org/monitoring/xml/eventformat", required = true)
    protected Object value;

    /**
     * Gets the value of the argName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArgName() {
        return argName;
    }

    /**
     * Sets the value of the argName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArgName(String value) {
        this.argName = value;
    }

    /**
     * Gets the value of the argType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArgType() {
        return argType;
    }

    /**
     * Sets the value of the argType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArgType(String value) {
        this.argType = value;
    }

    /**
     * Gets the value of the direction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirection() {
        return direction;
    }

    /**
     * Sets the value of the direction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirection(String value) {
        this.direction = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setValue(Object value) {
        this.value = value;
    }

}
