//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0.3-b24-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.05.05 at 10:58:30 AM BST 
//


package org.slasoi.monitoring.manageability.xml.eventformat;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for TxRateUnitKind.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TxRateUnitKind">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="tx_per_s"/>
 *     &lt;enumeration value="tx_per_m"/>
 *     &lt;enumeration value="tx_per_h"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum TxRateUnitKind {

    @XmlEnumValue("tx_per_s")
    TX_PER_S("tx_per_s"),
    @XmlEnumValue("tx_per_m")
    TX_PER_M("tx_per_m"),
    @XmlEnumValue("tx_per_h")
    TX_PER_H("tx_per_h");
    private final String value;

    TxRateUnitKind(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TxRateUnitKind fromValue(String v) {
        for (TxRateUnitKind c: TxRateUnitKind.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
