<?xml version="1.0" encoding="UTF-8"?>
<scm:Landscape xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:scm="http://scm.slasoi.org/1.0">
  <providedTypes ID="e19cd2cb-07b1-432b-bd82-ffa33f3fb0fc" ServiceTypeName="ORC_ServiceType" Description="ORC_Services">
    <Interfaces>PaymentService</Interfaces>
    <Interfaces>ORCInventoryService</Interfaces>
  </providedTypes>
  <implementations type="e19cd2cb-07b1-432b-bd82-ffa33f3fb0fc" ID="cd26ab65-743f-4b5f-bfeb-535557a04b7f" ServiceImplementationName="ORC_AllInOne" Description="ORC_AllInOne" Version="1">
    <artefacts xsi:type="scm:VirtualAppliance" ID="a6f783df-fcc3-487a-b5da-7bbb82920431" Name="ORC_AllInOneServer" Desciption="ORC_AllInOneServer" HyperVisor="undefined" Version="1" HypervisorVersion="undefined" ApplianceType="X86" ImageFormat="to be defined by Intel">
      <dependencies Name="AllInOne Infrastructure" TargetURI="" targetType="b2c3f591-f2ca-42b3-92a0-955ba2b36035" ID=""/>
      <serviceFeatures ID="" ConfigType="PropertyFile" ConfigFile="c:\AppServer\setup.properties" ParameterIdentifier="threadpool.size" DefaultValue="4"/>
    </artefacts>
    <provisioningInformation LeadTime="5" bootOrder="a6f783df-fcc3-487a-b5da-7bbb82920431"/>
  </implementations>
  <implementations type="e19cd2cb-07b1-432b-bd82-ffa33f3fb0fc" ID="2535f99b-b2db-46f6-be7d-6c7fb15424c1" ServiceImplementationName="ORC_Distributed" Description="ORC_Distributed" Version="1">
    <artefacts xsi:type="scm:VirtualAppliance" ID="5a02812d-64b6-4ad6-a220-5745245277c9" Name="ORC_DB" HyperVisor="KVM" Version="1" HypervisorVersion="0815" ApplianceType="X86" ImageFormat=".kvm">
      <dependencies Name="DB Infrastructure" Description="" TargetURI="" targetType="b2c3f591-f2ca-42b3-92a0-955ba2b36035" ID=""/>
    </artefacts>
    <artefacts xsi:type="scm:VirtualAppliance" ID="bf0e17df-4ff8-43ce-abed-92f3f637075c" Name="ORC_Services" HyperVisor="KVM" Version="1" HypervisorVersion="0815" ApplianceType="X86" ImageFormat=".kvm">
      <dependencies Name="Application Server Infrastructure" Description="" TargetURI="" targetType="b2c3f591-f2ca-42b3-92a0-955ba2b36035" ID=""/>
      <serviceFeatures ID="" ConfigType="PropertyFile" ConfigFile="c:\AppServer\setup.properties" ParameterIdentifier="threadpool.size" DefaultValue="4"/>
    </artefacts>
    <provisioningInformation LeadTime="20" bootOrder="5a02812d-64b6-4ad6-a220-5745245277c9 bf0e17df-4ff8-43ce-abed-92f3f637075c"/>
  </implementations>
  <requiredTypes ID="b2c3f591-f2ca-42b3-92a0-955ba2b36035" ServiceTypeName="Infrastructure" Description="Host for Virtual Machines">
    <Interfaces>CPU</Interfaces>
    <Interfaces>HDD</Interfaces>
    <Interfaces>MEM</Interfaces>
    <Interfaces>NET</Interfaces>
  </requiredTypes>
</scm:Landscape>
