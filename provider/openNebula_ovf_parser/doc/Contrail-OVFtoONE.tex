\documentclass[a4paper,10pt]{article}
\usepackage{lscape}
% define the title
\author{Gregor~Beslic, XLAB d.o.o.}
\title{OpenNebula OVF parser, v 0.1}
\begin{document}
% generates the title
\maketitle
% insert the table of contents
\tableofcontents
\setlength{\parskip}{6pt}
\newcommand{\tab}{\hspace*{1em}}

\section{General notes}
The OVF parser for OpenNebula is developed as part of the Contrail project. A user will submit the .ovf file and creata a Virtual Appliance as per the specification in the file. Each virtual applicance will consist of 
one or more virtual machines. The parser should be able to parse the elements as described in the following sections and leverage OpenNebula to provision the required infrastructure. Where possible, the 
parser should make use of OpenNebula VM templates:\\http://opennebula.org/documentation:archives:rel2.0:template and make working with disks as simple as possible.

The network used by the OVF Appliance (VirtualSystemsCollection) should be created by the system administrator before the actual parsing takes place. It should be made public, so all users can use it.

The parser should support both single .ovf files as well as complete OVF packages. Archives could also be fetched remotely as a single archive (gzip) file. Chunks of files are not supported at this point.

\section{References element}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    No & None & None & None
    \\\hline
  \end{tabular}
\end{center}

Example:\\
\texttt{
\tab<References>\\
\tab\tab<File ovf:id="lamp-base" ovf:href="lamp-base.vmdk" ovf:size="180114671"/>\\
\tab\tab<File ovf:id="lamp-db" ovf:href="lamp-db.vmdk" ovf:size="1801146"/>\\
\tab\tab<File ovf:id="lamp-app" ovf:href="lamp-app.vmdk" ovf:size="34311371"/>\\
\tab</References>
}
\\\\
This element references hard disk files inside the OVF package. It is only required if the whole OVF package is used (rather then a single .ovf file).
If single OVF file is used, this element should be ignored (no disks are supplied).
 
The parser should check if all the files exist and throw an Exception otherwise. It should also check for the correct filesizes of archive's files and store
 the references in an internal data structure for later use.

If an OperatingSystem file is detected (of type qcow2), the parser should copy the file into an appropriate directory (e.g. \texttt{/nebula/images}), 
unless the exact same copy of the file already exists. If datablock disk is detected, the file doesn't have to be moved.

\section{DiskSection element}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    Yes & None & None & None (referenced directly from VirtualSystem)
    \\\hline
  \end{tabular}
\end{center}

Example:\\
\texttt{
\tab<DiskSection>\\
\tab\tab<Info>List of the virtual disks used in the package</Info>\\
\tab\tab<Disk ovf:diskId="lamp-base" ovf:fileRef="lamp-base"\\
ovf:capacity="4294967296" ovf:populatedSize="1924967692"\\
ovf:format="http://www.vmware.com/interfaces/specifications/vmdk.html\#streamOptimized"/>\\
\tab</DiskSection>
}

All virtual disks referenced in \texttt{<References>} section should be listed here - only the disks referenced here can be used. Any disks which are not part of the OVF package should also be listed here (e.g. existing OpenNebula images or disk files on the local filesystem).\\
If the disk is part of the OVF package, the value of its \texttt{ovf:fileRef} atribute is set to \texttt{ovf:id} attribute of the corresponding \texttt{<References><File>} element.

The \texttt{capacity} and \texttt{populatedSize} attributes can be ignored.

\section{NetworkSection element}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    Yes & Network & Virtual Network & value of \texttt{ovf:name} maps to NETWORK=````
    \\\hline
  \end{tabular}
\end{center}

Example:\\
\texttt{
\tab<NetworkSection>\\
\tab\tab<Info>Logical networks used in the package</Info>\\
\tab\tab<Network ovf:name="VM Network">\\
\tab\tab<Description>The network that the LAMP Service will be available\\
\tab\tab on</Description>\\
\tab</Network>\\
</NetworkSection>
}

All networks used by VirtualSystems (VMs) should be referenced here. The value of the \texttt{ovf:name} attribute references an existing network in OpenNebula.\\
The user submitting the OVF file / package to the parser should be the owner of this network or the network has to be made public. 

All VirtualSystem elements use the network specified here. Currently, the parser only supports a single VM network per file and cannot create new networks.

\section{VirtualSystemsCollection element}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    No & Appliance & A group of Virtual Machines & None
    \\\hline
  \end{tabular}
\end{center}

This element makes describing the Contrail appliance a bit more elegant, e.g.\\
\texttt{
\tab<Info>Virtual appliance with a 2-tier distributed LAMP stack</Info>\\
\tab<Name>LAMP Service</Name>
}

The VirtualSystem element also defines all properties (key - value pairs) used within the VirtualSystem child elements. 
This is not a required element - it should also be possible to describe the Contrail Applicance as multiple VirtualSystem elements withour VirtualSystemsCollection parent element.
\\\\

\subsection{StartupSection element}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    No & None & None & None
    \\\hline
  \end{tabular}
\end{center}

Example:\\
\texttt{
\tab<StartupSection>\\
\tab\tab<Info>Startup order of the virtual machines</Info>\\
\tab\tab<Item ovf:id="DBServer" ovf:order="2" ovf:startDelay="120"\\
      ovf:startAction="powerOn" ovf:waitingForGuest="true" ovf:stopDelay="120"\\
      ovf:stopAction="guestShutdown"/>\\
\tab</StartupSection>
}

This section defines the order and startup delays of the provisioned systems. Systems with order = '2' should be provisioned after the system with order='1' enters the running state, but not later than startDelay seconds.

All attributes except \texttt{ovf:order} and \texttt{ovf:startDelay} should be ignored at this point. The VirtualSystems are referenced via attribute \texttt{ovf:id}, which should be unique withing the 
scope of the whole OVF document.

\subsection{VirtualSystem element}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    Yes & Virtual Machine & Virtual Machine & \texttt{value of <Name> maps to NAME = ''}
    \\\hline
  \end{tabular}
\end{center}

This element represents a single Virtual Machine in OpenNebula.

\subsubsection{Item, ResourceType = 3}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    No & Virtual CPU & Virtual CPU & \texttt{<VirtualQuantity> -> VCPU}
    \\\hline
  \end{tabular}
\end{center}

This element represents Virtual CPU count for a single VirtualSystem (VM). It can be empty - VMs provisioned without VCPU attribute will default to 1.

\subsubsection{Item, ResourceType = 4}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    No & RAM & Memory & \texttt{<VirtualQuantity> -> MEMORY, in megabytes}
    \\\hline
  \end{tabular}
\end{center}

This element represents the amount of RAM for a single VirtualSystem (VM). It cannot be empty. The parser should first determine the unit (by reading the \texttt{AllocationUnits field}, which can
either be set to \texttt{byte * 2\^20} (megabytes) or \texttt{byte * 2\^30} (gigabytes). Conversion to megabytes is mandatory for OpenNebula VM template!

\subsubsection{Item, ResourceType = 17}

\begin{center}
  \begin{tabular}{ | l | l | l | l | l | p{3cm} |}
    \hline
    Required & Contrail entity & OpenNebula entity & VM Template variables\\\hline
    Yes & Hard disk & OS / Swap / Datablock & \texttt{<HostResource> -> single DISK = [``]}
    \\\hline
  \end{tabular}
\end{center}

At least one hard disk is required (the first disk represents the operating system disk).\\
TODO: Datablock and Swap disks


\section{OpenNebula template template :)}

The following template can be used to provision a new VirtualMachine and replace the vm-prefixed values with the actual values retrieved by the parser.\\
\texttt{
\tab NAME = "vmNAME"\\
\tab CPU = 1\\
\tab VCPU = vmVCPU\\
\tab MEMORY = vmMEM\\
\tab OS = [ boot="hd", arch="x86\_64" ]\\
\tab DISK = [\\
\tab\tab "image = "vmDISKOS"\\
\tab ]\\
\tab NIC = [\\
\tab\tab NETWORK = "vmNETWORK"\\
\tab ]\\
\tab GRAPHICS = [\\
\tab\tab type="vnc",\\
\tab\tab listen="localhost"\\
\tab ]
}

\end{document}