/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.test;

import static org.junit.Assert.assertEquals;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.provider.pac.events.ViolationMessage;
import org.ow2.contrail.provider.pac.events.ViolationMessage.Alert.Provider;
import org.ow2.contrail.provider.pac.events.ViolationMessage.Alert.SlaGuaranteedState;
import org.ow2.contrail.provider.pac.events.ViolationMessageTranslator;
import org.slasoi.gslam.pac.EventType;
import org.slasoi.gslam.pac.events.Message;

public class ViolationMessageTest {

	private static final Logger logger = Logger.getLogger(ViolationMessageTest.class.getName());

	private static String xmlMess;

	private final String slaId = "3fe94058-e3e4-4190-8dde-6fae1e3f890d";

	@Before
	public void init() {

		xmlMess = "<ViolationMessage xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " + "xsi:noNamespaceSchemaLocation=\"./ContrailViolationEvent.xsd\"  "
				+ "ovfId=\"2\" vepId=\"4\" sid=\"vm_10_1_0_3-xc2-xlab-lan\">" + "    <time>2001-12-17T09:30:47.0Z</time>" + "  	<value id=\"free\">822380</value>" + "  	<alert>"
				+ "  		<type>violation</type>" + "   	<slaUUID>" + slaId + "</slaUUID>" + "   	<slaAgreementTerm>AppServer_Guarantees</slaAgreementTerm>" + " 		<slaGuaranteedState>"
				+ "	      <guaranteedId>MEMORY_for_AppServer</guaranteedId>" + "	      <operator>less_than</operator>" + "	      <guaranteedValue>2048</guaranteedValue>"
				+ "	    </slaGuaranteedState>" + "       <provider>" + "         <providerUUID>42</providerUUID>" + "         <slaUUID>3523h3oh432934lajeo02n</slaUUID>" + "       </provider>"
				+ "   </alert>" + " </ViolationMessage>";
	}

	private ViolationMessage createMessage() {
		logger.info("Creating new ViolationAgentMessage...");

		ViolationMessage message = new ViolationMessage(new GregorianCalendar(), 2, "ceeId", "vmName", "sid");

		// Value val = new Value("identificatore", "testo");

		ViolationMessage.Alert alert = message.new Alert();
		alert.setSlaUUID(slaId);
		alert.setType("violation");
		alert.setSlaAgreementTerm("AppServer_Guarantees");
		SlaGuaranteedState sat = alert.new SlaGuaranteedState();
		sat.setGuaranteedId("MEMORY_for_AppServer");
		sat.setOperator("less_than");
		sat.setGuaranteedValue(new Double(2048));
		alert.setSlaGuaranteedState(sat);
		Provider provider = alert.new Provider();
		provider.setProviderUUID("42");
		provider.setSlaUUID("3523h3oh432934lajeo02n");
		alert.setProvider(provider);

		// message.setValue(val);
		message.setAlert(alert);
		// message.setValue(val);

		logger.debug(message);

		return message;
	}

	@Test
	public void testSerialization() {

		ViolationMessage message = createMessage();
		ViolationMessageTranslator translator = new ViolationMessageTranslator();
		translator.setEventType(EventType.ViolationEvent);
		String str = translator.toXML(message);
		System.out.println("Mess. serialized=\n" + str);

		ViolationMessage messageAfter = (ViolationMessage) translator.fromXML(str);
		assertEquals(slaId, messageAfter.getAlert().getSlaUUID());
	}

	@Test
	public void testDeserialization() {

		System.out.println("Original=\n" + xmlMess);

		ViolationMessageTranslator translator = new ViolationMessageTranslator();
		translator.setEventType(EventType.ViolationEvent);
		Message messageAfter = (ViolationMessage) translator.fromXML(xmlMess);

		String str = translator.toXML(messageAfter);
		System.out.println("\nReserialized=\n" + str);

		int pos = str.indexOf("slaUUID") + 8;
		assertEquals(slaId, str.substring(pos, pos + slaId.length()));
	}

}
