/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.provider.pac.ProvisioningAdjustmentToManagerImpl;

/**
 * @author panuccio
 * 
 */
public class TestPAC_PM {

	private ProvisioningAdjustmentToManagerImpl endpoint;
	private String appName;
	private String slaID;
	private String username;
	private String xuser;
	private String ovfPath;
	private String ovfXML;

	@Before
	public void init() {
		String configFile = "contrail-slamanager" + System.getProperty("file.separator") + "provisioning-adjustment" + System.getProperty("file.separator") + "provisioning_adjustment.properties";

		endpoint = new ProvisioningAdjustmentToManagerImpl(configFile);
		endpoint.init();
		Random rnd = new Random();
		long id = rnd.nextLong();

		appName = "ContrailTestApp_" + id;
		slaID = "SLASOI.xml";
		username = "ContrailVEPUser_" + id;

		xuser = "gregorb";

		ovfPath = "src/main/resources/dsl-test-xlab.xml";

		ovfXML = getOVF(ovfPath);
	}

	private String getOVF(String path) {
		// TODO Auto-generated method stub
		StringBuffer sb = null;
		try {

			File file = new File(path);

			if (!file.exists()) {
				file = new File("dsl-test-xlab.xml");
			}

			BufferedReader reader = new BufferedReader(new FileReader(file));
			sb = new StringBuffer();
			while (reader.ready()) {
				sb.append(reader.readLine());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	//@Test
	public void test() {
		// creating the JSON object
		// JSONObject jobject = new JSONObject();

		// fill the JSON with the proper fields
		/*
		 * try { jobject.put("name", username);
		 * 
		 * // currently fixed to -1 as VEP requested jobject.put("vid", "-1");
		 * 
		 * // currently fixed to user jobject.put("role", "user");
		 * 
		 * // for now: fixed to gregorb jobject.put("xUser", xuser);
		 * 
		 * JSONArray jsonArray = new JSONArray(); jsonArray.put("user");
		 * jsonArray.put(username);
		 * 
		 * jobject.put("groups", jsonArray);
		 */

		String jsonResult = endpoint.manageADD("appId", "virtualSystem", 1);

	}

}
