/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.pac;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.MultiPart;

public class ProvisioningAdjustmentToManagerImpl extends ProvisioningAdjustmentToManager {

	private static final Logger logger = Logger.getLogger(ProvisioningAdjustmentToManagerImpl.class.getName());

	private String URI_basePath;
	private Properties properties;
	private String URIConfigurationFile;

	private String URI_BASE = null;

	private WebResource r;

	public ProvisioningAdjustmentToManagerImpl() {
		super();
	}

	public ProvisioningAdjustmentToManagerImpl(String configFile) {
		super();
		URIConfigurationFile = configFile;
	}

	public void init() {
		properties = new Properties();
		URI_basePath = System.getenv("SLASOI_HOME") + System.getProperty("file.separator");
		try {
			properties.load(new FileInputStream(URI_basePath + URIConfigurationFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		URI_BASE = properties.getProperty("URI_base");
		logger.info("URI_BASE: " + URI_BASE);
		r = client.resource(getBaseURI());
	}

	public String getURIConfigurationFile() {
		return URIConfigurationFile;
	}

	public void setURIConfigurationFile(String uRIConfigurationFile) {
		URIConfigurationFile = uRIConfigurationFile;
	}

	public URI getBaseURI() {
		return UriBuilder.fromUri(URI_BASE).build();
	}

	
	@Override
	public String manageADD(String appID, String virtualSystem, int number) {
		JSONObject js = new JSONObject();
		String status=null;
		try {
			js.put("action", "ADD");
			js.put("VS_ID", virtualSystem);
			js.put("number", number);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ClientResponse response = r.path("/application-mgmt/manage/"+appID).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, js.toString());
		try {
			JSONObject answer = new JSONObject(response.getEntity(String.class));
			status = js.getString("STATUS");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return status;
	}
	
	@Override
	public String manageDELETE(String appID, String virtualSystem, String vmId) {
		JSONObject js = new JSONObject();
		String status=null;
		try {
			js.put("action", "DEL");
			js.put("VS_ID", virtualSystem);
			js.put("VM_ID", vmId);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ClientResponse response = r.path("/application-mgmt/manage/"+appID).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, js.toString());
		try {
			JSONObject answer = new JSONObject(response.getEntity(String.class));
			status = js.getString("STATUS");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return status;
	}

}
