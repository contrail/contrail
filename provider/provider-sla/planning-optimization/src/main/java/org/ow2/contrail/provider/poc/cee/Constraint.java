/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.cee;

import java.util.ArrayList;

import org.ow2.contrail.provider.poc.enums.ContrailAgreementTerm;

public class Constraint {

	private String name;
	private String URI;
	private String value;
	private ContrailAgreementTerm type;
	private ArrayList<String> virtualResourcesReferences;

	public Constraint(String name, String uRI) {
		this.name = name;
		this.URI = uRI;
		this.value = null;
		this.type = null;
		this.virtualResourcesReferences = new ArrayList<String>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getURI() {
		return URI;
	}

	public void setURI(String uRI) {
		URI = uRI;
	}

	public void addValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public ContrailAgreementTerm getType() {
		return type;
	}

	public void setType(ContrailAgreementTerm type) {
		this.type = type;
	}

	public void addVirtualResourceReference(String virtualResourceReference) {
		virtualResourcesReferences.add(virtualResourceReference);
	}

	public ArrayList<String> getVirtualResourcesReferences() {
		return virtualResourcesReferences;
	}

	@Override
	public String toString() {
		String result = name + " : " + value + " : " + type + " : res: ";
		for (String s : virtualResourcesReferences) {
			result += s + " : ";
		}
		return result;
	}

}
