/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.ow2.contrail.provider.poc.cee.VMHandler;
import org.ow2.contrail.provider.poc.manager.resource.VepResourceManager;
import org.ow2.contrail.provider.poc.manager.supply.SupplyManagerImpl;
import org.ow2.contrail.provider.poc.slatemplate.request.VirtualSystem;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.ResourceGuarantee;

public class VMHandlerMapper implements Mapper {
	
	private static final Logger logger = Logger.getLogger(SupplyManagerImpl.class.getName());

	public VMHandlerMapper() {
	}

	public boolean isMappable(VMHandler vmh, ResourceGuarantee resourceRequest) {
		boolean mappable = false;
		if (resourceRequest.getMin() == -1) {
			if (resourceRequest.getMax() == -1) {
				// no upper and lower bound
				mappable = true;
			} else {
				if (resourceRequest.getMax() <= vmh.getUpperBound(resourceRequest.getAgreementTerm())) {
					// upper bound inside range
					mappable = true;
				} else {
					// upper bound outside range
					mappable = false;
				}
			}
		} else {
			if (resourceRequest.getMax() == -1) {
				if (resourceRequest.getMin() <= vmh.getUpperBound(resourceRequest.getAgreementTerm())) {
					// lower bound inside range
					mappable = true;
				}
			} else {
				if (resourceRequest.getMin() <= vmh.getUpperBound(resourceRequest.getAgreementTerm())) {
					// lower bound inside range
					mappable = true;
				} else if (resourceRequest.getMin() >= vmh.getLowerBound(resourceRequest.getAgreementTerm()) && resourceRequest.getMax() <= vmh.getUpperBound(resourceRequest.getAgreementTerm())) {
					// inside range
					mappable = true;
				} else {
					mappable = false;
				}
			}

		}
		return mappable;
	}

	public VMHandler map(ArrayList<ResourceGuarantee> requests) {
		VMHandler lastVmh = null;
		for (VMHandler vmh : VepResourceManager.vmHandlers) {
			boolean mappable = false;
			for (ResourceGuarantee r : requests) {
				if (!isMappable(vmh, r)) {
					mappable = false;
					break;
				} else {
					mappable = true;
				}
			}
			if (mappable)
				return vmh;
			lastVmh = vmh;
		}
		return lastVmh;
	}

	@Override
	public HashMap<VirtualSystem, VMHandler> mapRequest(Collection<VirtualSystem> virtualSystems) {
		HashMap<VirtualSystem, VMHandler> mapping = new HashMap<VirtualSystem, VMHandler>();
		for (VirtualSystem vs : virtualSystems) {
			ArrayList<ResourceGuarantee> requests = vs.getResourceGuarantees();
			VMHandler mapped = map(requests);
			mapping.put(vs, mapped);
			logger.debug("Mapped VS: "+ vs.getOvfId()+" with vmHanlder: "+mapped.getName());
		}

		return mapping;
	}
}
