/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.manager.resource;

import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;
import java.security.KeyStore;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.poc.cee.CEEObject;
import org.ow2.contrail.provider.poc.cee.Constraint;
import org.ow2.contrail.provider.poc.cee.VMHandler;
import org.ow2.contrail.provider.poc.cee.VirtualNetworkHandler;
import org.ow2.contrail.provider.poc.enums.ContrailAgreementTerm;
import org.ow2.contrail.provider.poc.enums.OperatorType;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.GenericGuarantee.Value;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

public class VepResourceManager {

	private static final Logger logger = Logger.getLogger(VepResourceManager.class.getName());

	private static VepResourceManager resourceManager = null;
	private Client client;
	private String basePath;
	private WebResource resourceRootWeb;
	public static ArrayList<VMHandler> vmHandlers;
	public static ArrayList<VirtualNetworkHandler> virtualNetworkHandlers;
	public static ArrayList<Constraint> constraints;

	private VepResourceManager() {
	}

	public static VepResourceManager getInstance() {
		if (resourceManager == null) {
			resourceManager = new VepResourceManager();
		}
		return resourceManager;
	}

	public void configManager(boolean secureConnection, String trustStore, String trustStorePwd) {
		ClientConfig cc = new DefaultClientConfig();
		if (secureConnection) {
			try {
				// setting client truststore
				TrustManager trustmanagers[] = null;
				KeyManager keymanagers[] = null;

				if (trustStorePwd != null && !trustStorePwd.isEmpty()) {
					trustmanagers = new TrustManager[] { new MyX509TrustManager(trustStore, trustStorePwd.toCharArray()) };
					keymanagers = new KeyManager[] { new MyX509KeyManager(trustStore, trustStorePwd.toCharArray()) };
				} else {
					trustmanagers = new TrustManager[] { new MyX509TrustManager(trustStore, null) };
					keymanagers = new KeyManager[] { new MyX509KeyManager(trustStore, null) };
				}
				// trust all hostname NOT SAFE!!!
				HostnameVerifier allHostsValid = new HostnameVerifier() {
					@Override
					public boolean verify(String urlHostName, SSLSession session) {
						return true;
					}
				};
				SSLContext ctx = null;
				// setting SSL context and client
				ctx = SSLContext.getInstance("SSL");
				// init contex with custom truststore
				ctx.init(keymanagers, trustmanagers, null);
				cc.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(allHostsValid, ctx));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		client = Client.create(cc);
	}

	public void setBasePath(String urlBasePath) {
		basePath = urlBasePath;
		initialServiceInitializer();
	}

	private void initialServiceInitializer() {
		resourceRootWeb = client.resource(basePath);
		System.out.println("Initialized client connection");
	}

	public JSONObject requestVMHandlers() {
		JSONObject jsonResp = null;
		try {
			ClientResponse response = resourceRootWeb.path("VMHandlers").accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			String res = response.getEntity(String.class);
			logger.debug("Reply from VEP: " + res);
			jsonResp = new JSONObject(res);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResp;
	}

	public JSONObject requestVMHandler(String ref) {
		JSONObject jsonResp = null;
		try {
			ClientResponse response = resourceRootWeb.path(ref).accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			String res = response.getEntity(String.class);
			logger.debug("Reply from VEP: " + res);
			jsonResp = new JSONObject(res);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResp;
	}

	public JSONObject requestNetworkHandler(String href) {
		JSONObject jsonResp = null;
		try {
			ClientResponse response = resourceRootWeb.path(href).accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			String res = response.getEntity(String.class);
			logger.debug("Reply from VEP: " + res);
			jsonResp = new JSONObject(res);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResp;
	}

	public JSONObject requestNetworkHandlers() {
		JSONObject jsonResp = null;
		try {
			ClientResponse response = resourceRootWeb.path("networkHandlers").accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			String res = response.getEntity(String.class);
			logger.debug("Reply from VEP: " + res);
			jsonResp = new JSONObject(res);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResp;
	}

	public JSONObject requestConstraints() {
		JSONObject jsonResp = null;
		try {
			ClientResponse response = resourceRootWeb.path("constraints").accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			String res = response.getEntity(String.class);
			logger.debug("Reply from VEP: " + res);
			jsonResp = new JSONObject(res);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonResp;
	}

	public ClientResponse createCEE(CEEObject cee, String userUUID) {
		ClientResponse response = null;
		logger.debug("Create CEE: ");
		logger.debug("Request to VEP: " + cee.toString());
		response = resourceRootWeb.path("cee").header("X-Username", userUUID).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, cee.toString());
		return response;
	}

	public ClientResponse checkFeasibilityCEE(CEEObject cee, String userUUID) {
		ClientResponse response = null;
		cee.setStateCheck();
		logger.debug("Check feasibility: ");
		logger.debug("Request to VEP: " + cee.toString());
		response = resourceRootWeb.path("cee").header("X-Username", userUUID).type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, cee.toString());
		return response;
	}

	public void initializeVMHandlers() {
		vmHandlers = new ArrayList<VMHandler>();
		JSONObject json = requestVMHandlers();
		try {
			JSONArray vmHandlersArray = json.getJSONArray("vmhandlers");
			int i = 0;
			while (vmHandlersArray.optJSONObject(i) != null) {
				JSONObject jsonElement = vmHandlersArray.optJSONObject(i);
				try {
					i++;
					String name = jsonElement.getString("name");
					String href = jsonElement.getString("href");
					JSONObject jVM = requestVMHandler(href.substring(href.lastIndexOf("VMHandler")));
					VMHandler vmHandler = new VMHandler(name, href, jVM.getDouble("cpufreq_low"), jVM.getDouble("cpufreq_high"), jVM.getDouble("ram_low"), jVM.getDouble("ram_high"),
							jVM.getDouble("corecount_low"), jVM.getDouble("corecount_high"), jVM.getDouble("disksize_low"), jVM.getDouble("disksize_high"), jVM.getString("resourceURI"));
					vmHandlers.add(vmHandler);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Collections.sort(vmHandlers);
	}

	public void initializeNetworkHandlers() {
		virtualNetworkHandlers = new ArrayList<VirtualNetworkHandler>();

		JSONObject json = requestNetworkHandlers();
		try {
			JSONArray vmHandlers = json.getJSONArray("nethandlers");
			int i = 0;
			while (vmHandlers.optJSONObject(i) != null) {
				JSONObject jsonElement = vmHandlers.optJSONObject(i);
				try {
					String name = jsonElement.getString("name");
					String href = jsonElement.getString("href");
					href = href.substring(href.lastIndexOf("networkHandler"));
					JSONObject jVM = requestNetworkHandler(href);
					VirtualNetworkHandler vnh = new VirtualNetworkHandler(name, jVM.getString("id"), jVM.getString("publicIpSupport"), null, jVM.getString("dhcpSupport"), jVM.getString("resourceURI"));
					virtualNetworkHandlers.add(vnh);
					i++;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void initializeConstraints() {
		constraints = new ArrayList<Constraint>();
		JSONObject json = requestConstraints();
		JSONArray constraintsArray = json.optJSONArray("constraints");
		int i = 0;
		while (constraintsArray != null && constraintsArray.optJSONObject(i) != null) {
			JSONObject jsonElement = constraintsArray.optJSONObject(i);
			try {
				String name = jsonElement.getString("name");
				String href = jsonElement.getString("href");
				Constraint constraint = new Constraint(name, href);
				constraints.add(constraint);
				i++;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static Constraint getConstraint(ContrailAgreementTerm cat, Value value) {
		String name = null;
		switch (cat) {
		case vs_location:
			if (value.getOperator().equals(OperatorType.EQUALS)) {
				name = "IN COUNTRY";
			} else
				name = "NOT IN COUNTRY";
			break;
		case storage_location:
			if (value.getOperator().equals(OperatorType.EQUALS)) {
				name = "IN COUNTRY";
			} else
				name = "NOT IN COUNTRY";
			break;
		case co_location_rack:
			name = "SAME RACK";
			break;
		case not_co_location_host:
			name = "NOT SAME HOST";
			break;
		}
		for (Constraint c : constraints) {
			if (c.getName().equals(name))
				return c;
		}
		return null;
	}

	class MyX509TrustManager implements X509TrustManager {

		/*
		 * The default PKIX X509TrustManager9. We'll delegate decisions to it,
		 * and fall back to the logic in this class if the default
		 * X509TrustManager doesn't trust it.
		 */
		X509TrustManager pkixTrustManager;

		MyX509TrustManager(String keyStore, char[] password) throws Exception {
			this(new File(keyStore), password);
		}

		MyX509TrustManager(File keyStore, char[] password) throws Exception {
			// create a "default" JSSE X509TrustManager.

			KeyStore ks = KeyStore.getInstance("PKCS12");
			ks.load(new FileInputStream(keyStore), password);

			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509", "SunJSSE");
			tmf.init(ks);

			TrustManager tms[] = tmf.getTrustManagers();

			/*
			 * Iterate over the returned trustmanagers, look for an instance of
			 * X509TrustManager. If found, use that as our "default" trust
			 * manager.
			 */
			for (int i = 0; i < tms.length; i++) {
				if (tms[i] instanceof X509TrustManager) {
					pkixTrustManager = (X509TrustManager) tms[i];
					return;
				}
			}

			/*
			 * Find some other way to initialize, or else we have to fail the
			 * constructor.
			 */
			throw new Exception("Couldn't initialize");
		}

		/*
		 * Delegate to the default trust manager.
		 */
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			try {
				pkixTrustManager.checkClientTrusted(chain, authType);
			} catch (CertificateException excep) {
				// do any special handling here, or rethrow exception.
			}
		}

		/*
		 * Delegate to the default trust manager.
		 */
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			try {
				pkixTrustManager.checkServerTrusted(chain, authType);
			} catch (CertificateException excep) {
				/*
				 * Possibly pop up a dialog box asking whether to trust the cert
				 * chain.
				 */
			}
		}

		/*
		 * Merely pass this through.
		 */
		public X509Certificate[] getAcceptedIssuers() {
			return pkixTrustManager.getAcceptedIssuers();
		}

	}

	static class MyX509KeyManager implements X509KeyManager {

		/*
		 * The default PKIX X509KeyManager. We'll delegate decisions to it, and
		 * fall back to the logic in this class if the default X509KeyManager
		 * doesn't trust it.
		 */
		X509KeyManager pkixKeyManager;

		MyX509KeyManager(String keyStore, char[] password) throws Exception {
			this(new File(keyStore), password);
		}

		MyX509KeyManager(File keyStore, char[] password) throws Exception {
			// create a "default" JSSE X509KeyManager.

			KeyStore ks = KeyStore.getInstance("PKCS12");
			ks.load(new FileInputStream(keyStore), password);

			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509", "SunJSSE");
			kmf.init(ks, password);

			KeyManager kms[] = kmf.getKeyManagers();

			/*
			 * Iterate over the returned keymanagers, look for an instance of
			 * X509KeyManager. If found, use that as our "default" key manager.
			 */
			for (int i = 0; i < kms.length; i++) {
				if (kms[i] instanceof X509KeyManager) {
					pkixKeyManager = (X509KeyManager) kms[i];
					return;
				}
			}
			/*
			 * Find some other way to initialize, or else we have to fail the
			 * constructor.
			 */
			throw new Exception("Couldn't initialize KeyManager");
		}

		public PrivateKey getPrivateKey(String arg0) {
			return pkixKeyManager.getPrivateKey(arg0);
		}

		public X509Certificate[] getCertificateChain(String arg0) {
			return pkixKeyManager.getCertificateChain(arg0);
		}

		public String[] getClientAliases(String arg0, Principal[] arg1) {
			return pkixKeyManager.getClientAliases(arg0, arg1);
		}

		public String chooseClientAlias(String[] arg0, Principal[] arg1, Socket arg2) {
			return pkixKeyManager.chooseClientAlias(arg0, arg1, arg2);
		}

		public String[] getServerAliases(String arg0, Principal[] arg1) {
			return pkixKeyManager.getServerAliases(arg0, arg1);
		}

		public String chooseServerAlias(String arg0, Principal[] arg1, Socket arg2) {
			return pkixKeyManager.chooseServerAlias(arg0, arg1, arg2);
		}
	}

}
