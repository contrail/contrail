/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.manager.pricing;

import org.slasoi.businessManager.common.model.pricing.OvfResource;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.model.pricing.UnitResource;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface PricingManager {

	public double getOvfResourcePrice(String ovfId);

	@Transactional(propagation = Propagation.REQUIRED)
	public abstract OvfResource findOvfResource(String ovfDescrName, String ovfId);

	public abstract Price findCurrentPrice(Long productId);

	public abstract Price findPriceOfGuarantee(Long guaranteeId);

	public abstract boolean isVariableFee(Price p);

	public abstract boolean isCompoundFee(Price p);

	public abstract boolean isFixedFee(Price p);

	public abstract PricedItem getPricedItem(Long id);

	public abstract Price getPrice(Long id);

	@Transactional(propagation = Propagation.REQUIRED)
	public abstract UnitResource findUnitResourceByType(String type);

}
