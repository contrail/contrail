/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.slatemplate;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

import org.apache.commons.collections.CollectionUtils;
import org.ow2.contrail.provider.poc.slatemplate.request.ContrailGenericRequest;
import org.ow2.contrail.provider.poc.slatemplate.request.ContrailRequest;
import org.ow2.contrail.provider.poc.slatemplate.request.ContrailResourceRequest;
import org.ow2.contrail.provider.poc.slatemplate.request.SharedDisk;
import org.ow2.contrail.provider.poc.slatemplate.request.VirtualSystem;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.OvfResourceGuarantee;
import org.slasoi.businessManager.common.model.pricing.Guarantee;
import org.slasoi.slamodel.sla.SLATemplate;

public class ContrailSlaTemplate {

	private String uuid = null;

	private String ovfFile = null;

	private Collection<ContrailRequest> contrailRequests = null;

	private SLATemplate slaTemplate = null;

	public ContrailSlaTemplate(Builder builder) {
		uuid = builder._uuid;
		ovfFile = builder._ovfFile;
		contrailRequests = builder._contrailRequests;
		slaTemplate = builder._slaTemplate;
	}

	public void addContrailResource(ContrailResourceRequest cr) {
		contrailRequests.add(cr);
	}

	public void removeContrailResource(ContrailResourceRequest cr) {
		contrailRequests.remove(cr);
	}

	public String getOvfFile() {
		return ovfFile;
	}

	public Collection<ContrailRequest> getContrailRequests() {
		return contrailRequests;
	}

	public SLATemplate getSlaTemplate() {
		return slaTemplate;
	}

	public Collection<VirtualSystem> getVirtualSystems() {
		Collection<VirtualSystem> virtualSystems = new HashSet<VirtualSystem>();
		for (ContrailRequest cr : contrailRequests) {
			if (cr instanceof VirtualSystem) {
				virtualSystems.add((VirtualSystem) cr);
			}
		}
		return virtualSystems;
	}

	public Collection<SharedDisk> getSharedDisks() {
		Collection<SharedDisk> sharedDisks = new HashSet<SharedDisk>();
		for (ContrailRequest cr : contrailRequests) {
			if (cr instanceof SharedDisk) {
				sharedDisks.add((SharedDisk) cr);
			}
		}
		return sharedDisks;
	}

	public Collection<ContrailGenericRequest> getGenericRequests() {
		Collection<ContrailGenericRequest> genericRequests = new HashSet<ContrailGenericRequest>();
		for (ContrailRequest cr : contrailRequests) {
			if (cr instanceof ContrailGenericRequest) {
				genericRequests.add((ContrailGenericRequest) cr);
			}
		}
		return genericRequests;
	}

	@Override
	public String toString() {
		String s = "Contrail Sla Template:\n";
		s += "UUID: " + uuid + "\n";
		for (ContrailRequest cr : contrailRequests) {
			s += cr.toString() + "\n";
		}
		return s;
	}

	public static class Builder {

		private String _uuid = null;
		private String _ovfFile = null;
		private Collection<ContrailRequest> _contrailRequests = null;
		private SLATemplate _slaTemplate = null;

		public Builder setUUID(String uuid) {
			_uuid = uuid;
			return this;
		}

		public Builder setOvfFile(String ovfFile) {
			_ovfFile = ovfFile;
			return this;
		}

		public Builder setContrailRequests(Collection<ContrailRequest> c) {
			_contrailRequests = c;
			return this;
		}

		public Builder setSlaTemplate(SLATemplate s) {
			_slaTemplate = s;
			return this;
		}

		public Builder() {

		}

		public ContrailSlaTemplate build() {
			return new ContrailSlaTemplate(this);
		}
	}

	public void removeVirtualSystem(String ovfID) {
		for (ContrailRequest cr : contrailRequests) {
			if (cr instanceof VirtualSystem && cr.getOvfId().equals(ovfID)) {
				contrailRequests.remove(cr);
				break;
			}
		}
	}

	public void removeSharedDisk(String ovfID) {
		for (ContrailRequest cr : contrailRequests) {
			if (cr instanceof SharedDisk && cr.getOvfId().equals(ovfID)) {
				contrailRequests.remove(cr);
				break;
			}
		}
	}

	public void removeAllSharedDisks() {
		for (ContrailRequest cr : contrailRequests) {
			if (cr instanceof SharedDisk) {
				contrailRequests.remove(cr);
			}
		}
	}

	public VirtualSystem getVirtualSystem(String ovfID) {
		for (ContrailRequest cr : contrailRequests) {
			if (cr instanceof VirtualSystem && cr.getOvfId().equals(ovfID)) {
				return (VirtualSystem) cr;
			}
		}
		return null;
	}

	public void removeAllOvfResourceRequests() {
		for (ContrailRequest cr : contrailRequests) {
			if (cr instanceof VirtualSystem) {
				CopyOnWriteArraySet<org.ow2.contrail.provider.poc.slatemplate.request.guarantee.Guarantee> cpaGuarantees=new CopyOnWriteArraySet<org.ow2.contrail.provider.poc.slatemplate.request.guarantee.Guarantee>(cr.getGuarantees());
				Iterator<org.ow2.contrail.provider.poc.slatemplate.request.guarantee.Guarantee> it=cpaGuarantees.iterator();
				while(it.hasNext()){
					org.ow2.contrail.provider.poc.slatemplate.request.guarantee.Guarantee g=it.next();
					if(g instanceof OvfResourceGuarantee){
						cr.removeGuarantee(g);
					}
				}
			}
		}
	}

}
