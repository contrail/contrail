/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.cee.converter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.ow2.contrail.provider.poc.enums.ContrailAgreementTerm;
import org.ow2.contrail.provider.poc.exceptions.NotSupportedUnitException;
import org.ow2.contrail.provider.poc.manager.pricing.PricingManagerImpl;
import org.ow2.contrail.provider.poc.slatemplate.ContrailSlaTemplate;
import org.ow2.contrail.provider.poc.slatemplate.request.ContrailGenericRequest;
import org.ow2.contrail.provider.poc.slatemplate.request.ContrailRequest;
import org.ow2.contrail.provider.poc.slatemplate.request.ContrailResourceRequest;
import org.ow2.contrail.provider.poc.slatemplate.request.SharedDisk;
import org.ow2.contrail.provider.poc.slatemplate.request.VirtualSystem;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.GenericGuarantee;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.GenericGuarantee.Value;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.Guarantee;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.ResourceGuarantee;
import org.ow2.contrail.provider.poc.utils.AgreementUtil;
import org.ow2.contrail.provider.poc.utils.ContrailUnits;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.UnitResource;
import org.slasoi.businessManager.common.service.GuaranteeManager;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.vocab.core;
import org.springframework.beans.factory.annotation.Autowired;

public class Cee2SlaConverter {

	private static final Logger logger = Logger.getLogger(Cee2SlaConverter.class.getName());

	@Autowired
	private PricingManagerImpl pricingManager;

	@Autowired
	private GuaranteeManager guaranteeService;

	private ContrailSlaTemplate contrailslaTemplate = null;

	private SLATemplate newSlaTemplate = null;

	private SLATemplate oldSlaTemplate = null;

	private ArrayList<AgreementTerm> aTerms = null;

	private String ceeID = null;

	public Cee2SlaConverter() {

	}

	public void init() {
		ceeID = null;
	}

	public Cee2SlaConverter setContrailSlatemplate(ContrailSlaTemplate cst) {
		contrailslaTemplate = cst;
		oldSlaTemplate = contrailslaTemplate.getSlaTemplate();
		return this;
	}

	public Cee2SlaConverter setCeeID(String c) {
		this.ceeID = c;
		return this;
	}

	public SLATemplate cee2Sla() {
		newSlaTemplate = new SLATemplate();
		aTerms = new ArrayList<AgreementTerm>();
		newSlaTemplate.setUuid(oldSlaTemplate.getUuid());
		addProperties();
		addInterfaceDecl();
		addAgreementTerms();
		addPrices();
		AgreementTerm[] terms = new AgreementTerm[aTerms.size()];
		int i = 0;
		for (AgreementTerm at : aTerms) {
			terms[i] = at;
			i++;
		}
		newSlaTemplate.setAgreementTerms(terms);
		return newSlaTemplate;
	}

	private void addPrices() {
		for (VirtualSystem vs : contrailslaTemplate.getVirtualSystems()) {
			addPriceVS(vs);
		}
		for (SharedDisk sd : contrailslaTemplate.getSharedDisks()) {
			addPriceSD(sd);
		}
	}

	private void addPriceVS(VirtualSystem vs) {
		AgreementTerm aTerm = null;
		ArrayList<ComponentProductOfferingPrice> cpops = new ArrayList<ComponentProductOfferingPrice>();
		ProductOfferingPrice po = null;
		String id = "Price_OF_" + vs.getOvfId();
		String billFreq = null;
		Date from = null;
		Date until = null;
		String priceType = null;
		String currency = null;
		ComponentProductOfferingPrice cpo = null;
		double totalPrice = 0, resourcesPrice = 0, guaranteePrice = 0, reservationPrice = 0;
		for (ResourceGuarantee r : vs.getResourceGuarantees()) {
			UnitResource ur = pricingManager.findUnitResourceByType(AgreementUtil.agreementToMetric(r.getAgreementTerm()));
			Price p = pricingManager.findCurrentPrice(ur.getUnitResourceId());
			priceType = p.getType();
			currency = p.getCurrencyType();
			from = p.getValidFrom();
			until = p.getValidUntil();
			billFreq = p.getPeriod();
			if (pricingManager.isCompoundFee(p)) {
				// future use for resources such as network
				// ArrayList<ComponentProductOfferingPrice>
				// cpopArray=buildProductOfferingPrice(p.getCompoundFee(), r);
				// cpops.addAll(cpopArray);
			} else if (pricingManager.isVariableFee(p)) {
				// future use: for resources such as network
				// cpo = buildComponentProductOfferingPrice(p.getVariableFee(),
				// r);
				// cpops.add(cpo);
			} else {
				resourcesPrice += (p.getPriceValue().doubleValue()) * (r.getDefault());
				// cpo = buildComponentProductOfferingPrice(p.getFixedFee(), r);

			}
		}
		StringBuilder sb = new StringBuilder();
		org.slasoi.businessManager.common.model.pricing.Guarantee g = null;
		org.slasoi.businessManager.common.model.pricing.Guarantee reservationGuarantee = null;
		for (GenericGuarantee gg : vs.getGenericGuarantees()) {
			sb.append(gg.getAgreementTerm().toString() + ", ");
			if (!gg.getAgreementTerm().equals(ContrailAgreementTerm.reserve)) {
				g = guaranteeService.getGuaranteeByType(AgreementUtil.agreementToMetric(gg.getAgreementTerm()));
				guaranteePrice += (resourcesPrice * g.getPercPriceVariation().doubleValue());
			} else {
				reservationGuarantee = guaranteeService.getGuaranteeByType(AgreementUtil.agreementToMetric(gg.getAgreementTerm()));
				Price price = pricingManager.findPriceOfGuarantee(reservationGuarantee.getGuaranteeId());
				reservationPrice = (price.getPriceValue().doubleValue()) * (new Integer(gg.getValues().get(0).getValue()).intValue());
				cpo = new ComponentProductOfferingPrice(new ID("Price for reservation of_" + vs.getOvfId()), new STND(Converter.$STND_business + price.getType()), new CONST(new Double(
						reservationPrice).toString(), new STND(Converter.$STND_units + price.getCurrencyType())), new CONST(gg.getValues().get(0).getValue(), new STND(Converter.$STND_units + "vm")));
				cpops.add(cpo);
			}
		}
		if (reservationGuarantee != null)
			guaranteePrice -= (resourcesPrice * reservationGuarantee.getPercPriceVariation().doubleValue()); // in
																												// the
																												// end
																												// subtract
																												// reservation
																												// guarantee
																												// price
		totalPrice = resourcesPrice + guaranteePrice;
		cpo = new ComponentProductOfferingPrice(new ID(id), new STND(Converter.$STND_business + priceType), new CONST(new Double(totalPrice).toString(), new STND(Converter.$STND_units + currency)),
				new CONST("1", new STND(Converter.$STND_units + "vm")));
		cpops.add(cpo);
		ComponentProductOfferingPrice[] cpopsArray = new ComponentProductOfferingPrice[cpops.size()];
		int i = 0;
		for (ComponentProductOfferingPrice c : cpops) {
			cpopsArray[i] = c;
			i++;
		}
		po = new ProductOfferingPrice(new ID("Product_Offering_Price_Of_" + vs.getOvfId()), "", new TIME(dateToCalendar(from)), new TIME(dateToCalendar(until)), new STND(Converter.$STND_business
				+ billFreq), cpopsArray);
		if (sb.length() != 0)
			po.setPropertyValue(new STND("Guarantees applied "), sb.toString().substring(0, sb.toString().length() - 2));
		ID ida = new ID(vs.getOvfId());
		Expr[] param = { ida };
		EventExpr ee = new EventExpr(new STND(Converter.$STND_coremodel + "invocation"), param);
		Guaranteed.Action priceAction = new Action(new ID("Price_Of_VirtualSystem_" + vs.getOvfId()), new ID("http://www.slaatsoi.org/slamodel#provider"), new STND(
				"http://www.slaatsoi.org/slamodel#mandatory"), ee, po);
		Guaranteed[] guarantees = { priceAction };
		aTerm = new AgreementTerm(new ID("Infrastructure_Price_Of_" + vs.getOvfId()), null, null, guarantees);
		aTerms.add(aTerm);
	}

	private void addPriceSD(SharedDisk sd) {
		AgreementTerm aTerm = null;
		ArrayList<ComponentProductOfferingPrice> cpops = new ArrayList<ComponentProductOfferingPrice>();
		ProductOfferingPrice po = null;
		String billFreq = null;
		Date from = null;
		Date until = null;
		ComponentProductOfferingPrice cpo = null;
		double reliabilityPrice = 0;
		org.slasoi.businessManager.common.model.pricing.Guarantee reliabilityGuarantee = null;
		for (GenericGuarantee gg : sd.getGenericGuarantees()) {
			if (gg.getAgreementTerm().equals(ContrailAgreementTerm.reliability)) {
				reliabilityGuarantee = guaranteeService.getGuaranteeByType(AgreementUtil.agreementToMetric(gg.getAgreementTerm()));
				Price price = pricingManager.findPriceOfGuarantee(reliabilityGuarantee.getGuaranteeId());
				if (price == null) {
					logger.debug("No price for reliability specified inside the DB!");
					return;
				}
				from = price.getValidFrom();
				until = price.getValidUntil();
				billFreq = price.getPeriod();
				reliabilityPrice = (price.getPriceValue().doubleValue()) * (new Integer(gg.getValues().get(0).getValue()).intValue());
				cpo = new ComponentProductOfferingPrice(new ID("Price for reliability of_" + sd.getOvfId()), new STND(Converter.$STND_business + price.getType()), new CONST(new Double(
						reliabilityPrice).toString(), new STND(Converter.$STND_units + price.getCurrencyType())), new CONST(gg.getValues().get(0).getValue(), new STND(Converter.$STND_units + "disk")));
				cpops.add(cpo);
				break;
			}
		}
		ComponentProductOfferingPrice[] cpopsArray = new ComponentProductOfferingPrice[cpops.size()];
		int i = 0;
		for (ComponentProductOfferingPrice c : cpops) {
			cpopsArray[i] = c;
			i++;
		}
		po = new ProductOfferingPrice(new ID("Product_Offering_Price_Of_" + sd.getOvfId()), "", new TIME(dateToCalendar(from)), new TIME(dateToCalendar(until)), new STND(Converter.$STND_business
				+ billFreq), cpopsArray);
		ID ida = new ID(sd.getOvfId());
		Expr[] param = { ida };
		EventExpr ee = new EventExpr(new STND(Converter.$STND_coremodel + "invocation"), param);
		Guaranteed.Action priceAction = new Action(new ID("Price_Of_SharedDisk_" + sd.getOvfId()), new ID("http://www.slaatsoi.org/slamodel#provider"), new STND(
				"http://www.slaatsoi.org/slamodel#mandatory"), ee, po);
		Guaranteed[] guarantees = { priceAction };
		aTerm = new AgreementTerm(new ID("Infrastructure_Price_Of_" + sd.getOvfId()), null, null, guarantees);
		aTerms.add(aTerm);
	}

	private void addAgreementTerms() {
		for (ContrailRequest cr : contrailslaTemplate.getContrailRequests()) {
			if (cr instanceof ContrailResourceRequest) {
				addAgreementTermResource((ContrailResourceRequest) cr);
			}
			if (cr instanceof ContrailGenericRequest) {
				addAgreementTermGeneric((ContrailGenericRequest) cr);
			}
		}
	}

	private void addAgreementTermGeneric(ContrailGenericRequest cr) {
		// add Agreement Term non negotiable
		for (AgreementTerm at : oldSlaTemplate.getAgreementTerms()) {
			if (at.getId().getValue().equals(cr.getId())) {
				aTerms.add(at);
			}
		}
	}

	private void addAgreementTermResource(ContrailResourceRequest cr) {
		AgreementTerm aTerm = null;
		String id = cr.getId();

		HashMap<String, Expr> variables = cr.getVariables();
		VariableDeclr[] var = buildVariableDecl(variables);

		Collection<Guarantee> guarantees = cr.getGuarantees();
		Guaranteed[] guaranteesArray = buildGuarantees(guarantees);

		aTerm = new AgreementTerm(new ID(id), null, var, guaranteesArray);
		aTerms.add(aTerm);
	}

	private Guaranteed[] buildGuarantees(Collection<Guarantee> guarantees) {
		Guaranteed[] guaranteesArray = new Guaranteed[guarantees.size()];
		int i = 0;
		for (Guarantee g : guarantees) {
			Guaranteed guar = buildGuarantee(g);
			guaranteesArray[i] = guar;
			i++;
		}
		return guaranteesArray;
	}

	private Guaranteed buildGuarantee(Guarantee g) {
		ID[] params = new ID[1];
		Guaranteed.State state = null;
		try {
			params[0] = new ID(g.getDomain());
			FunctionalExpr value = new FunctionalExpr(AgreementUtil.agreementToResource(g.getAgreementTerm()), params);
			if (g instanceof ResourceGuarantee) {
				SimpleDomainExpr domain;
				domain = new SimpleDomainExpr(new CONST(new Double(((ResourceGuarantee) g).getDefault()).toString(), ContrailUnits.convertToSTND(((ResourceGuarantee) g).getUnit().toString())),
						core.equals);
				TypeConstraintExpr tce = new TypeConstraintExpr(value, domain);
				state = new State(new ID(g.getId()), tce);
				return state;
			}
			if (g instanceof GenericGuarantee) {
				List<Value> values = ((GenericGuarantee) g).getValues();
				if (values.size() > 1) {
					SimpleDomainExpr[] sdeArray = new SimpleDomainExpr[values.size()];
					int i = 0;
					for (Value v : values) {
						SimpleDomainExpr sde = new SimpleDomainExpr(new CONST(v.getValue(), new STND(v.getType())), AgreementUtil.convertToSTND(v.getOperator()));
						sdeArray[i] = sde;
						i++;
					}
					CompoundDomainExpr cde = new CompoundDomainExpr(core.or, sdeArray);
					TypeConstraintExpr tce = new TypeConstraintExpr(value, cde);
					state = new State(new ID(g.getId()), tce);
					return state;
				} else {
					Value v = values.get(0); // only one simple expression
					SimpleDomainExpr domain;
					domain = new SimpleDomainExpr(new CONST(v.getValue(), ContrailUnits.convertToSTND(v.getType())), AgreementUtil.convertToSTND(v.getOperator()));
					TypeConstraintExpr tce = new TypeConstraintExpr(value, domain);
					state = new State(new ID(g.getId()), tce);
					return state;
				}
			}
		} catch (NotSupportedUnitException e) {
			e.printStackTrace();
		}
		return state;
	}

	private VariableDeclr[] buildVariableDecl(HashMap<String, Expr> variables) {
		VariableDeclr[] var = new VariableDeclr[variables.size()];
		int i = 0;
		for (String s : variables.keySet()) {
			VariableDeclr v = new VariableDeclr(new ID(s), variables.get(s));
			var[i] = v;
			i++;
		}
		return var;
	}

	private void addInterfaceDecl() {
		InterfaceDeclr[] intDecl = oldSlaTemplate.getInterfaceDeclrs();
		ArrayList<InterfaceDeclr> intDeclList = new ArrayList<InterfaceDeclr>();
		String ovfApp = null;
		for (InterfaceDeclr i : intDecl) {
			for (ContrailRequest cr : contrailslaTemplate.getContrailRequests()) {
				String ovfId = cr.getOvfId();
				org.slasoi.slamodel.sla.Endpoint[] e = i.getEndpoints();
				if (e != null && e.length != 0) { // is Virtual System OR
													// CustomAction
					ovfApp = e[0].getPropertyValue(new STND("OVF_VirtualSystem_ID"));
					if (ovfApp != null) { // is virtualSystem
						if (ovfApp.equals(ovfId)) {
							intDeclList.add(i);
							break;
						}
					} else { // is Custom Action
						intDeclList.add(i);
						break;
					}
				} else {
					ovfApp = i.getInterface().getPropertyValue(new STND("Shared_Disk_ID"));
					if (ovfId != null && ovfId.equals(ovfApp)) { // is Shared
																	// Disk
						intDeclList.add(i);
						break;
					}
				}
			}
		}
		InterfaceDeclr[] newIntDeclrs = new InterfaceDeclr[intDeclList.size()];
		for (int i = 0; i < intDeclList.size(); i++) {
			newIntDeclrs[i] = intDeclList.get(i);
		}
		newSlaTemplate.setInterfaceDeclrs(newIntDeclrs);
	}

	private void addProperties() {
		// add ProvidersList
		String providersList = oldSlaTemplate.getPropertyValue(new STND("ProvidersList"));
		if (providersList != null)
			newSlaTemplate.setPropertyValue(new STND("ProvidersList"), providersList);

		// add Criteria
		String criteria = oldSlaTemplate.getPropertyValue(new STND("Criteria"));
		if (criteria != null)
			newSlaTemplate.setPropertyValue(new STND("Criteria"), criteria);

		// add mapping to federationSlaId
		String federationSlaId = oldSlaTemplate.getPropertyValue(new STND("FederationSlaId"));
		if (federationSlaId != null)
			newSlaTemplate.setPropertyValue(new STND("FederationSlaId"), federationSlaId);

		// add UserUUID
		String userUUID = oldSlaTemplate.getPropertyValue(new STND("UserUUID"));
		if (userUUID != null)
			newSlaTemplate.setPropertyValue(new STND("UserUUID"), userUUID);

		// add providerUUID
		String providerUUID = oldSlaTemplate.getPropertyValue(new STND("ProviderUUid"));
		if (providerUUID != null)
			newSlaTemplate.setPropertyValue(new STND("ProviderUUid"), providerUUID);

		// add AppUUID
		String appUUID = oldSlaTemplate.getPropertyValue(new STND("AppUUID"));
		if (appUUID != null)
			newSlaTemplate.setPropertyValue(new STND("AppUUID"), appUUID);

		// add CEE ID
		if (ceeID != null)
			newSlaTemplate.setPropertyValue(new STND("CEE-ID"), ceeID);

		newSlaTemplate.setDescr(oldSlaTemplate.getDescr());

		newSlaTemplate.setParties(oldSlaTemplate.getParties());

		newSlaTemplate.setVariableDeclrs(oldSlaTemplate.getVariableDeclrs());

	}

	private static Calendar dateToCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

}
