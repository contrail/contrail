/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.cee.converter;

import java.util.HashMap;

import org.ow2.contrail.provider.poc.cee.CEEObject;
import org.ow2.contrail.provider.poc.cee.VMHandler;
import org.ow2.contrail.provider.poc.cee.CEEObject.Builder;
import org.ow2.contrail.provider.poc.slatemplate.ContrailSlaTemplate;
import org.ow2.contrail.provider.poc.slatemplate.request.VirtualSystem;

public class Sla2CeeConverter extends Converter {

	private CEEObject ceeObject;

	private ContrailSlaTemplate contrailSlaTemplate = null;

	private HashMap<VirtualSystem, VMHandler> mapping = null;

	public Sla2CeeConverter() {
		System.out.println("Inside Sla2CeeParser constructor");
	}

	public void init(ContrailSlaTemplate contrailSlaTemplate, HashMap<VirtualSystem, VMHandler> mapping) {
		ceeObject = null;
		this.contrailSlaTemplate = contrailSlaTemplate;
		this.mapping = mapping;
	}

	public CEEObject sla2Cee() {
		String slaName = contrailSlaTemplate.getSlaTemplate().getUuid().getValue();
		CEEObject.Builder ceeBuilder = new Builder(slaName);
		ceeBuilder.setContrailSlatemplate(contrailSlaTemplate);
		ceeBuilder.setMapping(mapping);
		ceeObject = ceeBuilder.build();
		return ceeObject;
	}
}
