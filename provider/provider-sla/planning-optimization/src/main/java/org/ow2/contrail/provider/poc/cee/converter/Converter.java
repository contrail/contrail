/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.cee.converter;

import java.util.ArrayList;
import java.util.Vector;

import org.ow2.contrail.provider.poc.enums.ContrailAgreementTerm;
import org.slasoi.slamodel.sla.VariableDeclr;

public class Converter {

	private Vector<String> resourceTerms;

	private ArrayList<String> nonNegotiableTerms;

	public static final String $STND_business = "http://www.slaatsoi.org/business#";
	public static final String $STND_units = "http://www.slaatsoi.org/coremodel/units#";
	public static final String $STND_coremodel = "http://www.slaatsoi.org/coremodel#";
	public static final String $STND_slamodel = "http://www.slaatsoi.org/slamodel#";

	public Converter() {
		setResourceTerms();
		setNonNegotiableTerms();
	}

	public int find(VariableDeclr[] vars, String key) {
		System.out.println("Finding ... " + key);
		for (int i = 0; i < vars.length; i++) {
			VariableDeclr vd = vars[i];
			if (key.contains(vd.getVar().getValue())) {
				return i;
			}

		}
		return -1;
	}

	public boolean isResourceTerm(String term) {

		for (int l = 0; l < resourceTerms.size(); l++) {

			if (resourceTerms.elementAt(l).equals(term.toLowerCase()))
				return true;
		}
		return false;
	}

	public boolean isNonNegotiableTerms(String term) {
		for (int i = 0; i < nonNegotiableTerms.size(); i++) {
			if (nonNegotiableTerms.get(i).equalsIgnoreCase(term))
				return true;
		}
		return false;
	}

	public void setResourceTerms() {
		resourceTerms = new Vector<String>();
		resourceTerms.add("vm_cores");
		resourceTerms.add("cpu_speed");
		resourceTerms.add("memory");

	}

	public void setNonNegotiableTerms() {
		nonNegotiableTerms = new ArrayList<String>();
		nonNegotiableTerms.add("vm_cpu_load");
		nonNegotiableTerms.add("availability");
		nonNegotiableTerms.add("minimum_loa");
	}

	public ContrailAgreementTerm metricToAgreement(String metric) {
		metric = metric.toLowerCase();
		System.out.println("check to convert " + metric);
		if (metric.equals("vs_location"))
			return ContrailAgreementTerm.vs_location;
		if (metric.equals("storage_location"))
			return ContrailAgreementTerm.storage_location;
		if (metric.equals("cpu_speed"))
			return ContrailAgreementTerm.cpu_speed;
		if (metric.equals("vm_cores"))
			return ContrailAgreementTerm.vm_cores;
		if (metric.equals("memory"))
			return ContrailAgreementTerm.memory;
		if (metric.equals("reserve"))
			return ContrailAgreementTerm.reserve;
		if (metric.equals("co_location_rack"))
			return ContrailAgreementTerm.co_location_rack;
		if (metric.equals("not_co_location_host"))
			return ContrailAgreementTerm.not_co_location_host;
		if (metric.equals("vm_cpu_load"))
			return ContrailAgreementTerm.vm_cpu_load;
		if (metric.equals("availability"))
			return ContrailAgreementTerm.availability;
		if (metric.equals("reliability"))
			return ContrailAgreementTerm.reliability;
		if (metric.equals("minimum_loa"))
			return ContrailAgreementTerm.minimum_loa;
		return null;
	}

	public String agreementToMetric(ContrailAgreementTerm agreement) {
		if (agreement.equals(ContrailAgreementTerm.vs_location))
			return "vs_location";
		if (agreement.equals(ContrailAgreementTerm.storage_location))
			return "storage_location";
		if (agreement.equals(ContrailAgreementTerm.vm_cores))
			return "vm_cores";
		if (agreement.equals(ContrailAgreementTerm.cpu_speed))
			return "cpu_speed";
		if (agreement.equals(ContrailAgreementTerm.memory))
			return "memory";
		if (agreement.equals(ContrailAgreementTerm.reserve))
			return "reserve";
		if (agreement.equals(ContrailAgreementTerm.co_location_rack))
			return "co_location_rack";
		if (agreement.equals(ContrailAgreementTerm.not_co_location_host))
			return "not_co_location_host";
		if (agreement.equals(ContrailAgreementTerm.vm_cpu_load))
			return "vm_cpu_load";
		if (agreement.equals(ContrailAgreementTerm.availability))
			return "availability";
		if (agreement.equals(ContrailAgreementTerm.reliability))
			return "reliability";
		if (agreement.equals(ContrailAgreementTerm.minimum_loa))
			return "minimum_loa";
		return null;
	}

	public int search(String[] input, String key) {
		for (int i = 0; i < input.length; i++) {

			if (key.contains(input[i])) {
				return i;
			}

		}
		return -1;
	}

	public int search(ArrayList<String> input, String key) {
		for (int i = 0; i < input.size(); i++) {

			if (key.contains(input.get(i))) {
				return i;
			}

		}
		return -1;
	}

}
