/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.manager.pricing;

import org.slasoi.businessManager.common.model.pricing.OvfResource;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.model.pricing.UnitResource;
import org.slasoi.businessManager.common.service.PriceManager;
import org.slasoi.businessManager.common.service.PricedItemManager;
import org.springframework.beans.factory.annotation.Autowired;

public class PricingManagerImpl implements PricingManager {

	@Autowired
	private PricedItemManager pricedItemService;

	@Autowired
	private PriceManager priceService;

	public PricingManagerImpl() {
		System.out.println("INSIDE PRICING MANAGER CONSTRUCTOR");
	}

	@Override
	public double getOvfResourcePrice(String ovfId) {
		return 0;
	}

	@Override
	public OvfResource findOvfResource(String ovfDescrName, String ovfId) {
		OvfResource ovfResource = pricedItemService.findOvfResource(ovfDescrName, ovfId);
		return ovfResource;
	}

	@Override
	public UnitResource findUnitResourceByType(String type) {
		UnitResource unitResource = pricedItemService.getUnitResourceByType(type);
		return unitResource;
	}

	@Override
	public PricedItem getPricedItem(Long id) {
		return pricedItemService.getPricedItemById(id);
	}

	@Override
	public Price getPrice(Long id) {
		return priceService.getPriceById(id);
	}

	@Override
	public Price findCurrentPrice(Long productId) {
		return priceService.findCurrentPrice(productId);
	}

	@Override
	public Price findPriceOfGuarantee(Long guaranteeId) {
		return priceService.findPriceOfGuarantee(guaranteeId);
	}

	@Override
	public boolean isVariableFee(Price p) {
		return priceService.isVariableFee(p.getPriceId());
	}

	@Override
	public boolean isFixedFee(Price p) {
		return priceService.isFixedFee(p.getPriceId());
	}

	@Override
	public boolean isCompoundFee(Price p) {
		return priceService.isCompound(p.getPriceId());
	}

}
