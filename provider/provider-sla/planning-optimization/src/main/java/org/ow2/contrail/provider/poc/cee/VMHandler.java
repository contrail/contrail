/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.cee;

import org.ow2.contrail.provider.poc.enums.ContrailAgreementTerm;

public class VMHandler implements Comparable<VMHandler> {

	private String name;
	private String href;
	private double CpuFreqMin;
	private double CpuFreqMax;
	private double RamMin;
	private double RamMax;
	private double CoresMin;
	private double CoresMax;
	private double DiskMin;
	private double DiskMax;
	private String resourceURI;

	public VMHandler(String name, String href, double cpuFreqMin, double cpuFreqMax, double ramMin, double ramMax, double coresMin, double coresMax, double diskMin, double diskMax, String uRI) {
		this.name = name;
		this.href = href;
		CpuFreqMin = cpuFreqMin;
		CpuFreqMax = cpuFreqMax;
		RamMin = ramMin;
		RamMax = ramMax;
		CoresMin = coresMin;
		CoresMax = coresMax;
		DiskMin = diskMin;
		DiskMax = diskMax;
		resourceURI = uRI;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public double getCpuFreqMin() {
		return CpuFreqMin;
	}

	public void setCpuFreqMin(double cpuFreqMin) {
		CpuFreqMin = cpuFreqMin;
	}

	public double getCpuFreqMax() {
		return CpuFreqMax;
	}

	public void setCpuFreqMax(double cpuFreqMax) {
		CpuFreqMax = cpuFreqMax;
	}

	public double getRamMin() {
		return RamMin;
	}

	public void setRamMin(double ramMin) {
		RamMin = ramMin;
	}

	public double getRamMax() {
		return RamMax;
	}

	public void setRamMax(double ramMax) {
		RamMax = ramMax;
	}

	public double getCoresMin() {
		return CoresMin;
	}

	public void setCoresMin(double coresMin) {
		CoresMin = coresMin;
	}

	public double getCoresMax() {
		return CoresMax;
	}

	public void setCoresMax(double coresMax) {
		CoresMax = coresMax;
	}

	public double getDiskMin() {
		return DiskMin;
	}

	public void setDiskMin(double diskMin) {
		DiskMin = diskMin;
	}

	public double getDiskMax() {
		return DiskMax;
	}

	public void setDiskMax(double diskMax) {
		DiskMax = diskMax;
	}

	public void setDiskMax(int diskMax) {
		DiskMax = diskMax;
	}

	public String getURI() {
		return resourceURI;
	}

	public void setURI(String uRI) {
		resourceURI = uRI;
	}

	public double getUpperBound(ContrailAgreementTerm t) {
		double upperBound = -1;
		switch (t) {
		case cpu_speed:
			upperBound = getCpuFreqMax();
			break;
		case memory:
			upperBound = getRamMax();
			break;
		case vm_cores:
			upperBound = getCoresMax();
			break;
		}
		return upperBound;
	}

	public double getLowerBound(ContrailAgreementTerm t) {
		double lowerBound = -1;
		switch (t) {
		case cpu_speed:
			lowerBound = getCpuFreqMin();
			break;
		case memory:
			lowerBound = getRamMin();
			break;
		case vm_cores:
			lowerBound = getCoresMin();
			break;
		}
		return lowerBound;
	}

	@Override
	public int compareTo(VMHandler o) {
		if (this.RamMin >= o.getRamMin() && this.RamMax >= o.getRamMax() && this.CpuFreqMin >= o.getCpuFreqMin() && this.CpuFreqMax >= o.getCpuFreqMax() && this.CoresMin >= o.getCoresMin()
				&& this.getCoresMax() >= o.getCoresMax())
			return 1;
		if (this.RamMin <= o.getRamMin() && this.RamMax <= o.getRamMax() && this.CpuFreqMin <= o.getCpuFreqMin() && this.CpuFreqMax <= o.getCpuFreqMax() && this.CoresMin <= o.getCoresMin()
				&& this.getCoresMax() <= o.getCoresMax())
			return -1;
		return 0;
	}

}
