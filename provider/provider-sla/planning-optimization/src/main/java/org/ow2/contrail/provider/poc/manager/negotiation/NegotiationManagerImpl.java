/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.manager.negotiation;

import org.ow2.contrail.provider.poc.exceptions.NotSupportedVEPOperationException;
import org.ow2.contrail.provider.poc.manager.supply.SupplyManagerImpl;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.SLATemplate;
import org.springframework.beans.factory.annotation.Autowired;

public class NegotiationManagerImpl implements NegotiationManager {

	@Autowired
	private SupplyManagerImpl supplyManager;

	public NegotiationManagerImpl() {
		System.out.println("Inside NegotiationManager Constructor");
	}

	@Override
	public SLATemplate negotiate(SLATemplate templateInitial, String negotiationID) throws NotSupportedVEPOperationException {
		String userUUID = templateInitial.getPropertyValue(new STND("UserUUID"));
		SLATemplate templateFinal = null;
		try {
			templateFinal = supplyManager.negotiate(templateInitial, negotiationID, userUUID);
		} catch (NotSupportedVEPOperationException e) {
			e.printStackTrace();
			throw new NotSupportedVEPOperationException(e.getMessage());
		}
		return templateFinal;
	}

	@Override
	public SLATemplate createAgreement(SLATemplate templateAccepted, String negotiationID) {
		String userUUID = templateAccepted.getPropertyValue(new STND("UserUUID"));
		SLATemplate templateFinal = supplyManager.createAgreement(templateAccepted, negotiationID, userUUID);
		return templateFinal;
	}

}
