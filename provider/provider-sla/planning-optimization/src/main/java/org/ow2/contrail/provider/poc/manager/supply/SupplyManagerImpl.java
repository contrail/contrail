/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.manager.supply;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.poc.cee.CEEObject;
import org.ow2.contrail.provider.poc.cee.VMHandler;
import org.ow2.contrail.provider.poc.cee.converter.Cee2SlaConverter;
import org.ow2.contrail.provider.poc.cee.converter.Sla2CeeConverter;
import org.ow2.contrail.provider.poc.enums.ContrailAgreementTerm;
import org.ow2.contrail.provider.poc.exceptions.NotSupportedVEPOperationException;
import org.ow2.contrail.provider.poc.manager.resource.VepResourceManager;
import org.ow2.contrail.provider.poc.mapper.Mapper;
import org.ow2.contrail.provider.poc.mapper.VMHandlerMapper;
import org.ow2.contrail.provider.poc.slatemplate.ContrailSlaTemplate;
import org.ow2.contrail.provider.poc.slatemplate.parser.ContrailSlaTemplateParser;
import org.ow2.contrail.provider.poc.slatemplate.request.VirtualSystem;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.GenericGuarantee;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.GenericGuarantee.Value;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.OvfResourceGuarantee;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.ResourceGuarantee;
import org.slasoi.slamodel.sla.SLATemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.jersey.api.client.ClientResponse;

public class SupplyManagerImpl implements SupplyManager {

	private static final Logger logger = Logger.getLogger(SupplyManagerImpl.class.getName());

	@Autowired
	private Cee2SlaConverter cee2SlaConverter;

	@Autowired
	private Sla2CeeConverter sla2ceeConverter;

	@Autowired
	private ContrailSlaTemplateParser contrailSlaParser;

	private static final String sepr = System.getProperty("file.separator");

	private static final String confPath = System.getenv("SLASOI_HOME");

	private static final String configFile = confPath + sepr + "contrail-slamanager" + sepr +

	"planning-optimization" + sepr + "planning_optimization.properties";

	private static Properties configProp;

	private VepResourceManager vepManager;

	private Mapper vmHandlerMapper;

	public SupplyManagerImpl() {
		logger.info("Inside Supply Manager Constructor");
		setResourceManager();
		vmHandlerMapper = new VMHandlerMapper();
	}

	@Override
	public void setResourceManager() {
		createRestManager();
		vepManager.initializeVMHandlers();
		vepManager.initializeNetworkHandlers();
		vepManager.initializeConstraints();
	}

	private void createRestManager() {
		vepManager = VepResourceManager.getInstance();
		config(configFile);
		String url = null;
		if (getProperty("trustStore") != null) {
			url = "https://" + getProperty("vep-address") + ":" + getProperty("vep-port") + "/" + getProperty("vep-base-path");
			vepManager.configManager(true, getProperty("trustStore"), getProperty("trustStorePassword"));
			vepManager.setBasePath(url);
		} else {
			url = "http://" + getProperty("vep-address") + ":" + getProperty("vep-port") + "/" + getProperty("vep-base-path");
			vepManager.configManager(false, null, null);
			vepManager.setBasePath(url);
		}
		System.out.println("vep address " + url);
		System.out.println("vep connection configured");
	}

	@Override
	public SLATemplate negotiate(SLATemplate templateInitial, String negotiationID, String userUUID) throws NotSupportedVEPOperationException {
		logger.debug("########## INITIATE NEGOTIATION #########");
		logger.debug("Sla Template to Negotiate:");
		logger.debug(templateInitial);
		ContrailSlaTemplate contrailSlaTemplate = contrailSlaParser.getContrailSlat(templateInitial);
		for (VirtualSystem vs : contrailSlaTemplate.getVirtualSystems()) {
			combineResourcesRequest(vs);
		}
		HashMap<VirtualSystem, VMHandler> mapping = vmHandlerMapper.mapRequest(contrailSlaTemplate.getVirtualSystems());
		fixRequestValues(mapping, contrailSlaTemplate.getVirtualSystems());
		sla2ceeConverter.init(contrailSlaTemplate, mapping);
		CEEObject cee = sla2ceeConverter.sla2Cee();
		ClientResponse vepReply;
		try {
			vepReply = vepManager.checkFeasibilityCEE(cee, userUUID);
			JSONObject jsonResp = null;
			String res = vepReply.getEntity(String.class);
			logger.debug("Response status code: " + vepReply.getStatus());
			logger.debug("Reply from VEP: " + res);
			if (vepReply.getStatus() >= 200 && vepReply.getStatus() < 300) {
				jsonResp = new JSONObject(res);
				JSONArray vsArray = jsonResp.optJSONArray("virtualSystems");
				if (vsArray != null) {
					for (int i = 0; i < vsArray.length(); i++) {
						JSONObject vs = vsArray.getJSONObject(i);
						String ovfApp = vs.optJSONObject("virtualSystem").getString("href");
						if (vs.getString("deployability").equals("false")) {
							contrailSlaTemplate.removeVirtualSystem(/*
																	 * skip "#"
																	 * character
																	 */ovfApp.substring(1));
						}
					}
				}
				JSONArray resArray = jsonResp.optJSONArray("reservations");
				if (resArray != null) {
					for (int i = 0; i < resArray.length(); i++) {
						JSONObject j = resArray.getJSONObject(i);
						JSONObject ja = j.optJSONObject("virtualSystem");
						String application = ja.optString("href");
						int count = j.optInt("count");
						VirtualSystem vs = contrailSlaTemplate.getVirtualSystem(application.substring(1));
						GenericGuarantee reservationGuarantee = vs.getReservationGuarantee();
						List<Value> values = reservationGuarantee.getValues();
						Value value = values.get(0); // reservation has only 1
														// value
						value.setValue(new Integer(count).toString());
					}
				}
			} else if (vepReply.getStatus() == 417) {
				jsonResp = new JSONObject(res);
				contrailSlaTemplate.removeAllSharedDisks();
				logger.info("removed not supported Shared Disk from counter-offer:");
			} else if (res.startsWith("<")) { // not a JSON response
				String errMsg = "Not recognized internal server error";
				throw new NotSupportedVEPOperationException(errMsg);
			} else {
				jsonResp = new JSONObject(res);
				String errMsg = jsonResp.optString("error");
				throw new NotSupportedVEPOperationException(errMsg);
			}
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		} catch (NotSupportedVEPOperationException e) {
			logger.debug(e.getMessage());
			throw new NotSupportedVEPOperationException(e.getMessage());
		}
		cee2SlaConverter.init();
		
		cee2SlaConverter.setContrailSlatemplate(contrailSlaTemplate);

		SLATemplate templateFinal = cee2SlaConverter.cee2Sla();
		logger.info("Sla Template negotiated:");
		logger.info(templateFinal);
		return templateFinal;
	}

	@Override
	public SLATemplate createAgreement(SLATemplate templateAccepted, String negotiationID, String userUUID) {
		logger.info("########## INITIATE CREATE AGREEMENT #########");
		logger.debug("Sla Template to Agree:");
		logger.debug(templateAccepted);
		ContrailSlaTemplate contrailSlaTemplate = contrailSlaParser.getContrailSlat(templateAccepted);

		// since already negotiated, remove ovf resource requests
		contrailSlaTemplate.removeAllOvfResourceRequests();
		HashMap<VirtualSystem, VMHandler> mapping = vmHandlerMapper.mapRequest(contrailSlaTemplate.getVirtualSystems());
		sla2ceeConverter.init(contrailSlaTemplate, mapping);
		CEEObject cee = sla2ceeConverter.sla2Cee();
		String cee_id = null;
		JSONObject jsonResp = null;
		ClientResponse vepReply = vepManager.createCEE(cee, userUUID);
		String res = vepReply.getEntity(String.class);
		logger.debug("Reply from VEP: " + res);
		try {
			jsonResp = new JSONObject(res);
			cee_id = jsonResp.getString("id");
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		cee2SlaConverter.init();
		
		cee2SlaConverter.setContrailSlatemplate(contrailSlaTemplate).setCeeID(cee_id);

		SLATemplate templateFinal = cee2SlaConverter.cee2Sla();
		logger.debug("Sla Template Agreed:");
		logger.debug(templateFinal);
		return templateFinal;
	}

	private void config(String filename) {
		configProp = new java.util.Properties();
		try {
			configProp.load(new FileReader(filename));
		} catch (Exception eta) {
			logger.debug(eta.getMessage());
		}
	}

	private static String getProperty(String key) {
		String value = configProp.getProperty(key);
		return value;
	}

	public VirtualSystem combineResourcesRequest(VirtualSystem vs) {
		org.ow2.contrail.provider.poc.enums.ContrailAgreementTerm[] termsToMatch = new ContrailAgreementTerm[] { ContrailAgreementTerm.vm_cores, ContrailAgreementTerm.memory,
				ContrailAgreementTerm.cpu_speed };
		for (ContrailAgreementTerm t : termsToMatch) {
			double min = 0, max = 0;
			ArrayList<ResourceGuarantee> resourceOfTerm = vs.getResourcesOfTerm(t);
			if (resourceOfTerm.size() == 2) {
				ResourceGuarantee r0 = resourceOfTerm.get(0);
				ResourceGuarantee r1 = resourceOfTerm.get(1);
				if (r0 instanceof OvfResourceGuarantee) {
					// if ovf doesn't specify lower bound, set sla request as
					// lower bound
					min = (r0.getDefault() != -1) ? r0.getDefault() : r1.getMin();
					max = (r1.getMax() != -1) ? Math.max(r0.getMax(), r1.getMax()) : -1;
					vs.removeGuarantee(r0);
					vs.removeGuarantee(r1);
					ResourceGuarantee newG = new ResourceGuarantee(r1.getId(), r1.getAgreementTerm());
					newG.setDomain(r1.getDomain());
					newG.setRanges(min, max, max);
					vs.addGuarantee(newG);
				} else if (r1 instanceof OvfResourceGuarantee) {
					// if ovf doesn't specify lower bound, set sla request as
					// lower bound
					min = (r1.getDefault() != -1) ? r1.getDefault() : r0.getMin();
					max = (r0.getMax() != -1) ? Math.max(r0.getMax(), r1.getMax()) : -1;
					vs.removeGuarantee(r0);
					vs.removeGuarantee(r1);
					ResourceGuarantee newG = new ResourceGuarantee(r0.getId(), r0.getAgreementTerm());
					newG.setDomain(r0.getDomain());
					newG.setRanges(min, max, max);
					vs.addGuarantee(newG);
				}
			} else if (resourceOfTerm.size() == 0) {
				// if not explicitly declared, initialize default resources with
				// -2
				ResourceGuarantee newGuarantee = new ResourceGuarantee("GeneratedGuaranteeForTerm" + t.toString(), t);
				newGuarantee.setDefault(-2);
				vs.addGuarantee(newGuarantee);
			}
		}
		return vs;
	}

	public void fixRequestValues(HashMap<VirtualSystem, VMHandler> mapping, Collection<VirtualSystem> collection) {
		for (VirtualSystem vs : collection) {
			VMHandler vmh = mapping.get(vs);
			ArrayList<ResourceGuarantee> requests = vs.getResourceGuarantees();
			for (ResourceGuarantee r : requests) {
				if (r.getDefault() == -1) {
					// if not specified, set the default value to upper bound
					// of vmHandler
					r.setDefault(vmh.getUpperBound(r.getAgreementTerm()));
				} else if (r.getDefault() == -2) {
					// if not specified, set the default value to lower bound
					// of vmHandler
					r.setDefault(vmh.getLowerBound(r.getAgreementTerm()));
				} else
				// if upper bound of request is greater than the upper bound of
				// vm Handler, set accordingly
				if (r.getMax() > vmh.getUpperBound(r.getAgreementTerm())) {
					r.setMax(vmh.getUpperBound(r.getAgreementTerm()));
					r.setDefault(vmh.getUpperBound(r.getAgreementTerm()));
				} else
				// if lower bound of request is lower than the lower bound of
				// vm Handler, set accordingly
				if (r.getMin() < vmh.getLowerBound(r.getAgreementTerm())) {
					r.setMin(vmh.getLowerBound(r.getAgreementTerm()));
					r.setDefault(vmh.getLowerBound(r.getAgreementTerm()));
				}
				// fixedResources.addResource(r);
			}
		}
	}
}
