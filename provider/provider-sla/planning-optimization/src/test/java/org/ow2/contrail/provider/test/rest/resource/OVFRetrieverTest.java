/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.test.rest.resource;

import org.junit.Test;
import org.ow2.contrail.provider.poc.manager.resource.OVFRetriever;

public class OVFRetrieverTest {

	//@Test
	public void test() {
		OVFRetriever ovfR = new OVFRetriever();
		ovfR.retrieveOvf("http://contrail.xlab.si/test-files/ubuntu-test-xlab.ovf");
		// ovfR.retrieveOvf("OVFPasquale.ovf");
	}

}
