/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.cee.test;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.ow2.contrail.provider.poc.cee.CEEObject;
import org.ow2.contrail.provider.poc.cee.VMHandler;
import org.ow2.contrail.provider.poc.cee.CEEObject.Builder;
import org.ow2.contrail.provider.poc.manager.resource.VepResourceManager;
import org.ow2.contrail.provider.poc.slatemplate.ContrailSlaTemplate;
import org.ow2.contrail.provider.poc.slatemplate.parser.ContrailSlaTemplateParser;
import org.ow2.contrail.provider.poc.slatemplate.request.VirtualSystem;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLATemplate;

public class CEEObjectBuilderTest {

	private static SLATemplate slatProposal;

	private VMHandler vmSmall = new VMHandler("Small VM", "vep.fr/VMHandler/1", 800, 1000, 256, 1024, 1, 2, 1024, 51200, "http://vep.fr/vmhandlers/112");
	private VMHandler vmMedium = new VMHandler("Medium VM", "vep.fr/VMHandler/2", 1200, 1800, 1024, 2048, 4, 8, 51200, 204800, "http://vep.fr/vmhandlers/113");
	private VMHandler vmBig = new VMHandler("Big VM", "vep.fr/VMHandler/3", 1200, 1800, 2048, 16384, 4, 16, 51200, 204800, "http://vep.fr/vmhandlers/114");

	private static ContrailSlaTemplate cst = null;

	private static final String sepr = System.getProperty("file.separator");

	private static final String confPath = System.getenv("SLASOI_HOME");

	private static final String configFile = confPath + sepr + "contrail-slamanager" + sepr +

	"planning-optimization" + sepr + "planning_optimization.properties";

	private VepResourceManager vepManager;

	private static Properties configProp;

	@Before
	public void setUp() throws Exception {

		String proposalXml = FileUtils.readFileToString(new File("src/test/resources/slats/SlaTemplateFINAL.xml"));
		SLASOITemplateParser tp = new SLASOITemplateParser();
		slatProposal = tp.parseTemplate(proposalXml);
		cst = ContrailSlaTemplateParser.getContrailSlat(slatProposal);
		System.out.println("PROPOSAL: " + cst);
		setupVep();
	}

	private void setupVep() {
		vepManager = VepResourceManager.getInstance();
		configProp = new java.util.Properties();
		try {
			configProp.load(new FileReader(configFile));
		} catch (Exception eta) {
			eta.printStackTrace();
		}
		;
		String url = null;
		if (getProperty("trustStore") != null) {
			url = "https://" + getProperty("vep-address") + ":" + getProperty("vep-port") + "/" + getProperty("vep-base-path");
			vepManager.configManager(true, getProperty("trustStore"), getProperty("trustStorePassword"));
			vepManager.setBasePath(url);
		} else {
			url = "http://" + getProperty("vep-address") + ":" + getProperty("vep-port") + "/" + getProperty("vep-base-path");
			vepManager.configManager(false, null, null);
			vepManager.setBasePath(url);
		}
		System.out.println("vep address " + url);
		System.out.println("vep connection configured");
		vepManager.initializeVMHandlers();
		vepManager.initializeNetworkHandlers();
		vepManager.initializeConstraints();

	}

	// @Test
	public void testCEEObjectBuilder() {
		CEEObject.Builder builder = new Builder(cst.getSlaTemplate().getUuid().getValue());
		builder.setContrailSlatemplate(cst);

		HashMap<VirtualSystem, VMHandler> mapping = new HashMap<VirtualSystem, VMHandler>();
		for (VirtualSystem vs : cst.getVirtualSystems()) {
			mapping.put(vs, vmMedium);
		}
		builder.setMapping(mapping);

		CEEObject cee = builder.build();
		System.out.println(cee);
	}

	private static String getProperty(String key) {
		String value = configProp.getProperty(key);
		return value;
	}
}
