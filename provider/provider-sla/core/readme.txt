NOTES about this artifact
-------------------------

* this artifact contains classes that will be used 
by each component within contrail-slam4osgi.

* Normally the 'core' module contains only interfaces
and data types.

* All contained classes will be exposed by the 
contrail-slam4osgi bundle within OSGi. 

