/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.contrail.vep;

import java.io.IOException;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import javax.ws.rs.core.UriBuilder;
import org.glassfish.grizzly.http.server.HttpServer;
import java.net.URI;
import java.util.Properties;

public class VepContainerMock {
	private static VepContainerMock container = null;
	// todo in future will be configured
	private static Properties configFile;

	private static URI BASE_URI;
	private static int defaultPort = 9998;

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://" + getProperty("vep-mock-url") + "/").port(defaultPort).build();
	}

	private static int getPort() {

		String port = getProperty("vep-mock-port");
		if (null != port) {
			try {
				return new Integer(port);
			} catch (NumberFormatException e) {
			}
		}
		return defaultPort;
	}

	private VepContainerMock() {
		Config();
		defaultPort = getPort();
		BASE_URI = getBaseURI();

	}

	private void Config() {
		configFile = new java.util.Properties();
		try {
			configFile.load(this.getClass().getClassLoader().getResourceAsStream("vepmock.config"));
		} catch (Exception eta) {
			eta.printStackTrace();
		}
	}

	public static String getProperty(String key) {
		String value = configFile.getProperty(key);
		return value;
	}

	public static VepContainerMock getInstance() {
		if (container == null) {
			container = new VepContainerMock();
		}
		return container;
	}

	public static HttpServer startServer() throws IOException {

		ResourceConfig rc = new PackagesResourceConfig("org.contrail.vep.resource");
		container = new VepContainerMock();
		System.out.println("Starting grizzly...");
		HttpServer server = GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
		server.start();
		return server;
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) throws IOException {
		HttpServer server = startServer();
		// port can change value based on jersey.http.port, don't hard code in
		// println
		int port = defaultPort;
		System.out.println("Server running on http://" + getProperty("vep-mock-url") + ":" + port + ",");
		System.out.println("press enter to stop the server");
		// System.in.read();
		try {
			while (true) {
				Thread.sleep(1000);
			}
		} catch (InterruptedException ie) {
			Thread.currentThread().interrupt();
		}
		System.out.println("Stopping server");
		server.stop();
		// httpServer.stop();
		System.out.println("Server stopped");
	}
}
