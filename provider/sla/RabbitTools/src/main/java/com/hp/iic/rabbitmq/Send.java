package com.hp.iic.rabbitmq;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;


public class Send {

	private CommandLine cmd; 

	private static Options options = null;
	private static final String cmdQueue = "q";
	private static final String cmdHost = "h";
	private static final String cmdPort = "p";
	private static final String cmdMess = "m";
	private static final String cmdUsername = "user";
	private static final String cmdPassword = "passwd";
	private static final String cmdVirtualhost = "vh";

	static{
		options = new Options();
		options.addOption(cmdQueue, true, "Queue name (mandatory)");
		options.addOption(cmdHost, true, "host (default: locahost)");
		options.addOption(cmdPort, true, "port (default: 5672)");
		options.addOption(cmdUsername, true, "username");
		options.addOption(cmdPassword, true, "password");
		options.addOption(cmdVirtualhost, true, "virtualhost");

		options.addOption(cmdMess, true, "message");
	}


	private static String host = "localhost";
	private static String port = "5672";
	private static String username = null;
	private static String password = null;
	private static String virtualhost = null;
	private static String queue;
	private static String mess;



	/**
	 * Check and set command line arguments
	 * @param args String[] arguments passed in main()
	 */
	private void loadArgs(String[] args){

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (!cmd.hasOption(cmdQueue) || !cmd.hasOption(cmdMess)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("Send", options);
			System.exit(1);
		}

		if (cmd.hasOption(cmdHost)) {
			host = cmd.getOptionValue(cmdHost);
		}

		if (cmd.hasOption(cmdPort)) {
			port = cmd.getOptionValue(cmdPort);
		}

		if (cmd.hasOption(cmdUsername)) {
			username = cmd.getOptionValue(cmdUsername);
		}

		if (cmd.hasOption(cmdPassword)) {
			password = cmd.getOptionValue(cmdPassword);
		}

		if (cmd.hasOption(cmdVirtualhost)) {
			virtualhost = cmd.getOptionValue(cmdVirtualhost);
		}

		
		queue = cmd.getOptionValue(cmdQueue);
		mess = cmd.getOptionValue(cmdMess);

		System.out.println("\nUsing: \nHost: " + host + 
				"\nPort: " + port + "\nVirtualHost: " + virtualhost +
				"\nUsername: " + username + "\nPassword: " + password +
				"\nQueue: " + queue + "\nMessage: " + mess);

	}	



	void SendMessage(String host, String port, String queue, String mess, 
			String username, String password, String virtualhost) 
			throws Exception {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(host);
		if (null != username) factory.setUsername(username);
		if (null != password) factory.setPassword(password);
		if (null != virtualhost) factory.setVirtualHost(virtualhost);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(queue, false, false, false, null);
		channel.basicPublish("", queue, null, mess.getBytes());
		System.out.println(" [x] Sent '" + mess + "'");

		channel.close();
		connection.close();

	}




	public static void main(String[] argv) throws Exception {

		Send inst = new Send();

		inst.loadArgs(argv);

		try {
			inst.SendMessage(host, port, queue, mess, username, password, virtualhost);
		} catch (Exception e) {
			System.err.println("Message could not be sent!");
			e.printStackTrace();
		}	  

	}
}
