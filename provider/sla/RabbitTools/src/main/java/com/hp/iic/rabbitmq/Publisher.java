package com.hp.iic.rabbitmq;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;


public class Publisher {

	private CommandLine cmd; 

	private static Options options = null;
	private static final String cmdExchange = "e";
	private static final String cmdHost = "h";
	private static final String cmdPort = "p";
	private static final String cmdMess = "m";
	private static final String cmdFile = "f";
	private static final String cmdUsername = "user";
	private static final String cmdPassword = "passwd";
	private static final String cmdVirtualhost = "vh";

	static{
		options = new Options();
		options.addOption(cmdExchange, true, "Exchange name (mandatory)");
		options.addOption(cmdHost, true, "host (default: locahost)");
		options.addOption(cmdPort, true, "port (default: 5672)");
		options.addOption(cmdUsername, true, "username");
		options.addOption(cmdPassword, true, "password");
		options.addOption(cmdVirtualhost, true, "virtualhost");
		options.addOption(cmdFile, true, "file containing the message");
		options.addOption(cmdMess, true, "message");
	}


	private static String host = "localhost";
	private static String port = "5672";
	private static String username = null;
	private static String password = null;
	private static String virtualhost = null;
	private static String exchange;
	private static String mess;


	private String readFile( String file ) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader (file));
	    String line  = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    String ls = System.getProperty("line.separator");
	    while( ( line = reader.readLine() ) != null ) {
	        stringBuilder.append( line );
	        stringBuilder.append( ls );
	    }
	    return stringBuilder.toString();
	 }

	
	

	/**
	 * Check and set command line arguments
	 * @param args String[] arguments passed in main()
	 * @throws IOException 
	 */
	private void loadArgs(String[] args) throws IOException{

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (!cmd.hasOption(cmdExchange) || !(cmd.hasOption(cmdFile) || cmd.hasOption(cmdMess))) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("Publisher", options);
			System.exit(1);
		}

		if (cmd.hasOption(cmdHost)) {
			host = cmd.getOptionValue(cmdHost);
		}

		if (cmd.hasOption(cmdPort)) {
			port = cmd.getOptionValue(cmdPort);
		}

		if (cmd.hasOption(cmdUsername)) {
			username = cmd.getOptionValue(cmdUsername);
		}

		if (cmd.hasOption(cmdPassword)) {
			password = cmd.getOptionValue(cmdPassword);
		}

		if (cmd.hasOption(cmdVirtualhost)) {
			virtualhost = cmd.getOptionValue(cmdVirtualhost);
		}

		if (cmd.hasOption(cmdMess)) {
			mess = cmd.getOptionValue(cmdMess);
		}

		if (cmd.hasOption(cmdFile)) {
			mess = readFile(cmd.getOptionValue(cmdFile));
		}

		exchange = cmd.getOptionValue(cmdExchange);

		System.out.println("\nUsing: \nHost: " + host + 
				"\nPort: " + port + "\nVirtualHost: " + virtualhost +
				"\nUsername: " + username + "\nPassword: " + password +
				"\nExchange: " + exchange + "\nMessage:\n" + mess);

	}	



	  public void publish(String host, String port, String exchange, String mess, 
				String username, String password, String virtualhost) throws Exception {

		    ConnectionFactory factory = new ConnectionFactory();
		    factory.setHost(host);
			if (null != username) factory.setUsername(username);
			if (null != password) factory.setPassword(password);
			if (null != virtualhost) factory.setVirtualHost(virtualhost);

		    Connection connection = factory.newConnection();
		    Channel channel = connection.createChannel();

	//	    channel.exchangeDeclare(exchange, "fanout", true);

		    channel.basicPublish(exchange, "#", MessageProperties.PERSISTENT_TEXT_PLAIN, mess.getBytes());
		    System.out.println(" [x] Sent '" + mess + "'");

		    channel.close();
		    connection.close();
		  }


	  public static void main(String[] argv) throws Exception {

			Publisher inst = new Publisher();

			inst.loadArgs(argv);

			try {
				inst.publish(host, port, exchange, mess, username, password, virtualhost);
			} catch (Exception e) {
				System.err.println("Message could not be sent!");
				e.printStackTrace();
			}	  
		  }



}
