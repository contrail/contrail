package com.hp.iic.rabbitmq;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;


public class Subscriber {

	   private static final String EXCHANGE_NAME = "rchannel";

	    public static void main(String[] argv)
	                  throws java.io.IOException,
	                  java.lang.InterruptedException {

	        ConnectionFactory factory = new ConnectionFactory();
	        factory.setHost("10.15.5.52");
	    	//factory.setUsername("slasoi");
	    	//factory.setPassword("slasoi");
	    	//factory.setVirtualHost("/slasoi");

	        Connection connection = factory.newConnection();
	        Channel channel = connection.createChannel();

	        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
	        String queueName = channel.queueDeclare().getQueue();
	        channel.queueBind(queueName, EXCHANGE_NAME, "#");

	        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

	        QueueingConsumer consumer = new QueueingConsumer(channel);
	        
	        channel.basicConsume(queueName, true, consumer);

	        while (true) {
	            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
	            String message = new String(delivery.getBody());

	            System.out.println(" [x] Received '" + message + "'");   
	        }
	    }
}