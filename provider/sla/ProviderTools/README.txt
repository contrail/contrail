##
 # Copyright 2014 Hewlett-Packard Development Company, L.P.                
 #                                                                          
 # Licensed under the Apache License, Version 2.0 (the "License");         
 # you may not use this file except in compliance with the License.        
 # You may obtain a copy of the License at                                 
 #                                                                          
 #     http://www.apache.org/licenses/LICENSE-2.0                          
 #                                                                          
 # Unless required by applicable law or agreed to in writing, software     
 # distributed under the License is distributed on an "AS IS" BASIS,       
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and     
 # limitations under the License.                                          
##
--------------------------------------------------------------------------------
            		   Provider Tools
--------------------------------------------------------------------------------

Support: For any question about the integration send an email to: 
christian.temporale@hp.com


Provider Tools: The provider tools are the command line tools used by Cloud 
Providers to insert new Products, new SLA Templates or new Product Offers into 
the repository and to approve a party (typically, the Customer) after its 
creation.

The provider tools available are:

1) AddProduct
2) AddTemplate
3) AddProductOffert
4) GetTemplate
5) GetTemplateList
6) GetProductList
7) ApproveParty

While tools 1,2,3, and 7 have effects in the SLA@SOI DB (inserts/updates), 
tools 4,5,6 are used to read contents only.

AddProduct
-----------

The AddProduct allows to insert a new product into a repository; this product 
will be uniquely identified by id into database. Some parameters are mandatory:

-b <arg>     Product brand. 
-d <arg>     Product description. 
-df <arg>    The date (format: YYYYMMDD; default: today) the product offer is 
             valid from.
-di <arg>    The date (format: YYYYMMDD; default: today) when the product offer
             is inserted.
-dt <arg>    The date (format: YYYYMMDD; default: 2 years later) the product 
             offer is valid to.
-n <arg>     Product name. 
-p <arg>     ID in the database, uniquely identifying the party, assigned at 
             the time of its creation.


Some of these values may be omitted, and therefore will be assigned default 
values.

Example:
./AddProduct.sh -b BrandTest -d DescriptionTest -n ProductName -p 465 



AddTemplate
------------

The AddTemplate adds a new SLA template into a SLA Template registry (Business 
SLAM registry, or Software SLAM regitry or Infrastructure SLAM registry), and 
it will be uniquely identified by an ID into the database. The parameters are:

-d <arg>      SLA Template description. 
-p <arg>      Provider ID in the database, assigned at the time of its 
              creation.
-r <arg>      Register name. 
-slam <arg>   SLA@SOI SLAM DB [BSLAM (default) | SSLAM | ISLAM].It defines the
              database in which will be inserted the template.
-t <arg>      The file that contain the SLA template. If it is not in the 
              Provider Tools directory, you must specify the file path.

Example:
./AddTemplate.sh -d TemplateDescription -p 465 -r RegisterName -slam BSLAM 
-t template.txt

In this case will add the template defined in the file "template.txt" into 
business SLA manager registry for the provider having ID=465.



AddProductOffer
----------------

To add a new product offer, the Provider can use the AddProductOffer program; 
this creates an association between a product and a SLA template. 
The parameters are:


-d <arg>    Product offer description. 
-df <arg>   The date (format: YYYYMMDD; default: today) the product offer 
            is valid from.
-di <arg>   The date (format: YYYYMMDD; default: today) when the product offer 
            is inserted.
-dt <arg>   The date (format: YYYYMMDD; default: 2 years later) when the 
            product offer expires.
-n <arg>    Product Offer name. 
-p <arg>    ID in the database, uniquely identifying the product, 
            assigned at the time of its creation. 
-pa <arg>   ID in the database, uniquely identifying the party, assigned at 
            the time of its creation. 
-t <arg>    ID in the database, uniquely identifying the SLA Template, 
            assigned when it's entered into the database. 
  

Example:
./AddProductOffer.sh -d ProductOfferDescription -n ProductOffernName -p 9 
 -pa 465 -t CT-Contrail-new-SLAT-8-NoVariable

This way AddProductOffer will create an association between the product having
ID=9 and the SLA template identified by ID=CT-Contrail-new-SLAT-8-NoVariable,
for party with ID=465.



GetTemplate
------------

GetTemplate shows the list of templates into a given registry; if the call 
does not specify a registry, the template will be searched into the Business 
SLA Manager.

-slam <arg>   SLA@SOI SLAM DB [BSLAM (default) | SSLAM | ISLAM]. It defines 
              the database in which search the template.
-t <arg>      Template ID in the database, uniquely identifying the Template. 

Example:
./GetTemplate.sh -t CT-Contrail-new-SLAT-8-NoVariable



GetTemplateList
----------------

GetTemplateList shows the list of templates defined in the input registry 
-or defaults to the registry BSLAM- associated to a product identified by
an input id.

-host <arg>   SLA@SOI host (default: localhost)
-p <arg>      Product ID in the database, assigned at the time of its creation.
-port <arg>   SLA@SOI port (default: 8080)
-slam <arg>   SLA@SOI SLAM DB [BSLAM (default) | SSLAM | ISLAM]. It defines 
              the database in which to search.
 
Example:
./GetTemplateList.sh -p 9
This will diplay the list of templates associated to the product with ID=9.



GetProductList
---------------
 
GetProductList shows the list of products present in the repository.
 
Example:
./GetProductList.sh



ApproveParty
-------------
  
After a party (Customer or Provider) is created, it has to be approved and it 
has to be assigned a budget, so that it can initiate a new negotiation. It is 
necessary to invoke the ApproveParty tool, in order to allow the party to pass 
from PENDING state (assigned at creation time) to APPROVED state, and to define
the initial budget for the negotiation.
This is an essential part of the process to complete the party registration 
into the system.

-b <arg>    Initial balance assigned to the party.
-cl <arg>   Credit Limit available to the party.
-p <arg>    Product ID in the database, assigned at time of its creation.

Example:
./ApproveParty.sh -b 10 -cl 5000 -p 465
 
This will change the state of party with ID=465 from PENDING to APPROVED state, 
and will assign the initial budget and balance.

