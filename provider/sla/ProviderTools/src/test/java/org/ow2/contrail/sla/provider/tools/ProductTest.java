/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.tools;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.ow2.contrail.sla.provider.slasoi.Guarantees;
import org.ow2.contrail.sla.provider.slasoi.PricedItems;
import org.ow2.contrail.sla.provider.slasoi.Prices;
import org.ow2.contrail.sla.provider.slasoi.Products;
import org.ow2.contrail.sla.provider.tools.pricing.AddOvfResource;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.springframework.test.AbstractTransactionalSpringContextTests;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@org.springframework.stereotype.Service(value="productDiscovery")
public class ProductTest extends AbstractTransactionalSpringContextTests{ 

	
	@Override
	protected String[] getConfigLocations() {
		return new String[] { "META-INF/spring/contrail/bm-common-context.xml" };
	}

	public ProductTest( String testName ) {
		super( testName );
		System.out.println("Initialization...");
		this.setDefaultRollback(true);
	}

	
	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite()
	{
		return new TestSuite( ProductTest.class );
	}



	/**
	 * Test create product.
	 */
	public void _testCreateProduct(){
		try{
			System.out.println("Creating product...");
			Products prods = new Products();
			Calendar ini = new GregorianCalendar();
			Calendar fin = (Calendar) ini.clone();
			fin.add(Calendar.YEAR, 2);
			prods.createProduct("CONTRAIL product test 2", "Product CONTRAIL test 2", "HP IIC", "1.0", 
					(long) 333, new BigDecimal(100), new BigDecimal(100), new BigDecimal(100), 
					new BigDecimal(10), new BigDecimal(1000), "A", "A", ini, ini, fin);

		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	

	/**
	 * Test create product.
	 */
	public void _testCreateProductOffer(){
		try{
			System.out.println("Creating product offer...");
			Products prods = new Products();
			Calendar ini = new GregorianCalendar();
			Calendar fin = (Calendar) ini.clone();
			fin.add(Calendar.YEAR, 2);
			prods.createProductOffer(11, "CONTRAIL Offer test 2", "Offer CONTRAIL test", ini, 
					ini, fin, "1.0", "A", 1, "CONTRAILBUSINESSSLAT3", 333);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}



	/**
	 * Find products
	 */
	public void _testFindProduct(){
		try{
			Products prods = new Products();

			List<EmSpProducts> LProds = prods.getAllProducts();
			for(EmSpProducts prod : LProds){
				System.out.println("Product ID: " + prod.getNuProductid());
				System.out.println("Product name: " + prod.getTxProductname());
				System.out.println("Product desc: " + prod.getTxProductdesc());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	/**
	 * Find pricedItems
	 */
	public void _testFindPricedItems(){
		try{
			PricedItems pricedItems= new PricedItems();

			PricedItem pI=pricedItems.getPricedItemByName("1 GB Memory");
			System.out.println("PricedItem ID: " + pI.getProductId());
			System.out.println("PricedItem name: " + pI.getProductName());
			System.out.println("PricedItem desc: " + pI.getProductDescr());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Find pricedItems
	 */
	public void _testCreatePricedItems(){
		try{
			PricedItems pricedItems= new PricedItems();

			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Find pricedItems
	 */
	public void _testDeletePricedItems(){
		try{
			PricedItems pricedItems= new PricedItems();

			pricedItems.deletePricedItem(pricedItems.getPricedItemByName("1 GB Memory").getProductId());
			System.out.println("PricedItem deleted!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Create unitResource
	 */
	public void _testCreateUnitResource(){
		try{
			PricedItems pricedItems= new PricedItems();
			pricedItems.createUnitResource("prova", "prova", false, "", "cpu", "MB");
			
			System.out.println("UnitResource created!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Find pricedItems
	 */
	public void _testDeleteUnitResource(){
		try{
			PricedItems pricedItems= new PricedItems();

			pricedItems.deleteUnitResource(pricedItems.getPricedItemByName("prova").getProductId());
			System.out.println("Unit Resource deleted!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Create unitResource
	 */
	public void _testCreateCompoundResource(){
		try{
			PricedItems pricedItems= new PricedItems();
			ArrayList<Long> ids=new ArrayList<Long>();
			ArrayList<Long> ids2=new ArrayList<Long>();
			ids.add(new Long(24));
			ids.add(new Long(25));
			ids.add(new Long(26));
			
			System.out.println("CompoundResource created!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Find pricedItems
	 */
	public void _testDeleteCompoundResource(){
		try{
			PricedItems pricedItems= new PricedItems();

			pricedItems.deleteCompoundResource(new Long("15"));
			System.out.println("Compound Resource deleted!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void _testCreateFixedFee(){
		try{
			Prices prices=new Prices();
			Calendar now=Calendar.getInstance();
			now.set(2012, 0, 1, 00, 00, 00);
			Calendar end=Calendar.getInstance();
			end.set(2012, 11, 31, 23, 59, 59);
			//prices.createFixedFee(new BigDecimal(100), "EUR", "per_month", now.getTime(), end.getTime(), "per_hour", 1, "vm");
			//prices.createVariableFee(new BigDecimal("0.15"), "EUR", "per_month", now.getTime(), end.getTime(), "per_hour", 1, "GB", "less_than_or_equals", "10" , "GB");
			ArrayList<Long> ids=new ArrayList<Long>();
			ids.add(new Long("10"));
			ArrayList<Long> ids2=new ArrayList<Long>();
			ids2.add(new Long("11"));
			Long l=prices.createCompoundFee("EUR", "per_month",now.getTime(), end.getTime(), ids2, ids);
			System.out.println("Fixed Fee created! id: "+l);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void _testAssociatePrice(){
		try{
			Prices prices=new Prices();
			prices.associatePrice(new Long("2"), new Long("1"));
			System.out.println("Price associated!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void _testDeletePrice(){
		try{
			Prices prices=new Prices();
			prices.deletePrice(new Long("6"));
			System.out.println("Price deleted!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void _testCreateGuarantee(){
		try{
			Guarantees guarantees=new Guarantees();
			
			System.out.println("Guarantee created!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void _testDeleteGuarantee(){
		try{
			Guarantees guarantees=new Guarantees();
			guarantees.deleteGuarantee(new Long("1"));
			System.out.println("Guarantee deleted!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void _testAssociateProduct(){
		try{
			Guarantees guarantees=new Guarantees();
			guarantees.associateProduct(new Long("1"), new Long("1"));
			System.out.println("Product Associated!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void _test(){
		PricedItems pricedItems=new PricedItems();
		Guarantees guarantees=new Guarantees();
		Prices prices=new Prices();
		ArrayList<Long> ids=new ArrayList<Long>();
		ArrayList<Long> ids2=new ArrayList<Long>();
		ids.add(new Long(4));
		ids.add(new Long(5));
		Calendar now=Calendar.getInstance();
		now.set(2012, 0, 1, 00, 00, 00);
		Calendar end=Calendar.getInstance();
		end.set(2012, 11, 31, 23, 59, 59);
		pricedItems.deleteOvfResource(new Long(7));
		System.out.println("Test Finished!");
	}
	
	public void _test2(){
		PricedItems pricedItems=new PricedItems();
		ArrayList<Long> a=new ArrayList<Long>();
		a.add(new Long(17));
		//pricedItems.createOvfDescriptor("lamp-multi-vm", "Contrail-SLAT-NewFeatures-01", a);
		pricedItems.createUnitResource("Price of vm_cores", "Price of a virtual core", false, "vm_cores", "vm_cores", "integer");
		System.out.println("Test Finished!");
	}
	
	/**
	 * Dummy
	 */
	public void _testDummy(){
		assertTrue(true);
	}


}
