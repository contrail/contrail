/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.slasoi;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.slasoi.businessManager.common.model.EmBillingFrecuency;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmProductOfferSlat;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.ProductManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.common.service.ServiceManager;
import org.slasoi.businessmanager.ws.impl.BusinessManager_QueryProductCatalogStub;
import org.slasoi.businessmanager.ws.impl.BusinessManager_QueryProductCatalogStub.GetProducts;
import org.slasoi.businessmanager.ws.impl.BusinessManager_QueryProductCatalogStub.GetProductsResponse;
import org.slasoi.businessmanager.ws.impl.BusinessManager_QueryProductCatalogStub.Product;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Products {

	
	@Resource
	private ProductManager productManager; 

	@Resource
	private ProductOfferManager productOfferManager; 

	@Resource
	private ServiceManager serviceManager; 

	
	private ClassPathXmlApplicationContext context = null;
	
	private final String serviceName = "BusinessManager_QueryProductCatalog";
	private final String host = "localhost";
	private final String port = "8080";

	
	
	public Products() {
		context = new ClassPathXmlApplicationContext("META-INF/spring/contrail/bm-common-context.xml");
		productOfferManager = (ProductOfferManager)context.getBean("productOfferService"); 
		productManager = (ProductManager)context.getBean("productService");
		serviceManager = (ServiceManager)context.getBean("serviceService");
	}

	
		
	/**
	 * Get all available products from slasoi DB
	 * @return List<EmSpProducts> List of all available products
	 */
    public List<EmSpProducts> getAllProducts() throws Exception {
       	return productManager.getAllInit();
   }	
	
    
 
    /**
     * Retrieves a product from its ID
     * @param prodId the product ID
     * @return returned product
     * @throws Exception
     */
    public EmSpProducts getProduct(Long prodId) throws Exception {
       	return productManager.getProductById(prodId);
    }	
	
    
    
	/**
	 * Retrieves the list of SLA templates for a given product, via WS
	 * @param customerId the ID of the customer (optional)
	 * @param host
	 * @param port
	 * @return list of products
	 * @throws RemoteException
	 */
	public Product[] getProducts(String customerId, String host, String port) 
			throws RemoteException {
		
		if (host == null)
			host = this.host;
		
		if (port == null)
			port = this.port;
		
		String bmQueryProductWSURL = "http://" + host + ":" + port + "/services/" + 
					serviceName + "?wsdl";
		BusinessManager_QueryProductCatalogStub qpcWSClient =
			new BusinessManager_QueryProductCatalogStub(bmQueryProductWSURL);	
	        
        GetProducts gp = new GetProducts();
        gp.setCustomerID(customerId);
        GetProductsResponse response = qpcWSClient.getProducts(gp);
        
        return response.get_return();
	}
    
    
        
	/**
	 * Add a new Product entity in the slasoi DB
	 * @param txName The product name
	 * @param txDesc The product description
	 * @param txBrand The product brand name
	 * @param txRel The release number
	 * @param party The ID of the party (provider/customer)
	 * @param maxPenCust Max penality customer 
	 * @param maxPenCanc Max penality canc
	 * @param maxPenRem Max penality rem
	 * @param unsuccRounds Unsuccessful rounds
	 * @param maxPrice Max price
	 * @param txNegType Negotiation type (e.g.'A')
	 * @param txStatus Status
	 * @param dtInsDate Insertion date
	 * @param dtValFrom Validity from 
	 * @param dtValTo Validity to
	 * @return The product ID of the product created
	 * @throws Exception
	 */
    public Long createProduct(String txName, String txDesc, String txBrand, String txRel, long party, 
    		BigDecimal maxPenCust, BigDecimal maxPenCanc, BigDecimal maxPenRem, BigDecimal unsuccRounds, 
    		BigDecimal maxPrice, String txNegType, String txStatus, Calendar dtInsDate, Calendar dtValFrom, 
    		Calendar dtValTo) throws Exception {
    	
    	EmSpProducts product = new EmSpProducts();
    	product.setTxProductname(txName);
    	product.setTxProductdesc(txDesc);
    	product.setTxBrand(txBrand);
    	product.setTxRelease(txRel);
    	EmParty emParty = new EmParty();
    	emParty.setNuPartyid(party);
    	product.setEmParty(emParty);
    	product.setNuMaxPenaltiesCust(maxPenCust);
    	product.setNuMaxPenaltyCancel(maxPenCanc);
    	product.setNuMaxPenaltyRemove(maxPenRem);
    	product.setNuUnsuccessRoundsNeg(unsuccRounds);
    	product.setNuMaxPriceDecr(maxPrice);
    	product.setTxNegotiationType(txNegType);
    	product.setTxStatus(txStatus);
    	product.setDtInsertdate(dtInsDate.getTime());
    	product.setDtValidfrom(dtValFrom.getTime());
    	product.setDtValidto(dtValTo.getTime());    	
    	
    	productManager.saveOrUpdate(product);
    	
    	return product.getNuProductid();
    	
    }	
	
	

	/**
	 * Add a new ProductOffer entity in the slasoi DB
	 * @param prodId The product ID related to this offer
	 * @param txName The product offer name
	 * @param txDesc The product offer description
	 * @param dtValFrom Validity from 
	 * @param dtValTo Validity to
	 * @param dtValTo2 
	 * @param txStatus Status
	 * @param billFreq Billing frequency ID 
	 * @param slatId SLA template ID 
	 * @param party 
	 * @return The ProductOffer ID of the offer created
	 * 
	 */
	public Long createProductOffer(long prodId, String txName, String txDesc, Calendar dtInsDate, Calendar dtValFrom, 
		Calendar dtValTo,  String revId, String txStatus, long billFreq, String slatId, long party) 
		throws Exception {

		EmSpProducts p = productManager.getProductById(prodId);
			
		if (p == null)
			throw new Exception ("Product not found");

		EmSpProductsOffer emSpProductsOffer = new EmSpProductsOffer();
		emSpProductsOffer.setDtValidFrom(dtValFrom.getTime());
		emSpProductsOffer.setDtValidTo(dtValTo.getTime());
		emSpProductsOffer.setTxName(txName);
		emSpProductsOffer.setTxDescription(txDesc);
		emSpProductsOffer.setTxRevisionid(revId);
		emSpProductsOffer.setTxStatus(txStatus);
			
		EmBillingFrecuency ebf = new EmBillingFrecuency();
		ebf.setNuBillingFrecuencyId(billFreq);
		emSpProductsOffer.setEmBillingFrecuency(ebf);
			
		emSpProductsOffer.setTxBslatid(slatId);
		emSpProductsOffer.setEmSpProducts(p);

		EmSpServices service= new EmSpServices();
		service.setTxSlatid(slatId);
		service.setTcServicetype("S");
		service.setTxRelease("1");
		service.setTxServicedesc(txDesc);
		service.setTxServicename(txName);
		service.setDtInsertdate(dtInsDate.getTime());
		EmParty emParty = new EmParty();
    	emParty.setNuPartyid(party);
    	service.setEmParty(emParty);

		EmProductOfferSlat pos= new EmProductOfferSlat();
		pos.setEmSpProductsOffer(emSpProductsOffer);
		pos.setEmSpServices(service);

		serviceManager.saveOrUpdate(service);

		Set<EmSpServices> setServices = new HashSet<EmSpServices>();
		setServices.add(service);
		
		emSpProductsOffer.setEmSpServiceses(setServices);
		
		productOfferManager.saveOrUpdate(emSpProductsOffer);
	
		return emSpProductsOffer.getNuIdproductoffering();

	}

	
}
