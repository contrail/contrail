/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.PricedItems;

public class AddOvfDescriptor {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdOvfDescriptorNameOpt = "n";
	private static final String cmdOvfdescriptorSlaTOpt = "s";
	private static final String cmdOvfResourcesIds = "i";
		
	static {
		options = new Options();
		options.addOption(cmdOvfDescriptorNameOpt, true, "OvfDescriptor Name (mandatory)");
		options.addOption(cmdOvfdescriptorSlaTOpt, true, "SlaTemplate id (mandatory)");
		options.addOption(cmdOvfResourcesIds, true, "OvfResource Ids");
	}
	
	
	private static String OvfDescriptorName;
	private static String OvfslaT;
	private static String OvfResourcesIds="-1";	
	private static ArrayList<Long> ovfRIdsArray;
	
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){
		
		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		if (!cmd.hasOption(cmdOvfDescriptorNameOpt) || !cmd.hasOption(cmdOvfdescriptorSlaTOpt)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddOvfDescriptor", options);
			System.exit(1);
		}
		
		OvfDescriptorName = cmd.getOptionValue(cmdOvfDescriptorNameOpt);
		OvfslaT = cmd.getOptionValue(cmdOvfdescriptorSlaTOpt);
		if(cmd.hasOption(cmdOvfResourcesIds))
			OvfResourcesIds = cmd.getOptionValue(cmdOvfResourcesIds);
		StringTokenizer st=new StringTokenizer(OvfResourcesIds,",");
		ovfRIdsArray=new ArrayList<Long>();
		while(st.hasMoreTokens()){
			ovfRIdsArray.add(new Long(st.nextToken()));
		}
		
		System.out.println("\nUsing: \nOvfDescriptor name: " + OvfDescriptorName + 
			"\nOvfResources Ids: " + OvfResourcesIds);

	}
	
	
	/**
	 * Main program 
	 * This program adds a new OvfDescriptor into repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		AddOvfDescriptor inst = new AddOvfDescriptor();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	PricedItems pricedItems = new PricedItems();
    	try {
    		pricedItems.createOvfDescriptor(OvfDescriptorName, OvfslaT, ovfRIdsArray);
    	} catch (Exception e) {
    		System.err.println("Error while adding OvfDescriptor");
    		e.printStackTrace();
    	}
	}

}
