/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.slasoi;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.slasoi.businessManager.common.model.pricing.CompoundResource;
import org.slasoi.businessManager.common.model.pricing.OvfDescriptor;
import org.slasoi.businessManager.common.model.pricing.OvfResource;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.model.pricing.UnitResource;
import org.slasoi.businessManager.common.service.PricedItemManager;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PricedItems {

		
	@Resource
	private PricedItemManager pricedItemManager; 
	
	private ClassPathXmlApplicationContext context = null;
	
	public PricedItems() {
		context = new ClassPathXmlApplicationContext("META-INF/spring/contrail/bm-common-context.xml");
		pricedItemManager = (PricedItemManager)context.getBean("pricedItemService");
	}
	
	public PricedItem getPricedItemByName(String name) {
		return pricedItemManager.getPricedItemByName(name);
	}
	
	public Long createPricedItem(String name, String description, boolean reservation){
		PricedItem pi=new PricedItem();
		pi.setProductName(name);
		pi.setProductDescr(description);
		pi.setReservation(reservation);
		pricedItemManager.saveOrUpdatePricedItem(pi);	
		return pi.getProductId();
	}

	public void deletePricedItem(Long id){
		pricedItemManager.deletePricedItem(id);
	}
	
	public UnitResource getUnitResourceByName(String name) {
		return pricedItemManager.getUnitResourceByName(name);
	}
	
	public Long createUnitResource(String name, String description, boolean reservation, String instanceId, String type, String unit){
		Long id=createPricedItem(name, description, reservation);
		PricedItem pi=pricedItemManager.getPricedItemById(id);
		UnitResource ur=new UnitResource();
		ur.setPricedItem(pi);
		ur.setUnitResourceType(type);
		ur.setUnitResourceIstanceId(instanceId);
		ur.setUnitResourceUnit(unit);
		pi.setUnitResource(ur);
		pricedItemManager.saveOrUpdatePricedItem(pi);
		pricedItemManager.saveOrUpdateUnitResource(ur);
		return pi.getProductId();
	}
	
	public void deleteUnitResource(Long id){
		pricedItemManager.deleteUnitResource(id);
	}
	
	public void createCompoundResource(String name, String description, boolean reservation, ArrayList<Long> unitResourceIds, ArrayList<Long> compoundResourceIds){
		LinkedHashSet<UnitResource> unitResources=new LinkedHashSet<UnitResource>();
		for(Long urL: unitResourceIds){
			UnitResource ur=pricedItemManager.getUnitResourceById(urL);
			if(ur!=null)
				unitResources.add(ur);
		}
		LinkedHashSet<CompoundResource> compoundResources=new LinkedHashSet<CompoundResource>();
		for(Long crL: compoundResourceIds){
			CompoundResource cr=pricedItemManager.getCompoundResourceById(crL);
			if(cr!=null)
				compoundResources.add(cr);
		}
		pricedItemManager.createCompoundResource(name, description, reservation, unitResources, compoundResources);
	}
	
	public void deleteCompoundResource(Long id){
		pricedItemManager.deleteCompoundResource(id);
	}
	
	public void createOvfResource(String name, String description, boolean reservation, String id, String instanceId, String type, String info, ArrayList<Long> unitResourceIds, ArrayList<Long> compoundResourceIds){
		LinkedHashSet<UnitResource> unitResources=new LinkedHashSet<UnitResource>();
		for(Long urL: unitResourceIds){
			UnitResource ur=pricedItemManager.getUnitResourceById(urL);
			if(ur!=null)
				unitResources.add(ur);
		}
		LinkedHashSet<CompoundResource> compoundResources=new LinkedHashSet<CompoundResource>();
		for(Long crL: compoundResourceIds){
			CompoundResource cr=pricedItemManager.getCompoundResourceById(crL);
			if(cr!=null)
				compoundResources.add(cr);
		}
		pricedItemManager.createOvfResource(name, description, reservation, id, instanceId, type, info, unitResources, compoundResources);
	}
	
	public void deleteOvfResource(Long id){
		pricedItemManager.deleteOvfResource(id);
	}
	
	public void createOvfDescriptor(String name, String slaTemplateId, ArrayList<Long> ovfResourceIds){
		LinkedHashSet<OvfResource> ovfResources=new LinkedHashSet<OvfResource>();
		for(Long l: ovfResourceIds){
			OvfResource or=pricedItemManager.getOvfResourceById(l);
			if(or!=null)
				ovfResources.add(or);
		}
		pricedItemManager.createOvfDescriptor(name, slaTemplateId, ovfResources);
	}
	
	public void deleteOvfDescriptor(Long id){
		pricedItemManager.deleteOvfDescriptor(id);
	}
}
