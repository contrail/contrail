/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.slasoi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.codec.binary.Base64;


public final class Base64Utils {


    /**
     * encode an Object into a Base64 String
     * @param obj object
     * @return result string
     */
    public static String encode(final Object obj) {
        String result = null;
        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
            os.writeObject(obj);
            result = new String(Base64.encodeBase64(bytesOutput.toByteArray()));
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    
    
    /**
     * decode a Base64 String
     * @param response Base64 string
     * @return result object
     */
    public static Object decode(final String response) {
        Object result = null;
        try {
            ByteArrayInputStream bytesInput = new ByteArrayInputStream(Base64
                    .decodeBase64(response.getBytes()));
            ObjectInputStream is = new ObjectInputStream(bytesInput);
            result = is.readObject();
            is.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
