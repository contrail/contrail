/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.Prices;

public class DeleteCompoundFee {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdCFIdOpt = "id";
		
	static {
		options = new Options();
		options.addOption(cmdCFIdOpt, true, "Compound Fee Id (mandatory)");
	}
	
	
	private static String compoundFeeId;	
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){
		
		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		if (!cmd.hasOption(cmdCFIdOpt)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("CompoundFee", options);
			System.exit(1);
		}
		
		compoundFeeId = cmd.getOptionValue(cmdCFIdOpt);

		
		System.out.println("\nUsing: \nCompound Fee id: " + compoundFeeId);

	}
	
	
	/**
	 * Main program 
	 * This program delete a Compound Fee from repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		DeleteCompoundFee inst = new DeleteCompoundFee();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	Prices prices = new Prices();
    	try {
    		prices.deleteCompoundFee(new Long(compoundFeeId));
    	} catch (Exception e) {
    		System.err.println("Error while deleting Compound Fee");
    		e.printStackTrace();
    	}
	}

}
