/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.slasoi;

import java.math.BigDecimal;
import java.util.Set;

import javax.annotation.Resource;

import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmPartyPartyrole;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.common.service.PartyPartyRoleManager;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Parties {
	
	@Resource
	private PartyManager partyManager; 

	@Resource
	private PartyPartyRoleManager partyPartyRoleManager ; 

	private ClassPathXmlApplicationContext context = null;
	

	public Parties() {
		context = new ClassPathXmlApplicationContext("META-INF/spring/contrail/bm-common-context.xml");
		partyManager = (PartyManager) context.getBean("partyService");
		partyPartyRoleManager = (PartyPartyRoleManager) context.getBean("partyPartyRoleService");
	}

		

    /**
     * set status and balance of a new customer
     * @param partyId customer ID
     * @param status account status
     * @param balance account balance
     * @param creditLimit account credit
     * @return true if successful
     * @throws Exception
     */
    public Boolean setParty(Long partyId, String status, 
    	BigDecimal balance, BigDecimal creditLimit) 
    		throws Exception {
    	
    	EmParty party = partyManager.getPartyById(partyId);
    	
    	if (null == party)
    		return false;
    	
    	Set<EmPartyPartyrole> roles = party.getEmPartyPartyroles();
    	for (EmPartyPartyrole role : roles) {
    		role.setTxStatus(status);
    		partyPartyRoleManager.saveOrUpdate(role);
    	}
    	
    	party.setNuBalance(balance);
    	party.setNuCreditLimit(creditLimit);
    	party.setEmPartyPartyroles(roles);
    	partyManager.saveOrUpdate(party);
    	
    	return true;
    }	
	
	
	
}
