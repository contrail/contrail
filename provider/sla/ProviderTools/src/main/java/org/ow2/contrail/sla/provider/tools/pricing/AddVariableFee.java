/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.StringTokenizer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.Prices;

public class AddVariableFee {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdValue = "v";
	private static final String cmdCurrency = "c";
	private static final String cmdPeriod = "p";
	private static final String cmdDateFrom = "f";
	private static final String cmdDateUntil = "u";
	private static final String cmdQuantity = "q";
	private static final String cmdType = "t";
	private static final String cmdDataType = "d";
	private static final String cmdCondition = "cond";
	private static final String cmdConditionValue = "condv";
	private static final String cmdCondDataType = "condd";
		
	static {
		options = new Options();
		options.addOption(cmdValue, true, "Variable Fee Value (mandatory)");
		options.addOption(cmdCurrency, true, "Variable Fee Currency [ EUR (default) ]");
		options.addOption(cmdPeriod, true, "Variable Fee Period (mandatory)");
		options.addOption(cmdDateFrom, true, "Variable Fee Validity From (mandatory) [dd/mm/yyy]");
		options.addOption(cmdDateUntil, true, "Variable Fee Validity Until (mandatory) [dd/mm/yyy]");
		options.addOption(cmdQuantity, true, "Variable Fee Quantity (mandatory)");
		options.addOption(cmdType, true, "Variable Fee Type (mandatory)");
		options.addOption(cmdDataType, true, "Variable Fee Datatype (mandatory)");
		options.addOption(cmdCondition, true, "Variable Fee Condition (mandatory)");
		options.addOption(cmdConditionValue, true, "Variable Fee Condition Value (mandatory) ]");
		options.addOption(cmdCondDataType, true, "Variable Fee Condition Datatype (mandatory) ");
	}
	
	
	private static String variableFeeValue;
	private static String variableFeeCurrency="EUR";
	private static String variableFeePeriod;
	private static String variableFeeDateFrom;
	private static String variableFeeDateUntil;
	private static String variableFeeType;
	private static String variableFeeQuantity;
	private static String variableFeeDataType;
	private static Calendar from=Calendar.getInstance();
	private static Calendar until=Calendar.getInstance();
	private static String variableFeeCondition;
	private static String variableFeeConditionValue;
	private static String variableFeeConditionDataType;
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){
		
		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		if (!cmd.hasOption(cmdValue) || !cmd.hasOption(cmdPeriod)
				|| !cmd.hasOption(cmdDateFrom) || !cmd.hasOption(cmdDateUntil) || !cmd.hasOption(cmdQuantity)
				|| !cmd.hasOption(cmdType) || !cmd.hasOption(cmdDataType) || !cmd.hasOption(cmdCondition) 
				|| !cmd.hasOption(cmdConditionValue)  || !cmd.hasOption(cmdCondDataType)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddVariableFee", options);
			System.exit(1);
		}
		
		variableFeeValue = cmd.getOptionValue(cmdValue);
		if(cmd.getOptionValue(cmdCurrency) != null)
			variableFeeCurrency=cmd.getOptionValue(cmdCurrency);
		variableFeePeriod = cmd.getOptionValue(cmdPeriod);
		variableFeeDateFrom = cmd.getOptionValue(cmdDateFrom);
		setCalendar(variableFeeDateFrom, from);
		variableFeeDateUntil = cmd.getOptionValue(cmdDateUntil);
		setCalendar(variableFeeDateUntil, until);
		variableFeeQuantity= cmd.getOptionValue(cmdQuantity);
		variableFeeType= cmd.getOptionValue(cmdType);
		variableFeeDataType = cmd.getOptionValue(cmdDataType);
		variableFeeCondition = cmd.getOptionValue(cmdCondition);
		variableFeeConditionValue = cmd.getOptionValue(cmdConditionValue);
		variableFeeConditionDataType = cmd.getOptionValue(cmdCondDataType);
		
		System.out.println("\nUsing: \nVariable Fee Value: " + variableFeeValue + 
			"\nVariable Fee Currency: " + variableFeeCurrency + "\nVariable Fee Period: " + variableFeePeriod +
			"\nVariable Fee Date From: " + variableFeeDateFrom + "\nVariable Fee Date Until: " + variableFeeDateUntil +
			"\nVariable Fee Quantity: " + variableFeeQuantity + "\nVariable Fee Type: " + variableFeeType + "\nVariable Fee DataType: " + variableFeeDataType +
			"\nVariable Fee Condition: " + variableFeeCondition + "\nVariable Fee Condition Value: " + variableFeeConditionValue +
			"\nVariable Fee Condition Datatype: " + variableFeeConditionDataType);

	}
	
	
	private void setCalendar(String time, Calendar c){
		int day = 0,month = 0,year=0;
		c.clear();
		StringTokenizer st=new StringTokenizer(time,"/");
		if(st.hasMoreElements())
			day=new java.lang.Integer(st.nextToken()).intValue();
		if(st.hasMoreElements())
			month=new java.lang.Integer(st.nextToken()).intValue();
		if(st.hasMoreElements())
			year=new java.lang.Integer(st.nextToken()).intValue();
		c.set(year, month-1, day);
	}
	
	/**
	 * Main program 
	 * This program adds a new variable Fee into repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		AddVariableFee inst= new AddVariableFee();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	Prices prices=new Prices();
    	try {
    		prices.createVariableFee(new BigDecimal(variableFeeValue), variableFeeCurrency, variableFeePeriod, from.getTime(), until.getTime(), variableFeeType, new Integer(variableFeeQuantity).intValue(), variableFeeDataType, variableFeeCondition, variableFeeConditionValue, variableFeeConditionDataType);
    	} catch (Exception e) {
    		System.err.println("Error while adding Variable Fee");
    		e.printStackTrace();
    	}
	}

}
