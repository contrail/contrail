/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.slasoi.context.mockup;

import java.util.Hashtable;

import org.slasoi.gslam.core.authorization.Authorization;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.ProtocolEngine;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.ServiceManagerRegistry;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;


public class SLAManagerContextMockup implements SLAManagerContext{
  

    protected SLARegistry slaRegistry;
    
    public static class SLAManagerContextException extends Exception {
        public SLAManagerContextException(String cause) {
            super(cause);
        }
    }

    public Authorization getAuthorization()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getEPR() throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getGroupID() throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public IMonitoringManager getMonitorManager()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public PlanningOptimization getPlanningOptimization()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public ProtocolEngine getProtocolEngine()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public ProvisioningAdjustment getProvisioningAdjustment()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getSLAManagerID() throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLARegistry getSLARegistry()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        return this.slaRegistry;
    }

    public SLATemplateRegistry getSLATemplateRegistry()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        return null;
    }

    public ServiceAdvertisement getServiceAdvertisement()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public ServiceManagerRegistry getServiceManagerRegistry()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public GenericSLAManagerUtils getSlamUtils() {
        // TODO Auto-generated method stub
        return null;
    }

    public Hashtable<SyntaxConverterType, ISyntaxConverter> getSyntaxConverters()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getWSPrefix() throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public void setAuthorization(Authorization arg0) {
        // TODO Auto-generated method stub
    }

    public void setGroupID(String arg0) {
        // TODO Auto-generated method stub
    }

    public void setPlanningOptimization(PlanningOptimization arg0) {
        // TODO Auto-generated method stub
    }

    public void setProvisioningAdjustment(ProvisioningAdjustment arg0) {
        // TODO Auto-generated method stub
    }

    public void setSlamUtils(GenericSLAManagerUtils arg0) {
        // TODO Auto-generated method stub
    }

    public SLARegistry getSlaRegistry() {
        return slaRegistry;
    }

    public void setSlaRegistry(SLARegistry slaRegistry) {
        this.slaRegistry = slaRegistry;
    }

    public Hashtable<String, String> getProperties()
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
        return null;
    }

    public void setProperties(Hashtable<String, String> arg0)
            throws org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException {
        // TODO Auto-generated method stub
    }

}
