/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.Guarantees;
import org.ow2.contrail.sla.provider.slasoi.PricedItems;

public class AddGuarantee {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdGuaranteeNameOpt = "n";
	private static final String cmdGuaranteeDescrOpt = "d";
	private static final String cmdGuaranteeTypeOpt = "t";
	private static final String cmdGuaranteeOpOpt = "op";
	private static final String cmdGuaranteeValueOpt = "v";
	private static final String cmdGuaranteeValueTypeOpt = "vt";
	private static final String cmdGuaranteeTemplateIdOpt = "s";
	private static final String cmdGuaranteeAgreementTermOpt = "a";
	private static final String cmdPercPriceVariationOpt = "p";
		
	static {
		options = new Options();
		options.addOption(cmdGuaranteeNameOpt, true, "Guarantee Name (mandatory)");
		options.addOption(cmdGuaranteeDescrOpt, true, "Guarantee Description (mandatory)");
		options.addOption(cmdGuaranteeTypeOpt, true, "Guarantee Type (mandatory) [location, reserve, co_location_rack, etc.]");
		options.addOption(cmdGuaranteeOpOpt, true, "Guarantee Operator");
		options.addOption(cmdGuaranteeValueOpt, true, "Guarantee  Value");
		options.addOption(cmdGuaranteeValueTypeOpt, true, "Guarantee ValueType");
		options.addOption(cmdGuaranteeTemplateIdOpt, true, "Guarantee slaTemplateId");
		options.addOption(cmdGuaranteeAgreementTermOpt, true, "Guarantee AgreementTerm");
		options.addOption(cmdPercPriceVariationOpt, true, "Guarantee Percentage Price Variation (mandatory) [ (0:1] ]");
	}
	
	
	private static String guaranteeName;
	private static String guaranteeDescr;
	private static String guaranteeType;
	private static String guaranteeOp;
	private static String guaranteeValue;
	private static String guaranteeValueType;
	private static String guaranteeTemplateId;
	private static String guaranteeAgreementTerm;
	private static String percPriceVariation;	
	
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){
		
		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		if (!cmd.hasOption(cmdGuaranteeNameOpt) || !cmd.hasOption(cmdGuaranteeDescrOpt) || !cmd.hasOption(cmdGuaranteeAgreementTermOpt)
			|| !cmd.hasOption(cmdGuaranteeTypeOpt) || !cmd.hasOption(cmdPercPriceVariationOpt)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddGuarantee", options);
			System.exit(1);
		}
		
		guaranteeName = cmd.getOptionValue(cmdGuaranteeNameOpt);
		guaranteeDescr = cmd.getOptionValue(cmdGuaranteeDescrOpt);
		guaranteeType = cmd.getOptionValue(cmdGuaranteeTypeOpt);
		guaranteeOp = cmd.getOptionValue(cmdGuaranteeOpOpt);
		guaranteeValue = cmd.getOptionValue(cmdGuaranteeValueOpt);
		guaranteeValueType = cmd.getOptionValue(cmdGuaranteeValueTypeOpt);
		guaranteeTemplateId=cmd.getOptionValue(cmdGuaranteeTemplateIdOpt);
		guaranteeAgreementTerm=cmd.getOptionValue(cmdGuaranteeAgreementTermOpt);
		percPriceVariation = cmd.getOptionValue(cmdPercPriceVariationOpt);
		
		System.out.println("\nUsing: \nGuarantee name: " + guaranteeName + 
			"\nGuarantee description: " + guaranteeDescr + "\nGuarantee type: " + guaranteeType +
			"\nGuarantee operator: " + guaranteeOp + "\nGuarantee value: " + guaranteeValue + "\nGuarantee valueType: " + guaranteeValueType +
			"\nGuarantee slaTemplateId: " + guaranteeTemplateId + "\nGuarantee AgreementTerm: " + guaranteeAgreementTerm +
			"\nPercentage Price Variation: " + percPriceVariation);

	}
	
	
	/**
	 * Main program 
	 * This program adds a new Guarantee Resource into repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		AddGuarantee inst = new AddGuarantee();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	Guarantees guarantees=new Guarantees();
    	try {
    		guarantees.createGuarantee(guaranteeName, guaranteeTemplateId, guaranteeAgreementTerm, guaranteeDescr, guaranteeType, guaranteeOp, guaranteeValue, guaranteeValueType, new Double(percPriceVariation).doubleValue());
    	} catch (Exception e) {
    		System.err.println("Error while adding Guarantee");
    		e.printStackTrace();
    	}
	}

}
