/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.StringTokenizer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.Prices;

public class AddFixedFee {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdValue = "v";
	private static final String cmdCurrency = "c";
	private static final String cmdPeriod = "p";
	private static final String cmdDateFrom = "f";
	private static final String cmdDateUntil = "u";
	private static final String cmdType = "t";
	private static final String cmdQuantity = "q";
	private static final String cmdDataType = "d";
		
	static {
		options = new Options();
		options.addOption(cmdValue, true, "Fixed Fee Value (mandatory)");
		options.addOption(cmdCurrency, true, "Fixed Fee Currency [ EUR (default) ]");
		options.addOption(cmdPeriod, true, "Fixed Fee Period (mandatory) [one_time_charge, per_hour, per_day, etc...]");
		options.addOption(cmdDateFrom, true, "Fixed Fee Validity From (mandatory) [dd/mm/yyy]");
		options.addOption(cmdDateUntil, true, "Fixed Fee Validity Until (mandatory) [dd/mm/yyy]");
		options.addOption(cmdType, true, "Fixed Fee Type (mandatory) [one_time_charge, per_hour, per_day, etc...]");
		options.addOption(cmdQuantity, true, "Fixed Fee Quantity [ 1 (default) ]");
		options.addOption(cmdDataType, true, "Fixed Fee Datatype [ vm (default), integer, MHz, MB ]");
	}
	
	
	private static String fixedFeeValue;
	private static String fixedFeeCurrency="EUR";
	private static String fixedFeePeriod;
	private static String fixedFeeDateFrom;
	private static String fixedFeeDateUntil;
	private static String fixedFeeType;
	private static String fixedFeeQuantity="1";
	private static String fixedfeeDataType="vm";
	private static Calendar from=Calendar.getInstance();;
	private static Calendar until=Calendar.getInstance();;
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){
		
		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		if (!cmd.hasOption(cmdValue) || !cmd.hasOption(cmdPeriod)
				|| !cmd.hasOption(cmdDateFrom) || !cmd.hasOption(cmdDateUntil)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddFixedFee", options);
			System.exit(1);
		}
		
		fixedFeeValue = cmd.getOptionValue(cmdValue);
		if(cmd.getOptionValue(cmdCurrency) != null)
			fixedFeeCurrency=cmd.getOptionValue(cmdCurrency);
		fixedFeePeriod = cmd.getOptionValue(cmdPeriod);
		fixedFeeDateFrom = cmd.getOptionValue(cmdDateFrom);
		setCalendar(fixedFeeDateFrom, from);
		fixedFeeDateUntil = cmd.getOptionValue(cmdDateUntil);
		setCalendar(fixedFeeDateUntil, until);
		fixedFeeType = cmd.getOptionValue(cmdType);
		if(cmd.getOptionValue(cmdQuantity) != null)
			fixedFeeQuantity=cmd.getOptionValue(cmdQuantity);
		if(cmd.getOptionValue(cmdDataType) != null)
			fixedfeeDataType=cmd.getOptionValue(cmdDataType);
		
		
		System.out.println("\nUsing: \nFixed Fee Value: " + fixedFeeValue + 
			"\nFixed Fee Cucrrency: " + fixedFeeCurrency + "\nFixed Fee Period: " + fixedFeePeriod +
			"\nFixed Fee Date From: " + fixedFeeDateFrom + "\nFixed Fee Date Until: " + fixedFeeDateUntil +
			"\nFixed Fee Type: " + fixedFeeType + "\nFixed Fee Quantity: " + fixedFeeQuantity + "\nFixed Fee DataType: " + fixedfeeDataType);

	}
	
	
	private void setCalendar(String time, Calendar c){
		int day = 0,month = 0,year=0;
		c.clear();
		StringTokenizer st=new StringTokenizer(time,"/");
		if(st.hasMoreElements())
			day=new java.lang.Integer(st.nextToken()).intValue();
		if(st.hasMoreElements())
			month=new java.lang.Integer(st.nextToken()).intValue();
		if(st.hasMoreElements())
			year=new java.lang.Integer(st.nextToken()).intValue();
		c.set(year, month-1, day);
	}
	
	/**
	 * Main program 
	 * This program adds a new Fixed Fee into repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		AddFixedFee inst= new AddFixedFee();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	Prices prices=new Prices();
    	try {
    		prices.createFixedFee(new BigDecimal(fixedFeeValue), fixedFeeCurrency, fixedFeePeriod, from.getTime(), until.getTime(), fixedFeeType, new Integer(fixedFeeQuantity).intValue(), fixedfeeDataType);
    	} catch (Exception e) {
    		System.err.println("Error while adding Fixed Fee");
    		e.printStackTrace();
    	}
	}

}
