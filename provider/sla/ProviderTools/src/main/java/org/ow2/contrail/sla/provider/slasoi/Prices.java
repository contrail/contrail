/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.slasoi;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.slasoi.businessManager.common.model.pricing.FixedFee;
import org.slasoi.businessManager.common.model.pricing.UnitResource;
import org.slasoi.businessManager.common.model.pricing.VariableFee;
import org.slasoi.businessManager.common.service.PriceManager;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Prices {
	
	@Resource
	private PriceManager priceManager;
	
	private ClassPathXmlApplicationContext context = null;
	
	public Prices() {
		context = new ClassPathXmlApplicationContext("META-INF/spring/contrail/bm-common-context.xml");
		priceManager = (PriceManager)context.getBean("priceService");
	}
	
	
	public void deletePrice(Long id){
		priceManager.deletePrice(id);
	}
	
	public void deleteFixedFee(Long id){
		priceManager.deleteFixedFee(id);
	}
	
	public void deleteVariableFee(Long id){
		priceManager.deleteVariableFee(id);
	}
	
	public void deleteCompoundFee(Long id){
		priceManager.deleteCompoundFee(id);
	}
	
	public void associatePrice(Long pricedId, Long priceId){
		priceManager.associatePrice(pricedId, priceId);
	}
	
	public void associatePriceToGuarantee(Long guaranteeId, Long priceId){
		priceManager.associatePriceToGuarantee(guaranteeId, priceId);
	}
	
	public Long createCompoundFee(String currencyType, String period, Date validFrom, Date validUntil, ArrayList<Long> variableFeeIds, ArrayList<Long> fixedFeeIds) {
		LinkedHashSet<FixedFee> fixedFees = new LinkedHashSet<FixedFee>();
		LinkedHashSet<VariableFee> variableFees = new LinkedHashSet<VariableFee>();
		if (fixedFeeIds != null && fixedFeeIds.size() != 0) {
			for (Long fl : fixedFeeIds) {
				FixedFee ff = priceManager.getFixedFeeById(fl);
				fixedFees.add(ff);
			}
		}
		if (variableFeeIds != null && variableFeeIds.size() != 0) {
			for (Long vl : variableFeeIds) {
				VariableFee vf = priceManager.getVariableFeeById(vl);
				variableFees.add(vf);
			}
		}
		return priceManager.createCompoundFee(currencyType, period, validFrom, validUntil, variableFees, fixedFees);
	}
	
	public Long createVariableFee(BigDecimal value, String currencyType, String period, Date validFrom, Date validUntil, String type, int quantity, String datatype, String cond, String condValue,
			String condDataType) {
		return priceManager.createVariableFee(value, currencyType, period, type, validFrom, validUntil, quantity, datatype, cond, condValue, condDataType);
	}
	
	public Long createFixedFee(BigDecimal value, String currencyType, String period, Date validFrom, Date validUntil, String type, int quantity, String dataType) {
		return priceManager.createFixedFee(value, currencyType, period, type, validFrom, validUntil, quantity, dataType);
	}

}
