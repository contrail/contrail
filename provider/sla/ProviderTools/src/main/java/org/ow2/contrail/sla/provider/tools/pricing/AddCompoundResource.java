/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.PricedItems;

public class AddCompoundResource {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdCRNameOpt = "n";
	private static final String cmdCRDescrOpt = "d";
	private static final String cmdCRReservation = "r";
	private static final String cmdCROwnedIds = "c";
	private static final String cmdUnitResIds = "URs";
		
	static {
		options = new Options();
		options.addOption(cmdCRNameOpt, true, "Compound Resource Name (mandatory)");
		options.addOption(cmdCRDescrOpt, true, "Compound Resource Description (mandatory)");
		options.addOption(cmdCRReservation, true, "Compound Resource Reservation [yes or no] (default=no)");
		options.addOption(cmdCROwnedIds, true, "Compound Resource Owned Id [id1,id2,...]");
		options.addOption(cmdUnitResIds, true, "Comma separated Ids of UnitResource [ id1,id2,... ]");
	}
	
	
	private static String CRResourceName;
	private static String CRResourceDescr;
	private static boolean CRReservation=false;
	private static String CROwnedIds;
	private static String ursIds;
	private static ArrayList<Long> ursIdsArray;
	private static ArrayList<Long> crsIdsArray;
	
	
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){
		
		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		if (!cmd.hasOption(cmdCRNameOpt) || !cmd.hasOption(cmdCRDescrOpt)
			 || !cmd.hasOption(cmdUnitResIds)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddCRResource", options);
			System.exit(1);
		}
		
		CRResourceName = cmd.getOptionValue(cmdCRNameOpt);
		CRResourceDescr = cmd.getOptionValue(cmdCRDescrOpt);
		
		crsIdsArray=new ArrayList<Long>();
		if(cmd.hasOption(cmdCROwnedIds)){
			CROwnedIds = cmd.getOptionValue(cmdCROwnedIds);		
			StringTokenizer st=new StringTokenizer(CROwnedIds,",");
			while(st.hasMoreTokens()){
				crsIdsArray.add(new Long(st.nextToken()));
			}
		}
		if(cmd.hasOption(cmdCRReservation)){
			if(cmd.getOptionValue(cmdCRReservation).equalsIgnoreCase("yes"))
				CRReservation=true;
		}
		
		ursIds = cmd.getOptionValue(cmdUnitResIds);
		StringTokenizer st2=new StringTokenizer(ursIds,",");
		ursIdsArray=new ArrayList<Long>();
		while(st2.hasMoreTokens()){
			ursIdsArray.add(new Long(st2.nextToken()));
		}
/*		ursIds = cmd.hasOption(cmdUnitResIds) ? 
			ContrailSlaManagerContext.getSlamDbName(cmd.getOptionValue(cmdSlamId)) : 
			ContrailSlaManagerContext.getSlamDbName(null);*/
		
		System.out.println("\nUsing: \nCompound Resource name: " + CRResourceName + 
			"\nCompound Resource description: " + CRResourceDescr + "\nCompoundResource reservation: " + CRReservation + "\nUnit Resource ids: " + ursIds + "\nCompound Resource Owned ids: " + CROwnedIds);

	}
	
	
	/**
	 * Main program 
	 * This program adds a new CR Resource into repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		AddCompoundResource inst = new AddCompoundResource();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	PricedItems pricedItems = new PricedItems();
    	try {
    		pricedItems.createCompoundResource(CRResourceName, CRResourceDescr, CRReservation, ursIdsArray, crsIdsArray);
    	} catch (Exception e) {
    		System.err.println("Error while adding CR Resource");
    		e.printStackTrace();
    	}
	}

}
