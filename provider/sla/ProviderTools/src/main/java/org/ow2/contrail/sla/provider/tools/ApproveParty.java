/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.tools;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.commons.cli.*;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.Parties;


public class ApproveParty {

	private CommandLine cmd;

	private static Options options = null;

	private static final String cmdParty = "p";
	private static final String cmdBalance = "b";
	private static final String cmdCreditLimit = "cl";

	static {
		options = new Options();
		options.addOption(cmdParty, true, "Party (mandatory)");
		options.addOption(cmdBalance, true, "Balance");
		options.addOption(cmdCreditLimit, true, "Credit Limit");
	}

	private static long party;
	private static BigDecimal balance;
	private static BigDecimal creditLimit;

	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args) {

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (!cmd.hasOption(cmdParty) || !cmd.hasOption(cmdBalance)
				|| !cmd.hasOption(cmdCreditLimit)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("ApproveParty", options);
			System.exit(1);
		}

		party = Long.parseLong(cmd.getOptionValue(cmdParty));
		balance = new BigDecimal(cmd.getOptionValue(cmdBalance));
		creditLimit = new BigDecimal(cmd.getOptionValue(cmdCreditLimit));

		System.out.println("\nUsing: \nParty: " + party + "\nBalance: "
				+ balance + "\nCreditLimit: " + creditLimit);

	}

	
	/**
	 * Main program 
	 * This program approves an existing customer created in SLA@SOI.
	 * The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */	
	public static void main(String[] args) throws XmlException, IOException {

		System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));

		ApproveParty inst = new ApproveParty();
		inst.loadArgs(args);

		Parties parties = new Parties();
		try {
			parties.setParty(party, "APPROVED", balance, creditLimit);
		} catch (Exception e) {
			System.err.println("Error while setting party");
			e.printStackTrace();
		}

	}

}
