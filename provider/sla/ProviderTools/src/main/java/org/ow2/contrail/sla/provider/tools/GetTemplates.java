/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.tools;

import java.io.IOException;

import org.apache.xmlbeans.XmlException;
import org.apache.commons.cli.*;

import org.ow2.contrail.sla.provider.slasoi.Templates;
import org.slasoi.slamodel.sla.SLATemplate;


public class GetTemplates {

	private CommandLine cmd; 

	private static String host = "localhost";
	private static String port = "8080";
	private static String productId = "0";
	private static String slamId;

	private static Options options = null;
	private static final String cmdProd = "p";
	private static final String cmdHost = "host";
	private static final String cmdPort = "port";
	private static final String cmdSlamId = "slam";

	static{
		options = new Options();
		options.addOption(cmdProd, true, "Product ID (mandatory)");
		options.addOption(cmdHost, true, "SLA@SOI host (default: localhost)");
		options.addOption(cmdPort, true, "SLA@SOI port (default: 8080)");
		options.addOption(cmdSlamId, true, "SLA@SOI SLAM DB (default: BSLAM)");
	}



	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (!cmd.hasOption(cmdProd)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("GetTemplates", options);
			System.exit(1);
		}

		if (cmd.hasOption(cmdHost))
			host = cmd.getOptionValue(cmdHost);

		if (cmd.hasOption(cmdPort))
			port = cmd.getOptionValue(cmdPort);
		
		
		slamId = cmd.hasOption(cmdSlamId) ? cmd.getOptionValue(cmdSlamId) : null;

		productId = cmd.getOptionValue(cmdProd);

		System.out.println("\nUsing: \nProduct ID: " + productId + 
				"\nHost: " + host + "\nPort: " + port  + "\nSLAM DB: " + slamId);

	}	




	/**
	 * Main program 
	 * This program gets a SLA template fromt the business SLA template repository
	 * of SLA@SOI, given the product ID as input. The program connects to the web services
	 * running on the host and port specified (by default this is localhost:8080) 
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {

		GetTemplates inst = new GetTemplates();

		inst.loadArgs(args);

		Templates sstempls = new Templates(slamId);
		try {
			SLATemplate[] slats = sstempls.getTemplates(Integer.parseInt(productId), host , port);
			int i = 0;
			if (slats == null || slats.length == 0) {
				System.out.println("No SLA templates associated to product ID=" + productId);
				return;
			}
			
			for (SLATemplate slat : slats) {
				System.out.println("\n\n___________________________________________");
				System.out.println("-----> SLA Template (" + ++i + " of " + slats.length + ")");
				System.out.println("ID: " + slat.getUuid().getValue());
				System.out.println("Description: " + slat.getDescr());
				System.out.println("SLA Template: " + slat.toString());
			}
		} catch (Exception e) {
			System.err.println("Error getting SLA templates:");
			e.printStackTrace();
		}

	}



}
