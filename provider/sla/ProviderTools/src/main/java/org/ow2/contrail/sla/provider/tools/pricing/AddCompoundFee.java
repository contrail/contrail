/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.Prices;

public class AddCompoundFee {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdCurrency = "c";
	private static final String cmdPeriod = "p";
	private static final String cmdDateFrom = "f";
	private static final String cmdDateUntil = "u";
	private static final String cmdFixFeesIds = "ff";
	private static final String cmdVarFeesIds = "vf";
		
	static {
		options = new Options();
		options.addOption(cmdCurrency, true, "Compound Fee Currency [ EUR (default) ]");
		options.addOption(cmdPeriod, true, "Compound Fee Period (mandatory)");
		options.addOption(cmdDateFrom, true, "Compound Fee Validity From (mandatory) [dd/mm/yyy]");
		options.addOption(cmdDateUntil, true, "Compound Fee Validity Until (mandatory) [dd/mm/yyy]");
		options.addOption(cmdFixFeesIds, true, "Fixed Fees Ids  [id1,id2,...]");
		options.addOption(cmdVarFeesIds, true, "Variable Fees Ids [id1,id2,...]");
	}
	
	
	private static String compoundFeeCurrency="EUR";
	private static String compoundFeePeriod;
	private static String compoundFeeDateFrom;
	private static String compoundFeeDateUntil;
	private static String fixFeesIds;
	private static String varFeesIds;
	private static ArrayList<Long> ffsIdsArray;
	private static ArrayList<Long> vrsIdsArray;
	private static Calendar from=Calendar.getInstance();
	private static Calendar until=Calendar.getInstance();
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args) {

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (!cmd.hasOption(cmdPeriod) || !cmd.hasOption(cmdDateFrom) || !cmd.hasOption(cmdDateUntil)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddCompoundFee", options);
			System.exit(1);
		}

		if (cmd.getOptionValue(cmdCurrency) != null)
			compoundFeeCurrency = cmd.getOptionValue(cmdCurrency);
		compoundFeePeriod = cmd.getOptionValue(cmdPeriod);
		compoundFeeDateFrom = cmd.getOptionValue(cmdDateFrom);
		setCalendar(compoundFeeDateFrom, from);
		compoundFeeDateUntil = cmd.getOptionValue(cmdDateUntil);
		setCalendar(compoundFeeDateUntil, until);
		fixFeesIds = cmd.getOptionValue(cmdFixFeesIds);
		if (fixFeesIds != null) {
			StringTokenizer st = new StringTokenizer(fixFeesIds, ",");
			ffsIdsArray = new ArrayList<Long>();
			while (st.hasMoreTokens()) {
				ffsIdsArray.add(new Long(st.nextToken()));
			}
		}
		varFeesIds = cmd.getOptionValue(cmdVarFeesIds);
		if (varFeesIds != null) {
			StringTokenizer st2 = new StringTokenizer(varFeesIds, ",");
			vrsIdsArray = new ArrayList<Long>();
			while (st2.hasMoreTokens()) {
				vrsIdsArray.add(new Long(st2.nextToken()));
			}
		}
		System.out.println("\nUsing: \nCompound Fee Currency: " + compoundFeeCurrency + "\nCompound Fee Period: " + compoundFeePeriod + "\nCompound Fee Date From: " + compoundFeeDateFrom
				+ "\nCompound Fee Date Until: " + compoundFeeDateUntil + "\nFixed Fees Ids: " + fixFeesIds + "\nVariable Fees Ids: " + varFeesIds);

	}
	
	
	private void setCalendar(String time, Calendar c){
		int day = 0,month = 0,year=0;
		c.clear();
		StringTokenizer st=new StringTokenizer(time,"/");
		if(st.hasMoreElements())
			day=new java.lang.Integer(st.nextToken()).intValue();
		if(st.hasMoreElements())
			month=new java.lang.Integer(st.nextToken()).intValue();
		if(st.hasMoreElements())
			year=new java.lang.Integer(st.nextToken()).intValue();
		c.set(year, month-1, day);
	}
	
	/**
	 * Main program 
	 * This program adds a new Compound Fee into repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		AddCompoundFee inst= new AddCompoundFee();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	Prices prices=new Prices();
    	try {
    		prices.createCompoundFee(compoundFeeCurrency, compoundFeePeriod, from.getTime(), until.getTime(), vrsIdsArray, ffsIdsArray);
    	} catch (Exception e) {
    		System.err.println("Error while adding Compound Fee");
    		e.printStackTrace();
    	}
	}

}
