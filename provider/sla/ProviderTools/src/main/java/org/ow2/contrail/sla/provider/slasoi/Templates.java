/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.slasoi;

import org.ow2.contrail.sla.provider.slasoi.context.ContrailSlaManagerContext;
import org.slasoi.businessmanager.ws.impl.BusinessManager_QueryProductCatalogStub;
import org.slasoi.businessmanager.ws.impl.BusinessManager_QueryProductCatalogStub.GetTemplates;
import org.slasoi.businessmanager.ws.impl.BusinessManager_QueryProductCatalogStub.GetTemplatesResponse;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLATemplate;


public class Templates {

	private SLAManagerContext slamContext;
	private BusinessManager_QueryProductCatalogStub qpcWSClient;
	private String bmQueryProductWSURL;
	private final String serviceName = "BusinessManager_QueryProductCatalog";
	private final String host = "localhost";
	private final String port = "8080";
	
	
	public Templates(String slamDb) {
		slamContext = new ContrailSlaManagerContext(slamDb);
	}
	
	
	public Templates() {
		slamContext = new ContrailSlaManagerContext(null);
	}
	
	
	
	/**
	 * Retrieves the list of SLA templates for a given product
	 * @param productId The ID of the product
	 * @param host The host name
	 * @param port The port number
	 * @return array of SLA templates
	 */
	public SLATemplate[] getTemplates(int productId, String host, String port) 
		throws Exception {

		if (host == null)
			host = this.host;
		
		if (port == null)
			port = this.port;
		
		bmQueryProductWSURL = "http://" + host + ":" + port + "/services/" + 
		serviceName + "?wsdl";
	    qpcWSClient = new BusinessManager_QueryProductCatalogStub(bmQueryProductWSURL);	
	        
	    GetTemplates gt = new GetTemplates();
	    gt.setCustomerId(0);
	    gt.setProductId(productId);
	    GetTemplatesResponse response = qpcWSClient.getTemplates(gt);
	    if (response == null) 
	    	return null;
	    
	    String encSlat = response.get_return();
	    if (encSlat ==  null) 
	    	return null;
	    
	    return (SLATemplate[]) Base64Utils.decode(encSlat);
	}

	
	
	/**
	 * Retrieves a template, given its ID
	 * @param templateId the SLA template ID
	 * @return the SLA template
	 */
	public SLATemplate getTemplate(String templateId) 
		throws Exception {

		SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl();
	    
	    reg.setSLAManagerContext(slamContext);
	         
	    UUID uuid = new UUID(templateId);
	    	
    	return reg.getSLATemplate(uuid);
    	
	}
	
	
	
	/**
	 * Add a new SLA template into the Business SLA template repository
	 * @param template the SLA template in XML format
	 * @param description the SLA template description
	 * @param providerId the ID of the provider
	 * @param registerId the name of the register (person)
	 */
	public void addTemplate(String template, String description, String providerId, 
			String registerId) throws Exception{

		SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl();
	    
	    reg.setSLAManagerContext(slamContext);
    	SLASOITemplateParser parser = new SLASOITemplateParser();
    	SLATemplate slat = parser.parseTemplate(template);
	         
    	SLATemplateRegistry.Metadata m = new SLATemplateRegistry.Metadata();
    	m.setPropertyValue(SLATemplateRegistry.Metadata.provider_uuid, providerId);
    	m.setPropertyValue(SLATemplateRegistry.Metadata.registrar_id, registerId);
	    	
    	reg.addSLATemplate(slat, m);

	}
	
}
