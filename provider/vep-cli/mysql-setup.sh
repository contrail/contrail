#!/bin/bash

# exit on error
set -e

MYSQL=`which mysql`
Q3="CREATE DATABASE IF NOT EXISTS vepdb; 
	USE vepdb;
	DROP PROCEDURE IF EXISTS vepdb.drop_user_if_exists ;
	DELIMITER $$
	CREATE PROCEDURE vepdb.drop_user_if_exists()
	BEGIN
		DECLARE foo BIGINT DEFAULT 0;
		SELECT COUNT(*) INTO foo FROM mysql.user WHERE User='vepuser' and Host='localhost';
		IF foo > 0 THEN
			DROP USER 'vepuser'@'localhost';
		END IF;
	END;$$
	CALL vepdb.drop_user_if_exists();
	DROP PROCEDURE IF EXISTS vepdb.drop_user_if_exists;
CREATE USER 'vepuser'@'localhost' IDENTIFIED BY 'contrail'; GRANT ALL ON vepdb.* TO 'vepuser'@'localhost' IDENTIFIED BY 'contrail';"
SQL="${Q3}"

if [ "$DEBIAN_FRONTEND" == "noninteractive" ]
then
	mysqlname="root"
	mysqlpasswd=""
else
	read -p "Please, provide mysql user name with admin rights: " mysqlname
	read -s -p "Please, provide mysql user's (${mysqlname}) password: " mysqlpasswd
fi

$MYSQL -u${mysqlname} --password="${mysqlpasswd}" -e "$SQL"
