/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.ow2.contrail.provider.cnr_pep_java.Attribute;
import org.ow2.contrail.provider.cnr_pep_java.PEP_callout;
import org.ow2.contrail.provider.cnr_pep_java.XACMLType;
/**
 *
 * @author piyush
 */
public class VEPHelperMethods 
{
    private static String vepProperties;
    private static Logger logger;
    
    VEPHelperMethods(String vepProp)
    {
        vepProperties = vepProp;
        logger = Logger.getLogger("VEP.HelperMethods");
    }
    
    public static String getPropertyFile()
    {
        return vepProperties;
    }
    
    public static String makeSHA1Hash(String input) throws NoSuchAlgorithmException
    {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.reset();
            byte[] buffer = input.getBytes();
            md.update(buffer);
            byte[] digest = md.digest();

            String hexStr = "";
            for (int i = 0; i < digest.length; i++) 
            {
                    hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
            }
            return hexStr;
    }
    
    public static boolean addProperty(String key, String val, Logger logger, String fileName)
    {
        Properties props = new Properties();
        try
        {
            logger.trace("Loading system properties into memory.");
            FileInputStream fis = new FileInputStream(fileName);
            FileOutputStream fos = null;
            props.load(fis);
            logger.trace("System properties successfully loaded.");
            props.setProperty(key, val);
            logger.trace("Adding/modifying system property [key=" + key + ", value=" + val + "]");
            fis.close();
            props.store((fos = new FileOutputStream(fileName)), "Author: Piyush Harsh");
            fos.close();
            logger.trace("Stored system properties back to file successfully.");
        }
        catch(Exception ex)
        {
            logger.warn("Unable to write back system properties file.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
            return false;
        }
        return true;
    }
    
    public static String getProperty(String key, Logger logger, String fileName)
    {
        String result = null;
        Properties props = new Properties();
        try
        {
            if(logger!=null && logger.isTraceEnabled())
                logger.trace("Trying to read system properties file.");
            FileInputStream fis = new FileInputStream(fileName);
            props.load(fis);
            result = props.getProperty(key);
            fis.close();
            if(logger!=null && logger.isTraceEnabled())
                logger.trace("Closing system properties file. [key=" + key + ", value=" + result + "].");
            else
            {
                if(logger == null)
                    System.out.println("LOGGER NOT YET INITIALIZED: Closing system properties file. [key=" + key + ", value=" + result + "].");
            }
        }
        catch(Exception ex)
        {
            result = null;
            if(logger!=null && logger.isEnabledFor(Priority.WARN))
                logger.warn("Error accessing system properties file.");
            if(logger!=null && logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else if(logger != null)
                logger.warn(ex.getMessage());
            else
                ex.printStackTrace(System.out);
        }
        return result;
    }
    
    public static String[] wrapText(String text, int len, boolean wordWrap)
    {
      // return empty array for null text
      if(text == null)
        return new String[] {};

      // return text if len is zero or less
      if(len <= 0)
        return new String[] {text};

      // return text if less than length
      if(text.length() <= len)
        return new String[] {text};

      char [] chars = text.toCharArray();
      ArrayList lines = new ArrayList();
      StringBuilder line = new StringBuilder();
      StringBuffer word = new StringBuffer();

      for(int i = 0; i < chars.length; i++)
      {
        word.append(chars[i]);

        if(wordWrap && chars[i] == ' ')
        {
          if((line.length() + word.length()) > len)
          {
            lines.add(line.toString());
            line.delete(0, line.length());
          }
          line.append(word);
          word.delete(0, word.length());
        }
        else if(!wordWrap)
        {
            if(word.length() == len)
            {
                lines.add(word.toString());
                word.delete(0, word.length());
            }
        }
      }

      // handle any extra chars in current word
      if (word.length() > 0)
      {
        if((line.length() + word.length()) > len)
        {
          lines.add(line.toString());
          line.delete(0, line.length());
        }
        line.append(word);
      }
      // handle extra line
      if (line.length() > 0)
      {
        lines.add(line.toString());
      }

      String[] ret = new String[lines.size()];
      for(int i =0; i<lines.size(); i++)
      {
          ret[i] = (String)lines.get(i);
      }
      return ret;
    }
    
    public static String getRESTwebHeader(boolean firstPage)
    {
        String response = "";
        response += "<html>\n" +
        "<head>\n" +
        "<title>Contrail: Virtual Execution Platform</title>\n" +
        "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n" +
        "<META HTTP-EQUIV=\"Expires\" CONTENT=\"-1\">\n" +
        "<style type=\"text/css\">\n" +
        "   A:link {color: #999999; text-decoration: none}\n" +
        "   A:visited {color: #333333; text-decoration: none}\n" +
        "   A:active {color: #333333; text-decoration: none}\n" +
        "   A:hover {color: #999999; font-weight:bold; color: #000000;}\n" +
        "</style>\n" +
        "<script type=\"text/javascript\">\n" +
        "<!--\n" +
        "function setContentHeight()\n" +
        "{\n" +
        "   var viewportwidth;\n" +
        "   var viewportheight;\n" +
        "   // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight\n" +
        "   if (typeof window.innerWidth != 'undefined')\n" +
        "   {\n" +
        "       viewportwidth = window.innerWidth,\n" +
        "       viewportheight = window.innerHeight\n" +
        "   }\n" +
        "   // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)\n" +
        "   else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)\n" +
        "   {\n" +
        "       viewportwidth = document.documentElement.clientWidth,\n" +
        "       viewportheight = document.documentElement.clientHeight\n" +
        "   }\n" +
        "   // older versions of IE\n" +
        "   else\n" +
        "   {\n" +
        "       viewportwidth = document.getElementsByTagName('body')[0].clientWidth,\n" +
        "       viewportheight = document.getElementsByTagName('body')[0].clientHeight\n" +
        "   }\n" +
        "   if(viewportheight - 370 > 240)\n" +
        "       document.getElementById('content').style.height = (viewportheight - 370) + 'px';\n" +
        "   else\n" +
        "       document.getElementById('content').style.height = '240px';\n" +
        "}\n\n" +
        "function goBack()\n" +
        "{\n" +
        "   window.history.back();\n" +
        "}\n" +
       "//-->\n" +
       "</script>\n" +
       "</head>\n\n" +

       "<body style=\"background:#CCCCCC;\" onResize=\"javascript:setContentHeight();\" onLoad=\"javascript:setContentHeight();\">\n\n" + 

        "<div id=\"header\" style=\"width:1024px;margin:0 auto;background:#333333;color:#CCCCCC;font-family:Arial;font-size:10pt;padding:10px;\">\n";
        if(firstPage)
            response += "<img src=\"https://www.cise.ufl.edu/~pharsh/public/banner.png\">\n";
        else
            response += "<img src=\"https://www.cise.ufl.edu/~pharsh/public/banner.png\" onClick=\"javascript:goBack();\">\n";
        response += "</div>\n\n" +

        "<div id=\"content\" style=\"width:1024px;margin:0 auto;background:white;color:#000000;font-family:Arial;font-size:10pt;padding:10px;overflow:auto;\">\n" +
        "<b>Welcome to the Virtual Execution Platform REST Web-Interface</b><br><br>\n" +
        "<i>Click on the links displayed below to retrieve information on VEP resources. Certain resources will allow you to perform actions on them, for other resources " +
        "only a GET operation is allowed from this web interface. For more complete feature access you are advised to use a full-featured REST client.</i><br><br>";   
        return response;
    }
    
    public static String getRESTwebFooter()
    {
        String response = "";
        response += "</div>\n\n" +

        "<div id=\"footer\" style=\"width:1024px;margin:0 auto;background:#333333;color:#CCCCCC;font-family:Arial;font-size:10pt;padding:10px;\">\n" +
        "<table style=\"border:0px;padding:0px;color:#FFFFFF;font-family:Arial;font-size:9pt;\">\n" +
        "<tr>\n" +
        "<td><img src=\"https://www.cise.ufl.edu/~pharsh/public/logo-footer.png\"></td>\n" + 
        "<td style=\"width:440px;text-align:justify;border-right:1px;border-left:0px;border-top:0px;border-bottom:0px;border-color:#FFFFFF;\" valign=\"top\">\n" +
        "This software is released under BSD license and is free to use. Contrail project is funded by European Commission under FP7 257438 directive.\n" +
        "The source code for Contrail VEP software can be downloaded from \n" +
        "<a href=\"http://websvn.ow2.org/listing.php?repname=contrail&path=%2Ftrunk%2Fprovider%2Fsrc%2Fvep%2F\" style=\"text-decoration:none;color:#CCCCFF;\" target=\"_blank\">OW2 repository</a>.\n" +
        "<br><br><font style=\"font-size:8pt;color:#CCCCCC;\">VEP REST Web-Interface and the software has been designed by Piyush Harsh with inputs from Florian Dudouet, Ales Cernivec, and Yvon Jegou.</font>\n" +
        "</td>\n" +
        "<td valign=\"top\">\n" +
        "   <table style=\"width:160px;border-left:1px;border-right:1px;border-top:0px;border-bottom:0px;border-style:dashed;border-color:#999999;\">\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Contrail consortium</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Contact us</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Contribute to VEP</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Features release timeline</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Documentation</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">VEP Wiki</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Download VEP</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Key developers</td></tr>\n" +
        "   </table>\n" +
        "</td>\n" +
        "<td valign=\"top\">\n" +
        "   <table style=\"width:160px;\">\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">WP5 Deliverables</td></tr>\n" +
        "   </table>\n" +
        "</td>\n" +
        "</table>\n" +
        "</div>\n\n" +

        "</body>\n" +
        "</html>";
        return response;
    }
    public static boolean pdpCheck(int uid, String ovfsno) throws SQLException
    {
        String pdp = VEPHelperMethods.getProperty("pdp.use", logger, VEPHelperMethods.getPropertyFile());
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        dbHandler db = new dbHandler("VEPHelperMethods", dbType);

        String activeVM = "0";
        PEP_callout pep_callout;

        List<Attribute> accessRequest = new ArrayList<Attribute>();
        String issuer = "CNR-Federation";
        //resource attributes
        Attribute attrResource = new Attribute(
                "urn:oasis:names:tc:xacml:1.0:resource:resource-id",
                XACMLType.STRING,
                "VMTemplate",
                issuer,
                Attribute.RESOURCE);
        accessRequest.add(attrResource);

        boolean usepdp = false;
        if(pdp != null)
            try
            {
                usepdp = Boolean.parseBoolean(pdp);
            }
            catch(Exception e)
            {
                usepdp = false;
            }
        else
            usepdp = false;

        if(!usepdp)
            return true;
        else
        {
            logger.debug("External access control through PDP module has been enabled.");
            if(ovfsno != null)
            {
                int ovfVM = 0;
                //Adding the number of VMs from the OVF to the total count, if there are more VMs defined in the OVF than the max number of VM allowed in PEP,
                //       VEP doesn't allow the instantiation of this OVF even if no VMs are active yet.
                ResultSet rs = db.query("select", "count(*) as no", "vmachinetemplate", "where ovfsno=" + ovfsno);
                if(rs.next())
                    ovfVM = rs.getInt("no");
                rs = db.query("select", "count(*) as no", " vmachine", "where uid="+uid+" and (state='UN' OR state='RN' OR state='PR' OR state='DP')");
                if(rs.next())
                    activeVM = String.valueOf(rs.getInt("no")+ovfVM-1);//-1, otherwise doesn't allow an ovf of 2 VMs to start even with no active VM
                else
                    activeVM = String.valueOf(ovfVM);
            }
            else
            {
                ResultSet rs = db.query("select", "count(*) as no", " vmachine", "where uid="+uid+" and (state='UN' OR state='RN' OR state='PR' OR state='DP')");
                if(rs.next())
                {
                    activeVM = String.valueOf(rs.getInt("no"));
                }
                else
                    activeVM = "0";
            }

            attrResource = new Attribute(
                    "urn:contrail:names:vep:resource:num-vm-running",
                    XACMLType.INT,
                    activeVM,
                    issuer,
                    Attribute.RESOURCE);
            accessRequest.add(attrResource);

            attrResource = new Attribute(
                    "urn:contrail:names:vep:resource:user",
                    XACMLType.STRING,
                    "username",
                    issuer,
                    Attribute.RESOURCE);
            accessRequest.add(attrResource);
            
            attrResource = new Attribute(
                    "urn:contrail:names:vep:resource:group",
                    XACMLType.STRING,
                    "usergroup[bronze,silver,gold]",
                    issuer,
                    Attribute.RESOURCE);
            accessRequest.add(attrResource);
            
            //action attributes
            Attribute attrAction = new Attribute(
                    "urn:contrail:vep:action:id",
                    XACMLType.STRING,
                    "deploy",
                    issuer,
                    Attribute.ACTION);
            accessRequest.add(attrAction);

            // Allocate PDP end point reference
            String pdp_endpoint = VEPHelperMethods.getProperty("pdp.endpoint", logger, VEPHelperMethods.getPropertyFile());
            if (pdp_endpoint == null)
                pdp_endpoint = "http://146.48.96.75:2000/contrailPDPwebApplication/contrailPDPsoap";
            pep_callout = new PEP_callout(pdp_endpoint);

            //invoke PDP for the access decision
            boolean accessDecision =  pep_callout.isPermit(accessRequest);
            // if true, the deploy action shoul be executed and denied otherwise

            return accessDecision;
        }
    } 
}
