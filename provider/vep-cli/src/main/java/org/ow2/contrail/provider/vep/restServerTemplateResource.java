/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 *
 * @author piyush
 */
public class restServerTemplateResource extends ServerResource 
{
    private dbHandler db;
    private Logger logger;
    private String dbType;
    
    public restServerTemplateResource()
    {
        logger = Logger.getLogger("VEP.restTemplateResource");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, VEPHelperMethods.getPropertyFile());
        db = new dbHandler("restServerTemplateResource", dbType);   
    }
    
    @Get("json")
    public Representation getValue() throws ResourceException
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        Representation response = null;

        String username = requestHeaders.getFirstValue("X-Username");
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(username);
            else if(acceptType.contains("json"))
                response = toJson(username);
        }
        else
        {
            //default rendering HTML
            response = toHtml(username);
        }
        
        return response;
    }
    
    public Representation toJson(String username) throws ResourceException 
    {
        JSONObject obj = new JSONObject();
        obj.put("title", "List of VM templates");
        JSONArray templateName = new JSONArray();
        JSONArray templateLink = new JSONArray();
        JSONArray ovfSno = new JSONArray();
        int count = 0;
        String[] groups;
        boolean isAdmin = false;
        
        if(username == null)
        {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            obj.put("error", "CLIENT_ERROR_BAD_REQUEST");
        }
        else
        {
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    rs.close();
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList for user: " + username + " is: " + groupList);
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    rs.close();
                    if(isAdmin)
                    {
                        rs = db.query("select", "vmid, ovfsno, ovfid", "vmachinetemplate", "");
                        while(rs.next())
                        {
                            // read the result set
                            templateName.add(rs.getString("ovfid"));
                            ovfSno.add(rs.getInt("ovfsno"));
                            templateLink.add("/template/" + rs.getInt("vmid"));
                            count++;
                        }
                        rs.close();
                        obj.put("templates", templateName);
                        obj.put("links", templateLink);
                        obj.put("ovf_sno", ovfSno);
                        obj.put("count", count);
                    }
                    else
                    {
                        rs = db.query("select", "vmid, ovfsno, ovfid", "vmachinetemplate", "where uid=" + uid);
                        while(rs.next())
                        {
                            // read the result set
                            templateName.add(rs.getString("ovfid"));
                            ovfSno.add(rs.getInt("ovfsno"));
                            templateLink.add("/template/" + rs.getInt("vmid"));
                            count++;
                        }
                        rs.close();
                        obj.put("templates", templateName);
                        obj.put("links", templateLink);
                        obj.put("ovf_sno", ovfSno);
                        obj.put("count", count);
                    }
                }
                else
                {
                    this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                    obj.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                }
            }
            catch(Exception ex)
            {
                obj.put("error", "SQL error occured.");
                logger.debug("Exception caught: " + ex.getMessage());
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
            }
        }
        StringRepresentation value = new StringRepresentation(obj.toJSONString(), MediaType.APPLICATION_JSON);
        return value;
    }
    
    public Representation toHtml(String username) throws ResourceException 
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(false));
        stringBuilder.append("List of VM Templates<br><br>");
        stringBuilder.append("<table border=1 style=\"border-size:1px;font-family:Courier;font-size:10pt;text-align:right;color:black;border-color:#000000;\" cellpadding=2 cellspacing=2>");
        stringBuilder.append("<tr><th align=right style=\"background:#333333;color:#CCCCCC;\">OVF Serial No</th>").
                append("<th align=right style=\"background:#333333;color:#CCCCCC;\">Template Name</th>");
        stringBuilder.append("<th align=right style=\"background:#333333;color:#CCCCCC;\">Link</th></tr>");
        //stringBuilder.append("<ul style='font-family:Times;font-size:11pt;color:white;'>");
        int count = 0;
        String[] groups;
        boolean isAdmin = false;
        
        if(username == null)
        {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            stringBuilder.append("</table><br><b>User credentials missing ... can not display template list.</b><br><br>");
        }
        else
        {
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    rs.close();
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList for user: " + username + " is: " + groupList);
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    rs.close();
                    if(isAdmin)
                    {
                        rs = db.query("select", "vmid, ovfsno, ovfid", "vmachinetemplate", "");
                        while(rs.next())
                        {
                            // read the result set
                            stringBuilder.append("<tr><td style=\"font-weight:bold;\">").append(rs.getInt("ovfsno")).append("</td><td style=\"font-weight:bold;\">").
                                    append(rs.getString("ovfid")).append("</td><td style=\"font-weight:bold;\"><a href='").append(rs.getInt("vmid")).append("' title='details of VM template ").append(rs.getString("ovfid")).append("'>/template/").append(rs.getInt("vmid")).append("</a></td></tr>");
                            count++;
                        }
                        rs.close();
                        stringBuilder.append("</table><br>");
                    }
                    else
                    {
                        rs = db.query("select", "vmid, ovfsno, ovfid", "vmachinetemplate", "where uid=" + uid);
                        while(rs.next())
                        {
                            // read the result set
                            stringBuilder.append("<tr><td style=\"font-weight:bold;\">").append(rs.getInt("ovfsno")).append("</td><td style=\"font-weight:bold;\">").
                                    append(rs.getString("ovfid")).append("</td><td style=\"font-weight:bold;\"><a href='").append(rs.getInt("vmid")).append("' title='details of VM template ").append(rs.getString("ovfid")).append("'>/template/").append(rs.getInt("vmid")).append("</a></td></tr>");
                            count++;
                        }
                        rs.close();
                        stringBuilder.append("</table><br>");
                    }
                }
                else
                {
                    this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                    stringBuilder.append("</table><br><b>User not found ... can not display template list.</b><br><br>");
                }
            }
            catch(Exception ex)
            {
                stringBuilder.append("</table><br>");
                stringBuilder.append("<B>SQL Query Error!! Details of exception follows ...</B>");
                stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                stringBuilder.append(ex.getMessage());
                logger.debug("Exception caught: " + ex.getMessage());
                stringBuilder.append("</div>");
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
            }
        }
        stringBuilder.append("Click on the banner image to go up one level<br>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
