/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author piyush
 */
public class dbHandler 
{
    public static Connection dbHandle;
    private Statement statement;
    private ResultSet rs;
    private Logger logger;
    private boolean systemOut;
    private String dbType;
    
    public dbHandler(String entryPoint, String dbtype)
    {
        try
        {
            systemOut = false;
            dbType = dbtype;
            logger = Logger.getLogger("VEP.dbHandler");
            if(dbHandle != null)
            {
                statement = dbHandle.createStatement();
                statement.setQueryTimeout(30);
                if(systemOut)
                System.out.println("Successfully initialized dbHandler object from " + entryPoint + " with type set to " + dbType + ".");
                logger.trace("dbHandler object initialized. EntryPoint: " + entryPoint);
            }
            else
            {
                logger.warn("dbHandler object initialization failed, null value received.");
                statement = null;
                if(systemOut)
                System.out.println("dbHandler object initialization from " + entryPoint + " failed! NULL value found.");
            }
        }
        catch(SQLException ex)
        {
            statement = null;
            logger.error("SQLException caught while initializating dbHandler.");
            if(systemOut)
            System.out.println("Caught exception in dbHandler construction.");
        }
    }
    
    public ResultSet query(String queryType, String filter, String table, String conditions)
    {
        try
        {
            if(systemOut)
            System.out.println("Executing query: " + queryType + " " + filter + " from " + table + " " + conditions);
            logger.debug("Executing query: " + queryType + " " + filter + " from " + table + " " + conditions);
            rs = statement.executeQuery(queryType + " " + filter + " from " + table + " " + conditions);
            return rs;
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL query.");
            ex.printStackTrace(System.out);
            return null;
        }
    }
    
    public boolean insert(String table, String values)
    {
        try
        {
            if(systemOut)
            System.out.println("Executing query: INSERT INTO " + table + " VALUES " + values);
            if(table.equals("ovf"))
                logger.debug("Executing query: INSERT INTO " + table + " VALUES " + " truncated for sanitization reasons.");
            else
                logger.debug("Executing query: INSERT INTO " + table + " VALUES " + values);
            int rcount = statement.executeUpdate("INSERT INTO " + table + " VALUES " + values);
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL insert.");
            ex.printStackTrace(System.out);
            return false;
        }
        return true;
    }
    
    public boolean update(String table, String values, String conditions)
    {
        try
        {
            logger.debug("Executing query: UPDATE " + table + " SET " + values + " " + conditions);
            if(systemOut)
            System.out.println("Executing query: UPDATE " + table + " SET " + values + " " + conditions);
            int rcount = statement.executeUpdate("UPDATE " + table + " SET " + values + " " + conditions);
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL update.");
            ex.printStackTrace(System.out);
            return false;
        }
        return true;
    }
    
    public boolean delete(String table, String conditions)
    {
        try
        {
            logger.debug("Executing query: DELETE FROM " + table + " " + conditions);
            if(systemOut)
            System.out.println("Executing query: DELETE FROM " + table + " " + conditions);
            int rcount = statement.executeUpdate("DELETE FROM " + table + " " + conditions);
        }
        catch(SQLException ex)
        {
            logger.error("SQLException caught while performing SQL delete.");
            ex.printStackTrace(System.out);
            return false;
        }
        return true;
    }
    
    public boolean beginTransaction()
    {
        try
        {
            logger.debug("Executing query: BEGIN TRANSACTION");
            if(systemOut)
            System.out.println("Executing query: BEGIN TRANSACTION");
            if(dbType.contains("mysql"))
                statement.execute("START TRANSACTION"); //changed from begin transaction
            else
                statement.execute("BEGIN TRANSACTION"); //changed from begin transaction
            return true;
        }
        catch(Exception ex)
        {
            logger.error("Exception caught while performing SQL begin transaction.");
            ex.printStackTrace(System.out);
            return false;
        }
    }
    
    public boolean commitTransaction()
    {
        try
        {
            logger.debug("Executing query: COMMIT TRANSACTION");
            if(systemOut)
            System.out.println("Executing query: COMMIT TRANSACTION");
            if(dbType.contains("mysql"))
                statement.execute("COMMIT"); //changed from COMMIT TRANSACTION
            else
                statement.execute("COMMIT TRANSACTION"); //changed from COMMIT TRANSACTION
            return true;
        }
        catch(Exception ex)
        {
            logger.error("Exception caught while performing SQL commit transaction.");
            ex.printStackTrace(System.out);
            return false;
        }
    }
    
    public boolean endTransaction()
    {
        try
        {
            logger.debug("Executing query: END TRANSACTION");
            if(systemOut)
            System.out.println("Executing query: END TRANSACTION");
            statement.execute("END TRANSACTION");
            return true;
        }
        catch(Exception ex)
        {
            logger.error("Exception caught while performing SQL end transaction.");
            ex.printStackTrace(System.out);
            return false;
        }
    }
    
    public static boolean validateSingleWord(String str)
    {
        //if str contains ' or " then invalid
        if(str.contains("'") || str.contains("\"") || str.contains(" ")) return false;
        else return true;
    }
    
    public static boolean validateSentence(String str)
    {
        //if str contains ' or " then invalid
        if(str.contains("'")) return false;
        else return true;
    }
    
    /**
     * makes a audit log entry into the db history table, entry timestamp is auto generated
     *
     * @param uname         username on whose behalf the action was performed
     * @param sqlString     the SQL statement that was executed
     * @param description   description string
     * @param code          three character code value to enter
     * @return              void, returns nothing
     */
    public void auditLog(String uname, String sqlString, String description, String code)
    {
        //this function only makes inserts into the history table.
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        try
        {
            insert("history", "('" + dateFormat.format(date) + "', '" + uname + "', '" + code + "', '" + sqlString + "', '" + description + "')");
        }
        catch(Exception ex)
        {
            insert("history", "('" + dateFormat.format(date) + "', '" + uname + "', '" + "EXCP" + "', '" + sqlString + "', '" + ex.getMessage() + "')");
        }
    }
}
