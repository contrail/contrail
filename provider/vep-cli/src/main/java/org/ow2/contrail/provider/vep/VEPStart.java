/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.io.BufferedReader;
import java.io.File;
import javax.swing.filechooser.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JTable;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.log4j.PropertyConfigurator;
import org.apache.commons.cli.*;

/**
 *
 * @author fdudouet
 */
public class VEPStart {
    
    private static BasicParser parser;
    private static CommandLine cli;
    private static HelpFormatter helpFormatter;
    private static boolean helpShown;
    private static boolean vepPropProvided;
    private static boolean loggerPropProvided;
    private static int logLevel;
    
    
    
    private int busyIconIndex = 0;
    private boolean adminAuthenticated;
    private String username;
    private String passDigest;
    private restServer rServer;
    private int restPortValue;
    private int restHTTPSPortValue;
    private String oneIP;
    private String oneXmlPort;
    private String oneUser;
    private String onePass;
    private ovfValidator xmlHandle;
    private LinkedList<PeriodicThread> pThreads;
    private ONExmlrpcHandler oneHandle;
    private VEPHost vepHost;
    private Connection dbHandle;
    private dbHandler dbhandler;
    private VEPComputeNode vepCNode;
    private Logger rootlogger;
    private Logger logger;
    private boolean systemOut;
    private String dbType;
    private Properties props;
    private VEPHelperMethods helperMethods;
    private String vepProperties;
    private String logPropFile;
    private CLIServer cliserver;
    
    public VEPStart(String propfile, String logpropfile, int lLevel){
        
//        systemOut=true;
        /////////////////////////Initializing the system properties/////////////
        vepProperties = propfile;
        logPropFile = logpropfile;
        logLevel = lLevel;
        helperMethods = new VEPHelperMethods(vepProperties);
        /////////////////////////Properties initialized/////////////////////////
        
        /////////////////////////Initializing the logger////////////////////////
        Properties log4j = new Properties();
        try
        {
            FileOutputStream fos = null;
            String logFile = "";
            String logSize = "";
            try
            {
                logFile = VEPHelperMethods.getProperty("veplog.file", logger, VEPHelperMethods.getPropertyFile());
                logSize = VEPHelperMethods.getProperty("veplog.size", logger, VEPHelperMethods.getPropertyFile());
            }
            catch(Exception ex)
            {
                //unable to read the log file values from system properties file
                //using default
                System.err.println("Unable to read log file settings from system properties file. using default values: vep.log and 1024 KB");
                System.err.println(ex.getMessage());
                logFile = "vep.log";
                logSize = "1024";
            }
            int LogSize = 0;
            boolean logFilePresent = false;
            if(logFile!=null && logFile.trim().length() > 0)
            {
                logFile = logFile.trim();
                if(logSize != null && logSize.trim().length() > 0)
                {
                    try
                    {
                        LogSize = Integer.parseInt(logSize);
                    }
                    catch(NumberFormatException ex)
                    {
                        System.err.println(ex.getMessage());
                        LogSize = 0;
                    }
                }
                //LogSize is in KB
                logFilePresent = true;
            }
            
            String logPart = "";
            switch(logLevel)
            {
                case 0:
                    logPart = "OFF";
                    break;
                case 1:
                    logPart = "FATAL";
                    break;
                case 2:
                    logPart = "ERROR";
                    break;
                case 3:
                    logPart = "WARN";
                    break;
                case 4:
                    logPart = "INFO";
                    break;
                case 5:
                    logPart = "DEBUG";
                    break;
                default:
                    logPart = "TRACE";
                    break;
            }
            
            if(logFilePresent)
                log4j.setProperty("log4j.rootLogger", "TRACE, stdout, R");
            else
                log4j.setProperty("log4j.rootLogger", "TRACE, stdout");
            log4j.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
            log4j.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
            log4j.setProperty("log4j.appender.stdout.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
            if(logFilePresent)
            {
                log4j.setProperty("log4j.appender.R", "org.apache.log4j.RollingFileAppender");
                log4j.setProperty("log4j.appender.R.File", logFile);
                log4j.setProperty("log4j.appender.R.MaxFileSize", Integer.toString(LogSize) + "KB");
                // Keep upto 10 backup file
                log4j.setProperty("log4j.appender.R.MaxBackupIndex", "10");
                log4j.setProperty("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
                log4j.setProperty("log4j.appender.R.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
            }
            log4j.store((fos = new FileOutputStream("log4j.properties")), "Author: Piyush Harsh");
            fos.close();
        }
        catch(Exception ex)
        {
            //ex.printStackTrace(System.err);
            System.out.println("Unable to save to log4j properties file: " + ex);
        }
        rootlogger = Logger.getLogger("VEP");
        //rootlogger.setLevel(Level.TRACE); //default
        switch(logLevel)
        {
            case 0:
                rootlogger.setLevel(Level.OFF);
                break;
            case 1:
                rootlogger.setLevel(Level.FATAL);
                break;
            case 2:
                rootlogger.setLevel(Level.ERROR);
                break;
            case 3:
                rootlogger.setLevel(Level.WARN);
                break;
            case 4:
                rootlogger.setLevel(Level.INFO);
                break;
            case 5:
                rootlogger.setLevel(Level.DEBUG);
                break;
            default:
                rootlogger.setLevel(Level.TRACE);
                break;
        }
        //BasicConfigurator.configure();
        PropertyConfigurator.configure("log4j.properties");
        logger = Logger.getLogger("VEP.main");
        /////////////////////Logger initialization done/////////////////////////
        
        ///////////////////// Thread init///////////////////////////////////////
        
        pThreads = new LinkedList<PeriodicThread>();
        
        ////////////////initializing the VEP database handle////////////////////
        try 
        {
            logger.trace("Trying to determine which database driver to use.");
            dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, VEPHelperMethods.getPropertyFile());
            logger.trace("Found database choice: " + dbType);
            if(dbType.contains("sqlite"))
                Class.forName("org.sqlite.JDBC");
            else
                Class.forName("org.gjt.mm.mysql.Driver");
            if(dbType.contains("sqlite"))
                logger.trace("jdbc:sqlite drivers loaded successfully.");
            else
                logger.trace("jdbc:mysql drivers loaded successfully.");
            if(dbType.contains("sqlite"))
            {
                logger.trace("Trying to read database file name.");
                String dbFile = VEPHelperMethods.getProperty("sqlite.db", logger, VEPHelperMethods.getPropertyFile());
                if(dbFile != null && dbFile.length() > 3)
                {
                    try
                    {
                        dbHandle = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
                        //dbHandle = DriverManager.getConnection("jdbc:mysql://localhost/vepdb?" + "user=vepuser&password=pass!@#$");
                        logger.trace("VEP jdbc:sqlite connection was established successfully.");
                        logger.trace("Performaing database sanity checks next.");
                        testDb();
                    }
                    catch(SQLException sqlEx)
                    {
                        dbHandle = null;
                        logger.warn("DB connection could not be established for the specified DB file.");
                        sqlEx.printStackTrace(System.out);
                    }
                }
                else
                {
                    logger.warn("Database file is not specified. Check system settings and try again.");
                }
            }
            else
            {
                logger.trace("Trying to open mysql connection.");
                String mysqlUser = VEPHelperMethods.getProperty("mysql.user", logger, VEPHelperMethods.getPropertyFile());
                String mysqlPass = VEPHelperMethods.getProperty("mysql.pass", logger, VEPHelperMethods.getPropertyFile());
                String mysqlIP = VEPHelperMethods.getProperty("mysql.ip", logger, VEPHelperMethods.getPropertyFile());
                String mysqlPort = VEPHelperMethods.getProperty("mysql.port", logger, VEPHelperMethods.getPropertyFile());
                if(mysqlUser != null && mysqlPass != null)
                {
                    try
                    {
                        //dbHandle = DriverManager.getConnection("jdbc:mysql://localhost/vepdb?" + "user=vepuser&password=pass!@#$");
                        dbHandle = DriverManager.getConnection("jdbc:mysql://" + mysqlIP + ":" + mysqlPort + "/vepdb?" + "user=" + mysqlUser + "&password=" + mysqlPass);
                        logger.trace("VEP jdbc:mysql connection was established successfully.");
                        logger.trace("Performaing database sanity checks next.");
                        testDb();
                    }
                    catch(SQLException sqlEx)
                    {
                        dbHandle = null;
                        logger.warn("Mysql DB connection could not be established for the specified mysql parameters.");
                        sqlEx.printStackTrace(System.out);
                    }
                }
                else
                {
                    logger.warn("Mysql connection parameters not specified. Check system settings and try again.");
                }
            }
        } 
        catch (ClassNotFoundException ex) 
        {
            //Logger.getLogger(VEPView.class.getName()).log(Level.SEVERE, null, ex);
            logger.fatal("Missing mysql or sqlite library ... could not load necessary drivers.");
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
        if(dbHandle != null)
        {
            dbHandler.dbHandle = dbHandle;
            dbhandler = new dbHandler("VEP Main", dbType);
            logger.trace("DB handler created successfully using jdbc:sqlite connection object.");
        }
        else
        {
            logger.warn("VEP DB file has not be specified. Check system settings and try again.");
        }
        ///////////////////VEP database handle initialization done//////////////
        
        //////////////////VEP DB Sync Thread Start////////////////////////// 
            PeriodicThread tempThread = new PeriodicThread(60000, "vepdb-sync", "dosync", null, null, null, null, "");
            tempThread.start();
            logger.trace("VEPdbSync thread (periodic, periodicity 60 seconds) started.");
            pThreads.push(tempThread);
        
        //////////////////VEP DB Sync Thread done////////////////////////// 
            
        //////////////////VEP CLI Server Start////////////////////////// 
            int port = 10555;
            String cliport = VEPHelperMethods.getProperty("cli.port", logger, VEPHelperMethods.getPropertyFile());
            try
            {
                port = Integer.parseInt(cliport);
            }
            catch(NumberFormatException nfex)
            {
                logger.warn("Garbled/missing CLI Server port value found ... trying to use default port 10555.");
            }
            
            cliserver = new CLIServer(port, dbhandler, vepProperties); //change this port with parameter from vep.configuration
            cliserver.startCLIserver();
            logger.trace("Contrail CLI Server has started on port "+ port + "."); 
            logger.trace("Contrail VEP application has started.");            
            
        //////////////////VEP CLI Server done//////////////////////////
            
        //////////////////VEP REST Service Start//////////////////////////
            //do necessary actions depending on the state of the toggle switch
            if(rServer == null)
            {
                 //Get the properties from the properties file

                String restPortString = VEPHelperMethods.getProperty("rest.restHTTPPort", logger, VEPHelperMethods.getPropertyFile());
                Boolean http = true;
                boolean https = true;
                if(restPortString == null || restPortString.equals("")){
                    logger.warn("Empty REST HTTP port value specified, using default port 10500.");
                    restPortValue = 10500; //using default
                } 
                else if(restPortString.equals(-1))
                {
                    logger.warn("-1 provided for HTTP Port, disabling HTTP Rest.");
                    restPortValue = -1;
                }
                else 
                {
                    try {
                        restPortValue = Integer.parseInt(restPortString);
                    }
                    catch(Exception e){
                        logger.error("Property rest.restHTTPPort could not be read, disabling rest Server. Please correct this value or provide none to use the default value");
                        http = false;
                    }
                }
                
                String restHTTPSPortString = VEPHelperMethods.getProperty("rest.restHTTPSPort", logger, VEPHelperMethods.getPropertyFile());
                if(restHTTPSPortString == null || restHTTPSPortString.equals("") || restHTTPSPortString.equals("-1")){
                    logger.error("Empty or -1 provided for HTTPSPort, disabling https");
                    restHTTPSPortValue = -1;
                }
                else
                {
                    try
                    {
                        restHTTPSPortValue = Integer.parseInt(restHTTPSPortString);
                    }
                    catch(Exception e){
                        logger.error("Property rest.restHTTPSPort could not be read, disabling rest Server. Please correct this value or provide none to disable HTTPS");
                        https = false;
                    }
                }

                String keyStorePath = VEPHelperMethods.getProperty("rest.keystore", logger, VEPHelperMethods.getPropertyFile());
                String keyStorePass = VEPHelperMethods.getProperty("rest.keystorepass", logger, VEPHelperMethods.getPropertyFile());
                String keyPass = VEPHelperMethods.getProperty("rest.keypass", logger, VEPHelperMethods.getPropertyFile());

                if(http && https){
                    boolean clientAuthSelected = Boolean.parseBoolean(VEPHelperMethods.getProperty("rest.clientAuthSelected", logger, VEPHelperMethods.getPropertyFile()));

                    if(clientAuthSelected)
                        rServer = new restServer(restPortValue, restHTTPSPortValue, true, keyStorePath, keyStorePass, keyPass);
                    else
                        rServer = new restServer(restPortValue, restHTTPSPortValue, false, keyStorePath, keyStorePass, keyPass);
                    logger.trace("Starting REST server.");
                    rServer.start();
                    if(restPortValue != -1)
                    {
                        logger.trace("Started REST server at HTTP port " + restPortValue + " HTTPS port " + restHTTPSPortValue + ".");
                    }
                    else
                    {
                    logger.trace("Started REST server at HTTPS port " + restHTTPSPortValue + "."); 
                    }
                } else {
                    logger.warn("REST Server could not be started because of incorrect values for HTTP or HTTPS port");
                }
                //introducing a delay of 500 msec
                try
                {
                    Thread.sleep(500);
                }
                catch(Exception ex)
                {
                    //ignore this exception
                }
                    
            }

        
        /////////////////VEP REST Service started//////////////////////////
                
        logger.trace("Contrail VEP application has started.");
        
    }
    
//     private String getProperty(String key)
//    {
//        String result = null;
////        Properties props = new Properties();
//        try
//        {
//            if(logger!=null && logger.isTraceEnabled())
//                logger.trace("Trying to read system properties file.");
////            FileInputStream fis = new FileInputStream(confFile);
//            
////            props.load(confStream);
//            result = props.getProperty(key);
////            fis.close();
//            if(logger!=null && logger.isTraceEnabled())
//                logger.trace("Closing system properties file. [key=" + key + ", value=" + result + "].");
//            else
//            {
//                if(logger == null)
//                    System.out.println("LOGGER NOT YET INITIALIZED: Closing system properties file. [key=" + key + ", value=" + result + "].");
//            }
//        }
//        catch(Exception ex)
//        {
//            result = null;
//            if(logger!=null && logger.isEnabledFor(Priority.WARN))
//                logger.warn("Error accessing system properties file.");
//            ex.printStackTrace(System.out);
//            System.exit(0);
//            if(systemOut)
//            System.out.println("Unable to open properties file." + ex);
//        }
//        return result;
//    }
//    

    
    
    private void testDb()
    {
        try
        {   
            logger.trace("Running VEP database sanity checks (primitive checks) now.");
            if(dbHandle != null)
            {
                DatabaseMetaData dbm = dbHandle.getMetaData();
                String[] types = {"TABLE"};
                ResultSet rs = dbm.getTables(null,null,"%",types);
                int count=0;
                if(systemOut)
                System.out.println("Table name:");
                while (rs.next())
                {
                    count++;
                    String table = rs.getString("TABLE_NAME");
                    if(systemOut)
                    System.out.println(table);
                }
                if(systemOut)
                System.out.println("Number of tables: " + count);
                if(count < 20)
                {
                    logger.warn("VEP database file failed sanity checks and will be reinitialized");
                    int n=0;
//                    logger.warn("VEP database file failed sanity checks. Asking user for reinitialization permission.");
//                    dbStatus.setToolTipText("Incorrect DB structure.");
//                    Object[] options = {"Initialize", "Don't Initialize"};
//                    int n = JOptionPane.showOptionDialog(mainPanel, "VEP Database internal structure seems incorrect.\n"
//                        + "This message could be displayed in case the VEP db is not initialized.\n"
//                        + "I can re-initialize the database to an empty db.\n"
//                        + "Do you wish to re-initialize to fix errors?", "VEP DB Initialization",
//                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
//                        null,     //do not use a custom Icon
//                        options,  //the titles of buttons
//                        options[1]); //default button title
                    if(n == 0)
                    {
                        logger.trace("Initiating database reinitialization process.");
                        //call initialize database function here
                        initializeDb();
                    }
                    else
                    {
                        //if(dbHandle != null) dbHandle.close();
                        //dbHandle = null;
                        logger.warn("VEP database reinitialization permit denied. VEP database actions will be disabled.");
                    }
                }
                else
                {
                    logger.debug("VEP database sanity checks (primitive checks) were completed successfully.");
                    logger.trace("VEP database actions are now enabled.");
                    //now test for table schema
//                    try
//                    {
//                        //if(dbhandler == null) dbhandler = new dbHandler("VEP Main");
//                        rs = dbhandler.query("SELECT", "did, name, loc, desc", "datacenter", "");
//                        rs = dbhandler.query("SELECT", "rid, clid, name, desc", "rack", "");
//                        rs = dbhandler.query("SELECT", "username, uid, vid, role", "user", "");
//                        rs = dbhandler.query("SELECT", "clid, nicid, did, desc, name", "cluster", "");
//                        rs = dbhandler.query("SELECT", "gname, uid", "ugroup", "");
//                        rs = dbhandler.query("SELECT", "cid, mem, cpu, rid, virt, hostname, clmgrtype, vid, desc, cpuarch", "computenode", "");
//                        rs = dbhandler.query("SELECT", "cid, hostid", "clmanager", "");
//                        dbActionSelector.setEnabled(true);
//                    }
//                    catch(Exception ex)
//                    {
//                        ex.printStackTrace(System.out);
//                        dbStatus.setBackground(Color.orange);
//                        dbActionSelector.setEnabled(false);
//                        dbStatus.setToolTipText("Incorrect DB structure.");
//                        Object[] options = {"Initialize", "Don't Initialize"};
//                        int n = JOptionPane.showOptionDialog(mainPanel, "VEP Database internal structure seems incorrect.\n"
//                            + "This message could be displayed in case the VEP db is not initialized.\n"
//                            + "I can re-initialize the database to an empty db.\n"
//                            + "Do you wish to re-initialize to fix errors?", "VEP DB Initialization",
//                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
//                            null,     //do not use a custom Icon
//                            options,  //the titles of buttons
//                            options[1]); //default button title
//                        if(n == 0)
//                        {
//                            //call initialize database function here
//                            initializeDb2();
//                        }
//                        else
//                        {
//                            //if(dbHandle != null) dbHandle.close();
//                            //dbHandle = null;
//                            JDialog infoMessage;
//                            JOptionPane infoMess = new JOptionPane("VEP DB functionality will be disabled..", JOptionPane.INFORMATION_MESSAGE);
//                            infoMessage = infoMess.createDialog(mainPanel, "Warning");
//                            infoMessage.setVisible(true);
//                        }
//                    }
                }
            }
            else
            {
                logger.warn("VEP database handler object is not valid. Terminating database sanity checks. Check system properties and try again.");
            }
        }
        catch(Exception ex)
        {
            logger.error("Exception caught during database sanity checks.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
    
    private void initializeDb()
    {
        if(dbHandle != null)
        {
            try
            {
                logger.trace("Initiating database (re)initialization process, deleteing any existing schema.");
                Statement stat = dbHandle.createStatement();
                stat.executeUpdate("drop table if exists user;");
                stat.executeUpdate("drop table if exists ugroup;"); 
                stat.executeUpdate("drop table if exists vorg;");
                stat.executeUpdate("drop table if exists datacenter;");
                stat.executeUpdate("drop table if exists cluster;");
                stat.executeUpdate("drop table if exists rack;");
                stat.executeUpdate("drop table if exists computenode;");
                stat.executeUpdate("drop table if exists vmachinetemplate;");
                stat.executeUpdate("drop table if exists vmachine;");
                stat.executeUpdate("drop table if exists vnet;");
                stat.executeUpdate("drop table if exists storage;");
                stat.executeUpdate("drop table if exists clmanager;");
                stat.executeUpdate("drop table if exists ipaddress;");
                stat.executeUpdate("drop table if exists history;");
                stat.executeUpdate("drop table if exists ovf;");
                stat.executeUpdate("drop table if exists ceeobj;");
                stat.executeUpdate("drop table if exists diskimage;");
                stat.executeUpdate("drop table if exists deployment;");
                stat.executeUpdate("drop table if exists ovfvmmap;");
                stat.executeUpdate("drop table if exists vepadmin;");
                
                logger.trace("Creating VEP DB schema again.");
                
                String createTable = "create table vepadmin "
                        + "(username varchar(40) NOT NULL, " 
                        + "password varchar(45) NOT NULL, "
                        + "PRIMARY KEY (username))";
                stat.executeUpdate(createTable);
                logger.trace("VEP table vepadmin created successfully.");
                
                createTable = "create table user "
                        + "(username varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "vid varchar(20) NOT NULL, "
                        + "oneuser varchar(45) NOT NULL, "
                        + "onepass varchar(45) NOT NULL, "
                        + "oneid integer NOT NULL, "
                        + "role varchar(20), PRIMARY KEY (username, vid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table user created successfully.");
                
                createTable = "create table ugroup "
                        + "(gname varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL)";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ugroup created successfully.");
                
                createTable = "create table vorg "
                        + "(vid varchar(40) NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "name varchar(40), "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3) NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (vid, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vorg created successfully.");
                
                //for datacenter entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table datacenter "
                        + "(did integer NOT NULL, " 
                        + "loc varchar(2) NOT NULL, "
                        + "name varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (did))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table datacenter created successfully.");
                
                //for cluster entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table cluster "
                        + "(clid integer NOT NULL, " 
                        + "nicid varchar(3), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "name varchar(40), "
                        + "PRIMARY KEY (clid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table cluster created successfully.");
                
                //for rack entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table rack "
                        + "(rid integer NOT NULL, " 
                        + "clid integer NOT NULL, "
                        + "name varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (rid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table rack created successfully.");
                
                //for computenode entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table computenode "
                        + "(cid integer NOT NULL, " 
                        + "mem integer(10) NOT NULL, "
                        + "cpu integer(5) NOT NULL, "
                        + "cpufreq integer(10), "
                        + "cpuarch varchar(40), "
                        + "rid integer, "
                        + "virt varchar(20) NOT NULL, "
                        + "hostname varchar(256) NOT NULL, "
                        + "clmgrtype varchar(256) NOT NULL, "
                        + "vid varchar(40), "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (cid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table computenode created successfully.");
                
                createTable = "create table vmachinetemplate "
                        + "(vmid integer NOT NULL, "
                        + "uid integer NOT NULL, "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3) NOT NULL, "
                        + "appname varchar(128), "
                        + "nid varchar(40), "
                        + "ip varchar(15), "
                        + "state varchar(2), "
                        + "ovfsno integer, "
                        + "ovfid varchar(40), "
                        + "ceeid integer, "
                        + "descp varchar(2048), " //changed desc to descp
                        + "PRIMARY KEY (vmid, nid, ip, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vmachinetemplate created successfully.");
                
                createTable = "create table vmachine "
                        + "(id integer NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "controller varchar(45), "
                        + "onevmid integer, "
                        + "vmid integer, "
                        + "name varchar(128), "
                        + "state varchar(2), "
                        + "cid integer NOT NULL, "
                        + "ipaddress varchar(15) NOT NULL, "
                        + "vncport varchar(10), "
                        + "vncip varchar(15), "
                        + "fqdn varchar(128), "
                        + "PRIMARY KEY (id))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vmachine created successfully.");
                
                //for vnet entry, assume default access rights, admins can create, modify, others can only read
                createTable = "create table vnet "
                        + "(nid varchar(40) NOT NULL, " 
                        + "iprange varchar(256) NOT NULL, " //changed range to iprange
                        + "ispublic varchar(1) NOT NULL, "
                        + "vid varchar(40), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (nid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table vnet created successfully.");
                
                //for storage entry, assume default access rights, admins can create, modify, others can only access
                createTable = "create table storage "
                        + "(sid varchar(40) NOT NULL, " 
                        + "type varchar(3) NOT NULL, "
                        + "vid varchar(40), "
                        + "size integer(10), "
                        + "did integer NOT NULL, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (sid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table storage created successfully.");
                
                //not accessible from REST, internal to VEP
                createTable = "create table clmanager "
                        + "(cid integer NOT NULL, " 
                        + "hostid varchar(40) NOT NULL, "
                        + "PRIMARY KEY (cid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table clmanager created successfully.");
                
                createTable = "create table ipaddress "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "nid varchar(40) NOT NULL, "
                        + "address varchar(15) NOT NULL, "
                        + "isassigned varchar(1) NOT NULL, "
                        + "vmid integer)";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ipaddress created successfully.");
                
                //not accessible from REST, internal to VEP
                createTable = "create table history "
                        + "(time varchar(40) NOT NULL, "
                        + "username varchar(40), "
                        + "code varchar(4), "
                        + "sqlstring varchar(1024), "
                        + "descp varchar(1024))"; //changed desc to descp
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table history created successfully.");
                
                createTable = "create table ovf "
                        + "(ovfname varchar(40) NOT NULL, " 
                        + "sno integer NOT NULL, "
                        + "ceeid integer NOT NULL, "
                        + "uid integer NOT NULL, "
                        + "state varchar(2), "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3), "
                        + "descp text(50240), " //changed desc to descp
                        + "PRIMARY KEY (ovfname, uid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ovf created successfully.");
                
                createTable = "create table ceeobj "
                        + "(ceeid integer NOT NULL, " 
                        + "uid integer NOT NULL, "
                        + "gname varchar(40) NOT NULL, "
                        + "perm varchar(3), "
                        + "slaobj BLOB, "
                        + "descp varchar(1024), " //changed desc to descp
                        + "PRIMARY KEY (ceeid, gname))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ceeobj created successfully.");
                
                createTable = "create table diskimage "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "gafslink varchar(1024), "
                        + "name varchar(1024), "
                        + "oneimgname varchar(128), "
                        + "oneimgid integer, "
                        + "vmid integer, "
                        + "state varchar(2), "
                        + "oneimgdescp varchar(1024), "
                        + "localpath varchar(1024))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table diskimage created successfully.");
                
                createTable = "create table deployment "
                        + "(id INTEGER PRIMARY KEY, " 
                        + "ovfsno integer NOT NULL, "
                        + "ceeid integer NOT NULL, "
                        + "content varchar(10240))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table deployment created successfully.");
                
                createTable = "create table ovfvmmap "
                        + "(ovfsno integer NOT NULL, " 
                        + "ovfid varchar(40) NOT NULL, "
                        + "vmid integer, PRIMARY KEY (ovfsno, ovfid))";
                stat.executeUpdate(createTable);
                logger.trace("VEP DB Table ovfvmmap created successfully.");
                
                logger.trace("Database (re)initialization process successful, enabling database panel actions.");
            }
            catch(SQLException ex)
            {
                logger.error("Database (re)initialization process failed.");
                ex.printStackTrace(System.out);
            }
        }
        else
        {
            logger.trace("Database handler is not valid, initialization process was stopped. Check system parameters and restart the software.");
        }
    }
    
    
        public static void main(String[] args) {
        
        helpShown = false;
        vepPropProvided = false;
        loggerPropProvided = false;
        int lLevel = 6;
        String pFile = "";
        String lFile = "";
        VEPStart vep;
        String defaultPath = System.getProperty("user.home") + System.getProperty("file.separator") + ".vep-cli" + System.getProperty("file.separator");
        Options options = new Options();
        options.addOption("p", "vep-properties", true, "path to the VEP properties file");
        options.addOption("l", "log-properties", true, "path to the VEP logger properties file");
        options.addOption("h", "help", false, "usage help");
        options.addOption("d", "default", false, "start with a default VEP properties file, you must change the defaults to the right values manually");
        options.addOption("v", "log-level", true, "log verbosity level, (0 = off, 1 = fatal, 2 = error, 3 = warn, 4 = info, 5 = debug, 6 = everything)");
        if(args.length > 0)
        {
            parser = new BasicParser();
            try
            {
                cli = parser.parse(options, args);
                if(cli.hasOption('p'))
                {
                    pFile = cli.getOptionValue('p');
                    vepPropProvided = true;
                }
                if(cli.hasOption('l'))
                {
                    lFile = cli.getOptionValue('l');
                    loggerPropProvided = true;
                }
                if(cli.hasOption('h'))
                {
                    helpFormatter = new HelpFormatter();
                    helpFormatter.printHelp(new PrintWriter(System.out, true), 80, "java -jar VEPController.jar", "Contrail Virtual Execution Platform Controller Module", 
                                    options, 3, 5, "", true);
                    helpShown = true;
                }
                if(cli.hasOption('d'))
                {
                    try
                    {
                        //check to see if the default path exists
                        System.out.println("Checking to see if the system default path exists or not.");
                        boolean success = (new File(defaultPath)).mkdir();
                        if(success)
                            System.out.println("Default path " + defaultPath + " was created successfully.");
                        else
                            System.out.println("Default path " + defaultPath + " already exists.");
                    }
                    catch(Exception ex)
                    {
                        System.out.println("Could not create the default directory to store the vep properties, exception caught: " + ex.getMessage());
                    }
                    Properties props = new Properties();
                    System.out.println("Creating the default vep.properties file at " + defaultPath + "vep.properties");
                    FileOutputStream fos = null;
                    String IPval = "127.0.0.1";
                    String port = "2633";
                    String userName = "oneadmin";
                    String userPass = "changeme";
                    String dbFile = "vep.db";
                    String logFile = "vep.log";
                    String size = "1024";
                    String clusterId = "";
                    String restkeyFile = "";
                    String restStorePass = "changeme";
                    String restKeyPass = "changeme";
                    String dbType = "sqlite";
                    String mySqlIP = "127.0.0.1";
                    String mySqlPort = "3306";
                    String mySqlUser = "vepuser";
                    String mySqlPass = "contrail";
                    String restHTTPSPort = "-1";
                    String restHTTPPort = "10500";
                    String clientAuthSelected = "false";
                    props.setProperty("vepdb.choice", dbType);
                    props.setProperty("mysql.ip", mySqlIP);
                    props.setProperty("mysql.port", mySqlPort);
                    props.setProperty("mysql.user", mySqlUser);
                    props.setProperty("mysql.pass", mySqlPass);
                    props.setProperty("one.ip", IPval);
                    props.setProperty("one.port", port);
                    props.setProperty("one.user", userName);
                    props.setProperty("one.pass", userPass);
                    props.setProperty("sqlite.db", dbFile);
                    props.setProperty("veplog.file", logFile);
                    props.setProperty("veplog.size", size);
                    props.setProperty("rest.keystore", restkeyFile);
                    props.setProperty("rest.keystorepass", restStorePass);
                    props.setProperty("rest.keypass", restKeyPass);
                    props.setProperty("rest.restHTTPSPort", restHTTPSPort);
                    props.setProperty("rest.restHTTPPort", restHTTPPort);
                    props.setProperty("rest.clientAuthSelected", clientAuthSelected);
                    props.setProperty("contrail.cluster", clusterId);
                    props.setProperty("pdp.use", "false");
                    props.setProperty("pdp.endpoint", "http://146.48.96.75:2000/contrailPDPwebApplication/contrailPDPsoap");
                    props.setProperty("cli.port", "10555");
                    try
                    {
                        props.store((fos = new FileOutputStream(defaultPath + "vep.properties")), "Author: Piyush Harsh");
                        fos.close();
                        System.out.println("Created the vep.properties file.");
                    }
                    catch(Exception ex)
                    {
                        System.out.println("Exception caught while creating the vep properties file at " + defaultPath + "vep.properties");
                        System.out.println("Exception: " + ex.getMessage());
                    }
                }
                
                if(cli.hasOption('v'))
                {
                    String loglevel = cli.getOptionValue('v');
                    try
                    {
                        lLevel = Integer.parseInt(loglevel);
                    }
                    catch(Exception ex)
                    {
                        lLevel = 6;
                    }
                }
                
                if(vepPropProvided && !loggerPropProvided && !helpShown)
                {
                    
                    lFile = defaultPath + "log4j.properties";
                    if(new File(lFile).exists())
                        System.out.println("VEP logger properties file path was not specified, using the default path for vep logger properties file: " + lFile);
                    else
                    {
                        System.out.println("VEP logger properties file path was not specified, a default one will be created");
                    }
                }
                else if(!vepPropProvided && loggerPropProvided && !helpShown)
                {
                    pFile = defaultPath + "vep.properties";
                    if(new File(pFile).exists())
                        System.out.println("VEP system properties file path was not specified, using the default path for vep properties file: " + pFile);
                    else
                    {
                        System.out.println("VEP system properties file path was not specified and the default file " +lFile+ " does not exist");
                        helpFormatter = new HelpFormatter();
                        helpFormatter.printHelp(new PrintWriter(System.out, true), 80, "java -jar VEPController.jar", "Contrail Virtual Execution Platform Controller Module", 
                                    options, 3, 5, "", true);
                        helpShown = true;
                    }
                }
                else if(!vepPropProvided && !loggerPropProvided && !helpShown)
                {
                    lFile = defaultPath + "log4j.properties";
                    pFile = defaultPath + "vep.properties";
                    if(new File(pFile).exists() && new File(lFile).exists())
                    {
                        System.out.println("VEP system properties file and VEP logger properties paths were not specified, "
                                + "using the default path for vep properties (" + pFile + ") and vep logger properties files (" + lFile + ").");
                    }
                    else if(new File(pFile).exists())
                    {
                        System.out.println("VEP system properties file and VEP logger properties paths were not specified, "
                                + "using the default path for vep properties (" + pFile + ") and created a new default logger properties files");
                    }
                    else
                    {
                        System.out.println("VEP system properties file and VEP logger properties paths were not specified and either vep.properties or log4j.properties are not in the default directory: "+defaultPath);
                        helpFormatter = new HelpFormatter();
                        helpFormatter.printHelp(new PrintWriter(System.out, true), 80, "java -jar VEPController.jar", "Contrail Virtual Execution Platform Controller Module", 
                                    options, 3, 5, "", true);
                        helpShown = true;
                    }
                }
            }
            catch(ParseException ex)
            {
                helpFormatter = new HelpFormatter();
                helpFormatter.printUsage(new PrintWriter(System.out, true), 80, "Contrail Virtual Execution Platform Controller Module", options);
                helpShown = true;
            }
        }
        else
        {
            lFile = defaultPath + "log4j.properties";
            pFile = defaultPath + "vep.properties";
            if(new File(pFile).exists() && new File(lFile).exists())
            {
                System.out.println("VEP system properties file and VEP logger properties paths were not specified, "
                        + "using the default values for vep properties (" + pFile + ") and vep logger properties files (" + lFile + ").");
            }
            else if(new File(pFile).exists())
            {
                System.out.println("VEP system properties file and VEP logger properties paths were not specified, "
                        + "using the default path for vep properties (" + pFile + ") and created a new default logger properties files");
            }
            else
            {
                System.out.println("VEP system properties file and VEP logger properties paths were not specified and either vep.properties or log4j.properties are not in the default directory: "+defaultPath);
                helpFormatter = new HelpFormatter();
                helpFormatter.printHelp(new PrintWriter(System.out, true), 80, "java -jar VEPController.jar", "Contrail Virtual Execution Platform Controller Module", 
                            options, 3, 5, "", true);
                helpShown = true;
            }   
        }
        if(!helpShown)
            vep = new org.ow2.contrail.provider.vep.VEPStart(pFile, lFile, lLevel);
    } 
    
}
