/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

/**
 *
 * @author piyush
 */
public class ONEVm {
    public int id;
    public int uid;
    public String name;
    public String last_poll;
    public String state;
    public String lcm_state;
    public String start_time;
    public String end_time;
    public long memory;
    public int cpu;
    public String deploy_id;
    public String graphics_ip;
    public int graphics_port;
    public String graphics_type;
    public String bridge;
    public String ip;
    public String mac;
    public String network;
    public int network_id;
    public String host_fqdn;
    public int host_id;
    
    public ONEVm()
    {
        cpu = -1;
        memory = -1;
        graphics_ip = "";
        graphics_port = -1;
        graphics_type = "";
        bridge = "";
        ip = "";
        mac = "";
        network = "";
        network_id = -1;
        host_fqdn = "";
        host_id = -1;
    }
}
