/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.awt.Color;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.log4j.Logger;
import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.host.HostPool;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
/**
 *
 * @author piyush
 */
public class PeriodicThread extends DefaultHandler implements Runnable
{
    private Thread t;
    private long periodicity;
    private String objectType;
    private String actionType;
    private JTextArea logArea;
    private boolean killSignal;
    private JPanel panelStatus;
    private String[] parameters;
    private boolean isAlive;
    private LinkedList<ONEHost> rowList;
    private ONEHost tempHost;
    private String tempVal;
    private JLabel output;
    private JComboBox list;
    private Logger logger;
    private boolean systemOut;
    private VEPDBSyncCleanup dbsync;
    
    public PeriodicThread(long period, String obj, String action, JTextArea log, JLabel out, JComboBox ls, JPanel stat, String param)
    {
        periodicity = period;
        objectType = obj;
        actionType = action;
        logArea = log;
        output = out;
        list = ls;
        panelStatus = stat;
        logger = Logger.getLogger("VEP.PeriodicThread");
        killSignal = false;
        systemOut = false;
        parameters = param.split(":"); //param to be provided has to be : separated
        t = new Thread(this);
    }
    
    public void start()
    {
        logger.trace("Starting thread " + objectType + " with [parameters[0]: " + parameters[0] + ", periodicity: " + periodicity + "].");
        t.start();
        isAlive = true;
    }
    
    public void terminate()
    {
        killSignal = true;
        //now try to awake the thread if asleep
        try
        {
            t.interrupt();
            logger.trace("Stopping thread " + objectType + " [parameters[0]: " + parameters[0] + ", periodicity: " + periodicity + "].");
        }
        catch(Exception ex)
        {
            
        }
        if(panelStatus != null)
            panelStatus.setBackground(Color.red);
    }

    @SuppressWarnings("SleepWhileInLoop")
    public void run() 
    {
        if(systemOut)
            System.out.println("Starting periodic thread: " + objectType + ":" + actionType);
        logger.trace("Thread " + objectType + " with action=" + actionType + " started.");
        if(objectType.equalsIgnoreCase("one-xmlrpc") && actionType.equalsIgnoreCase("hostmonitor"))
        {
            if(parameters.length != 4)
            {
                if(systemOut)
                    System.out.println("PeriodicThread: " + objectType + ":" + actionType + " :: incorrect parameters passed!");
                JDialog errorMessage;
                JOptionPane errMess = new JOptionPane("ONE XMLRPC parameters are not set correctly.<br>Go to <b>Edit -> Settings</b> and set parameters and try again.", JOptionPane.ERROR_MESSAGE);
                errorMessage = errMess.createDialog(output.getRootPane(), "Action can not be completed");
                logger.error("PeriodicThread: " + objectType + ":" + actionType + " :: incorrect parameters passed!");
                errorMessage.setVisible(true);
                panelStatus.setBackground(Color.ORANGE);
            }
        }
        else if(objectType.equalsIgnoreCase("vepdb-sync") && actionType.equalsIgnoreCase("dosync"))
        {
            dbsync = new VEPDBSyncCleanup();
        }
        while(!killSignal)
        {
            try
            {
                if(objectType.equalsIgnoreCase("one-xmlrpc") && actionType.equalsIgnoreCase("hostmonitor") && (parameters.length == 4))
                {
                    Client oneClient;
                    try
                    {
                        oneClient  = new Client(parameters[2] + ":" + parameters[3], "http://" + parameters[0] + ":" + parameters[1] + "/RPC2");
                        panelStatus.setBackground(Color.GREEN);
                    }
                    catch(Exception ex)
                    {
                        logger.error("Unable to connect to the XML-RPC endpoint. Check system properties and try again.");
                        if(systemOut)
                            System.out.println("Unable to connect to the XML-RPC endpoint. Check authentication/connection parameters.");
			oneClient = null;
                        panelStatus.setBackground(Color.ORANGE);
                        panelStatus.setToolTipText("Connection error.");
                        if(logger.isDebugEnabled())
                            ex.printStackTrace(System.err);
                        else
                            logger.fatal(ex.getMessage());
                    }
                    if(oneClient != null)
                    {
                        OneResponse val = HostPool.info(oneClient);
                        String response = "";
                        if(!val.isError())
                        {
                            //System.out.println("HostPool Response:\n " + val.getMessage());
                            response = val.getMessage();
                            panelStatus.setToolTipText("ONE XML-RPC connection is open.");
                        }
                        else
                        {
                            logger.warn("OpenNebula XML-RPC connection error. Check OpenNebula connection seetings under system properties. Check if oned is running.");
                            panelStatus.setBackground(Color.ORANGE);
                            panelStatus.setToolTipText("HostPool query resulted in error.");
                            if(systemOut)
                            System.out.println("Connection error! HostPool query resulted in error.");
                        }
                        //parse the XML response and populate the table next
                        if(response.length() > 1)
                        {
                            createTable(response, output);
                        }
                    }
                }
                else if(objectType.equalsIgnoreCase("vepdb-sync") && actionType.equalsIgnoreCase("dosync"))
                {
                    if(dbsync != null)
                        dbsync.doSyncCleanup();
                }
                Thread.sleep(periodicity);
            }
            catch(Exception ex)
            {
                
            }
        }
        if(systemOut)
        System.out.println("Exitting periodic thread: " + objectType + ":" + actionType);
        logger.trace("Thread " + objectType + " with action=" + actionType + " stopped.");
        isAlive = false;
    }
    
    public boolean isRunning()
    {
        return isAlive;
    }
    
    public void createTable(String input, JLabel panel)
    {
        rowList = new LinkedList<ONEHost>();
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try
        {
            SAXParser sp = spf.newSAXParser();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(input));
            sp.parse(is, this);
        }
        catch(SAXException se)
        {
            if(logger.isDebugEnabled())
                se.printStackTrace(System.err);
            else
                logger.warn(se.getMessage());
	}
        catch(ParserConfigurationException pce)
        {
            if(logger.isDebugEnabled())
                pce.printStackTrace(System.err);
            else
                logger.warn(pce.getMessage());
	}
        catch(IOException ie)
        {
            if(logger.isDebugEnabled())
                ie.printStackTrace(System.err);
            else
                logger.warn(ie.getMessage());
	}
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
    {
        tempVal = "";
        if(qName.equalsIgnoreCase("HOST"))
        {
            tempHost = new ONEHost(); //blank it for new row
	}
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
	tempVal = new String(ch,start,length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if(qName.equalsIgnoreCase("HOST"))
        {
            rowList.add(tempHost);
        }
        else if(qName.equalsIgnoreCase("ID"))
        {
            tempHost.id = tempVal;
        }
        else if(qName.equalsIgnoreCase("NAME"))
        {
            tempHost.name = tempVal;
        }
        else if(qName.equalsIgnoreCase("STATE"))
        {
            tempHost.state = tempVal;
        }
        else if(qName.equalsIgnoreCase("IM_MAD"))
        {
            tempHost.im_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("VM_MAD"))
        {
            tempHost.vm_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("TM_MAD"))
        {
            tempHost.tm_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("LAST_MON_TIME"))
        {
            tempHost.mon_time = tempVal;
        }
        else if(qName.equalsIgnoreCase("CLUSTER"))
        {
            tempHost.cluster = tempVal;
        }
        else if(qName.equalsIgnoreCase("HOST_POOL"))
        {
            list.setEnabled(false);
            String outHTML = "";
            outHTML += "<html><div style='padding:5px;background:white;font-family:Verdana;font-size:10pt;width:370px;'>";
            outHTML += "<table style='background:white;font-family:Verdana;font-size:10pt;border:0px;' cellpadding='2' cellspacing='1'>";
            outHTML += "<tr><th>Id</th><th>Name</th><th>State</th><th>Im_mad</th><th>Vm_mad</th><th>Tm_mad</th><th>Cluster</th><th>Last Mon Time</th></tr>";
            list.removeAllItems();
            list.addItem("not selected");
            //output.setText("ID\tNAME\tSTATE\tIM MAD\tVM MAD\tTM MAD\tCLUSTER\tLAST MON TIME\n");
            //output.append("--\t----\t-----\t------\t------\t------\t-------\t-------------\n");
            while(rowList.size() != 0)
            {
                ONEHost temp = rowList.pop();
                list.addItem("HostID " + temp.id);
                outHTML += "<tr><td style='background:gray;'>" + temp.id + "</td><td style='background:gray;'>" + temp.name + "</td><td style='background:gray;'>" + 
                        temp.state + "</td><td style='background:gray;'>" + temp.im_mad + "</td><td style='background:gray;'>" + temp.vm_mad + 
                        "</td><td style='background:gray;'>" + temp.tm_mad + "</td><td style='background:gray;'>" + temp.cluster + 
                        "</td><td style='background:gray;'>" + temp.mon_time + "</td></tr>";
                //output.append(temp.id + "\t" + temp.name + "\t" + temp.state + "\t" + temp.im_mad + "\t" + temp.vm_mad + "\t" + temp.tm_mad + "\t" + temp.cluster + "\t" + temp.mon_time + "\n");
                if(systemOut)
                System.out.println(temp.id + " " + temp.name + " " + temp.state + " " + temp.im_mad + " " + temp.vm_mad + " " + temp.tm_mad + " " + temp.cluster + " " + temp.mon_time);
                logger.trace("Found host: " + temp.id + " " + temp.name + " " + temp.state + " " + temp.im_mad + " " + temp.vm_mad + " " + temp.tm_mad + " " + temp.cluster + " " + temp.mon_time);
            }
            outHTML += "</table></div></html>";
            output.setText(outHTML);
            list.setEnabled(true);
        }
    }
}
