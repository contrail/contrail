/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 *
 * @author piyush
 */
public class restServerAdminControl extends ServerResource {
    @Get("html")
    public Representation toHtml() throws ResourceException 
    {
        StringBuilder stringBuilder = new StringBuilder();
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        this.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        return value;
    }
}
