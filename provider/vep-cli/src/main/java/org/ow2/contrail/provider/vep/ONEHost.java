/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import org.apache.log4j.Logger;

/**
 *
 * @author piyush
 */
public class ONEHost
{
    public String id;
    public String name;
    public String state;
    public String im_mad;
    public String vm_mad;
    public String tm_mad;
    public String cluster;
    public String mon_time;
    public String hid;
    public String disk_usage;
    public String mem_usage;
    public String cpu_usage;
    public String max_disk;
    public String max_mem;
    public String max_cpu;
    public String free_disk;
    public String free_mem;
    public String free_cpu;
    public String used_disk;
    public String used_mem;
    public String used_cpu;
    public String running_vms;
    public String hostName;
    public String hypervisor;
    public String arch;
    public String modelname;
    public String cpuspeed;
    private Logger logger;
    
    public ONEHost()
    {
        logger = Logger.getLogger("VEP.OneHost");
        logger.trace("ONE Host object created.");
    }
    
    public String getHTMLFormattedOutput()
    {
        String result = "";
        result += "<tr><th style='text-align:left;'>id</th><th style='text-align:left;'>name</th><th style='text-align:left;'>state</th><th style='text-align:left;'>cluster</th><th style='text-align:left;'>im_mad</th><th style='text-align:left;'>vm_mad</th></tr>";
        result += "<tr><td style='background:gray;'>" + id + "</td><td style='background:gray;'>" + name + "</td><td style='background:gray;'>" + state + "</td><td style='background:gray;'>" + cluster + "<td style='background:gray;'>" + im_mad + "</td><td style='background:gray;'>" + vm_mad + "</td></tr>";
        result += "<tr><th style='text-align:left;'>tm_mad</th><th style='text-align:left;'>last mon time</th><th style='text-align:left;'>hid</th><th style='text-align:left;'>disk usage</th><th style='text-align:left;'>mem usage</th><th style='text-align:left;'>cpu usage</th></tr>";
        result += "<tr><td style='background:gray;'>" + tm_mad + "</td><td style='background:gray;'>" + mon_time + "<td style='background:gray;'>" + hid + "</td><td style='background:gray;'>" + disk_usage + "</td><td style='background:gray;'>" + mem_usage + "</td><td style='background:gray;'>" + cpu_usage +"</td></tr>";
        result += "<tr><th style='text-align:left;'>max disk</th><th style='text-align:left;'>max mem</th><th style='text-align:left;'>max cpu</th><th style='text-align:left;'>running vms</th><th style='text-align:left;'>free disk</th><th style='text-align:left;'>free mem</th></tr>";
        result += "<tr><td style='background:gray;'>" + max_disk + "</td><td style='background:gray;'>" + max_mem + "</td><td style='background:gray;'>" + max_cpu + "</td><td style='background:gray;'>" + running_vms + "<td style='background:gray;'>" + free_disk + "</td><td style='background:gray;'>" + free_mem + "</td></tr>";
        result += "<tr><th style='text-align:left;'>free cpu</th><th style='text-align:left;'>hypervisor</th><th style='text-align:left;'>used disk</th><th style='text-align:left;'>used mem</th><th style='text-align:left;'>used cpu</th><th style='text-align:left;'>cpu arch</th></tr>";
        result += "<tr><td style='background:gray;'>" + free_cpu + "</td><td style='background:gray;'>" + hypervisor + "<td style='background:gray;'>" + used_disk + "</td><td style='background:gray;'>" + used_mem + "</td><td style='background:gray;'>" + used_cpu + "</td><td style='background:gray;'>" + arch + "</td></tr>";
        result += "<tr><th colspan='2' style='text-align:left;'>host name</th><th colspan='4' style='text-align:left;'>model name</th></tr>";
        result += "<tr><td colspan='2' style='background:gray;'>" + hostName + "<td colspan='4' style='background:gray;'>" + modelname + "</td></tr>";
        logger.trace("Generated HTML formatted output for OneHost Object [id=" + id + "].");
        return result;
    }
}