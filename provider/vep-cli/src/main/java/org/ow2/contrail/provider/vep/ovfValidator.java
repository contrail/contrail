/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.swing.JTextArea;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/**
 *
 * @author piyush
 */
public class ovfValidator
{
    private String xmlFile;
    private JTextArea logArea;
    private Logger logger;
    private boolean systemOut;
    
    public ovfValidator(String fName, JTextArea area)
    {
        xmlFile = fName;
        logArea = area;
        logger = Logger.getLogger("VEP.ovfValidator");
        systemOut = false;
    }
    
    public int validate()
    {
        try
        {
            SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            URL url = new URL("http://schemas.dmtf.org/ovf/envelope/1/dsp8023_1.1.xsd");
            logArea.append("Trying to download the OVF schema from DMTF site ...\n");
            InputStream is = url.openStream(); 
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            FileWriter fstream = new FileWriter("tmp.xsd");
            BufferedWriter out = new BufferedWriter(fstream);
            boolean eof = false;
            String s = br.readLine();
            while(!eof)
            {
                out.write(s);
                try
                {
                    s = br.readLine();
                    if(s == null)
                    {
                        eof = true;
                        br.close();
                    }
                }
                catch(EOFException eo)
                {
                    eof = true;
                }
                catch(IOException e)
                {
                    logger.error("Error while downloading OVF schema file from DMTF servers.");
                    if(systemOut)
                    System.out.println("IO Error : "+ e.getMessage());
                    return -1;
                }
            }
            out.close();
            logArea.append("OVF schema downloaded!\n");
            logger.trace("DMTF schema file downloaded. Starting the OVF validation process.");
            //File schemaLocation = new File("ovf_envelop_1_1_0.xsd");
            logArea.append("Starting OVF validation ...");
            File schemaLocation = new File("tmp.xsd");
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(xmlFile);
            try
            {
                validator.validate(source);
                if(systemOut)
                System.out.println(xmlFile + " is a valid OVF.");
                logArea.append(" done.\n");
                return 1;
            }
            catch (SAXException ex)
            {
                logger.debug("OVF Invalid! SAX Exception Caught: " + ex.getMessage());
                if(systemOut)
                {
                    System.out.println(xmlFile + " is not valid OVF because ");
                    System.out.println(ex.getMessage());
                }
                logArea.append(" done.\n");
                return 0;
            }  
        }
        catch(SAXException saxex)
        {
            logger.error("SAX Exception Caught! OVF validation Interrupted.");
            if(systemOut)
            System.out.println("Something went bad: caught SAXException.");
            saxex.printStackTrace(System.out);
        }
        catch(IOException ioex)
        {
            logger.error("IO Exception Caught! OVF validation Interrupted.");
            if(systemOut)
            System.out.println("Something went bad: caught IOException.");
            ioex.printStackTrace(System.out);
        }
        return -1;
    }
}
