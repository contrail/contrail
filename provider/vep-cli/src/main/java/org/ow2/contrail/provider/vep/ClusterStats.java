/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

/**
 *
 * @author piyush, florian
 */
public class ClusterStats 
{
    public double freeMemory;
    public double usedMemory;
    public double freeCPU;
    public double usedCPU;
    
    public ClusterStats()
    {
        freeMemory = 0.0;
        usedMemory = 0.0;
        freeCPU = 0.0;
        usedCPU = 0.0;
    }
    
    public void reset()
    {
        freeMemory = usedMemory = freeCPU = usedCPU = 0.0;
    }
}
