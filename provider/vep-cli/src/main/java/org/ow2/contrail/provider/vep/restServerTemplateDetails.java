/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Put;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 *
 * @author piyush
 */
public class restServerTemplateDetails extends ServerResource 
{
    private dbHandler db;
    private Logger logger;
    private String dbType;
    private String oneuser;
    private String onepass;
    private String oneip;
    private String oneport;
    private boolean usepdp;
    
    public restServerTemplateDetails()
    {
        logger = Logger.getLogger("VEP.restTemplateDetails");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, VEPHelperMethods.getPropertyFile());
        db = new dbHandler("restServerTemplateDetails", dbType);   
        oneip = VEPHelperMethods.getProperty("one.ip", logger, VEPHelperMethods.getPropertyFile());
        oneport = VEPHelperMethods.getProperty("one.port", logger, VEPHelperMethods.getPropertyFile());
        String pdp = VEPHelperMethods.getProperty("pdp.use", logger, VEPHelperMethods.getPropertyFile());
        if (pdp != null)
            try{
                usepdp = Boolean.parseBoolean(pdp);
            }catch(Exception e){
                usepdp = false;
            }
        else
            usepdp = false;
    }
    
    @Put("json")
    public Representation deployTemplate() throws ResourceException
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        
        String acceptType = requestHeaders.getFirstValue("Accept");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        String templateId = ((String) getRequest().getAttributes().get("id"));
        
        Representation response = null;

        String username = requestHeaders.getFirstValue("X-Username");
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        String[] groups;
        boolean isAdmin = false;
        JSONObject obj = new JSONObject();
        obj.put("title", "VM template deploy action result");
        
        if(username == null)
        {
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            obj.put("error", "CLIENT_ERROR_BAD_REQUEST");
        }
        else
        {
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    oneuser = rs.getString("oneuser");
                    onepass = rs.getString("onepass");
                    rs.close();
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList for user: " + username + " is: " + groupList);
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    rs.close();
                    
                    if(VEPHelperMethods.pdpCheck(uid, null))
                    {
                        rs = db.query("select", "uid", "vmachinetemplate", "where vmid=" + templateId);
                        int vm_uid = -1;
                        if(rs.next())
                        {
                            vm_uid = rs.getInt("uid");
                            rs.close();
                        }
                        if(isAdmin || vm_uid == uid) //if administrator or owner of the ovf
                        {
                            rs = db.query("select", "*", "vmachinetemplate", "where vmid=" + templateId);
                            if(rs.next())
                            {
                                logger.debug("Creating corresponding ONE account for user " + username + " [" + oneuser + ", " + onepass + "].");
                                ONExmlrpcHandler onehandle = new ONExmlrpcHandler(oneip, oneport, oneuser, onepass, "restServerTemplateDetails:deployTemplate()");
                                String vmTemplate = rs.getString("descp");
                                String appName = rs.getString("appname");
                                String ovfid = rs.getString("ovfid");
                                String templateState = rs.getString("state");
                                //vmid is same as templateId
                                rs.close();
                                //now get the disk image list associated with this vm template and if not registered with ONE then try to register it
                                rs = db.query("select", "id, localpath, name, state, oneimgdescp", "diskimage", "where vmid=" + templateId);
                                int counter = 0;
                                while(rs.next())
                                {
                                    counter++;
                                    String imgName = rs.getString("name");
                                    String imgPath = rs.getString("localpath");
                                    String imgState =rs.getString("state");
                                    String imgTemplate = rs.getString("oneimgdescp");
                                    int imgId = rs.getInt("id");
                                    if(!imgState.equalsIgnoreCase("OR")) //if state is ORPHANed then do not do anything, it will be garbage collected soon
                                    {
                                        //getting the already registered ONE image list
                                        LinkedList<ONEImage> imageList = onehandle.getImageList();
                                        boolean found = false;
                                        for(int i=0; i< imageList.size(); i++)
                                        {
                                            ONEImage img = imageList.get(i);
                                            logger.debug("Got ONE Image Details: " + img.imageName + ", " + img.localPath + ", " + img.state);
                                            if(img.imageName.equalsIgnoreCase(imgName) && img.localPath.equalsIgnoreCase(imgPath))
                                            {
                                                //update the diskimage entry with missing details if not already updated
                                                if(!imgState.equalsIgnoreCase("RG"))
                                                {
                                                    boolean status = db.update("diskimage", "oneimgname='" + img.imageName + "', oneimgid=" + img.id + ", state='RG'", "where id=" + imgId);
                                                    if(status)
                                                    {
                                                        logger.debug("For discimage id=" + imgId + " corresponding ONE image with id " + img.id + " was found. VEP-DB updated.");
                                                    }
                                                    else
                                                    {
                                                        logger.warn("For discimage id=" + imgId + " corresponding ONE image with id " + img.id + " was found. VEP-DB update failed.");
                                                    }
                                                }
                                                found = true;
                                                break;
                                            }
                                        }

                                        if(!found)
                                        {
                                            //issue ONE image register command and store corresponding data
                                            //for now we assume that the VM image resides locally.
                                            int oneImgId = onehandle.addImage(imgTemplate);
                                            if(oneImgId != -1)
                                            {
                                                ONEImage temp = onehandle.getImageInfo(oneImgId);
                                                boolean status = db.update("diskimage", "oneimgname='" + temp.imageName + "', oneimgid=" + temp.id + ", state='RG'", "where id=" + imgId);
                                                if(status)
                                                {
                                                    logger.debug("For discimage id=" + imgId + " corresponding ONE image with id " + temp.id + " was registered. VEP-DB updated.");
                                                }
                                                else
                                                {
                                                    logger.warn("For discimage id=" + imgId + " corresponding ONE image with id " + temp.id + " was registered. VEP-DB update failed.");
                                                }
                                            }
                                            else
                                            {
                                                logger.warn("Some error while registering image " + imgName + " disk-id: " + imgId + " with ONE.");
                                                obj.put("error", "SERVER_ERROR_INTERNAL");
                                                this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                            }
                                        }
                                    }
                                    rs = db.query("select", "id, localpath, name, state, oneimgdescp", "diskimage", "where vmid=" + templateId);
                                    for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after updates.
                                }
                                rs.close();
                                //now send start for the vmtemplate
                                if(!templateState.equalsIgnoreCase("OR")) //ignore OR state as it will be garbage collected soon
                                {
                                    int onevmid = onehandle.addVM(vmTemplate + "\nREQUIREMENTS = \"CLUSTER = contrail\"");
                                    if(onevmid != -1)
                                    {
                                        ONEVm temp = onehandle.getVmInfo(onevmid);

                                        //now store in the vmachine table, also change the state of the ovf entry to DP from ND
                                        ResultSet rs1 = db.query("select", "max(id)", "vmachine", "");
                                        int vid = 0;
                                        if(rs1.next()) vid = rs1.getInt(1) + 1;
                                        rs1.close();
                                        boolean status = db.insert("vmachine", "(" + vid + ", " + uid + ", 'OpenNebula', " + temp.id + ", " + templateId + ", '" + temp.name + "', 'DP', -1, '" +
                                                temp.ip + "', '" + temp.graphics_port + "', '" + temp.graphics_ip + "', '')");
                                        obj.put("vm_id", vid);
                                        obj.put("controller", "OpenNebula");
                                        obj.put("iaas_id", temp.id);
                                        obj.put("app_name", appName);
                                        if(status)
                                        {
                                            logger.debug("Successfully deployed VM information - oneId:" + temp.id + " oneName:" + temp.name 
                                                + " Graphics:" + temp.graphics_type + "-" + temp.graphics_ip + "-" + temp.graphics_port);
                                            db.update("vmachinetemplate", "state='DP'", "where vmid=" + templateId);
                                            obj.put("vm_state", "DP");
                                        }
                                        else
                                        {
                                            logger.warn("Failed to store deployed VM information - oneId:" + temp.id + " oneName:" + temp.name 
                                                + " Graphics:" + temp.graphics_type + "-" + temp.graphics_ip + "-" + temp.graphics_port);
                                            db.update("vmachinetemplate", "state='UN'", "where vmid=" + templateId);
                                            obj.put("vm_state", "UN");
                                        }
                                    }
                                    else
                                    {
                                        logger.warn("Some error while deploying VM template " + templateId + ":" + ovfid + " with ONE.");
                                        db.update("vmachinetemplate", "state='ER'", "where vmid=" + templateId);
                                        obj.put("error", "SERVER_ERROR_INTERNAL");
                                        this.setStatus(Status.SERVER_ERROR_INTERNAL);
                                    }
                                }
                                else
                                {
                                    this.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
                                    obj.put("error", "CLIENT_ERROR_UNPROCESSABLE_ENTITY");
                                }
                            }
                            else
                            {
                                //not found
                                rs.close();
                                this.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                                obj.put("error", "CLIENT_ERROR_NOT_FOUND");
                            }
                        }
                        else
                        {
                            this.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                            obj.put("error", "CLIENT_ERROR_UNAUTHORIZED");
                        }
                    }
                    else
                    {
                        this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                        obj.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                        obj.put("origin", "PDP Check denied authorization");
                    }

                }
                else
                {
                    this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                    obj.put("error", "CLIENT_ERROR_PRECONDITION_FAILED");
                }
            }
            catch(Exception ex)
            {
                obj.put("error", "SQL error occured.");
                logger.debug("Exception caught: " + ex.getMessage());
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
            }
        }
        
        
        response = new StringRepresentation(obj.toJSONString(), MediaType.APPLICATION_JSON);
        return response;
    }
}
