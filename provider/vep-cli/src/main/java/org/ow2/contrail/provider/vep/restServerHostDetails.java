/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 *
 * @author piyush
 */
public class restServerHostDetails extends ServerResource {
    private dbHandler db;
    private Logger logger;
    private String dbType;
    
    public restServerHostDetails()
    {
        logger = Logger.getLogger("VEP.restHostDetails");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, VEPHelperMethods.getPropertyFile());
        db = new dbHandler("restServerHostDetails", dbType);   
    }
    
    @Get("json")
    public Representation getValue() throws ResourceException
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        String username = requestHeaders.getFirstValue("X-Username");
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(username);
            else if(acceptType.contains("json"))
                response = toJson(username);
        }
        else
        {
            //default rendering ...
            response = toHtml(username);
        }
        //System.out.println(contentType);
        return response;
    }
    
    public Representation toJson(String username) throws ResourceException
    {
        String hostId = ((String) getRequest().getAttributes().get("id"));
        JSONObject obj = new JSONObject();
        obj.put("title", "Host details for computenode " + hostId);
        try
        {
            ResultSet rs = db.query("select", "*", "computenode", "where cid=" + hostId);
            if(rs.next())
            {
                // read the result set
                int rack_id = rs.getInt("rid");
                obj.put("memory", rs.getInt("mem"));
                obj.put("no_cpu_cores", rs.getInt("cpu"));
                obj.put("rackid", rs.getInt("rid"));
                obj.put("virt", rs.getString("virt"));
                obj.put("hostname", rs.getString("hostname"));
                obj.put("controller", rs.getString("clmgrtype"));
                obj.put("desc", rs.getString("descp"));
                obj.put("vid", rs.getString("vid"));
                obj.put("cpu_freq", rs.getInt("cpufreq"));
                rs.close();
                //locating cluster_id
                rs = db.query("select", "clid", "rack", "where rid=" + rack_id);
                if(rs.next())
                {
                    int cluster_id = rs.getInt("clid");
                    rs.close();
                    rs = db.query("select", "did", "cluster", "where clid=" + cluster_id);
                    if(rs.next())
                    {
                        int datacenter_id = rs.getInt("did");
                        rs.close();
                        rs = db.query("select", "loc", "datacenter", "where did=" + datacenter_id);
                        if(rs.next())
                        {
                            String location = rs.getString("loc");
                            obj.put("country_code", location);
                            rs.close();
                        }
                    }
                }
                //now trying to locate the IaaS id for this host
                rs = db.query("select", "*", "clmanager", "where cid=" + hostId);
                if(rs.next())
                {
                    obj.put("iaas_id", rs.getString("hostid"));
                    rs.close();
                }
                else
                {
                    obj.put("iaas_id", "-1");
                    rs.close();
                }
            }
            else
            {
                obj.put("error", "no data found");
                rs.close();
            }
        }
        catch(Exception ex)
        {
            logger.debug("Exception caught: " + ex.getMessage());
            obj.put("error", "exception caught: " + ex.getMessage());
            this.setStatus(Status.SERVER_ERROR_INTERNAL);
        }
        StringRepresentation value = new StringRepresentation(obj.toJSONString(), MediaType.APPLICATION_JSON);
        return value;
    }
    
    public Representation toHtml(String username) throws ResourceException 
    {   
        String hostId = ((String) getRequest().getAttributes().get("id"));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(false));
        stringBuilder.append("Details on Computenode <i>").append(hostId).append("</i> is shown below<br><br>");
        stringBuilder.append("<table style='width:1014px;hspace:5px;background:silver;border:0px;cellspacing:2px;padding:2px;font-family:Courier;font-size:10pt;color:black;'>");
        stringBuilder.append("<tr>");
        stringBuilder.append("<td valign='top' style='width:128px;'><img src='https://www.cise.ufl.edu/~pharsh/public/terminal.png'>");
        stringBuilder.append("<td valign='top' align='left' bgcolor='white' width='*'>");
        try
        {
            ResultSet rs = db.query("select", "*", "computenode", "where cid=" + hostId);
            if(rs.next())
            {
                // read the result set
                stringBuilder.append("<div style='background:#CCCCFF;'>");
                stringBuilder.append("Memory: ").append(rs.getInt("mem")).append("<br>");
                stringBuilder.append("Number of CPU cores (x100): ").append(rs.getInt("cpu")).append("<br>");
                stringBuilder.append("CPU Frequency (in MHz): ").append(rs.getInt("cpufreq")).append("<br>");
                stringBuilder.append("RackID: ").append(rs.getInt("rid")).append("<br>");
                stringBuilder.append("Virtualization Technology: ").append(rs.getString("virt")).append("<br>");
                stringBuilder.append("Hostname: ").append(rs.getString("hostname")).append("<br>");
                stringBuilder.append("Cloud controller: ").append(rs.getString("clmgrtype")).append("<br>");
                stringBuilder.append("Description: ").append(rs.getString("descp")).append("<br>");
                stringBuilder.append("Virtual Organization ID (-1 means NOT assigned): ").append(rs.getString("vid")).append("<br>");
                stringBuilder.append("</div>");    
            }
            else
            {
                stringBuilder.append("<div style='background:#99CCCC;'>");
                stringBuilder.append("No details found ...");   
                stringBuilder.append("</div>");   
            }
            rs.close();
        }
        catch(Exception ex)
        {
            stringBuilder.append("<B>SQL Query Error!! Details of exception follows ...</B>");
            stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
            stringBuilder.append(ex.getMessage());
            logger.debug("Exception caught: " + ex.getMessage());
            stringBuilder.append("</div>");
        }
        stringBuilder.append("</table><br>");
        stringBuilder.append("Click on the banner image to go up one level<br>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
}
