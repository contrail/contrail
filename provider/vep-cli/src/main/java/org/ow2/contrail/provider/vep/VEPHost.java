/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

/**
 *
 * @author piyush
 */
public class VEPHost {
    public String hostName;
    public String infoDriver;
    public String virtDriver;
    public String storageDriver;
    public String clusterId;
    public String rackId;
    public String location;
    public String interConnect;
    private int statusCode;
    public VEPHost()
    {
        statusCode = -1; //-1 means data not valid
    }
    public void setValidity()
    {
        statusCode = 1; //1 means data is valid
    }
    public void resetValidity()
    {
        statusCode = -1; //1 means data is valid
    }
    public int getValidity()
    {
        return statusCode;
    }
}
