/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.util.LinkedList;
import org.apache.log4j.Logger;
/**
 *
 * @author piyush
 */
public class customLineBuffer 
{
    private int lineCount;
    private LinkedList<String> buffer;
    private Logger logger;
    
    public customLineBuffer(int count)
    {
        lineCount = count;
        buffer = new LinkedList<String>();
        logger = Logger.getLogger("VEP.customLineBuffer");
    }
    
    public boolean add(String val)
    {
        try
        {
            if(lineCount == -1 || buffer.size() < lineCount) //if linecount = -1 then size restrictions dont apply
            {
                buffer.add(val);
            }
            else
            {
                //remove the first element and add to end
                String temp = buffer.remove(0);
                buffer.add(val);
                //logger.trace("Removed log line: " + temp);
            }
            //logger.trace("Added log line: " + val);
            return true;
        }
        catch(Exception ex)
        {
            logger.warn("Exception caught! " + ex.getMessage());
            return false;
        }
    }
    
    public String[] getDump()
    {
        String[] temp = new String[buffer.size()];
        for(int i=0; i<buffer.size(); i++)
            temp[i] = buffer.get(i);
        //logger.debug("Returned " + buffer.size() + " lines of log messages.");
        return temp;
    }
    
}
