==========================================
=== REST Proxy for cluster's head node ===
==========================================

I) DESCRIPTION:

This component provides REST-ful HTTP(S) communication with (OpenNebula) cluster head node. It is implemented
with Jersey (http://http://jersey.java.net) and Tomcat6 as a servlet container.

Currently, there are two available APIs:
* monitoring API
* openNebula API for OVF deployment via URL

For testing purposes, both APIs still support HTTP access.

II) USAGE:

	II.I) MONITORING API:

	Monitoring api implements three @POST methods: hostMetricsMonitorable, vmMetricsMonitorable, startMonitoring and stopMonitoring.
	All APIs also support @GET requests to check the availability of the service (server's TEXT/PLAIN response: "it works")
	
	For XLAB's XC1 cluster, use:
	http://10.30.1.14:8080/rest-monitoring/monitoring/hostMetricsMonitorable
	https://10.30.1.14:8443/rest-monitoring/monitoring/hostMetricsMonitorable
	
	http://10.30.1.14:8080/rest-monitoring/monitoring/vmMetricsMonitorable
	https://10.30.1.14:8443/rest-monitoring/monitoring/vmMetricsMonitorable
	
	http://10.30.1.14:8080/rest-monitoring/monitoring/startMonitoring
	https://10.30.1.14:8443/rest-monitoring/monitoring/startMonitoring
	
	http://10.30.1.14:8080/rest-monitoring/monitoring/stopMonitoring/ovf
	https://10.30.1.14:8443/rest-monitoring/monitoring/stopMonitoring/ovf
	
	The POST parameter metrics is a JSON Array of metric names, which
	can be found below, e.g. ["LOCATION","LOAD_ONE","SAS70_COMPLIANCE"]
	
	The service will return a JSON Array (response type "application/json"), where each metric is either true or
	false, e.g.: {"LOCATION":true,"SAS70_COMPLIANCE":false,"LOAD_ONE":true}
	
	START MONITORING:
	- the method will trigger metric registration on the sensors
	- it is a POST call of type application/json with two possible parameters, vepId is required
	- vepId: ID of the corresponding VEP vm (required)
	- ovfId: ID of the corresponding VirtualSystem in OVF file
	- on success, the method will return HTTP 200 response with a RabbitMq routing key as the request body (e.g. input.vm.ovf.235.#)

	STOP MONITORING:
	- the method will stop AMQP message sending to the corresponding topic, for example: input.vm.ovf.3.# and default back to input.vm.# (the topic before startMonitoring was called)
	- it is a POST call of type "application/json" with a single required parameter:
		- ovfId: ID of the corresponding VirtualSystem in OVF file
	- on success, the method will return HTTP 200 response with a RabbitMq routing key (which has just been deleted) as the request body (e.g. input.vm.ovf.235.#)

	II.II) OPENNEBULA OVF API:

	OpenNebula OVF deployment API supports a single @POST method: deployOvfFromUrl
	It also supports @GET protocol to test if service works (see Monitoring API).
	
	For XLAB's XC1 cluster, use:
	http://10.30.1.14:8080/rest-monitoring/openNebula/deployOvfFromUrl
	https://10.30.1.14:8443/rest-monitoring/openNebula/deployOvfFromUrl
	
	The service accepts URL to OVF file. URL is passed as url-encoded string. URL can also be URI (e.g. path to the file in the local filesystem or
	http URL. OS disk images, referenced in the OVF file, can also be specified in a similar manner - the 
	OVF deployer will automatically download and cache the images in the local directory, specified in the 
	project.properties file. This directory can also be an XtreemFS volume, but bear in mind, that tomcat6 user needs
	READ access to XtreemFS volume.
	
	Simple JAVA client:
	___________________________________________________________________________________________
	
	String urlService = "http://10.30.1.14:8080/rest-monitoring/openNebula/deployOvfFromUrl";
	Client client = Client.create();
	WebResource webResource = client.resource(urlService);
	MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
	String urlOvf = "http://mydomain.com/sample.ovf";
	try {
		formData.add("ovfUrl", URLEncoder.encode(urlOvf, "UTF-8"));
		String response = webResource.type("application/x-www-form-urlencoded").post(String.class, formData);
		System.out.println("Server response for OVF deploy was: " + response);
	} catch (UnsupportedEncodingException e) {
		e.printStackTrace();
	}
	___________________________________________________________________________________________


	II.III) SLA-EXTRACTOR API:

    The sla-extractor API provides methods for processing service level agreement (SLA) and extracting relevant
    information from it. It is intended for use by monitoring component. The main purpose of the sla-extractor is
    to abstract monitoring component from specific SLA implementation and SLA language used. Currently SLA@SOI Year 2
    SLA model is supported.

    Sla-extractor REST API provides following methods:

    1. createMonConf

    Description:
    The createMonConf method parses the given SLA document, extracts relevant information and generates monitoring
    configuration that shows agreement terms, metrics to monitor and their constraints.

    * Usage:
    POST http://<server>:<port>/rest-monitoring/sla/slaextractor/createMonConf

    * Request headers:
    Content-Type: text/xml

    * Request body:
    SLA document in XML format

    * Response:
    Monitoring configuration in XML format

    * Example:
    Sample input SLA document: svn+ssh://svn.forge.objectweb.org/
       svnroot/contrail/trunk/resource/rest-monitoring/docs/samples/ubuntu-test-xlab-SLA.xml

    Request:
    curl -X POST --header "Content-Type: text/xml" --data "@ubuntu-test-xlab-SLA.xml"
       http://10.30.1.14:8080/rest-monitoring/sla/slaextractor/createMonConf

    Sample response: svn+ssh://svn.forge.objectweb.org/svnroot/
       contrail/trunk/resource/rest-monitoring/docs/samples/monitoring-configuration.xml

    2. getOVFs

    * Description
    The getOVFs method parses the given SLA document and extracts all OVF files referenced by the SLA.

    * Usage:
    POST http://<server>:<port>/rest-monitoring/sla/slaextractor/getOVFs

    * Request headers:
    Content-Type: text/xml

    * Request body:
    SLA document in XML format

    * Response:
    List of URLs of OVF files in JSON format

    * Example:
    Sample input SLA document: svn+ssh://svn.forge.objectweb.org/
       svnroot/contrail/trunk/resource/rest-monitoring/docs/samples/ubuntu-test-xlab-SLA.xml

    Request:
    curl -X POST --header "Content-Type: text/xml" --data "@ubuntu-test-xlab-SLA.xml"
       http://10.30.1.14:8080/rest-monitoring/sla/slaextractor/getOVFs

    Sample response:
    [
       "http://contrail.xlab.si/test-files/ubuntu-test-xlab.ovf
    ]
	___________________________________________________________________________________________


DEPLOYMENT:

For Tomcat installation, war deployment and certificate setup, please use the following guide:
/svnroot/contrail/trunk/federation/src/federation-api/src/site/apt/tomcat-deployment.apt
