package org.ow2.contrail.common.headNodeRestProxy.sla;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

import static junit.framework.Assert.*;

public class SLAExtractorResourceTest {
    private static Logger log = Logger.getLogger(SLAExtractorResourceTest.class);

    @Test
    public void testCreateMonConf() throws Exception {
        log.info("testCreateMonConf() started.");
        SLAExtractorResource resource = new SLAExtractorResource();
        String slaDocument = readFile("/lamp-SLA.xml");
        Response response = resource.createMonConf(slaDocument);
        assertEquals(response.getStatus(), 200);
        String monConf = (String) response.getEntity();
        assertNotNull(monConf);
        log.info("testCreateMonConf() finished successfully.");
    }

    @Test
    public void testCreateMonConf1() throws Exception {
        // test with invalid SLA
        log.info("testCreateMonConf1() started.");
        SLAExtractorResource resource = new SLAExtractorResource();
        String slaDocument = "Invalid";
        try {
            Response response = resource.createMonConf(slaDocument);
            fail();
        }
        catch (Exception e) {
            assertEquals(e.getClass(), WebApplicationException.class);
            assertEquals(((WebApplicationException) e).getResponse().getStatus(), 400);
        }
        log.info("testCreateMonConf1() finished successfully.");
    }

    @Test
    public void testGetOVFs() throws Exception {
        log.info("testGetOVFs() started.");
        SLAExtractorResource resource = new SLAExtractorResource();
        String slaDocument = readFile("/lamp-SLA.xml");
        Response response = resource.getOVFs(slaDocument);
        assertEquals(response.getStatus(), 200);

        Gson gson = new Gson();
        Type ovfListType = new TypeToken<List<String>>() {
        }.getType();
        List<String> ovfList = gson.fromJson(response.getEntity().toString(), ovfListType);
        assertEquals(ovfList.size(), 1);

        log.info("testGetOVFs() finished successfully.");
    }

    private String readFile(String fileName) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(
                SLAExtractorResourceTest.class.getResourceAsStream(fileName)));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            builder.append(line).append("\n");
        }
        r.close();
        return builder.toString();
    }
}
