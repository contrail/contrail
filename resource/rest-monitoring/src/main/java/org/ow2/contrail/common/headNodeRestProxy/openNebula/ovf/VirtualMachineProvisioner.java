package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf;

import java.util.ArrayList;

import org.opennebula.client.Client;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.VM;

public class VirtualMachineProvisioner {
	private Client oneClient;
	private String ovfFile;
	
	public static final String COMMON_VM_TEMPLATE = "NAME = \"vmNAME\" CPU = vmCAP VCPU = vmVCPU MEMORY = vmMEM\n" +
			"OS = [ boot=\"hd\", arch=\"x86_64\" ]\n" +
			"DISK   = [\n" + 
			          "source   = \"vmDISKOS\",\n" + 
			          "target   = \"vda\",\n" + 
			          "readonly = \"no\",\n" +
			          "DEV_PREFIX = vd,\n" +
			          "DRIVER = qcow2,\n" +
			          "BUS = virtio\n" +
			        "]\n" + 
			// Used for referencing images in repository
			//"DISK = [\n" +
			//	"image = \"vmDISKOS\"" +NFO  (org.ow2.contrail.common.open
			//"]\n" +
			"NIC = [\n" +
				"NETWORK = \"vmNETWORK\"\n" +
			"]\n" +
			"GRAPHICS = [\n" +
				"type=\"vnc\",\n" +
				"listen=\"localhost\"\n" +
			"]\n";
	
	public VirtualMachineProvisioner(Client oneClient, String ovfFile) {
//		log.debug("OVF FILE: " + ovfFile);
		this.setOneClient(oneClient);
		this.setOvfFile(ovfFile);
	}
	
	public Client getOneClient() {
		return oneClient;
	}

	public void setOneClient(Client oneClient) {
		this.oneClient = oneClient;
	}

	public String getOvfFile() {
		return ovfFile;
	}

	public void setOvfFile(String ovfFile) {
		this.ovfFile = ovfFile;
	}

	public String provision() {
		SimpleOvfParser parser = new SimpleOvfParser(this.ovfFile);
		ArrayList<VM> vms = parser.getVms();
		String error = null;
		for(VM vm : vms) {
			error = vm.provision(this.oneClient);
			if(error != null)
				break;
		}
		return error;
	}
}
