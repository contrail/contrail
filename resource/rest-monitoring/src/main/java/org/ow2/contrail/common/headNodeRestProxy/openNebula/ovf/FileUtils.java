package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class FileUtils {
	private static final int BUFFER_LEN_STRING = 8192;
	
	public static String fileToString(String path) throws FileNotFoundException {
		StringBuilder text = new StringBuilder();
		Scanner scanner = new Scanner(new FileInputStream(path), "UTF-8");
		try {
			while (scanner.hasNextLine()){
				text.append(scanner.nextLine());
			}
		}
		finally {
			scanner.close();
		}
		return text.toString();
	}

	public static boolean isLocalFile(String path) {
		if(
				path.startsWith("http://")
				|| path.startsWith("https://")
				|| path.startsWith("ftp://")
		)
			return false;
		return true;
	}

	public static String cache(String href, String cacheDir) throws MalformedURLException, IOException {
		Logger log = Logger.getLogger(FileUtils.class);
		log.debug("Caching of file " + href + " started...");
		if(isLocalFile(href))
			return href;
		if(!isHttpUrl(href)) {
			log.error("Only HTTP connections are supported");
			return null;
		}
        URL url = new URL(href);
        // XXX: Support https and / or FTP
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        uc.setConnectTimeout(1000);
        int length = uc.getContentLength();
        log.debug("Content lenght: " + length);
        uc.setReadTimeout(1000);
        InputStream is = uc.getInputStream();
        String cachePath = cacheDir + "/";
        log.debug("Cache path: " + cachePath);
        File p = new File(cachePath);
        if(!p.exists() || !p.isDirectory()) {
        	log.error("Directory doesn't exits: " + cachePath);
        	return null;
        }
        if(!p.canRead()) {
        	log.error("Directory " + cachePath + " not readable");
        	return null;
        }
        if(!p.canWrite()) {
        	log.error("Directory " + cachePath + " not writable");
        	return null;
        }
        
        String[] parts = href.split("/");
        String fileName = parts[parts.length-1];
        log.debug("Filename: " + fileName);
        //fileNameParts = parts[parts.length-1].split(".");
        String[] fileNameParts = fileName.split("\\.");
        String cachedFile = "";
        int i = 0;
        boolean first = true;
        for(String s : fileNameParts) {
        	if(first)
        		first = false;
        	else
        		cachedFile += ".";
            cachedFile += s;
        	if(i++ == fileNameParts.length - 2) {
        		cachedFile += "_" + length;
        	}
        }
        log.debug("Cached file:" + cachedFile);
        File f = new File(cachePath, cachedFile);
        if(f.exists()) {
        	log.info("File already cached");
        	return cachePath + cachedFile;
        }
        FileOutputStream fs = new FileOutputStream(f);
        byte[] buffer = new byte[BUFFER_LEN_STRING];
        int len = 0;
        log.info("Downloading image file ... this might take a while.");
        while ((len = is.read(buffer)) > 0) {
            fs.write(buffer, 0, len);
        }
        is.close();
        log.info("Download finished.");
        return cachePath + cachedFile;
	}

	private static boolean isHttpUrl(String href) {
		return href.startsWith("http://") ? true : false;
	}
	
}
