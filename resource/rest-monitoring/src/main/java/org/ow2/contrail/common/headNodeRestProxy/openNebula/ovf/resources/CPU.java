package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources;

public class CPU extends Resource {
	private int numVirtualCpus;

	public CPU() {
		this.setDescription("Number of virtual CPUs");
	}
	
	public int getNumVirtualCpus() {
		return numVirtualCpus;
	}

	public void setNumVirtualCpus(int numVirtualCpus) {
		this.numVirtualCpus = numVirtualCpus;
	}
	
}