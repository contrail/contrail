package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources;

public class VmDisk {
	public enum Type {OS, DATABLOCK, SWAP};
	private Type type;
	private String ovfId;
	private VmFile vmFile;
	private String repositoryImage;
	private boolean readOnly;
	private boolean save;
	private String target;

	// Used when the image referenced in the OVF document is not already in OpenNebula repository
	public VmDisk(String ovfId, VmFile vmFile) {
		this.setOvfId(ovfId);
		this.setVmFile(vmFile);
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getRepositoryImage() {
		return repositoryImage;
	}

	public void setRepositoryImage(String repositoryImage) {
		this.repositoryImage = repositoryImage;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public boolean isSave() {
		return save;
	}

	public void setSave(boolean save) {
		this.save = save;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public VmFile getVmFile() {
		return vmFile;
	}

	public void setVmFile(VmFile vmFile) {
		this.vmFile = vmFile;
	}

	public String getOvfId() {
		return ovfId;
	}

	public void setOvfId(String ovfId) {
		this.ovfId = ovfId;
	}

	public boolean resourceIdEquals(String diskImageRef) {
		if(diskImageRef.startsWith("/disk/")) {
			return diskImageRef.substring("/disk/".length()).equalsIgnoreCase(this.ovfId) ? true : false;
		}
		return false;
	}
}
