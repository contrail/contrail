package org.ow2.contrail.common.headNodeRestProxy.monitoring;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

@Path("/vmMetricsMonitorable")
public class vmMetricsMonitorable {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String Test()  {
		return "it works";
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response vmMonitoringStatus(String jsonArray)  {
		if(jsonArray.isEmpty())
			return Response.status(400).entity("Parameters required").build();
		
		XmlRpcClient client = ProxyXmlRpcClient.getInstance();
		if(client == null)
			return Response.status(500).build();
		
		try {
			String res = (String) client.execute(
					"monitoring.vmMetricsMonitorable",
					new Object[]{new String(jsonArray.toString())}
					);
			return Response.status(200).entity(res).build();
		} catch (XmlRpcException e) {
			e.printStackTrace();
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}
	}
}