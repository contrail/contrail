package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources;


import java.util.List;

import org.apache.log4j.Logger;
import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.vm.VirtualMachine;
//import org.ow2.contrail.common.deployers.opennebula.ApplicationProvisioner;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.OpenNebulaDeployer;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.VirtualMachineProvisioner;


public class VM {
	private Float vmCap; // cap CPU to this value (0:1)
	private String name;
	private int vCpus = 1;
	private VmDisk disk;
	private List<String> dataDisks;
	private String memory;
	private String info;
	private String hostname;
	private String id; // Internal opennebula ID
	private VirtualMachine oneVm; // OpenNebula virtual machine representation
	private List<VmNetwork> vmNetworks;
	
	private static final String VM_NETWORK = "private-lan";
	private static Logger log = Logger.getLogger(OpenNebulaDeployer.class);
	
	public VM(VmDisk disk, int vCpus, String memory, String name) {
		this.disk = disk;
		this.vCpus = vCpus;
		this.memory = memory;
		this.name = name;
		// XXX: Might become part of the parser's user's properties
		this.vmCap = (float) 1;
//		this.setVmNetworks(networks);
	}
	
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String provision(Client oneClient) {
		log.info("Provisioning VM " + this.name);
		if(this.vCpus > 0 && this.disk != null && this.memory != null && this.name != null) {
			if(this.disk.getVmFile().getCache() != null) {
				String tplDisk = String.valueOf(this.disk.getVmFile().getCache());
				try {
					OneResponse rc = VirtualMachine.allocate(
						oneClient,
						VirtualMachineProvisioner.COMMON_VM_TEMPLATE
							.replaceAll("vmNAME", this.name)
							.replaceAll("vmCAP", String.valueOf(this.vmCap))
							.replaceAll("vmVCPU", String.valueOf(this.vCpus))
							.replaceAll("vmMEM", String.valueOf(this.memory))
							.replaceAll("vmDISKOS", tplDisk)
							.replaceAll("vmNETWORK", VM_NETWORK)
					);
					if(rc.isError()) {
						log.error(rc.getErrorMessage());
						return rc.getErrorMessage();
					}
					setId(rc.getMessage());
					return null;
				} catch (Exception e) {
					e.printStackTrace();
				}			
			}
			else {
				log.error("Problems getting VM disk cache");
				return "Problems getting VM disk cache";
			}
		}
		return "Cannot set values from OVF";
	}

	
	public void setName(String name) {
		this.name = name;
	}

	public String getHostname() {
		if(hostname == null && this.oneVm != null) {
			OneVm oneVm = new OneVm(this.oneVm);
			String ip = oneVm.getVmIpAddress();
			hostname = "vm-" + ip.replace(".", "-");
		}
		return hostname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public VirtualMachine getOneVm() {
		return oneVm;
	}

	public void setOneVm(VirtualMachine vm) {
		this.oneVm = vm;
	}

	public void shutdown() {
		this.oneVm.shutdown();		
	}

	public List<VmNetwork> getVmNetworks() {
		return vmNetworks;
	}

	public void setVmNetworks(List<VmNetwork> vmNetworks) {
		this.vmNetworks = vmNetworks;
	}

	public List<String> getDataDisks() {
		return dataDisks;
	}

	public void setDataDisks(List<String> dataDisks) {
		this.dataDisks = dataDisks;
	}


}
