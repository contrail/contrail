package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.apache.xml.dtm.ref.DTMNodeList;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.CPU;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.Memory;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.VM;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.VmDisk;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.VmFile;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.VmDisk.Type;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class SimpleOvfParser {
	private static final String RESOURCE_TYPE_CPU = "3";
	private static final String RESOURCE_TYPE_MEM = "4";
	private static final String RESOURCE_TYPE_DISK = "17";
	private ArrayList<VmFile> files = null;
	private ArrayList<VmDisk> disks = null;

	private static Logger log = Logger.getLogger(SimpleOvfParser.class);
	
	private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	private DocumentBuilder db;
	private Node docEle = null;
	private XPathFactory factory = XPathFactory.newInstance();
    private XPath xpath = factory.newXPath();
    
    public SimpleOvfParser(String ovf) {
    	if(ovf != null && !ovf.isEmpty()) {
    		try {
    			db = dbf.newDocumentBuilder();
    			Document dom = db.parse(new ByteArrayInputStream(ovf.getBytes()));
    			this.docEle = dom.getDocumentElement();
    		} catch (ParserConfigurationException e) {
    			e.printStackTrace();
    		} catch (SAXException e) {
    			e.printStackTrace();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
   }
    
	public ArrayList<VM> getVms() {
		if(this.docEle == null)
			return null;
		try {
			disks = new ArrayList<VmDisk>();
			files = new ArrayList<VmFile>();
			DTMNodeList fileNodes = (DTMNodeList) xpath.evaluate("References/File", this.docEle, XPathConstants.NODESET);
			for(int i=0; i<fileNodes.getLength(); i++) {
				Node node = fileNodes.item(i);
				NamedNodeMap attr = node.getAttributes();
				
				files.add(new VmFile(
							attr.getNamedItem("ovf:id").getNodeValue(),
							attr.getNamedItem("ovf:href").getNodeValue(),
							Integer.parseInt(attr.getNamedItem("ovf:size").getNodeValue())
							)
				);
			}
			
			DTMNodeList diskNodes = (DTMNodeList) xpath.evaluate("DiskSection/Disk", this.docEle, XPathConstants.NODESET);
			
			if(diskNodes.getLength() > fileNodes.getLength())
				throw new Exception("List of disk longer than list of files?");
			
			for(int i=0; i<diskNodes.getLength(); i++) {
				Node node = diskNodes.item(i);
				NamedNodeMap attr = node.getAttributes();
				
				// Check if reference for the file referenced by disk exists
				String fileId = attr.getNamedItem("ovf:fileRef").getNodeValue();
				VmFile currVmFile = null;
				for(VmFile f : files) {
					if(f.getId().equalsIgnoreCase(fileId)) {
						currVmFile = f;
						break;
					}
				}
				if(currVmFile == null)
					throw new Exception("File reference for " + fileId + " does not exist");
				
				disks.add(new VmDisk(attr.getNamedItem("ovf:diskId").getNodeValue(), currVmFile));
			}
			
			DTMNodeList nodes = (DTMNodeList) xpath.evaluate("VirtualSystem", this.docEle, XPathConstants.NODESET);
			ArrayList<VM> vms = new ArrayList<VM>();
			for(int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				
				String name = null;
				Node nodeName = (Node) xpath.evaluate("Name", node, XPathConstants.NODE);
				name = nodeName.getTextContent();
				
				DTMNodeList virtualHardware = (DTMNodeList) xpath.evaluate("VirtualHardwareSection/Item", node, XPathConstants.NODESET);
				CPU c = null;
				Memory m = null;
				VmDisk os = null;
				log.info("Provisioning new virtual machine named \"" + name + "\"");
				for(int j=0; j<virtualHardware.getLength(); j++) {
					Node item = virtualHardware.item(j);
					Node rtype = (Node) xpath.evaluate("ResourceType", item, XPathConstants.NODE);
					if(rtype.getTextContent().equalsIgnoreCase(RESOURCE_TYPE_CPU)) {
						c = new CPU();
						Node instanceId = (Node) xpath.evaluate("InstanceID", item, XPathConstants.NODE);
						c.setId(Integer.parseInt(instanceId.getTextContent()));
						Node elementName = (Node) xpath.evaluate("ElementName", item, XPathConstants.NODE);
						c.setName(elementName.getTextContent());
						Node virtualQuantity = (Node) xpath.evaluate("VirtualQuantity", item, XPathConstants.NODE);
						c.setNumVirtualCpus(Integer.parseInt(virtualQuantity.getTextContent()));
						log.debug("Setting number of VCPUs to " + virtualQuantity.getTextContent());
					}
					else if(rtype.getTextContent().equalsIgnoreCase(RESOURCE_TYPE_MEM)) {
						m = new Memory();
						Node instanceId = (Node) xpath.evaluate("InstanceID", item, XPathConstants.NODE);
						m.setId(Integer.parseInt(instanceId.getTextContent()));
						Node elementName = (Node) xpath.evaluate("ElementName", item, XPathConstants.NODE);
						m.setName(elementName.getTextContent());
						Node virtualQuantity = (Node) xpath.evaluate("VirtualQuantity", item, XPathConstants.NODE);
						m.setSize(Integer.parseInt(virtualQuantity.getTextContent()));
						System.out.println("Setting memory to " + virtualQuantity.getTextContent());
					}
					// XXX: Only first disk is OS disk
					else if(rtype.getTextContent().equalsIgnoreCase(RESOURCE_TYPE_DISK)) {
						Node hostResource = (Node) xpath.evaluate("HostResource", item, XPathConstants.NODE);
						System.out.println("Setting disk to " + hostResource.getTextContent());
						String diskImageRef = hostResource.getTextContent();
						for(VmDisk disk : disks) {
							if(disk.resourceIdEquals(diskImageRef)) {
								os = disk;
								os.setType(Type.OS);
								break;
							}
						}
					}
				}
				if(name != null && os != null && c != null && m != null) {
					vms.add(new VM(os, c.getNumVirtualCpus(), m.getSizeInMegabytes(), name));
				}
				else
					log.error("Could not provision VM " + name + " + bad data");
			}
			return vms;

		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
