package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.OpenNebulaUser;

public class OpenNebulaDeployer {
	private static final String PROPS_FILE = "/etc/contrail/contrail-rest-monitoring/rest-monitoring.config";
	
	private static Logger log = Logger.getLogger(OpenNebulaDeployer.class);
	private static OpenNebulaConnector nebula = null;
	
	private static OpenNebulaUser oneUser;
	private static String adminUsername;
    private static String adminPassword;
    private static String rpcHost;
    private static String rpcPort;
    private static String imageCacheDir;
    private static String ovfCacheDir;
    
    public OpenNebulaDeployer() {
    	Properties properties = new Properties();
		FileInputStream props = null;
		try {
			props = new FileInputStream(PROPS_FILE);
		} catch (FileNotFoundException e) {
			System.out.println("Config file not found at " + PROPS_FILE);
		}
		try {
			properties.load(props);
			rpcHost = properties.getProperty("one_rpc_host");
	        rpcPort = properties.getProperty("one_rpc_port");
	        adminUsername = properties.getProperty("one_admin_username");
	        adminPassword = properties.getProperty("one_admin_password");
	        imageCacheDir = properties.getProperty("image_cache_dir");
	        ovfCacheDir = properties.getProperty("ovf_cache_dir");
	        nebula = OpenNebulaConnector.getInstance();
	        
	        setUser(new OpenNebulaUser(adminUsername, adminPassword));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public void setUser(OpenNebulaUser user) {
    	oneUser = user;
    	try {
			if(nebula.maybeCreateUser(user))
				log.info("New OpenNebula user " + oneUser.getUsername() + " created successfully");
		} catch (Exception e) {
			if(e.getLocalizedMessage().equalsIgnoreCase("[UserAllocate] Error trying to CREATE USER Returned error code [-1].. Reason: Existing user, cannot duplicate."))
				log.info("OpenNebula user exists");
			else
				log.error(e.getLocalizedMessage());
		}
    }

    public String deployOvfFile(String vmTemplateUrl) {
    	String error = null;
		if(oneUser == null) {
    		error = "OpenNebula user not set - exiting";
			log.error(error);
    	}
		else {
			try {
				String filePath;;
		    	if(!FileUtils.isLocalFile(vmTemplateUrl)) {
		    		filePath = FileUtils.cache(vmTemplateUrl, ovfCacheDir);
		    		log.info("Provisioning remote OVF file");
		    	}
		    	else {
		    		filePath = vmTemplateUrl;
		    		log.info("Provisioning local OVF file");
		    	}
	        	
				VirtualMachineProvisioner ap = new VirtualMachineProvisioner(nebula.getAdminClient(), FileUtils.fileToString(filePath));
	        	return ap.provision();
	        		
	        } catch (MalformedURLException e) {
				error = e.getMessage();
	        	log.error(error);
			} catch (IOException e) {
				error = e.getMessage();
	        	log.error(error);
			}
		}
		return error;
    }

	public static String getAdminUsername() {
		return adminUsername;
	}

	public static String getAdminPassword() {
		return adminPassword;
	}

	public static String getRpcHost() {
		return rpcHost;
	}

	public static String getRpcPort() {
		return rpcPort;
	}

	public static String getImageCacheDir() {
		return imageCacheDir;
	}

	public static void setImageCacheDir(String imageCacheDir) {
		OpenNebulaDeployer.imageCacheDir = imageCacheDir;
	}
}