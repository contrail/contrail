package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.FileUtils;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.OpenNebulaDeployer;

public class VmFile {
	private String id;
	private String href;
	private String cachedFile;
	private int size;
	
	private static Logger log = Logger.getLogger(VmFile.class);
	
	public String getCache() {
		if(FileUtils.isLocalFile(this.href)) {
			log.info("Not caching - local file.");
			setCachedFile(this.href);
		}
		else {
			log.info("Caching - remote file.");
			try {
				setCachedFile(FileUtils.cache(this.href, OpenNebulaDeployer.getImageCacheDir()));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(getCachedFile() != null) {
			File f = new File(getCachedFile());
			if(f.exists()) {
				if(f.length() == this.size) {
					return this.getCachedFile();
				}
				log.warn("Bad file size; OVF size: " + this.size + ", actual size: " + f.length());
			}
			else
				log.warn("File " + this.getCachedFile() + " does not exist in the local filesystem");
		}
		else 
			log.error("There was an error caching the remote file");
		
		return null;
	}
	
	public VmFile(String id, String href, int size) {
		this.setId(id);
		this.setHref(href);
		this.setSize(size);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	private String getCachedFile() {
		return cachedFile;
	}

	private void setCachedFile(String cachedFile) {
		this.cachedFile = cachedFile;
	}
}
