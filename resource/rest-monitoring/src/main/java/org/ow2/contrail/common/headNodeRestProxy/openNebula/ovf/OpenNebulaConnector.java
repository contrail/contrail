package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf;

import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.user.User;
import org.opennebula.client.user.UserPool;
import org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources.OpenNebulaUser;

public class OpenNebulaConnector {
	private static Client oneClient;
	private static OpenNebulaConnector oneConnector;
    
    private OpenNebulaConnector() {
    	try {
			oneClient = new Client(OpenNebulaDeployer.getAdminUsername() + ":" + OpenNebulaDeployer.getAdminPassword(), "http://" + OpenNebulaDeployer.getRpcHost() + ":" + OpenNebulaDeployer.getRpcPort() + "/RPC2");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
    
    public static synchronized OpenNebulaConnector getInstance() {
    	if(oneConnector == null)
			oneConnector = new OpenNebulaConnector();
			
    	return oneConnector;
    }
    
    public Client getAdminClient() {
    	return oneClient;
    }

	public boolean maybeCreateUser(OpenNebulaUser user) throws Exception {
		UserPool users = new UserPool(oneClient);
		for (User u : users) {
			if (u.getName().equalsIgnoreCase(user.getUsername()))
				return false;
		}
		//XXX: Real passwords
		OneResponse res = User.allocate(oneClient, user.getUsername(), "7bc8559a8fe509e680562b85c337f170956fcb06");
		if(res.getErrorMessage().isEmpty())
			return true;
		
		throw new Exception(res.getErrorMessage());
	}
}
