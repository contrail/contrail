package org.ow2.contrail.resource.monitoringnotifications;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class MongoDBNotificationServiceTest {

    @Before
    public void setUp() throws Exception {
        NotificationServiceFactory.createMongoDBNotificationService("src/test/resources/mongo-conf.properties");
    }

    @Test
    public void testNotify() {
        INotificationService notificationService = NotificationServiceFactory.getNotificationService();

        EventSource eventSource = new EventSource();
        eventSource.setApplicationUuid("testApp");
        eventSource.setHost("testHost");
        eventSource.setOneId("testOne");
        eventSource.setSlaId("testSla");
        eventSource.setUserUuid("testUser");
        eventSource.setVepId("testVep");

        notificationService.notify(eventSource, ActionType.START);
        notificationService.notify(eventSource, ActionType.STOP);
    }
}
