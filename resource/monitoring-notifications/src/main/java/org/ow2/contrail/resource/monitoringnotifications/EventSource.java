package org.ow2.contrail.resource.monitoringnotifications;

public class EventSource {
    private String host;
    private String oneId;
    private String vepId;
    private String slaId;
    private String userUuid;
    private String applicationUuid;

    public EventSource() {
    }

    public EventSource(String host, String oneId, String vepId, String slaId, String userUuid, String applicationUuid) {
        this.host = host;
        this.oneId = oneId;
        this.vepId = vepId;
        this.slaId = slaId;
        this.userUuid = userUuid;
        this.applicationUuid = applicationUuid;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getOneId() {
        return oneId;
    }

    public void setOneId(String oneId) {
        this.oneId = oneId;
    }

    public String getVepId() {
        return vepId;
    }

    public void setVepId(String vepId) {
        this.vepId = vepId;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getApplicationUuid() {
        return applicationUuid;
    }

    public void setApplicationUuid(String applicationUuid) {
        this.applicationUuid = applicationUuid;
    }
}
