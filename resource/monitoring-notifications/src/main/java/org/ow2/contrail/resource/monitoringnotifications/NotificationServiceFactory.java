package org.ow2.contrail.resource.monitoringnotifications;

import java.io.FileInputStream;
import java.util.Properties;

public class NotificationServiceFactory {
    private static INotificationService notificationService;

    public static void createMongoDBNotificationService(String confFilePath) throws Exception {
        Properties props = new Properties();
        props.load(new FileInputStream(confFilePath));
        createMongoDBNotificationService(props);
    }

    public static void createMongoDBNotificationService(Properties props) throws Exception {
        if (notificationService == null) {
            notificationService = new MongoDBNotificationService(props);
        }
        else {
            throw new Exception("Only one NotificationService is allowed.");
        }
    }

    public static INotificationService getNotificationService() {
        return notificationService;
    }
}
