package org.ow2.contrail.resource.monitoringnotifications;

public enum ActionType {
    START,
    STOP,
    PAUSE,
    RESUME
}
