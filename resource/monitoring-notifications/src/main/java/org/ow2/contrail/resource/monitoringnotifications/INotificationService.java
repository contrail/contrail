package org.ow2.contrail.resource.monitoringnotifications;

public interface INotificationService {

    public void notify(EventSource eventSource, ActionType action);

}
