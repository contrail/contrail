# This script checks if the OpenNebula monitor is running and starts the monitor otherwise
# This script should be installed as a cron job: */5 * * * * /CONTRAIL_DIR/cron.sh (check if the monitor is running every 5 minutes)
#
# @author Gregor Beslic gregor.beslic@cloud.si
#
#!/bin/bash

ps -ef | grep one-monitor.jar | grep -v grep > m_running
if [ ! -s m_running ] ; then
  java -jar one-monitor.jar &
fi
rm m_running
