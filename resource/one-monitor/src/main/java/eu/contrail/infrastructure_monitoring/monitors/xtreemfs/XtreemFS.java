package eu.contrail.infrastructure_monitoring.monitors.xtreemfs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import eu.contrail.infrastructure_monitoring.enums.DataType;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.data.XtreemFSData;
import eu.contrail.infrastructure_monitoring.monitors.xtreemfs.XtreemFSRawMetricList.XtreemFSRawMetric;

public class XtreemFS {
	private XtreemFSSNMPMonitor xtreemFSMonitor;
	private boolean hasData;
	private List<XtreemFSData> servicesMetrics;
    private static Logger log = Logger.getLogger(XtreemFS.class);
	
	private XtreemFSRawMetricList getServiceMetrics(String type) {
		XtreemFSRawMetricList xfsMetrics = new XtreemFSRawMetricList(type);
		xfsMetrics.add("1.3.6.1.4.1.38350.1.13.0", RawMetric.XTREEMS_SNMP_SERVICE_UUID, DataType.STRING, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.1.1.0", RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_USED, DataType.INTEGER, "B");
		xfsMetrics.add("1.3.6.1.4.1.38350.1.2.0", RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_MAX, DataType.INTEGER, "B");
		xfsMetrics.add("1.3.6.1.4.1.38350.1.3.0", RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_FREE, DataType.INTEGER, "B");
		xfsMetrics.add("1.3.6.1.4.1.38350.1.8.0", RawMetric.XTREEMS_SNMP_SERVICE_CLIENT_CONN, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.1.9.0", RawMetric.XTREEMS_SNMP_SERVICE_PENDING_REQ, DataType.INTEGER, "");
		return xfsMetrics;
	}
	
	private void getDirMetrics() {
		XtreemFSRawMetricList xfsMetrics = getServiceMetrics("DIR");
		xfsMetrics.add("1.3.6.1.4.1.38350.2.1.0", RawMetric.XTREEMS_SNMP_DIR_AMC, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.2.2.0", RawMetric.XTREEMS_SNMP_DIR_SC, DataType.INTEGER, "");
		getMetrics(xfsMetrics, XtreemFSMonitoringEngine.XTREEMFS_SNMP_PORT_DIR);
	}
	
	private void getMrcMetrics() {
		XtreemFSRawMetricList xfsMetrics = getServiceMetrics("MRC");
		xfsMetrics.add("1.3.6.1.4.1.38350.3.1.0", RawMetric.XTREEMS_SNMP_MRC_VC, DataType.INTEGER, "");
		getMetrics(xfsMetrics, XtreemFSMonitoringEngine.XTREEMFS_SNMP_PORT_MRC);
	}
	
	private void getOsdMetrics() {
		XtreemFSRawMetricList xfsMetrics = getServiceMetrics("OSD");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.1.0", RawMetric.XTREEMS_SNMP_OSD_OBJ_RX, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.2.0", RawMetric.XTREEMS_SNMP_OSD_REP_OBJ_RX, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.3.0", RawMetric.XTREEMS_SNMP_OSD_OBJ_TX, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.4.0", RawMetric.XTREEMS_SNMP_OSD_REP_BYTES_RX, DataType.INTEGER, "B");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.5.0", RawMetric.XTREEMS_SNMP_OSD_BYTES_RX, DataType.INTEGER, "B");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.6.0", RawMetric.XTREEMS_SNMP_OSD_BYTES_TX, DataType.INTEGER, "B");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.7.0", RawMetric.XTREEMS_SNMP_OSD_QUEUE_PRE, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.8.0", RawMetric.XTREEMS_SNMP_OSD_QUEUE_STOR, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.9.0", RawMetric.XTREEMS_SNMP_OSD_QUEUE_DEL, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.10.0", RawMetric.XTREEMS_SNMP_OSD_FILES_OPEN, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.11.0", RawMetric.XTREEMS_SNMP_OSD_FILES_DELETED, DataType.INTEGER, "");
		xfsMetrics.add("1.3.6.1.4.1.38350.4.12.0", RawMetric.XTREEMS_SNMP_OSD_FREE_SPACE, DataType.INTEGER, "B");
		getMetrics(xfsMetrics, XtreemFSMonitoringEngine.XTREEMFS_SNMP_PORT_OSD);
	}
	
	public XtreemFS() {
		servicesMetrics = new ArrayList<XtreemFSData>();
		hasData = false;
		getDirMetrics();
		getMrcMetrics();
		getOsdMetrics();
		hasData = true;
	}
	
	private void getMetrics(XtreemFSRawMetricList xfsMetrics, String port) {
		xtreemFSMonitor = new XtreemFSSNMPMonitor(
			XtreemFSMonitoringEngine.XTREEMFS_SNMP_HOST,
			port,
			XtreemFSMonitoringEngine.xTREEMFS_SNMP_COMMUNITY,
			xfsMetrics
		);
		
		try {
			xfsMetrics = xtreemFSMonitor.getData();
			if(xfsMetrics != null) {
				XtreemFSData data = new XtreemFSData();
				data.setServiceType(xfsMetrics.getMetricType());
				for(XtreemFSRawMetric xm : xfsMetrics.getMetrics()) {
					data.putMetricData(new MetricData(xm.getRawMetric(), xm.getValue(), xm.getDataType(), xm.getUnit()));
				}
				this.servicesMetrics.add(data);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean hasData() {
		return hasData;
	}
	
	public List<XtreemFSData> getServicesMetrics() {
		if(servicesMetrics != null && servicesMetrics.size() > 0) {
			return servicesMetrics;
		}
		else
			log.error("XtreemFS metrics empty?");
		
		return null;
	}
}
