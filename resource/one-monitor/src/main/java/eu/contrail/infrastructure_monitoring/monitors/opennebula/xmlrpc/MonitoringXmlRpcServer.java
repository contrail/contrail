/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

/**
 * This serves as an access point to monitoring related information and configuration
 * It is only accessbile from localhost. To access these methods publicly, a public REST-ful server has to 
 * be invoked, which communicates with this embedded Webserver
 */

package eu.contrail.infrastructure_monitoring.monitors.opennebula.xmlrpc;

import org.apache.log4j.Logger;

import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;

public class MonitoringXmlRpcServer {
	
	private static Logger log = Logger.getLogger(MonitoringXmlRpcServer.class);
	
	public MonitoringXmlRpcServer() {
		try {
			XmlRpcServer server = new XmlRpcServer(OpenNebula.XMLRPC_SERVER_PORT);
			server.addHandler("monitoring", new MonitoringXmlRpcHandler());
			server.serve();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
}
