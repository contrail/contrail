package eu.contrail.infrastructure_monitoring.monitors.xtreemfs;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.snmp4j.CommunityTarget;
import org.snmp4j.MessageDispatcher;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.AbstractTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import eu.contrail.infrastructure_monitoring.monitors.xtreemfs.XtreemFSRawMetricList.XtreemFSRawMetric;

public class XtreemFSSNMPMonitor {
	private static Logger log = Logger.getLogger(XtreemFSSNMPMonitor.class);
	private static final int SNMP_RETRIES = 1;
	private static final int SNMP_TIMEOUT = 5000;
	private XtreemFSRawMetricList metrics;
	CommunityTarget target;
	Snmp snmp;
	
	public XtreemFSSNMPMonitor(String host, String port, String community, XtreemFSRawMetricList metrics) {
		this.setMetrics(metrics);
		AbstractTransportMapping transport;
		try {
			MessageDispatcher md = new MessageDispatcherImpl();
			md.addMessageProcessingModel(new MPv2c());
			transport = new DefaultUdpTransportMapping();
			snmp = new Snmp(md, transport);	
			snmp.listen();
			Address targetAddress = GenericAddress.parse("udp:" + host + "/" + port);
			if(targetAddress.isValid()) {
			   target = new CommunityTarget();
			   target.setCommunity(new OctetString(community));
			   target.setAddress(targetAddress);
			   target.setRetries(SNMP_RETRIES);
			   target.setTimeout(SNMP_TIMEOUT);
			   target.setVersion(SnmpConstants.version2c);
			}
			else
				log.error("Invalid SNMP address: " + targetAddress.toString());
				
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		   
	}

	public XtreemFSRawMetricList getData() throws IOException {
	   if(metrics != null && !metrics.getMetrics().isEmpty()) {
		   PDU pdu = new PDU();
		   String fullResponseVal = null;
		   String[] responseVals = null;
		   for(XtreemFSRawMetric m : metrics.getMetrics()) {
			   OID oid = new OID(convertOid(m.getOid()));
			   if(oid != null) {
				   pdu.add(new VariableBinding((oid)));
			   }
		   }
		   ResponseEvent response = snmp.send(pdu, target);
		   PDU responsePDU = response.getResponse();
		   if(responsePDU.getErrorStatus() == 0) {
			   for(int i=0; i<responsePDU.getVariableBindings().size(); i++) {
				   fullResponseVal = responsePDU.get(i).toString();
				   responseVals = fullResponseVal.split("=");
				   if(responseVals.length == 2) {
					   metrics.update(responseVals[0].trim(), responseVals[1].trim());
				   }   
			   }
		   }
		   else
			   log.error("Failed to fetch XtreemFS metric for type " + metrics.getMetricType() + ", reason: " + responsePDU.getErrorStatusText());
	   
	   }
	   return metrics;
	}
	
	private static int[] convertOid(String oidStr) {
		int[] rtn = null;
		if(oidStr != null && !oidStr.isEmpty()) {
			String[] p = oidStr.split("\\.");
			if(p.length > 0) {
				rtn = new int[p.length];
				for(int i=0; i<p.length; i++) {
					rtn[i] = Integer.parseInt(p[i]);
				}
			}
		}
		return rtn;
	}

	public XtreemFSRawMetricList getMetrics() {
		return metrics;
	}

	public void setMetrics(XtreemFSRawMetricList metrics) {
		this.metrics = metrics;
	}
}