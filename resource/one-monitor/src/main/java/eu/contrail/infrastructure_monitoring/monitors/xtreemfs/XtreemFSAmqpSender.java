/**
	 * Copyright (c) 2011, XLAB d.o.o.
	 * All rights reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without
	 * modification, are permitted provided that the following conditions are met:
	 *     * Redistributions of source code must retain the above copyright
	 *       notice, this list of conditions and the following disclaimer.
	 *     * Redistributions in binary form must reproduce the above copyright
	 *       notice, this list of conditions and the following disclaimer in the
	 *       documentation and/or other materials provided with the distribution.
	 *     * Neither the name of XLAB d.o.o. nor the
	 *       names of its contributors may be used to endorse or promote products
	 *       derived from this software without specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
	 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * @author Gregor Beslic - gregor.beslic@cloud.si
	 */

package eu.contrail.infrastructure_monitoring.monitors.xtreemfs;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import eu.contrail.infrastructure_monitoring.monitors.AmqpMetricsRegistration;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.data.XtreemFSData;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;

public class XtreemFSAmqpSender {
	public static final String EXCHANGE_NAME = "input.xtreemfs";
	public static final String ROUTING_KEY_PREFIX = "input.xtreemfs";
	
	public static final boolean DURABLE_FLAG = false;
	public static final boolean AUTO_DELETE_FLAG = true;
	public static final boolean INTERNAL_FLAG = false;
	
	private Document xmlDoc = null;
	private Document xmlRegistrationDoc = null;
	
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder = null;
	
    private static SimpleDateFormat ISO8601FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	private static Logger log = Logger.getLogger(XtreemFSAmqpSender.class);
    
	private static Channel channel = null;
	private static XtreemFSAmqpSender scalarixAmqpSender;
	private static boolean dirMetricsRegistered = false;
	private static boolean mrcMetricsRegistered = false;
	private static boolean osdMetricsRegistered = false;
	
	
	private XtreemFSAmqpSender() throws IOException {
		try {
			docBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(OpenNebula.RABBIT_MQ_HOST);
        Connection connection = factory.newConnection();
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "topic", DURABLE_FLAG, AUTO_DELETE_FLAG, INTERNAL_FLAG, null);
	}	
	
    public static XtreemFSAmqpSender getInstance() {
    	if(scalarixAmqpSender == null)
			try {
				scalarixAmqpSender = new XtreemFSAmqpSender();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	return scalarixAmqpSender;
    }
    
    private void sendMessage(String type, String routingKey, String message) {
        String fullRoutingKey = ROUTING_KEY_PREFIX + "." + type;
        if(routingKey != null && !routingKey.isEmpty()) {
        	fullRoutingKey += routingKey;
        }
        log.trace("Publishing " + message + " with routing key " + fullRoutingKey);
    	try {
			channel.basicPublish(EXCHANGE_NAME, fullRoutingKey, null, message.getBytes());
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public void sendMessages(List<XtreemFSData> clusterMetrics) {
		if(clusterMetrics.size() > 0) {
			for(XtreemFSData data : clusterMetrics) {
				xmlDoc = null;
				try {
					if(!dirMetricsRegistered || !mrcMetricsRegistered || !osdMetricsRegistered) {
						Element message = createXmlHeaderRegistration("xtreemfs", "snmp");
						xmlRegistrationDoc.appendChild(message);
				    	
						Element serviceUuid = createXmlMetricRegistration("service_uuid", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_UUID));
					    message.appendChild(serviceUuid);
				    	
					    Element serviceJvmMemMax = createXmlMetricRegistration("jvm_mem_max", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_MAX));
					    message.appendChild(serviceJvmMemMax);
				    	
					    Element serviceJvmMemUsed = createXmlMetricRegistration("jvm_mem_used", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_USED));
					    message.appendChild(serviceJvmMemUsed);
				    	
					    Element serviceJvmMemFree = createXmlMetricRegistration("jvm_mem_free", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_FREE));
					    message.appendChild(serviceJvmMemFree);
				    	
					    Element serviceClientConnections = createXmlMetricRegistration("client_connections", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_CLIENT_CONN));
					    message.appendChild(serviceClientConnections);
				    	
					    Element servicePendingRequests = createXmlMetricRegistration("pending_requests", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_PENDING_REQ));
					    message.appendChild(servicePendingRequests);
				    	
					    if(data.getServiceType().equalsIgnoreCase("DIR")) {
					    	Element dirAddressMappingCount = createXmlMetricRegistration("address_mapping_count", data.getMetricData(RawMetric.XTREEMS_SNMP_DIR_AMC));
						    message.appendChild(dirAddressMappingCount);
						    
							Element dirServiceCount = createXmlMetricRegistration("service_count", data.getMetricData(RawMetric.XTREEMS_SNMP_DIR_SC));
						    message.appendChild(dirServiceCount);
						    
						    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
						    
						    AmqpMetricsRegistration.getInstance().sendMessage("xtreemfs", ".dir.snmp", xmlRegistration);
						    dirMetricsRegistered = true;
					    } 
					    else if(data.getServiceType().equalsIgnoreCase("MRC")) {
					    	Element mrcVolumeCount = createXmlMetricRegistration("volume_count", data.getMetricData(RawMetric.XTREEMS_SNMP_MRC_VC));
						    message.appendChild(mrcVolumeCount);
						    
						    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
						    
						    AmqpMetricsRegistration.getInstance().sendMessage("xtreemfs", ".mrc.snmp", xmlRegistration);
						    mrcMetricsRegistered = true;
						}
					    else {
					    	Element osdObjectsReceived = createXmlMetricRegistration("objects_received", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_OBJ_RX));
						    message.appendChild(osdObjectsReceived);
							
						    Element osdReplObjectsReceived = createXmlMetricRegistration("replicated_objects_received", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_REP_OBJ_RX));
						    message.appendChild(osdReplObjectsReceived);
							    
						    Element osdObjectsTransmitted = createXmlMetricRegistration("objects_transmitted", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_OBJ_TX));
						    message.appendChild(osdObjectsTransmitted);

						    Element osdReplBytesReceived = createXmlMetricRegistration("replicated_bytes_received", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_REP_BYTES_RX));
						    message.appendChild(osdReplBytesReceived);
							    
					    	Element osdBytesReceived = createXmlMetricRegistration("bytes_received", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_BYTES_RX));
						    message.appendChild(osdBytesReceived);
							
						    Element osdBytesTransmitted = createXmlMetricRegistration("bytes_transmitted", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_BYTES_TX));
						    message.appendChild(osdBytesTransmitted);
						    
						    Element osdQueuePreprocessing = createXmlMetricRegistration("queue_preprocessing", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_QUEUE_PRE));
						    message.appendChild(osdQueuePreprocessing);
					    
						    Element osdQueueStorage = createXmlMetricRegistration("queue_storage", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_QUEUE_STOR));
						    message.appendChild(osdQueueStorage);
					    
						    Element osdQueueDeletion = createXmlMetricRegistration("queue_deletion", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_QUEUE_DEL));
						    message.appendChild(osdQueueDeletion);
						    
						    Element osdFilesOpen = createXmlMetricRegistration("files_open", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_FILES_OPEN));
						    message.appendChild(osdFilesOpen);
					    
						    Element osdFilesDeleted = createXmlMetricRegistration("files_deleted", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_FILES_DELETED));
						    message.appendChild(osdFilesDeleted);
						
						    Element osdFreeSpace = createXmlMetricRegistration("free_space", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_FREE_SPACE));
						    message.appendChild(osdFreeSpace);
						    
						    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
						    
						    AmqpMetricsRegistration.getInstance().sendMessage("xtreemfs", ".osd.snmp", xmlRegistration);
						    osdMetricsRegistered = true;
					    }
					}
					Element message = createXmlHeaderMetric(data.getServiceType());
					xmlDoc.appendChild(message);
			    	
					Element serviceUuid = createXmlMetricValue("service_uuid", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_UUID));
				    message.appendChild(serviceUuid);
			    	
				    Element serviceJvmMemMax = createXmlMetricValue("jvm_mem_max", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_MAX));
				    message.appendChild(serviceJvmMemMax);
			    	
				    Element serviceJvmMemUsed = createXmlMetricValue("jvm_mem_used", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_USED));
				    message.appendChild(serviceJvmMemUsed);
			    	
				    Element serviceJvmMemFree = createXmlMetricValue("jvm_mem_free", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_JVM_MEM_FREE));
				    message.appendChild(serviceJvmMemFree);
			    	
				    Element serviceClientConnections = createXmlMetricValue("client_connections", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_CLIENT_CONN));
				    message.appendChild(serviceClientConnections);
			    	
				    Element servicePendingRequests = createXmlMetricValue("pending_requests", data.getMetricData(RawMetric.XTREEMS_SNMP_SERVICE_PENDING_REQ));
				    message.appendChild(servicePendingRequests);
			    	
				    if(data.getServiceType().equalsIgnoreCase("DIR")) {
				    	Element dirAddressMappingCount = createXmlMetricValue("address_mapping_count", data.getMetricData(RawMetric.XTREEMS_SNMP_DIR_AMC));
					    message.appendChild(dirAddressMappingCount);
				    
						Element dirServiceCount = createXmlMetricValue("service_count", data.getMetricData(RawMetric.XTREEMS_SNMP_DIR_SC));
					    message.appendChild(dirServiceCount);
				    }
				    else if(data.getServiceType().equalsIgnoreCase("MRC")) {
				    	Element mrcVolumeCount = createXmlMetricValue("volume_count", data.getMetricData(RawMetric.XTREEMS_SNMP_MRC_VC));
					    message.appendChild(mrcVolumeCount);
				    }
				    else if(data.getServiceType().equalsIgnoreCase("OSD")) {
				    	Element osdObjectsReceived = createXmlMetricValue("objects_received", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_OBJ_RX));
					    message.appendChild(osdObjectsReceived);
					
					    Element osdReplObjectsReceived = createXmlMetricValue("replicated_objects_received", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_REP_OBJ_RX));
					    message.appendChild(osdReplObjectsReceived);
					    
					    Element osdObjectsTransmitted = createXmlMetricValue("objects_transmitted", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_OBJ_TX));
					    message.appendChild(osdObjectsTransmitted);

					    Element osdReplBytesReceived = createXmlMetricValue("replicated_bytes_received", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_REP_BYTES_RX));
					    message.appendChild(osdReplBytesReceived);
					    
				    	Element osdBytesReceived = createXmlMetricValue("bytes_received", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_BYTES_RX));
					    message.appendChild(osdBytesReceived);
					
					    Element osdBytesTransmitted = createXmlMetricValue("bytes_transmitted", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_BYTES_TX));
					    message.appendChild(osdBytesTransmitted);
				    
					    Element osdQueuePreprocessing = createXmlMetricValue("queue_preprocessing", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_QUEUE_PRE));
					    message.appendChild(osdQueuePreprocessing);
				    
					    Element osdQueueStorage = createXmlMetricValue("queue_storage", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_QUEUE_STOR));
					    message.appendChild(osdQueueStorage);
				    
					    Element osdQueueDeletion = createXmlMetricValue("queue_deletion", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_QUEUE_DEL));
					    message.appendChild(osdQueueDeletion);
					    
					    Element osdFilesOpen = createXmlMetricValue("files_open", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_FILES_OPEN));
					    message.appendChild(osdFilesOpen);
				    
					    Element osdFilesDeleted = createXmlMetricValue("files_deleted", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_FILES_DELETED));
					    message.appendChild(osdFilesDeleted);
					
					    Element osdFreeSpace = createXmlMetricValue("free_space", data.getMetricData(RawMetric.XTREEMS_SNMP_OSD_FREE_SPACE));
					    message.appendChild(osdFreeSpace);
				    }
				    else {
				    	throw new Exception("Unknown service type");
				    }
				    
				    String xml = createXmlFooter(xmlDoc);
				    sendMessage(data.getServiceType().toLowerCase(), ".snmp", xml);
				    
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private Element createXmlHeaderRegistration(String source, String group) throws ParserConfigurationException {
    	xmlRegistrationDoc = docBuilder.newDocument();
        Element hostMessage = xmlRegistrationDoc.createElement("Message");
        hostMessage.setAttribute("group", group);
        hostMessage.setAttribute("source", source);
        return hostMessage;
    }
	
	private Element createXmlHeaderMetric(String sid) throws ParserConfigurationException {
    	xmlDoc = docBuilder.newDocument();
        Element hostMessage = xmlDoc.createElement("Message");
        hostMessage.setAttribute("time", ISO8601FORMAT.format(new Date()));
        //hostMessage.setAttribute("sid", sid);
        return hostMessage;
    }
    
    private String createXmlFooter(Document doc) throws TransformerException {
    	Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        return result.getWriter().toString();
    }
    
    private Element createXmlMetricRegistration(String metricId, MetricData metricData) {
    	Element metric = xmlRegistrationDoc.createElement("Metric");
    	metric.setAttribute("id", metricId);
		if(metricData != null) {
    		if(metricData.getType() != null) {
        		Element metricType = xmlRegistrationDoc.createElement("Type");
            	metricType.setTextContent(metricData.getType().name());
            	metric.appendChild(metricType);
            }
        	if(metricData.getUnit() != null) {
            	Element metricUnit = xmlRegistrationDoc.createElement("Unit");
            	metricUnit.setTextContent(metricData.getUnit());
            	metric.appendChild(metricUnit);	
        	}
    	}
    	else
    		log.warn("MetricData is null for " + metricId);
    	
    	return metric;
    }
    
    private Element createXmlMetricValue(String metricId, MetricData metricData) {
    	Element metric = xmlDoc.createElement("Value");
    	metric.setAttribute("id", metricId);
		if(metricData != null) {
    		if(metricData.getStringValue() != null) {
        		metric.setTextContent(metricData.getStringValue());
            }
    	}
    	else
    		log.warn("MetricData is null for " + metricId);
    	
    	return metric;
    }
}