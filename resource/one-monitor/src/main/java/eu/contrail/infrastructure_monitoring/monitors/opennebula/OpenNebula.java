/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.monitors.opennebula;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.cluster.Cluster;
import org.opennebula.client.cluster.ClusterPool;
import org.opennebula.client.host.Host;
import org.opennebula.client.host.HostPool;
import org.opennebula.client.vm.VirtualMachine;
import org.opennebula.client.vm.VirtualMachinePool;
import org.xml.sax.SAXException;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import eu.contrail.infrastructure_monitoring.enums.DataType;
import eu.contrail.infrastructure_monitoring.monitors.Metrics;
import eu.contrail.infrastructure_monitoring.monitors.data.ClusterData;
import eu.contrail.infrastructure_monitoring.monitors.data.HostData;
import eu.contrail.infrastructure_monitoring.monitors.data.IdMappingData;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.MonitoringData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.data.VmData;
import eu.contrail.infrastructure_monitoring.utils.SshConnector;
import eu.contrail.infrastructure_monitoring.utils.Util;

public class OpenNebula {
	public static String PROVIDER_ID;
	public static String ONE_RPC_HOST;
	public static String ONE_RPC_PORT;
	public static String ONE_ADMIN_USERNAME;
	public static String ONE_ADMIN_PASSWORD;
	public static String RABBIT_MQ_HOST;
	public static String CLUSTER_FQDN;
	public static int RABBIT_ACK_TIMEOUT_SECS;
	public static int SOCKET_SERVER_PORT;
	public static int XMLRPC_SERVER_PORT;
	public static String SEND_TO_MONITORING_HUB;
	public static String FEDERATION_FINAGLE_HOST;
	public static String FEDERATION_FINAGLE_PORT;
	public static String FEDERATION_FINAGLE_CLIENT_CONN;
	public static String LOG4J_LOG_LEVEL;
	
	private static OpenNebula oneObject;
	private static Client oneClient = null;
	private static ClusterPool clusterPool;
	private static HostPool hostPool;
	private static VirtualMachinePool vmPool;
	
	private Connection connection;
 	private Channel channel;
 	private String replyQueueName;
 	private QueueingConsumer consumer;
 	
 	private static boolean hasData = false;
 	
 	private static Logger log = Logger.getLogger(OpenNebula.class);
 	private Metrics metrics;
 	
    private static int VM_STATE_RUNNING = 3;
    private static int HOST_STATE_DISABLED = 4;
	private static String firstHostFqdn = null;
    
    private static final String CMD_UPTIME = "uptime";
	private static final String CMD_CPU_CORES = "grep -i processor /proc/cpuinfo | wc -l";
//	private static final String CMD_CPU_SPEED = "cat /proc/cpuinfo | grep \"cpu MHz\" | sed 's/[^0-9\\.]//g'";
	private static final String CMD_HOSTNAME = "hostname";
	private static final String CMD_MEM_USAGE = "free -m";
	private static final String CMD_DISK_FREE = "df |grep FS";
	private static final String CMD_NET_ROUTE_DEV = "awk ' { if ($2 == '00000000') print $1 ; } ' < /proc/net/route";
	private static final String CMD_NET_RX = "grep <routeDevice> /proc/net/dev | cut -d':' -f2 | awk ' { print $1; } '";
	private static final String CMD_NET_TX = "grep <routeDevice> /proc/net/dev | cut -d':' -f2 | awk ' { print $9; } '";
		
	// Used to store VEP -> OVF -> ONE id mappings
	private static ArrayList<IdMappingData> idMaps = new ArrayList<IdMappingData>();
	
	private OpenNebula() {
		 try {
			 oneClient = new Client(ONE_ADMIN_USERNAME + ":" + ONE_ADMIN_PASSWORD, "http://" + ONE_RPC_HOST + ":" + ONE_RPC_PORT + "/RPC2");
			 clusterPool = new ClusterPool(oneClient);
			 hostPool = new HostPool(oneClient);
			 vmPool = new VirtualMachinePool(oneClient, -2);
			 ConnectionFactory factory = new ConnectionFactory();
			 factory.setHost(RABBIT_MQ_HOST);
			 connection = factory.newConnection();
			 channel = connection.createChannel();
			 replyQueueName = channel.queueDeclare().getQueue(); 
			 consumer = new QueueingConsumer(channel);
			 channel.basicConsume(replyQueueName, true, consumer);
			 
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
	
	public Metrics getMetrics() {
		return metrics;
	}
	
	public static boolean getHasData() {
		return hasData;
	}
	
	private void initMetrics() {
		hasData = false;
		metrics = new Metrics();
		metrics.setVmLayoutData(new MonitoringData());
		metrics.setClusterConfData(new MonitoringData());
	}
	
	public static synchronized OpenNebula getInstance() {
		if(oneObject == null)
			oneObject = new OpenNebula();
		return oneObject;
	}
	
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
	
	private String getNodeInfo(String message, String queue) throws Exception {     
	    if(channel == null)
	    	throw new Exception("Is RabbitMq running?");
	    
		String response = null;
	    String corrId = java.util.UUID.randomUUID().toString();

	    BasicProperties props = new BasicProperties
	                                .Builder()
	                                .correlationId(corrId)
	                                .replyTo(replyQueueName)
	                                .build();
	    channel.queueDeclare(queue, false, false, false, null);
	    channel.basicPublish("", queue, props, message.getBytes());

	    while (true) {
	    	QueueingConsumer.Delivery delivery = consumer.nextDelivery(RABBIT_ACK_TIMEOUT_SECS * 4000);
	    	if(delivery == null) {
	    		// Purge the queue if sensor is unavailable, so old messages don't build up
	    		channel.queuePurge(queue);
	    		return null;
	    	}
	    	
	    	if (delivery.getProperties().getCorrelationId().equals(corrId)) {
	            response = new String(delivery.getBody());
	            break;
	        }
	    }

	    return response;
	}
    
    /**
     * Iterates through all the clusters in clusterPool and stores info
     * After the clusters are scanned, it updates all the hosts
     */
	public void updateClusters() {
		initMetrics();
		OneResponse response = clusterPool.info();
		if (response.getErrorMessage() != "null") {
			Iterator<Cluster> iterator = clusterPool.iterator();
			String clusterPrefix = OpenNebula.CLUSTER_FQDN;
			while(iterator.hasNext()) {
				Cluster cluster = iterator.next();
				ClusterData clusterData = new ClusterData();
				clusterData.setName(cluster.getName());
				clusterData.setFqdn(cluster.getId() + "." + clusterPrefix);
				metrics.getClusterConfData().putClusterData(clusterData);
			}
			updateHosts();
			hasData = true;
		}
		else
			log.error(response.getErrorMessage());
	}
	
	/**
	 * Iterates through all the hosts and updates info
	 */
	private void updateHosts() {
		OneResponse response = hostPool.info();
		if (response.getErrorMessage() != "null") {
			Iterator<Host> iterator = hostPool.iterator();
			
			// Create a dummy host for pending / unscheduled VMs
			HostData vmLayoutData = new HostData();
			vmLayoutData.setName("temp");
			vmLayoutData.setOneId("0");
			vmLayoutData.setFqdn("temp");
			vmLayoutData.putMetricData(new MetricData(RawMetric.HOSTNAME, "temp", DataType.STRING, ""));
			
			metrics.getVmLayoutData().putHostData(vmLayoutData);
			
			while (iterator.hasNext()) {
				Host host = iterator.next();
				
				vmLayoutData = new HostData();
				vmLayoutData.setName(host.getName());
				vmLayoutData.setOneId(host.getId());
								
				HostData clusterConfData = new HostData();
				clusterConfData.setName(host.getName());
				
				HostData storageData = new HostData();
				storageData.setName(host.getName());
				
				String nodeInfo;
				try {
					nodeInfo = getNodeInfo("HostData", host.getName());
					if(nodeInfo == null) {
						log.warn(host.getName() + " monitoring agent down or not working");
						continue;
					}
					JSONObject jsonObj = new JSONObject(nodeInfo);
					
					if(jsonObj.has("FQDN")) {
						log.trace(host.getName() + " FQDN: " + jsonObj.get("FQDN").toString());
						if(firstHostFqdn == null || firstHostFqdn.isEmpty()) {
							firstHostFqdn = jsonObj.get("FQDN").toString();
						}
						vmLayoutData.setFqdn(jsonObj.get("FQDN").toString());
						clusterConfData.setFqdn(jsonObj.get("FQDN").toString());
						
						// API Metrics
						String hostState = "1";
						if(host.state() == HOST_STATE_DISABLED)
							hostState = "0";
						vmLayoutData.putMetricData(new MetricData(RawMetric.AVAILABILITY_STATUS, hostState, DataType.INTEGER, ""));
						
						// Sensor metrics
						if(jsonObj.has("LOAD_ONE")) {
							log.trace(host.getName() + " LOAD_ONE: " + jsonObj.get("LOAD_ONE").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.LOAD_ONE, jsonObj.get("LOAD_ONE").toString(), DataType.STRING, ""));
						}
                        if(jsonObj.has("LOAD_ONE_NORM")) {
                            log.trace(host.getName() + " LOAD_ONE_NORM: " + jsonObj.get("LOAD_ONE_NORM").toString());
                            vmLayoutData.putMetricData(new MetricData(RawMetric.LOAD_ONE_NORM,
                                    jsonObj.get("LOAD_ONE_NORM").toString(), DataType.STRING, ""));
                        }
						if(jsonObj.has("LOAD_FIVE")) {
							log.trace(host.getName() + " LOAD_FIVE: " + jsonObj.get("LOAD_FIVE").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.LOAD_FIVE, jsonObj.get("LOAD_FIVE").toString(), DataType.STRING, ""));
						}
                        if(jsonObj.has("LOAD_FIVE_NORM")) {
                            log.trace(host.getName() + " LOAD_FIVE_NORM: " + jsonObj.get("LOAD_FIVE_NORM").toString());
                            vmLayoutData.putMetricData(new MetricData(RawMetric.LOAD_FIVE_NORM,
                                    jsonObj.get("LOAD_FIVE_NORM").toString(), DataType.STRING, ""));
                        }
						if(jsonObj.has("IP_ADDRESS")) {
							log.trace(host.getName() + " IPS: " + jsonObj.get("IP_ADDRESS").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.IP_ADDRESS, jsonObj.get("IP_ADDRESS").toString(), DataType.STRING, ""));
						}
						
						if(jsonObj.has("HOSTNAME")) {
							log.trace(host.getName() + " HOSTNAME: " + jsonObj.get("HOSTNAME").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.HOSTNAME, jsonObj.get("HOSTNAME").toString(), DataType.STRING, ""));
						}
						
						if(jsonObj.has("MEM_TOTAL")) {
							log.trace(host.getName() + " MEM_TOTAL: " + jsonObj.get("MEM_TOTAL").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.MEM_TOTAL, jsonObj.get("MEM_TOTAL").toString(), DataType.INTEGER, "MB"));
						}
						
						if(jsonObj.has("MEM_USAGE")) {
							log.trace(host.getName() + " MEM_USAGE: " + jsonObj.get("MEM_USAGE").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.MEM_USAGE, jsonObj.get("MEM_USAGE").toString(), DataType.INTEGER, "MB"));
						}
						
						if(jsonObj.has("MEM_FREE")) {
							log.trace(host.getName() + " MEM_FREE: " + jsonObj.get("MEM_FREE").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.MEM_FREE, jsonObj.get("MEM_FREE").toString(), DataType.INTEGER, "MB"));
						}
						
						if(jsonObj.has("CPU_CORES_COUNT")) {
							log.trace(host.getName() + " CPU_CORES_COUNT: " + jsonObj.get("CPU_CORES_COUNT").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.CPU_CORES_COUNT, jsonObj.get("CPU_CORES_COUNT").toString(), DataType.INTEGER, ""));
						}
						
						if(jsonObj.has("CPU_SPEED")) {
							log.trace(host.getName() + " CPU_SPEED: " + jsonObj.get("CPU_SPEED").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.CPU_SPEED, jsonObj.get("CPU_SPEED").toString(), DataType.DOUBLE, "MHz"));
						}
						
						if(jsonObj.has("CPU_USER")) {
							log.trace(host.getName() + " CPU_USER: " + jsonObj.get("CPU_USER").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.CPU_USER, jsonObj.get("CPU_USER").toString(), DataType.DOUBLE, "%"));
						}					
						
						if(jsonObj.has("CPU_SYSTEM")) {
							log.trace(host.getName() + " CPU_SYSTEM: " + jsonObj.get("CPU_SYSTEM").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.CPU_SYSTEM, jsonObj.get("CPU_SYSTEM").toString(), DataType.DOUBLE, "%"));
						}
						
						if(jsonObj.has("CPU_IDLE")) {
							log.trace(host.getName() + " CPU_IDLE: " + jsonObj.get("CPU_IDLE").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.CPU_IDLE, jsonObj.get("CPU_IDLE").toString(), DataType.DOUBLE, "%"));
						}
						
						if(jsonObj.has("SHARED_IMAGES_DISK_FREE")) {
							log.trace(host.getName() + " SHARED_IMAGES_DISK_FREE " + jsonObj.get("SHARED_IMAGES_DISK_FREE").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.SHARED_IMAGES_DISK_FREE, Double.parseDouble(jsonObj.get("SHARED_IMAGES_DISK_FREE").toString()), DataType.DOUBLE, "KB"));
						}
						
						if(jsonObj.has("SHARED_IMAGES_DISK_USED")) {
							log.trace(host.getName() + " SHARED_IMAGES_DISK_USED " + jsonObj.get("SHARED_IMAGES_DISK_USED").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.SHARED_IMAGES_DISK_USED, Double.parseDouble(jsonObj.get("SHARED_IMAGES_DISK_USED").toString()), DataType.DOUBLE, "KB"));
						}
						
						if(jsonObj.has("NET_BYTES_RX")) {
							log.trace(host.getName() + " NET_BYTES_RX " + jsonObj.get("NET_BYTES_RX").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.NET_BYTES_RX, jsonObj.get("NET_BYTES_RX").toString(), DataType.STRING, "B"));
						}
						
						if(jsonObj.has("NET_BYTES_TX")) {
							log.trace(host.getName() + " NET_BYTES_TX " + jsonObj.get("NET_BYTES_TX").toString());
							vmLayoutData.putMetricData(new MetricData(RawMetric.NET_BYTES_TX, jsonObj.get("NET_BYTES_TX").toString(), DataType.STRING, "B"));
						}
												
						metrics.getVmLayoutData().putHostData(vmLayoutData);
						
						// ClusterConfiguration
						if(jsonObj.has("AUDITABILITY")) {
							log.trace(host.getName() + " AUDITABILITY " + jsonObj.get("AUDITABILITY").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.AUDITABILITY, jsonObj.get("AUDITABILITY").toString(), DataType.BOOLEAN, ""));
						}
						
						if(jsonObj.has("LOCATION")) {
							log.trace(host.getName() + " LOCATION " + jsonObj.get("LOCATION").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.LOCATION, jsonObj.get("LOCATION").toString(), DataType.STRING, ""));
						}
						
						if(jsonObj.has("SAS70_COMPLIANCE")) {
							log.trace(host.getName() + " SAS70_COMPLIANCE " + jsonObj.get("SAS70_COMPLIANCE").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.SAS70_COMPLIANCE, jsonObj.get("SAS70_COMPLIANCE").toString(), DataType.BOOLEAN, ""));
						}
						
						if(jsonObj.has("CCR")) {
							log.trace(host.getName() + " CCR " + jsonObj.get("CCR").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.CCR, jsonObj.get("CCR").toString(), DataType.BOOLEAN, ""));
						}
						
						if(jsonObj.has("DATA_CLASSIFICATION")) {
							log.trace(host.getName() + " DATA_CLASSIFICATION " + jsonObj.get("DATA_CLASSIFICATION").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.DATA_CLASSIFICATION, jsonObj.get("DATA_CLASSIFICATION").toString(), DataType.STRING, ""));
						}
						
						if(jsonObj.has("HW_REDUNDANCY_LEVEL")) {
							log.trace(host.getName() + " HW_REDUNDANCY_LEVEL " + jsonObj.get("HW_REDUNDANCY_LEVEL").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.HW_REDUNDANCY_LEVEL, jsonObj.get("HW_REDUNDANCY_LEVEL").toString(), DataType.STRING, ""));
						}
						
						if(jsonObj.has("DISK_THROUGHPUT")) {
							log.trace(host.getName() + " DISK_THROUGHPUT " + jsonObj.get("DISK_THROUGHPUT").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.DISK_THROUGHPUT, jsonObj.get("DISK_THROUGHPUT").toString(), DataType.STRING, ""));
						}
						
						if(jsonObj.has("NET_THROUGHPUT")) {
							log.trace(host.getName() + " NET_THROUGHPUT " + jsonObj.get("NET_THROUGHPUT").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.NET_THROUGHPUT, jsonObj.get("NET_THROUGHPUT").toString(), DataType.STRING, ""));
						}
						
						if(jsonObj.has("DATA_ENCRYPTION")) {
							log.trace(host.getName() + " DATA_ENCRYPTION " + jsonObj.get("DATA_ENCRYPTION").toString());
							clusterConfData.putMetricData(new MetricData(RawMetric.DATA_ENCRYPTION, jsonObj.get("DATA_ENCRYPTION").toString(), DataType.BOOLEAN, ""));
						}
						
						metrics.getClusterConfData().putHostData(clusterConfData);
						
					}
					else
						log.warn("No FQDN for host " + host.getName());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			updateVms();
		}
		else
			log.error(response.getErrorMessage());
	}
		
	
	/** 
	 * Updates all the OpenNebula virtual machines
	 * 
	 * There might be other VMs running by KVM on the host node (not part of OpenNebula), hence we have 
	 * to take the host list from ONE and retrieve metric data back from each host in the cluster
	**/
	private void updateVms() {
		OneResponse response = vmPool.info();
		if (response.getErrorMessage() != "null") {
			Iterator<VirtualMachine> iterator = vmPool.iterator();
			while (iterator.hasNext()) {
				VirtualMachine vm = iterator.next();
				try {
					if(vm == null)
						continue;
					
					// Name is created as one-vmID, so we're able to identify the VM by name only (RawMetric doesn't support internal ID)
					String vmName = Util.createVmName(vm);
					
					OneVm oneVm = new OneVm(vm);
					
					String hostName = oneVm.getVmHostname();
					
					log.trace("VM " + vm.getId() + " with name " + vm.getName() + " (monitoring name " + vmName + ")");
					VmData vmData = new VmData();
					vmData.setOneId(oneVm.getVmId());

					vmData.setName(vmName);
					vmData.putMetricData(new MetricData(RawMetric.VM_STATE, vm.stateStr().toLowerCase(), DataType.STRING, ""));
					
					String uid = oneVm.getVmOwner();
					vmData.putMetricData(new MetricData(RawMetric.ID_OWNER, uid, DataType.STRING, ""));
					
					String source = oneVm.getVmSource();
					vmData.putMetricData(new MetricData(RawMetric.VM_IMAGE_TEMPLATE, source, DataType.STRING, ""));
					String readOnly = oneVm.getVmReadOnly();
					boolean persistent = false;
					if(readOnly.equalsIgnoreCase("no"))
						persistent = true;
					
					vmData.putMetricData(new MetricData(RawMetric.VM_PERSISTENCE, persistent, DataType.STRING, ""));
					
					String ipAddress = oneVm.getVmIpAddress();
					vmData.putMetricData(new MetricData(RawMetric.IP_ADDRESS, ipAddress, DataType.STRING, ""));
					
					String vCpus = oneVm.getVirtualCpus();
					if(vCpus.isEmpty())
						vCpus = "1";
					vmData.putMetricData(new MetricData(RawMetric.CPU_CORES_COUNT, vCpus, DataType.STRING, ""));
			
					String memory = oneVm.getMemory();
					vmData.putMetricData(new MetricData(RawMetric.MEM_TOTAL, memory, DataType.STRING, ""));
					
					String vmFqdn = null;
					String hostFqdn = null;
					String nodeInfo = getNodeInfo("VmData_one-" + vm.getId() + ";" + ipAddress, hostName);
					JSONObject jsonObj = null;
					if(nodeInfo == null) {
						log.warn("Virtual machine " + vm.getId() + " host " + hostName + " monitoring agent down or not working");
//						continue;
					}
					else
						jsonObj = new JSONObject(nodeInfo);
					
					boolean gotVmSshResponse = false;
					
					if(jsonObj != null) {
						if(jsonObj.has("HOST_FQDN")	&& metrics.getVmLayoutData().containsHostData(jsonObj.get("HOST_FQDN").toString())) {
							hostFqdn = jsonObj.get("HOST_FQDN").toString();
							vmFqdn = Util.createVmFqdnFromIpAndHostFqdn(ipAddress, hostFqdn);
							vmData.setFqdn(vmFqdn);
						}
						if(jsonObj.has("CPU_SECONDS")) {
							log.trace("VM " + vm.getId() + " CPU SECONDS: " + jsonObj.get("CPU_SECONDS").toString());
							if(jsonObj.get("CPU_SECONDS").toString().equalsIgnoreCase("N/A"))
								vmData.putMetricData(new MetricData(RawMetric.CPU_SECONDS, "N/A", DataType.STRING, ""));
							else
								vmData.putMetricData(new MetricData(RawMetric.CPU_SECONDS, jsonObj.get("CPU_SECONDS").toString(), DataType.DOUBLE, "s"));
							
							if(jsonObj.get("CPU_SHARE").toString().equalsIgnoreCase("N/A"))
								vmData.putMetricData(new MetricData(RawMetric.CPU_SHARE, "N/A", DataType.STRING, ""));
							else
								vmData.putMetricData(new MetricData(RawMetric.CPU_SHARE, jsonObj.get("CPU_SHARE").toString(), DataType.STRING, "%"));
						
							if(jsonObj.get("MEM_HOST").toString().equalsIgnoreCase("N/A"))
								vmData.putMetricData(new MetricData(RawMetric.MEM_HOST, "N/A", DataType.STRING, ""));
							else
								vmData.putMetricData(new MetricData(RawMetric.MEM_HOST, jsonObj.get("MEM_HOST").toString(), DataType.DOUBLE, "MB"));
						}
						// VM CPU SPEED is calculated from HOST_CPU_SPEED * NUM_VM_CPUs
						if(jsonObj.has("CPU_SPEED")) {
							float hostCpuSpeed = Float.parseFloat(jsonObj.get("CPU_SPEED").toString());
							float vmNumCPUs = Float.parseFloat(oneVm.getCpus());
							float vmCpuSpeed = hostCpuSpeed * vmNumCPUs;
							log.trace("CPU_SPEED " + String.valueOf(vmCpuSpeed) + " for VM " + vmName + " with IP " + ipAddress);
							vmData.putMetricData(new MetricData(RawMetric.CPU_SPEED, String.valueOf(vmCpuSpeed), DataType.STRING, ""));
						}
						
						if(jsonObj.has("LOCATION")) {
							log.trace("LOCATION " + jsonObj.get("LOCATION").toString() + " for VM " + vmName + " with IP " + ipAddress);
							vmData.putMetricData(new MetricData(RawMetric.LOCATION, String.valueOf(jsonObj.get("LOCATION").toString()), DataType.STRING, ""));
						}
					}
					
					if(vm.state() == VM_STATE_RUNNING) {
						log.trace("Trying to SSH to VM " + vmName + " with IP " + ipAddress);
						SshConnector ssh = new SshConnector(ipAddress);
						if(ssh.isReady()) {
							log.trace("Executing first command on " + vmName + " with IP " + ipAddress);
							String[] vmResponse = null;
							vmResponse = ssh.exec(CMD_UPTIME, false, false);
							String repVal = "N/A";
							if(vmResponse != null && vmResponse.length == 1) {
								gotVmSshResponse = true;
								log.trace("Got SSH response from VM, collecting other monitoring data...");
								log.trace("adding AVAILABILITY_STATUS 1" + " for VM " + vmName + " with IP " + ipAddress);
								vmData.putMetricData(new MetricData(RawMetric.AVAILABILITY_STATUS, "1", DataType.STRING, ""));
								
								String find = "load average: ";
								String loadAvgRow = vmResponse[0].substring(vmResponse[0].indexOf(find) + find.length());
								String[] loadAvgs = loadAvgRow.split(",");
								repVal = loadAvgs[0].trim();
								log.trace("CPU_LOAD " + repVal + " for VM " + vmName + " with IP " + ipAddress);
								vmData.putMetricData(new MetricData(RawMetric.CPU_LOAD, repVal, DataType.STRING, ""));
								
								vmResponse = ssh.exec(CMD_HOSTNAME, false, false);
								if(vmResponse != null) {
									log.trace("HOSTNAME " + vmResponse[0].trim() + " for VM " + vmName + " with IP " + ipAddress);
									vmData.putMetricData(new MetricData(RawMetric.HOSTNAME, vmResponse[0].trim(), DataType.STRING, ""));
								}
								
								vmResponse = ssh.exec(CMD_CPU_CORES, false, false);
								if(vmResponse != null) {
									log.trace("CPU_CORES_COUNT " + vmResponse[0].trim() + " for VM " + vmName + " with IP " + ipAddress);
									vmData.putMetricData(new MetricData(RawMetric.CPU_CORES_COUNT, vmResponse[0].trim(), DataType.STRING, ""));
								}
								
								/* OLD WAY OF RETRIEVING VM_CPU_SPEED (equal to HOST_CPU_SPEED)
								
								vmResponse = ssh.exec(CMD_CPU_SPEED, false, false);
								if(vmResponse != null) {
									log.trace("CPU_SPEED " + vmResponse[0].trim() + " for VM " + vmName + " with IP " + ipAddress);
									vmData.putMetricData(new MetricData(RawMetric.CPU_SPEED, vmResponse[0].trim(), DataType.STRING, ""));
								}
								*/
								
								vmResponse = ssh.exec(CMD_MEM_USAGE, false, false);
								if(vmResponse != null && vmResponse.length > 1) {
									String[] memMetrics = vmResponse[1].split("\\s+");
									
//									log.trace("MEM_TOTAL " + memMetrics[1] + " for VM " + vmName + " with IP " + ipAddress);
//									vmData.putMetricData(new MetricData(RawMetric.MEM_TOTAL, memMetrics[1], DataType.STRING, "MB"));
									
									log.trace("MEM_USAGE " + memMetrics[2] + " for VM " + vmName + " with IP " + ipAddress);
									vmData.putMetricData(new MetricData(RawMetric.MEM_USAGE, memMetrics[2].trim(), DataType.STRING, "MB"));
									
									log.trace("MEM_FREE " + memMetrics[3] + " for VM " + vmName + " with IP " + ipAddress);
									vmData.putMetricData(new MetricData(RawMetric.MEM_FREE, memMetrics[3].trim(), DataType.STRING, "MB"));
								}
								
								vmResponse = ssh.exec(CMD_DISK_FREE.replace("FS", "vda"), false, false);
								if(vmResponse != null) {
									String[] filesystems = {"vda", "hda", "sda"};
									String[] diskMetrics = null;
									for(String s : filesystems) {
										String[] cmdResponse = ssh.exec(CMD_DISK_FREE.replaceAll("FS", s), false, false);
										if(cmdResponse != null && cmdResponse.length == 1 && cmdResponse[0] != null && !cmdResponse[0].isEmpty()) {
											diskMetrics = cmdResponse[0].split("\\s+");
										}
									}
									if(diskMetrics != null) {
										log.trace("DISK_TOTAL " + diskMetrics[1] + " for VM " + vmName + " with IP " + ipAddress);
										vmData.putMetricData(new MetricData(RawMetric.DISK_TOTAL, diskMetrics[1], DataType.STRING, "KB"));
									
										log.trace("DISK_FREE " + diskMetrics[3] + " for VM " + vmName + " with IP " + ipAddress);
										vmData.putMetricData(new MetricData(RawMetric.DISK_FREE, diskMetrics[3], DataType.STRING, "KB"));
									}
								}
								
								String[] routeDeviceResponse = ssh.exec(CMD_NET_ROUTE_DEV, false, false);
								if(routeDeviceResponse != null && routeDeviceResponse.length == 1 && !routeDeviceResponse[0].isEmpty()) {
									String[] bytesTxResponse = ssh.exec(CMD_NET_TX.replace("<routeDevice>", routeDeviceResponse[0]), false, false);
									if(bytesTxResponse != null && bytesTxResponse.length == 1 && !bytesTxResponse[0].isEmpty()) {
										log.trace("NET_BYTES_TX " + bytesTxResponse[0] + " for VM " + vmName + " with IP " + ipAddress);
										vmData.putMetricData(new MetricData(RawMetric.NET_BYTES_TX, bytesTxResponse[0], DataType.STRING, "B"));
									}
									else
										vmData.putMetricData(new MetricData(RawMetric.NET_BYTES_TX, "N/A", DataType.STRING, ""));
									
									String[] bytesRxResponse = vmResponse = ssh.exec(CMD_NET_RX.replace("<routeDevice>", routeDeviceResponse[0]), false, false);
									if(bytesRxResponse != null && bytesRxResponse.length == 1 && !bytesRxResponse[0].isEmpty()) {
										log.trace("NET_BYTES_RX " + bytesRxResponse[0] + " for VM " + vmName + " with IP " + ipAddress);
										vmData.putMetricData(new MetricData(RawMetric.NET_BYTES_RX, bytesRxResponse[0], DataType.STRING, "B"));
									}
									else
										vmData.putMetricData(new MetricData(RawMetric.NET_BYTES_RX, "N/A", DataType.STRING, ""));
								}
								else {
									vmData.putMetricData(new MetricData(RawMetric.NET_BYTES_TX, "N/A", DataType.STRING, ""));
									vmData.putMetricData(new MetricData(RawMetric.NET_BYTES_RX, "N/A", DataType.STRING, ""));
								}
							}
						}
						
						ssh.closeChannel();
						ssh.closeSession();
					}
					
					if(jsonObj == null || !gotVmSshResponse) {
						// VM is still in pending state
						if(jsonObj == null) {
							// This is used for VEP-ID matching, so an actual hostname has to be used
							vmFqdn = Util.createVmFqdnFromIpAndHostFqdn(ipAddress, firstHostFqdn);
							vmData.setFqdn(vmFqdn);
							hostFqdn = "temp";
							vmData.setHostFqdn(hostFqdn);
							log.warn("Not SSH-ing to " + vm.getId() + " on host " + hostFqdn + ", status: " + vm.stateStr());
							vmData.putMetricData(new MetricData(RawMetric.CPU_SHARE, "N/A", DataType.STRING, ""));
							vmData.putMetricData(new MetricData(RawMetric.CPU_SECONDS, "N/A", DataType.STRING, ""));
							vmData.putMetricData(new MetricData(RawMetric.MEM_HOST, "N/A", DataType.STRING, ""));
							vmData.putMetricData(new MetricData(RawMetric.CPU_SPEED, "N/A", DataType.STRING, ""));
							vmData.putMetricData(new MetricData(RawMetric.LOCATION, "N/A", DataType.STRING, ""));
						}
						// VM is in running state but SSH is not possible
						else
							log.warn("Didn't get response from VM, setting all monitoring values to N/A...");

						vmData.putMetricData(new MetricData(RawMetric.HOSTNAME, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.AVAILABILITY_STATUS, "0", DataType.BOOLEAN, ""));
						vmData.putMetricData(new MetricData(RawMetric.CPU_LOAD, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.HOSTNAME, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.CPU_CORES_COUNT, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.MEM_TOTAL, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.MEM_USAGE, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.MEM_FREE, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.DISK_TOTAL, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.DISK_FREE, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.NET_BYTES_TX, "N/A", DataType.STRING, ""));
						vmData.putMetricData(new MetricData(RawMetric.NET_BYTES_RX, "N/A", DataType.STRING, ""));
					}
					if(hostFqdn != null && vmFqdn != null)
						metrics.getVmLayoutData().getHostData(hostFqdn).putVm(vmFqdn);
					
					metrics.getVmLayoutData().putVmData(vmData);
					
				} catch(ParserConfigurationException pce) {
					pce.printStackTrace();
				} catch(SAXException se) {
					se.printStackTrace();
				} catch(IOException ioe) {
					ioe.printStackTrace();
				} catch (XPathExpressionException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}	
		}
		else
			log.error(response.getErrorMessage());
	}
	
	public VirtualMachine getVm(String id) {
		OneResponse response = vmPool.info();
		if (response.getErrorMessage() != "null") {
			Iterator<VirtualMachine> iterator = vmPool.iterator();
			while (iterator.hasNext()) {
				VirtualMachine vm = iterator.next();
				if(vm.getId().equalsIgnoreCase(id))
					return vm;
			}
		}
		return null;
	}
	
	public Host getHost(String id) {
		Iterator<Host> iterator = hostPool.iterator();
		while (iterator.hasNext()) {
			Host host = iterator.next();
			if(host.getId().equalsIgnoreCase(id))
				return host;
		}
		return null;
	}

	// TODO separate map for hosts?
	public ArrayList<IdMappingData> getIdMaps() {
		return idMaps;
	}
	
	public IdMappingData getIdMap(String fqdn) {
		ArrayList<IdMappingData> maps = getIdMaps();
		for(IdMappingData d : maps) {
			if(d.getSid().equalsIgnoreCase(fqdn))
				return d;
		}
		return null;
	}
	
	public void setIdMap(String sid, String oneId, String vepId, String ovfId) {
		ArrayList<IdMappingData> maps = getIdMaps();
		boolean found = false;
		for(IdMappingData d : maps) {
			if(d.getSid().equalsIgnoreCase(sid)) {
				found = true;
				if(oneId != null && !oneId.isEmpty())
					d.setOneId(oneId);
				if(ovfId != null && !ovfId.isEmpty())
					d.setOvfId(ovfId);
				if(vepId != null && !vepId.isEmpty())
					d.setVepId(vepId);
			}
		}
		if(!found) {
			IdMappingData d = new IdMappingData(sid);
			if(oneId != null && !oneId.isEmpty())
				d.setOneId(oneId);
			if(ovfId != null && !ovfId.isEmpty())
				d.setOvfId(ovfId);
			if(vepId != null && !vepId.isEmpty())
				d.setVepId(vepId);
			maps.add(d);
		}
	}

	public boolean clearIdMapOvf(String ovfId) {
		ArrayList<IdMappingData> maps = getIdMaps();
		for(int i=0; i<maps.size(); i++) {
			if(maps.get(i).getOvfId().equalsIgnoreCase(ovfId)) {
				maps.remove(i);
				return true;
			}
		}
		return false;
	}
}