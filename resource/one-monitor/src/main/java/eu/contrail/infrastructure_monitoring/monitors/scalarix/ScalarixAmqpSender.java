/**
	 * Copyright (c) 2011, XLAB d.o.o.
	 * All rights reserved.
	 *
	 * Redistribution and use in source and binary forms, with or without
	 * modification, are permitted provided that the following conditions are met:
	 *     * Redistributions of source code must retain the above copyright
	 *       notice, this list of conditions and the following disclaimer.
	 *     * Redistributions in binary form must reproduce the above copyright
	 *       notice, this list of conditions and the following disclaimer in the
	 *       documentation and/or other materials provided with the distribution.
	 *     * Neither the name of XLAB d.o.o. nor the
	 *       names of its contributors may be used to endorse or promote products
	 *       derived from this software without specific prior written permission.
	 *
	 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
	 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	 *
	 * @author Gregor Beslic - gregor.beslic@cloud.si
	 */

package eu.contrail.infrastructure_monitoring.monitors.scalarix;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import eu.contrail.infrastructure_monitoring.monitors.AmqpMetricsRegistration;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.data.ScalarixData;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;
import eu.contrail.infrastructure_monitoring.utils.Util;

public class ScalarixAmqpSender {
	public static final String EXCHANGE_NAME = "input.scalarix";
	public static final String ROUTING_KEY_PREFIX = "input.scalarix";
	
	public static final boolean DURABLE_FLAG = false;
	public static final boolean AUTO_DELETE_FLAG = true;
	public static final boolean INTERNAL_FLAG = false;
	
	private Document xmlDoc = null;
	private static Document xmlRegistrationDoc = null;
	
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder docBuilder = null;
	
    private static SimpleDateFormat ISO8601FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	private static Logger log = Logger.getLogger(ScalarixAmqpSender.class);
    
	private static Channel channel = null;
	private static ScalarixAmqpSender scalarixAmqpSender;
	private static boolean metricsRegistered = false;
	
	private ScalarixAmqpSender() throws IOException {
		try {
			docBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(OpenNebula.RABBIT_MQ_HOST);
        Connection connection = factory.newConnection();
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "topic", DURABLE_FLAG, AUTO_DELETE_FLAG, INTERNAL_FLAG, null);
	}	
	
    public static ScalarixAmqpSender getInstance() {
    	if(scalarixAmqpSender == null)
			try {
				scalarixAmqpSender = new ScalarixAmqpSender();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	return scalarixAmqpSender;
    }
    
    private void sendMessage(String type, String routingKey, String message) {
        String fullRoutingKey = ROUTING_KEY_PREFIX + "." + type;
        if(routingKey != null && !routingKey.isEmpty()) {
        	fullRoutingKey += routingKey;
        }
        log.trace("Publishing " + message + " with routing key " + fullRoutingKey);
    	try {
			channel.basicPublish(EXCHANGE_NAME, fullRoutingKey, null, message.getBytes());
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public void sendMessages(List<ScalarixData> nodesMetrics) {
		if(nodesMetrics.size() > 0) {
			for(ScalarixData data : nodesMetrics) {
				try {
					if(!metricsRegistered) {
						Element message = createXmlHeaderRegistration("scalarix", "ganglia");
						xmlRegistrationDoc.appendChild(message);
						
						Element erlangProcesses = createXmlMetricRegistration("erlang_processes", data.getMetricData(RawMetric.SCALARIX_GANGLIA_ERLANG_PROCESSES));
					    message.appendChild(erlangProcesses);
				    
					    Element memoryAtoms = createXmlMetricRegistration("memory_atoms", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ATOMS));
					    message.appendChild(memoryAtoms);
				    	
					    Element memoryBinaries = createXmlMetricRegistration("memory_binaries", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_BINARIES));
					    message.appendChild(memoryBinaries);
				    	
					    Element memoryDht = createXmlMetricRegistration("memory_dht", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_DHT));
					    message.appendChild(memoryDht);
				    	
					    Element memoryErlangProcesses = createXmlMetricRegistration("memory_erlang_processes", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ERLANG_PROCESSES));
					    message.appendChild(memoryErlangProcesses);
				    	
					    Element memoryEts = createXmlMetricRegistration("memory_ets", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ETS));
					    message.appendChild(memoryEts);
				    	
					    Element txSecond = createXmlMetricRegistration("transactions_per_second", data.getMetricData(RawMetric.SCALARIX_GANGLIA_TRANSACTIONS_PER_SECOND));
					    message.appendChild(txSecond);
					    
					    Element avgTxLatency = createXmlMetricRegistration("average_transaction_latency", data.getMetricData(RawMetric.SCALARIX_GANGLIA_AVG_TRANSACTION_LATENCY));
					    message.appendChild(avgTxLatency);
					    
					    Element kvPairs = createXmlMetricRegistration("kv_pairs", data.getMetricData(RawMetric.SCALARIX_GANGLIA_KV_PAIRS));
					    message.appendChild(kvPairs);
					    
					    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
					    
					    AmqpMetricsRegistration.getInstance().sendMessage("scalarix", ".ganglia", xmlRegistration);
					    metricsRegistered = true;
					}
					Element message = createXmlHeaderMetric(data.getFqdn());
					xmlDoc.appendChild(message);
			    	
					Element erlangProcesses = createXmlMetricValue("erlang_processes", data.getMetricData(RawMetric.SCALARIX_GANGLIA_ERLANG_PROCESSES));
				    message.appendChild(erlangProcesses);
			    	
				    Element memoryAtoms = createXmlMetricValue("memory_atoms", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ATOMS));
				    message.appendChild(memoryAtoms);
			    	
				    Element memoryBinaries = createXmlMetricValue("memory_binaries", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_BINARIES));
				    message.appendChild(memoryBinaries);
			    	
				    Element memoryDht = createXmlMetricValue("memory_dht", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_DHT));
				    message.appendChild(memoryDht);
			    	
				    Element memoryErlangProcesses = createXmlMetricValue("memory_erlang_processes", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ERLANG_PROCESSES));
				    message.appendChild(memoryErlangProcesses);
			    	
				    Element memoryEts = createXmlMetricValue("memory_ets", data.getMetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ETS));
				    message.appendChild(memoryEts);
			    	
				    Element txSecond = createXmlMetricValue("transactions_per_second", data.getMetricData(RawMetric.SCALARIX_GANGLIA_TRANSACTIONS_PER_SECOND));
				    message.appendChild(txSecond);
				    
				    Element avgTxLatency = createXmlMetricValue("average_transaction_latency", data.getMetricData(RawMetric.SCALARIX_GANGLIA_AVG_TRANSACTION_LATENCY));
				    message.appendChild(avgTxLatency);
				    
				    Element kvPairs = createXmlMetricValue("kv_pairs", data.getMetricData(RawMetric.SCALARIX_GANGLIA_KV_PAIRS));
				    message.appendChild(kvPairs);
				    
				    String xml = createXmlFooter(xmlDoc);
				    sendMessage(Util.prepareFqdnTopic(data.getFqdn()), ".ganglia", xml);
				    
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private Element createXmlHeaderRegistration(String source, String group) throws ParserConfigurationException {
    	xmlRegistrationDoc = docBuilder.newDocument();
        Element hostMessage = xmlRegistrationDoc.createElement("Message");
        hostMessage.setAttribute("group", group);
        hostMessage.setAttribute("source", source);
        return hostMessage;
    }
	
	private Element createXmlHeaderMetric(String sid) throws ParserConfigurationException {
    	xmlDoc = docBuilder.newDocument();
        Element hostMessage = xmlDoc.createElement("Message");
        hostMessage.setAttribute("time", ISO8601FORMAT.format(new Date()));
        //hostMessage.setAttribute("sid", sid);
        return hostMessage;
    }
    
    private String createXmlFooter(Document doc) throws TransformerException {
    	Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        return result.getWriter().toString();
    }
    
    private Element createXmlMetricValue(String metricId, MetricData metricData) {
    	Element metric = xmlDoc.createElement("Value");
    	metric.setAttribute("id", metricId);
		if(metricData != null) {
    		if(metricData.getStringValue() != null) {
        		metric.setTextContent(metricData.getStringValue());
            }
    	}
    	else
    		log.warn("MetricData is null for " + metricId);
    	
    	return metric;
    }
    
    private Element createXmlMetricRegistration(String metricId, MetricData metricData) {
    	Element metric = xmlRegistrationDoc.createElement("Metric");
    	metric.setAttribute("id", metricId);
		if(metricData != null) {
    		if(metricData.getType() != null) {
        		Element metricType = xmlRegistrationDoc.createElement("Type");
        		metricType.setTextContent(metricData.getType().name());
            	metric.appendChild(metricType);
            }
        	if(metricData.getUnit() != null) {
            	Element metricUnit = xmlRegistrationDoc.createElement("Unit");
            	metricUnit.setTextContent(metricData.getUnit());
            	metric.appendChild(metricUnit);	
        	}
    	}
    	else
    		log.warn("MetricData is null for " + metricId);
    	
    	return metric;
    }
}