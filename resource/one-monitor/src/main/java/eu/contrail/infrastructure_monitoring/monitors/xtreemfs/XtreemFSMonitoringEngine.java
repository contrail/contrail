package eu.contrail.infrastructure_monitoring.monitors.xtreemfs;

import java.util.Date;

import org.apache.log4j.Logger;

import eu.contrail.infrastructure_monitoring.monitors.IMonitoringEngine;
import eu.contrail.infrastructure_monitoring.monitors.IMonitoringEngine.Status;

public class XtreemFSMonitoringEngine {
	private static Logger log = Logger.getLogger(XtreemFSMonitoringEngine.class);
	Status status;
    Date metricsCollectedDate;
	public static String XTREEMFS_SNMP_HOST;
	public static String XTREEMFS_SNMP_PORT_DIR;
	public static String XTREEMFS_SNMP_PORT_MRC;
	public static String XTREEMFS_SNMP_PORT_OSD;
	public static String xTREEMFS_SNMP_COMMUNITY;
	
	public void report() throws Exception {
		log.trace("Collecting XtreemFS metrics data...");
        this.status = IMonitoringEngine.Status.BUSY;
        try {
            XtreemFS xtreemFS = new XtreemFS();
            XtreemFSAmqpSender.getInstance().sendMessages(xtreemFS.getServicesMetrics());
		}
        catch (Exception e) {
            this.status = IMonitoringEngine.Status.ERROR;
            log.warn("Failed to collect XtreemFS monitoring data");
        }
        metricsCollectedDate = new Date();
        this.status = IMonitoringEngine.Status.OK;
        log.trace("XtreemFS metrics data collected successfully.");
    }
}
