/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;


/**
 * Listens for OpenNebula hooks on RabbitMq queue and creates XML event reports
 * @author gregor.beslic@cloud.si
 *
 */
public class EventsSubscriber extends Thread {

	private static Logger log = Logger.getLogger(EventsSubscriber.class);
	private static final String EXCHANGE_NAME = "openNebulaHooks";
	
	@Override
	public void run() {
		try {
			ConnectionFactory factory = new ConnectionFactory();
	        factory.setHost(OpenNebula.RABBIT_MQ_HOST);
			Connection connection = factory.newConnection();
	        Channel channel = connection.createChannel();

	        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
	        String queueName = channel.queueDeclare().getQueue();
	        channel.queueBind(queueName, EXCHANGE_NAME, "");

	        log.trace(" [*] Waiting for events. To exit press CTRL+C");

	        QueueingConsumer consumer = new QueueingConsumer(channel);
	        channel.basicConsume(queueName, true, consumer);

	        while (true) {
	            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
	            String message = new String(delivery.getBody());
	            
//	            PublishEventXml slaSoiPublisher = new PublishEventXml(message.split("_"), false);
//	            slaSoiPublisher.start();
	            
	            PublishEventXml contrailPublisher = new PublishEventXml(message.split("_"), true);
	            contrailPublisher.start();
	        }
		} catch (IOException e) {
			log.error(e.getMessage());
		} catch (ShutdownSignalException e) {
			log.error(e.getMessage());
		} catch (ConsumerCancelledException e) {
			log.error(e.getMessage());
		} catch (InterruptedException e) {
			log.error(e.getMessage());
		}
	}
}
