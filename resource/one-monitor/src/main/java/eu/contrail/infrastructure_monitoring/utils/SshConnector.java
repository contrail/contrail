/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SshConnector {
	private JSch jsch = null;
	
	private static final String ROOT_SSH_USER = "root";
	private static final String ROOT_SSH_PASS = "root";
	
	private static final String MONITOR_SSH_USER = "contrail_monitor";
	private static final String MONITOR_SSH_PASS = "n4Kjh5_";
	private static final String MONITOR_SSH_PASS_HASH = "'$6$FzON0ec9$kWZHtyiTzssmFyPjlRG1n/GF5NGuaMTM0/Fn61aX9TiYcRQ0Ar83fOXZrJLj1ba1NaK2vI36JAb6xY3k5kfIu0'";
	private static final String CMD_DELETE_MONITORING_USER = "userdel " + MONITOR_SSH_USER;
	private static final String CMD_CREATE_MONITORING_USER = "useradd " + MONITOR_SSH_USER + " -p " + MONITOR_SSH_PASS_HASH + " -d /tmp";
	
	private static final int DEFAULT_TIMEOUT_SECS = 2;
	
	private String ip = null;
	private Session session = null;
	private Channel channel = null;
	
	private static Logger log = Logger.getLogger(SshConnector.class);
	
	private boolean ready = false;
	
	public SshConnector(String ip) {
		this.jsch = new JSch();
		try {
			this.ip = ip;
			initSession(MONITOR_SSH_USER, MONITOR_SSH_PASS);
			ready = true;
		} catch (JSchException e) {
			if(e.getMessage().equals("Auth fail")) {
				log.trace("SSH monitoring user does not exist - will try to create one...");
				try {
					createMonitoringUser();
					ready = true;
				} catch (JSchException e1) {
					ready = true;
					try {
						initSession(ROOT_SSH_USER, ROOT_SSH_PASS);
					} catch (JSchException e2) {
						log.warn(e2.getMessage() + " for IP " + this.ip);
					}
				}
			}
			else {
				ready = true;
				try {
					initSession(ROOT_SSH_USER, ROOT_SSH_PASS);
				} catch (JSchException e2) {
					log.warn(e2.getMessage() + " for IP " + this.ip);
				}
			}
		}
	}
	
	public boolean isReady() {
		return ready;
	}
	
	private void initSession(String user, String pass) throws JSchException {
		if(this.jsch != null) {
			this.session = null;
			log.trace("Initializing SSH session with user " + user + " on IP " + ip);
			Properties config = new Properties();
	        config.put("StrictHostKeyChecking", "no");
	        this.session = this.jsch.getSession(user, this.ip);
	        this.session.setPassword(pass);
	        this.session.setPort(22);
			this.session.setConfig(config);
	        this.session.setTimeout(DEFAULT_TIMEOUT_SECS * 1000);
	        this.session.connect();
		}
	}
	
	public void createMonitoringUser() throws JSchException {
		log.info("Creating monitoring SSH user");
		initSession(ROOT_SSH_USER, ROOT_SSH_PASS);
		// Monitoring user might have been created with a wrong password
		this.exec(CMD_DELETE_MONITORING_USER, false, false);
		this.exec(CMD_CREATE_MONITORING_USER, true, true);
		initSession(MONITOR_SSH_USER, MONITOR_SSH_PASS);
	}
	
	public String[] exec(String command, boolean closeChannelWhenDone, boolean closeSessionWhenDone) {

		if (!this.session.isConnected()) {
    		log.warn("SSH session is not connected");
    		return null;
        }

		String[] retStr = null;
		log.trace("Executing SSH command '" + command + "'");
		try {
			if(this.channel == null) {
				this.channel = (ChannelExec) this.session.openChannel("exec");
				this.channel.setInputStream(null, true);
				this.channel.setOutputStream(System.out, true);
				this.channel.setExtOutputStream(System.err, true);
		    }
			this.channel = (ChannelExec) this.session.openChannel("exec");
			((ChannelExec)this.channel).setCommand(command);
	    	this.channel.connect();
	        
	    	String s = new String();
	    	BufferedReader stdInput = new BufferedReader(new InputStreamReader(channel.getInputStream()));
	    	retStr = new String[1];
        	int i = 0;
            while ((s = stdInput.readLine()) != null) {
            	if(i >= retStr.length)
            		retStr = Util.extendArray(retStr);
            			
            	retStr[i++] = s;
            }
            
		} catch (JSchException e) {
			log.warn(e.getMessage());
			
		} catch (IOException e) {
			log.warn(e.getMessage());
		}
		
		if(closeChannelWhenDone)
			closeChannel();
		
		if(closeSessionWhenDone)
			closeSession();
		
		return retStr;
	}
	
	public void closeChannel() {
		if(this.channel != null && !channel.isClosed())  
			channel.disconnect();
	}
	
	public void closeSession() {
		if(this.session != null)  
			this.session.disconnect();
	}
}
