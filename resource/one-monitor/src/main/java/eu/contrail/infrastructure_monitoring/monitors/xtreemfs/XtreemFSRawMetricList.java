package eu.contrail.infrastructure_monitoring.monitors.xtreemfs;

import java.util.ArrayList;
import java.util.List;

import eu.contrail.infrastructure_monitoring.enums.DataType;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;

public class XtreemFSRawMetricList {
	private String metricType;
	private List<XtreemFSRawMetric> metrics;
	
	public XtreemFSRawMetricList(String metricType) {
		this.metricType = metricType;
		this.metrics = new ArrayList<XtreemFSRawMetric>();
	}
	
	public void add(String oid, RawMetric rawMetric, DataType dataType, String unit) {
		XtreemFSRawMetric xm = new XtreemFSRawMetric(oid, rawMetric, dataType, unit);
		this.metrics.add(xm);
	}
	
	public String getValue(String oid) {
		for(XtreemFSRawMetric xm : this.metrics) {
			if(xm.getOid().equalsIgnoreCase(oid))
				return xm.getValue();
		}
		return null;
	}
	
	public void update(String oid, String value) {
		for(XtreemFSRawMetric xm : this.metrics) {
			if(xm.getOid().equalsIgnoreCase(oid)) {
				xm.setValue(value);
			}
		}
	}
	
	public String getMetricType() {
		return metricType;
	}

	public void setMetricType(String metricType) {
		this.metricType = metricType;
	}

	public List<XtreemFSRawMetric> getMetrics() {
		return metrics;
	}

	public void setMetrics(List<XtreemFSRawMetric> metrics) {
		this.metrics = metrics;
	}

	class XtreemFSRawMetric {
		private RawMetric rawMetric;
		private String oid;
		private String value;
		private DataType dataType;
		private String unit;
		
		public XtreemFSRawMetric(String oid, RawMetric rawMetric, DataType dataType, String unit) {
			this.setOid(oid);
			this.setRawMetric(rawMetric);
			this.dataType = dataType;
			this.unit = unit;
		}

		public RawMetric getRawMetric() {
			return rawMetric;
		}

		public void setRawMetric(RawMetric rawMetric) {
			this.rawMetric = rawMetric;
		}

		public String getOid() {
			return oid;
		}

		public void setOid(String oid) {
			this.oid = oid;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public DataType getDataType() {
			return dataType;
		}

		public void setDataType(DataType dataType) {
			this.dataType = dataType;
		}

		public String getUnit() {
			return unit;
		}

		public void setUnit(String unit) {
			this.unit = unit;
		}	
	}
}
