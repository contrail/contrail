/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.monitors.opennebula.xmlrpc;

import java.util.HashMap;

import org.opennebula.client.vm.VirtualMachine;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

//import eu.contrail.infrastructure_monitoring.OneMonitor;
import eu.contrail.infrastructure_monitoring.monitors.Metrics;
import eu.contrail.infrastructure_monitoring.monitors.data.IdMappingData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OneMonitoringEngine;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OneVm;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;
import eu.contrail.infrastructure_monitoring.utils.Util;

public class MonitoringXmlRpcHandler extends XmlRpcHandler {
	private Gson gson = null;
	
	enum MetricType {VM, HOST};
	
	public MonitoringXmlRpcHandler() {
		this.gson = new Gson();
	}
	
	public String hostMetricsMonitorable(String jsonMetrics) {
		return metricsMonitorable(jsonMetrics, MetricType.HOST);
	}
	
	public String vmMetricsMonitorable(String jsonMetrics) {
		return metricsMonitorable(jsonMetrics, MetricType.VM);
	}
	
	public String startMonitoring(String vmOneId, String ovfVsId, String vepId) throws Exception {
		if(
			vmOneId == null
			|| vmOneId.isEmpty()
			|| ovfVsId == null
			|| ovfVsId.isEmpty()
		)
			return "bad params";
			
		while(!OpenNebula.getHasData()) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		VirtualMachine vm = OpenNebula.getInstance().getVm(vmOneId);
		if(vm != null) {
			Metrics metrics = OpenNebula.getInstance().getMetrics();
			String fqdn = metrics.getFqdnFromOneId(vmOneId);
			// VM still in pending state
			if(fqdn == null) {
				OneVm vm2 = new OneVm(vm);
				String ip = vm2.getVmIpAddress();
				fqdn = Util.createVmFqdnFromIpAndHostFqdn(ip, OpenNebula.CLUSTER_FQDN);
			}
			OpenNebula.getInstance().setIdMap(fqdn, vmOneId, vepId, ovfVsId);
			IdMappingData md = OpenNebula.getInstance().getIdMap(fqdn);
			return "ok;" + md.getFullRoutingKey();
		}
		return "Virtual machine with OpenNebula ID " + vmOneId + " does not exist";
	}
	
	public String stopMonitoringOvf(String ovfId) throws Exception {
		if(
			ovfId == null
			|| ovfId.isEmpty()
		)
			return "bad params";
			
		while(!OpenNebula.getHasData()) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(OpenNebula.getInstance().clearIdMapOvf(ovfId)) {
			return "ok;" + IdMappingData.getFullRoutingKeyByOvfId(ovfId);
		}

		return "Virtual machine with OVF ID " + ovfId + " does not exist";
	}
	
	private String metricsMonitorable(String jsonMetrics, MetricType type) {
		JsonParser parser = new JsonParser();
		JsonArray arr = (JsonArray)parser.parse(jsonMetrics);
		HashMap<String, Boolean> map = new HashMap<String, Boolean>();
		for (Object o : arr) {
			JsonPrimitive p = (JsonPrimitive) o;
			Boolean result = type.equals(MetricType.VM) ? 
					isVmMetricMonitorable(p.getAsString()) :
					isHostMetricMonitorable(p.getAsString());
			
			map.put(p.getAsString(), result);
		}
		return gson.toJson(map);
	}
	
	public Boolean isHostMetricMonitorable(String rawMetricStr) {
		try {
			RawMetric rawMetric = gson.fromJson(rawMetricStr, RawMetric.class);
			return OneMonitoringEngine.oneHostMetricsSet.contains(rawMetric) ? true : false;
		}
		catch (JsonParseException e) {
			return false;
		}
	}
	
	public Boolean isVmMetricMonitorable(String rawMetricStr) {
		try {
			RawMetric rawMetric = gson.fromJson(rawMetricStr, RawMetric.class);
			return OneMonitoringEngine.oneVmMetricsSet.contains(rawMetric) ? true : false;
		}
		catch (JsonParseException e) {
			return false;
		}
	}
	/* 
	public String getAllHostMachines() {
		waitForData();
		return gson.toJson(OneMonitor.getMonitoringEngine().getAllHostMachines());
	}

	private void waitForData() {
		while(!OpenNebula.getHasData()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void query() throws Exception {
		OneMonitor.getMonitoringEngine().query();
	}

    public String getStatus() {
    	return gson.toJson(OneMonitor.getMonitoringEngine().getStatus());
    }

    public Date getMetricsCollectedDate() {
    	waitForData();
    	return OneMonitor.getMonitoringEngine().getMetricsCollectedDate();
    }

    public String getVmMetricData(String fqdn, String rawMetricStr) throws MetricNotSupportedException {
    	RawMetric rawMetric = gson.fromJson(rawMetricStr, RawMetric.class);
    	return gson.toJson(OneMonitor.getMonitoringEngine().getVmMetricData(fqdn, rawMetric));
    }

    public String getHostMetricData(String fqdn, String rawMetricStr) throws MetricNotSupportedException {
    	RawMetric rawMetric = gson.fromJson(rawMetricStr, RawMetric.class);
    	return gson.toJson(OneMonitor.getMonitoringEngine().getHostMetricData(fqdn, rawMetric));
    }

    public boolean checkVmExists(String fqdn) {
    	return OneMonitor.getMonitoringEngine().checkVmExists(fqdn);
    }

    public boolean checkHostExists(String fqdn) {
    	return OneMonitor.getMonitoringEngine().checkHostExists(fqdn);
    }

    public String getVmHostMachine(String fqdn) {
    	return OneMonitor.getMonitoringEngine().getVmHostMachine(fqdn);
    }

    public String getGuestMachines(String hostFqdn) {
    	return gson.toJson(OneMonitor.getMonitoringEngine().getGuestMachines(hostFqdn));
    }

    public String getAllGuestMachines() {
    	return gson.toJson(OneMonitor.getMonitoringEngine().getAllGuestMachines());
    }

    public String getInfrastructureLayoutXml() {
    	return OneMonitor.getMonitoringEngine().getInfrastructureLayoutXml();
    }

	//public boolean isVmMonitoringDataAvailable(String[] fqdns){}
	
	*/
}
