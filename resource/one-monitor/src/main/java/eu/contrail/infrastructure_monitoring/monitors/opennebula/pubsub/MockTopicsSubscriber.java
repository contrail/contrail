/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */


package eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;

public class MockTopicsSubscriber extends Thread {
	private static final String bindingKey = "input.opennebula.resources.network.#";
	
	private static Logger log = Logger.getLogger(MockTopicsSubscriber.class);
	
    public void run() {
    	ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(OpenNebula.RABBIT_MQ_HOST);
        Connection connection;
		try {
			connection = factory.newConnection();
			Channel channel = connection.createChannel();

		    channel.exchangeDeclare(AmqpSender.EXCHANGE_NAME, "topic", AmqpSender.DURABLE_FLAG, AmqpSender.AUTO_DELETE_FLAG, AmqpSender.INTERNAL_FLAG, null);
			
	        String queueName = channel.queueDeclare().getQueue();
	        
	        channel.queueBind(queueName, AmqpSender.EXCHANGE_NAME, bindingKey);

	        log.trace(" [*] Mock topics subscriber waiting for messages. To exit press CTRL+C");

	        QueueingConsumer consumer = new QueueingConsumer(channel);
	        channel.basicConsume(queueName, true, consumer);

	        while (true) {
	            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
	            String message = new String(delivery.getBody());
	            String routingKey = delivery.getEnvelope().getRoutingKey();

	            log.trace(" [x] Received '" + routingKey + "':'" + message + "'");
	        }
		} catch (IOException e) {
			log.error(e.getMessage());
		} catch (ShutdownSignalException e) {
			log.error(e.getMessage());
		} catch (ConsumerCancelledException e) {
			log.error(e.getMessage());
		} catch (InterruptedException e) {
			log.error(e.getMessage());
		}
        
    }
}
