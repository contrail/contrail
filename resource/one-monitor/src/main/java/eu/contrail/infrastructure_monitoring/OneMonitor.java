/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import eu.contrail.infrastructure_monitoring.monitors.opennebula.OneExporter;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OneMonitoringEngine;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub.AmqpSender;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub.EventsSubscriber;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub.XmlBuilder;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.socket.OneSocketServer;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.xmlrpc.MonitoringXmlRpcServer;
import eu.contrail.infrastructure_monitoring.monitors.scalarix.ScalarixMonitoringEngine;
import eu.contrail.infrastructure_monitoring.monitors.xtreemfs.XtreemFSMonitoringEngine;
import eu.contrail.infrastructure_monitoring.utils.Util;

public class OneMonitor {
	
	private static final String PROPS_FILE_DEFAULT = "/etc/contrail/contrail-one-monitor/one-monitor.config";
	private static String PROPS_FILE = null;
	private static final int SHUTDOWN_TIME = 1;
	private static OneMonitoringEngine oneMonitoringEngine = null;
	private static ScalarixMonitoringEngine scalarixMonitoringEngine = null;
	private static XtreemFSMonitoringEngine xtreemFSMonitoringEngine = null;
	private static boolean socketServer = false;
	private static boolean xmlRpcServer = false;
	private static OneSocketServer oneSocket;
	
	private static int REFRESH_FREQ;
    
    private static Logger log = Logger.getLogger(OneMonitor.class);
    
	public static void main(String[] args) {
		if(args != null && args.length > 0 && !args[0].isEmpty())
			new OneMonitor(args[0]);
		else
			new OneMonitor(null);
	}
	
	private OneMonitor(String propsFile) {
		try {
			Properties properties = new Properties();
			PROPS_FILE = propsFile == null || propsFile.isEmpty() ? PROPS_FILE_DEFAULT : propsFile;
			properties.load(new FileInputStream(PROPS_FILE));
			REFRESH_FREQ = Integer.parseInt(properties.getProperty("monitor_refresh_secs"));
			OpenNebula.ONE_RPC_HOST = properties.getProperty("one_rpc_host");
			OpenNebula.ONE_RPC_PORT = properties.getProperty("one_rpc_port");
			OpenNebula.ONE_ADMIN_USERNAME = properties.getProperty("one_admin_user");
			OpenNebula.ONE_ADMIN_PASSWORD = properties.getProperty("one_admin_password");
			OpenNebula.RABBIT_MQ_HOST = properties.getProperty("rabbit_mq_host");
			OpenNebula.RABBIT_ACK_TIMEOUT_SECS = Integer.parseInt(properties.getProperty("rabbit_ack_timeout_secs"));
			OpenNebula.CLUSTER_FQDN = properties.getProperty("cluster_fqdn");
			OpenNebula.SEND_TO_MONITORING_HUB = properties.getProperty("send_to_monitoring_hub");
			OpenNebula.FEDERATION_FINAGLE_HOST = properties.getProperty("federation_finagle_host");
			OpenNebula.FEDERATION_FINAGLE_PORT = properties.getProperty("federation_finagle_port");
			OpenNebula.FEDERATION_FINAGLE_CLIENT_CONN = properties.getProperty("federation_finagle_client_conn");
			OpenNebula.PROVIDER_ID = properties.getProperty("provider_id");
			
			OpenNebula.LOG4J_LOG_LEVEL = properties.getProperty("log4j_log_level");
			if(!OpenNebula.LOG4J_LOG_LEVEL.isEmpty()) {
				if(OpenNebula.LOG4J_LOG_LEVEL.equalsIgnoreCase("info")) {
					LogManager.getRootLogger().setLevel((Level)Level.INFO);
				}
				else if(OpenNebula.LOG4J_LOG_LEVEL.equalsIgnoreCase("debug")) {
					LogManager.getRootLogger().setLevel((Level)Level.DEBUG);
				}
				else if(OpenNebula.LOG4J_LOG_LEVEL.equalsIgnoreCase("trace")) {
					LogManager.getRootLogger().setLevel((Level)Level.TRACE);
				}
			}
			
			if(Util.isPropertyTrue(properties.getProperty("scalarix_ganglia"))) {
				ScalarixMonitoringEngine.GANGLIA_HOST = properties.getProperty("ganglia_host");
				ScalarixMonitoringEngine.GANGLIA_PORT = properties.getProperty("ganglia_port");
				scalarixMonitoringEngine = new ScalarixMonitoringEngine();
			}
			
			if(Util.isPropertyTrue(properties.getProperty("xtreemfs_snmp"))) {
				XtreemFSMonitoringEngine.XTREEMFS_SNMP_HOST = properties.getProperty("xtreemfs_snmp_host");
				XtreemFSMonitoringEngine.XTREEMFS_SNMP_PORT_DIR = properties.getProperty("xtreemfs_snmp_port_dir");
				XtreemFSMonitoringEngine.XTREEMFS_SNMP_PORT_MRC = properties.getProperty("xtreemfs_snmp_port_mrc");
				XtreemFSMonitoringEngine.XTREEMFS_SNMP_PORT_OSD = properties.getProperty("xtreemfs_snmp_port_osd");
				XtreemFSMonitoringEngine.xTREEMFS_SNMP_COMMUNITY = properties.getProperty("xtreemfs_snmp_community");
				xtreemFSMonitoringEngine = new XtreemFSMonitoringEngine();
			}
			
			if(Util.isPropertyTrue(properties.getProperty("socket_server"))) {
				socketServer = true;
				OpenNebula.SOCKET_SERVER_PORT = Integer.parseInt(properties.getProperty("socket_server_port"));
			}
			if(Util.isPropertyTrue(properties.getProperty("xmlrpc_server"))) {
				xmlRpcServer = true;
				OpenNebula.XMLRPC_SERVER_PORT = Integer.parseInt(properties.getProperty("xmlrpc_server_port"));
			}
			
			Thread runtimeHookThread = new Thread() {
				public void run() {
				    shutdownHook(); 
				}
			};
			
			Runtime.getRuntime().addShutdownHook (runtimeHookThread);
			oneMonitoringEngine = new OneMonitoringEngine();
			log.trace("Contrail monitoring started...");
			
			if(socketServer) {
				oneSocket = new OneSocketServer();
				oneSocket.start();
			}
			if(xmlRpcServer) {
				new MonitoringXmlRpcServer();
			}
			
			Thread auditRecordsSubscriber = new EventsSubscriber();
			auditRecordsSubscriber.start();
//			Thread testTopicsSubscriber = new MockTopicsSubscriber();
//			testTopicsSubscriber.start();
			
			OneExporter oneExporter = new OneExporter();
			XmlBuilder amqpData = new XmlBuilder();
			log.info("Monitor started ... to exit press CTRL+C");
			while (true) {
				try {
					oneMonitoringEngine.query();
					
					if(scalarixMonitoringEngine != null)
						scalarixMonitoringEngine.report();

					if(xtreemFSMonitoringEngine != null)
						xtreemFSMonitoringEngine.report();
				
				} catch (Exception e) {
				    e.printStackTrace();
				}
				//log.trace(oneMonitoringEngine.getInfrastructureLayoutXml());
				//log.trace(oneExporter.getVmLayoutXml());
				//log.trace(oneExporter.getClusterConfigurationXml());

                AmqpSender.getInstance().sendMessages(amqpData.getFresh());
				Thread.sleep (REFRESH_FREQ * 1000);
			}
		
		} catch (FileNotFoundException e) {
			System.out.println("USAGE: First parameter should be the full path of the config file (" + PROPS_FILE_DEFAULT + ")");
			System.exit(5);
		}
		catch (Exception e) {
			log.error(e.getMessage());
		}
    }
  
	public static OneMonitoringEngine getMonitoringEngine() {
		return oneMonitoringEngine;
	}
	
    private static void shutdownHook() {
		long t0 = System.currentTimeMillis();
		while (true) {
		    try {
		    	if(oneSocket.isListening()) {
			    	log.info("Stopping socket server...");
			    	oneSocket.stopServer();
		    	}
		    }
		    catch (Exception e) {
		    	log.error("Exception: " +e.toString());
		    	break;
		    }
		    if (System.currentTimeMillis() - t0 > SHUTDOWN_TIME * 1000)
		    		break;
		}
		log.trace("Shutdown completed");
    }
}