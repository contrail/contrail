/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */


package eu.contrail.infrastructure_monitoring.monitors.opennebula;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import eu.contrail.infrastructure_monitoring.exceptions.MetricNotSupportedException;
import eu.contrail.infrastructure_monitoring.monitors.Metrics;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;


public class OneExporter {
    private static String infrastructureLayoutXml;
    private static String vmLayoutXml;
    private static String clusterConfigurationXml;
    
    
    private static SimpleDateFormat ISO8601FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	
    private static OneExporter oneObject;
	private static Logger log = Logger.getLogger(OneExporter.class);
    
 	public static synchronized OneExporter getInstance() {
 		if(oneObject == null)
 			oneObject = new OneExporter();
 		return oneObject;
 	}
 	
 	public Object clone() throws CloneNotSupportedException {
 		throw new CloneNotSupportedException();
 	}
    
    public String getInfrastructureLayoutXml() {
    	try {
			createInfrastructureLayoutXml();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return infrastructureLayoutXml;
    }
    
    public String getVmLayoutXml() {
    	try {
			createVmLayoutXml();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return vmLayoutXml;
    }
    
    public String getClusterConfigurationXml() {
    	try {
			createClusterConfigurationXml();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return clusterConfigurationXml;
    }
    
	private void createInfrastructureLayoutXml() throws InterruptedException, ParserConfigurationException, MetricNotSupportedException, TransformerFactoryConfigurationError, TransformerException {
        while(!OpenNebula.getHasData()) {
        	Thread.sleep(100);
        }
        
    	log.trace("Creating infrastructure layout xml.");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
        Document infrastructureDoc = docBuilder.newDocument();
        Element root = infrastructureDoc.createElement("Infrastructure");
        infrastructureDoc.appendChild(root);
        Node networkEntityCluster = createNetworkEntity(infrastructureDoc, "OpenNebula", OpenNebula.CLUSTER_FQDN, "cluster");
        root.appendChild(networkEntityCluster);
        
        List<String> hosts = OpenNebula.getInstance().getMetrics().getVmLayoutData().getHostList();
        for (int i = 0; i < hosts.size(); i++) {
            String hostFqdn = hosts.get(i);
            MetricData hostMetricData = OpenNebula.getInstance().getMetrics().getHostMetricData(hostFqdn, RawMetric.HOSTNAME);
            Node networkEntityHost = createNetworkEntity(infrastructureDoc, hostMetricData.getStringValue(), hostFqdn, "host");
            networkEntityCluster.appendChild(networkEntityHost);
            List<String> vms = OpenNebula.getInstance().getMetrics().getGuestMachines(hostFqdn);
        	for (int j = 0; j < vms.size(); j++) {
                String vmFqdn = vms.get(j);
                MetricData vmMetricData = OpenNebula.getInstance().getMetrics().getVmMetricData(vmFqdn, RawMetric.HOSTNAME);
                String val = "N/A";
                if (vmMetricData != null)
                	val = vmMetricData.getStringValue();
                Node networkEntityVm = createNetworkEntity(infrastructureDoc, val, vmFqdn, "vm");
                networkEntityHost.appendChild(networkEntityVm);
            }
        }

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(infrastructureDoc);
        transformer.transform(source, result);

        infrastructureLayoutXml = result.getWriter().toString();
        log.trace("Infrastructure layout xml created successfully.");
    }
    
    private void createVmLayoutXml() throws InterruptedException, ParserConfigurationException, MetricNotSupportedException, TransformerFactoryConfigurationError, TransformerException {
        while(!OpenNebula.getHasData()) {
        	Thread.sleep(100);
        }
    	log.trace("Creating VM layout xml.");
    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    	DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
		Document vmLayoutDoc = docBuilder.newDocument();
        Element message = vmLayoutDoc.createElement("Message");
        message.setAttribute("time", ISO8601FORMAT.format(new Date()));
        message.setAttribute("type", "VmLayout");
        vmLayoutDoc.appendChild(message);
        
        Element networkEntities = vmLayoutDoc.createElement("NetworkEntities");
        message.appendChild(networkEntities);
        
        List<String> hosts = OpenNebula.getInstance().getMetrics().getVmLayoutData().getHostList();
        for (int i = 0; i < hosts.size(); i++) {
            // TODO: fqdn
        	String hostFqdn = hosts.get(i);
            Metrics hostMetrics = OpenNebula.getInstance().getMetrics();
            
            Node networkEntityHost = createNetworkEntityVmLayoutHost(
            		vmLayoutDoc,
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.HOSTNAME).getStringValue(),
            		hostFqdn,
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.CPU_CORES_COUNT).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.CPU_SPEED).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.LOAD_ONE).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.MEM_TOTAL).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.AVAILABILITY_STATUS).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.CPU_CORES_COUNT).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.CPU_SPEED).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.MEM_TOTAL).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.SHARED_IMAGES_DISK_FREE).getStringValue(),
            		hostMetrics.getHostMetricData(hostFqdn, RawMetric.SHARED_IMAGES_DISK_USED).getStringValue()
            		);
            networkEntities.appendChild(networkEntityHost);
            List<String> vms = OpenNebula.getInstance().getMetrics().getGuestMachines(hostFqdn);
        	for (int j = 0; j < vms.size(); j++) {
                String vmFqdn = vms.get(j);
                Metrics vmMetrics = OpenNebula.getInstance().getMetrics();
				String disks =
						"[{'uri': '" + vmMetrics.getVmMetricData(vmFqdn, RawMetric.VM_IMAGE_TEMPLATE).getStringValue() +
						"', 'persistent': " + vmMetrics.getVmMetricData(vmFqdn, RawMetric.VM_PERSISTENCE).getStringValue() + "}]";

				String hostname = vmMetrics.getVmMetricData(vmFqdn, RawMetric.HOSTNAME) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.HOSTNAME).getStringValue();
                String vmState = vmMetrics.getVmMetricData(vmFqdn, RawMetric.VM_STATE) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.VM_STATE).getStringValue();
                String coresCount = vmMetrics.getVmMetricData(vmFqdn, RawMetric.CPU_CORES_COUNT) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.CPU_CORES_COUNT).getStringValue();
                String cpuSpeed = vmMetrics.getVmMetricData(vmFqdn, RawMetric.CPU_SPEED) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.CPU_SPEED).getStringValue();
                String memTotal = vmMetrics.getVmMetricData(vmFqdn, RawMetric.MEM_TOTAL) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.MEM_TOTAL).getStringValue();
                String idOwner = vmMetrics.getVmMetricData(vmFqdn, RawMetric.ID_OWNER) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.ID_OWNER).getStringValue();
                String cpuLoad = vmMetrics.getVmMetricData(vmFqdn, RawMetric.CPU_LOAD) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.CPU_LOAD).getStringValue();
                String memUsage = vmMetrics.getVmMetricData(vmFqdn, RawMetric.MEM_USAGE) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.MEM_USAGE).getStringValue();
                String cpuShare = vmMetrics.getVmMetricData(vmFqdn, RawMetric.CPU_SHARE) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.CPU_SHARE).getStringValue();
                String memHost = vmMetrics.getVmMetricData(vmFqdn, RawMetric.MEM_HOST) == null ? "N/A" : vmMetrics.getVmMetricData(vmFqdn, RawMetric.MEM_HOST).getStringValue();
                
				Node networkEntityVm = createNetworkEntityVmLayoutVm(
                		vmLayoutDoc,
                		hostname,
                		vmFqdn,
                		vmState,
                		cpuLoad,
                		cpuShare,
                		memHost,
                		memUsage,
                		coresCount,
                		cpuSpeed,
                		disks,
                		memTotal,
                		idOwner
                );
                networkEntityHost.appendChild(networkEntityVm);
            }
        }

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(vmLayoutDoc);
        transformer.transform(source, result);

        vmLayoutXml = result.getWriter().toString();
        log.trace("VM layout xml created successfully.");
    }
    
	private Node createNetworkEntityVmLayoutVm(Document vmLayoutDoc, String name, String fqdn, 
			String state, String cpu_load, String cpu_share, String mem_host, String mem_usage, String cpu_cores, String cpu_speed, 
			String disks, String memory, String userId) {
	
		Element node = vmLayoutDoc.createElement("NetworkEntity");
        node.setAttribute("name", name);
        node.setAttribute("fqdn", fqdn);
        node.setAttribute("type", "vm");
        node.setAttribute("state", state);
        node.setAttribute("cpu_load", cpu_load);
        node.setAttribute("cpu_share", cpu_share);
        node.setAttribute("mem_host", mem_host);
        node.setAttribute("mem_usage", mem_usage);
        node.setAttribute("cpu_cores", cpu_cores);
        node.setAttribute("cpu_speed", cpu_speed);
        node.setAttribute("disks", disks);
        node.setAttribute("memory", memory);
        node.setAttribute("userId", userId);
    
        return node;
	}

	private Node createNetworkEntityVmLayoutHost(Document vmLayoutDoc, String name, String fqdn,
			String cpu_cores, String cpu_speed, String load_one, String memory, String up, 
			String used_cpu_cores, String used_cpu_speed, String used_memory, 
			String disk_available, String disk_used) {
        
		Element node = vmLayoutDoc.createElement("NetworkEntity");
        node.setAttribute("name", name);
        node.setAttribute("fqdn", fqdn);
        node.setAttribute("type", "host");
        node.setAttribute("cpu_cores", cpu_cores);
        node.setAttribute("cpu_speed", cpu_speed);
        node.setAttribute("load_one", load_one);
        node.setAttribute("memory", memory);
        node.setAttribute("up", up);
        node.setAttribute("used_cpu_cores", used_cpu_cores);
        node.setAttribute("used_cpu_speed", used_cpu_speed);
        node.setAttribute("used_memory", used_memory);
        node.setAttribute("disk_available", disk_available);
        node.setAttribute("disk_used", disk_used);
        
        return node;
	}

	private Node createNetworkEntity(Document doc, String name, String fqdn, String type) {
        Element node = doc.createElement("NetworkEntity");
        node.setAttribute("name", name);
        node.setAttribute("fqdn", fqdn);
        node.setAttribute("type", type);
        return node;
    }
	
	private void createClusterConfigurationXml() throws ParserConfigurationException, MetricNotSupportedException, TransformerFactoryConfigurationError, TransformerException, InterruptedException {
		while(!OpenNebula.getHasData()) {
        	Thread.sleep(100);
        }
		
		log.trace("Creating cluster configuration xml.");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    	DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
		Document clusterConfigurationDoc = docBuilder.newDocument();
        Element message = clusterConfigurationDoc.createElement("Message");
        message.setAttribute("time", ISO8601FORMAT.format(new Date()));
        message.setAttribute("type", "ClusterConfiguration");
        clusterConfigurationDoc.appendChild(message);
        
        Element clusterConfiguration = clusterConfigurationDoc.createElement("ClusterConfiguration");
        clusterConfiguration.setAttribute("fqdn", OpenNebula.CLUSTER_FQDN);
        message.appendChild(clusterConfiguration);
        
        List<String> hosts = OpenNebula.getInstance().getMetrics().getClusterConfData().getHostList();
        for (int i = 0; i < hosts.size(); i++) {
            String hostFqdn = hosts.get(i);
            Metrics hostMetrics = OpenNebula.getInstance().getMetrics();
            
            Element node = clusterConfigurationDoc.createElement("Host");
            node.setAttribute("id", hostMetrics.getHostMetricData(hostFqdn, RawMetric.HOSTNAME).getStringValue());
            node.setAttribute("fqdn", hostFqdn);
            
            Element aud = clusterConfigurationDoc.createElement("Auditability");
            aud.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.AUDITABILITY).getStringValue());
            node.appendChild(aud);
            
            Element loc = clusterConfigurationDoc.createElement("Location");
            Element cc = clusterConfigurationDoc.createElement("CountryCode");
            loc.appendChild(cc);
            cc.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.LOCATION).getStringValue());
            node.appendChild(loc);
            
            Element sas = clusterConfigurationDoc.createElement("SAS70");
            sas.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.SAS70_COMPLIANCE).getStringValue());
            node.appendChild(sas);
            
            Element ccr = clusterConfigurationDoc.createElement("CCR");
            ccr.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.CCR).getStringValue());
            node.appendChild(ccr);
            
            Element dc = clusterConfigurationDoc.createElement("DataClassification");
            dc.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.DATA_CLASSIFICATION).getStringValue());
            node.appendChild(dc);
            
            Element hwr = clusterConfigurationDoc.createElement("HWRedundancyLevel");
            hwr.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.HW_REDUNDANCY_LEVEL).getStringValue());
            node.appendChild(hwr);
            
            Element dt = clusterConfigurationDoc.createElement("DiskThroughput");
            dt.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.DISK_THROUGHPUT).getStringValue());
            node.appendChild(dt);
            
            Element nt = clusterConfigurationDoc.createElement("NetThroughput");
            nt.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.NET_THROUGHPUT).getStringValue());
            node.appendChild(nt);
            
            Element de = clusterConfigurationDoc.createElement("DataEncryption");
            de.setTextContent(hostMetrics.getHostMetricData(hostFqdn, RawMetric.DATA_ENCRYPTION).getStringValue());
            node.appendChild(de);

            clusterConfiguration.appendChild(node);
        }

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(clusterConfigurationDoc);
        transformer.transform(source, result);

        clusterConfigurationXml = result.getWriter().toString();
        log.trace("Cluster configuration xml created successfully.");
	}
}
