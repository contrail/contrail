package eu.contrail.infrastructure_monitoring.monitors.scalarix;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

public class ScalarixGangliaMonitor {
	private String xml;
	private static Logger log = Logger.getLogger(ScalarixGangliaMonitor.class);
	
	public ScalarixGangliaMonitor(String host, String port) {
		Socket socket;
		try {
			socket = new Socket(host, Integer.parseInt(port));
			log.trace("Connected to Ganglia on host " + host + " and port " + port);
            InputStream in = socket.getInputStream();
        	
	        StringWriter writer = new StringWriter();
	        IOUtils.copy(in, writer, "utf-8");
	        
	        if(writer != null && !writer.toString().isEmpty()) {
	        	setXml(writer.toString());
	        }
	        in.close();
		    socket.close();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SocketException e) {
			if(e.getMessage().equalsIgnoreCase("Connection reset")) {
				log.trace("Finished reading Ganglia metrics");	
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}
}
