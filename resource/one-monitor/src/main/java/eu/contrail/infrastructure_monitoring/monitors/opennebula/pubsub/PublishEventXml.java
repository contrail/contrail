/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import eu.contrail.infrastructure_monitoring.monitors.opennebula.OneVm;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;

public class PublishEventXml extends Thread {
	
	private String[] parts;
	private boolean structured;
	private String xml = null;
	
	private static Logger log = Logger.getLogger(PublishEventXml.class);
	private static SimpleDateFormat ISO8601FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	
	public PublishEventXml(String[] parts, boolean structured) {
		super("PublishEventXml");
		this.structured = structured;
		this.parts = parts;
	}
	
	public void run() {
		createXml();
        sendXml();
    }
		
	private void sendXml() {
		log.info("Sending xml...");
		AmqpSender.getInstance().sendEventMessage("", xml);
	}
	
	private void createXml() {
		log.info("Creating XML");
    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
		try {
			docBuilder = dbFactory.newDocumentBuilder();
			Document auditRecordDoc = docBuilder.newDocument();
	        Element message = auditRecordDoc.createElement("Message");
	        if(!structured) {
	        	message.setAttribute("id", "none");
	        	message.setAttribute("userId", "none");
		    }
	        message.setAttribute("time", ISO8601FORMAT.format(new Date()));
	        if(parts[0].equalsIgnoreCase("audit"))
	        	message.setAttribute("type", "auditRecord");
	        else
	        	message.setAttribute("type", "scheduler");
	        
	        auditRecordDoc.appendChild(message);
	        Node action = auditRecordDoc.createElement("Action");
	        
	    	// Used for Contrail
	        if(structured) {
	        	if(((String)parts[1]).equalsIgnoreCase("vm")) {
	        		Element vmId = auditRecordDoc.createElement("vmId");
	        		vmId.setTextContent(parts[3]);
	        		action.appendChild(vmId);
	        		
	        		OneVm oneVm = new OneVm(OpenNebula.getInstance().getVm(parts[3]));
	        		
	        		Element userId = auditRecordDoc.createElement("userId");
	        		String vmOwner = oneVm.getVmOwner();
	        		if(vmOwner != null)
	        			userId.setTextContent(vmOwner);
	        		action.appendChild(userId);
	        	
	        		Element host = auditRecordDoc.createElement("host");
	        		String vmHostname = oneVm.getVmHostname();
	        		if(vmHostname != null)
	        			host.setTextContent(vmHostname);
	        		action.appendChild(host);
	        	}
	        	else {
	        		Element hostId = auditRecordDoc.createElement("hostId");
	        		hostId.setTextContent(parts[3]);
	        		action.appendChild(hostId);
	        	
	        		Element host = auditRecordDoc.createElement("host");
	        		host.setTextContent(OpenNebula.getInstance().getHost(parts[3]).getName());
	        		action.appendChild(host);
	        	}
	        	
        		Element event = auditRecordDoc.createElement("event");
        		event.setTextContent(parts[2].toLowerCase());
        		action.appendChild(event);
	        }
	        // Used for Sla@Soi
	        else {
	        String actionVal = "";
		        if(((String)parts[1]).equalsIgnoreCase("vm")) {
		        	OneVm oneVm = new OneVm(OpenNebula.getInstance().getVm(parts[3]));
		        	
		        	// TODO: fqdn
		        	actionVal += "VM " + parts[3] + " on host " + oneVm.getVmHostname() + " ";
		        	if(((String)parts[2]).equalsIgnoreCase("create")) {
		        		actionVal += "is created";
		        	}
		        	else if(((String)parts[2]).equalsIgnoreCase("running")) {
		        		actionVal += "is running";
		        	}
		        	else if(((String)parts[2]).equalsIgnoreCase("done")) {
		        		actionVal += "is deleted or shutdown";
		        	}
		        	else if(((String)parts[2]).equalsIgnoreCase("shutdown")) {
		        		actionVal += "is shutdown";
		        	}
		        	else if(((String)parts[2]).equalsIgnoreCase("stop")) {
		        		actionVal += "is stopped";
		        	}
		        	else if(((String)parts[2]).equalsIgnoreCase("failed")) {
		        		actionVal += "failed";
		        	}
		        	else {
		        		log.warn("Unknown VM message type: " + parts[2]);
		        		return;
		        	}
		        }
		        else if(((String)parts[1]).equalsIgnoreCase("host")) {
		        	// TODO: fqdn
		        	actionVal += "Host " + parts[3] + " ";
		        	if(((String)parts[2]).equalsIgnoreCase("error")) {
		        		actionVal += "encountered an error";
		        	}
		        	else if(((String)parts[2]).equalsIgnoreCase("create")) {
		        		actionVal += "is created";
		        	}
		        	else if(((String)parts[2]).equalsIgnoreCase("disable")) {
		        		actionVal += "is disabled";
		        	}
		        	else {
		        		log.warn("Unknown Host message type: " + parts[2]);
		        		return;
		        	}
		        }
		        else {
		        	log.warn("Unknown message entity: " + parts[1]);
		        	return;
		        }
		        action.setTextContent(actionVal);
		    }
	        
	        message.appendChild(action);
	        Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

	        StreamResult result = new StreamResult(new StringWriter());
	        DOMSource source = new DOMSource(auditRecordDoc);
	        transformer.transform(source, result);
	        
	        log.info("XML created successfully");
	        this.xml = result.getWriter().toString();
	        log.info(this.xml);
	        
		} catch (ParserConfigurationException e1) {
			log.error(e1.getMessage());
		} catch (TransformerConfigurationException e) {
			log.error(e.getMessage());
		} catch (TransformerFactoryConfigurationError e) {
			log.error(e.getMessage());
		} catch (TransformerException e) {
			log.error(e.getMessage());
		}
	}
	
	public String getXml() {
		return xml;
	}
}
