/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */


package eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import eu.contrail.infrastructure_monitoring.exceptions.MetricNotSupportedException;
import eu.contrail.infrastructure_monitoring.monitors.AmqpMetricsRegistration;
import eu.contrail.infrastructure_monitoring.monitors.Metrics;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.federation.FederationRouting;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OneExporter;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;
import eu.contrail.infrastructure_monitoring.utils.Util;

public class XmlBuilder {
    Document xmlDoc = null;
    Document xmlRegistrationDoc = null;
    
    List<AmqpData> amqpData = null;
    Metrics metrics = null;
    
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = null;
    
    private static SimpleDateFormat ISO8601FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	
    // Holds the FQDNs of Hosts and VMs whose metrics have already been registered
    private static Map<String, ArrayList<String>> hostMetrics = new HashMap<String, ArrayList<String>>();
    private static Map<String, ArrayList<String>> vmMetrics = new HashMap<String, ArrayList<String>>();
		
    private static Logger log = Logger.getLogger(OneExporter.class);
    
	public XmlBuilder() {
		try {
			docBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		amqpData = new ArrayList<AmqpData>();
	}
	
	public List<AmqpData> getFresh() {
		amqpData.clear();
		metrics = OpenNebula.getInstance().getMetrics();
	    try {
			createAllAmqpXmls();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MetricNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return amqpData;
	}
	
	private AmqpData createHostCommonData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
		String key = "hostCommonMetricsRegistered";
		
		if(!hostMetrics.containsKey(key))
			hostMetrics.put(key, new ArrayList<String>());
		
		if(!hostMetrics.get(key).contains(fqdn)){
        	Element message = createXmlHeaderRegistration("host", "common", fqdn);
            xmlRegistrationDoc.appendChild(message);
        	Element hostname = createXmlMetricRegistration("hostname", metrics.getHostMetricData(fqdn, RawMetric.HOSTNAME));
    	    message.appendChild(hostname);
    	    
        	Element availability = createXmlMetricRegistration("availability", metrics.getHostMetricData(fqdn, RawMetric.AVAILABILITY_STATUS));
    	    message.appendChild(availability);
        	
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
		    AmqpMetricsRegistration.getInstance().sendMessage("host", Util.prepareFqdnTopic(fqdn) + ".common", xmlRegistration);
		    hostMetrics.get(key).add(fqdn);
        }
		Element message = createXmlHeaderMetric(fqdn);
        xmlDoc.appendChild(message);
    	
        Element hostname = createXmlMetricValue("hostname", metrics.getHostMetricData(fqdn, RawMetric.HOSTNAME));
	    message.appendChild(hostname);
    	
    	Element availability = createXmlMetricValue("availability", metrics.getHostMetricData(fqdn, RawMetric.AVAILABILITY_STATUS));
	    message.appendChild(availability);
    	
	    String routingKey = Util.prepareFqdnTopic(fqdn) + ".common";
	    String fedKey = "host." + routingKey; 
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedKey, content))
	    	log.info("Metric " + fedKey + " sent to federation.");
	    
	    return new AmqpData("host", routingKey, content);
    }
	
	private AmqpData createHostClusterData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
		String key = "hostClusterMetricsRegistered";
		
		if(!hostMetrics.containsKey(key))
			hostMetrics.put(key, new ArrayList<String>());
		
		if(!hostMetrics.get(key).contains(fqdn)){
        	Element message = createXmlHeaderRegistration("host", "cluster", fqdn);
            xmlRegistrationDoc.appendChild(message);
        	
            Element sas70 = createXmlMetricRegistration("sas70_compliance", metrics.getHostMetricData(fqdn, RawMetric.SAS70_COMPLIANCE));
    	    message.appendChild(sas70);
        	
    	    Element auditability = createXmlMetricRegistration("auditability", metrics.getHostMetricData(fqdn, RawMetric.AUDITABILITY));
    	    message.appendChild(auditability);
        	
    	    Element location = createXmlMetricRegistration("location", metrics.getHostMetricData(fqdn, RawMetric.LOCATION));
    	    message.appendChild(location);
    	    
    	    Element ccr = createXmlMetricRegistration("ccr", metrics.getHostMetricData(fqdn, RawMetric.CCR));
    	    message.appendChild(ccr);

    	    Element dc = createXmlMetricRegistration("data_classification", metrics.getHostMetricData(fqdn, RawMetric.DATA_CLASSIFICATION));
    	    message.appendChild(dc);

    	    Element de = createXmlMetricRegistration("data_encryption", metrics.getHostMetricData(fqdn, RawMetric.DATA_ENCRYPTION));
    	    message.appendChild(de);
    	    
    	    Element hr = createXmlMetricRegistration("hardware_redundancy_level", metrics.getHostMetricData(fqdn, RawMetric.HW_REDUNDANCY_LEVEL));
    	    message.appendChild(hr);

    	    Element dt = createXmlMetricRegistration("disk_throughput", metrics.getHostMetricData(fqdn, RawMetric.DISK_THROUGHPUT));
    	    message.appendChild(dt);

    	    Element nt = createXmlMetricRegistration("net_throughput", metrics.getHostMetricData(fqdn, RawMetric.NET_THROUGHPUT));
    	    message.appendChild(nt);
    	    
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
		    AmqpMetricsRegistration.getInstance().sendMessage("host", Util.prepareFqdnTopic(fqdn) + ".cluster", xmlRegistration);
		    hostMetrics.get(key).add(fqdn);
        }
		Element message = createXmlHeaderMetric(fqdn);
        xmlDoc.appendChild(message);
    	
        Element sas70 = createXmlMetricValue("sas70_compliance", metrics.getHostMetricData(fqdn, RawMetric.SAS70_COMPLIANCE));
	    message.appendChild(sas70);
    	
	    Element auditability = createXmlMetricValue("auditability", metrics.getHostMetricData(fqdn, RawMetric.AUDITABILITY));
	    message.appendChild(auditability);
    	
	    Element location = createXmlMetricValue("location", metrics.getHostMetricData(fqdn, RawMetric.LOCATION));
	    message.appendChild(location);
	    
	    Element ccr = createXmlMetricValue("ccr", metrics.getHostMetricData(fqdn, RawMetric.CCR));
	    message.appendChild(ccr);

	    Element dc = createXmlMetricValue("data_classification", metrics.getHostMetricData(fqdn, RawMetric.DATA_CLASSIFICATION));
	    message.appendChild(dc);

	    Element de = createXmlMetricValue("data_encryption", metrics.getHostMetricData(fqdn, RawMetric.DATA_ENCRYPTION));
	    message.appendChild(de);
	    
	    Element hr = createXmlMetricValue("hardware_redundancy_level", metrics.getHostMetricData(fqdn, RawMetric.HW_REDUNDANCY_LEVEL));
	    message.appendChild(hr);

	    Element dt = createXmlMetricValue("disk_throughput", metrics.getHostMetricData(fqdn, RawMetric.DISK_THROUGHPUT));
	    message.appendChild(dt);

	    Element nt = createXmlMetricValue("net_throughput", metrics.getHostMetricData(fqdn, RawMetric.NET_THROUGHPUT));
	    message.appendChild(nt);
    	
	    String routingKey = Util.prepareFqdnTopic(fqdn) + ".cluster";
	    String fedKey = "host." + routingKey; 
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedKey, content))
	    	log.info("Metric " + fedKey + " sent to federation.");
	    
	    return new AmqpData("host", routingKey, content);
    }
	
    private AmqpData createHostMemoryData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "hostMemoryMetricsRegistered";
		
		if(!hostMetrics.containsKey(key))
			hostMetrics.put(key, new ArrayList<String>());
		
		if(!hostMetrics.get(key).contains(fqdn)){
        	Element message = createXmlHeaderRegistration("host", "memory", fqdn);
            xmlRegistrationDoc.appendChild(message);
        	
            Element memTotal = createXmlMetricRegistration("total", metrics.getHostMetricData(fqdn, RawMetric.MEM_TOTAL));
    	    message.appendChild(memTotal);
        	
        	Element memUsed = createXmlMetricRegistration("used", metrics.getHostMetricData(fqdn, RawMetric.MEM_USAGE));
    	    message.appendChild(memUsed);
        	
    	    Element memFree = createXmlMetricRegistration("free", metrics.getHostMetricData(fqdn, RawMetric.MEM_FREE));
    	    message.appendChild(memFree);
    	     
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
		    AmqpMetricsRegistration.getInstance().sendMessage("host", Util.prepareFqdnTopic(fqdn) + ".memory", xmlRegistration);
		    hostMetrics.get(key).add(fqdn);
        }
    	Element message = createXmlHeaderMetric(fqdn);
        xmlDoc.appendChild(message);
    	
        Element memTotal = createXmlMetricValue("total", metrics.getHostMetricData(fqdn, RawMetric.MEM_TOTAL));
	    message.appendChild(memTotal);
    	
    	Element memUsed = createXmlMetricValue("used", metrics.getHostMetricData(fqdn, RawMetric.MEM_USAGE));
	    message.appendChild(memUsed);
    	
	    Element memFree = createXmlMetricValue("free", metrics.getHostMetricData(fqdn, RawMetric.MEM_FREE));
	    message.appendChild(memFree);
    	
	    String routingKey = Util.prepareFqdnTopic(fqdn) + ".memory";
	    String fedKey = "host." + routingKey; 
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedKey, content))
	    	log.info("Metric " + fedKey + " sent to federation.");
	    
	    return new AmqpData("host", routingKey, content);
    }

    private AmqpData createHostCpuData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "hostCpuMetricsRegistered";
		
		if(!hostMetrics.containsKey(key))
			hostMetrics.put(key, new ArrayList<String>());
		
		if(!hostMetrics.get(key).contains(fqdn)){
        	Element message = createXmlHeaderRegistration("host", "cpu", fqdn);
            xmlRegistrationDoc.appendChild(message);
        	
            Element cpuCores = createXmlMetricRegistration("cores", metrics.getHostMetricData(fqdn, RawMetric.CPU_CORES_COUNT));
    	    message.appendChild(cpuCores);
        	
        	Element cpuSpeed = createXmlMetricRegistration("speed", metrics.getHostMetricData(fqdn, RawMetric.CPU_SPEED));
    	    message.appendChild(cpuSpeed);
        	
    	    Element cpuUser = createXmlMetricRegistration("user", metrics.getHostMetricData(fqdn, RawMetric.CPU_USER));
    	    message.appendChild(cpuUser);
        	
    	    Element cpuSystem = createXmlMetricRegistration("system", metrics.getHostMetricData(fqdn, RawMetric.CPU_SYSTEM));
    	    message.appendChild(cpuSystem);
        	
    	    Element cpuIdle = createXmlMetricRegistration("idle", metrics.getHostMetricData(fqdn, RawMetric.CPU_IDLE));
    	    message.appendChild(cpuIdle);
        	
    	    Element loadOne = createXmlMetricRegistration("load_one", metrics.getHostMetricData(fqdn, RawMetric.LOAD_ONE));
    	    message.appendChild(loadOne);
        	
    	    Element loadFive = createXmlMetricRegistration("load_five", metrics.getHostMetricData(fqdn, RawMetric.LOAD_FIVE));
    	    message.appendChild(loadFive);

            Element loadOneNorm = createXmlMetricRegistration("load_one_norm", metrics.getHostMetricData(fqdn,
                    RawMetric.LOAD_ONE_NORM));
            message.appendChild(loadOneNorm);

            Element loadFiveNorm = createXmlMetricRegistration("load_five_norm", metrics.getHostMetricData(fqdn,
                    RawMetric.LOAD_FIVE_NORM));
            message.appendChild(loadFiveNorm);

    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
		    AmqpMetricsRegistration.getInstance().sendMessage("host", Util.prepareFqdnTopic(fqdn) + ".cpu", xmlRegistration);
		    hostMetrics.get(key).add(fqdn);
        }
    	Element message = createXmlHeaderMetric(fqdn);
        xmlDoc.appendChild(message);
    	
        Element cpuCores = createXmlMetricValue("cores", metrics.getHostMetricData(fqdn, RawMetric.CPU_CORES_COUNT));
	    message.appendChild(cpuCores);
    	
    	Element cpuSpeed = createXmlMetricValue("speed", metrics.getHostMetricData(fqdn, RawMetric.CPU_SPEED));
	    message.appendChild(cpuSpeed);
    	
	    Element cpuUser = createXmlMetricValue("user", metrics.getHostMetricData(fqdn, RawMetric.CPU_USER));
	    message.appendChild(cpuUser);
    	
	    Element cpuSystem = createXmlMetricValue("system", metrics.getHostMetricData(fqdn, RawMetric.CPU_SYSTEM));
	    message.appendChild(cpuSystem);
    	
	    Element cpuIdle = createXmlMetricValue("idle", metrics.getHostMetricData(fqdn, RawMetric.CPU_IDLE));
	    message.appendChild(cpuIdle);
    	
	    Element loadOne = createXmlMetricValue("load_one", metrics.getHostMetricData(fqdn, RawMetric.LOAD_ONE));
	    message.appendChild(loadOne);
    	
	    Element loadFive = createXmlMetricValue("load_five", metrics.getHostMetricData(fqdn, RawMetric.LOAD_FIVE));
	    message.appendChild(loadFive);

        Element loadOneNorm = createXmlMetricValue("load_one_norm", metrics.getHostMetricData(fqdn,
                RawMetric.LOAD_ONE_NORM));
        message.appendChild(loadOneNorm);

        Element loadFiveNorm = createXmlMetricValue("load_five_norm", metrics.getHostMetricData(fqdn,
                RawMetric.LOAD_FIVE_NORM));
        message.appendChild(loadFiveNorm);

	    String routingKey = Util.prepareFqdnTopic(fqdn) + ".cpu";
	    String fedKey = "host." + routingKey; 
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedKey, content))
	    	log.info("Metric " + fedKey + " sent to federation.");
	    
	    return new AmqpData("host", routingKey, content);
    }
    
    private AmqpData createHostDiskData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "hostDiskMetricsRegistered";
		
		if(!hostMetrics.containsKey(key))
			hostMetrics.put(key, new ArrayList<String>());
		
		if(!hostMetrics.get(key).contains(fqdn)){
        	Element message = createXmlHeaderRegistration("host", "disk", fqdn);
            xmlRegistrationDoc.appendChild(message);
        	
            Element av = createXmlMetricRegistration("available", metrics.getHostMetricData(fqdn, RawMetric.SHARED_IMAGES_DISK_FREE));
    	    message.appendChild(av);
        	
        	Element us = createXmlMetricRegistration("used", metrics.getHostMetricData(fqdn, RawMetric.SHARED_IMAGES_DISK_USED));
    	    message.appendChild(us);
    	    
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
		    AmqpMetricsRegistration.getInstance().sendMessage("host", Util.prepareFqdnTopic(fqdn) + ".disk", xmlRegistration);
		    hostMetrics.get(key).add(fqdn);
        }
    	Element message = createXmlHeaderMetric(fqdn);
        xmlDoc.appendChild(message);
    	
        Element av = createXmlMetricValue("available", metrics.getHostMetricData(fqdn, RawMetric.SHARED_IMAGES_DISK_FREE));
	    message.appendChild(av);
    	
    	Element us = createXmlMetricValue("used", metrics.getHostMetricData(fqdn, RawMetric.SHARED_IMAGES_DISK_USED));
	    message.appendChild(us);
	    
	    String routingKey = Util.prepareFqdnTopic(fqdn) + ".disk";
	    String fedKey = "host." + routingKey; 
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedKey, content))
	    	log.info("Metric " + fedKey + " sent to federation.");
	    
	    return new AmqpData("host", routingKey, content);
    }
    
    private AmqpData createHostNetworkData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "hostNetworkMetricsRegistered";
		
		if(!hostMetrics.containsKey(key))
			hostMetrics.put(key, new ArrayList<String>());
		
		if(!hostMetrics.get(key).contains(fqdn)){
        	Element message = createXmlHeaderRegistration("host", "network", fqdn);
            xmlRegistrationDoc.appendChild(message);

            Element rx = createXmlMetricRegistration("rx", metrics.getHostMetricData(fqdn, RawMetric.NET_BYTES_RX));
    	    message.appendChild(rx);
            
            Element tx = createXmlMetricRegistration("tx", metrics.getHostMetricData(fqdn, RawMetric.NET_BYTES_TX));
    	    message.appendChild(tx);
        	
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
		    AmqpMetricsRegistration.getInstance().sendMessage("host", Util.prepareFqdnTopic(fqdn) + ".network", xmlRegistration);
		    hostMetrics.get(key).add(fqdn);
        }
    	Element message = createXmlHeaderMetric(fqdn);
        xmlDoc.appendChild(message);
    	
        Element rx = createXmlMetricValue("rx", metrics.getHostMetricData(fqdn, RawMetric.NET_BYTES_RX));
	    message.appendChild(rx);
    	
    	Element tx = createXmlMetricValue("tx", metrics.getHostMetricData(fqdn, RawMetric.NET_BYTES_TX));
	    message.appendChild(tx);
    	
	    String routingKey = Util.prepareFqdnTopic(fqdn) + ".network";
	    String fedKey = "host." + routingKey; 
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedKey, content))
	    	log.info("Metric " + fedKey + " sent to federation.");
	    
	    return new AmqpData("host", routingKey, content);
    }

    private AmqpData createVmCommonData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "vmCommonMetricsRegistered";
		
		if(!vmMetrics.containsKey(key))
			vmMetrics.put(key, new ArrayList<String>());
		
		if(!vmMetrics.get(key).contains(fqdn) && metrics.checkVmMonitorable(fqdn)) {

			Element message = createXmlHeaderRegistration("vm", "common", fqdn);
        	xmlRegistrationDoc.appendChild(message);
        	
        	Element vmState = createXmlMetricRegistration("state", metrics.getVmMetricData(fqdn, RawMetric.VM_STATE));
    	    message.appendChild(vmState);
        	
        	Element hostname = createXmlMetricRegistration("hostname", metrics.getVmMetricData(fqdn, RawMetric.HOSTNAME));
    	    message.appendChild(hostname);
    	    
    	    Element persistence = createXmlMetricRegistration("persistence", metrics.getVmMetricData(fqdn, RawMetric.VM_PERSISTENCE));
    	    message.appendChild(persistence);
    	    
    	    Element imageTemplate = createXmlMetricRegistration("image_template", metrics.getVmMetricData(fqdn, RawMetric.VM_IMAGE_TEMPLATE));
    	    message.appendChild(imageTemplate);
    	    
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
    	    String routingKey =  OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
    	    
		    AmqpMetricsRegistration.getInstance().sendMessage("vm", routingKey + ".common", xmlRegistration);
		    vmMetrics.get(key).add(fqdn);
    	}
    	Element message = createXmlHeaderMetric(fqdn);
    	xmlDoc.appendChild(message);
    	
    	Element vmState = createXmlMetricValue("state", metrics.getVmMetricData(fqdn, RawMetric.VM_STATE));
	    message.appendChild(vmState);
    	
    	Element hostname = createXmlMetricValue("hostname", metrics.getVmMetricData(fqdn, RawMetric.HOSTNAME));
	    message.appendChild(hostname);
	    
	    Element persistence = createXmlMetricValue("persistence", metrics.getVmMetricData(fqdn, RawMetric.VM_PERSISTENCE));
	    message.appendChild(persistence);
	    
	    Element imageTemplate = createXmlMetricValue("image_template", metrics.getVmMetricData(fqdn, RawMetric.VM_IMAGE_TEMPLATE));
	    message.appendChild(imageTemplate);
	    
	    String routingKey = "";
	    if(metrics.checkVmMonitorable(fqdn))
	    	routingKey += OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
	    else
	    	routingKey += Util.prepareFqdnTopic(fqdn);
	    
	    String fedRoute = "vm." + routingKey + ".common";
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedRoute, content))
	    	log.info("Metric " + fedRoute + " sent to federation.");
	    
    	return new AmqpData("vm", routingKey + ".common", content);
    }
    
    private AmqpData createVmNetworkData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "vmNetworkMetricsRegistered";
		
		if(!vmMetrics.containsKey(key))
			vmMetrics.put(key, new ArrayList<String>());
		
		if(!vmMetrics.get(key).contains(fqdn) && metrics.checkVmMonitorable(fqdn)) {

			Element message = createXmlHeaderRegistration("vm", "network", fqdn);
			xmlRegistrationDoc.appendChild(message);
        	
        	Element ip = createXmlMetricRegistration("ip", metrics.getVmMetricData(fqdn, RawMetric.IP_ADDRESS));
    	    message.appendChild(ip);
    	    
    	    Element rx = createXmlMetricRegistration("rx", metrics.getVmMetricData(fqdn, RawMetric.NET_BYTES_RX));
    	    message.appendChild(rx);
    	    
    	    Element tx = createXmlMetricRegistration("tx", metrics.getVmMetricData(fqdn, RawMetric.NET_BYTES_TX));
    	    message.appendChild(tx);
    	    
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
    	    String routingKey = OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
    	    
		    AmqpMetricsRegistration.getInstance().sendMessage("vm", routingKey + ".network", xmlRegistration);
		    vmMetrics.get(key).add(fqdn);
    	}
    	Element message = createXmlHeaderMetric(fqdn);
    	xmlDoc.appendChild(message);
    	
    	Element ip = createXmlMetricValue("ip", metrics.getVmMetricData(fqdn, RawMetric.IP_ADDRESS));
	    message.appendChild(ip);
	    
	    Element rx = createXmlMetricValue("rx", metrics.getVmMetricData(fqdn, RawMetric.NET_BYTES_RX));
	    message.appendChild(rx);
	    
	    Element tx = createXmlMetricValue("tx", metrics.getVmMetricData(fqdn, RawMetric.NET_BYTES_TX));
	    message.appendChild(tx);
	    
	    String routingKey = "";
	    if(metrics.checkVmMonitorable(fqdn))
	    	routingKey += OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
	    else
	    	routingKey += Util.prepareFqdnTopic(fqdn);
	    
	    String fedRoute = "vm." + routingKey + ".network";
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedRoute, content))
	    	log.info("Metric " + fedRoute + " sent to federation.");
	    
    	return new AmqpData("vm", routingKey + ".network", createXmlFooter(xmlDoc));
    }
    
    private AmqpData createVmMemoryData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "vmMemoryMetricsRegistered";
		
		if(!vmMetrics.containsKey(key))
			vmMetrics.put(key, new ArrayList<String>());
		
		if(!vmMetrics.get(key).contains(fqdn) && metrics.checkVmMonitorable(fqdn)) {

			Element message = createXmlHeaderRegistration("vm", "memory", fqdn);
			xmlRegistrationDoc.appendChild(message);
        	
        	Element memTotal = createXmlMetricRegistration("total", metrics.getVmMetricData(fqdn, RawMetric.MEM_TOTAL));
    	    message.appendChild(memTotal);
        	
        	Element memUsed = createXmlMetricRegistration("used", metrics.getVmMetricData(fqdn, RawMetric.MEM_USAGE));
    	    message.appendChild(memUsed);
        	
    	    Element memFree = createXmlMetricRegistration("free", metrics.getVmMetricData(fqdn, RawMetric.MEM_FREE));
    	    message.appendChild(memFree);
       	
    	    Element memHost = createXmlMetricRegistration("host", metrics.getVmMetricData(fqdn, RawMetric.MEM_HOST));
    	    message.appendChild(memHost);
    	    
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
    	    String routingKey =  OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
    	    
		    AmqpMetricsRegistration.getInstance().sendMessage("vm", routingKey + ".memory", xmlRegistration);
		    vmMetrics.get(key).add(fqdn);
    	}
    	Element message = createXmlHeaderMetric(fqdn);
    	xmlDoc.appendChild(message);
    	
    	Element memTotal = createXmlMetricValue("total", metrics.getVmMetricData(fqdn, RawMetric.MEM_TOTAL));
	    message.appendChild(memTotal);
    	
    	Element memUsed = createXmlMetricValue("used", metrics.getVmMetricData(fqdn, RawMetric.MEM_USAGE));
	    message.appendChild(memUsed);
    	
	    Element memFree = createXmlMetricValue("free", metrics.getVmMetricData(fqdn, RawMetric.MEM_FREE));
	    message.appendChild(memFree);
   	
	    Element memHost = createXmlMetricValue("host", metrics.getVmMetricData(fqdn, RawMetric.MEM_HOST));
	    message.appendChild(memHost);
   	    
	    String routingKey = "";
	    if(metrics.checkVmMonitorable(fqdn))
	    	routingKey += OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
	    else
	    	routingKey += Util.prepareFqdnTopic(fqdn);
	    
	    String fedRoute = "vm." + routingKey + ".memory";
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedRoute, content))
	    	log.info("Metric " + fedRoute + " sent to federation.");
	    
    	return new AmqpData("vm", routingKey + ".memory", createXmlFooter(xmlDoc));
    }
    
    private AmqpData createVmCpuData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "vmCpuMetricsRegistered";
		
		if(!vmMetrics.containsKey(key))
			vmMetrics.put(key, new ArrayList<String>());
		
		if(!vmMetrics.get(key).contains(fqdn) && metrics.checkVmMonitorable(fqdn)) {

			Element message = createXmlHeaderRegistration("vm", "cpu", fqdn);
			xmlRegistrationDoc.appendChild(message);
        	
            Element cpuCores = createXmlMetricRegistration("cores", metrics.getVmMetricData(fqdn, RawMetric.CPU_CORES_COUNT));
    	    message.appendChild(cpuCores);
        	
        	Element cpuSpeed = createXmlMetricRegistration("speed", metrics.getVmMetricData(fqdn, RawMetric.CPU_SPEED));
    	    message.appendChild(cpuSpeed);
    	    
    	    Element cpuLoad = createXmlMetricRegistration("load", metrics.getVmMetricData(fqdn, RawMetric.CPU_LOAD));
    	    message.appendChild(cpuLoad);
    	    
    	    Element cpuShare = createXmlMetricRegistration("share", metrics.getVmMetricData(fqdn, RawMetric.CPU_SHARE));
    	    message.appendChild(cpuShare);
    	    
    	    Element cpuSeconds = createXmlMetricRegistration("seconds", metrics.getVmMetricData(fqdn, RawMetric.CPU_SECONDS));
    	    message.appendChild(cpuSeconds);
        
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
    	    String routingKey = OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
    	    
		    AmqpMetricsRegistration.getInstance().sendMessage("vm", routingKey + ".cpu", xmlRegistration);
		    vmMetrics.get(key).add(fqdn);
        }
    	Element message = createXmlHeaderMetric(fqdn);
        xmlDoc.appendChild(message);
    	
        Element cpuCores = createXmlMetricValue("cores", metrics.getVmMetricData(fqdn, RawMetric.CPU_CORES_COUNT));
	    message.appendChild(cpuCores);
    	
    	Element cpuSpeed = createXmlMetricValue("speed", metrics.getVmMetricData(fqdn, RawMetric.CPU_SPEED));
	    message.appendChild(cpuSpeed);
	    
	    Element cpuLoad = createXmlMetricValue("load", metrics.getVmMetricData(fqdn, RawMetric.CPU_LOAD));
	    message.appendChild(cpuLoad);
	    
	    Element cpuShare = createXmlMetricValue("share", metrics.getVmMetricData(fqdn, RawMetric.CPU_SHARE));
	    message.appendChild(cpuShare);
	    
	    Element cpuSeconds = createXmlMetricValue("seconds", metrics.getVmMetricData(fqdn, RawMetric.CPU_SECONDS));
	    message.appendChild(cpuSeconds);
	    
	    String routingKey = "";
	    if(metrics.checkVmMonitorable(fqdn))
	    	routingKey += OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
	    else
	    	routingKey += Util.prepareFqdnTopic(fqdn);
	    
	    String fedRoute = "vm." + routingKey + ".cpu";
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedRoute, content))
	    	log.info("Metric " + fedRoute + " sent to federation.");
	    
    	return new AmqpData("vm", routingKey + ".cpu", createXmlFooter(xmlDoc));
    }
    
    private AmqpData createVmDiskData(String fqdn) throws ParserConfigurationException, TransformerException, DOMException, MetricNotSupportedException {
    	String key = "vmDiskMetricsRegistered";
		
		if(!vmMetrics.containsKey(key))
			vmMetrics.put(key, new ArrayList<String>());
		
		if(!vmMetrics.get(key).contains(fqdn) && metrics.checkVmMonitorable(fqdn)) {

			Element message = createXmlHeaderRegistration("vm", "disk", fqdn);
			xmlRegistrationDoc.appendChild(message);
        	
        	Element free = createXmlMetricRegistration("free", metrics.getVmMetricData(fqdn, RawMetric.DISK_FREE));
    	    message.appendChild(free);
        	
        	Element total = createXmlMetricRegistration("total", metrics.getVmMetricData(fqdn, RawMetric.DISK_TOTAL));
    	    message.appendChild(total);
        
    	    String xmlRegistration = createXmlFooter(xmlRegistrationDoc);
		    
    	    String routingKey = OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
    	    
		    AmqpMetricsRegistration.getInstance().sendMessage("vm", routingKey + ".disk", xmlRegistration);
		    vmMetrics.get(key).add(fqdn);  
    	}
    	Element message = createXmlHeaderMetric(fqdn);
    	xmlDoc.appendChild(message);
    	
    	Element free = createXmlMetricValue("free", metrics.getVmMetricData(fqdn, RawMetric.DISK_FREE));
	    message.appendChild(free);
    	
    	Element total = createXmlMetricValue("total", metrics.getVmMetricData(fqdn, RawMetric.DISK_TOTAL));
	    message.appendChild(total);
    	
	    String routingKey = "";
	    if(metrics.checkVmMonitorable(fqdn))
	    	routingKey += OpenNebula.getInstance().getIdMap(fqdn).getRoutingKey();
	    else
	    	routingKey += Util.prepareFqdnTopic(fqdn);
	    
	    String fedRoute = "vm." + routingKey + ".disk";
	    String content = createXmlFooter(xmlDoc);
	    if(FederationRouting.getInstance().maybeSendToFederation(fedRoute, content))
	    	log.info("Metric " + fedRoute + " sent to federation.");
	    
    	return new AmqpData("vm", routingKey + ".disk", createXmlFooter(xmlDoc));
    }
    
    private Element createXmlMetricValue(String metricId, MetricData metricData) {
    	Element metric = xmlDoc.createElement("Value");
    	metric.setAttribute("id", metricId);
		if(metricData != null) {
    		if(metricData.getStringValue() != null) {
        		metric.setTextContent(metricData.getStringValue());
            }
    	}
    	else
    		log.warn("MetricData is null for " + metricId);
    	
    	return metric;
    }
    
    private Element createXmlMetricRegistration(String metricId, MetricData metricData) {
    	Element metric = xmlRegistrationDoc.createElement("Metric");
    	metric.setAttribute("id", metricId);
		if(metricData != null) {
    		if(metricData.getType() != null) {
        		Element metricType = xmlRegistrationDoc.createElement("Type");
            	metricType.setTextContent(metricData.getType().name());
            	metric.appendChild(metricType);
            }
        	if(metricData.getUnit() != null) {
            	Element metricUnit = xmlRegistrationDoc.createElement("Unit");
            	metricUnit.setTextContent(metricData.getUnit());
            	metric.appendChild(metricUnit);	
        	}
    	}
    	else
    		log.warn("MetricData is null for " + metricId);
    	
    	return metric;
    }

    private Element createXmlHeaderMetric(String sid) throws ParserConfigurationException {
    	xmlDoc = docBuilder.newDocument();
        Element hostMessage = xmlDoc.createElement("Message");
        hostMessage.setAttribute("time", ISO8601FORMAT.format(new Date()));
        
        hostMessage.setAttribute("sid", Util.prepareFqdnTopic(sid));
        if(OpenNebula.getInstance().getIdMap(sid) != null) {
        	if(
        		OpenNebula.getInstance().getIdMap(sid).getOvfId() != null
        		&& !OpenNebula.getInstance().getIdMap(sid).getOvfId().isEmpty()
        	)
            	hostMessage.setAttribute("ovf_id", OpenNebula.getInstance().getIdMap(sid).getOvfId());
       
          	if(
          		OpenNebula.getInstance().getIdMap(sid).getVepId() != null
          		&& !OpenNebula.getInstance().getIdMap(sid).getVepId().isEmpty()
          	)
          		hostMessage.setAttribute("vep_id", OpenNebula.getInstance().getIdMap(sid).getVepId());	
        }
        
        return hostMessage;
    }

    private Element createXmlHeaderRegistration(String source, String group, String sid) throws ParserConfigurationException {
    	xmlRegistrationDoc = docBuilder.newDocument();
        Element hostMessage = xmlRegistrationDoc.createElement("Message");
        hostMessage.setAttribute("group", group);
        hostMessage.setAttribute("source", source);
        hostMessage.setAttribute("sid", Util.prepareFqdnTopic(sid));
        
        if(OpenNebula.getInstance().getIdMap(sid) != null) {
        	if(
            	OpenNebula.getInstance().getIdMap(sid).getOvfId() != null
            	&& !OpenNebula.getInstance().getIdMap(sid).getOvfId().isEmpty()
            )
            	hostMessage.setAttribute("ovf_id", OpenNebula.getInstance().getIdMap(sid).getOvfId());
       
          	if(
          		OpenNebula.getInstance().getIdMap(sid).getVepId() != null
          		&& !OpenNebula.getInstance().getIdMap(sid).getVepId().isEmpty()
          	)
          		hostMessage.setAttribute("vep_id", OpenNebula.getInstance().getIdMap(sid).getVepId());
        }
        
        return hostMessage;
    }

    private String createXmlFooter(Document doc) throws TransformerException {
    	Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        return result.getWriter().toString();
    }
	
    private void createAllAmqpXmls() throws ParserConfigurationException, MetricNotSupportedException, TransformerFactoryConfigurationError, TransformerException {
    	log.trace("Creating Amqp XMLs.");
    	List<String> hosts = OpenNebula.getInstance().getMetrics().getVmLayoutData().getHostList();
    	AmqpData dh,dv;
        for (int i = 0; i < hosts.size(); i++) {
        	String hostFqdn = hosts.get(i);
            // Skip host data for "virtual" host named "temp"
        	if(!hostFqdn.equals("temp")) {
            	dh = createHostCommonData(hostFqdn);
            	log.trace("Adding host common XML " + dh.getXml());
            	amqpData.add(dh);
            	
            	dh = createHostMemoryData(hostFqdn);
            	log.trace("Adding host memory XML " + dh.getXml());
                amqpData.add(dh);
                
                dh = createHostCpuData(hostFqdn);
                log.trace("Adding host cpu XML " + dh.getXml());
                amqpData.add(dh);
                
                dh = createHostDiskData(hostFqdn);
                log.trace("Adding host disk XML " + dh.getXml());
                amqpData.add(dh);
                
                dh = createHostNetworkData(hostFqdn);
                log.trace("Adding host network XML " + dh.getXml());
                amqpData.add(dh);
                
                dh = createHostClusterData(hostFqdn);
                log.trace("Adding host cluster XML " + dh.getXml());
                amqpData.add(dh);            	
            }
            
            List<String> vms = OpenNebula.getInstance().getMetrics().getGuestMachines(hostFqdn);
        	for (int j = 0; j < vms.size(); j++) {
        		String vmFqdn = vms.get(j);
                dv = createVmCommonData(vmFqdn);
            	log.trace("Adding VM common XML " + dv.getXml());
                amqpData.add(dv);
                
                dv = createVmMemoryData(vmFqdn);
                log.trace("Adding VM memory XML " + dv.getXml());
                amqpData.add(dv);
                
                dv = createVmCpuData(vmFqdn);
                log.trace("Adding VM cpu XML " + dv.getXml());
                amqpData.add(dv);
                
                dv = createVmNetworkData(vmFqdn);
                log.trace("Adding VM network XML " + dv.getXml());
                amqpData.add(dv);
                
                dv = createVmDiskData(vmFqdn);
                log.trace("Adding VM disk XML " + dv.getXml());
                amqpData.add(dv);
                log.trace("create amqp xml for vm " + j + " on host " + i);
            }
        }
        FederationRouting.getInstance().printRoutes();
    	log.info("Successfully created Amqp XMLs.");
    }
}
