/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.opennebula.client.vm.VirtualMachine;


public class Util {
	private static Logger log = Logger.getLogger(Util.class);
	
	public static String[] shellCmd(String command) throws IOException {
	    String[] cmd = {"/bin/bash", "-c", command};
        Process p = Runtime.getRuntime().exec(cmd);
        String s = null;
    	BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        while ((s = stdError.readLine()) != null) {
        	log.warn("Shell command STD_ERR: " + s);
        }
        s = null;
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String[] retStr = new String[1];
        int i = 0;
        while ((s = stdInput.readLine()) != null) {
        	if(i >= retStr.length)
        		retStr = Util.extendArray(retStr);
        			
        	retStr[i++] = s;
        }
        p.destroy();
		return retStr;
    }
    
    public static String[] extendArray(String[] arr) {
    	String[] rtn = new String[arr.length+1];
    	int i = 0;
    	for (String s : arr) {
    		rtn[i++] = s;
    	}
    	return rtn;
    }
    
	public static float round(float rval, int rpl) {
		float p = (float) Math.pow(10, rpl);
		rval = rval * p;
		float tmp = Math.round(rval);
		return (float)tmp/p;
	}

	private static String createVmHostnameFromIp(String ipAddress) {
		return "vm-" + ipAddress.replace('.', '-');
	}
	
	public static String createVmName(VirtualMachine vm) {
		if(vm != null)
			return "one-" + vm.getId();
		return "null";
	}

	public static String createVmFqdnFromIpAndHostFqdn(String ipAddress, String hostFqdn) {
		String domain = "";
		if(hostFqdn != null) {
			String[] split = hostFqdn.split("\\.");
		    for(int i=1; i<split.length; i++) {
		    	domain = domain + "." + split[i];
		    }
		}
		else
			log.error("Host fqdn is null!");
		
		return createVmHostnameFromIp(ipAddress) + domain;
	}
	
	public static boolean isPropertyTrue(String value) {
		if(
			value != null &&
			!value.isEmpty() &&
			(
				value.equalsIgnoreCase("yes")
				|| value.equalsIgnoreCase("y")
				|| value.equalsIgnoreCase("1")
				|| value.equalsIgnoreCase("on")
			)
		)
			return true;
		
		return false;
	}
	
	
	public static String prepareFqdnTopic(String fqdn) {
		return fqdn.replace('-', '_').replace('.', '-').replace('@', '|');
	}
}
