package eu.contrail.infrastructure_monitoring.monitors.federation;

import java.net.InetSocketAddress;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.handler.codec.http.DefaultHttpRequest;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpVersion;

import com.twitter.finagle.Service;
import com.twitter.finagle.builder.ClientBuilder;
import com.twitter.finagle.http.Http;
import com.twitter.util.FutureEventListener;

import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;
import eu.contrail.infrastructure_monitoring.utils.Util;

public class FederationRouting {
	
	//private static final String PROVIDER_ID = "92effa30-efa1-11e2-b778-0800200c9a66";
	private static String PROVIDER_ID;
	private static ArrayList<String> federationRoutes = new ArrayList<String>();
	
	private static Service<HttpRequest, HttpResponse> httpClient;
	private static FederationRouting federationRouting = null;
	private static Logger log = Logger.getLogger(FederationRouting.class);
	
	private static String federationFinagleServer = null;
	private static String federationFinaglePort = null;
	private static boolean sendToHub = false;
	
	private static PendingFutures pending = null;
	
	public static boolean canSendToHub() {
		return sendToHub;
	}
	
	public static void sendToHub(boolean s) {
		sendToHub = s;
	}
	
	public static FederationRouting getInstance() {
		if(federationRouting == null)
			return new FederationRouting();
		
		return federationRouting;
	}
	
	private FederationRouting () {
		if(!Util.isPropertyTrue(OpenNebula.SEND_TO_MONITORING_HUB))
			return;
		
		PROVIDER_ID = OpenNebula.PROVIDER_ID;
		sendToHub(true);
		federationFinagleServer = OpenNebula.FEDERATION_FINAGLE_HOST;
		federationFinaglePort = OpenNebula.FEDERATION_FINAGLE_PORT;
		int hcl = OpenNebula.FEDERATION_FINAGLE_CLIENT_CONN.isEmpty() ?
				50 :
				Integer.parseInt(OpenNebula.FEDERATION_FINAGLE_CLIENT_CONN);
		
		setFinagleClient(ClientBuilder.safeBuild(
				ClientBuilder.get()
		             .codec(Http.get())
		             .hosts(new InetSocketAddress(federationFinagleServer, Integer.parseInt(federationFinaglePort)))
		             .hostConnectionLimit(hcl)
		             ));
		
		if(pending == null) {
			pending = new PendingFutures();
			log.info("Starting pending future");
			pending.start();
		}
	}
	
	public boolean maybeSendToFederation(String routingKey, String content) {
		if(sendToHub) {
			for(String m : federationRoutes) {
				if(m.equalsIgnoreCase(routingKey)) {
					HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.PUT, "/metrics/route/" + routingKey);
					// TODO: Replace with real provider ID
			    	request.setHeader("X-Provider-ID", PROVIDER_ID);
				    byte[] cntBytes = content.getBytes();
				    request.setHeader("Content-Length", cntBytes.length);
				    ChannelBuffer buffer = ChannelBuffers.copiedBuffer(cntBytes);
					request.setContent(buffer);
					httpClient.apply(request);
					log.trace("Sending metric to federation using /metrics/route/" + routingKey + " with content: " + content);
				}
			}	
		}
		return false;
	}
	
    public void registerRouteOnFederation(String fullRoutingKey) {
    	HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.PUT, "/offers/route/" + fullRoutingKey);
	    // TODO: Replace with real provider ID
    	request.setHeader("X-Provider-ID", PROVIDER_ID);
	    if(!PendingFutures.setPendingRegistration(fullRoutingKey)) {
			//log.info("Pending registration exists: " + fullRoutingKey);
	    }
	    else {
	    	log.info("Adding event listener");
	    	httpClient.apply(request).addEventListener(new HubEventListener(fullRoutingKey));
	    }
    }
    
    public void printRoutes() {
    	if(federationRoutes != null && !federationRoutes.isEmpty()) {
    		log.trace("Received responses for the following futures:");
    		for(String s : federationRoutes) {
    			log.trace(s);
    		}
    	}
    }

	private static class HubEventListener implements FutureEventListener<HttpResponse> {

		private String fullRoutingKey = null;
		
		public HubEventListener(String fullRoutingKey) {
			this.fullRoutingKey = fullRoutingKey;
		}
		
   		public void onSuccess(HttpResponse response) {
   			log.trace("Received Finagle response for route " + fullRoutingKey);
  			federationRoutes.add(fullRoutingKey);
  			if(!PendingFutures.unsetPendingRegistration(fullRoutingKey))
  				log.error("Cannot unset pending registration " + fullRoutingKey);
  			//sendToHub(true);
   		}

		public void onFailure(Throwable throwable) {
			String cause = throwable.getCause().toString();
			if(
				cause.contains("refused")
				|| cause.contains("timed out")
			) {
				sendToHub(false);
				PendingFutures.setFailedTimestamp(System.currentTimeMillis());
				log.error("Cannot connect to monitoring hub");
			}
			else {
				log.warn(throwable.getCause() + ", repeating request...");
				FederationRouting.getInstance().registerRouteOnFederation(fullRoutingKey);
			}
		}
	}

	private static void setFinagleClient(Service<HttpRequest, HttpResponse> finagleClient) {
		FederationRouting.httpClient = finagleClient;
	}
}
