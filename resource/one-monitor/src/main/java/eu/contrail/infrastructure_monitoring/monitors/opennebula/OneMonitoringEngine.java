/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.monitors.opennebula;


import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import eu.contrail.infrastructure_monitoring.exceptions.MetricNotSupportedException;
import eu.contrail.infrastructure_monitoring.monitors.IMonitoringEngine;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.data.VmData;


public class OneMonitoringEngine implements IMonitoringEngine {
    Status status;
    Date metricsCollectedDate;
    private static Logger log = Logger.getLogger(OneMonitoringEngine.class);
    
    private static RawMetric[] oneVmMetrics = {
    	RawMetric.HOSTNAME,
    	RawMetric.AVAILABILITY_STATUS,
    	RawMetric.CPU_CORES_COUNT,
        RawMetric.MEM_TOTAL,
        RawMetric.VM_STATE,
        RawMetric.CPU_SPEED,
        RawMetric.VM_IMAGE_TEMPLATE,
        RawMetric.VM_PERSISTENCE,
        // Previously fetched from Ganglia
        RawMetric.MEM_USAGE,
    	RawMetric.CPU_LOAD,
    	// New metrics
    	RawMetric.CPU_SECONDS,
    	RawMetric.ID_OWNER,
    	// New in Contrail (for SLAs)
    	RawMetric.LOCATION,
    };
    public static final Set<RawMetric> oneVmMetricsSet = new HashSet<RawMetric>(Arrays.asList(oneVmMetrics));

    private static RawMetric[] oneHostMetrics = {
    	RawMetric.HOSTNAME,
    	RawMetric.AUDITABILITY,
    	RawMetric.LOCATION,
    	RawMetric.SAS70_COMPLIANCE,
    	RawMetric.CCR,
        RawMetric.DATA_CLASSIFICATION,
        RawMetric.HW_REDUNDANCY_LEVEL,
        RawMetric.DISK_THROUGHPUT,
        RawMetric.NET_THROUGHPUT,
        RawMetric.DATA_ENCRYPTION,
        RawMetric.AVAILABILITY_STATUS,
        RawMetric.CPU_CORES_COUNT,
        // Previously fetched from Ganglia
        RawMetric.MEM_FREE,
        RawMetric.MEM_TOTAL,
        RawMetric.CPU_SPEED,
        RawMetric.CPU_SYSTEM,
        RawMetric.CPU_USER,
        RawMetric.CPU_IDLE,
        RawMetric.DISK_FREE,
        RawMetric.DISK_TOTAL,
        RawMetric.IP_ADDRESS,
        RawMetric.LOAD_ONE,
        RawMetric.LOAD_FIVE,
        RawMetric.LOAD_ONE_NORM,
        RawMetric.LOAD_FIVE_NORM,
    };
    public static final Set<RawMetric> oneHostMetricsSet = new HashSet<RawMetric>(Arrays.asList(oneHostMetrics));
    
	public void query() throws Exception {
		log.info("Collecting metrics data...");
        this.status = IMonitoringEngine.Status.BUSY;
        try {
            OpenNebula.getInstance().updateClusters();
        }
        catch (Exception e) {
            this.status = IMonitoringEngine.Status.ERROR;
            throw new Exception("Failed to collect monitoring data: " + e.getMessage(), e);
        }
        metricsCollectedDate = new Date();
        this.status = IMonitoringEngine.Status.OK;
        log.info("Metrics data collected successfully.");
    }

    public Status getStatus() {
        return status;
    }

    public Date getMetricsCollectedDate() {
        return metricsCollectedDate;
    }

    public MetricData getVmMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (oneVmMetricsSet.contains(rawMetric)) {
            return OpenNebula.getInstance().getMetrics().getVmMetricData(fqdn, rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported.");
        }
    }

	public MetricData getHostMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
		if (oneHostMetricsSet.contains(rawMetric)) {
			return OpenNebula.getInstance().getMetrics().getHostMetricData(fqdn, rawMetric);
		}
		else {
			throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported.");
		}
	}

	public boolean checkVmExists(String fqdn) {
		return OpenNebula.getInstance().getMetrics().getVmLayoutData().containsVmData(fqdn);
	}

	public boolean checkHostExists(String fqdn) {
		if(OpenNebula.getInstance().getMetrics().getVmLayoutData().getHostData(fqdn) != null)
			return true;
		
		return false;
	}

	public String getVmHostMachine(String fqdn) {
		VmData vmData = OpenNebula.getInstance().getMetrics().getVmLayoutData().getVmData(fqdn);
        if (vmData != null) {
            return OpenNebula.getInstance().getMetrics().getVmLayoutData().getVmData(fqdn).getHostFqdn();
        }
        else {
            return null;
        }
	}

	public List<String> getGuestMachines(String hostFqdn) {
		return OpenNebula.getInstance().getMetrics().getVmLayoutData().getHostData(hostFqdn).getVmList();
	}

	public List<String> getAllHostMachines() {
		return OpenNebula.getInstance().getMetrics().getVmLayoutData().getHostList();
	}

	public List<String> getAllGuestMachines() {
		return OpenNebula.getInstance().getMetrics().getVmLayoutData().getVmList();
	}

	public String getInfrastructureLayoutXml() {
		return OpenNebula.getInstance().getMetrics().getInfrastructureLayoutXml();
	}
	
	public boolean isVmMonitoringDataAvailable(String[] fqdns) {
		for (String s : fqdns) {
            if (!OpenNebula.getInstance().getMetrics().getVmLayoutData().containsVmData(s)) {
                return false;
            }
        }
        return true;
    }

	public static void setOneVmMetrics(RawMetric[] oneVmMetrics) {
		OneMonitoringEngine.oneVmMetrics = oneVmMetrics;
	}
}
