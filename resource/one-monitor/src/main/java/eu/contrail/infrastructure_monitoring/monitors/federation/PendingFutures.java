package eu.contrail.infrastructure_monitoring.monitors.federation;

import java.util.ArrayList;

import org.apache.log4j.Logger;

public class PendingFutures extends Thread {
	private boolean running = false;
	private static ArrayList<String> federationPendingRegistrations = new ArrayList<String>();
	public static long sendToHubFailedTimestamp = 0;
	public static final int RETRY_SECONDS = 60;
	private static Logger log = Logger.getLogger(PendingFutures.class);
	
	public PendingFutures() {
		super("PendingFutures");
    }
	
	@Override
	public void run() {
		running = true;
		sendToHubFailedTimestamp = System.currentTimeMillis();
        while (running) {
        	long diff = (System.currentTimeMillis() - sendToHubFailedTimestamp) / 1000;
        	if(!federationPendingRegistrations.isEmpty() && diff >= RETRY_SECONDS) {
        		log.trace("Trying to send to hub again...");
        		FederationRouting.sendToHub(true);
        		for(String route : federationPendingRegistrations) {
        			FederationRouting.getInstance().registerRouteOnFederation(route);
        		}
        	}
        	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
        }
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public void stopThread() {
		this.running = false;
	}
	
	protected static boolean setPendingRegistration(String route) {
		for(String s : federationPendingRegistrations) {
			if(s.equalsIgnoreCase(route))
				return false;
		}
		federationPendingRegistrations.add(route);
		log.trace("Added " + route + " to pending futures.");
		return true;
	}
	
	protected static boolean unsetPendingRegistration(String route) {
		int idx = 0;
		for(String s : federationPendingRegistrations) {
			if(s.equalsIgnoreCase(route))
				break;
			idx++;
		}
		// Key was found
		if(idx < federationPendingRegistrations.size()) {
			federationPendingRegistrations.remove(idx);
			log.trace("Successfully removed key " + route + " from pending futures");
			return true;
		}
		return false;
	}
	
	protected static void setFailedTimestamp(long ts) {
		sendToHubFailedTimestamp = ts;
	}
}
