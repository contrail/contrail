/* File: util.h 
 *
 * Description of exported objects of util.c.
 */

#ifndef _INC_UTIL_H
#define _INC_UTIL_H

#include "origin.h"

extern FILE *ckfopen( const char *nm, const char *acc );
extern void ckfreopen( const char *nm, const char *acc, FILE *f );
extern void assert_file_executable( const origin *org, const char *fnm );
extern void assert_file_not_go_writable( const char *fnm );
extern void set_caller_privs();
extern void set_privs_of_file(const char *fnm);

extern char *read_file(const char *fnm);
extern char *trim_string(const char *s);

#endif
