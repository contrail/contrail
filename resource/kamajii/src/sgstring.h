/* File: sgstring.h
 *
 * Description of exported objects of sgstring.c.
 */

#ifndef _INC_SGSTRING_H
#define _INC_SGSTRING_H

#define stringNIL ((char *) 0)

#include "word.h"
#include "origin.h"

extern const char *scanword( const origin *org, const char *s, char **w );
extern void scan1par( const origin *org, const char *pl, char **p1 );
extern bool cknumpar( const origin *org, const char *n );
extern char *newboolstr( bool b );
extern char *newintstr( int n );
extern char *newuintstr( unsigned int n );
extern word *chopstring( const origin *org, const char *p );
extern char *sepstrings( const word *sl, const char *sep );
extern char *flatstrings( const word *sl );
extern bool isfalsestr( const char *s );
extern bool istruestr( const char *s );
extern void fre_string(char *s);
extern char *create_string( size_t sz );
extern char *new_string(const char *s);
extern char *extract_string(const char *start, const char *end);
extern char *realloc_string(char *s, unsigned int sz);
extern char *flat_string_array( char **params, int startix);

#endif
