//============================================================================
// Name        : main.c
// Author      : Kees van Reeuwijk
// Version     :
// Description : command-line interface of the safeconfig program
//============================================================================

#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "flag.h"
#include "sgstring.h"
#include "error.h"
#include "util.h"
#include "version.h"

static const char *prognm;
static char *outfilename;     /* Possible output redirection file. */
static char *errfilename;     /* Possible error redirection file. */
static char *scriptfilename = stringNIL;    // The name of the shell script to write to
static char *configfilename = stringNIL;    // The name of the script to run.
static word *scriptargv = wordNIL;

/* Table of debugging flags plus associated information.

   Table is ended by an entry with flagchar '\0'
 */
static const dbflag flagtab[] = {
        { 'e', &sevaltr, "tracing of string evaluations" },
        { 'n', &noerrorline, "don't generate line numbers in error messages (useful for diffs)" },
        { 'r', &hide_regex_error, "do not show regular-expression error (useful in testsuite)" },
        { 'v', &vartr, "variable tracing" },
        { '\0', &fntracing, "" }
};


static const char helptext[] =
        "Usage: safeconfig [<flags>] <configfile> [<parameters>]\n"
        " -D              Enable debugprint and debugrun statements"
        " -d<debugflags>  Set given debug flags\n"
        " -e<file>        Redirect errors to file <file>\n"
        " -h              Show this help text\n"
        " -n              Do not run commands, only show what would be run (implies -x)\n"
        " -o<file>        Redirect output to file <file>\n"
        " -r              Show the commands that are run\n"
        " -s<var>         Set variable <var> to the empty string\n"
        " -s<var>=<val>   Set variable <var> to string <val>\n"
        " -z<script>      Write a shell script to implement this config\n"
        "\n"
        ;

/* Scan command line arguments and options as passed by 'argc' and 'argv'. */
static void scanargs( int argc, char **argv )
{
    bool printusage = false;
    int exitcode = EXIT_SUCCESS;
    bool parseOptions = true;
    word **backptr = &scriptargv;

    outfilename = stringNIL;
    errfilename = stringNIL;
    prognm = argv[0];
    if( prognm[0] == '\0' ){
        prognm = "safeconfig";
    }
    argv++;
    argc--;
    while( argc>0 ){
        int op = argv[0][1];
        if(!parseOptions || argv[0][0] != '-'){
            parseOptions = false;
            if( configfilename == stringNIL ){
                configfilename = new_string( argv[0] );
            }
            else {
                // Add a parameter to the list
                word *w = new_word( argv[0] );
                *backptr = w;
                backptr = &w->next;
            }
            argc--;
            argv++;
            continue;
        }
        switch( op ){
        case 'd':
            setdbflags( &argv[0][2], flagtab, true );
            break;

        case 'D':
            debug = true;
            break;

        case 'e':
            if( argv[0][2] != '\0' ){
                if( errfilename != stringNIL ){
                    fre_string( errfilename );
                }
                errfilename = new_string( argv[0]+2 );
            }
            else {
                if( argc<1 ){
                    error( "Missing argument for -e" );
                }
                else {
                    if( errfilename != stringNIL ){
                        fre_string( errfilename );
                    }
                    errfilename = new_string( argv[1] );
                    argc--;
                    argv++;
                }
            }
            break;

        case '?':
        case 'h':
            printusage = true;
            exitcode = EXIT_SUCCESS;
            break;

        case 'n':
            dryrun = true;
            showrun = true;
            break;

        case 'r':
            showrun = true;
            break;

        case 'o':
        case 'O':
            if( argv[0][2] != '\0' ){
                if( outfilename != stringNIL ){
                    fre_string( outfilename );
                }
                outfilename = new_string( argv[0]+2 );
            }
            else {
                if( argc<1 ){
                    error( "Missing argument for -o" );
                }
                else {
                    if( outfilename != stringNIL ){
                        fre_string( outfilename );
                    }
                    outfilename = new_string( argv[1] );
                    argc--;
                    argv++;
                }
            }
            break;

        case 'z':
        case 'Z':
        {
            const char *s;
            const char *pos;

            if( argv[0][2] != '\0' ){
                s = argv[0]+2;
            }
            else {
                if( argc<1 ){
                    error( "Missing argument for -s" );
                    s = argv[0]+2;
                }
                else {
                    s = argv[1];
                    argc--;
                    argv++;
                }
            }
            scriptfilename = new_string(s);
            break;
        }

        default:
            fprintf(stderr,"Unknown flag '%s'\n", argv[0]);
            printusage = true;
            exitcode = EXIT_FAILURE;
            break;
        }
        argc--;
        argv++;
    }
    if(configfilename == stringNIL){
        fputs("No configuration file specified\n", stderr);
        printusage = true;
        exitcode = EXIT_FAILURE;
    }
    if( printusage ){
        fprintf( stdout, "safeconfig version %s\n", VERSION );
        (void) fputs( helptext, stdout);
        helpdbflags( stdout, flagtab );
        exit( exitcode );
    }
}


int main(int argc, char *argv[]) {
    tracestream = stderr;
    scanargs( argc, argv );
    return 0;
}
