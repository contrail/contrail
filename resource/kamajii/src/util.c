/* File: util.c
 *
 * Various low-level routines.
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>

#include "tmdefs.h"

#include "util.h"
#include "error.h"
#include "sgstring.h"

/* Same as fopen, but give error message if file can't be opened */
FILE *ckfopen( const char *nm, const char *acc )
{
    FILE *hnd = fopen( nm, acc );

    if( NULL == hnd ){
	sys_error( errno, "'%s'", nm );
	exit( EXIT_FAILURE );
    }
    return hnd;
}

/* Similar to freopen, but give error message if file can't be opened.
 * Therefore, file handle need not be returned.
 */
void ckfreopen( const char *nm, const char *acc, FILE *f )
{
    if( freopen( nm, acc, f ) == NULL ){
	sys_error( errno, "'%s'", nm );
	exit( EXIT_FAILURE );
    }
}



char *read_file(const char *fnm)
{
    long sz;
    char *buffer;

    FILE *fp = ckfopen( fnm, "rb" );

    fseek( fp, 0L , SEEK_END);
    sz = ftell( fp );
    rewind( fp );

    /* allocate memory for entire content */
    buffer = (char *) malloc( sz+1 );
    if( buffer == stringNIL ){
         fclose(fp);
         fatal_error("Cannot allocate memory for %ld bytes", sz+1);
    }


    /* copy the file into the buffer */
    if( 1!=fread( buffer , sz, 1 , fp) ){
        fclose(fp);
        free(buffer);
        fatal_error("Cannot read file '%s'", fnm);
    }
    buffer[sz] = '\0';

    fclose(fp);
    return buffer;
}

char *trim_string(const char *s)
{
    while(isspace(*s)){
        s++;
    }
    char *res = create_string( strlen(s) );
    char *dst = res;
    bool sawSpace = false;
    while(*s != '\0'){
        int c = *s++;
        if(isspace(c)){
            if(!sawSpace){
                *dst++ = ' ';
                sawSpace = true;
            }
        }
        else {
            *dst++ = c;
            sawSpace = false;
        }
        if(*s == '\0'){
            if(sawSpace){
                // Don't let it end with a space.
                dst--;
                *dst = '\0';
            }
        }
    }
    return res;
}
