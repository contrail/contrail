#!/usr/bin/ruby
# Copyright 2011 Zuse Institute Berlin
#
# // TODO(mberlin): Include license.
#
# Authors: Thorsten Schütt, Michael Berlin

require 'rubygems'
require 'json'
require 'net/http'
require 'optparse'
require 'pp'

$url = 'http://localhost:8080/xtreemfs-jsonrpc/executeMethod'
$uri = URI.parse $url

def json_call(function, params)
  req = Net::HTTP::Post.new($uri.path)
  req.add_field 'Content-Type', 'application/json'
  req.body =
    {
    :version => '1.1',
    :method => function,
    :params => params,
    :id => 0}.to_json
  res = Net::HTTP.start($uri.host, $uri.port){|http|http.request(req)}
  jsonres = JSON.parse(res.body)
  if jsonres['error'] == nil
    JSON.parse(res.body)['result']
  else
    JSON.parse(res.body)['error']
  end
end

options = {}

optparse = OptionParser.new do |opts|
  opts.banner = 'Usage: xtreemfs-json-test.rb (-c|-d|-l) [options]'
  opts.separator ''
  
  options[:list_volumes] = nil
  opts.on('-l', '--list-volumes', 'List all available volumes.' ) do |key|
    options[:list_volumes] = true
  end

  options[:create_volume] = nil
  opts.on('-c', '--create-volume volume', String, 'Create a new volume "volume".' ) do |volume|
    options[:create_volume] = volume
  end

  options[:delete_volume] = nil
  opts.on('-d', '--delete-volume volume', String, 'Delete the volume "volume".' ) do |volume|
    options[:delete_volume] = volume
  end
  
  opts.separator ''
  opts.separator 'Create and Delete Options:'
  options[:password] = nil
  opts.on('-p', '--password pw', String, 'If required, set the configured "admin_password".' ) do |password|
    options[:password] = password
  end

  opts.separator ''
  opts.separator 'Help:'
  opts.on_tail('-h', '--help', 'Show this message.') do
    puts opts
    exit
  end
end

begin
  unless ARGV.length > 0
    puts optparse
    exit
  end
  
  optparse.parse!
rescue OptionParser::ParseError
  $stderr.print "Error: " + $! + "\n"
  exit
end

puts json_call('listVolumes', []).to_json unless options[:list_volumes] == nil
puts json_call('createVolume', [options[:create_volume], options[:password]]).to_json unless options[:create_volume] == nil
puts json_call('deleteVolume', [options[:delete_volume], options[:password]]).to_json unless options[:delete_volume] == nil
