package org.xtreemfs.contrib.provisioning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.xtreemfs.contrib.provisioning.JsonRPC.METHOD;

import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;

public class JSONRPCClient {

  static int requestId = 0;

  public static JSONRPC2Response callJSONRPC(
      JsonRPC xtreemfsRPC, METHOD method, Object... parameters) throws JSONRPC2ParseException {
    JSONRPC2Request req = new JSONRPC2Request(
        method.toString(),
        parameters != null? Arrays.asList(parameters):new ArrayList<String>(),
            "id-"+(++requestId));
    System.out.println("\tRequest: \n\t" + req);
    return JSONRPC2Response.parse(xtreemfsRPC.executeMethod(req.toString()), true, true);

  }

  @SuppressWarnings("unchecked")
  public static void main(String argv[]) {
    try {
      // initialize JSONPRC for local installation
      JsonRPC xtreemfsRPC = new JsonRPC("default_dir");
      xtreemfsRPC.init();

      // get list of servers
      JSONRPC2Response res = callJSONRPC(xtreemfsRPC, METHOD.getServers, "");
      AbstractTestCase.checkSuccess(res, false);

      // start vin on all servers
      List<String> servers = (List<String>) res.getResult();

      // TODO add ibisPoolName, ibisServerAddress
      String ibisPoolName = "";
      String ibisServerAddress = "";

      // TODO add some server => vin_agent_identifier mapping
      List<String> vin_agent_identifier = new ArrayList<String>();
      for (String server : servers) {
        vin_agent_identifier.add(server);
      }

      res = callJSONRPC(xtreemfsRPC, METHOD.startVINAgents, servers, vin_agent_identifier, ibisPoolName, ibisServerAddress);
      AbstractTestCase.checkSuccess(res, false);

    } catch (JSONRPC2ParseException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}
