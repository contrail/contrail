/*
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

#include "styx.h"
#include "styx_backend.h"
#include "styx_parser.h"


typedef struct private_styx_plugin_t private_styx_plugin_t;

FILE *f;

/**
 * Private data of an styx_t object.
 */
struct private_styx_plugin_t {

    /**
     * Public part of styx_t object.
     */
    styx_plugin_t public;

    /**
     * the list to hold the dynamic configs
     */
    linked_list_t *list;

    /**
     * mutex to protect the access to the dynamic configs
     */
    mutex_t *mutex;

    /**
     * styx configuration backend
     */
    styx_config_t *config;

    /**
     * unix socket fd
     */
    int socket;

    /**
     * job accepting stroke messages
     */
    callback_job_t *job;

    /**
     * error list
     */
    linked_list_t *error_list;

    /**
     * socket name
     */
    char *sock_name;
};

ENUM(ike_sa_state_lower_names, IKE_CREATED, IKE_DELETING,
    "created",
    "connecting",
    "established",
    "rekeying",
    "deleting",
);


/**
 * cleanup helper function for open file descriptors
 */
static void closefdp(int *fd)
{
    close(*fd);
}


static bool json_callback(void* fdp, debug_t group, level_t level,
                         ike_sa_t* ike_sa, char* format, va_list args)
{
    return true;
}

static void styx_terminate(json_rpc_t *call, json_t *params, private_styx_plugin_t *this)
{
    char *str;
    u_int32_t id;
    int writer = call->fd;
    status_t status;
    json_t *varj, *err;
    bool ike;
    if(check_terminate(call,params))
    {
        return;
    }
    varj = json_array_get(params,0);

    id = (u_int32_t)json_integer_value(varj);

    DBG1(DBG_CFG, "styx: terminating IKE_SA %d", id);

    status = charon->controller->terminate_ike(
             charon->controller, id,
             (controller_cb_t)json_callback, &writer, 0);

    if (status == SUCCESS)
    {
       json_rpc_reply(call, JRPC_REPLY, json_integer(0));
       return;
    }
    if (status == NOT_FOUND)
    {
        str = strdup("Error: Terminate has failed. CHILD_SA "
                    " not found");
    }
    else
    {
        str = strdup("Error: Terminate has failed, either because of a "
                     "timeout or because of failed callback call");
    }
    err = json_rpc_error_payload(JRPC_ERROR, str);
    json_rpc_reply(call, JRPC_ERROR, err);
    free(str);
}

static u_int32_t styx_connect(json_rpc_t *call, json_t *params, private_styx_plugin_t *this)
{
    char *str,*vars;
    bool ike;
    int writer = call->fd;
    status_t status = FAILED;
    peer_cfg_t *peer;
    child_cfg_t *child = NULL;
    ike_sa_t *ike_sa;
    enumerator_t *enumerator;
    json_t  *varj, *err;

    if(check_connect(call, params))
    {
        return -1;
    }

    varj = json_array_get(params, 1);
    vars = json_dumps(varj,JSON_ENCODE_ANY);
    str = malloc(strlen(vars)) - 2;
    strncpy(str,vars + 1, strlen(vars) - 2);
    str[strlen(vars) - 2] = 0;
    free(vars);

    varj = json_array_get(params, 0);

    if (json_true() == varj)
        ike = 1;
    else
        ike = 0;

    DBG1(DBG_CFG, "styx: initiating %s_SA %s", ike ? "IKE" : "CHILD", str);
    this->mutex->lock(this->mutex);
    peer = charon->backends->get_peer_cfg_by_name(charon->backends, (char*)str);
    if (peer)
        {
        enumerator = peer->create_child_cfg_enumerator(peer);
        if (ike)
        {
            if (!enumerator->enumerate(enumerator, &child))
            {
                str = strdup("Error: No child_cfg found for specified config."
                             " Possibly addConfig has failed");
                child = NULL;
            }
            child->get_ref(child);
        }
        else
        {
            while (enumerator->enumerate(enumerator, &child))
            {
                if (streq(child->get_name(child), str))
                {
                    child->get_ref(child);
                    break;
                }
                child = NULL;
            }
            if (child == NULL)
            {
                str = strdup("Error: Cannot find an IKE_SA with"
                             " specified config name");
            }
        }
        enumerator->destroy(enumerator);
        if (child)
        {
            status = charon->controller->initiate(charon->controller,
                          peer, child, (controller_cb_t)json_callback,
                          &writer, 0);
        }
        else
        {
            this->mutex->unlock(this->mutex);
            err = json_rpc_error_payload(JRPC_ERROR, str);
            json_rpc_reply(call, JRPC_ERROR, err);
            free(str);
            peer->destroy(peer);
            return -1;
        }
    }
    this->mutex->unlock(this->mutex);

    enumerator = charon->controller->create_ike_sa_enumerator(
                                                charon->controller, TRUE);
    while (enumerator->enumerate(enumerator, &ike_sa))
    {
        if (streq(str, ike_sa->get_name(ike_sa)))
        {
            enumerator->destroy(enumerator);
            json_rpc_reply(call, JRPC_REPLY, json_integer(ike_sa->get_unique_id(ike_sa)));
            return 0;
        }
    }
    str = malloc(64 * sizeof(char));
    snprintf(str, 63, "Connect failed with status %d" ,status);
    err = json_rpc_error_payload(JRPC_ERROR, str);
    json_rpc_reply(call, JRPC_ERROR, err);
    free(str);
    return -1;
}

static void styx_addConfig(json_rpc_t *context, json_t *params, private_styx_plugin_t *this)
{
    styx_config_t *conf = this->config;
    styx_conn_t *conn;
    json_t *tmp, *name, *err;
    u_int32_t klength, i;
    char *payload, *ptr;
    const char *token;
    int rc, errors;
    rc = 0;

    if (context->type == JRPC_NOTIFY)
    {
        DBG2(DBG_CFG, "styx: addConfig is not available as a notify call");
        err = json_rpc_error_payload(RPC_INVALID_METHOD, "addConfig is not"
                                    " available as a notify call");
        json_rpc_reply(context, JRPC_ERROR, err);
        return;
    }
    if (check_addConfig(context, params))
    {
        DBG2(DBG_CFG, "styx addConfig: RPC parameter check failed");
        return;
    }
    conn = default_conn();

    klength = json_array_size(params);
    name = json_array_get(params, STYX_CONN_NAME);
    conn->name = strdup(json_string_value(name));
    tmp = json_array_get(params, STYX_CONN_LOCAL);
    conn->me.address = strdup(json_string_value(tmp));
    tmp = json_array_get(params, STYX_CONN_REMOTE);
    conn->other.address = strdup(json_string_value(tmp));
    /* pre-parsing the options */
    for (rc = 0, i = STYX_OPTIONS; i < klength; ++i)
    {
        tmp = json_array_get(params, i);
        token = json_string_value(tmp);
        rc += pre_parse(conf->error_list, conn, token);
    }
    /* actual parsing */
    if (rc == 0)
    for (i = STYX_OPTIONS; i < klength; ++i)
    {
        tmp = json_array_get(params, i);
        token = json_string_value(tmp);
        rc += parse_option(conf->error_list, conn, token);
    }

    if (rc == 0)
    {
        this->mutex->lock(this->mutex);
        conf->add(conf,conn);
        this->mutex->unlock(this->mutex);
    }
    errors = conf->error_list->get_count(conf->error_list);
    if (errors == 0)
        json_rpc_reply(context, JRPC_REPLY, json_integer(0));
    else
    {
        payload = calloc(256 * errors , sizeof(char));
        for (i = 0; i < errors; ++i)
        {
            this->mutex->lock(this->mutex);
            conf->error_list->remove_first(conf->error_list, (void**)&ptr);
            this->mutex->unlock(this->mutex);
            strcat(payload, "\n");
            strncat(payload, ptr, 253);
            free(ptr);
        }
        err = json_rpc_error_payload(JRPC_ERROR, payload);
        json_rpc_reply(context, JRPC_ERROR, err);
        free(payload);
    }
    free_conn(conn);
    return;
}

static void styx_version(json_rpc_t *call, json_t *params)
{
    json_rpc_reply(call, JRPC_REPLY, json_string(STYX_VERSION));
    return;
}

static void styx_removeConfig(json_rpc_t *context, json_t *params, private_styx_plugin_t *this)
{
    styx_config_t *conf = this->config;
    const char *conn_name;
    char *payload, *ptr;
    int errors, i;
    json_t *arg, *err;

    if (check_remove(context, params))
    {
        DBG2(DBG_CFG, "styx: RPC parameter check failed");
        return;
    }

    arg = json_array_get(params, 0);
    conn_name = json_string_value(arg);

    this->mutex->lock(this->mutex);
    conf->removeConfig(conf, conn_name);
    this->mutex->unlock(this->mutex);

    errors = conf->error_list->get_count(conf->error_list);
    if (errors == 0)
        json_rpc_reply(context, JRPC_REPLY, json_integer(0));
    else
    {
        payload = calloc(256 * errors , sizeof(char));
        for (i = 0; i < errors; ++i)
        {
            this->mutex->lock(this->mutex);
            conf->error_list->remove_first(conf->error_list, (void**)&ptr);
            this->mutex->unlock(this->mutex);
            strcat(payload, "\n");
            strncat(payload, ptr, 253);
            free(ptr);
        }
        err = json_rpc_error_payload(JRPC_ERROR, payload);
        json_rpc_reply(context, JRPC_ERROR, err);
        free(payload);
    }
    return;
}

/**
 * read from an opened connection and process it
 */
static void process(int *fdp, private_styx_plugin_t *this)
{
    int         rc, fd = *fdp;
    bool        oldstate;
    char        buffer[4096];
    ssize_t     len;
    json_rpc_t  *rpc_call;

    thread_cleanup_push((thread_cleanup_t)closefdp, (void*)fdp);
    oldstate = thread_cancelability(TRUE);
    memset(buffer, 0, 4096);
    len = recv(fd, buffer, sizeof(buffer), MSG_WAITALL);
    thread_cancelability(oldstate);
    thread_cleanup_pop(FALSE);

    rpc_call = json_rpc_object_init(fd, stderr);
    rc = json_rpc_object_decode(rpc_call,buffer);
    if (rc == -1)
    {
        DBG2(DBG_CFG, "styx: Received an invalid jsonrpc query '%s'. Discarding.", buffer);
        json_rpc_object_clear(rpc_call);
        closefdp(&fd);
        return;
    }

    rc = json_rpc_dispatch(rpc_call, this);
    if (rc == -1)
        DBG2(DBG_CFG, "styx: RPC invocation failed. Cleaning up.");

    json_rpc_object_clear(rpc_call);
    closefdp(fdp);
}

/**
 * Accept from socket and create jobs to process connections.
 *
 * This function does not accept several connections at the same
 * time, as it processes requests one by one. This is done in order
 * to avoid unwanted concurrency issues
 */
static job_requeue_t dispatch(private_styx_plugin_t *this)
{
    struct sockaddr_un strokeaddr;
    int fd, *fdp, strokeaddrlen = sizeof(strokeaddr);
    bool oldstate;

    /* wait for connections, but allow thread to terminate */
    oldstate = thread_cancelability(TRUE);
    fd = accept(this->socket, (struct sockaddr *)&strokeaddr, &strokeaddrlen);
    thread_cancelability(oldstate);

    if (fd < 0)
    {
        DBG1(DBG_CFG, "accepting STYX socket failed: %s", strerror(errno));
        sleep(1);
        return JOB_REQUEUE_FAIR;;
    }

    fdp = malloc_thing(int);
    *fdp = fd;

    process(fdp, this);
    free(fdp);
    return JOB_REQUEUE_DIRECT;
}

METHOD(plugin_t, get_name, char*,
    private_styx_plugin_t *this)
{
    return "styx";
}


METHOD(plugin_t, destroy, void,
    private_styx_plugin_t *this)
{
    close(this->socket);
    this->list->destroy_offset(this->list, offsetof(peer_cfg_t, destroy));
    this->error_list->destroy_function(this->error_list, free);
    this->mutex->destroy(this->mutex);
    json_rpc_unregister_all();
    this->job->cancel(this->job);
    charon->backends->remove_backend(charon->backends, &this->config->backend);
    this->config->destroy(this->config);
    DBG3(DBG_CFG, "styx daemon destroyed");
    free(this);
    fprintf(f, "-----styx destroyed-----\n");
    fclose(f);
    unlink(this->sock_name);
}

static struct sockaddr_un *get_socket_unix_addr()
{
    struct sockaddr_un *toRet;
    toRet = malloc(sizeof(struct sockaddr_un));
    char *address = lib->settings->get_str(lib->settings, "%s.plugins.styx.socket",
                                        NULL, charon->name);
    toRet->sun_family = AF_UNIX;
    if (!address)
    {
        DBG2(DBG_CFG, "styx: no path provided for styx socket, "
                        "defaulting to '%s'",IPSEC_PIDDIR "/styx.sock");
        strncpy(toRet->sun_path, IPSEC_PIDDIR "/styx.sock",
                sizeof(toRet->sun_path) - 1);
    }
    else
    {
        strncpy(toRet->sun_path, address, sizeof(toRet->sun_path) - 1);
    }
    return toRet;
}

static mode_t get_socket_mask()
{
    mode_t toRet;
    char *mask = lib->settings->get_str(lib->settings, "%s.plugins.styx.mask",
                                        NULL, charon->name);
    toRet = ~(S_IRWXU | S_IRWXG | S_IRWXO);
    if (!mask || strlen(mask) != 4 || *mask != '0')
    {
        DBG2(DBG_CFG, "styx: no mask found, defaulting to 0777");
    }
    else
    {
        char *endptr;
        unsigned value = strtoul(mask+1, &endptr, 8);

        if(*endptr != '\0')
        {
            DBG1(DBG_CFG, "styx: invalid mask found, defaulting to 0777");
            return toRet;
        }
        toRet = ~value;
    }
    return toRet;
}

/*
 * Described in header file
 */
plugin_t *styx_plugin_create()
{
    struct sockaddr_un *unix_addr = get_socket_unix_addr();
    private_styx_plugin_t *this;
    mode_t old;
    f = fopen(IPSEC_PIDDIR "/styx.log","w");
    fprintf(f, "-----styx initiated-----\n");
    INIT(this,
        .public = {
            .plugin = {
                .get_name = _get_name,
                .reload = (void*)return_false,
                .destroy = _destroy,
            },
        },
        .list = linked_list_create(),
        .mutex = mutex_create(MUTEX_TYPE_RECURSIVE),
        .error_list = linked_list_create(),
        .sock_name = unix_addr->sun_path,
    );
    DBG3(DBG_CFG, "styx: starting daemon");

    /* set up unix socket */
    unlink(unix_addr->sun_path);
    this->socket = socket(AF_UNIX, SOCK_STREAM, 0);
    if (this->socket == -1)
    {
        DBG1(DBG_CFG, "could not create socket");
        free(this);
        return NULL;
    }
    DBG3(DBG_CFG, "styx: socket '%s' created", unix_addr->sun_path);

    old = umask(get_socket_mask());
    if (bind(this->socket, (struct sockaddr *)unix_addr, sizeof(struct sockaddr_un)) < 0)
    {
        DBG1(DBG_CFG, "styx: could not bind socket %s: %s", unix_addr->sun_path, strerror(errno));
        close(this->socket);
        free(this);
        return NULL;
    }
    DBG3(DBG_CFG, "styx: socket '%s' bound", unix_addr->sun_path);
    umask(old);
    if (chown(unix_addr->sun_path, charon->uid, charon->gid) != 0)
    {
        DBG1(DBG_CFG, "styx: changing socket permissions failed: %s", strerror(errno));
    }

    if (listen(this->socket, 5) < 0)
    {
        DBG1(DBG_CFG, "could not listen on socket: %s", strerror(errno));
        close(this->socket);
        free(this);
        return NULL;
    }
    DBG3(DBG_CFG, "styx: now listening on socket '%s'", unix_addr->sun_path);
    
    json_rpc_register_method("connect", styx_connect);
    json_rpc_register_method("terminate", styx_terminate);
    json_rpc_register_method("addConfig", styx_addConfig);
    json_rpc_register_method("version", styx_version);
    json_rpc_register_method("removeConfig", styx_removeConfig);
    this->config = styx_config_create(this->list, this->error_list,  this->mutex);

    charon->backends->add_backend(charon->backends, &this->config->backend);
    this->job = callback_job_create_with_prio((callback_job_cb_t)dispatch,
                                        this, NULL, NULL, JOB_PRIO_CRITICAL);
    lib->processor->queue_job(lib->processor, (job_t*)this->job);

    DBG3(DBG_CFG, "styx: plugin is ready");
    return &this->public.plugin;
}

