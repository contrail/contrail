/*
 * Copyright (C) 2008 Tobias Brunner
 * Copyright (C) 2008 Martin Willi
 * Hochschule fuer Technik Rapperswil
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

/**
 * @defgroup styx_ca styx_ca
 * @{ @ingroup styx
 */

#ifndef STROKE_CA_H_
#define STROKE_CA_H_

#include <stroke_msg.h>

#include "styx_cred.h"

typedef struct styx_ca_t styx_ca_t;

/**
 * ipsec.conf ca section handling.
 */
struct styx_ca_t {

	/**
	 * Implements credential_set_t
	 */
	credential_set_t set;

	/**
	 * Add a CA to the set using a stroke_msg_t.
	 *
	 * @param msg		styx message containing CA info
	 */
	void (*add)(styx_ca_t *this, stroke_msg_t *msg);

	/**
	 * Remove a CA from the set using a stroke_msg_t.
	 *
	 * @param msg		styx message containing CA info
	 */
	void (*del)(styx_ca_t *this, stroke_msg_t *msg);

	/**
	 * List CA sections to styx console.
	 *
	 * @param msg		styx message
	 */
	void (*list)(styx_ca_t *this, stroke_msg_t *msg, FILE *out);

	/**
	 * Check if a certificate can be made available through hash and URL.
	 *
	 * @param cert		peer certificate
	 */
	void (*check_for_hash_and_url)(styx_ca_t *this, certificate_t* cert);

	/**
	 * Destroy a styx_ca instance.
	 */
	void (*destroy)(styx_ca_t *this);
};

/**
 * Create a styx_ca instance.
 */
styx_ca_t *styx_ca_create(styx_cred_t *cred);

#endif /** STROKE_CA_H_ @}*/
