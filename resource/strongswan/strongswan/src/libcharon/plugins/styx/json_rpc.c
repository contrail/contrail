/*
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

#include "styx.h"
#include "json_rpc.h"


struct rpc_method_array
{
	u_int32_t count;
	u_int32_t size;
	char **name;
	void **function;
};


static struct rpc_method_array rpc_methods = {0, 0, NULL, NULL};
static FILE *stream;

/* method used to add a new method to the API */
int	json_rpc_register_method(const char *method, void *ptr)
{
	if (rpc_methods.count == rpc_methods.size)
	{
		rpc_methods.size += UNIT;
		rpc_methods.name = realloc(rpc_methods.name,
									rpc_methods.size * sizeof(char*));
		rpc_methods.function = realloc(rpc_methods.function,
									rpc_methods.size * sizeof(void*));
	}
	rpc_methods.name[rpc_methods.count] = strdup(method);
	rpc_methods.function[rpc_methods.count] = ptr;
	return ++(rpc_methods.count);
}

int json_rpc_unregister_method(const char *method)
{
	int i;
	for (i = 0; i < rpc_methods.count; ++i)
		if(strcmp(rpc_methods.name[i],method) == 0)
		{
			free(rpc_methods.name[i]);
			free(rpc_methods.function[i]);
			rpc_methods.name[i] = NULL;
			rpc_methods.function[i] = NULL;
			rpc_methods.count--;
			break;
		}
	for (; i < rpc_methods.count; ++i)
		rpc_methods.name[i] = rpc_methods.name[i+1];
	return rpc_methods.count;
}

/* method used to clean all the registered API methods */
void json_rpc_unregister_all()
{
    int i;
    for (i = 0; i < rpc_methods.count; ++i)
    {
    if (rpc_methods.name[i] != NULL)
        free(rpc_methods.name[i]);
        rpc_methods.name[i] = NULL;
        rpc_methods.function[i] = NULL;
    }
    free(rpc_methods.name);
    free(rpc_methods.function);
    rpc_methods.name = NULL;
    rpc_methods.function = NULL;
    rpc_methods.count = 0;
}

/* function used to check if a method has been registered with the API */
void *json_rpc_check_method(const char *method)
{
    int i;
    for (i = 0; i < rpc_methods.count; ++i)
        if (strcmp(method, rpc_methods.name[i]) == 0)
            return rpc_methods.function[i];
    return NULL;
}

/* function used to do an actual dispatch to one of the registered API methods */
int json_rpc_dispatch(json_rpc_t *object, void *data)
{
    rpc_dispatch func;

    *(void**)(&func) = json_rpc_check_method(object->method);
    if (func == NULL) {
        json_t *err;
        char buffer[256];

        DBG1(DBG_CFG, "Unknown styx jsonRpc method name: '%s'",object->method);
        snprintf(buffer, 255, "invalid method name '%s'", object->method);
        err = json_rpc_error_payload(RPC_INVALID_METHOD, buffer);
        json_rpc_reply(object, JRPC_ERROR, err);
                return -1;
    }
    DBG1(DBG_CFG, "executing styx jsonRpc method '%s'", object->method);
    func(object, object->params, data);
    return 0;
}

/**
 * Function used to create an empty json_rpc_t object, that
 * can be further used to parse an incoming procedure call
 *
 * @return: json_rpc_t *
 * 		The object that was created.
 */
json_rpc_t *json_rpc_object_init(int fd, FILE *log)
{
    json_rpc_t *toRet;
    toRet = malloc(sizeof(json_rpc_t));
    toRet->jsonrpc = NULL;
    toRet->method = NULL;
    toRet->params = NULL;
    toRet->id = NULL;
    toRet->result = NULL;
    toRet->fd = fd;
    stream = log;
    return toRet;
}

json_t *json_rpc_error_payload(int code, const char *message)
{
    json_t *payload;
    int rc;
    payload = json_object();
    if ((rc = json_object_set_new(payload, CODE_STR, json_integer(code))) == -1)
    {
        d2printf(stream, "styx: setting the error payload code failed\n", rc);
        return NULL;
    }
    if ((rc = json_object_set_new(payload, MESSAGE_STR, json_string(message))) == -1)
    {
        d2printf(stream, "styx: setting the error message failed\n", rc);
        return NULL;
    }
    return payload;
}

/**
 * Reply to the json_rpc callee
 *
 * This function is used to provide the feedback resulted
 * from the issued call to the json_rpc client
 *
 */
void json_rpc_reply(json_rpc_t *context, int type, json_t *payload)
{
    json_t *toSend;
    char *buffer;
    int rc, len, written;
    toSend = json_object();
    rc = json_object_set_new(toSend, JSONRPC_STR, json_real(2.0));
    if (rc == -1)
    {
        d2printf(stream, "styx: setting the jsonrpc version failed with %d\n",rc);
        goto err_tag;
    }
    if (type == JRPC_REPLY)
    {
        if((rc = json_object_set_new(toSend, RESULT_STR, payload)) == -1)
        {
            d2printf(stream, "styx: setting the jsonrpc reply payload failed with %d\n",rc);
            goto err_tag;
        }
    }
    else if (type == JRPC_ERROR)
    {
        if((rc = json_object_set_new(toSend, ERROR_STR, payload)) == -1)
        {
            d2printf(stream, "styx: setting the jsonrpc error payload failed with %d\n",rc);
            goto err_tag;
        }
    }
    else 
    {
        d2printf(stream, "styx: invalid reply type: %d\n", type);
        goto err_tag;
    }
    if (context->id != NULL)
        json_object_set_new(toSend, ID_STR, json_string(context->id));
    buffer = json_dumps(toSend, JSON_ENCODE_ANY);
    
    len = strlen(buffer);
    written = 0;
    while(len != written)
    {
        rc = write(context->fd, buffer + written, len-written);
        if (rc == -1)
        {
            d2printf(stream, "styx: failed writing back: %s\n", buffer);
            free(buffer);
            goto err_tag;
        }
        written += rc;
    }
    free(buffer);

err_tag:
    json_object_clear(payload);
    json_object_clear(toSend);
    json_decref(toSend);
}



/**
 * This function is used to decode a json message inputed as a const char* into a json_rpc_t.
 *
 * @param:	const_char *output
 * @param:	const_char *input
 * @return:
 *	0 on succes
 *	-1 on error
 */
int json_rpc_object_decode(json_rpc_t *output, const char *input)
{
    json_error_t j_err;
    void		*iter;
    json_t      *value, *obj;
    u_int32_t   flags, klength;
    const char  *key;
    char		*foo;

    obj = json_loads(input, 0, &j_err);
    if (obj == NULL)
    {
        output->type = JRPC_INVALID;
        d2printf(stream, "styx: recevied message '%s'\n source: %s\n",j_err.text, j_err.source);
        return -1;
    }
    iter = json_object_iter(obj);

    flags = 0;
    while(iter)
    {
        key = json_object_iter_key(iter);
        value = json_object_iter_value(iter);
        klength = strlen(key);

        if (!strncmp(key, JSONRPC_STR, klength))
		{
            flags |= JSONRPC_V;
			foo = json_dumps(value, JSON_ENCODE_ANY);
			output->jsonrpc = malloc(strlen(foo) - 1);
			strncpy(output->jsonrpc, foo + 1, strlen(foo) - 2);
			output->jsonrpc[strlen(foo) - 2] = 0;
			free(foo);
		}
        if (!strncmp(key, METHOD_STR, klength))
		{
            flags |= METHOD_V;
            if (json_is_string(value))
                output->method = strdup(json_string_value(value));
		}
        if (!strncmp(key, PARAMS_STR, klength))
		{
            flags |= PARAMS_V;
			output->params = json_deep_copy(value);
		}
        if (!strncmp(key, ID_STR, klength))
		{
            flags |= ID_V;
            if (json_is_string(value))
                output->id = strdup(json_string_value(value));
		}

        iter = json_object_iter_next(obj, iter);
    }
    json_object_clear(obj);
    json_decref(obj);

	if (flags == JSON_RPC_NOTIFY)
	{
		output->type = JRPC_NOTIFY;
		return 0;
	}
	if (flags == JSON_RPC_REQUEST)
	{
		output->type = JRPC_REQUEST;
		return 0;
	}

	return -1;
}

/**
 * This function is used to clear and deallocate a json_rpc_t struct
 *
 * @param:	json_rpc_t *obj
 * @return:	void
 */
void json_rpc_object_clear(json_rpc_t *obj)
{
	int rc;
	if (obj->jsonrpc != NULL)
	{
		free(obj->jsonrpc);
		obj->jsonrpc = NULL;
	}
	if (obj->method != NULL)
	{
		free(obj->method);
		obj->method = NULL;
	}
	if (obj->params != NULL)
	{
            rc = json_array_clear(obj->params);
            json_decref(obj->params);
            if (rc < 0)	d2printf(stream, "styx: json_t object could not be cleared %d\n",rc);
            obj->params = NULL;
	}
	if (obj->id != NULL)
	{
            free(obj->id);
            obj->id = NULL;
	}
	if(obj->result != NULL)
	{
            if(json_is_object(obj->result))
            {
                rc = json_object_clear(obj->result);
                json_decref(obj->result);
                if (rc < 0)	d2printf(stream, "styx: json_t object could not be cleared %d\n",rc);
            }
            else if(json_is_array(obj->result))
            {
                rc = json_array_clear(obj->result);
                json_decref(obj->result);
                if (rc < 0)	d2printf(stream, "styx: json_t object could not be cleared %d\n",rc);
            }
	}
    close(obj->fd);
    free(obj);
	return;
}


/**
 * This function is used to print a json_rpc_t object to a file offered by the caller
 *
 * @param:	json_rpc_t *obj
 * @param:	FILE	*stream
 * @return:	void
 */
void print_json_rpc_object(json_rpc_t *obj, FILE *stream)
{
    char *dump;
	switch (obj->type)
	{
		case JRPC_REQUEST:
		{
			fprintf(stream,"Request:\n");
			fprintf(stream,"\tid: 		%s\n",obj->id);
			fprintf(stream,"\tmethod: 	%s\n",obj->method);
            dump = json_dumps(obj->params, 0);
			fprintf(stream,"\targs: 	%s\n",dump);
			fprintf(stream,"\tversion: 	%s\n",obj->jsonrpc);
			break;
		}
		case JRPC_NOTIFY:
		{
			fprintf(stream,"Notify:\n");
			fprintf(stream,"\tmethod: 	%s\n",obj->method);
            dump = json_dumps(obj->params, 0);
			fprintf(stream,"\targs: 	%s\n",dump);
			fprintf(stream,"\tversion: 	%s\n",obj->jsonrpc);
			break;
		}
		default:
			fprintf(stream,"Invalid JSON-RPC object\n");
			break;
	}
    free(dump);
	fflush(stream);
	return;
}


