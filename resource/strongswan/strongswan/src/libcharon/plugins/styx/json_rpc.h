/*
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */


#ifndef JSON_RPC_H
#define JSON_RPC_H
/**
 * necessary libraries for json_rpc
 */

#include <stdio.h>
#define __GNU_SOURCE
#define __USE_GNU
#include <unistd.h>
#include <jansson.h>
#include <string.h>

/**
 * flags and strings used by json-rpc
 */
#define CODE_STR       "code"
#define MESSAGE_STR    "message"
#define DATA_STR       "data"
#define JSONRPC_STR	   "jsonrpc"
#define JSONRPC_V	   0x01
#define METHOD_STR     "method"
#define METHOD_V       0x02
#define PARAMS_STR     "params"
#define PARAMS_V       0x04
#define ID_STR         "id"
#define ID_V           0x08
#define RESULT_STR     "result"
#define ERROR_STR      "error"

#define JSON_RPC_REQUEST       0x0f
#define JSON_RPC_NOTIFY        0x07

#define JRPC_REQUEST	1
#define JRPC_NOTIFY		2
#define JRPC_REPLY      3
#define JRPC_ERROR      4
#define JRPC_INVALID	-1

#define RPC_PARSE_ERROR     -32700
#define RPC_INVALID_REQUEST -32600
#define RPC_INVALID_METHOD  -32601
#define RPC_INVALID_PARAMS  -32602
#define RPC_INTERNAL_ERROR  -32603

#define dprintf(fmt,...)	do { fprintf(stderr, "(%s, %s, %d): " fmt,	\
							__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__);  \
						   } while(0)
#define d2printf(fd,fmt,...)	do { fprintf(fd, "(%s, %s, %d): " fmt,	\
							__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__);  \
						   } while(0)

typedef struct json_rpc_t {
    int fd;
	/* type of json rpc */
	u_int8_t type;
	/* request specific fields */
	char *jsonrpc;
	char *method;
	json_t *params;
	/* optional in a json rpc notification as stated in the json-rpc standard */
	char *id;
	/* response specific fields */
	json_t *result;
}json_rpc_t;

typedef void *(*rpc_dispatch)(json_rpc_t *, json_t *, ...);

#define UNIT	64

/* API methods */

json_rpc_t *json_rpc_object_init(int fd, FILE* log);
int json_rpc_object_decode(json_rpc_t *, const char* input);
void print_json_rpc_object(json_rpc_t *, FILE *);
void json_rpc_object_clear(json_rpc_t*);
int json_rpc_dispatch(json_rpc_t *, void *);
int json_rpc_register_method(const char *, void *);
int json_rpc_unregister_method(const char *);
void json_rpc_unregister_all();
void json_rpc_reply(json_rpc_t *context, int type, json_t *payload);
json_t *json_rpc_error_payload(int code, const char *message);
#endif
