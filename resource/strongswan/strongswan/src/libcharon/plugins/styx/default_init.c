/*
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */


#include "styx.h"
#include "styx_backend.h"
#include "json_rpc.h"

#define BUFFERSIZE 500


static void dump_end(FILE *dbg,stroke_end_t *end)
{
   fprintf(dbg,"-> stroke_end\n");
   fprintf(dbg,"\tauth1    = %s\n", end->auth);
   fprintf(dbg,"\tauth2    = %s\n", end->auth2);
   fprintf(dbg,"\tid1      = %s\n", end->id);
   fprintf(dbg,"\tid2      = %s\n", end->id2);
   fprintf(dbg,"\teap_id   = %s\n", end->eap_id);
   fprintf(dbg,"\trsakey   = %s\n", end->rsakey);
   fprintf(dbg,"\tcert     = %s\n", end->cert);
   fprintf(dbg,"\tcert2    = %s\n", end->cert2);
   fprintf(dbg,"\tca       = %s\n", end->ca);
   fprintf(dbg,"\tca2      = %s\n", end->ca2);
   fprintf(dbg,"\tgroups   = %s\n", end->groups);
   fprintf(dbg,"\tc_policy = %s\n", end->cert_policy);
   fprintf(dbg,"\tupdown   = %s\n", end->updown);
   fprintf(dbg,"\taddress  = %s\n", end->address);
   fprintf(dbg,"\tike_port = %d\n", end->ikeport);
   fprintf(dbg,"\tsourceip = %s\n", end->sourceip);
   fprintf(dbg,"\tsip_mask = %d\n", end->sourceip_mask);
   fprintf(dbg,"\tsubnets  = %s\n", end->subnets);
   fprintf(dbg,"\tsendcert = %d\n", end->sendcert);
   fprintf(dbg,"\thaccess  = %d\n", end->hostaccess);
   fprintf(dbg,"\ttohost   = %d\n", end->tohost);
   fprintf(dbg,"\tprotocol = %d\n", end->protocol);
   fprintf(dbg,"\tport     = %d\n", end->port);
   fflush(dbg);
}

stroke_end_t default_stroke_end(stroke_end_t *end)
{
    end->auth = strdup("pubkey");
    end->auth2 = NULL;
    end->id = NULL;
    end->id2 = NULL;
    end->eap_id = NULL;
    end->rsakey = NULL;
    end->cert = NULL;
    end->cert2 = NULL;
    end->ca = NULL;
    end->ca2 = NULL;
    end->groups = NULL;
    end->cert_policy = NULL;
    end->updown = NULL;
    end->address = NULL;
    end->ikeport = 500;
    end->sourceip = NULL;
    end->sourceip_mask = 0;
    end->subnets = NULL;
    end->sendcert = 1;
    end->hostaccess = 0;
    end->tohost = 1;
    end->protocol = 0;
    end->port = 0;
    return *end;
}

void default_styx_end_state(styx_end_state_t *end)
{
    end->allow_any = false;
    end->has_natip = 0;
    end->has_sourceip = 0;
}

styx_conn_t *default_conn()
{
    styx_conn_t *conn;
    conn = malloc_thing(styx_conn_t);
    default_stroke_end(&(conn->me));
    default_stroke_end(&(conn->other));
    default_styx_end_state(&(conn->me_state));
    default_styx_end_state(&(conn->other_state));
    conn->name = NULL;
    conn->version = 2;
    conn->eap_identity = NULL;
    conn->aaa_identity = NULL;
    conn->xauth_identity = NULL;
    conn->mode = 2;
    conn->mobike = 1;
    conn->aggresive = 0;
    conn->force_encap = 0;
    conn->ipcomp = 0;
    conn->inactivity = 0;
    conn->proxy_mode = 0;
    conn->install_policy = 1;
    conn->close_action = 0;
    conn->reqid = 0;
    conn->tfc = 0;
    conn->crl_policy = 0;
    conn->unique = 0;
    conn->algorithms.ike = strdup("aes128-sha1-modp2048,3des-sha1-modp1536");
    conn->algorithms.esp = strdup("aes128-sha1,3des-sha1");
    conn->rekey.reauth = 1;
    conn->rekey.ipsec_lifetime = 3600;
    conn->rekey.ike_lifetime = 10800;
    conn->rekey.margin = 540;
    conn->rekey.life_bytes = 0;
    conn->rekey.margin_bytes = 0;
    conn->rekey.life_packets = 0;
    conn->rekey.margin_packets = 0;
    conn->rekey.tries = 3;
    conn->rekey.fuzz = 100;
    conn->dpd.delay = 30;
    conn->dpd.action = 0;
    conn->ikeme.mediation = 0;
    conn->ikeme.mediated_by = NULL;
    conn->ikeme.peerid = NULL;
    conn->mark_in.value = 0;
    conn->mark_in.mask = 0;
    conn->mark_out.value = 0;
    conn->mark_out.mask = 0;

    conn->type_set = 0;
    conn->dont_rekey = 0;
    conn->dont_reauth = 0;
    return conn;
}

static void free_end(stroke_end_t *end)
{
    if (end->auth) free(end->auth);
    if (end->auth2) free(end->auth2);
    if (end->id) free(end->id);
    if (end->id2) free(end->id2);
    if (end->eap_id) free(end->eap_id);
    if (end->rsakey) free(end->rsakey);
    if (end->cert) free(end->cert);
    if (end->cert2) free(end->cert2);
    if (end->ca) free(end->ca);
    if (end->ca2) free(end->ca2);
    if (end->groups) free(end->groups);
    if (end->cert_policy) free(end->cert_policy);
    if (end->updown) free(end->updown);
    if (end->address) free(end->address);
    if (end->sourceip) free(end->sourceip);
    if (end->subnets) free(end->subnets);
    memset(end, 0, sizeof(stroke_end_t));
}

void free_conn(styx_conn_t *c)
{
   if (c->name) free(c->name);
   if (c->eap_identity) free(c->eap_identity);
   if (c->aaa_identity) free(c->aaa_identity);
   if (c->xauth_identity) free(c->xauth_identity);
   if (c->algorithms.ike) free(c->algorithms.ike);
   if (c->algorithms.esp) free(c->algorithms.esp);
   if (c->ikeme.mediated_by) free(c->ikeme.mediated_by);
   if (c->ikeme.peerid) free(c->ikeme.peerid);
   free_end(&c->me);
   free_end(&c->other);
   memset(c, 0, sizeof(styx_conn_t));
   free(c);
}


/**
 * Given a JSON type, return a constant string representing
 * that type.
 */
static const char *get_type_name(int type)
{
    const char *res;

    switch(type){
    case JSON_OBJECT:
        res = "object";
        break;

    case JSON_ARRAY:
        res = "array";
        break;

    case JSON_STRING:
        res = "string";
        break;

    case JSON_INTEGER:
        res = "integer";
        break;

    case JSON_REAL:
        res = "real";
        break;

    case JSON_TRUE:
        res = "true";
        break;

    case JSON_FALSE:
        res = "false";
        break;

    case JSON_NULL:
        res = "null";
        break;

    default:
        res = "(unknown type)";
        break;
    }
    return res;
}


int check_addConfig(json_rpc_t *context, json_t *params)
{
    int len, i;
    json_t *var, *err;
    char buffer[BUFFERSIZE];
    len = json_array_size(params);
    if (len < STYX_OPTIONS)
    {
        snprintf(buffer, BUFFERSIZE-1, "invalid number of parameters. "
                "Expected at least 3, got %d", len);
        err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
        json_rpc_reply(context, JRPC_ERROR, err);
        return -1;
    }
    for (i = 0; i < STYX_OPTIONS; ++i)
    {
        var = json_array_get(params, i);
        if(!json_is_string(var))
        {
            const char *content = json_dumps(var,0);
            const char *tn = get_type_name(json_typeof(var));
            snprintf(buffer, BUFFERSIZE-1, "Parameter %d ('%s') is not of type string, but of type %s", i, content, tn);
            free(content);
            err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
            json_rpc_reply(context, JRPC_ERROR, err);
            return -1;
        }
    }
    return 0;
}

int check_connect(json_rpc_t *context, json_t *params)
{
    json_t *var, *err;
    char buffer[BUFFERSIZE];
    
    if (json_array_size(params) != 2)
    {
        DBG2(DBG_CFG, "styx: connect received %d params "
                 "instead of 2", json_array_size(params)); 
        snprintf(buffer, BUFFERSIZE-1, "Invalid number of parameters. "
                "Expected 2, got %d", json_array_size(params));
        err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
        json_rpc_reply(context, JRPC_ERROR, err);
        return -1;
    }

    var = json_array_get(params, 0);
    if (!json_is_boolean(var))
    {
        const char *tn = get_type_name(json_typeof(var));
        const char *content = json_dumps(var, 0);
        DBG2(DBG_CFG, "styx: invalid first parameter for connect ('%s'); "
                 "expected a boolean, got a %s", content, tn); 
        snprintf(buffer, BUFFERSIZE-1, "Invalid first parameter for connect ('%s'); "
                "expected a boolean, got a %s", content, tn);
        free(content);
        err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
        json_rpc_reply(context, JRPC_ERROR, err);
        return -1;
    }
    
    var = json_array_get(params, 1);
    if (!json_is_string(var))
    {
        const char *tn = get_type_name(json_typeof(var));
        const char *content = json_dumps(var, 0);
        DBG2(DBG_CFG, "styx: invalid second parameter for connect ('%s'); "
                 "expected a string, got a %s", content, tn); 
        snprintf(buffer, BUFFERSIZE-1, "Invalid second parameter for connect ('%s'); "
                "expected a string, got a %s", content, tn);
        free(content);
        err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
        json_rpc_reply(context, JRPC_ERROR, err);
        return -1;
    }
    return 0;
}

int check_terminate(json_rpc_t *context, json_t *params)
{
    json_t *var, *err;
    char buffer[BUFFERSIZE];
    
    if (json_array_size(params) != 1)
    {
        DBG2(DBG_CFG, "styx: terminate received %d params "
                 "instead of 2", json_array_size(params)); 
        snprintf(buffer, BUFFERSIZE-1, "Invalid number of parameters. "
                "Expected 2, got %d", json_array_size(params));
        err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
        json_rpc_reply(context, JRPC_ERROR, err);
        return -1;
    }

    var = json_array_get(params, 0);
    if (!json_is_integer(var))
    {
        const char *tn = get_type_name(json_typeof(var));
        const char *content = json_dumps(var, 0);
        DBG2(DBG_CFG, "styx: invalid first parameter for terminate ('%s'); "
                 "expected an integer, got a %s", content, tn); 
        snprintf(buffer, BUFFERSIZE-1, "Invalid first parameter for terminate ('%s'); "
                "expected an integer, got a %s", content, tn);
        free(content);
        err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
        json_rpc_reply(context, JRPC_ERROR, err);
        return -1;
    }
    return 0;
}

int check_remove(json_rpc_t *context, json_t *params)
{
    json_t *var, *err;
    char buffer[BUFFERSIZE];
    
    if (json_array_size(params) != 1)
    {
        DBG2(DBG_CFG, "styx: invalid number of params for remove; "
                 "expected 1"); 
        snprintf(buffer, BUFFERSIZE-1, "Invalid number of parameters for remove. "
                "Expected 1");
        err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
        json_rpc_reply(context, JRPC_ERROR, err);
        return -1;
    }

    var = json_array_get(params, 0);
    if (!json_is_string(var))
    {
        const char *tn = get_type_name(json_typeof(var));
        const char *content = json_dumps(var, 0);
        DBG2(DBG_CFG, "styx: invalid first parameter for remove ('%s'); "
                 "expected a string, got a %s", content, tn); 
        snprintf(buffer, BUFFERSIZE-1, "Invalid first parameter for remove ('%s'); "
                "expected a string, got a %s", content, tn);
        free(content);
        err = json_rpc_error_payload(RPC_INVALID_PARAMS, buffer);
        json_rpc_reply(context, JRPC_ERROR, err);
        return -1;
    }

    return 0;    
}
