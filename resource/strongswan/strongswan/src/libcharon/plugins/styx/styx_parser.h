/*
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */


/**
 * @defgroup styx_parser styx_parser
 * @{ @ingroup styx
 */


#ifndef STYX_PARSER_H_
#define STYX_PARSER_H_
#include "styx.h"
#include <netdb.h>
#include <sys/socket.h>
#include <starter/keywords.h> 
#include <starter/args.h>
#include <starter/ipsec-parser.h>

#define ip_version(string)  ((strchr(string, '.') ? AF_INET : AF_INET6))
typedef struct styx_kw_entry_t styx_kw_entry_t;

struct styx_kw_entry_t {
    char *name;
    char *value;
    kw_token_t token;
};
/**
 * Parses a string to check if it contains
 * a valid option for the styx_backend API
 *
 * @param token:    the string to be parsed
 *
 * @return entry:   an styx_kw_entry if success
 *                  NULL if the string does not provide a valid configuration
 *                  option
 */
styx_kw_entry_t *parse_entry(const char *token);

/**
 * Safely deletes a styx_kw_entry structure
 *
 * @param entry:    the entry to be deleted
 *
 * @return void
 */
void del_entry(styx_kw_entry_t *entry);

/**
 * The first stage of the option parser.
 *
 * Function used to pre-parse the option list, and set all
 * the global state flags.
 *
 * @param report: List with the error messages to pass to the client,
 *                at the end of the rpc call.
 * @param conn:   A pointer to the styx_conn_t that is used to setup the
 *                connection at a later point. The preparse method, uses
 *                mainly the styx_end_state_t.
 * @param token:  The current option token.
 *
 * @return rc:    0 if success, and report will not have any additional nodes
 *                -1 is failure, and there will be additional node added to report
 */
int pre_parse(linked_list_t *report, styx_conn_t *conn, const char *token);

/**
 * The second stage of the option parser.
 *
 * Function used to do the actual parsing of the option list
 * provided via the json-rpc. The syntax used is similar to the config
 * file one.
 *
 * @param report: List with the error messages to pass to the client,
 *                at the end of the rpc call.
 * @param conn:   A pointer to the styx_conn_t that is used to setup the
 *                connection at a later point. 
 * @param token:  The current option token.
 *
 * @return rc:    0 if success, and report will not have any additional nodes
 *                -1 is failure, and there will be additional node added to report
 */
int parse_option(linked_list_t *report, styx_conn_t *conn, const char * token);

/**
 * Here the straightforward options are resolved. More exactly, the ones
 * that don't need a lot of additional interpretation (for example integers,
 * or time intervals).
 *
 * @param base: The base of the structure we are trying to fill
 * @param option: A pointer to a styx_kw_entry_t structure that contains
 *                the option type, as well as the key-value pair, of the option
 *
 * @return rc:  0 if success
 *              1 if internal error
 *              -1 if input error
 */
int parse_direct_value(char *base, styx_kw_entry_t *option);

/**
 * Function used to parse the options that refer to an end-point
 *
 * @param conn: The styx_conn_t base structure from which
 *              the end is part of
 * @param end: The styx_end_t structure to be filled by this function
 * @param state: The styx_state_t structure used to hold various flags
 *              from the phase one parser
 * @param index: The index into the token list
 * @param option: The styx_kw_entry_t used to store a key value mapping
 *              for each option provided to the parser
 * @return  0 if success, 1 if there was an error.
 */
int load_end(styx_conn_t *conn, 
             stroke_end_t *end, 
             styx_end_state_t *state,
             kw_token_t index,
             styx_kw_entry_t *option);



#endif /** STYX_PARSER_H_ @}*/
