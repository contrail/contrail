# This script checks if the OpenNebula sensor is running and starts the sensor otherwise
# This script should be installed as a cron job: */5 * * * * /CONTRAIL_DIR/cron.sh (check if the sensor is running every 5 minutes)
#
# @author Gregor Beslic gregor.beslic@cloud.si
#
#!/bin/bash

ps -ef | grep one-sensor.jar | grep -v grep > s_running
if [ ! -s s_running ] ; then
  java -jar one-sensor.jar &
fi
rm s_running
