/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.sensors.host;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import eu.contrail.infrastructure_monitoring.utils.SensorUtil;

public class LoadAveragesSensor {
	private final static String CMD_LOAD_AVGS = "uptime";
	private String[] loadAvgs = null;
	
	public LoadAveragesSensor() throws IOException {
		String[] cmdResponse = SensorUtil.shellCmd(CMD_LOAD_AVGS);
		getLoadAverages(cmdResponse);
	}
	
	public LoadAveragesSensor(String cmdResponse) throws UnsupportedEncodingException, IOException {
		getLoadAverages(SensorUtil.convertInputStream(new ByteArrayInputStream(cmdResponse.getBytes("UTF-8"))));
	}
	
	private void getLoadAverages(String[] response) {
		String find = "load average: ";
		String loadAvgRow = response[0].substring(response[0].indexOf(find) + find.length());
		this.loadAvgs = loadAvgRow.split(",");
	}
	
	public String getLoadOne() {
		return loadAvgs[0].trim();
	}
	
	public String getLoadFive() {
		return loadAvgs[1].trim();
	}
}
