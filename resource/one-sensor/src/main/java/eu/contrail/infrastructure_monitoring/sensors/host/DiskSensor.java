/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.sensors.host;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import eu.contrail.infrastructure_monitoring.utils.SensorUtil;

public class DiskSensor {
	private static final String CMD_DISK_FREE = "df |grep FS";
	private static final String[] filesystems = {"hda", "sda", "vda"};
	
	private String[] diskInfo = null;
	
	public DiskSensor() throws IOException {
		for(String s : filesystems) {
			String[] cmdResponse = SensorUtil.shellCmd(CMD_DISK_FREE.replaceAll("FS", s));
			if(cmdResponse != null && cmdResponse.length == 1 && cmdResponse[0] != null && !cmdResponse[0].isEmpty()) {
				getDiskInfo(cmdResponse);
				break;
			}
		}
	}
	
	public DiskSensor(String cmdResponse) throws UnsupportedEncodingException, IOException {
		getDiskInfo(SensorUtil.convertInputStream(new ByteArrayInputStream(cmdResponse.getBytes("UTF-8"))));
	}
	
	private void getDiskInfo(String[] response) {
		this.diskInfo = response[0].split("\\s+");
	}

	public String getFree() {
		if(diskInfo == null)
			return "0";
		return diskInfo[3];
	}
	
	public String getTotal() {
		if(diskInfo == null)
			return "0";
		return diskInfo[1];
	}

	public String getUsed() {
		if(diskInfo == null)
			return "0";
		return diskInfo[2];
	}
}
