/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.sensors.vm;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import eu.contrail.infrastructure_monitoring.OneSensor;
import eu.contrail.infrastructure_monitoring.utils.SensorUtil;

public class VmSensor {
	
	private String hostFqdn = "N/A";
	private String cpuSeconds = "N/A";
	private String cpuShare = "N/A";
	private String memHost = "N/A";
	
	private String command = "virsh --connect qemu:///system --readonly";
	private String commandDominfo = command + " dominfo";
	private String commandProcessInfo = "ps auxwww | grep -- '-uuid UUID' | grep -v grep";
	
	public VmSensor(String domName) {
		String[] response;
		try {
			response = SensorUtil.shellCmd(commandDominfo + " " + domName);
			if (response != null && response.length > 0) {
				String[] line = null;
				// Fetch data from domInfo
				for (int i=0; i<response.length; i++) {
					line = response[i].split(":");
					if(line.length != 2)
						continue;
					
					String key = line[0].trim();
					String val = line[1].trim();
					
					if (key.equalsIgnoreCase("uuid")) {
						String[] result = SensorUtil.shellCmd(commandProcessInfo.replaceFirst("UUID", val));
						if(result != null && result.length > 0) {
							String[] psInfo = result[0].split("\\s+");
							String processId = psInfo[1];
							
							if(!processId.isEmpty()) {
								float secs1 = doGetCpuSeconds(processId);
								Thread.sleep(1000);
								float secs2 = doGetCpuSeconds(processId);
								this.cpuShare = String.valueOf(Math.round(100 * (secs2 - secs1)));
								this.cpuSeconds = String.valueOf(secs2);
								float currMemHost = doGetMemHost(processId);
								if(currMemHost != 0) {
									this.memHost = String.valueOf(currMemHost);
								}
							}
						}
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private float doGetMemHost(String processId) throws IOException {
		String[] fullProcInfoArr = SensorUtil.shellCmd("cat /proc/" + processId + "/status");
		String[] row;
		String rss = null;
		for(String s : fullProcInfoArr) {
			row = s.split(":");
			if(row[0].equalsIgnoreCase("VmRSS")) {
				rss = row[1].trim().replaceAll(" kB", "").trim();
				BigDecimal bd = new BigDecimal(Float.valueOf(rss)/1024).setScale(3, RoundingMode.HALF_EVEN);
				return (float) bd.doubleValue();
			}
		}
		return 0;
	}
	
	private float doGetCpuSeconds(String processId) throws IOException {
		String[] fullProcInfoArr = SensorUtil.shellCmd("cat /proc/" + processId + "/stat");
		String[] procStat = fullProcInfoArr[0].split("\\s+");

		int ticksPerSecond = Integer.parseInt(OneSensor.getTicksPerSecond());
		float userTicks = Float.parseFloat(procStat[13]); // scheduled in user mode
		float sysTicks = Float.parseFloat(procStat[14]); // scheduled in kernel mode
		float myTicks = userTicks + sysTicks;
		float cpuSeconds = myTicks / ticksPerSecond;
		return cpuSeconds;
	}

	public String getHostFqdn() {
		return hostFqdn;
	}

	public String getCpuSeconds() {
		return cpuSeconds;
	}
	
	public String getCpuShare() {
		return cpuShare;
	}

	public String getMemHost() {
		return memHost;
	}
}
