/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.sensors.host;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import eu.contrail.infrastructure_monitoring.utils.SensorUtil;

public class CpuSensor {
	private static final String CMD_CPU_CORES = "grep -i processor /proc/cpuinfo | wc -l";
	private static final String CMD_CPU_SPEED = "cat /proc/cpuinfo | grep \"cpu MHz\" | sed 's/[^0-9\\.]//g'";
	// Command iostat requires sysstat to be installed on the system
	private static final String CMD_CPU_USAGE = "iostat";
	
	private String cores = null;
	private String speed = null;
	private String[] stats = null;
	
	public CpuSensor() throws IOException {
		String[] response = SensorUtil.shellCmd(CMD_CPU_CORES);
		cores = response[0];
		response = SensorUtil.shellCmd(CMD_CPU_SPEED);
		speed = response[0];
		getStats(SensorUtil.shellCmd(CMD_CPU_USAGE));
	}
	
	public CpuSensor(String cmdResponse) throws IOException {
		getStats(SensorUtil.convertInputStream(new ByteArrayInputStream(cmdResponse.getBytes("UTF-8"))));
	}
	
	private void getStats(String[] response) {
		stats = response[3].split("\\s+");
	}
	
	public String getCoresCount() {
		return cores;
	}
	
	public String getSpeed() {
		return speed;
	}
	
	public String getCpuUser() {
		return stats[1];
	}
	
	public String getCpuSystem() {
		return stats[3];
	}
	
	public String getCpuIdle() {
		return stats[6];
	}
} 
