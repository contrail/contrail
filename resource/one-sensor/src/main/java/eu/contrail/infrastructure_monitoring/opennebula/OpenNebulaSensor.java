/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.opennebula;

import com.rabbitmq.client.*;
import eu.contrail.infrastructure_monitoring.sensors.host.*;
import eu.contrail.infrastructure_monitoring.sensors.vm.VmSensor;
import org.apache.log4j.Logger;
import org.json.JSONStringer;
import org.json.JSONWriter;

import java.net.InetAddress;
import java.net.UnknownHostException;


public class OpenNebulaSensor extends Thread {
    private static int CONN_RETRY_INTERVAL = 15;
    private static Logger log = Logger.getLogger(OpenNebulaSensor.class);

    private String hostFqdn;
    private String hostname;
    private ConnectionFactory factory;
    private Connection connection;
    private QueueingConsumer consumer;
    private Channel channel;

    public OpenNebulaSensor(String rabbitMQHost) throws UnknownHostException {
        InetAddress addr = InetAddress.getLocalHost();
        hostname = addr.getHostName();
        hostFqdn = addr.getCanonicalHostName();
        factory = new ConnectionFactory();
        factory.setHost(rabbitMQHost);
    }

    public void connect() throws InterruptedException {
        while (true) {
            try {
                log.info("Connecting to RabbitMQ server on " + factory.getHost() + "...");
                connection = factory.newConnection();
                channel = connection.createChannel();
                channel.queueDeclare(hostname, false, false, false, null);
                channel.basicQos(1);
                consumer = new QueueingConsumer(channel);
                channel.basicConsume(hostname, false, consumer);
                log.info("Connection to RabbitMQ established successfully.");
                return;
            }
            catch (Exception e) {
                log.error(String.format("Failed to establish connection to RabbitMQ. Retrying in %d seconds.",
                        CONN_RETRY_INTERVAL));
            }

            Thread.sleep(CONN_RETRY_INTERVAL * 1000);
        }
    }

    public void reconnect() throws InterruptedException {
        if (connection.isOpen()) {
            try {
                connection.close();
            }
            catch (Exception ignored) {
            }
        }

        connect();
    }

    @Override
    public void run() {
        // connect to RabbitMQ server
        try {
            connect();
        }
        catch (InterruptedException e) {
            shutdown();
            return;
        }

        // handle requests until interrupt is received
        while (true) {
            try {
                if (!channel.isOpen()) {
                    log.info("Connection to RabbitMQ failed. Trying to reconnect...");
                    reconnect();
                }

                QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                String message = new String(delivery.getBody());
                if (message != null) {
                    AMQP.BasicProperties props = delivery.getProperties();
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                            .Builder()
                            .correlationId(props.getCorrelationId())
                            .build();
                    if (log.isTraceEnabled()) {
                        log.trace(String.format("Received new request on queue '%s': %s",
                                delivery.getEnvelope().getRoutingKey(), message));
                    }

                    String response = parseResponse(message);

                    if (response != null) {
                        if (!response.isEmpty()) {
                            channel.basicPublish("", props.getReplyTo(), replyProps, response.getBytes());
                        }
                        channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                        log.trace("The request was processed successfully.");
                    }
                }
            }
            catch (InterruptedException e) {
                log.trace("Interrupt received.");
                shutdown();
                break;
            }
            catch (Exception e) {
                log.error("OpenNebulaSensor encountered an error: " + e.getMessage(), e);
            }
        }
    }

    private void shutdown() {
        log.trace("shutdown() started.");
        if (channel != null && channel.isOpen()) {
            try {
                channel.close();
            }
            catch (Exception ignored) {
            }
        }

        if (connection != null && connection.isOpen()) {
            try {
                connection.close();
            }
            catch (Exception ignored) {
            }
        }
        log.info("Connection to RabbitMQ closed.");
    }

    public String parseResponse(String message) {
        String[] split = message.split("_");
        JSONWriter json;
        try {
            json = new JSONStringer().object();

            if (split[0].equalsIgnoreCase("VmData")) {
                return fetchVmData(split[1], json);
            }
            else if (message.equalsIgnoreCase("HostData")) {
                return fetchHostData(json);
            }
            else {
                log.warn("Unexpected message received: " + message);
                return null;
            }
        }
        catch (Exception e) {
            log.error(String.format("Failed to handle the request '%s'.", message), e);
            return null;
        }
    }

    private String fetchHostData(JSONWriter json) throws Exception {
        log.trace("Generating HostData...");
        json.key("FQDN").value(hostFqdn);

        LoadAveragesSensor la = new LoadAveragesSensor();
        json.key("LOAD_ONE").value(la.getLoadOne());
        json.key("LOAD_FIVE").value(la.getLoadFive());

        IpAddressesSensor ipa = new IpAddressesSensor();
        json.key("IP_ADDRESS").value(ipa.getCommaDelimited());

        HostnameSensor hn = new HostnameSensor();
        json.key("HOSTNAME").value(hn.getHostname());

        MemorySensor mem = new MemorySensor();
        json.key("MEM_TOTAL").value(mem.getTotal());
        json.key("MEM_USAGE").value(mem.getUsage());
        json.key("MEM_FREE").value(mem.getFree());

        CpuSensor cpu = new CpuSensor();
        json.key("CPU_CORES_COUNT").value(cpu.getCoresCount());
        json.key("CPU_SPEED").value(cpu.getSpeed());
        json.key("CPU_USER").value(cpu.getCpuUser());
        json.key("CPU_SYSTEM").value(cpu.getCpuSystem());
        json.key("CPU_IDLE").value(cpu.getCpuIdle());

        DiskSensor disk = new DiskSensor();
        json.key("SHARED_IMAGES_DISK_FREE").value(disk.getFree());
        json.key("SHARED_IMAGES_DISK_USED").value(disk.getTotal());

        NetworkSensor network = new NetworkSensor();
        json.key("NET_BYTES_TX").value(network.getNetTx());
        json.key("NET_BYTES_RX").value(network.getNetRx());

        // Cluster conf
        HostConfig hc = new HostConfig();
        json.key("AUDITABILITY").value(hc.getAuditability());
        json.key("LOCATION").value(hc.getLocation());
        json.key("SAS70_COMPLIANCE").value(hc.getSasCompliance());
        json.key("CCR").value(hc.getCcr());
        json.key("DATA_CLASSIFICATION").value(hc.getDataClassification());
        json.key("HW_REDUNDANCY_LEVEL").value(hc.getRedundancyLevel());
        json.key("DISK_THROUGHPUT").value(hc.getDiskThroughput());
        json.key("NET_THROUGHPUT").value(hc.getNetThroughput());
        json.key("DATA_ENCRYPTION").value(hc.getDataEncryption());

        double coresCount = Double.parseDouble(cpu.getCoresCount());
        double loadOne = Double.parseDouble(la.getLoadOne());
        double loadFive = Double.parseDouble(la.getLoadFive());
        json.key("LOAD_ONE_NORM").value(String.valueOf(loadOne / coresCount));
        json.key("LOAD_FIVE_NORM").value(String.valueOf(loadFive / coresCount));

        String jsonString = json.endObject().toString();
        log.trace("HostData generated successfully.");
        if (log.isTraceEnabled()) {
            log.trace(String.format("HostData(%s):\n%s", hostFqdn, jsonString));
        }
        return jsonString;
    }

    private String fetchVmData(String vmNameAndIp, JSONWriter json) throws Exception {
        log.trace("Generating VmData...");

        String[] parts = vmNameAndIp.split(";");
        String vmName = parts[0];
        String ip = parts[1];

        VmSensor vmSensor = new VmSensor(vmName);
        json.key("HOST_FQDN").value(hostFqdn);
        json.key("CPU_SECONDS").value(vmSensor.getCpuSeconds());
        json.key("CPU_SHARE").value(vmSensor.getCpuShare());
        json.key("MEM_HOST").value(vmSensor.getMemHost());

        HostConfig hc = new HostConfig();
        json.key("LOCATION").value(hc.getLocation());
        CpuSensor cpu = new CpuSensor();
        json.key("CPU_SPEED").value(cpu.getSpeed());

        String jsonString = json.endObject().toString();
        log.trace("VmData generated successfully.");
        if (log.isTraceEnabled()) {
            log.trace(String.format("VmData(%s):\n%s", vmName, jsonString));
        }
        return jsonString;
    }

    public String getHostname() {
        return hostname;
    }
}
