/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.sensors.host;

import java.io.IOException;

import eu.contrail.infrastructure_monitoring.utils.SensorUtil;

public class NetworkSensor {
	private static final String CMD_NET_ROUTE_DEV = "awk ' { if ($2 == '00000000') print $1 ; } ' < /proc/net/route";
	private static final String CMD_NET_RX = "grep <routeDevice> /proc/net/dev | cut -d':' -f2 | awk ' { print $1; } '";
	private static final String CMD_NET_TX = "grep <routeDevice> /proc/net/dev | cut -d':' -f2 | awk ' { print $9; } '";
	
	private String routeDevice = null;
	private String netRx = null;
	private String netTx = null;
	
	public NetworkSensor() throws IOException {
		String[] response = SensorUtil.shellCmd(CMD_NET_ROUTE_DEV);
		if(response != null && response.length == 1 && !response[0].isEmpty()) {
			routeDevice = response[0];
			
			String[] resRx = SensorUtil.shellCmd(CMD_NET_RX.replace("<routeDevice>", routeDevice));
			if(resRx != null && !resRx[0].isEmpty())
				setNetRx(resRx[0]);
				
			String[] resTx = SensorUtil.shellCmd(CMD_NET_TX.replace("<routeDevice>", routeDevice));
			if(resTx != null && !resTx[0].isEmpty())
				setNetTx(resTx[0]);
		}
	}
	
	public String getNetRx() {
		return netRx;
	}

	public void setNetRx(String netRx) {
		this.netRx = netRx;
	}

	public String getNetTx() {
		return netTx;
	}

	public void setNetTx(String netTx) {
		this.netTx = netTx;
	}
} 
