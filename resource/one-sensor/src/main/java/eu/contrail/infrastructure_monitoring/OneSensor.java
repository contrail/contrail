/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

import eu.contrail.infrastructure_monitoring.opennebula.OpenNebulaSensor;
import eu.contrail.infrastructure_monitoring.opennebula.events.EventsSocketListener;

public class OneSensor {

	private static final String PROPS_FILE_DEFAULT = "/etc/contrail/contrail-one-sensor/one-sensor.config";
	private static String PROPS_FILE = null;
	private static final int SHUTDOWN_TIME = 1;
	private static String RABBIT_MQ_HOST;
	private static String HOST_PROPERTIES_FILE;
	private static String TICKS_PER_SECOND;
	private static String LOG4J_LOG_LEVEL;
    private OpenNebulaSensor openNebulaSensor;
	
	private static String hostname;
	
	private static Logger log = Logger.getLogger(OneSensor.class);
	
	public static void main(String[] args) {
		if(args != null && args.length > 0 && !args[0].isEmpty())
			new OneSensor(args[0]);
		else
			new OneSensor(null);
	}
	
	public OneSensor(String propsFile) {
		try {
            log.info("One-sensor is starting...");
	    	Properties properties = new Properties();
	    	PROPS_FILE = propsFile == null || propsFile.isEmpty() ? PROPS_FILE_DEFAULT : propsFile;
	    	properties.load(new FileInputStream(PROPS_FILE));
			RABBIT_MQ_HOST = properties.getProperty("rabbit_mq_host");
	    	HOST_PROPERTIES_FILE = properties.getProperty("host_properties_file");
			TICKS_PER_SECOND = properties.getProperty("ticks_per_second");
			
			LOG4J_LOG_LEVEL = properties.getProperty("log4j_log_level");
			if((LOG4J_LOG_LEVEL != null) && !LOG4J_LOG_LEVEL.isEmpty()) {
				if(LOG4J_LOG_LEVEL.equalsIgnoreCase("info")) {
					LogManager.getRootLogger().setLevel((Level)Level.INFO);
				}
				else if(LOG4J_LOG_LEVEL.equalsIgnoreCase("debug")) {
					LogManager.getRootLogger().setLevel((Level)Level.DEBUG);
				}
				else if(LOG4J_LOG_LEVEL.equalsIgnoreCase("trace")) {
					LogManager.getRootLogger().setLevel((Level)Level.TRACE);
				}
			}
			
			Thread runtimeHookThread = new Thread() {
				public void run() {
				    shutdownHook(); 
				}
			};
			Runtime.getRuntime().addShutdownHook (runtimeHookThread);
		
			// Listen to OpenNebula hooks on socket and publish these events on RabbitMq "audit" queue
			Thread eventListener = new EventsSocketListener();
			eventListener.start();

            // start OpenNebulaSensor
            openNebulaSensor = new OpenNebulaSensor(RABBIT_MQ_HOST);
            openNebulaSensor.start();

            log.info("One-sensor started successfully.");
        }
        catch (Exception e) {
            System.out.println("One-sensor failed to start: " + e.getMessage());
            log.error("One-sensor failed to start: " + e.getMessage(), e);
        }
	}
	
	private void shutdownHook() {
        log.trace("Received shutdown signal. One-sensor is shutting down.");
        openNebulaSensor.interrupt();

		long t0 = System.currentTimeMillis();
		while (true) {
		    try {
		    	Thread.sleep (500);
		    }
		    catch (Exception e) {
		    	log.error("Exception: " + e.toString());
		    	break;
		    }
		    if (System.currentTimeMillis() - t0 > SHUTDOWN_TIME * 1000)
		    		break;
		}
		log.trace("Shutdown completed.");
	}

	public static String getRabbitMqHost() {
		return RABBIT_MQ_HOST;
	}
	
	public static String getHostname() {
		return hostname;
	}
	
	public static String getHostPropertiesFile() {
		return HOST_PROPERTIES_FILE;
	}

	public static String getTicksPerSecond() {
		return TICKS_PER_SECOND;
	}
}
