/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.sensors.host;

import eu.contrail.infrastructure_monitoring.OneSensor;
import eu.contrail.infrastructure_monitoring.hostconfig.IConfigReader;
import eu.contrail.infrastructure_monitoring.hostconfig.PropertiesReader;

public class HostConfig {
	private IConfigReader hostConfig;
	
	public HostConfig(String pathToFile) {
		if(pathToFile != null && !pathToFile.isEmpty())
	    	hostConfig = new PropertiesReader(pathToFile);
	}
	
	public HostConfig() {
		if(OneSensor.getHostPropertiesFile() != null && !OneSensor.getHostPropertiesFile().isEmpty())
	    	hostConfig = new PropertiesReader();
	}
	
	public String getAuditability() {
		return hostConfig.readKey("Auditability");
	}

	public String getLocation() {
		return hostConfig.readKey("Location.CountryCode");
	}

	public String getSasCompliance() {
		return hostConfig.readKey("SAS70");
	}

	public String getCcr() {
		return hostConfig.readKey("CCR");
	}

	public String getDataClassification() {
		return hostConfig.readKey("DataClassification");
	}

	public String getRedundancyLevel() {
		return hostConfig.readKey("HWRedundancyLevel");
	}

	public String getDiskThroughput() {
		return hostConfig.readKey("DiskThroughput");
	}

	public String getNetThroughput() {
		return hostConfig.readKey("NetThroughput");
	}

	public String getDataEncryption() {
		return hostConfig.readKey("DataEncryption");
	}
} 
