/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class SensorUtil {
    
	private static Logger log = Logger.getLogger(SensorUtil.class);
	
	public static String[] shellCmd(String command) throws IOException {
	    String[] cmd = {"/bin/bash", "-c", command};
        Process p = Runtime.getRuntime().exec(cmd);
        String s = null;
    	BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        while ((s = stdError.readLine()) != null) {
        	log.warn("Shell command STD_ERR: " + s);
        }
        String[] res = convertInputStream(p.getInputStream());
        p.destroy();
    	return res;
    }
	
	public static String[] convertInputStream(InputStream is) throws IOException {
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(is));
        String[] retStr = new String[1];
        int i = 0;
        String s = null;
        while ((s = stdInput.readLine()) != null) {
        	if(i >= retStr.length)
        		retStr = SensorUtil.extendArray(retStr);
        			
        	retStr[i++] = s;
        }
    	return retStr;
	}

    static String[] extendArray(String[] arr) {
    	String[] rtn = new String[arr.length+1];
    	int i = 0;
    	for (String s : arr) {
    		rtn[i++] = s;
    	}
    	return rtn;
    }
}
