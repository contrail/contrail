package eu.contrail.infrastructure_monitoring;

import com.rabbitmq.client.*;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class RabbitMQTestClient {

    private static final String RABBIT_MQ_HOST = "10.31.1.10";

    @Test
    public void testOpenNebulaSensor() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(RABBIT_MQ_HOST);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        String replyQueueName = channel.queueDeclare().getQueue();
        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(replyQueueName, true, consumer);

        String corrId = "1";
        String message = "HostData";
        String queue = "n0010";
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();
        channel.queueDeclare(queue, false, false, false, null);
        channel.basicPublish("", queue, props, message.getBytes());

        QueueingConsumer.Delivery delivery = consumer.nextDelivery(5000);
        if (delivery != null) {
            String response = new String(delivery.getBody());
            System.out.println(response);
        }

        channel.close();
        connection.close();
    }
}