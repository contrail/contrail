/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import eu.contrail.infrastructure_monitoring.sensors.host.CpuSensor;
import eu.contrail.infrastructure_monitoring.sensors.host.DiskSensor;
import eu.contrail.infrastructure_monitoring.sensors.host.HostConfig;
import eu.contrail.infrastructure_monitoring.sensors.host.IpAddressesSensor;
import eu.contrail.infrastructure_monitoring.sensors.host.LoadAveragesSensor;
import eu.contrail.infrastructure_monitoring.sensors.host.MemorySensor;
import junit.framework.TestCase;

public class ParseHostResponseTest extends TestCase {
	
	public ParseHostResponseTest(String name) {
       super(name);
    }
	
	public void testLoadAveragesSensor() throws UnsupportedEncodingException, IOException {
		String testCase = " 11:49:08 up  3:06,  3 users,  load average: 0.00, 0.01, 0.05";
		LoadAveragesSensor la = new LoadAveragesSensor(testCase);
		
		assertEquals("0.00", la.getLoadOne());
		assertEquals("0.01", la.getLoadFive());
	}
	
	public void testIpAddressesSensor() throws UnsupportedEncodingException, IOException {
		String testCase = "192.168.29.117\n"+
						  "10.30.2.5\n"+
						  "192.168.122.1";
		
		IpAddressesSensor ipa = new IpAddressesSensor(testCase);
		
		assertEquals("192.168.29.117, 10.30.2.5, 192.168.122.1", ipa.getCommaDelimited());
	}
	
	public void testMemorySensor() throws UnsupportedEncodingException, IOException {
		String testCase = "             total       used       free     shared    buffers     cached\n"+
						  "Mem:          7985       5340       2644          0        228       2594\n"+
						  "-/+ buffers/cache:       2518       5466\n"+
						  "Swap:         8173          0       8173";
		
		MemorySensor mem = new MemorySensor(testCase);
		
		assertEquals("7985", mem.getTotal());
		assertEquals("5340", mem.getUsage());
		assertEquals("2644", mem.getFree());
	}
	
	public void testCpuSensor() throws IOException {
		String testCase = "Linux 2.6.38-11-generic (ubuntu) 	09/07/2011 	_x86_64_	(8 CPU)\n"+
						  "\n"+
						  "avg-cpu:  %user   %nice %system %iowait  %steal   %idle\n"+
						  "           2.26    0.03    0.47    0.23    0.00   97.01\n"+
						  "\n"+
						  "Device:            tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn\n"+
						  "sda               9.76       100.04        58.73    2541903    1492206";
		
		CpuSensor cpu = new CpuSensor(testCase);
		
		assertEquals("2.26", cpu.getCpuUser());
		assertEquals("0.47", cpu.getCpuSystem());
		assertEquals("97.01", cpu.getCpuIdle());
	}
	
	public void testHostConfig() {
		String testFile = "src/test/resources/hostConfig.test";
		
		HostConfig hc = new HostConfig(testFile);
		
		assertEquals("1", hc.getAuditability());
		assertEquals("SI", hc.getLocation());
		assertEquals("0", hc.getSasCompliance());
		assertEquals("1", hc.getCcr());
		assertEquals("Confidential", hc.getDataClassification());
		assertEquals("Better", hc.getRedundancyLevel());
		assertEquals("STANDARD", hc.getDiskThroughput());
		assertEquals("STANDARD", hc.getNetThroughput());
		assertEquals("False", hc.getDataEncryption());
	}
	
	public void testHarddiskSensor() throws UnsupportedEncodingException, IOException {
		String testCase = "/dev/sda1             68682272  21349932  43843420  33% /";

		DiskSensor disk = new DiskSensor(testCase);

		assertEquals("68682272", disk.getTotal());
		assertEquals("21349932", disk.getUsed());
		assertEquals("43843420", disk.getFree());
	}
}