# Add the local VIN address to the host
#
# Usage:
#     safeconfig add-iproute2-physical-address.conf <local-interface-name>
#         <local-vin-address>

# IF_NAME:     The name of the local interface corresponding to this network
# LOCAL_VIN:   The IP address of the local VIN
parameters IF_NAME LOCAL_VIN

# Do sanity checks on the parameters.

# Require that an identifier only consists of alphanumeric characters, or 
#  '-'. (Note that the '-' must be at the end, otherwise it introduces a range.)
assertmatch [[:alnum:]-]+ $(IF_NAME)

# Require that the addresses only consist of hex digits or ':' or '.' characters.
# This matches both ipv4 and ipv6 addresses.
assertmatch [[:xdigit:]:.]+ $(LOCAL_VIN)

debugprint "IF_NAME=$(IF_NAME) LOCAL_VIN=$(LOCAL_VIN)"

# Add the new interface
run /bin/ip link add $(IF_NAME) mtu 1476 type dummy
debugprint Added interface $(IF_NAME)

# Add the IP address to the interface
run /bin/ip addr add $(LOCAL_VIN)/32 dev $(IF_NAME)
debugprint Added address $(LOCAL_VIN) to interface $(IF_NAME)

# Bring the interface up
run /bin/ip link set $(IF_NAME) up
debugprint Interface $(IF_NAME) is now up

debugrun /bin/ip addr show
