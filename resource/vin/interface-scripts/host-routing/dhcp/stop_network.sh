#!/bin/bash
#
# Stops a libvirt network.
#
# Usage:
#   stop_network <bridge-name>
#
# Where <bridge-name> is a bridge name on a physical host
# used by libvirt network.

die () {
    echo >&2 "$@"
    exit 1
}

set -e

[ "$#" -eq 1 ] || die "1 arguments required, $# provided: [$@]"

BR_NAME=$1

#parameters sanity check
echo "$BR_NAME" | grep -E -q '^[[:alnum:]]+$' || die "Incorrect bridge name '$BR_NAME'"

#stop libvirt network
virsh net-destroy "$BR_NAME"

#stop dhcp server
PID=(`cat "/var/run/libvirt/network/$BR_NAME.pid"`)
kill $PID

#remove leases file
rm "/var/lib/libvirt/dnsmasq/$BR_NAME.leases"
#remove hosts file
rm "/var/lib/libvirt/dnsmasq/$BR_NAME.hostsfile"
