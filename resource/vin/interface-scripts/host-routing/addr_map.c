#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void help(void) {
    fprintf(stderr, "addr_map -i ip_addr -s ip_range_start -b eth_base | -s ip_range_start -e ip_range_end -b eth_base\n");
    exit(2);
}

unsigned long parse_ip(char *ip_str) {
    unsigned int ip[4];
    if (sscanf(ip_str, "%u.%u.%u.%u", &ip[0],&ip[1],&ip[2],&ip[3]) != 4) {
        help();
    }
    return ip[3] + ip[2]*256 + ip[1]*256*256 + ip[0]*256*256*256;
}

void print_ip(unsigned int ip) {
    printf("%u.%u.%u.%u",
           ip/256/256/256,
           ip/256/256%256,
           ip/256%256,
           ip%256);
}

unsigned long long parse_mac(char *mac_str) {
    unsigned long long mac[6];
    if (sscanf(mac_str, "%2llx:%2llx:%2llx:%2llx:%2llx:%2llx", &mac[0],&mac[1],&mac[2],&mac[3],&mac[4],&mac[5]) != 6) {
        help();
    }
    return mac[5] + mac[4]*256 + mac[3]*256*256 + mac[2]*256*256*256 + mac[1]*256*256*256*256
           + mac[0]*256*256*256*256*256;
}

void print_mac(unsigned long long mac) {
    printf("%02llx:%02llx:%02llx:%02llx:%02llx:%02llx",
           mac/256/256/256/256/256,
           mac/256/256/256/256%256,
           mac/256/256/256%256,
           mac/256/256%256,
           mac/256%256,
           mac%256);
}

int main(int argc, char**argv) {
    int i;
    unsigned int ip_addr = 0, ip_range_start = 0, ip_range_end = 0;
    unsigned long long eth_base = 0;
    for (i=1; i<argc; i++) {
        if (!strcmp(argv[i], "-i") && i+1 < argc) {
            ip_addr = parse_ip(argv[++i]);
        } else if (!strcmp(argv[i], "-s") && i+1 < argc) {
            ip_range_start = parse_ip(argv[++i]);
        } else if (!strcmp(argv[i], "-e") && i+1 < argc) {
            ip_range_end = parse_ip(argv[++i]);
        } else if (!strcmp(argv[i], "-b") && i+1 < argc) {
            eth_base = parse_mac(argv[++i]);
        } else {
            help();
        }
    }
    
    if (eth_base == 0
        || (ip_addr != 0 && ip_range_end != 0)
        || (ip_addr != 0 && ip_range_start == 0)
        || (ip_range_end != 0 && ip_range_start == 0)) {
        help();
    }

    if (ip_addr != 0) {
        print_mac(eth_base + (ip_addr - ip_range_start));
        printf("\n");
    } else {
        for (i=ip_range_start; i<=ip_range_end; i++) {
            print_ip(i);
            printf(" ");
            print_mac(eth_base + (i - ip_range_start));
            printf(" %d\n", i - ip_range_start);
        }
    }
    
    return 0;
}
