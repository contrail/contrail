#!/bin/bash
#
# Starts a new libvirt network.
#
# Usage:
#   start-network <debug-flags> <bridge-name> <bridge-ip-address>
#        <netmask> <ip-range-start> <ip-range-end>
#        <mac-map-path>
#
# Where <bridge-name> is a bridge name on a physical host
# used by libvirt network, <bridge-ip-address> and <netmask>
# are bridge IP address and netmask, <ip-range-start>
# and <ip-range-end> are IP addresses used by by virtual machines
# and <mac-map-path> is a IP-Mac address mapping file path.

die () {
    echo >&2 "$@"
    exit 1
}

set -e

[ "$#" -eq 7 ] || die "7 arguments required, $# provided: [$@]"

FLAGS=$1
BR_NAME=$2
BR_IP_ADDR=$3
NETMASK=$4
IP_RANGE_START=$5
IP_RANGE_END=$6
MAC_MAP=$7

case "$FLAGS" in
  *[iI]*)
    id
esac
case "$FLAGS" in
  *[xX]*)
    set -x
esac

#parameters sanity check
echo "$BR_NAME" | grep -E -q '^[[:alnum:]-]+$' || die "Incorrect bridge name '$BR_NAME'"
echo "$BR_IP_ADDR" | grep -E -q '^[[:xdigit:]:.]+$' || die "Incorrect bridge IP address '$BR_IP_ADDR'"
echo "$NETMASK" | grep -E -q '^[[:xdigit:]:.]+$' || die "Incorrect netmask '$NETMASK'"
echo "$IP_RANGE_START" | grep -E -q '^[[:xdigit:]:.]+$' || die "Incorrect IP range start '$IP_RANGE_START'"
echo "$IP_RANGE_END" | grep -E -q '^[[:xdigit:]:.]+$' || die "Incorrect IP range end '$IP_RANGE_END'"
echo "$MAC_MAP" | grep -E -q '^[[:alnum:]./-\_]+$' || die "Incorrect IP-Mac mapping file path '$MAC_MAP'"

#load Mac-IP mapping file
MACS=(`cat "$MAC_MAP"`)

#create virtual network definition
NET_XML="/var/log/contrail/vin/$BR_NAME.xml"
echo "<network>                                                                   " >  $NET_XML
echo " <name>$BR_NAME</name>                                                      " >> $NET_XML
echo " <bridge name='$BR_NAME'/>                                                  " >> $NET_XML
echo " <forward mode='route'/>                                                    " >> $NET_XML
echo " <ip address='$BR_IP_ADDR' netmask='$NETMASK'>                              " >> $NET_XML
echo "  <dhcp>                                                                    " >> $NET_XML
echo "   <range start='$IP_RANGE_START' end='$IP_RANGE_END'/>                     " >> $NET_XML
for (( i=0; i<${#MACS[*]}; i+=3 ))
do
echo "   <host mac=\"${MACS[$i+1]}\" ip=\"${MACS[$i]}\" name=\"VM${MACS[$i+2]}\"/>" >> $NET_XML
done
echo "  </dhcp>                                                                   " >> $NET_XML
echo " </ip>                                                                      " >> $NET_XML
echo "</network>                                                                  " >> $NET_XML

#start libvirt network
virsh net-create "$NET_XML"
rm "$NET_XML"

#HACK: remove generic in/out iptables FORWARD rules created by libvirt
iptables -D FORWARD -d "$BR_IP_ADDR/$NETMASK" -o "$BR_NAME" -j ACCEPT
iptables -D FORWARD -s "$BR_IP_ADDR/$NETMASK" -i "$BR_NAME" -j ACCEPT

#enable proxy arp
sysctl net.ipv4.conf."$BR_NAME".proxy_arp=1
