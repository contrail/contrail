#!/bin/sh

set -u -e -x
die() {
    echo>& 2 "$@"
    exit 1
}


[ "$#" -eq 2 ] || die "2 parameters required, but $# provided: [$@]"
VMADDRESS="$1"
VINADDRESS="$2"


#echo "${VMADDRESS}" | grep -E -q '^[[:xdigit:]:.]+$' || die "Failed assertion"
echo "${VINADDRESS}" | grep -E -q '^[[:xdigit:]:.]+$' || die "Failed assertion"

echo Addresses on VM:
ip addr list

echo Routes on VM:
ip route list

echo Route to ${VINADDRESS} from this VM:
ip route get ${VINADDRESS}

echo Pinging ${VINADDRESS}
ping -c 1 ${VINADDRESS}

