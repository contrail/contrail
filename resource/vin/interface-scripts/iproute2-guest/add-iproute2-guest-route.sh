#!/bin/sh

set -u -e -x
die() {
    echo>& 2 "$@"
    exit 1
}

[ "$#" -eq 3 ] || die "3 parameters required, but $# provided: [$@]"
TUNNEL="$1"
LOCAL_VIN="$2"
REMOTE_VIN="$3"

echo "${TUNNEL}" | grep -E -q '^[[:alnum:]_:-]+$' || die "Failed assertion"
echo "${LOCAL_VIN}" | grep -E -q '^[[:xdigit:]:.]+$' || die "Failed assertion"
echo "${REMOTE_VIN}" | grep -E -q '^[[:xdigit:]:.]+$' || die "Failed assertion"
echo "TUNNEL=${TUNNEL} LOCAL_VIN="${LOCAL_VIN}" REMOTE_VIN=${REMOTE_VIN}"
/bin/ip addr add "${LOCAL_VIN}"/32 dev ${TUNNEL} scope link
echo Added address "${LOCAL_VIN}" to tunnel ${TUNNEL}:
/bin/ip addr show
/bin/ip route add ${REMOTE_VIN} dev ${TUNNEL} src ${LOCAL_VIN}
echo Added route from ${LOCAL_VIN} to ${REMOTE_VIN} via tunnel ${TUNNEL}:
/bin/ip route show
