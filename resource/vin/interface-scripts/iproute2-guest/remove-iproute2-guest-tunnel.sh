#!/bin/sh

set -u -e
die() {
    echo>& 2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "1 parameter required, but $# provided: [$@]"
TUNNEL="$1"

echo "${TUNNEL}" | grep -E -q '^[[:alnum:]_-]+$' || die "Failed assertion"
/bin/ip tunnel del name "${TUNNEL}"
echo Removed tunnel "'${TUNNEL}':"
/bin/ip tunnel show
