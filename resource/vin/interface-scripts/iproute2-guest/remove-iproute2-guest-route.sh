#!/bin/sh

set -u -e
die() {
    echo>& 2 "$@"
    exit 1
}

[ "$#" -eq 3 ] || die "3 parameters required, but $# provided: [$@]"
TUNNEL="$1"
LOCAL_VIN="$2"
REMOTE_VIN="$3"

echo "${REMOTE_VIN}" | grep -E -q '^[[:xdigit:]:.]+$' || die "Failed assertion"
echo "${TUNNEL}" | grep -E -q '^[[:alnum:]_-]+$' || die "Failed assertion"
/bin/ip route del "${REMOTE_VIN}" dev "${TUNNEL}"
/bin/ip addr del "${LOCAL_VIN}"/32 dev ${TUNNEL}
echo Removed route:
/bin/ip route show
echo Removed address:
/bin/ip addr show
