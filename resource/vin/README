Introduction
------------

This directory contain the Virtual Interface Network (VIN) component
of Contrail. It offers a web service that allows virtual networks
between Contrail VMs to be created and destroyed on demand.

This README first describes some security considerations that are
important for the installation procedure.  The VIN depends on a number
of packages that are listed below.  Finally, the installation procedure
itself is described.


Security considerations
-----------------------

To be able to function, the VIN needs to modify the network
configuration of a cloud host. In particular, it it needs to create
IPSec or other tunnels between cloud hosts.  Since Linux distributions
normally do not allow a normal user to do this, the VIN requires
some extra privileges.

However, giving these extra privileges to the entire VIN infrastructure
would be problematic, since we cannot expect system administrators
to check it all for security issues.  Instead, the VIN isolates
the sensitive operations in small configuration commands that
are executed by a special configuration tool called 'safeconfig'.
Safeconfig is also part of the Contrail distribution.


Thus, rather than the entire VIN code, a system administrator only
has to inspect these configuration files.

The interface scripts are provided in the directory interface-scripts.
You are invited to thoroughly inspect them to convince yourself
that they are not security risks.


Requirements
------------

The VIN requires the following packages to be installed on both the central
controller and on the OpenNebula nodes:

- The safeconfig command.

- A Java compiler
- The 'ant' command.

At least one of the following:
- strongSwan and the v2 IKE
- the iproute2 command (if you have it, things like 'ip route list' work)


Installation
------------

The installation procedure is subject to change to make it adhere to
the Contrail standards in this, but for now you can but for now you can
simply do:

ant vin-support.tar.gz

This will create a tar file with all necessary files, and if you untar
it as root in the root directory, the files will be put in their correct
place:

cd /
sudo tar xf vin-support.tar.gz

You are of course welcome to inspect the tar file before you actually do
the un-tarring.

Once untarred, you need to do:

sudo chmod +x /usr/share/contrail/support/vin/interface-scripts/*
sudo chmod u+s /usr/share/contrail/support/vin/interface-scripts/*

to make the interface scripts executable and set-UID root.


System configuration
--------------------

- On the agent machines, make sure that IP forwarding is enabled by
adding or uncommenting in the file /etc/sysctl.conf the line:

net.ipv4.ip_forward=1


It is also necessary to point strongSwan to the VIN configuration files. To
do this, add the following line to /etc/ipsec.conf (the actual directory
may differ, depending on decisions made by the Contrail packaging team):

include /var/lib/contrail/vin/strongSwan/*.conf


and force a restart of the strongSwan server with:

sudo ipsec restart


Agent laucher
-------------

The VIN software requires that agents are lauched on every machine that hosts a
VM of an application. These agents require parameters, in particular an identifier
of the agent. Lauching an agent therefore needs some administration.

Part of the VIN software is a set of processes that run on the head node
and the nodes that host the VM. These processes launch the agents of an
application with the necessary parameters.





Usage
-----

The central VIN controller is accessed using a Java API. Read the VIN
documentation in the D10.2 document for details.

The VIN agents are separate processes, typically started from the command line.
Again the D10.2 document has the details.

