/**
 * Test address ranges.
 */
package org.ow2.contrail.resource.vin.controller.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;

import junit.framework.Assert;

import org.junit.Test;
import org.ow2.contrail.resource.vin.common.VinError;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestAddressRanges {
    private static void testInRange(AddressRanges ars, String address,
            boolean correctVerdict) throws UnknownHostException {
        final InetAddress a = InetAddress.getByName(address);
        final boolean verdict = ars.contains(a);
        Assert.assertEquals(correctVerdict, verdict);
    }

    /**
     * Test alignment computation
     * 
     * @throws VinError
     *             Thrown on a parsing error.
     * @throws UnknownHostException
     *             Thrown if the address is malformed.
     */
    @Test()
    public void addressRangesTest() throws VinError, UnknownHostException {
        final AddressRanges ars = AddressRanges.parseRangesString("10.0.0.0/8");
        testInRange(ars, "10.1.1.5", true);
        testInRange(ars, "10.255.254.250", true);
        testInRange(ars, "11.1.1.5", false);
    }
}
