/**
 * Test ByteArrayPool functions.
 */
package org.ow2.contrail.resource.vin.common;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestArrayValue {

    /**
     * Test offsetof computation
     * 
     */
    @Test()
    public void byteArrayOffsetTest() {
        final ArrayValue v1 = new ArrayValue(0x00, 0x3E, 0x16, 0x02);
        final ArrayValue v2 = new ArrayValue(0x00, 0x3E, 0x16, 0x07);
        final ArrayValue v3 = new ArrayValue(0x00, 0x3E, 0x19, 0x02);
        final ArrayValue v4 = new ArrayValue(0x00, 0x3E, 0x19, 0x07);
        final ArrayValue mask1 = new ArrayValue(0x00, 0x00, 0x00, 0x01);
        final ArrayValue mask2 = new ArrayValue(0x00, 0x00, 0x00, 0x03);
        final ArrayValue mask3 = new ArrayValue(0x00, 0x00, 0x00, 0x07);
        final ArrayValue mask4 = new ArrayValue(0x00, 0x00, 0x00, 0x0f);
        final ArrayValue mask5 = new ArrayValue(0x00, 0x00, 0x00, 0x1f);
        final ArrayValue mask6 = new ArrayValue(0x00, 0x00, 0x00, 0x3f);
        final ArrayValue mask7 = new ArrayValue(0x00, 0x00, 0x00, 0x7f);
        final ArrayValue mask8 = new ArrayValue(0x00, 0x00, 0x00, 0xff);
        final ArrayValue mask9 = new ArrayValue(0x00, 0x00, 0x01, 0xff);
        final ArrayValue mask10 = new ArrayValue(0x00, 0x00, 0x03, 0xff);

        testOffsetOf(v2, v1, 5);
        testOffsetOf(v3, v1, 3 * 256);
        testOffsetOf(v4, v1, 3 * 256 + 5);
        testAddOffset(v1, 0, "00:3e:16:02");
        testAddOffset(v1, 2, "00:3e:16:04");
        testAddOffset(v1, 5, "00:3e:16:07");
        testAddOffset(v1, 256, "00:3e:17:02");
        testAddOffset(v1, 256 * 256, "00:3f:16:02");
        testAddOffset(v1, 256 * 256 * 256, "01:3e:16:02");
        testMaskCalculation(1, 4, mask1);
        testMaskCalculation(2, 4, mask2);
        testMaskCalculation(3, 4, mask3);
        testMaskCalculation(4, 4, mask4);
        testMaskCalculation(5, 4, mask5);
        testMaskCalculation(6, 4, mask6);
        testMaskCalculation(7, 4, mask7);
        testMaskCalculation(8, 4, mask8);
        testMaskCalculation(9, 4, mask9);
        testMaskCalculation(10, 4, mask10);
    }

    private void testAddOffset(final ArrayValue v, final int n, final String ref) {
        final ArrayValue res = v.addOffset(n);
        final String s = res.buildBuildMACAddressString();
        Assert.assertEquals(ref, s);
    }

    private void testOffsetOf(final ArrayValue val, final ArrayValue base,
            final long offset) {
        final long testOffset = base.offsetOf(val);
        Assert.assertEquals(offset, testOffset);
    }

    private void oneTestConstructor(final int[] l) {
        final ArrayValue v = new ArrayValue(l);
        for (int i = 0; i < l.length; i++) {
            Assert.assertEquals(l[i], v.get(i));
        }
    }

    /**
     * Test vararg constructor.
     * 
     */
    @Test()
    public void testConstructor() {
        oneTestConstructor(new int[] { 1 });
        oneTestConstructor(new int[] { 1, 3 });
        oneTestConstructor(new int[] { 1, 3, 5 });
        oneTestConstructor(new int[] { 1, 3, 5, 7 });
    }

    private void testMaskCalculation(final int alignmentBits,
            final int maskSize, final ArrayValue reference) {
        final ArrayValue mask = ArrayValue.buildAlignmentMask(alignmentBits,
                maskSize);
        Assert.assertTrue(mask.equals(reference));
    }
}
