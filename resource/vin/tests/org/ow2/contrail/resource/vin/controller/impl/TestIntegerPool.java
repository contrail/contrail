/**
 * Test the integer pool manager.
 */
package org.ow2.contrail.resource.vin.controller.impl;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestIntegerPool {

    private static void assertIsInRange(final int val, final int min,
            final int i) {
        Assert.assertTrue(min <= i);
        Assert.assertTrue(val >= min);
        Assert.assertTrue(val <= i);
    }

    private static void assertIsNotAPreviousResult(final int[] results,
            final int resultCount, final int newResult) {
        for (int j = 0; j < resultCount; j++) {
            final int prev = results[j];
            Assert.assertTrue(newResult != prev);
        }
    }

    /**
     * Test handling of pool exhaustion.
     * 
     * @throws OutOfEntriesError
     *             Thrown if the pool is exhausted (the expected outcome)
     */
    @Test(expected = OutOfEntriesError.class)
    public void poolExhaustionTest() throws OutOfEntriesError {
        final int n = 10;
        final int startValue = 0;
        final int endValue = n - 2;
        final IntegerPool pool = new IntegerPool("Test pool", startValue,
                endValue);
        for (int i = 0; i < n; i++) {
            final int res = pool.getNextValue();
            assertIsInRange(res, 0, n);
        }
    }

    /**
     * Run a pool with one-int entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simplestRecycleTest() throws OutOfEntriesError {
        final int startValue = 0;
        final int endValue = 100;
        final IntegerPool pool = new IntegerPool("Test pool", startValue,
                endValue);
        final int r1 = pool.getNextValue();
        pool.recycleValue(r1);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-int entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simpleRecycleTest4() throws OutOfEntriesError {
        final int startValue = 10;
        final int endValue = 100;
        final IntegerPool pool = new IntegerPool("Test pool", startValue,
                endValue);
        final int r1 = pool.getNextValue();
        final int r2 = pool.getNextValue();
        final int r3 = pool.getNextValue();
        final int r4 = pool.getNextValue();
        pool.recycleValue(r1);
        pool.recycleValue(r2);
        pool.recycleValue(r3);
        pool.recycleValue(r4);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-int entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simpleRecycleTest3() throws OutOfEntriesError {
        final int startValue = 10;
        final int endValue = 100;
        final IntegerPool pool = new IntegerPool("Test pool", startValue,
                endValue);
        final int r1 = pool.getNextValue();
        final int r2 = pool.getNextValue();
        final int r3 = pool.getNextValue();
        pool.recycleValue(r1);
        pool.recycleValue(r2);
        pool.recycleValue(r3);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool: draw values from them, return them in non-trivial order, and
     * assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simpleRecycleTest1() throws OutOfEntriesError {
        final int startValue = 8;
        final int endValue = 42;
        final IntegerPool pool = new IntegerPool("Test pool", startValue,
                endValue);
        final int r1 = pool.getNextValue();
        final int r2 = pool.getNextValue();
        pool.recycleValue(r1);
        pool.recycleValue(r2);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-int entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simpleRecycleTest2() throws OutOfEntriesError {
        final int startValue = 12;
        final int endValue = 25;
        final IntegerPool pool = new IntegerPool("Test pool", startValue,
                endValue);
        final int r1 = pool.getNextValue();
        final int r2 = pool.getNextValue();
        pool.recycleValue(r2);
        pool.recycleValue(r1);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-int entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void recycleTest() throws OutOfEntriesError {
        final int n = 20;
        final int startValue = 0;
        final int endValue = 255;
        final IntegerPool pool = new IntegerPool("Test pool", startValue,
                endValue);
        final int results[] = new int[n];
        for (int i = 0; i < n; i++) {
            final int res = pool.getNextValue();
            assertIsInRange(res, 0, n);
            assertIsNotAPreviousResult(results, i, res);
            results[i] = res;
        }
        final int r1 = pool.getNextValue();
        assertIsNotAPreviousResult(results, n, r1);
        pool.recycleValue(r1);
        final int r2 = pool.getNextValue();
        assertIsNotAPreviousResult(results, n, r2);
        pool.recycleValue(r2);

        int h = 7;
        int l = 4;
        while (h > l) {
            final int tmp = results[h];
            results[h] = results[l];
            results[l] = tmp;
            h--;
            l++;
        }
        for (int i = 0; i < n; i++) {
            pool.recycleValue(results[i]);
        }
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Smallest test: just get a few values out of the pool. No value return, no
     * overdemand.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void minimalTest() throws OutOfEntriesError {
        final int n = 5;
        final int startValue = 0;
        final int endValue = n + 3;
        final IntegerPool pool = new IntegerPool("Test pool", startValue,
                endValue);
        final int results[] = new int[n];
        for (int i = 0; i < n; i++) {
            final int res = pool.getNextValue();
            assertIsInRange(res, 0, n);
            assertIsNotAPreviousResult(results, i, res);
            results[i] = res;
        }
    }
}
