/**
 * Test the InetAddress pool manager.
 */
package org.ow2.contrail.resource.vin.common;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.contrail.resource.vin.common.ArrayValue;
import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.ByteArrayPool;
import org.ow2.contrail.resource.vin.common.IPv4Subnet;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestIPv4AddressPool {

    /**
     * Given an array of integers, make sure that it represents a permutation.
     * That is easily done by making sure that all values are non-negative,
     * smaller than the length of the array, and that there are no duplicates.
     * 
     * @param perm
     *            The permutation to test.
     */
    private static void assertIsAPermutation(final int perm[]) {
        final int sz = perm.length;
        final boolean seen[] = new boolean[sz];

        for (final int i : perm) {
            Assert.assertTrue(i >= 0);
            Assert.assertTrue(i < sz); // value should be in range.
            Assert.assertFalse(seen[i]); // no duplicates.
            seen[i] = true;
        }
    }

    private static void assertIsInRange(final ArrayValue v, final int ix,
            final int min, final int i) {
        final int val = v.get(ix);
        Assert.assertTrue(min <= i);
        Assert.assertTrue(val >= min);
        Assert.assertTrue(val <= i);
    }

    private static void assertIsNotAPreviousResult(final ArrayValue[] results,
            final int resultCount, final ArrayValue newResult) {
        for (int j = 0; j < resultCount; j++) {
            final ArrayValue prev = results[j];
            Assert.assertFalse(prev.equals(newResult));
        }
    }

    /**
     * Test handling of pool exhaustion.
     * 
     * @throws OutOfEntriesError
     *             Thrown if the pool is exhausted (the expected outcome)
     */
    @Test(expected = OutOfEntriesError.class)
    public void poolExhaustionTest() throws OutOfEntriesError {
        final int n = 10;
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(n - 2);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        for (int i = 0; i < n; i++) {
            final ArrayValue res = pool.getNextValue();
            Assert.assertEquals(1, res.size());
            assertIsInRange(res, 0, 0, n);
        }
    }

    /**
     * Test multibyte pool. No value return, no overdemand, two-byte values.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void multibyteTest() throws OutOfEntriesError {
        final int n = 259;
        final ArrayValue startBytes = new ArrayValue(0, 0);
        final ArrayValue endBytes = new ArrayValue(0xFF, 0xFF);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue results[] = new ArrayValue[n];
        for (int i = 0; i < n; i++) {
            final ArrayValue res = pool.getNextValue();
            Assert.assertEquals(2, res.size());
            assertIsInRange(res, 0, 0, n);
            assertIsNotAPreviousResult(results, i, res);
            results[i] = res;
        }
    }

    /**
     * Run a pool with one-byte entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simplestRecycleTest() throws OutOfEntriesError {
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(0xFF);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue r1 = pool.getNextValue();
        pool.recycleValue(r1);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-byte entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simpleRecycleTest4() throws OutOfEntriesError {
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(0xFF);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue r1 = pool.getNextValue();
        final ArrayValue r2 = pool.getNextValue();
        final ArrayValue r3 = pool.getNextValue();
        final ArrayValue r4 = pool.getNextValue();
        pool.recycleValue(r1);
        pool.recycleValue(r2);
        pool.recycleValue(r3);
        pool.recycleValue(r4);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-byte entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simpleRecycleTest3() throws OutOfEntriesError {
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(0xFF);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue r1 = pool.getNextValue();
        final ArrayValue r2 = pool.getNextValue();
        final ArrayValue r3 = pool.getNextValue();
        pool.recycleValue(r1);
        pool.recycleValue(r2);
        pool.recycleValue(r3);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-byte entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simpleRecycleTest1() throws OutOfEntriesError {
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(0xFF);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue r1 = pool.getNextValue();
        final ArrayValue r2 = pool.getNextValue();
        pool.recycleValue(r1);
        pool.recycleValue(r2);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-byte entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void simpleRecycleTest2() throws OutOfEntriesError {
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(0xFF);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue r1 = pool.getNextValue();
        final ArrayValue r2 = pool.getNextValue();
        pool.recycleValue(r2);
        pool.recycleValue(r1);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Run a pool with one-byte entries: draw values from them, return them in
     * non-trivial order, and assert that the pool is in pristine state again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void recycleTest() throws OutOfEntriesError {
        final int n = 20;
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(0xFF);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue results[] = new ArrayValue[n];
        for (int i = 0; i < n; i++) {
            final ArrayValue res = pool.getNextValue();
            Assert.assertEquals(1, res.size());
            assertIsInRange(res, 0, 0, n);
            assertIsNotAPreviousResult(results, i, res);
            results[i] = res;
            Assert.assertTrue(pool.isInCanonicalState());
        }
        final ArrayValue r1 = pool.getNextValue();
        assertIsNotAPreviousResult(results, n, r1);
        Assert.assertTrue(pool.isInCanonicalState());
        pool.recycleValue(r1);
        final ArrayValue r2 = pool.getNextValue();
        assertIsNotAPreviousResult(results, n, r2);
        Assert.assertTrue(pool.isInCanonicalState());
        pool.recycleValue(r2);

        {
            /*
             * Swap a range of entries in the array.
             */
            int h = 7;
            int l = 4;
            while (h > l) {
                final ArrayValue tmp = results[h];
                results[h] = results[l];
                results[l] = tmp;
                h--;
                l++;
            }
        }
        for (int i = 0; i < n; i++) {
            Assert.assertTrue(pool.isInCanonicalState());
            final ArrayValue val = results[i];
            pool.recycleValue(val);
        }
        Assert.assertTrue(pool.isInPristineState());
    }

    private static void testPoolWithAllPermutations(final int n)
            throws OutOfEntriesError {
        final int a[] = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i;
        }
        testPoolWithAllPermutations(a, 0);
    }

    private static void testPoolWithAllPermutations(final int[] a, final int i)
            throws OutOfEntriesError {
        final int sz = a.length;
        if (i + 1 == sz) {
            testPoolWithPermutation(a);
        } else {
            for (int p = i; p < sz; p++) {
                swap(a, i, p);
                testPoolWithAllPermutations(a, i + 1);
                swap(a, i, p);
            }
        }
    }

    private static void swap(final int[] a, final int i1, final int i2) {
        final int tmp = a[i1];
        a[i1] = a[i2];
        a[i2] = tmp;
    }

    /**
     * Run a pool with one-byte entries: draw values from them, return them all
     * possible permutations, and assert that the pool is in pristine state
     * again.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void permutationsTest() throws OutOfEntriesError {
        testPoolWithPermutation(new int[] { 0, 1, 2, 3, 4, 5, 6 });
        testPoolWithPermutation(new int[] { 6, 5, 4, 3, 2, 1, 0 });
        testPoolWithPermutation(new int[] { 0, 2, 4, 6, 1, 3, 5 });
        testPoolWithPermutation(new int[] { 0, 4, 2, 6, 1, 3, 5 });
        testPoolWithPermutation(new int[] { 5, 3, 1, 6, 2, 4, 0 });
        testPoolWithPermutation(new int[] { 5, 1, 3, 6, 4, 2, 0 });
        testPoolWithPermutation(new int[] { 5, 1, 3, 6, 4, 2, 0, 7, 8, 9 });
        testPoolWithAllPermutations(7);
    }

    private static void testPoolWithPermutation(final int perm[])
            throws OutOfEntriesError {
        assertIsAPermutation(perm);
        final int n = perm.length;
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(0xFF);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue results[] = new ArrayValue[n];
        for (int i = 0; i < n; i++) {
            final ArrayValue res = pool.getNextValue();
            Assert.assertEquals(1, res.size());
            assertIsInRange(res, 0, 0, n);
            assertIsNotAPreviousResult(results, i, res);
            results[i] = res;
            Assert.assertTrue(pool.isInCanonicalState());
        }
        /* Now return the values in the order that the permutation specifies. */
        for (int i = 0; i < n; i++) {
            final int ix = perm[i];
            Assert.assertTrue(pool.isInCanonicalState());
            final ArrayValue val = results[ix];
            pool.recycleValue(val);
        }
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Smallest test: just get a few values out of the pool. No value return, no
     * overdemand, only one byte.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void minimalTest() throws OutOfEntriesError {
        final int n = 5;
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(n + 3);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue results[] = new ArrayValue[n];
        for (int i = 0; i < n; i++) {
            final ArrayValue res = pool.getNextValue();
            Assert.assertEquals(1, res.size());
            assertIsInRange(res, 0, 0, n);
            assertIsNotAPreviousResult(results, i, res);
            results[i] = res;
        }
    }

    /**
     * Smallest range test: just get a few values out of the pool. No value
     * return, no overdemand, only one byte.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void minimalRangeTest() throws OutOfEntriesError {
        final int n = 23;
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(n);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue results[] = new ArrayValue[n];
        for (int i = 0; i < 10; i++) {
            final ArrayValueRange resRange = pool.getNextSubrange(1L, 0);
            final ArrayValue res = resRange.startValue;
            Assert.assertEquals(1, res.size());
            assertIsInRange(res, 0, 0, n);
            Assert.assertTrue(pool.isInCanonicalState());
            assertIsNotAPreviousResult(results, i, res);
            results[i] = res;
        }
    }

    /**
     * Smallest range test: just get a few values out of the pool. No value
     * return, no overdemand, only one byte.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void recyclingRangeTest() throws OutOfEntriesError {
        final int n = 23;
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(n);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        for (int i = 0; i < 10; i++) {
            final int bits = 2;
            final int r = i + 3;
            final ArrayValueRange resRange = pool.getNextSubrange(r, bits);
            final ArrayValue start = resRange.startValue;
            Assert.assertEquals(1, start.size());
            assertIsInRange(start, 0, 0, n);
            final ArrayValue end = resRange.endValue;
            Assert.assertEquals(1, end.size());
            assertIsInRange(end, 0, 0, n);
            final long span = start.offsetOf(end);
            Assert.assertEquals(ByteArrayPool.computeNextAlignedValue(r, bits),
                    span + 1);
            Assert.assertTrue(pool.isInCanonicalState());
            pool.addToRecyclebin(resRange);
            Assert.assertTrue(pool.isInPristineState());
        }
    }

    /**
     * Smallest range test: just get a few values out of the pool. No value
     * return, no overdemand, only one byte.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void recyclingUnalignedRangeTest() throws OutOfEntriesError {
        final int n = 90;
        final ArrayValue startBytes = new ArrayValue(0);
        final ArrayValue endBytes = new ArrayValue(n);
        final ByteArrayPool pool = new ByteArrayPool(startBytes, endBytes,
                "test Pool");
        final ArrayValue single = pool.getNextValue();
        for (int i = 0; i < 10; i++) {
            final int bits = 2;
            final ArrayValueRange resRange = pool.getNextSubrange(i + 3, bits);
            Assert.assertTrue(pool.isInCanonicalState());
            final ArrayValue res = resRange.startValue;
            Assert.assertEquals(res.size(), startBytes.size());
            assertIsInRange(res, 0, 0, n);
            pool.addToRecyclebin(resRange);
            Assert.assertTrue(pool.isInCanonicalState());
        }
        pool.recycleValue(single);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Range test: just get a few ranges out of the pool.
     * 
     * @throws OutOfEntriesError
     *             Should not throw any exceptions.
     */
    @Test
    public void recyclingMultiByteUnalignedRangeTest() throws OutOfEntriesError {
        final ArrayValue startBytes = new ArrayValue(10, 0, 0, 0);
        final ArrayValue endBytes = new ArrayValue(10, 0, 2, 255);
        final IPv4AddressRangePool pool = new IPv4AddressRangePool(startBytes,
                endBytes, "test Pool");
        final IPv4Subnet subnet1 = pool.getNextSubnet(200);
        final IPv4Subnet subnet2 = pool.getNextSubnet(3);
        final IPv4Subnet subnet3 = pool.getNextSubnet(200);
        Assert.assertTrue(subnet1.overlaps(subnet1));
        Assert.assertTrue(subnet2.overlaps(subnet2));
        Assert.assertTrue(subnet3.overlaps(subnet3));
        Assert.assertFalse(subnet1.overlaps(subnet2));
        Assert.assertFalse(subnet1.overlaps(subnet3));
        Assert.assertFalse(subnet2.overlaps(subnet1));
        Assert.assertFalse(subnet2.overlaps(subnet3));
        Assert.assertFalse(subnet3.overlaps(subnet1));
        Assert.assertFalse(subnet3.overlaps(subnet2));
        pool.recycleSubnet(subnet1);
        pool.recycleSubnet(subnet3);
        pool.recycleSubnet(subnet2);
        Assert.assertTrue(pool.isInPristineState());
    }

    /**
     * Try to get more subnets out of the pool than there are available
     * 
     * @throws OutOfEntriesError
     *             Thrown because there is no room.
     */
    @Test(expected = OutOfEntriesError.class)
    public void exhaustPoolTest() throws OutOfEntriesError {
        final ArrayValue startBytes = new ArrayValue(10, 0, 0, 0);
        final ArrayValue endBytes = new ArrayValue(10, 0, 1, 255);
        final IPv4AddressRangePool pool = new IPv4AddressRangePool(startBytes,
                endBytes, "test Pool");
        pool.getNextSubnet(200);
        pool.getNextSubnet(3);
        pool.getNextSubnet(200);
    }
}
