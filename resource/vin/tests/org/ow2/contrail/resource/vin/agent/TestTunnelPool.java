/**
 * Test tunnel pool functions.
 */
package org.ow2.contrail.resource.vin.agent;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import junit.framework.Assert;

import org.junit.Test;
import org.ow2.contrail.resource.vin.agent.TunnelPool.TunnelPoolEntry;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestTunnelPool {
    private static int n = 0;

    private static AgentConnectionDescriptor buildConnection(
            final AgentNetworkDescriptor network, final String vmId,
            final String localAddressString, final String remoteAddressString)
            throws UnknownHostException {
        final int ord = n++;
        final InetAddress localAddress = InetAddress
                .getByName(localAddressString);
        final InetAddress remoteAddress = InetAddress
                .getByName(remoteAddressString);
        return new AgentConnectionDescriptor(network, n++, "id" + ord, vmId,
                localAddress, remoteAddress, true, new Properties());
    }

    /**
     * Test IPv4 byte array to string conversion.
     * 
     * @throws UnknownHostException
     *             Thrown iff one of the dummy IP addresses we specify cannot be
     *             translated.
     * 
     */
    @Test()
    public void mappingTest() throws UnknownHostException {
        final TunnelPool pool = new TunnelPool("testpool");

        Assert.assertEquals(pool.isEmpty(), true);
        final AgentNetworkDescriptor network = null;
        final String localAddressString = "192.168.1.1";
        final String remoteAddressString1 = "192.168.1.2";
        final String remoteAddressString2 = "192.168.1.3";
        final AgentConnectionDescriptor c1 = buildConnection(network, "vm1",
                localAddressString, remoteAddressString1);
        final AgentConnectionDescriptor c2 = buildConnection(network, "vm2",
                localAddressString, remoteAddressString2);
        final AgentConnectionDescriptor c3 = buildConnection(network, "vm3",
                localAddressString, remoteAddressString1);
        final TunnelPoolEntry e1 = pool.addConnection(c1);
        Assert.assertEquals(false, e1.isEmpty());
        Assert.assertEquals(true, e1.hasOneEntry());
        final TunnelPoolEntry e2 = pool.addConnection(c2);
        Assert.assertEquals(false, e2.isEmpty());
        Assert.assertEquals(true, e2.hasOneEntry());
        final TunnelPoolEntry e3 = pool.addConnection(c3);
        Assert.assertEquals(false, e3.isEmpty());
        Assert.assertEquals(false, e3.hasOneEntry());
        final TunnelPoolEntry e4 = pool.removeConnection(c2);
        Assert.assertEquals(true, e4.isEmpty());
        Assert.assertEquals(false, e4.hasOneEntry());
        final TunnelPoolEntry e5 = pool.removeConnection(c1);
        Assert.assertEquals(false, e5.isEmpty());
        Assert.assertEquals(true, e5.hasOneEntry());
        final TunnelPoolEntry e6 = pool.removeConnection(c3);
        Assert.assertEquals(true, e6.isEmpty());
        Assert.assertEquals(false, e6.hasOneEntry());
        Assert.assertEquals(pool.isEmpty(), true);
    }
}
