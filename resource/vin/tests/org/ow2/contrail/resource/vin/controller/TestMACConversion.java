/**
 * Test MAC address conversion function.
 */
package org.ow2.contrail.resource.vin.controller;

import junit.framework.Assert;

import org.junit.Test;
import org.ow2.contrail.resource.vin.common.ArrayValue;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestMACConversion {
    private static void testMacConversion(final ArrayValue mac, final String ref) {
        final String res = mac.buildBuildMACAddressString();
        Assert.assertEquals(ref, res);
    }

    private static void testIP4Conversion(final ArrayValue addr,
            final String ref) {
        final String res = addr.makeIP4AddressString();
        Assert.assertEquals(ref, res);
    }

    private static void testIP6Conversion(final ArrayValue addr,
            final String ref) {
        final String res = addr.makeIP6AddressString();
        Assert.assertEquals(ref, res);
    }

    /**
     * Test MAC byte array to string conversion.
     * 
     */
    @Test()
    public void macConversionTest() {
        final ArrayValue mac1 = new ArrayValue(0x00, 0x16, 0x3E, 0x00, 0x00,
                0x00);
        final ArrayValue mac2 = new ArrayValue(0x00, 0x16, 0x3E, 0xFF, 0xFF,
                0xFF);

        testMacConversion(mac1, "00:16:3e:00:00:00");
        testMacConversion(mac2, "00:16:3e:ff:ff:ff");
    }

    /**
     * Test IPv4 byte array to string conversion.
     * 
     */
    @Test()
    public void ip4ConversionTest() {
        final ArrayValue addr1 = new ArrayValue(192, 168, 3, 7);
        final ArrayValue addr2 = new ArrayValue(10, 0, 23, 7);

        testIP4Conversion(addr1, "192.168.3.7");
        testIP4Conversion(addr2, "10.0.23.7");
    }

    /**
     * Test IPv6 byte array to string conversion.
     * 
     */
    @Test()
    public void ip6ConversionTest() {
        final ArrayValue addr1 = new ArrayValue(0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 1);
        final ArrayValue addr2 = new ArrayValue(255, 255, 255, 255, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0);

        testIP6Conversion(addr1, "0:0:0:0:0:0:0:1");
        testIP6Conversion(addr2, "ffff:ffff:0:0:0:0:0:0");
    }
}
