/**
 * Test IPv6Subnet functions.
 */
package org.ow2.contrail.resource.vin.common;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestIPv6Subnet {
    private static void testNetmask(final int bits, final String ref) {
        final String s = IPv6Subnet.getNetmaskString(bits);
        Assert.assertEquals(ref, s);
    }

    /**
     * Test IPv6 byte array to string conversion.
     * 
     */
    @Test()
    public void netmaskTest() {
        testNetmask(24, "255.255.255.0");
        testNetmask(28, "255.255.255.240");
        testNetmask(8, "255.0.0.0");
    }
}
