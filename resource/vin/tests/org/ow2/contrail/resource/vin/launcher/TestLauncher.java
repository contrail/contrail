/**
 * Test host context functions.
 */
package org.ow2.contrail.resource.vin.launcher;

import java.io.File;
import java.util.Properties;

import junit.framework.Assert;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.Test;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestLauncher {
    private static final String COORDINATOR_HOSTID = "coordinator";
    private static final String LAUNCHER1_HOSTID = "launcher1";
    private static final String LAUNCHER2_HOSTID = "launcher2";
    private static final int RPC_PORTNUMBER = 22785;
    private static final int ibisPortNumber = 22784;
    private static final File testLaunchCommand = new File(
            "unittest-files/launch-agent");

    static {
        final Logger rootLogger = Logger.getRootLogger();
        rootLogger.setLevel(Level.DEBUG);
        rootLogger.addAppender(new ConsoleAppender(new PatternLayout(
                "%-6r [%p] %c - %m%n")));
    }

    @Test()
    public void parametersTest() {
        final String pl[] = { "-verbose", "-hostId=" + COORDINATOR_HOSTID };
        final Parameters parms = new Parameters(pl);
        Assert.assertTrue(parms.verbose);
        Assert.assertTrue(parms.runLauncher);
        Assert.assertFalse(parms.runRPCServer);
        Assert.assertFalse(parms.runHub);
        Assert.assertEquals(parms.hostId, COORDINATOR_HOSTID);
    }

    private Properties buildProperties(final String hostid) {
        final Properties res = new Properties();
        res.setProperty("hostid", hostid);
        return res;
    }

    private static final String getLog4jFile() {
        final File f = new File("unittest-files/log4j.properties");
        if (!f.exists()) {
            System.err.println("File '" + f + "' does not exist");
        } else {
            System.out.println("Configuration files '" + f + "' exists");
        }
        return f.getAbsolutePath();
    }

    // @Test()
    public void launcherTest() throws Exception {
        try {
            System.setProperty("ibis.server.address", "localhost:"
                    + ibisPortNumber);
            System.setProperty("log4j.configuration", getLog4jFile());
            final LauncherEngine coord = new LauncherEngine(true, false,
                    ibisPortNumber, testLaunchCommand, COORDINATOR_HOSTID, true);
            coord.start();
            final LauncherEngine launcher1 = new LauncherEngine(false, false,
                    ibisPortNumber, testLaunchCommand, LAUNCHER1_HOSTID, true);
            launcher1.start();
            final LauncherEngine launcher2 = new LauncherEngine(false, false,
                    ibisPortNumber, testLaunchCommand, LAUNCHER2_HOSTID, true);
            launcher2.start();
            final RpcServer server = new RpcServer(coord, RPC_PORTNUMBER, true);
            server.start();
            final Properties props1 = buildProperties("hostid1");
            final String launchId1 = LaunchServerTalker.requestAgentLaunch(
                    RPC_PORTNUMBER, true, LAUNCHER1_HOSTID, props1);
            LaunchServerTalker.waitForLaunch(RPC_PORTNUMBER, launchId1);
            final Properties props2 = buildProperties("hostid2");
            final String launchId2 = LaunchServerTalker.requestAgentLaunch(
                    RPC_PORTNUMBER, true, LAUNCHER2_HOSTID, props2);
            LaunchServerTalker.waitForLaunch(RPC_PORTNUMBER, launchId2);
            server.interrupt();
            coord.setStopped();
            launcher1.setStopped();
            launcher2.setStopped();
        } catch (final Exception e) {
            System.err.println("Exception: " + e);
            throw e;
        }
    }
}
