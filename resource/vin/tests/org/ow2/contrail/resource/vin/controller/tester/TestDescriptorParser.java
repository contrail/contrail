/**
 * Test the Json parser and writer for VIN descriptors.
 */
package org.ow2.contrail.resource.vin.controller.tester;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.contrail.resource.vin.common.VinError;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestDescriptorParser {
    private static void runStringToStringTest(final String json)
            throws IOException, VinError {
        final VinDescriptor d = VinDescriptor.parseJsonString(json);
        final String newJson = d.buildJsonString();
        Assert.assertEquals(json, newJson);
    }

    private static void runTreeToTreeTest(final VinDescriptor d)
            throws IOException, VinError {
        final String newJson = d.buildJsonString();
        System.out.println("newJson='" + newJson + "'");
        final VinDescriptor newd = VinDescriptor.parseJsonString(newJson);
        Assert.assertEquals(d, newd);
    }

    /**
     * @throws IOException
     *             Thrown if there is a read or write error. (Should never
     *             happen since we're using strings rather than files.)
     * @throws VinError
     *             Thrown if for some reason the test causes an error. This
     *             should not happen, and is caught by the test framework.
     */
    @Test
    public void zeroNetworksTest() throws IOException, VinError {
        runStringToStringTest("{\"machines\":[],\"networks\":[]}");
    }

    /**
     * @throws IOException
     *             Thrown if there is a read or write error. (Should never
     *             happen since we're using strings rather than files.)
     * @throws VinError
     *             Thrown if for some reason the test causes an error. This
     *             should not happen, and is caught by the test framework.
     */
    @Test
    public void onePnTest() throws IOException, VinError {
        runStringToStringTest("{\"machines\":[],\"networks\":[{\"properties\":{\"name\":\"private network\"},\"machines\":[]}]}");
    }

    /**
     * @throws IOException
     *             Thrown if there is a read or write error. (Should never
     *             happen since we're using strings rather than files.)
     * @throws VinError
     *             Thrown if for some reason the test causes an error. This
     *             should not happen, and is caught by the test framework.
     */
    @Test
    public void minimalTreeTest() throws IOException, VinError {
        final VinDescriptor tree = new VinDescriptor(
                new ArrayList<MachineDescriptor>(), new ArrayList<Network>(),
                new Properties(), 2, "OpenNebula");
        runTreeToTreeTest(tree);
    }
}
