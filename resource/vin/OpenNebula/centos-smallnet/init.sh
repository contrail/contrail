#!/bin/bash

# -------------------------------------------------------------------------- #
# Copyright 2002-2011, OpenNebula Project Leads (OpenNebula.org)             #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
#--------------------------------------------------------------------------- #

if [ -f /mnt/context.sh ]
then
  . /mnt/context.sh
fi


echo $HOSTNAME > /etc/hostname
hostname $HOSTNAME
sed -i "/127.0.1.1/s/ubuntu/$HOSTNAME/" /etc/hosts

if [ -n "$IP_PUBLIC" ]; then
	ifconfig eth0 $IP_PUBLIC
fi
 
if [ -n "$NETMASK" ]; then
	ifconfig eth0 netmask $NETMASK
fi


if [ -f /mnt/$ROOT_PUBKEY ]; then
	mkdir -p /root/.ssh
	cat /mnt/$ROOT_PUBKEY >> /root/.ssh/authorized_keys
	#chmod -R 600 /root/.ssh/
	chmod 600 /root/.ssh/authorized_keys
	chmod 700 /root/.ssh
fi

if [ -n "$USERNAME" ]; then
	useradd -s /bin/bash -m $USERNAME
	if [ -f /mnt/$USER_PUBKEY ]; then
		mkdir -p /home/$USERNAME/.ssh/
		cat /mnt/$USER_PUBKEY >> /home/$USERNAME/.ssh/authorized_keys
		chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh
		chmod 600 /home/$USERNAME/.ssh/authorized_keys
	fi
fi

# added:
if [ -n "$GATEWAY" ]; then
        # Change gateway from default; note eth0 is already active
        ( echo Orig network:
          ifconfig -a
          netstat -r
          echo Now set GATEWAY to $GATEWAY
        ) >> /var/log/context.log 2>&1

        ifdown eth0
	# Update GATEWAY and NETMASK configs in ifcfg-eth0
        mv /etc/sysconfig/network-scripts/ifcfg-eth0 /etc/sysconfig/network-scripts/ifcfg-eth0.orig
        sed -e "s/ GATEWAY=.*/ GATEWAY=$GATEWAY/" -e "s/ NETMASK=.*/ NETMASK=$NETMASK/" < /etc/sysconfig/network-scripts/ifcfg-eth0.orig > /etc/sysconfig/network-scripts/ifcfg-eth0
        cp /etc/sysconfig/network-scripts/ifcfg-eth0 /etc/sysconfig/network-scripts/ifcfg-eth0.sav
        ifup eth0

        ( echo current network:
          ifconfig -a
          netstat -r
        ) >> /var/log/context.log 2>&1
fi

# added:
if [ -n "$DNS" ]; then
        echo "Setting DNS server to $DNS" >>/var/log/context.log
        echo "nameserver $DNS" >/etc/resolv.conf
fi

if [ -f /mnt/vin.jar ]; then
    CONTRAILDIR=/usr/share/contrail/vin
    CONTRAILWORKDIR=/var/lib/contrail/vin
    ONEDIR=$CONTRAILDIR/one
    INTERFACEDIR=$CONTRAILDIR/interface-scripts
    STRONGSWANDIR=$CONTRAILWORKDIR/strongSwan

    mkdir -p $CONTRAILDIR
    mkdir -p $CONTRAILWORKDIR
    mkdir -p $INTERFACEDIR
    mkdir -p $STRONGSWANDIR

    cp /mnt/vin.jar $CONTRAILDIR

    # strongSwan doesn't like an empty dir, so give it a dummy .conf
    touch $STRONGSWANDIR/dummy.conf

    # Install interface scripts for the various network modes
    cp /mnt/interface-scripts/strongswan/* $INTERFACEDIR
    cp /mnt/interface-scripts/open/* $INTERFACEDIR
    cp /mnt/interface-scripts/iproute2/* $INTERFACEDIR
    cp /mnt/interface-scripts/test/* $INTERFACEDIR
    cp /mnt/interface-scripts/host/* $INTERFACEDIR

    # Make the whole lot executable and SUID root
    chmod +x $INTERFACEDIR/*
    chmod u+s $INTERFACEDIR/*

    echo 1 > /proc/sys/net/ipv4/ip_forward
    echo 1 > /proc/sys/net/ipv6/ip_forward
    echo "Starting VIN agent: VIN_POOL_NAME=$VIN_POOL_NAME VIN_IBIS_SERVER=$VIN_IBIS_SERVER VIN_VM_ID=$VIN_VM_ID" >>/var/log/context.log
    # FIXME: should run as non-root
    /usr/bin/java -Dibis.pool.name=$VIN_POOL_NAME \
         -Dibis.server.address=$VIN_IBIS_SERVER \
        -jar $CONTRAILDIR/vin.jar \
        -scriptsDir=$INTERFACEDIR $VIN_VM_ID > /var/log/vin-agent.log 2>&1 &
fi
