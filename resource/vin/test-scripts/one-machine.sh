#!/bin/sh
set -e -u
# Check setting of VIN_HOME
if [ -z "$VIN_HOME" ];  then
    echo "please set VIN_HOME to the location of your Vin installation" 1>&2
    exit 1
fi

# Jar-files from library.
LIBCLASSPATH="logconfig"
add_to_libclasspath () {
    JARFILES=`cd "$1" && ls *.jar 2>/dev/null`
    for i in ${JARFILES} ; do
	if [ -z "$LIBCLASSPATH" ] ; then
	    LIBCLASSPATH="$1/$i"
	else
	    LIBCLASSPATH="$LIBCLASSPATH:$1/$i"
	fi
    done
}

# Add the jar files in the VideoPlayer lib dir to the classpath.
#add_to_libclasspath "${VIN_HOME}"/used-jars/ipl
#add_to_libclasspath "${VIN_HOME}"/used-jars
add_to_libclasspath "${VIN_HOME}"/jars

#    -Dsmartsockets.file="$VIN_HOME"/smartsockets.properties \
#    -server -XX:+HeapDumpOnOutOfMemoryError \
# And finally, run ...
exec java \
    -classpath bin:"${CLASSPATH-""}:$LIBCLASSPATH" \
    -enableassertions \
     org.ow2.contrail.resource.vin.controller.tester.CommandLine -p=demo/minimal.properties demo/das4.vin
