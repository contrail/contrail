#!/bin/bash
#
# Usage:
#   test_host <guest-ip-address> <username> <remote-ip-address>
#
# Where <guest-ip-address> is an IP address of a guest VM,
# <username> is a username to be used when executing commands
# in the guest VM and <remote-ip-address> is an address of
# a machine to be tested for connectivity.

die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 3 ] || die "3 arguments required, $# provided: [$@]"

VM_IP_ADDR=$1
USERNAME=$2
REMOTE_IP_ADDR=$3

#parameters sanity check
echo "$VM_IP_ADDR" | grep -E -q '^[[:xdigit:]:.]+$' || die "Incorrect VM IP address '$VM_IP_ADDR'"
echo "$USERNAME" | grep -E -q '^[[:alnum:].-\_]+$' || die "Incorrect username '$USERNAME'"
echo "$REMOTE_IP_ADDR" | grep -E -q '^[[:xdigit:]:.]+$' || die "Incorrect remote IP address '$REMOTE_IP_ADDR'"

#copy test script to VM
scp ./test_guest.sh "$USERNAME@$VM_IP_ADDR:/tmp/test_guest$$.sh" >/dev/null
if [ $? -eq 1 ]
then
    echo "Could not copy test script to '$VM_IP_ADDR'"
    exit 1
fi

#execute tests in a guest VM
ssh "$USERNAME@$VM_IP_ADDR" "/tmp/test_guest$$.sh '$REMOTE_IP_ADDR'"
if ! [ $? -eq 0 ]
then
    echo "Tests failed in '$VM_IP_ADDR'"
    exit 1
fi

