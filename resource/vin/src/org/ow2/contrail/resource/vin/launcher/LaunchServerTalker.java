package org.ow2.contrail.resource.vin.launcher;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.Arrays;
import java.util.Properties;

import org.ow2.contrail.resource.vin.annotations.API;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.jsonrpc4j.JsonRpcClient;

/**
 * This class handles connection requests the launcher daemon.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class LaunchServerTalker {
    private static final Logger logger = LoggerFactory
            .getLogger(LaunchServerTalker.class);

    private static Object doRpc(final int serverPortNumber,
            final Type returnType, final String command, final Object... args)
            throws IOException {
        final Socket sock;

        System.out
                .println("Sending RPC to socket " + serverPortNumber
                        + ": command=" + command + " args="
                        + Arrays.deepToString(args));
        try {
            sock = new Socket("localhost", serverPortNumber);
        } catch (final IOException e) {
            logger.error("Cannot connect to server at localhost:"
                    + serverPortNumber);
            throw e;
        }
        final JsonRpcClient client = new JsonRpcClient();
        final InputStream is = sock.getInputStream();
        final OutputStream os = sock.getOutputStream();
        client.writeRequest(command, args, os, "0");
        // FIXME: for now, add a terminator.
        os.write('\n');
        os.flush();
        final Object rc;
        try {
            rc = client.readResponse(returnType, is);
            System.out.println("Got back: " + rc);
        } catch (final Throwable e) {
            logger.error("Cannot read back response", e);
            throw new VinInternalError(
                    "Cannot read back response of RPC to strongSwan", e);
        } finally {
            os.close();
            is.close();
            sock.close();
        }
        return rc;
    }

    @API
    static String requestAgentLaunch(final int portNumber,
            final boolean getIdentifier, final String host,
            final Properties props) throws VinError {
        String res = null;

        final Object args[] = { getIdentifier, host, props };
        try {
            res = (String) doRpc(portNumber, String.class,
                    "requestAgentLaunch", args);
        } catch (final IOException e) {
            throw new VinError("Failed to request an agent launch: props="
                    + props, e);
        }
        return res;
    }

    @API
    static int waitForLaunch(final int portNumber, final String id)
            throws VinError {
        int res = 0;

        final Object args[] = { id };
        try {
            res = (Integer) doRpc(portNumber, Integer.class, "waitForLaunch",
                    args);
        } catch (final IOException e) {
            throw new VinError("Failed to wait for launch: id=" + id, e);
        }
        return res;
    }

    @API
    static int stopRpc(final int portNumber) throws VinError {
        int res = 0;

        final Object args[] = {};
        try {
            res = (Integer) doRpc(portNumber, Integer.class, "stop", args);
        } catch (final IOException e) {
            throw new VinError("Failed to stop RPC server", e);
        }
        return res;
    }

}
