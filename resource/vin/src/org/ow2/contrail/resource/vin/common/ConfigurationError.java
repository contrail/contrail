package org.ow2.contrail.resource.vin.common;

/**
 * Exception thrown by the Vin framework.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class ConfigurationError extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new configuration exception with the given error message.
     * 
     * @param msg
     *            The error message associated with this exception.
     */
    public ConfigurationError(final String msg) {
        super(msg);
    }

    /**
     * Constructs a new configuration exception with the given error message.
     * 
     * @param msg
     *            The error message associated with this exception.
     * @param e
     *            The exception associated with this error.
     */
    public ConfigurationError(final String msg, final Exception e) {
        super(msg, e);
    }
}
