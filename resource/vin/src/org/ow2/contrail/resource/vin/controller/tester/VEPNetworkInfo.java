package org.ow2.contrail.resource.vin.controller.tester;

import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.vepdeployer.VEPDeployer;

class VEPNetworkInfo extends VINNetworkInfo {
    private final String user;
    private final String applicationId;

    VEPNetworkInfo(final String user, final String applicationId) {
        this.user = user;
        this.applicationId = applicationId;

    }

    @Override
    void destroy() throws VinError {
        VEPDeployer.deleteVM(user, applicationId);
    }

}