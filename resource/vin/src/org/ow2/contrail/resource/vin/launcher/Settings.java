package org.ow2.contrail.resource.vin.launcher;

class Settings {
    static final int MAXIMAL_RECEIVED_MESSAGE_QUEUE_LENGTH = 20;
    static final int MAXIMAL_ENGINE_SLEEP_INTERVAL = 1000;
    static final boolean traceDetailedProgress = true;
    static final long TRANSMITTER_SHUTDOWN_TIMEOUT = 10000;

    // FIXME choose a better default port number
    public static final int RPC_PORT_NUMBER = 32100;
    public static final int IBIS_PORT_NUMBER = 32101;
}
