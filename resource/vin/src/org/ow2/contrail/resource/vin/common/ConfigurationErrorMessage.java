package org.ow2.contrail.resource.vin.common;

/**
 * A message sent by an agent to the coordinator, informing it that there has
 * been a configuration error.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class ConfigurationErrorMessage extends Message {
    private static final long serialVersionUID = 1L;
    /** The identifier of the agent that reported this error. */
    public final String agentId;
    /** The error message. */
    public final String message;

    /**
     * @param agentId
     *            The identifier of the agent that reported this error.
     * @param message
     *            The error message of this error.
     */
    public ConfigurationErrorMessage(final String agentId, final String message) {
        this.agentId = agentId;
        this.message = message;
    }

    @Override
    public String toString() {
        return "ConfigurationErrorMessage[id=" + agentId + " message="
                + message + ']';
    }
}
