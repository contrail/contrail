package org.ow2.contrail.resource.vin.controller.impl;

import ibis.ipl.IbisIdentifier;

import java.net.InetAddress;
import java.util.LinkedList;
import java.util.Properties;

import org.ow2.contrail.resource.vin.annotations.Nullable;
import org.ow2.contrail.resource.vin.common.AgentContextType;
import org.ow2.contrail.resource.vin.common.Message;
import org.ow2.contrail.resource.vin.common.NetworkConnectionRemovalMessage;
import org.ow2.contrail.resource.vin.common.NetworkRemovalMessage;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.StopAgentMessage;
import org.ow2.contrail.resource.vin.common.Transmitter;
import org.ow2.contrail.resource.vin.common.VMDescriptionMessage;
import org.ow2.contrail.resource.vin.common.VinError;

class AgentInfo {
    InetAddress address;

    final String agentId;
    @Nullable
    IbisIdentifier ibisIdentifier = null;
    private boolean deleted = false;
    final LinkedList<Message> localQueue = new LinkedList<Message>();
    private final Properties vmProperties = new Properties();

    AgentInfo(final String agentId, final AgentContextType contextType,
            final Properties properties) {
        this.agentId = agentId;
        this.vmProperties.putAll(properties);
        // Queue a description message for the agent.
        final VMDescriptionMessage msg = new VMDescriptionMessage(contextType,
                properties);
        localQueue.add(msg);
    }

    void setDeleted() {
        deleted = true;
        localQueue.clear();
    }

    void setIbisIdentifier(final IbisIdentifier ii) {
        assert ibisIdentifier == null;
        ibisIdentifier = ii;
    }

    void sendPrivateNetworkDescriptionMessage(
            final ControllerNetworkAdministration privateNetworkAdministration,
            final String networkId, final Transmitter transmitter)
            throws VinError {
        final Message msg = privateNetworkAdministration
                .builPrivateNetworkDescriptionMessage(networkId);
        if (ibisIdentifier == null) {
            localQueue.add(msg);
        } else {
            transmitter.addToDataQueue(ibisIdentifier, msg);
        }
    }

    void sendPrivateNetworkMembershipMessage(
            final ControllerNetworkAdministration privateNetworkAdministration,
            final String networkId, final String connectionId,
            final Transmitter transmitter) throws VinError {
        final Message msg = privateNetworkAdministration
                .builPrivateNetworkMembershipMessage(networkId, connectionId);
        if (ibisIdentifier == null) {
            localQueue.add(msg);
        } else {
            transmitter.addToDataQueue(ibisIdentifier, msg);
        }
    }

    void sendNetworkConnectionRemovalMessage(final String networkId,
            final String connectionId, final Transmitter transmitter) {
        final Message msg = new NetworkConnectionRemovalMessage(networkId,
                connectionId);
        if (ibisIdentifier == null) {
            localQueue.add(msg);
        } else {
            transmitter.addToDataQueue(ibisIdentifier, msg);
        }
    }

    @Override
    public String toString() {
        return "AgentInfo[address=" + address + " id=" + agentId
                + " properties=" + vmProperties + " ibisIdentifier="
                + ibisIdentifier + " deleted=" + deleted + "]";
    }

    void sendNetworkRemovalMessage(final String networkId,
            final Transmitter transmitter) {
        final Message msg = new NetworkRemovalMessage(networkId);
        if (ibisIdentifier == null) {
            localQueue.add(msg);
        } else {
            transmitter.addToDataQueue(ibisIdentifier, msg);
        }
    }

    void sendStopMessage(final String machineId, final Transmitter transmitter) {
        final Message msg = new StopAgentMessage(machineId);
        if (ibisIdentifier == null) {
            localQueue.add(msg);
        } else {
            transmitter.addToDataQueue(ibisIdentifier, msg);
        }
    }

    /**
     * Send a stop message to the agent itself.
     * 
     * @param transmitter
     *            The transmitter to use to send the message.
     */
    void sendStopMessage(final Transmitter transmitter) {
        sendStopMessage(agentId, transmitter);
    }

    void setAddress(final InetAddress addr) {
        address = addr;
    }

    boolean isThisNode(final IbisIdentifier node) {
        return ibisIdentifier != null && ibisIdentifier.equals(node);
    }

    public boolean isActive() {
        return !deleted;
    }

    public String getHostName() {
        return vmProperties.getProperty(PropertyNames.NAME);
    }
}