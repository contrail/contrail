package org.ow2.contrail.resource.vin.agent;

import java.net.InetAddress;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.VinInternalError;

final class AgentConnectionDescriptor {
    final AgentNetworkDescriptor network;
    final String connectionId;
    final String vmId;
    private final InetAddress vinAddress;
    final InetAddress hostAddress;
    final int ordinal;
    final boolean ready;
    private final Properties properties;

    AgentConnectionDescriptor(final AgentNetworkDescriptor network,
            final int ordinal, final String connectionId, final String vmId,
            final InetAddress vinAddress, final InetAddress hostAddress,
            final boolean ready, final Properties properties) {
        this.network = network;
        this.ordinal = ordinal;
        this.connectionId = connectionId;
        this.vmId = vmId;
        this.vinAddress = vinAddress;
        this.hostAddress = hostAddress;
        this.ready = ready;
        this.properties = properties;
    }

    @Override
    public String toString() {
        return vmId + "(" + vinAddress.getHostAddress() + ") hostAddress="
                + hostAddress + " network=" + network.networkId + " ordinal="
                + ordinal + " ready=" + ready;
    }

    String getHostAddressString() {
        if (hostAddress == null) {
            throw new VinInternalError("Host address unknown for connection "
                    + connectionId + " vmId=" + vmId);
        }
        return hostAddress.getHostAddress();
    }

    String getVINAddressString() {
        return vinAddress.getHostAddress();
    }

    String getMacAddress() {
        return network.getMacAddress(vinAddress);
    }

    boolean isOnSameHost(final AgentConnectionDescriptor other) {
        return hostAddress.equals(other.hostAddress);
    }

    public void setProperty(final String nm, final String val) {
        properties.setProperty(nm, val);
    }

    public String getProperty(final String nm) {
        return properties.getProperty(nm);
    }

    public String getHostName() {
        final String domain = network.getDomain();
        if (domain == null) {
            return null;
        }
        final String hostName = properties.getProperty(PropertyNames.NAME);
        if (hostName == null) {
            return null;
        }
        return hostName + "." + domain;
    }
}
