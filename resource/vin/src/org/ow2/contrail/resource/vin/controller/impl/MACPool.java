package org.ow2.contrail.resource.vin.controller.impl;

import org.ow2.contrail.resource.vin.common.ArrayValue;
import org.ow2.contrail.resource.vin.common.ByteArrayPool;
import org.ow2.contrail.resource.vin.common.VinError;

class MACPool extends ByteArrayPool {
    private static final ArrayValue parseMacAddress(final String s)
            throws VinError {
        final String byteStrings[] = s.split(":|-");
        if (byteStrings.length != 6) {
            throw new VinError("Malformed MAC address '" + s
                    + "': requires exactly six values");
        }
        final byte res[] = new byte[6];
        for (int i = 0; i < 6; i++) {
            final String bs = byteStrings[i];
            res[i] = (byte) Integer.parseInt(bs, 16);
        }
        return new ArrayValue(res);
    }

    MACPool(final String startAddress, final String endAddress) throws VinError {
        super(parseMacAddress(startAddress), parseMacAddress(endAddress),
                "MAC addresses");
    }

}
