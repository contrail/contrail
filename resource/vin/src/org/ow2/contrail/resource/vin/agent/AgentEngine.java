package org.ow2.contrail.resource.vin.agent;

import ibis.ipl.Ibis;
import ibis.ipl.IbisCapabilities;
import ibis.ipl.IbisFactory;
import ibis.ipl.IbisIdentifier;
import ibis.ipl.Registry;
import ibis.ipl.RegistryEventHandler;
import ibis.smartsockets.direct.IPAddressSet;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.ow2.contrail.resource.vin.common.AgentContextType;
import org.ow2.contrail.resource.vin.common.AgentErrorMessage;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.ConfigurationErrorMessage;
import org.ow2.contrail.resource.vin.common.Flag;
import org.ow2.contrail.resource.vin.common.GlobalGlobals;
import org.ow2.contrail.resource.vin.common.Message;
import org.ow2.contrail.resource.vin.common.NetworkConnectionRemovalMessage;
import org.ow2.contrail.resource.vin.common.NetworkDescriptionMessage;
import org.ow2.contrail.resource.vin.common.NetworkMembershipMessage;
import org.ow2.contrail.resource.vin.common.NetworkRemovalMessage;
import org.ow2.contrail.resource.vin.common.PacketSendPort;
import org.ow2.contrail.resource.vin.common.PacketUpcallReceivePort;
import org.ow2.contrail.resource.vin.common.ReceivedMessageQueue;
import org.ow2.contrail.resource.vin.common.RegisterAgentMessage;
import org.ow2.contrail.resource.vin.common.StopAgentMessage;
import org.ow2.contrail.resource.vin.common.Transmitter;
import org.ow2.contrail.resource.vin.common.TransmitterListener;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VMDescriptionMessage;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class AgentEngine extends Thread implements RegistryEventHandler,
        TransmitterListener {
    private static final IbisCapabilities ibisCapabilities = new IbisCapabilities(
            IbisCapabilities.MEMBERSHIP_UNRELIABLE,
            IbisCapabilities.ELECTIONS_STRICT);
    private final ReceivedMessageQueue receivedMessageQueue = new ReceivedMessageQueue(
            Settings.MAXIMAL_RECEIVED_MESSAGE_QUEUE_LENGTH);
    private final Flag stopped = new Flag(false);
    private final Transmitter transmitter;
    private final AgentNetworkAdministration networkAdministration;
    private final ConcurrentLinkedQueue<IbisIdentifier> deletedNodes = new ConcurrentLinkedQueue<IbisIdentifier>();
    private final PacketUpcallReceivePort receivePort;
    private final IbisIdentifier controllerIdentifier;
    private final Ibis localIbis;
    private final String localAgentId;
    private final static Logger logger = LoggerFactory
            .getLogger(AgentEngine.class);
    private boolean controllerIsGone = false;
    private final File configDir;
    private final File scriptDir;
    private final ScheduledExecutorService scheduler = Executors
            .newScheduledThreadPool(1);
    private final File safeconfigPath;
    private final InetAddress testAddress;

    /**
     * Creates a new Vin engine.
     * 
     * @param configDir
     *            The directories where the interface scripts live.
     * @param startScripts
     *            The list of scripts to run when a new network is created.
     * 
     * @param stopScripts
     *            The list of scripts to run when a network is stopped.
     * @param agentId
     *            The identifier of this agent.
     * @param libivrtVmId
     *            The libvirt identifier of this agent.
     * @throws Exception
     *             Thrown on problems.
     */
    protected AgentEngine(final File scriptDir, final File configDir,
            final File safeconfigPath, final ArrayList<File> startScripts,
            final ArrayList<File> stopScripts, final String agentId,
            final String libvirtId, final InetAddress testAddress)
            throws Exception {
        super("agent engine thread");
        localAgentId = agentId;
        this.scriptDir = scriptDir;
        this.configDir = configDir;
        this.safeconfigPath = safeconfigPath;
        this.testAddress = testAddress;
        networkAdministration = new AgentNetworkAdministration(scriptDir,
                configDir, safeconfigPath, startScripts, stopScripts, libvirtId);
        transmitter = new Transmitter(this);
        final Properties ibisProperties = new Properties();
        localIbis = IbisFactory.createIbis(ibisCapabilities, ibisProperties,
                true, this, PacketSendPort.portType,
                PacketUpcallReceivePort.portType);
        final Registry registry = localIbis.registry();
        final IbisIdentifier myIbis = localIbis.identifier();
        controllerIdentifier = registry
                .getElectionResult(GlobalGlobals.CONTROLLER_ELECTION_NAME);
        receivePort = new PacketUpcallReceivePort(localIbis,
                GlobalGlobals.receivePortName, receivedMessageQueue, this);
        transmitter.start();
        registry.enableEvents();
        receivePort.enable();
        final InetAddress[] addresses = determineAddresses();
        /* Tell the master we're ready. */
        final RegisterAgentMessage msg = new RegisterAgentMessage(agentId,
                addresses);
        transmitter.addToDataQueue(controllerIdentifier, msg);
        logger.info("Created ibis " + myIbis + " " + Utils.getPlatformVersion()
                + " host " + Utils.getCanonicalHostname());
    }

    /**
     * Try to figure out which address is the best to reach this machine. For
     * now we do that by relying on a SmartSockets ranking of the addresses of
     * this machine, and picking the first public one in the ranking. This is
     * certainly not perfect, but for now it is sufficient.
     * 
     * @return The best address to reach this machine.
     * @throws VinError
     *             Thrown if there is no public address.
     */
    private static InetAddress[] determineAddresses() throws VinError {
        final IPAddressSet set = IPAddressSet.getLocalHost();
        if (false) {
            if (!set.containsPublicAddress()) {
                throw new VinError(
                        "Cannot find a public IP address. Known addresses are: "
                                + set.toString());
            }
        }
        logger.debug("Found the following IP addresses: " + set.toString());
        return set.getAddresses();
    }

    @Override
    public void died(final IbisIdentifier agent) {
        if (agent.equals(localIbis.identifier())) {
            if (!stopped.isSet()) {
                logger.error("This agent has been declared dead, we might as well stop");
                setStopped();
            }
        } else {
            transmitter.deleteNode(agent);
            deletedNodes.add(agent);
        }
        wakeTransmitterListenerThread(); // Something interesting has happened.
    }

    @Override
    public void left(final IbisIdentifier agent) {
        if (agent.equals(localIbis.identifier())) {
            if (!stopped.isSet()) {
                logger.error("This agent has been declared `left', we might as well stop");
                setStopped();
            }
        } else {
            transmitter.deleteNode(agent);
            deletedNodes.add(agent);
        }
        wakeTransmitterListenerThread(); // Something interesting has happened.
    }

    @Override
    public void electionResult(final String arg0, final IbisIdentifier arg1) {
        // Not interesting.
    }

    @Override
    public void gotSignal(final String arg0, final IbisIdentifier arg1) {
        // Not interesting.
    }

    @Override
    public void joined(final IbisIdentifier agent) {
        // Ignore
    }

    /**
     * Register the fact that no new agents can join this pool. Not relevant, so
     * we ignore this.
     */
    @Override
    public void poolClosed() {
        // Not interesting.
    }

    @Override
    public void poolTerminated(final IbisIdentifier arg0) {
        setStopped();
    }

    int suspectCount = 0;

    /**
     * This ibis was reported as 'may be dead'. Try not to communicate with it.
     * 
     * @param node
     *            The node that may be dead.
     */
    @Override
    public void setSuspect(final IbisIdentifier node) {
        suspectCount++;
        if (Integer.bitCount(suspectCount) == 1) {
            if (suspectCount == 1) {
                logger.info("Node " + node + " may be dead", new Throwable());
            } else {
                logger.info("Node " + node + " may be dead (repeated "
                        + suspectCount + " times)", new Throwable());
            }
        }
        try {
            localIbis.registry().maybeDead(node);
        } catch (final IOException e) {
            // Nothing we can do about it.
        }
    }

    /**
     * Handle any new and deleted nodes that have been registered after the last
     * call.
     * 
     * @return <code>true</code> iff we made any changes to the node state.
     */
    private boolean registerNewAndDeletedNodes() {
        boolean changes = false;
        while (true) {
            final IbisIdentifier node = deletedNodes.poll();
            if (node == null) {
                break;
            }
            registerNodeLeft(node);
            changes = true;
        }
        return changes;
    }

    private void setStopped() {
        stopped.set();
        wakeTransmitterListenerThread(); // Something interesting has happened.
    }

    /** Tell the engine thread that something interesting has happened. */
    @Override
    public void wakeTransmitterListenerThread() {
        synchronized (this) {
            notifyAll();
        }
    }

    private void registerVMInfo(final AgentContextType context,
            final Properties properties) {
        networkAdministration.setContextType(context);
        networkAdministration.setVMProperties(properties);
    }

    @SuppressWarnings("synthetic-access")
    private class ConfigTestRun implements Runnable {
        final String configName;
        private final String[] parameters;

        public ConfigTestRun(final String configName,
                final String... parameters) {
            this.configName = configName;
            this.parameters = parameters;
        }

        @Override
        public void run() {
            logger.debug("Starting test config " + configName);
            try {
                Utils.cleanlyRunConfig(safeconfigPath, true, configDir,
                        configName, parameters);
            } catch (final Throwable e) {
                final String message = "Test failed: configName=" + configName
                        + " parameters=" + Arrays.deepToString(parameters);
                final Message msg = new AgentErrorMessage(localAgentId,
                        message, e);
                logger.error(message, e);
                transmitter.sendEmergencyMessage(controllerIdentifier, msg);
            }
        }

        @Override
        public String toString() {
            return "Test run of script " + configName + " parameters="
                    + Arrays.deepToString(parameters);
        }
    }

    @SuppressWarnings("synthetic-access")
    private class ScriptTestRun implements Runnable {
        final String scriptName;
        private final String[] parameters;

        public ScriptTestRun(final String configName,
                final String... parameters) {
            this.scriptName = configName;
            this.parameters = parameters;
        }

        @Override
        public void run() {
            logger.debug("Starting test script " + scriptName);
            try {
                Utils.cleanlyExecuteScript(scriptDir, scriptName, parameters);
            } catch (final Throwable e) {
                final String message = "Test failed: configName=" + scriptName
                        + " parameters=" + Arrays.deepToString(parameters);
                final Message msg = new AgentErrorMessage(localAgentId,
                        message, e);
                logger.error(message, e);
                transmitter.sendEmergencyMessage(controllerIdentifier, msg);
            }
        }

        @Override
        public String toString() {
            return "Test run of script " + scriptName + " parameters="
                    + Arrays.deepToString(parameters);
        }
    }

    private void handleSingleMessage(final Message msg) {
        logger.debug("Handling message " + msg);
        GlobalGlobals.demoLogger.info("AGENT: handling message " + msg);
        try {
            if (msg instanceof VMDescriptionMessage) {
                final VMDescriptionMessage vmm = (VMDescriptionMessage) msg;
                registerVMInfo(vmm.context, vmm.properties);
            } else if (msg instanceof NetworkMembershipMessage) {
                final NetworkMembershipMessage pnmm = (NetworkMembershipMessage) msg;
                networkAdministration.registerConnection(this, pnmm.ordinal,
                        pnmm.networkId, pnmm.connectionId, pnmm.machine,
                        pnmm.address, pnmm.hostAddress, localAgentId,
                        pnmm.ready, pnmm.properties);
            } else if (msg instanceof NetworkDescriptionMessage) {
                final NetworkDescriptionMessage pndm = (NetworkDescriptionMessage) msg;
                networkAdministration.createNetwork(pndm.networkType,
                        pndm.networkId, pndm.properties, pndm.subnet,
                        pndm.macAddresses, pndm.caUri);
            } else if (msg instanceof NetworkConnectionRemovalMessage) {
                final NetworkConnectionRemovalMessage m = (NetworkConnectionRemovalMessage) msg;
                networkAdministration.removeConnection(m.networkId,
                        m.connectionId, localAgentId);
            } else if (msg instanceof NetworkRemovalMessage) {
                final NetworkRemovalMessage m = (NetworkRemovalMessage) msg;
                networkAdministration.removeNetwork(m.networkId);
            } else if (msg instanceof StopAgentMessage) {
                final StopAgentMessage m = (StopAgentMessage) msg;
                if (localAgentId != null && localAgentId.equals(m.agentId)) {
                    // Hey, that's us!
                    setStopped();
                }
            } else if (msg instanceof RegisterAgentMessage) {
                throw new VinInternalError("why do I get this message: " + msg);
            } else {
                logger.error("Internal error: " + "Don't know how to handle a "
                        + msg.getClass() + " message", new Throwable());
            }
        } catch (final VinError e) {
            final ConfigurationErrorMessage errmsg = new ConfigurationErrorMessage(
                    localAgentId, e.getLocalizedMessage());
            sendToController(errmsg);
            setStopped();
        } catch (final ConfigurationError e) {
            final ConfigurationErrorMessage errmsg = new ConfigurationErrorMessage(
                    localAgentId, e.getLocalizedMessage());
            transmitter.addToDataQueue(controllerIdentifier, errmsg);
            setStopped();
        }
    }

    private boolean handleIncomingMessages() {
        boolean progress = false;
        while (true) {
            final Message msg = receivedMessageQueue.getNext();
            if (msg == null) {
                break;
            }
            logger.info("Get incoming message from queue: " + msg);
            handleSingleMessage(msg);
            progress = true;
        }
        return progress;
    }

    private void registerNodeLeft(final IbisIdentifier node) {
        if (controllerIdentifier != null && node.equals(controllerIdentifier)) {
            controllerIsGone = true;
        }
    }

    @Override
    public Ibis getLocalIbis() {
        return localIbis;
    }

    @Override
    public void run() {
        final int sleepTime = Settings.MAXIMAL_ENGINE_SLEEP_INTERVAL;
        try {
            while (!stopped.isSet()) {
                boolean progress;
                do {
                    // Keep doing bookkeeping chores until all is done.
                    final boolean progressIncoming = handleIncomingMessages();
                    final boolean progressNodeChurn = registerNewAndDeletedNodes();
                    progress = progressIncoming || progressNodeChurn;
                    if (Settings.traceDetailedProgress) {
                        if (progress) {
                            logger.trace("Main loop: progress=true incoming="
                                    + progressIncoming + " churn="
                                    + progressNodeChurn);
                            if (progressIncoming) {
                                logger.info(receivedMessageQueue.formatCounts());
                            }
                        }
                    }
                } while (progress);
                if (controllerIsGone) {
                    logger.info("Setting engine to stopped state");
                    stopped.set();
                    break;
                }
                synchronized (this) {
                    if (receivedMessageQueue.isEmpty() && !stopped.isSet()
                            && deletedNodes.isEmpty()) {
                        try {
                            this.wait(sleepTime);
                        } catch (final InterruptedException e) {
                            // Ignored.
                        }
                    }
                }
            }
        } catch (final Throwable e) {
            final Message msg = new AgentErrorMessage(localAgentId,
                    "Agent is fatally wounded", e);
            logger.error("Agent is fatally wounded", e);
            transmitter.sendEmergencyMessage(controllerIdentifier, msg);
        } finally {
            networkAdministration.onShutDown();
            logger.info("Shutting down transmitter");
            transmitter.setStopped();
            try {
                transmitter.join(Settings.TRANSMITTER_SHUTDOWN_TIMEOUT);
            } catch (final InterruptedException e) {
                // ignore.
            }
            logger.info("Transmitter has shut down");
            try {
                receivePort.close();
                logger.info("Receive port was closed");
                localIbis.end();
            } catch (final IOException x) {
                // Nothing we can do about it.
            }
        }
        transmitter.printStatistics();
        Utils.printThreadStats(logger);
    }

    void runAConfigTest(final String config, final int delay,
            final String vinAddress) {
        // FIXME: don't assume localhost if there is no test address
        final String testAddressString = testAddress == null ? "localhost"
                : testAddress.getHostAddress();
        final ConfigTestRun test = new ConfigTestRun(config, testAddressString,
                vinAddress);
        logger.debug("Scheduling test '" + test + "' to run in " + delay
                + " seconds, testAddress=" + testAddressString
                + ", vinAddress=" + vinAddress);
        // FIXME: count outstanding tests, don't stop until zero...
        scheduler.schedule(test, delay, TimeUnit.SECONDS);
    }

    void runAScriptTest(final String script, final int delay,
            final String vinAddress) {
        // FIXME: don't assume localhost if there is no test address
        final String testAddressString = testAddress == null ? "localhost"
                : testAddress.getHostAddress();
        final ScriptTestRun test = new ScriptTestRun(script, testAddressString,
                vinAddress);
        logger.debug("Scheduling test '" + test + "' to run in " + delay
                + " seconds, testAddress=" + testAddressString);
        // FIXME: count outstanding tests, don't stop until zero...
        scheduler.schedule(test, delay, TimeUnit.SECONDS);
    }

    void sendToController(final Message msg) {
        transmitter.addToDataQueue(controllerIdentifier, msg);
    }
}
