package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.io.IOException;

import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class to manipulate the /etc/hosts file. At the moment we simply assume
 * that we have sufficient rights to overwrite the file.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class EtcHostsManager {
    private static final Logger logger = LoggerFactory
            .getLogger(EtcHostsManager.class);
    private static final File etcFile = new File("/etc/hosts");

    private static String readFile() throws VinError {
        try {
            return Utils.readFile(etcFile);
        } catch (final IOException e) {
            throw new VinError("Cannot read " + etcFile, e);
        }
    }

    private static void writeFile(final String content) throws VinError {
        try {
            final File f = File.createTempFile("vinagent-etchosts", "txt",
                    etcFile.getParentFile());
            Utils.writeFile(f, content);
            final boolean ok = f.renameTo(etcFile);
            if (!ok) {
                throw new VinError("Cannot overwrite " + etcFile);
            }
            logger.debug("Writing new " + etcFile + ":\n" + content);
        } catch (final IOException e) {
            throw new VinError("Cannot write " + etcFile, e);
        }
    }

    private static String buildEntryString(final String hostname,
            final String ipAddress) {
        final String entryString = ipAddress + " " + hostname;
        return entryString;
    }

    static void addEntry(final String hostname, final String ipAddress)
            throws VinError {
        String content = readFile();
        final String entryString = buildEntryString(hostname, ipAddress);
        content += entryString + " # Entry added by VIN agent" + "\n";
        writeFile(content);
    }

    static void removeEntry(final String hostname, final String ipAddress)
            throws VinError {
        final String content = readFile();
        final String lines[] = content.split("[\r\n]+");
        final StringBuilder newContent = new StringBuilder();
        final String entry = buildEntryString(hostname, ipAddress);
        boolean changed = false;
        for (final String line : lines) {
            if (!line.startsWith(entry)) {
                newContent.append(line);
            } else {
                changed = true;
            }
        }
        if (changed) {
            writeFile(newContent.toString());
        } else {
            throw new VinInternalError("Removing " + etcFile + " entry "
                    + entry + " does not change file", new Exception());
        }
    }
}
