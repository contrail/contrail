package org.ow2.contrail.resource.vin.agent;

import java.io.IOException;

public class PingStyx {

    /**
     * @param args
     *            The command-line parameters
     */
    public static void main(final String[] args) {
        try {
            final String v = StrongSwanDaemonTalker.testConnection();
            System.out.println("Version: " + v);
        } catch (final IOException e) {
            System.err.println("Failure: " + e.getLocalizedMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

}
