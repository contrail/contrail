package org.ow2.contrail.resource.vin.agent;

import java.io.File;

import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.SanityChecks;
import org.ow2.contrail.resource.vin.common.VinError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tunnel creation handler using StrongSwan.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class StrongSwanGuestNetworkHandler implements NetworkHandler {
    private final String networkId;
    private boolean didSanityChecks;
    private final StrongSwanDaemonTalker daemonTalker;
    private static final Logger logger = LoggerFactory
            .getLogger(StrongSwanGuestNetworkHandler.class);
    private static final boolean useCmd = true;

    StrongSwanGuestNetworkHandler(final StrongSwanDaemonTalker daemonTalker,
            final String networkId) {
        this.daemonTalker = daemonTalker;
        this.networkId = networkId;
    }

    private static String buildConnectionIdentifier(final String networkId,
            final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) {
        assert local != null;
        assert remote != null;
        final String id = networkId + "-link-" + local.ordinal + '-'
                + remote.ordinal;
        return id;
    }

    @Override
    public void createConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError,
            VinError {
        logger.debug("creating connection: local=" + local + " remote="
                + remote);
        final String connectionId = buildConnectionIdentifier(networkId, local,
                remote);
        if (useCmd) {
        } else {
            daemonTalker.createConnection(connectionId, local, remote);
        }
        logger.trace("created connection: local=" + local + " remote=" + remote);
    }

    @Override
    public void removeConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError,
            VinError {
        logger.debug("destroying connection: local=" + local + " remote="
                + remote);
        assert local != null;
        assert remote != null;
        final String connectionId = buildConnectionIdentifier(networkId, local,
                remote);
        daemonTalker.removeConnection(connectionId);
        logger.trace("destroyed connection: local=" + local + " remote="
                + remote);
    }

    @Override
    public void createNetwork(final AgentNetworkDescriptor network) {
        // Nothing
    }

    @Override
    public void removeNetwork(final AgentNetworkDescriptor network) {
        // Nothing
    }

    @Override
    public void createLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection) {
        // Ignore.
    }

    @Override
    public void removeLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection)
            throws ConfigurationError {
        // Ignore
    }

    @Override
    public void doSanityChecks() throws ConfigurationError {
        if (!didSanityChecks) {
            didSanityChecks = true;
            // Ensure forwarding is enabled.
            SanityChecks.ensureFileContentMatches(new File(
                    "/proc/sys/net/ipv4/ip_forward"), "1\n");
            logger.debug("System passed strongSwan sanity checks");
        }
    }

    @Override
    public String getFriendlyName() {
        return "StrongSwan";
    }
}
