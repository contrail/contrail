package org.ow2.contrail.resource.vin.launcher;

import ibis.ipl.IbisIdentifier;

import java.util.LinkedList;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.Transmitter;

class LauncherInfo {
    final String hostId;
    final IbisIdentifier ibisIdentifier;
    private final LinkedList<LaunchQueueEntry> launchQueue = new LinkedList<LaunchQueueEntry>();

    /**
     * @param hostId
     *            The identifier of the launcher in the launching
     *            administration.
     * @param ibisIdentifier
     *            The Ibis identifier of the launcher.
     */
    public LauncherInfo(final String hostId, final IbisIdentifier ibisIdentifier) {
        this.hostId = hostId;
        this.ibisIdentifier = ibisIdentifier;
    }

    void registerFutureLaunch(final String launchId, final Properties properties) {
        final LaunchQueueEntry e = new LaunchQueueEntry(launchId, properties);
        launchQueue.add(e);
    }

    void transmitBacklog(final Transmitter transmitter) {
        for (final LaunchQueueEntry e : launchQueue) {
            final LaunchAgentMessage msg = new LaunchAgentMessage(e.launchId,
                    hostId, e.properties);
            transmitter.addToDataQueue(ibisIdentifier, msg);
        }
    }

}
