package org.ow2.contrail.resource.vin.common;

import ibis.util.RunProcess;

public interface ExecuteCommandListener {

    void onCommandCompleted(RunProcess p);

}
