package org.ow2.contrail.resource.vin.common;

import ibis.ipl.IbisIdentifier;
import ibis.ipl.SendPort;
import ibis.ipl.WriteMessage;

import java.io.IOException;

import org.slf4j.Logger;

/**
 * Maintains a cache of open connections.
 * 
 * @author Kees van Reeuwijk.
 */
class ConnectionCache {
    private final TransmitterListener node;

    private final SendPortCache cache;

    ConnectionCache(final TransmitterListener node2) {
        node = node2;
        cache = new SendPortCache(node2, Settings.CONNECTION_CACHE_SIZE,
                Settings.CONNECTION_CACHE_MAXIMAL_UNUSED_COUNT);
    }

    /**
     * Given a message, send it to the given ibis.
     * 
     * @param ibis
     *            The ibis to send the message to.
     * @param message
     *            The message to send.
     * @return The number of bytes that were used to send this message, or -1 if
     *         the message could not be sent.
     */
    private long cachedSendMessage(final IbisIdentifier ibis,
            final Message message) {
        long len = -1;
        try {
            final SendPort port = cache.getSendPort(ibis);
            if (port == null) {
                // We could not create a connection to this ibis.
                // Presumably the node is dead.
                Globals.transmitterLogger
                        .debug("Could not get send port for ibis " + ibis);
                cache.closeSendPort(ibis);
                return -1;
            }
            final WriteMessage msg = port.newMessage();
            msg.writeObject(message);
            len = msg.finish();
        } catch (final IOException x) {
            Globals.transmitterLogger.warn("Could not get send port for ibis "
                    + ibis + ": " + x.getLocalizedMessage(), x);
            node.setSuspect(ibis);
            cache.closeSendPort(ibis);
        }
        return len;
    }

    /**
     * Given an ibis, returns a WriteMessage to use.
     * 
     * @param ibis
     *            The ibis to send to.
     * @return The WriteMessage to fill.
     */
    private long uncachedSendMessage(final IbisIdentifier ibis,
            final Message message) {
        long len = -1;
        SendPort port = null;
        try {
            port = node.getLocalIbis().createSendPort(PacketSendPort.portType);
            port.connect(ibis, Globals.receivePortName,
                    Settings.COMMUNICATION_TIMEOUT, true);
            WriteMessage msg = null;
            try {
                msg = port.newMessage();
                msg.writeObject(message);
            } finally {
                if (msg != null) {
                    len = msg.finish();
                }
            }
            port.close();
            return len;
        } catch (final IOException x) {
            node.setSuspect(ibis);
        } finally {
            try {
                if (port != null) {
                    port.close();
                }
            } catch (final Exception x) {
                // Nothing we can do.
            }
        }
        return len;
    }

    /**
     * Given an ibis and a message, send the message.
     * 
     * @param ibis
     *            The ibis to send to.
     * @param message
     *            The message to send.
     * @return The number of bytes that were transmitted.
     */
    long sendMessage(final IbisIdentifier ibis, final Message message) {
        long sz;

        if (Settings.CACHE_CONNECTIONS) {
            sz = cachedSendMessage(ibis, message);
        } else {
            sz = uncachedSendMessage(ibis, message);
        }
        return sz;
    }

    void printStatistics(final Logger s) {
        cache.printStatistics(s);
    }

    public void close() {
        cache.close();
    }
}
