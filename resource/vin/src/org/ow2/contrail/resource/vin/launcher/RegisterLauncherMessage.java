package org.ow2.contrail.resource.vin.launcher;

import org.ow2.contrail.resource.vin.common.Message;

/**
 * A message from a launcher running on a particular host to the central
 * coordinator, telling it that it is present, and what is ibis identifier and
 * its host identifier are.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class RegisterLauncherMessage extends Message {
    private static final long serialVersionUID = 1L;

    final String hostId;

    public RegisterLauncherMessage(final String hostId) {
        this.hostId = hostId;
    }

    @Override
    public String toString() {
        return "RegisterLauncherMessage[source=" + source + ", hostId="
                + hostId + "]";
    }

}
