package org.ow2.contrail.resource.vin.common;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class IPv6AddressRangePool extends ByteArrayPool implements
        IPAddressRangePool {

    private static final int BYTES_IN_ADDRESS = 16;

    IPv6AddressRangePool(final ArrayValue startBytes,
            final ArrayValue endBytes, final String friendlyName) {
        super(startBytes, endBytes, friendlyName);
        assert (startBytes.size() == BYTES_IN_ADDRESS);
        assert (endBytes.size() == BYTES_IN_ADDRESS);
    }

    public IPv6AddressRangePool(final String startAddress,
            final String endAddress, final String friendlyName)
            throws UnknownHostException {
        this(getAddressBytes(startAddress), getAddressBytes(endAddress),
                friendlyName);
    }

    private static ArrayValue getAddressBytes(final String a)
            throws UnknownHostException {
        final InetAddress address = InetAddress.getByName(a);
        final byte[] addressBytes = address.getAddress();
        assert (addressBytes.length == BYTES_IN_ADDRESS);
        return new ArrayValue(addressBytes);
    }

    /**
     * Given the number of elements in a subnet, return the number of bits in
     * the subnet mask that is needed for this.
     * 
     * @param n
     *            The number of elements in the subnet.
     * @return The number of bits in the subnet mask.
     */
    private static int calculateAlignmentBits(final long n) {
        final int hob = 63 - Long.numberOfLeadingZeros(n);
        final long roundedValue = 1L << hob;
        if (roundedValue == n) {
            return hob;
        }
        return hob + 1;
    }

    /**
     * Given the number of member that at maximum have to participate in this
     * subnet, return a subnet to accommodate them all.
     * 
     * @param n
     *            The number of standard members to participate in this subnet.
     * @return The subnet to use.
     * @throws OutOfEntriesError
     *             Thrown if there are not enough addresses left in the pool to
     *             fulfill this request.
     */
    @Override
    public IPv6Subnet getNextSubnet(final long n) throws OutOfEntriesError {
        // Gateway, broadcast, dhcp
        final long members = n + 3;
        final int alignmentBits = calculateAlignmentBits(members);
        final ArrayValueRange addressBytes = getNextSubrange(members,
                alignmentBits);
        return new IPv6Subnet(addressBytes, alignmentBits);
    }

    @Override
    public InetAddress getNextAddress() throws OutOfEntriesError {
        final ArrayValue address = getNextValue();
        try {
            assert address.size() == 16;
            final byte addressBytes[] = new byte[] { (byte) address.get(0),
                    (byte) address.get(1), (byte) address.get(2),
                    (byte) address.get(3), (byte) address.get(4),
                    (byte) address.get(5), (byte) address.get(6),
                    (byte) address.get(7), (byte) address.get(8),
                    (byte) address.get(9), (byte) address.get(10),
                    (byte) address.get(11), (byte) address.get(12),
                    (byte) address.get(13), (byte) address.get(14),
                    (byte) address.get(15) };
            return InetAddress.getByAddress(addressBytes);
        } catch (final UnknownHostException e) {
            throw new VinInternalError(e);
        }
    }

    /**
     * Given an address, add it to the pool of available addresses.
     * 
     * @param addr
     *            The address to return.
     */
    @Override
    public void recycleAddress(final InetAddress addr) {
        final byte[] address = addr.getAddress();
        assert address.length == BYTES_IN_ADDRESS;
        final ArrayValue v = new ArrayValue(address);
        recycleValue(v);
    }

    /**
     * Given an subrange, add it to the pool of available addresses.
     * 
     * @param sn
     *            The subrange to return.
     */
    @Override
    public void recycleSubnet(final Subnet sn) {
        assert sn instanceof IPv6Subnet;
        final IPv6Subnet subnet = (IPv6Subnet) sn;
        recycleSubrange(subnet.startValue, subnet.endValue);
    }

    private void recycleSubrange(final ArrayValue s, final ArrayValue e) {
        addToRecyclebin(new ArrayValueRange(s, e));
    }

}