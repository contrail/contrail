package org.ow2.contrail.resource.vin.controller.impl;

import java.net.InetAddress;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.NetworkMembershipMessage;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;

final class ConnectionDescriptor {
    final String connectionId;
    private final InetAddress VINAddress;
    final AgentInfo agent;
    private final int ordinal;
    private final String networkId;
    private final Properties properties;
    private boolean ready;

    ConnectionDescriptor(final int ordinal, final AgentInfo agent,
            final String networkId, final String connectionId,
            final InetAddress address, final boolean ready,
            final Properties properties) {
        this.ordinal = ordinal;
        this.networkId = networkId;
        this.connectionId = connectionId;
        this.VINAddress = address;
        this.agent = agent;
        this.ready = ready;
        this.properties = properties;
    }

    NetworkMembershipMessage buildPrivateNetworkMembershipMessage() {
        final InetAddress hostAddress = agent.address;
        if (hostAddress == null) {
            throw new VinInternalError(
                    "Host address unknown for connection connectionId="
                            + connectionId + " vmId=" + agent.agentId);
        }
        return new NetworkMembershipMessage(ordinal, networkId, connectionId,
                agent.agentId, VINAddress, hostAddress, ready, properties);
    }

    @Override
    public String toString() {
        return agent.agentId + "(" + VINAddress.getHostAddress() + ")";
    }

    String getVINAddressString() {
        return VINAddress.getHostAddress();
    }

    void returnVINAddress(final ControllerEngine controller) throws VinError {
        controller.returnLocalAddress(networkId, VINAddress);
    }

    public String getName() {
        return properties.getProperty(PropertyNames.NAME);
    }

    boolean setReady() {
        final boolean changed = ready != true;
        ready = true;
        return changed;
    }

}