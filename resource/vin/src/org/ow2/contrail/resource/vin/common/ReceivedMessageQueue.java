package org.ow2.contrail.resource.vin.common;

import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * A queue of incoming messages.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class ReceivedMessageQueue {
    private final BlockingQueue<Message> q;
    private int maximalQueueLength = 0;
    private final HashMap<String, Integer> counts = new HashMap<String, Integer>();

    /**
     * Constructs a new message queue with the capacity given in
     * <code>capacity</code>.
     * 
     * @param capacity
     *            The capacity of the new message queue.
     */
    public ReceivedMessageQueue(final int capacity) {
        this.q = new ArrayBlockingQueue<Message>(capacity, true);
    }

    /**
     * Returns the next message in the queue, or <code>null</code> if the queue
     * is empty.
     * 
     * @return The message.
     */
    public Message getNext() {
        return q.poll();
    }

    private void countMessage(final Message m) {
        final Class<? extends Message> cl = m.getClass();
        final String nm = cl.getSimpleName();
        synchronized (counts) {
            if (counts.containsKey(nm)) {
                final Integer n = counts.get(nm);
                counts.put(nm, n + 1);
            } else {
                counts.put(nm, 1);
            }
        }
    }

    public String formatCounts() {
        final StringBuilder buf = new StringBuilder();
        synchronized (counts) {
            final Set<String> keys = counts.keySet();
            buf.append("Received messages:");
            for (final String k : keys) {
                final Integer n = counts.get(k);
                buf.append(" " + k + ": " + n);
            }
        }
        return buf.toString();
    }

    void add(final Message msg) {
        try {
            q.put(msg);
            final int sz = q.size();
            if (maximalQueueLength < sz) {
                maximalQueueLength = sz;
            }
            countMessage(msg);
        } catch (final InterruptedException e) {
            Globals.receiverLogger
                    .error("Internal error: "
                            + "Got interrupt waiting for receive message queue to drain",
                            e);
        }
    }

    /**
     * Returns <code>true</code> iff the message queue is empty.
     * 
     * @return <code>true</code> iff the message queue is empty.
     */
    public boolean isEmpty() {
        return q.isEmpty();
    }

    public void dump() {
        Globals.receiverLogger.trace("There are " + q.size()
                + " messages in the receive queue");
    }

    /**
     * Returns the maximal queue length achieved thus-far.
     * 
     * @return The current maximal queue size.
     */
    public int getMaximalQueueLength() {
        return maximalQueueLength;
    }

}
