package org.ow2.contrail.resource.vin.controller.tester;

class PhysicalMachineInfo extends VINNetworkInfo {

    private final String machineId;

    public PhysicalMachineInfo(final String machineId) {
        this.machineId = machineId;
    }

    @Override
    void destroy() {
        // FIXME: destroy the agent on the physical machine
    }

}