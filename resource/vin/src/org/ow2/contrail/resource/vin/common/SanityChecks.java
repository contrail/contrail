package org.ow2.contrail.resource.vin.common;

import ibis.util.RunProcess;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * A collection of checks that verify whether the execution environment is as it
 * should be.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class SanityChecks {

    /**
     * Given a directory file, throw an exception if the file does not exist, or
     * is not a directory.
     * 
     * @param dir
     *            The file to check.
     * @param friendlyName
     *            The name of the directory to use in any error messages.
     * @throws ConfigurationError
     *             Thrown if the given file does not exist or is not an ordinary
     *             file.
     */
    public static void ensureDirectoryExists(final File dir,
            final String friendlyName) throws ConfigurationError {
        if (!dir.exists()) {
            throw new ConfigurationError("The " + friendlyName + " directory "
                    + dir + " does not exist");
        }
        if (!dir.isDirectory()) {
            throw new ConfigurationError("The supposed " + friendlyName
                    + " directory " + dir + " is not a directory");
        }
        Globals.sanityLogger.debug("Verified: " + friendlyName + " directory '"
                + dir + "' exists");
    }

    /**
     * Given a file, throw an exception if the file does not exist, or is not an
     * ordinary file.
     * 
     * @param f
     *            The file to check.
     * @throws ConfigurationError
     *             Thrown if the given file does not exist or is not an ordinary
     *             file.
     */
    static void ensureFileExists(final File f) throws ConfigurationError {
        if (!f.exists()) {
            throw new ConfigurationError("The file " + f + " does not exist");
        }
        if (!f.isFile()) {
            throw new ConfigurationError("The supposed file " + f
                    + " is not a file");
        }
    }

    private static void ensureSpecialFileExists(final File f,
            final String friendlyName) throws ConfigurationError {
        if (!f.exists()) {
            throw new ConfigurationError("The " + friendlyName
                    + " special file " + f + " does not exist");
        }
        if (f.isFile() || f.isDirectory()) {
            throw new ConfigurationError("The supposed " + friendlyName
                    + " special file " + f + " is not a special file");
        }
        Globals.sanityLogger.debug("Verified: " + friendlyName
                + " special file '" + f + "' exists");
    }

    public static void ensureScriptIsSane(final File scriptsDir,
            final String scriptName) throws ConfigurationError {
        final File f = new File(scriptsDir, scriptName);
        if (!f.exists()) {
            throw new ConfigurationError("The script " + f + " does not exist");
        }
        if (!f.isFile()) {
            throw new ConfigurationError("The supposed script file " + f
                    + " is not a file");
        }
        if (!f.canExecute()) {
            throw new ConfigurationError("The supposed script " + f
                    + " is not executable");
        }
        Globals.sanityLogger.debug("Verified: script '" + f
                + "' exists and is executable");
    }

    private static void ensureConfigIsSane(final File configDir,
            final String configName) throws ConfigurationError {
        final File f = new File(configDir, configName);
        if (!f.exists()) {
            throw new ConfigurationError("The configuration file " + f
                    + " does not exist");
        }
        if (!f.isFile()) {
            throw new ConfigurationError("The supposed configuration file " + f
                    + " is not a file");
        }
        if (f.canExecute()) {
            throw new ConfigurationError("The supposed configuration file " + f
                    + " is executable");
        }
        Globals.sanityLogger.debug("Verified: script '" + f
                + "' exists and is not executable");
    }

    public static void ensureConfigsAreSane(final File configDir,
            final String... configNames) throws ConfigurationError {
        for (final String configName : configNames) {
            ensureConfigIsSane(configDir, configName);
        }
    }

    /**
     * Given a file and a matching pattern, throw an exception if the given file
     * does not exist, is not an ordinary file, cannot be read, or does not
     * match the specified pattern.
     * 
     * @param f
     *            The file to check.
     * @param pattern
     *            The pattern to match against.
     * @throws ConfigurationError
     *             Thrown if the file does not match the pattern.
     */
    public static void ensureFileContentMatches(final File f,
            final String pattern) throws ConfigurationError {
        final Pattern patternMatcher = Pattern.compile(pattern, Pattern.DOTALL
                | Pattern.MULTILINE);
        if (!f.exists()) {
            throw new ConfigurationError("The file " + f + " does not exist");
        }
        if (!f.isFile()) {
            throw new ConfigurationError("The supposed file " + f
                    + " is not a file");
        }
        String content;
        try {
            content = Utils.readFile(f);
        } catch (final IOException e) {
            throw new ConfigurationError(
                    "The file '" + f + "' is not readable", e);
        }
        if (!patternMatcher.matcher(content).matches()) {
            throw new ConfigurationError("The content of file '" + f + "' is '"
                    + content + "', which does not match '" + pattern + "'");
        }
    }

    private static final Pattern svmMatcher = Pattern.compile(".*svm.*",
            Pattern.DOTALL | Pattern.MULTILINE);
    private static final Pattern vmxMatcher = Pattern.compile(".*vmx.*",
            Pattern.DOTALL | Pattern.MULTILINE);

    private static boolean hardwareIsXenDom() {
        final File f = new File("/proc/xen/capabilities");
        return f.exists();
    }

    public static void ensureHardwareSupportsVirtualization()
            throws ConfigurationError {
        if (hardwareIsXenDom()) {
            return;
        }
        final File f = new File("/proc/cpuinfo");
        if (!f.exists()) {
            /*
             * We're not running on a Linux platform, or at least not a sane
             * one.
             */
            return;
        }
        if (!f.isFile()) {
            throw new ConfigurationError("The supposed file " + f
                    + " is not a file");
        }
        String content;
        try {
            content = Utils.readFile(f);
        } catch (final IOException e) {
            throw new ConfigurationError(
                    "The file '" + f + "' is not readable", e);
        }

        if (!svmMatcher.matcher(content).matches()
                && !vmxMatcher.matcher(content).matches()) {
            throw new ConfigurationError(
                    "The processor does not support virtualization");
        }

    }

    private static void ensureScriptOutputMatches(final File scriptsDir,
            final String script, final String pattern,
            final String... parameters) throws ConfigurationError {
        final RunProcess p = Utils
                .executeScript(scriptsDir, script, parameters);
        final Pattern patternMatcher = Pattern.compile(pattern, Pattern.DOTALL
                | Pattern.MULTILINE);
        final int x = p.getExitStatus();
        if (x != 0) {
            final File f = new File(scriptsDir, script);
            throw new ConfigurationError("Executing '" + f
                    + "' with parameters " + Arrays.deepToString(parameters)
                    + " results in exit code " + x);

        }
        final String out = new String(p.getStdout());
        if (!patternMatcher.matcher(out).matches()) {
            final File f = new File(scriptsDir, script);
            throw new ConfigurationError("Executing '" + f
                    + "' with parameters " + Arrays.deepToString(parameters)
                    + " results in output '" + out
                    + "', which does not match '" + pattern + "'");
        }
    }

    public static void ensureConfigOutputMatches(final File safeconfigPath,
            final File scriptsDir, final String script, final String pattern,
            final String... parameters) throws ConfigurationError {
        final RunProcess p = Utils.cleanlyRunConfig(safeconfigPath, false,
                scriptsDir, script, parameters);
        final int x = p.getExitStatus();
        if (x != 0) {
            final File f = new File(scriptsDir, script);
            throw new ConfigurationError("Running configuration '" + f
                    + "' with parameters " + Arrays.deepToString(parameters)
                    + " results in exit code " + x);

        }
        final String out = new String(p.getStdout());
        final Pattern patternMatcher = Pattern.compile(pattern, Pattern.DOTALL
                | Pattern.MULTILINE);
        if (!patternMatcher.matcher(out).matches()) {
            final File f = new File(scriptsDir, script);
            throw new ConfigurationError("Executing '" + f
                    + "' with parameters " + Arrays.deepToString(parameters)
                    + " results in output '" + out
                    + "', which does not match '" + pattern + "'");
        }
    }
}
