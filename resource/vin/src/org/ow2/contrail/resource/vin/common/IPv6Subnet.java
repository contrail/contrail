package org.ow2.contrail.resource.vin.common;

import java.io.Serializable;

import org.ow2.contrail.resource.vin.annotations.TestAPI;

/**
 * An IPv6 subnet.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class IPv6Subnet implements Subnet, Serializable {
    private static final long serialVersionUID = 1L;
    private final int maskBits;

    /** The first address in the range of this subnet. */
    final ArrayValue startValue;

    /** The last address in the range of this subnet. */
    final ArrayValue endValue;

    /**
     * Constructs a new IPv6 subset.
     * 
     * @param startValue
     *            The first address in the range of this subnet.
     * @param endValue
     *            The last address in the range of this subnet.
     * @param maskBits
     *            The number of bits in the bit mask of this subnet.
     */
    private IPv6Subnet(final ArrayValue startValue, final ArrayValue endValue,
            final int maskBits) {
        this.startValue = startValue;
        this.endValue = endValue;
        this.maskBits = maskBits;
    }

    IPv6Subnet(final ArrayValueRange range, final int alignmentBits) {
        this(range.startValue, range.endValue, alignmentBits);
    }

    /**
     * Returns the address that is reserved for bridging in this IPv6 network.
     * 
     * @return The bridge address.
     */
    @Override
    public String getBridgeAddressString() {
        return getBridgeAddress().makeIP4AddressString();
    }

    /**
     * @return The address of the bridge in this network.
     */
    private ArrayValue getBridgeAddress() {
        return startValue.increment();
    }

    /**
     * @return The address of the bridge in this network.
     */
    private ArrayValue getFreeRangeStartAddress() {
        return getBridgeAddress().increment();
    }

    /**
     * Returns the first freely available address in this subnet.
     * 
     * @return The address.
     */
    @Override
    public String getFreeRangeStartString() {
        return getFreeRangeStartAddress().makeIP4AddressString();
    }

    /**
     * Returns the highest freely available address in this subnet.
     * 
     * @return The address.
     */
    @Override
    public String getFreeRangeEndString() {
        final ArrayValue end = endValue.decrement();
        return end.makeIP4AddressString();
    }

    /**
     * Given the number of bits in the address that are significant, build a
     * mask that reflects this.
     * 
     * So: 24 -> 255.255.255.0
     * 
     * @param addressBits
     *            The number of significant address bits.
     * @return The net mask.
     */
    public static ArrayValue buildNetmask(final int addressBits) {
        final byte[] maskByteArray = new byte[4];
        maskByteArray[(addressBits - 1) / 8] = (byte) (1 << addressBits % 8);
        ArrayValue maskBytes = new ArrayValue(maskByteArray);
        maskBytes = maskBytes.decrement();
        maskBytes = maskBytes.invert();
        return maskBytes;
    }

    /**
     * Returns the net mask of this subnet.
     * 
     * @param addressBits
     *            The number of mask bits of this subnet.
     * 
     * @return The netmask of this network.
     */
    static String getNetmaskString(final int addressBits) {
        final ArrayValue maskBytes = buildNetmask(addressBits);
        return maskBytes.makeIP4AddressString();
    }

    @Override
    public String getNetmaskString() {
        return getNetmaskString(128 - maskBits);
    }

    @Override
    public String getSubnetAddressString() {
        return startValue.makeIP6AddressString() + "/" + (128 - maskBits);
    }

    @Override
    public String toString() {
        return "IPv6Subnet[" + startValue.makeIP6AddressString() + "..."
                + endValue.makeIP6AddressString() + " maskBits=" + maskBits
                + ']';
    }

    @Override
    public String getNetmaskBitsString() {
        return Integer.toString(128 - maskBits);
    }

    public static boolean isInSubnet(final byte[] a1bytes,
            final byte[] a2bytes, final int[] mask) {
        assert a1bytes.length == a2bytes.length;
        assert a1bytes.length == mask.length;
        for (int i = 0; i < a1bytes.length; i++) {
            final int b1 = a1bytes[i] & mask[i];
            final int b2 = a2bytes[i] & mask[i];
            if (b1 != b2) {
                return false;
            }
        }
        return true;
    }

    @TestAPI
    boolean overlaps(final IPv6Subnet other) {
        if (!ArrayValue.isLessOrEqual(this.endValue, other.startValue)) {
            return false;
        }
        if (!ArrayValue.isLessOrEqual(other.endValue, this.startValue)) {
            return false;
        }
        return true;
    }

    @Override
    public long getAddressCount() {
        return 1L << maskBits;
    }

    @Override
    public ArrayValue getStartValue() {
        return startValue;
    }

    @Override
    public ArrayValue getEndValue() {
        return endValue;
    }

    @Override
    public IPAddressRangePool createPool(final String friendlyName) {
        return new IPv6AddressRangePool(startValue, endValue, friendlyName);
    }
}
