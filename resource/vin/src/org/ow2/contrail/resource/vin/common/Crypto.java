package org.ow2.contrail.resource.vin.common;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.X509Certificate;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.ow2.contrail.resource.vin.agent.CATalker;

import eu.contrail.security.SecurityCommons;

public class Crypto {

    private static final String CSR_CIPHER = "SHA256withRSA";
    private static final String CIPHER = "RSA";

    /**
     * Create a self-signed X.509 Certificate
     * 
     * @param distinguishedName
     *            the X.509 Distinguished Name, eg "CN=Test, L=London, C=GB"
     * @param keypair
     *            the KeyPair
     * @param validDays
     *            how many days from now the Certificate is valid for
     * @param signingAlgorithmName
     *            the signing algorithm, eg "SHA1withRSA"
     * @return The generated certificate
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public static X509Certificate generateX509Certificate(
            final String distinguishedName, final KeyPair keypair,
            final int validDays, final String signingAlgorithmName)
            throws GeneralSecurityException, IOException {
        return null;
    }

    public static KeyPair generateKeyPair(final String algorithm) {
        final KeyPairGenerator keyGen;
        try {
            keyGen = KeyPairGenerator.getInstance(algorithm);
        } catch (final NoSuchAlgorithmException e) {
            throw new VinInternalError("Unknown crypto algorithm '" + algorithm
                    + "'", e);
        }
        return keyGen.generateKeyPair();
    }

    public static String generateCertificate(final CATalker caTalker,
            final String id) throws VinError {

        Security.addProvider(new BouncyCastleProvider());

        final SecurityCommons sc = new SecurityCommons();

        KeyPair kp = null;

        try {
            kp = sc.generateKeyPair(CIPHER, 2048);
        } catch (final NoSuchAlgorithmException ex) {
            throw new VinInternalError("Could not generate keypair with '"
                    + CIPHER + "' algorithm");
        }

        final String csrSpec;

        try {
            final PKCS10CertificationRequest request = new PKCS10CertificationRequest(
                    CSR_CIPHER, new X500Principal("CN=" + id), kp.getPublic(),
                    null, kp.getPrivate());
            try {
                csrSpec = sc.writeCSR(request);
            } catch (final IOException e) {
                throw new VinInternalError(
                        "Cannot generate a string from a CSR: "
                                + e.getLocalizedMessage(), e);
            }
            final String cert = caTalker.requestCertificateSignature(csrSpec);
            return cert;
        } catch (final NoSuchAlgorithmException ex) {
            throw new VinInternalError(
                    "Cannot create CSR, or obtain public key from request, with "
                            + CSR_CIPHER + " algorithm: "
                            + ex.getLocalizedMessage(), ex);
        } catch (final NoSuchProviderException ex) {
            throw new VinInternalError("Cannot use SecurityCommons provider: "
                    + ex.getLocalizedMessage(), ex);
        } catch (final InvalidKeyException ex) {
            throw new VinInternalError("Cannot use the keypair provided: "
                    + ex.getLocalizedMessage(), ex);
        } catch (final SignatureException ex) {
            throw new VinInternalError("Signature exception: "
                    + ex.getLocalizedMessage(), ex);
        }

    }

}
