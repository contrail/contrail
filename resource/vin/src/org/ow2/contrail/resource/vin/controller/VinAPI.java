package org.ow2.contrail.resource.vin.controller;

import java.util.Properties;

import org.ow2.contrail.resource.vin.annotations.API;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.VinError;

/**
 * The Vin API definition.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public interface VinAPI {
    /**
     * Returns the name of the Ibis pool used for this VIN.
     * 
     * @return The name of the Ibis pool used for this VIN.
     */
    @API
    String getIbisPoolName();

    /**
     * 
     * @return The address of the Ibis server of this VIN.
     */
    @API
    String getIbisServerAddress();

    /**
     * Given an encapsulation method and a set of properties, create a new
     * network. Initially there are no member nodes in the network.
     * 
     * FIXME: update documentation of supported encapsulations.
     * 
     * @param encapsulation
     *            The encapsulation mode to use. Currently, the only supported
     *            encapsulation mode is <code>"gre"</code>. There is incomplete
     *            and experimental support for other encapsulations:
     *            <code>"strongSwan"</code> StrongSwan encrypted encapsulation,
     *            and <code>"OpenVPN"</code> for OpenVPN encapsulation.
     * @param properties
     *            The properties of the network. Currently, the following
     *            properties are supported: if the <code>"debug"</code> flag is
     *            set, additional information is printed when nodes are added
     *            and deleted from a network. if the <code>"verbose"</code> flag
     *            is set, additional info is printed when agents are deployed,
     *            and if the <code>"show-execution"</code> flag is set, the
     *            execution of every command in the interface scripts is
     *            printed.
     * @return The identifier of the network. For the caller this is an opaque
     *         handle that is only used to identify the network.
     * @throws VinError
     *             Thrown if one of the parameters is incorrect.
     * @throws ConfigurationError
     *             Thrown if the configuration, as specified by the properties,
     *             is incorrect.
     * @throws OutOfEntriesError
     *             Thrown if one of the allocation pools is out of entries.
     */
    @API
    String addNetwork(String encapsulation, Properties properties)
            throws VinError, ConfigurationError, OutOfEntriesError;

    /**
     * Given a network identifier as returned by <code>addNetwork()</code>,
     * remove this network.
     * 
     * @param networkId
     *            The identifier of the network to remove.
     * @throws VinError
     *             Thrown if for some reason the network cannot be removed.
     */
    @API
    void removeNetwork(String networkId) throws VinError;

    /**
     * Given a set of properties, register a new virtual machine with these
     * properties.
     * 
     * @param properties
     *            The properties of the new VM. Currently no properties are
     *            supported.
     * 
     * @return The identifier of the virtual machine. For the caller this is an
     *         opaque handle that is only used to identify the virtual machine.
     * @throws VinError
     *             Thrown if for some reason the VM cannot be registered.
     */
    @API
    String registerVirtualMachine(Properties properties) throws VinError;

    /**
     * Given a set of properties, register a new physical machine with these
     * properties.
     * 
     * @param properties
     *            The properties of the new physical machine. Currently no
     *            properties are supported.
     * 
     * @return The identifier of the physical machine. For the caller this is an
     *         opaque handle that is only used to identify the physical network.
     * @throws VinError
     *             Thrown if for some reason the physical machine cannot be
     *             registered.
     */
    @API
    String registerPhysicalMachine(Properties properties) throws VinError;

    /**
     * Given a machine identifier as returned by
     * <code>registerVirtualMachine()</code> or
     * <code>registerPhysicalMachine()</code>, removes this machine from the
     * administration, and removes any network connections that were made to it.
     * 
     * @param machine
     *            The virtual machine to remove.
     */
    @API
    void removeMachine(String machine);

    /**
     * Given a network identifier as returned by
     * <code>createPrivateNetwork()</code> and a virtual machine identifier as
     * returned by <code>requestVirtualMachine()</code>, or a physical machine
     * identifier as returned by <code>requestPhysicalMachine()</code>, adds
     * that machine to the private network. Upon completion it is not guaranteed
     * that the machine is reachable over the private network, merely that the
     * system will make its best effort to connect the machine to the given
     * private network.
     * 
     * @param networkId
     *            The identifier of the private network to connect the machine
     *            to.
     * @param machineId
     *            The identifier of the virtual or physical machine to connect
     *            to the network.
     * @param properties
     *            The properties of this connection. Currently only a property
     *            with the name <code>name</code> is used.
     * @return The identifier of the connection. For the caller this is an
     *         opaque handle that is only used to identify the connection.
     * @throws VinError
     *             Thrown if there is an operational error in the VIN (e.g. an
     *             allocation pool is exhausted.
     * @throws ConfigurationError
     *             Thrown if one of the parameters is incorrect.
     */
    @API
    String addConnection(String networkId, String machineId,
            Properties properties) throws VinError, ConfigurationError;

    /**
     * Given a network identifier and a connection identifier, returns the IP
     * address assigned to that connection.
     * 
     * @param networkId
     *            The identifier of the network, as returned by addNetwork().
     * @param connectionId
     *            The identifier of the connection, as returned by
     *            addConnection().
     * @return The IP address of the connection.
     * @throws VinError
     */
    @API
    String getAddress(String networkId, String connectionId) throws VinError;

    /**
     * Given a connection identifier as returned by
     * <code>addPrivateMachineConnection()</code>, removes this connection.
     * 
     * @param networkId
     *            The identifier of the network that contains the connection to
     *            remove.
     * @param connectionId
     *            The identifier of the connection to remove.
     * @throws VinError
     *             Thrown if one of the parameters is incorrect.
     */
    @API
    void removeConnection(String networkId, String connectionId)
            throws VinError;

    /**
     * Stops this Vin instance.
     */
    @API
    void setStopped();

    /**
     * Stops this VIN instance.
     * 
     * @param reason
     *            A description of the reason for stopping this VIN instance.
     *            The content of this string does not influence the behavior of
     *            the VIN in any way, except that this string is used in logging
     *            texts as the reason for stopping.
     * @param isNormalTermination
     *            Indicates whether this VIN instance has terminated normally.
     *            The value of this boolean does not influence the behavior of
     *            the VIN in any way, except that the standard test environment
     *            of the VIN software will use it to determine whether a test
     *            has failed or succeeded.
     * 
     */
    @API
    void setStopped(String reason, boolean isNormalTermination);

    /**
     * Returns <code>true</code> iff this Vin instance has terminated normally.
     * 
     * @return <code>true</code> iff this Vin instance has terminated normally.
     */
    @API
    boolean getTerminatedNormally();
}
