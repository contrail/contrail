package org.ow2.contrail.resource.vin.controller.tester;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

import org.ow2.contrail.resource.vin.controller.Settings;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * The description of a private network.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class Network {
    final String encapsulation;

    /** The properties of this network. */
    final Properties properties;

    /** The machines to use in this network. */
    final ArrayList<String> machines;

    private Network(final String encapsulation, final Properties properties,
            final ArrayList<String> machines) {
        super();
        assert encapsulation != null;
        assert properties != null;
        this.encapsulation = encapsulation;
        this.properties = properties;
        this.machines = machines;
    }

    void addJSon(final ObjectMapper mapper, final ObjectNode node) {
        if (!properties.isEmpty()) {
            final ObjectNode pn = mapper.createObjectNode();
            final Set<String> keys = properties.stringPropertyNames();
            for (final String key : keys) {
                pn.put(key, properties.getProperty(key));
            }
            node.put("properties", pn);
        }
        if (machines != null) {
            final ArrayNode subtree = node.putArray("machines");
            for (final String e : machines) {
                subtree.add(e);
            }
        }
    }

    static Network extractJson(final JsonNode node) {
        String encapsulation;
        Properties properties;
        final ArrayList<String> ml = new ArrayList<String>();

        final JsonNode encapsulationNode = node.path("encapsulation");
        if (encapsulationNode == null) {
            encapsulation = Settings.DEFAULT_ENCAPSULATION;
        } else {
            encapsulation = encapsulationNode.asText();
            if (encapsulation == null) {
                encapsulation = Settings.DEFAULT_ENCAPSULATION;
            }
        }
        final JsonNode propertiesNode = node.path("properties");
        if (propertiesNode == null) {
            properties = new Properties();
        } else {
            properties = VinDescriptorParser.extractProperties(propertiesNode);
        }
        final JsonNode machinesNode = node.path("machines");
        if (machinesNode != null) {
            for (final JsonNode e : machinesNode) {
                final String c = e.asText();
                if (c != null) {
                    ml.add(c);
                }
            }
        }
        return new Network(encapsulation, properties, ml);
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Network)) {
            return false;
        }
        final Network that = (Network) obj;
        return machines.equals(that.machines);
    }

    @Override
    public int hashCode() {
        return machines.hashCode();
    }
}
