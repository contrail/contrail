package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

import org.ow2.contrail.resource.vin.annotations.API;
import org.ow2.contrail.resource.vin.annotations.Nullable;
import org.ow2.contrail.resource.vin.common.Defaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The command-line interface of the VIN agent.
 * 
 * @author Kees van Reeuwijk
 * 
 */
@API
public class VinAgent {
    private static final String KEY_IBIS_POOL_NAME = "ibis.pool.name";
    private static final String KEY_IBIS_SERVER_ADDRESS = "ibis.server.address";
    final static Logger logger = LoggerFactory.getLogger(VinAgent.class);

    private static void usage(final PrintStream stream) {
        stream.println("Usage: "
                + VinAgent.class.getName()
                + " <option> ... <option> <agent-id> <server-addr> <pool-name>\n");
        stream.println("Where <option> is:");
        stream.println(" -h\t\t\t\tShow this help text");
        stream.println(" -configDir=<dir>\t\tThe directory where the configuration files live");
        stream.println(" -libvirtId=<string>\t\tThe libvirt identifier of the VM that this agent is for");
        stream.println(" -safeconfigPath=<dir>\t\tThe directory where the safeconfig executable lives");
        stream.println(" -scriptDir=<dir>\t\tThe directory where the configuration scripts live");
        stream.println(" -networkStartScript=<path>\tThe path to a network start script");
        stream.println(" -networkStopScript=<path>\tThe path to a network stop script");
        stream.println(" -testAddress=<adddress>\tThe IP address to use for ssh-ing into the VM");
    }

    private static File extractDirFromArg(final String arg) {
        final int ix = arg.indexOf('=');
        if (ix < 0) {
            throw new Error("No '=' in argument '" + arg + '\'');
        }
        final String nm = arg.substring(ix + 1);
        if (nm.isEmpty()) {
            throw new Error("Empty name in argument '" + arg + '\'');
        }
        final File dir = new File(nm);
        if (!dir.exists()) {
            throw new Error("Directory '" + dir
                    + "' does not exist, (specified in argument '" + arg
                    + "\')");
        }
        if (!dir.isDirectory()) {
            throw new Error("Not a directory: '" + dir
                    + "' (specified in argument '" + arg + '\'');
        }
        return dir;
    }

    private static File extractExecFromArg(final String arg) {
        final int ix = arg.indexOf('=');
        if (ix < 0) {
            throw new Error("No '=' in argument '" + arg + '\'');
        }
        final String nm = arg.substring(ix + 1);
        if (nm.isEmpty()) {
            throw new Error("Empty name in argument '" + arg + '\'');
        }
        final File f = new File(nm);
        if (!f.exists()) {
            throw new Error("File '" + f
                    + "' does not exist, (specified in argument '" + arg
                    + "\')");
        }
        if (!f.isFile()) {
            throw new Error("Not a file: '" + f + "' (specified in argument '"
                    + arg + '\'');
        }
        return f;
    }

    private static InetAddress extractAddressFromArg(final String arg) {
        final int ix = arg.indexOf('=');
        if (ix < 0) {
            throw new Error("No '=' in argument '" + arg + '\'');
        }
        final String addr = arg.substring(ix + 1);
        if (addr.isEmpty()) {
            return null;
        }
        try {
            final InetAddress res = InetAddress.getByName(addr);
            return res;
        } catch (final UnknownHostException e) {
            throw new Error("Bad address in argument '" + arg + "'", e);
        }
    }

    private static String extractStringFromArg(final String arg) {
        final int ix = arg.indexOf('=');
        if (ix < 0) {
            throw new Error("No '=' in argument '" + arg + '\'');
        }
        final String addr = arg.substring(ix + 1);
        return addr;
    }

    private static class Parameters {
        @Nullable
        String agentId = null;
        private final File contrailDir = Defaults.CONTRAIL_DIR;
        public File scriptDir = new File(contrailDir, "interface-scripts");
        public File configDir = new File(contrailDir, "interface-configs");
        public File safeconfigPath = Defaults.SAFECONFIG_PATH;
        public ArrayList<File> startScripts = new ArrayList<File>();
        public ArrayList<File> stopScripts = new ArrayList<File>();
        public InetAddress testAddress = null;
        public String libvirtId = null;
        String serverAddress = null;
        String poolName = null;

        @SuppressWarnings("synthetic-access")
        private Parameters(final String[] args) {
            for (final String arg : args) {
                if (arg.equals("-h")) {
                    usage(System.out);
                    System.exit(0);
                } else if (arg.startsWith("-libvirtId=")) {
                    libvirtId = extractStringFromArg(arg);
                } else if (arg.startsWith("-testAddress=")) {
                    testAddress = extractAddressFromArg(arg);
                } else if (arg.startsWith("-scriptDir=")) {
                    scriptDir = extractDirFromArg(arg);
                } else if (arg.startsWith("-configDir=")) {
                    configDir = extractDirFromArg(arg);
                } else if (arg.startsWith("-safeconfigPath=")) {
                    safeconfigPath = extractDirFromArg(arg);
                } else if (arg.startsWith("-networkStartScript=")) {
                    final File script = extractExecFromArg(arg);
                    startScripts.add(script);
                } else if (arg.startsWith("-networkStopScript=")) {
                    final File script = extractExecFromArg(arg);
                    stopScripts.add(script);
                } else if (arg.startsWith("-")) {
                    System.err.println("Error: Unknown option '" + arg + "'");
                    usage(System.err);
                    System.err.println("Actual arguments: "
                            + Arrays.deepToString(args));
                    throw new Error("Bad command-line arguments");
                } else {
                    if (agentId == null) {
                        agentId = arg;
                    } else if (serverAddress == null) {
                        serverAddress = arg;
                    } else if (poolName == null) {
                        poolName = arg;
                    } else {
                        System.err.println("Error: superfluous argument '"
                                + arg + "'");
                        usage(System.err);
                        System.err.println("Actual arguments: "
                                + Arrays.deepToString(args));
                        throw new Error("Bad command-line arguments");
                    }
                }
            }
        }
    }

    /**
     * The command-line interface of the Vin agent.
     * 
     * @param args
     *            The command line arguments
     */
    @SuppressWarnings("synthetic-access")
    public static void main(final String[] args) {
        logger.debug("Command line: " + Arrays.deepToString(args));
        final Parameters parameters = new Parameters(args);

        try {
            if (parameters.agentId == null) {
                System.err.println("Error: no agentId specified");
                usage(System.err);
                System.err.println("Actual arguments: "
                        + Arrays.deepToString(args));
                throw new Error("Bad command-line arguments");
            }
            if (parameters.serverAddress == null) {
                if (System.getProperty(KEY_IBIS_SERVER_ADDRESS) == null) {
                    System.err
                            .println("Error: no Ibis server address specified");
                    usage(System.err);
                    System.err.println("Actual arguments: "
                            + Arrays.deepToString(args));
                    throw new Error("Bad command-line arguments");
                }
            } else {
                System.setProperty(KEY_IBIS_SERVER_ADDRESS,
                        parameters.serverAddress);
            }
            if (parameters.poolName == null) {
                if (System.getProperty(KEY_IBIS_POOL_NAME) == null) {
                    System.err.println("Error: no pool name specified");
                    usage(System.err);
                    System.err.println("Actual arguments: "
                            + Arrays.deepToString(args));
                    throw new Error("Bad command-line arguments");
                }
            } else {
                System.setProperty(KEY_IBIS_POOL_NAME, parameters.poolName);
            }

            // FIXME: better handling of libvirt vm id.
            final AgentEngine e = new AgentEngine(parameters.scriptDir,
                    parameters.configDir, parameters.safeconfigPath,
                    parameters.startScripts, parameters.stopScripts,
                    parameters.agentId, parameters.libvirtId,
                    parameters.testAddress);
            logger.debug("Agent parameters: configDir=" + parameters.configDir
                    + " safeconfigPath=" + parameters.safeconfigPath
                    + " agentId=" + parameters.agentId + " libvirtId="
                    + parameters.libvirtId);
            e.start();
            e.join();
        } catch (final Exception e) {
            System.err.println("Agent failed: " + e.getLocalizedMessage());
            logger.error("Agent failed", e);
            System.exit(1);
        }
    }
}
