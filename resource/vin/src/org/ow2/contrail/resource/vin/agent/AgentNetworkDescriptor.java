package org.ow2.contrail.resource.vin.agent;

import ibis.util.RunProcess;

import java.io.File;
import java.net.InetAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Properties;

import org.ow2.contrail.resource.vin.annotations.Nullable;
import org.ow2.contrail.resource.vin.common.ArrayValue;
import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.EncapsulationType;
import org.ow2.contrail.resource.vin.common.GlobalGlobals;
import org.ow2.contrail.resource.vin.common.LocalNetworkMembershipReadyMessage;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.Subnet;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Relevant information for a network: type, name, and encryption key, and such.
 *
 * @author Kees van Reeuwijk
 * @author Teodor Crivat
 *
 */
class AgentNetworkDescriptor {
    private final String name;
    final String domain;
    final String networkId;
    private final AgentNetworkAdministration networkAdmin;
    private final NetworkHandler networkHandler;
    private final ArrayList<AgentConnectionDescriptor> connections = new ArrayList<AgentConnectionDescriptor>();
    private static final Logger logger = LoggerFactory
            .getLogger(AgentNetworkDescriptor.class);
    private final CATalker caTalker;
    final Subnet subnet;

    /**
     * The local connection.
     */
    private @Nullable
    AgentConnectionDescriptor localConnection = null;
    private final ArrayValueRange macAddresses;

    /**
     * The lockfile that advertises that we already created the network. Remains
     * null if we did not create the network, and therefore that we don't have
     * to clean up.
     */
    private File lockfile = null;
    private final Properties properties;
    private StrongSwanDaemonTalker strongSwanDaemonTalker = null;

    AgentNetworkDescriptor(final AgentNetworkAdministration networkAdmin,
            final Properties properties, final EncapsulationType type,
            final String networkId, final Subnet subnet,
            final ArrayValueRange macAddresses, final URI caServer)
            throws ConfigurationError {
        super();
        assert networkId != null;
        this.networkAdmin = networkAdmin;
        this.networkId = networkId;
        this.subnet = subnet;
        this.macAddresses = macAddresses;
        this.properties = properties;
        if (caServer == null) {
            this.caTalker = null;
        } else {
            this.caTalker = new CATalker(caServer, networkId);
        }
        name = properties.getProperty(PropertyNames.NAME, networkId);
        domain = properties.getProperty(PropertyNames.DOMAIN);
        switch (type) {
        case StrongSwan: {
            if (strongSwanDaemonTalker == null) {
                strongSwanDaemonTalker = new StrongSwanDaemonTalker();
            }
            switch (networkAdmin.context) {
            case CLIENT:
                networkHandler = new StrongSwanGuestNetworkHandler(
                        strongSwanDaemonTalker, networkId);
                break;

            case PHYSICAL_MACHINE:
                networkHandler = new StrongSwanPhysicalNetworkHandler(
                        strongSwanDaemonTalker, networkId,
                        properties, networkAdmin.configDir,
                        networkAdmin.safeconfigPath);
                break;

            case HOST:
                networkHandler = new StrongSwanHostNetworkHandler(
                        strongSwanDaemonTalker, networkId,
                        properties, networkAdmin.configDir,
                        networkAdmin.safeconfigPath, macAddresses);
                break;

            default:
                throw new VinInternalError("unknown context type "
                        + networkAdmin.context);
            }
            break;
        }

        case GRE: {
            final TunnelPool tunnelPool = networkAdmin.getIproute2Pool();
            switch (networkAdmin.context) {
            case CLIENT:
                networkHandler = new Iproute2GuestNetworkHandler(tunnelPool,
                        networkAdmin.scriptDir);
                break;

            case PHYSICAL_MACHINE:
                networkHandler = new Iproute2PhysicalNetworkHandler(tunnelPool,
                        properties, networkAdmin.configDir,
                        networkAdmin.safeconfigPath);
                break;

            case HOST:
                networkHandler = new Iproute2HostNetworkHandler(tunnelPool,
                        properties, networkAdmin.configDir,
                        networkAdmin.safeconfigPath, macAddresses);
                break;

            default:
                throw new VinInternalError("unknown context type "
                        + networkAdmin.context);
            }
            break;
        }

        case OpenVPN:
            switch (networkAdmin.context) {
            case CLIENT:
                networkHandler = new OpenVPNGuestNetworkHandler(properties,
                        networkAdmin.configDir);
                break;

            case HOST: {
                networkHandler = new OpenVPNHostNetworkHandler(properties,
                        networkAdmin.configDir, networkAdmin.safeconfigPath,
                        macAddresses);
                break;
            }

            case PHYSICAL_MACHINE:
                throw new VinInternalError(
                        "Physical machine context not supported for OpenVPN");

            default:
                throw new VinInternalError("unknown context type "
                        + networkAdmin.context);
            }
            break;

        default:
            throw new VinInternalError("unknown network type " + type);
        }
        GlobalGlobals.demoLogger.info("AGENT: do sanity checks for "
                + networkHandler.getFriendlyName());
        networkHandler.doSanityChecks();
    }

    private static final int searchMemberByConnectionId(
            final ArrayList<AgentConnectionDescriptor> l,
            final String connectionId) {
        for (int i = 0; i < l.size(); i++) {
            final AgentConnectionDescriptor m = l.get(i);
            if (m.connectionId.equals(connectionId)) {
                return i;
            }
        }
        return -1;
    }

    private void testConnection(final AgentEngine engine,
            final AgentConnectionDescriptor c) throws VinError {
        {
            final String config = networkAdmin.properties
                    .getProperty(PropertyNames.TESTCONFIG);
            if (config != null) {
                final int delay = Utils.getIntProperty(networkAdmin.properties,
                        PropertyNames.TESTDELAY,
                        "The delay before executing the test");
                logger.debug("There is a test configuration file specified: file="
                        + config + " delay=" + delay + " connection=" + c);
                engine.runAConfigTest(config, delay, c.getVINAddressString());
            }
        }
        {
            final String script = networkAdmin.properties
                    .getProperty(PropertyNames.TESTSCRIPT);
            if (script != null) {
                logger.debug("There is a test script specified: script="
                        + script);
                final int delay = Utils.getIntProperty(networkAdmin.properties,
                        PropertyNames.TESTDELAY,
                        "The delay before executing the test");
                engine.runAScriptTest(script, delay, c.getVINAddressString());
            }
        }
    }

    private void runNotificationScripts(final ArrayList<File> scripts) {
        for (final File script : scripts) {
            final RunProcess p = Utils.cleanlyExecuteCommand(script, networkId);
        }
    }

    private void runNetworkStartNotificationScripts() {
        final ArrayList<File> scripts = networkAdmin.getNetworkStartScripts();
        runNotificationScripts(scripts);
    }

    private void runNetworkStopNotificationScripts() {
        final ArrayList<File> scripts = networkAdmin.getNetworkStopScripts();
        runNotificationScripts(scripts);
    }

    private void createConnection(final AgentEngine engine,
            final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote, final String info)
            throws ConfigurationError, VinError {
        logger.debug("Creating connection to " + remote);
        GlobalGlobals.demoLogger.info("AGENT: create connection to VM "
                + remote.vmId + " for " + networkHandler.getFriendlyName()
                + " network " + networkId + ' ' + info);
        networkHandler.createConnection(local, remote);
        testConnection(engine, remote);
    }

    private static int searchConnection(
            final ArrayList<AgentConnectionDescriptor> connections,
            final int ordinal) {
        for (int i = 0; i < connections.size(); i++) {
            final AgentConnectionDescriptor c1 = connections.get(i);
            if (c1.ordinal == ordinal) {
                return i;
            }
        }
        return -1;
    }

    void registerConnection(final AgentEngine engine, final int ordinal,
            final String virtualMachine, final String connectionId,
            final InetAddress address, final InetAddress hostAddress,
            final String localHostId, final boolean ready,
            final Properties connectionProperties) throws ConfigurationError,
            VinError {
        final AgentConnectionDescriptor c = new AgentConnectionDescriptor(this,
                ordinal, connectionId, virtualMachine, address, hostAddress,
                ready, connectionProperties);
        final int ix = searchConnection(connections, ordinal);
        boolean update;
        if (ix >= 0) {
            connections.set(ix, c);
            logger.debug("Updating connection descriptor " + c);
            update = true;
        } else {
            connections.add(c);
            logger.debug("Adding connection descriptor " + c);
            update = false;
        }
        if (virtualMachine.equals(localHostId)) {
            if (!update) {
                // New: the local connection.
                localConnection = c;
                logger.debug("Creating local connection " + c);
                GlobalGlobals.demoLogger
                        .info("AGENT: creating local connection to: "
                                + c.getVINAddressString());
                GlobalGlobals.demoLogger.info("AGENT: creating local "
                        + networkHandler.getFriendlyName()
                        + " connection for network " + networkId);
                networkHandler.createLocalConnection(networkAdmin, c);
                if (!c.ready) {
                    // The central controller needs to be told we're ready.
                    final LocalNetworkMembershipReadyMessage readyMsg = new LocalNetworkMembershipReadyMessage(
                            networkId, connectionId);
                    engine.sendToController(readyMsg);
                }
                for (final AgentConnectionDescriptor cOld : connections) {
                    logger.debug("Creating deferred connection to " + cOld);
                    if (cOld != localConnection && cOld.ready) {
                        createConnection(engine, localConnection, cOld,
                                "(catching up)");
                    }
                }
                runNetworkStartNotificationScripts();
            }
        } else {
            // Not a connection to this machine.
            if (localConnection != null) {
                if (c.ready) {
                    createConnection(engine, localConnection, c,
                            "(new connection)");
                }
            } else {
                /*
                 * We are not connected to this VIN network. If later we are, we
                 * will catch up, if we are never connected, nothing happens.
                 */
                logger.debug("Local connection not yet known, deferring connection creation for "
                        + c);
                GlobalGlobals.demoLogger
                        .info("AGENT: local connection to network " + networkId
                                + " uncertain, deferring connection to VM "
                                + c.vmId);
            }
        }
    }

    private void removeConnection(final String localHostId,
            final AgentConnectionDescriptor c) throws ConfigurationError {
        if (c.vmId.equals(localHostId)) {
            logger.debug("Destroying local connection " + c);
            GlobalGlobals.demoLogger
                    .info("AGENT: destroy local connection for "
                            + networkHandler.getFriendlyName() + " network "
                            + c.network.networkId);
            try {
                networkHandler.removeLocalConnection(networkAdmin,
                        localConnection);
                GlobalGlobals.demoLogger
                        .info("AGENT: destroyed local connection to: "
                                + c.getVINAddressString());
            } catch (final VinError e) {
                logger.error("Could not remove local connection to: "
                        + c.getVINAddressString(), e);
            }
            runNetworkStopNotificationScripts();
        } else {
            logger.debug("Destroying connection to " + c);
            if (localConnection != null) {
                GlobalGlobals.demoLogger
                        .info("AGENT: destroying connection for "
                                + networkHandler.getFriendlyName()
                                + " network " + c.network.networkId + " to VM "
                                + c.vmId);
                try {
                    networkHandler.removeConnection(localConnection, c);
                    GlobalGlobals.demoLogger
                            .info("AGENT: destroyed connection to: "
                                    + c.getVINAddressString());
                } catch (final VinError e) {
                    logger.error("Could not remove connection to: "
                            + c.getVINAddressString(), e);
                }
            }
        }
    }

    void removeConnection(final String connectionId, final String localHostId)
            throws ConfigurationError {
        logger.debug("Removing configuration for networkId=" + networkId + "("
                + name + ") connectionId=" + connectionId + " localHostId="
                + localHostId);
        final int ix = searchMemberByConnectionId(connections, connectionId);
        if (ix < 0) {
            throw new VinInternalError("Unknown connection id '" + connectionId
                    + "' in network " + networkId);
        }
        final AgentConnectionDescriptor c = connections.remove(ix);
        removeConnection(localHostId, c);
    }

    void ensureConnectionIdExists(final String connectionId) {
        final int ix = searchMemberByConnectionId(connections, connectionId);
        if (ix < 0) {
            throw new VinInternalError("Unknown connection id '" + connectionId
                    + "' in network " + networkId);
        }
    }

    void createNetwork() throws ConfigurationError {
        logger.debug("Creating network for networkId=" + networkId + " ("
                + name + ")");
        GlobalGlobals.demoLogger.info("AGENT: create "
                + networkHandler.getFriendlyName() + " network " + networkId);
        networkHandler.createNetwork(this);
    }

    void removeNetwork() throws ConfigurationError {
        logger.debug("Removing network with networkId=" + networkId + " ("
                + name + ")");
        if (localConnection != null) {
            // We are participating in the network
            final String localHostId = localConnection.vmId;
            for (final AgentConnectionDescriptor c : connections) {
                if (c.vmId.equals(localHostId))
                    continue;
                removeConnection(localHostId, c);
            }
            // Remove the local connection last
            removeConnection(localHostId, localConnection);
        }
        GlobalGlobals.demoLogger.info("AGENT: remove "
                + networkHandler.getFriendlyName() + " network " + networkId);
        networkHandler.removeNetwork(this);
    }

    public String getBridgeIPAddress() {
        return subnet.getBridgeAddressString();
    }

    public String getMacAddress(final InetAddress address) {
        final ArrayValue addr = new ArrayValue(address.getAddress());
        final long offset = subnet.getStartValue().offsetOf(addr);
        final ArrayValue val = macAddresses.startValue.addOffset(offset);
        logger.debug("getMacAddress(): addr=" + addr + " offset=" + offset
                + " val=" + val);
        return val.buildBuildMACAddressString();
    }

    public String getNetmask() {
        return subnet.getNetmaskString();
    }

    void setLockfile(final File f) {
        this.lockfile = f;
    }

    File getLockfile() {
        return this.lockfile;
    }

    public String getDomain() {
        return properties.getProperty(PropertyNames.DOMAIN);
    }

    CATalker getCATalker() {
        return caTalker;
    }

    String getSubnetAddressString() {
        return subnet.getSubnetAddressString();
    }
}
