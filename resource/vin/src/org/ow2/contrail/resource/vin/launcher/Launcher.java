package org.ow2.contrail.resource.vin.launcher;

import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The command-line interface of the VIN launcher.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class Launcher {
    private final static Logger logger = LoggerFactory
            .getLogger(Launcher.class);

    static String extractStringFromArg(final String arg) {
        final int ix = arg.indexOf('=');
        if (ix < 0) {
            throw new Error("No '=' in argument '" + arg + '\'');
        }
        final String str = arg.substring(ix + 1);
        if (str.isEmpty()) {
            throw new Error("Empty string in argument '" + arg + '\'');
        }
        return str;
    }

    static int extractIntFromArg(final String arg) {
        final int ix = arg.indexOf('=');
        if (ix < 0) {
            throw new Error("No '=' in argument '" + arg + '\'');
        }
        final String str = arg.substring(ix + 1);
        if (str.isEmpty()) {
            throw new Error("Empty string in argument '" + arg + '\'');
        }
        int res;
        try {
            res = Integer.valueOf(str);
        } catch (final NumberFormatException x) {
            throw new Error("Malformed number '" + str + " in argument '" + arg
                    + '\'');
        }
        return res;
    }

    static void usage(final PrintStream stream) {
        stream.println("Usage: " + Launcher.class.getName()
                + " <option> ... <option>\n");
        stream.println("Where <option> is:");
        stream.println(" -h\t\tShow this help text");
        stream.println(" -verbose\tBe more verbose in execution");
        stream.println(" -rpcserver\tRun a JsonRpc server to accept configuration agent launch requests");
        stream.println(" -rpcPort=<number>\tThe server listens on the given port number (default: "
                + Settings.RPC_PORT_NUMBER + ")");
        stream.println("\t\t\tImplies -rpcserver");
        stream.println(" -launcher\tRun as an agent launcher (requires -hostid option)");
        stream.println(" -lauchCommand=<path>\tSpecify the path to the command to launch an agent");
        stream.println(" -hostid=<string>\tThe identifier of the host");
        stream.println("\t\t\tImplies -launcher");
        stream.println(" -hub\t\tRun an Ibis hub (the JsonRpc server always runs an Ibis hub)");
    }

    static File extractFileFromArg(final String arg) {
        final int ix = arg.indexOf('=');
        if (ix < 0) {
            throw new Error("No '=' in argument '" + arg + '\'');
        }
        final String nm = arg.substring(ix + 1);
        if (nm.isEmpty()) {
            throw new Error("Empty name in argument '" + arg + '\'');
        }
        final File file = new File(nm);
        if (!file.exists()) {
            throw new Error("File '" + file
                    + "' does not exist, (specified in argument '" + arg
                    + "\')");
        }
        if (!file.isFile()) {
            throw new Error("Not a file: '" + file
                    + "' (specified in argument '" + arg + '\'');
        }
        return file;
    }

    /**
     * The command-line interface of the launcher infrastructure.
     * 
     * @param args
     *            The command line arguments
     */
    public static void main(final String[] args) {
        logger.debug("Command line: " + Arrays.deepToString(args));
        final Parameters parameters = new Parameters(args);

        try {
            if (parameters.runLauncher && parameters.hostId == null) {
                System.err.println("Error: no hostId specified");
                usage(System.err);
                System.err.println("Actual arguments: "
                        + Arrays.deepToString(args));
                throw new Error("Bad command-line arguments");
            }
            // When we run a RPC server, also run an Ibis server.
            final LauncherEngine e = new LauncherEngine(
                    parameters.runRPCServer, parameters.runHub,
                    parameters.ibisPortNumber, parameters.launchCommand,
                    parameters.hostId, parameters.verbose);
            logger.debug("Launcher parameters: runRPCServer="
                    + parameters.runRPCServer + " runLauncher="
                    + parameters.runLauncher + " hostId=" + parameters.hostId
                    + " rpcPortNumber=" + parameters.rpcPortNumber);
            e.start();
            if (parameters.runRPCServer) {
                final RpcServer server = new RpcServer(e,
                        parameters.rpcPortNumber, parameters.verbose);
                server.start();
            }
            e.join();
        } catch (final Exception e) {
            System.err.println("Agent failed: " + e.getLocalizedMessage());
            logger.error("Agent failed", e);
            System.exit(1);
        }
    }
}
