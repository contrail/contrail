package org.ow2.contrail.resource.vin.controller.impl;

import java.util.Arrays;

import org.ow2.contrail.resource.vin.common.OutOfEntriesError;

/**
 * Maintains a pool to allocate integers from.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class IntegerPool {
    private final String name;
    private final int startValue;
    private final int endValue;
    private int nextValue;
    private int recycleBin[] = new int[10];
    private int recycledEntries = 0;

    IntegerPool(final String name, final int startValue, final int endValue) {
        this.name = name;
        this.startValue = startValue;
        this.endValue = endValue;
        this.nextValue = startValue;
    }

    public synchronized int getNextValue() throws OutOfEntriesError {
        if (recycledEntries > 0) {
            recycledEntries--;
            return recycleBin[recycledEntries];
        }
        if (nextValue > endValue) {
            throw new OutOfEntriesError(name + " exhausted");
        }
        return nextValue++;
    }

    private static int compare(final int a, final int b) {
        if (a > b) {
            return 1;
        }
        if (a < b) {
            return -1;
        }
        return 0;
    }

    private static int findInsertionPosition(final int arr[], final int sz,
            final int val) {
        if (sz == 0) {
            return 0;
        }
        int l = 0;
        int h = sz;
        while (l + 1 < h) {
            final int mid = (l + h) / 2;
            final int c = compare(arr[mid], val);
            if (c > 0) {
                // Value is smaller than the mid-array value.
                h = mid;
            } else {
                l = mid;
            }
        }
        final int c = compare(arr[l], val);
        return c > 0 ? l : l + 1;
    }

    /**
     * Keep a sorted list of entries, by inserting new entries at the right
     * position.
     * 
     * @param val
     */
    private void addEntryToRecyclebin(final int val) {
        final int pos = findInsertionPosition(recycleBin, recycledEntries, val);
        int ix = recycledEntries;
        while (ix > pos) {
            recycleBin[ix] = recycleBin[ix - 1];
            ix--;
        }
        recycleBin[pos] = val;
        recycledEntries++;
    }

    /**
     * Given an address, add it to the pool of available values.
     * 
     * @param val
     *            The value to return.
     */
    public synchronized void recycleValue(final int val) {
        if (val + 1 == nextValue) {
            nextValue = val;
            /*
             * Now try to reduce the size of the recycle bin by decreasing
             * nextValue.
             */
            while (recycledEntries > 0) {
                final int ix = recycledEntries - 1;
                if (recycleBin[ix] + 1 != nextValue) {
                    // We cannot absorb this value.
                    break;
                }
                // Absorb this recycle bin startValue in nextValue
                nextValue--;
                recycledEntries--;
            }
            return;
        }
        if (recycledEntries >= recycleBin.length) {
            recycleBin = Arrays.copyOf(recycleBin, 2 * recycleBin.length);
        }
        addEntryToRecyclebin(val);
    }

    /**
     * Return <code>true</code> iff there are no outstanding resources in this
     * pool.
     * 
     * @return <code>true</code> iff there are no outstanding resources.
     */
    synchronized boolean isInPristineState() {
        final boolean res = recycledEntries == 0 && nextValue == startValue;
        if (!res) {
            final StringBuilder el = new StringBuilder();
            el.append('[');
            for (int i = 0; i < recycledEntries; i++) {
                if (i != 0) {
                    el.append(',');
                }
                el.append(recycleBin[i]);
            }
            el.append(']');
            System.err
                    .println("IntegerPool: not in pristine state: startValue="
                            + startValue + " nextValue=" + nextValue
                            + " recycleBin=" + el);
        }
        return res;
    }

}
