package org.ow2.contrail.resource.vin.agent;

import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.VinError;

interface NetworkHandler {
    /**
     * Run any sanity checks we can do for this type of network. Throw a
     * ConfigurationError if there is a problem.
     *
     * @throws ConfigurationError
     *             Thrown if the system configuration for this type of network
     *             is incorrect.
     */
    void doSanityChecks() throws ConfigurationError;

    /**
     * Create a new network with the given settings.
     *
     * @param network
     *            The network to create.
     * @throws ConfigurationError
     *             Thrown if there is a problem in the system that prevents us
     *             from creating the network.
     */
    void createNetwork(AgentNetworkDescriptor network)
            throws ConfigurationError;

    /**
     * Remove the network with the given settings.
     *
     * @param network
     *            The network to destroy.
     * @throws ConfigurationError
     *             Thrown if there is a problem in the system that prevents us
     *             from destroying the network.
     */
    void removeNetwork(AgentNetworkDescriptor network)
            throws ConfigurationError;

    /**
     * Create the local connection in a network.
     *
     * @param networkAdmin
     *            The network administration this connection is part of.
     * @param localConnection
     *            The connection to create, it is guaranteed to describe a
     *            connection to the local machine.
     * @throws ConfigurationError
     *             Thrown if there is a problem in the system that prevents us
     *             from creating the connection.
     * @throws VinError
     *             Thrown if something bad happens.
     */
    void createLocalConnection(final AgentNetworkAdministration networkAdmin,
            AgentConnectionDescriptor localConnection)
            throws ConfigurationError, VinError;

    /**
     * Remove the local connection in a network.
     *
     * @param networkAdmin
     *            The network administration this connection is part of.
     * @param localConnection
     *            The connection to destroy. It is guaranteed to describe a
     *            connection to the local machine.
     * @throws ConfigurationError
     *             Thrown if there is a problem in the system that prevents us
     *             from removing the connection.
     * @throws VinError
     *             Thrown if something bad happens.
     */
    void removeLocalConnection(final AgentNetworkAdministration networkAdmin,
            AgentConnectionDescriptor localConnection)
            throws ConfigurationError, VinError;

    /**
     * Create a remote connection in a network.
     *
     * @param local
     *            The local side of the connection to create.
     * @param remote
     *            The remote side of the connection to create. It is guaranteed
     *            to describe a connection NOT local to this machine.
     * @throws ConfigurationError
     *             Thrown if there is a problem in the system that prevents us
     *             from creating the connection.
     * @throws VinError
     *             Thrown if there is a problem during the creation of the
     *             connection.
     */
    void createConnection(AgentConnectionDescriptor local,
            AgentConnectionDescriptor remote) throws ConfigurationError,
            VinError;

    /**
     * Remove a remote connection in a network.
     *
     * @param local
     *            The local side of the connection to destroy.
     * @param remote
     *            The remote side of the connection to destroy. It is guaranteed
     *            to describe a connection NOT local to this machine.
     * @throws ConfigurationError
     *             Thrown if there is a problem in the system that prevents us
     *             from removing the connection.
     * @throws VinError
     *             Thrown if there is a problem during the creation of the
     *             connection.
     */
    void removeConnection(AgentConnectionDescriptor local,
            AgentConnectionDescriptor remote) throws ConfigurationError,
            VinError;

    /**
     * Return a readable name for this particular type of network.
     *
     * @return The name of the network type.
     */
    String getFriendlyName();

}
