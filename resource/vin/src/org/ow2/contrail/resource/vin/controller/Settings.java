package org.ow2.contrail.resource.vin.controller;

/**
 * Some default configuration settings.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class Settings {
    public static final String DEFAULT_ENCAPSULATION = "strongswan";
}
