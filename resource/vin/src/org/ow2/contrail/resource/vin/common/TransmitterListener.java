package org.ow2.contrail.resource.vin.common;

import ibis.ipl.Ibis;
import ibis.ipl.IbisIdentifier;

/**
 * The interface of a listener to the transmitter state.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public interface TransmitterListener {
    // FIXME: do we need this?
    /**
     * Get the local Ibis of this node.
     * 
     * @return The local ibis of this node.
     */
    Ibis getLocalIbis();

    /**
     * Tell our listener that the given node does not listen to message
     * properly, and may be dead.
     * 
     * @param node
     *            The node that is suspect.
     */
    void setSuspect(IbisIdentifier node);

    /**
     * Wake the transmitter listener, something interesting may have happened.
     */
    void wakeTransmitterListenerThread();
}
