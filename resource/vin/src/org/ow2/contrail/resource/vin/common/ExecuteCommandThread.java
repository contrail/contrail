package org.ow2.contrail.resource.vin.common;

import ibis.util.RunProcess;

public class ExecuteCommandThread extends Thread {

    private final String command;
    private final String[] parameters;
    private final ExecuteCommandListener listener;

    public ExecuteCommandThread(final ExecuteCommandListener listener,
            final String command, final String... parameters) {
        this.listener = listener;
        this.command = command;
        this.parameters = parameters;
    }

    @Override
    public void run() {
        final RunProcess p = Utils.executeCommand(command, parameters);
        listener.onCommandCompleted(p);
    }
}
