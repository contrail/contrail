package org.ow2.contrail.resource.vin.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Globals {
    /** The name of the receive port of a node. */
    static final String receivePortName = "receivePort";

    final static Logger receiverLogger = LoggerFactory
            .getLogger("org.ow2.contrail.resource.vin.common.receiver");

    final static Logger externalExecutionLogger = LoggerFactory
            .getLogger("org.ow2.contrail.resource.vin.common.externalExecution");

    final static Logger transmitterLogger = LoggerFactory
            .getLogger("org.ow2.contrail.resource.vin.common.transmitter");

    final static Logger sanityLogger = LoggerFactory
            .getLogger("org.ow2.contrail.resource.vin.common.sanitycheck");

}
