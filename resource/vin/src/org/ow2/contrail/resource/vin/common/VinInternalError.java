package org.ow2.contrail.resource.vin.common;

/**
 * Internal error thrown by the Vin framework.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class VinInternalError extends Error {
    private static final String PREFIX = "INTERNAL ERROR: ";
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with the given error message.
     * 
     * @param msg
     *            The error message of this exception.
     */
    public VinInternalError(final String msg) {
        super(PREFIX + msg);
    }

    /**
     * Constructs a new exception with the given error message and exception.
     * 
     * @param e
     *            The exception related to this internal error.
     */
    VinInternalError(final Throwable e) {
        super(PREFIX, e);
    }

    /**
     * Constructs a new exception with the given error message and exception.
     * 
     * @param msg
     *            The error message of this exception.
     * @param e
     *            The exception related to this internal error.
     */
    public VinInternalError(final String msg, final Throwable e) {
        super(PREFIX + msg, e);
    }

}
