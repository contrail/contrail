package org.ow2.contrail.resource.vin.controller.impl;

import ibis.ipl.Ibis;
import ibis.ipl.IbisCapabilities;
import ibis.ipl.IbisFactory;
import ibis.ipl.IbisIdentifier;
import ibis.ipl.Registry;
import ibis.ipl.RegistryEventHandler;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.ow2.contrail.resource.vin.common.AgentContextType;
import org.ow2.contrail.resource.vin.common.AgentErrorMessage;
import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.ConfigurationErrorMessage;
import org.ow2.contrail.resource.vin.common.EncapsulationType;
import org.ow2.contrail.resource.vin.common.Flag;
import org.ow2.contrail.resource.vin.common.GlobalGlobals;
import org.ow2.contrail.resource.vin.common.IbisServer;
import org.ow2.contrail.resource.vin.common.LocalNetworkMembershipReadyMessage;
import org.ow2.contrail.resource.vin.common.Message;
import org.ow2.contrail.resource.vin.common.NetworkConnectionRemovalMessage;
import org.ow2.contrail.resource.vin.common.NetworkDescriptionMessage;
import org.ow2.contrail.resource.vin.common.NetworkMembershipMessage;
import org.ow2.contrail.resource.vin.common.NetworkRemovalMessage;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.PacketSendPort;
import org.ow2.contrail.resource.vin.common.PacketUpcallReceivePort;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.ReceivedMessageQueue;
import org.ow2.contrail.resource.vin.common.RegisterAgentMessage;
import org.ow2.contrail.resource.vin.common.StopAgentMessage;
import org.ow2.contrail.resource.vin.common.Subnet;
import org.ow2.contrail.resource.vin.common.Transmitter;
import org.ow2.contrail.resource.vin.common.TransmitterListener;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VMDescriptionMessage;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.ow2.contrail.resource.vin.controller.VinAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class ControllerEngine extends Thread implements RegistryEventHandler,
        VinAPI, TransmitterListener {
    private static final IbisCapabilities ibisCapabilities = new IbisCapabilities(
            IbisCapabilities.MEMBERSHIP_UNRELIABLE,
            IbisCapabilities.ELECTIONS_STRICT);
    private final ReceivedMessageQueue receivedMessageQueue = new ReceivedMessageQueue(
            ImplSettings.MAXIMAL_RECEIVED_MESSAGE_QUEUE_LENGTH);
    private final Transmitter transmitter;
    private final ControllerNetworkAdministration networkAdministration;
    private final ConcurrentLinkedQueue<IbisIdentifier> deletedNodes = new ConcurrentLinkedQueue<IbisIdentifier>();
    private final ConcurrentLinkedQueue<IbisIdentifier> newAgents = new ConcurrentLinkedQueue<IbisIdentifier>();
    private final PacketUpcallReceivePort receivePort;
    private final boolean isMaster;
    private final IbisIdentifier controllerIdentifier;
    private final Ibis localIbis;
    private final IbisServer localIbisServer;
    private final Flag stopped = new Flag(false);
    private int vmIdCounter = 0;
    private final String ibisPoolName;
    private final UsedHostAdministration agents;
    private final ControllerServiceAPI vinAdministrator;
    private final int ibisPortNumber;
    private boolean hasTerminatedNormally = false;
    private static final Logger logger = LoggerFactory
            .getLogger(ControllerEngine.class);
    private final CAFactoryTalker caFactoryTalker;

    /**
     * Creates a new Vin engine.
     * 
     * @param poolName
     *            The name of the Ibis pool.
     * @param localAddressPool
     *            The pool of local addresses to use for our address allocation.
     * @param macAddressPool
     *            The pool of MAC addresses to use for our mac addresses.
     * @param verbose
     *            Should there be verbose logging?
     * @param ibisPortNumber
     *            The port number to use for the Ibis communication.
     * @param hostAddressRanges
     * @throws java.lang.Exception
     *             Thrown on problems.
     */
    ControllerEngine(final int ibisPortNumber, final boolean verbose,
            final String ibisPoolName,
            final ControllerServiceAPI vinAdministrator,
            final AddressRanges hostAddressRanges, final URI caFactoryUri)
            throws java.lang.Exception {
        super("VIN controller engine thread");
        this.vinAdministrator = vinAdministrator;
        this.ibisPoolName = ibisPoolName;
        this.ibisPortNumber = ibisPortNumber;
        localIbisServer = new IbisServer(verbose, ibisPortNumber, false);
        networkAdministration = new ControllerNetworkAdministration(
                ibisPortNumber);
        agents = new UsedHostAdministration(hostAddressRanges);
        transmitter = new Transmitter(this);
        if (caFactoryUri == null) {
            caFactoryTalker = null;
        } else {
            caFactoryTalker = new CAFactoryTalker(caFactoryUri, ibisPoolName);
        }
        final Properties ibisProperties = new Properties();
        final String serverAddress = getIbisServerAddress();
        if (serverAddress != null) {
            ibisProperties.setProperty("ibis.server.address", serverAddress);
        }
        localIbis = IbisFactory.createIbis(ibisCapabilities, ibisProperties,
                true, this, PacketSendPort.portType,
                PacketUpcallReceivePort.portType);
        final Registry registry = localIbis.registry();
        final IbisIdentifier myIbis = localIbis.identifier();
        controllerIdentifier = registry
                .elect(GlobalGlobals.CONTROLLER_ELECTION_NAME);
        isMaster = controllerIdentifier.equals(myIbis);
        receivePort = new PacketUpcallReceivePort(localIbis,
                GlobalGlobals.receivePortName, receivedMessageQueue, this);
        transmitter.start();
        registry.enableEvents();
        receivePort.enable();
        logger.info("Created controller ibis " + myIbis + " "
                + Utils.getPlatformVersion() + " host "
                + Utils.getCanonicalHostname());
        logger.info(" isMaster=" + isMaster);
    }

    @Override
    public void died(final IbisIdentifier agent) {
        if (agent.equals(localIbis.identifier())) {
            if (!stopped.isSet()) {
                logger.error("This controller has been declared dead, we might as well stop");
                setStoppedWithoutWaiting(
                        "This controller has been declared dead", false);
            }
        } else {
            transmitter.deleteNode(agent);
            deletedNodes.add(agent);
        }
        wakeTransmitterListenerThread(); // Something interesting has happened.
    }

    @Override
    public void left(final IbisIdentifier agent) {
        if (agent.equals(localIbis.identifier())) {
            if (!stopped.isSet()) {
                logger.error("This controller has been declared `left', we might as well stop");
                setStoppedWithoutWaiting(
                        "This controller has been declared `left'", false);
            }
        } else {
            transmitter.deleteNode(agent);
            deletedNodes.add(agent);
        }
        wakeTransmitterListenerThread(); // Something interesting has happened.
    }

    @Override
    public void electionResult(final String arg0, final IbisIdentifier arg1) {
        // Not interesting.
    }

    @Override
    public void gotSignal(final String arg0, final IbisIdentifier arg1) {
        // Not interesting.
    }

    @Override
    public void joined(final IbisIdentifier agent) {
        newAgents.add(agent);
        logger.info("New agent " + agent);
        wakeTransmitterListenerThread(); // Something interesting has happened.
    }

    /**
     * Register the fact that no new workers can join this pool. Not relevant,
     * so we ignore this.
     */
    @Override
    public void poolClosed() {
        // Not interesting.
    }

    @Override
    public void poolTerminated(final IbisIdentifier ibis) {
        logger.debug("Ibis pool terminated, ibis=" + ibis);
        setStoppedWithoutWaiting("Ibis pool terminated", false);
    }

    /**
     * This ibis was reported as 'may be dead'. Try not to communicate with it.
     * 
     * @param node
     *            The node that may be dead.
     */
    @Override
    public void setSuspect(final IbisIdentifier node) {
        logger.info("Node " + node + " may be dead");
        try {
            localIbis.registry().maybeDead(node);
        } catch (final IOException e) {
            // Nothing we can do about it.
        }
    }

    /**
     * Handle any new and deleted nodes that have been registered after the last
     * call.
     * 
     * @return <code>true</code> iff we made any changes to the node state.
     */
    private boolean registerNewAndDeletedNodes() {
        boolean changes = false;
        while (true) {
            final IbisIdentifier node = newAgents.poll();
            if (node == null) {
                break;
            }
            if (!node.equals(localIbis.identifier())) {
                logger.trace("Node " + node + " has joined");
                /*
                 * We only add it to the administration when it has sent a join
                 * message. Otherwise we might send it work before it has
                 * initialized.
                 */
            }
            changes = true;
        }
        while (true) {
            final IbisIdentifier node = deletedNodes.poll();
            if (node == null) {
                break;
            }
            logger.trace("Agent " + node + " has left");
            agents.removeIbis(node);
            try {
                networkAdministration.registerNodeLeft(node, this);
            } catch (final VinError e) {
                logger.error("Could not remove node " + node + " properly: "
                        + e.getLocalizedMessage(), e);
            }
            // FIXME make optional to stop whole network on dead agent.
            setStoppedWithoutWaiting("Agent " + node + " has left", false);
            changes = true;
        }
        return changes;
    }

    /** Tell the engine thread that something interesting has happened. */
    @Override
    public void wakeTransmitterListenerThread() {
        synchronized (this) {
            notifyAll();
        }
    }

    private void handleMessage(final Message msg) {
        GlobalGlobals.demoLogger.info("CONTROL: handling message " + msg);
        logger.debug("Handling message " + msg);
        if (msg instanceof NetworkMembershipMessage
                || msg instanceof NetworkDescriptionMessage
                || msg instanceof NetworkConnectionRemovalMessage
                || msg instanceof NetworkRemovalMessage
                || msg instanceof VMDescriptionMessage
                || msg instanceof StopAgentMessage) {
            throw new VinInternalError("why do I get this message: " + msg);
        } else if (msg instanceof RegisterAgentMessage) {
            final RegisterAgentMessage m = (RegisterAgentMessage) msg;
            try {
                agents.onAgentIsAvailable(msg.source, networkAdministration,
                        m.agentId, m.addresses, transmitter);
            } catch (final VinError e) {
                logger.error("Could not handle agent registration message: "
                        + e.getLocalizedMessage(), e);
                e.printStackTrace();
            }
        } else if (msg instanceof LocalNetworkMembershipReadyMessage) {
            final LocalNetworkMembershipReadyMessage rm = (LocalNetworkMembershipReadyMessage) msg;
            try {
                logger.debug("Network connection is ready: networkId="
                        + rm.networkId + " connectionId=" + rm.connectionId);
                final boolean changed = networkAdministration
                        .registerNetworkMemberReady(rm.networkId,
                                rm.connectionId);
                final String vmId = networkAdministration.getVmId(rm.networkId,
                        rm.connectionId);
                registerConnectionReady(rm.networkId, vmId, rm.connectionId,
                        changed);
            } catch (final ConfigurationError e) {
                logger.error("Configuration error: " + e.getLocalizedMessage());
                e.printStackTrace();
            } catch (final VinError e) {
                logger.error("VIN error: " + e.getLocalizedMessage());
                e.printStackTrace();
            }
        } else if (msg instanceof ConfigurationErrorMessage) {
            final ConfigurationErrorMessage em = (ConfigurationErrorMessage) msg;
            logger.error("Agent " + em.agentId + " has a configuration error "
                    + em.message);
        } else if (msg instanceof AgentErrorMessage) {
            final AgentErrorMessage em = (AgentErrorMessage) msg;
            logger.error("Agent " + em.agentId + " has a fatal error "
                    + em.message, em.throwable);
        } else {
            logger.error("Internal error: " + "Don't know how to handle a "
                    + msg.getClass() + " message", new Throwable());
        }
    }

    private boolean handleIncomingMessages() {
        boolean progress = false;
        while (true) {
            final Message msg = receivedMessageQueue.getNext();
            if (msg == null) {
                break;
            }
            logger.info("Get incoming message from queue: " + msg);
            handleMessage(msg);
            progress = true;
        }
        return progress;
    }

    private synchronized void dumpEngineState() {
        logger.trace("=== Main thread was only woken by timeout ===");
        receivedMessageQueue.dump();
        logger.trace("Maximal receive queue length: "
                + receivedMessageQueue.getMaximalQueueLength());
        transmitter.dumpState();
        logger.trace("=== End of dump of engine state ===");
    }

    @Override
    public void run() {
        final int sleepTime = ImplSettings.MAXIMAL_ENGINE_SLEEP_INTERVAL;
        try {
            while (!stopped.isSet()) {
                boolean progress;
                do {
                    // Keep doing bookkeeping chores until all is done.
                    final boolean progressIncoming = handleIncomingMessages();
                    final boolean progressNodeChurn = registerNewAndDeletedNodes();
                    progress = progressIncoming || progressNodeChurn;
                    if (progress) {
                        logger.trace("Main loop: progress=true incoming="
                                + progressIncoming + " churn="
                                + progressNodeChurn);
                        if (progressIncoming) {
                            logger.info(receivedMessageQueue.formatCounts());
                        }
                    }
                } while (progress);
                synchronized (this) {
                    if (receivedMessageQueue.isEmpty() && !stopped.isSet()
                            && newAgents.isEmpty() && deletedNodes.isEmpty()) {
                        try {
                            this.wait(sleepTime);
                        } catch (final InterruptedException e) {
                            // Ignored.
                        }
                    }
                }
            }
        } finally {
            logger.info("Sending stop messages to agents");
            agents.stop(transmitter);
            logger.info("Shutting down transmitter");
            transmitter.setStopped();
            logger.info("Transmitter setting stopped");
            try {
                transmitter.join(ImplSettings.TRANSMITTER_SHUTDOWN_TIMEOUT);
            } catch (final InterruptedException e) {
                // ignore.
            }
            logger.info("Transmitter has shut down");
            try {
                receivePort.close();
                logger.info("Receive port was closed");
                localIbisServer.stop();
                logger.info("Ibis server was stopped");
                localIbis.end();
            } catch (final IOException x) {
                // Nothing we can do about it.
            }
            vinAdministrator.onControllerTerminated(ibisPortNumber);
        }
        transmitter.printStatistics();
        Utils.printThreadStats(logger);
    }

    @Override
    public Ibis getLocalIbis() {
        return localIbis;
    }

    @Override
    public String registerVirtualMachine(final Properties properties)
            throws VinError {
        final String vmId = "vm" + vmIdCounter++;
        AgentContextType contextType;
        {
            final String contextString = properties.getProperty(
                    PropertyNames.AGENT_CONTEXT, "host");
            if (contextString.equals("guest") || contextString.equals("client")
                    || contextString.equals("vm")) {
                contextType = AgentContextType.CLIENT;
            } else if (contextString.equals("host")) {
                contextType = AgentContextType.HOST;
            } else if (contextString.equals("physical")
                    || contextString.equals("physicial-machine")) {
                contextType = AgentContextType.PHYSICAL_MACHINE;
            } else {
                throw new VinError("Unsupported agent context '"
                        + contextString + '\'');
            }
        }
        agents.registerMachine(vmId, contextType, properties);
        return vmId;
    }

    @Override
    public String registerPhysicalMachine(final Properties properties)
            throws VinError {
        final String pmId = "pm" + vmIdCounter++;
        agents.registerMachine(pmId, AgentContextType.PHYSICAL_MACHINE,
                properties);
        return pmId;
    }

    @Override
    public void removeMachine(final String machineId) {
        agents.removeMachine(machineId, transmitter);
    }

    @Override
    public String addNetwork(final String encapsulation,
            final Properties rawProperties) throws VinError,
            ConfigurationError, OutOfEntriesError {
        final EncapsulationType networkType;
        final Properties properties = new Properties(rawProperties);
        final String sizeString = properties.getProperty(
                PropertyNames.MAXIMUM_NETWORK_SIZE_KEY, null);
        logger.debug("Adding new network: encapsulation=" + encapsulation
                + " properties=" + rawProperties);
        if (sizeString == null) {
            throw new VinError(
                    "No maximum network size has been specified: you must set the '"
                            + PropertyNames.MAXIMUM_NETWORK_SIZE_KEY
                            + "' property");
        }
        long sz;
        try {
            sz = Long.parseLong(sizeString);
        } catch (final NumberFormatException x) {
            throw new VinError("Malformed value for '"
                    + PropertyNames.MAXIMUM_NETWORK_SIZE_KEY + "' property: '"
                    + sizeString + "' is not a number");
        }
        if (sz < 0) {
            throw new VinError("Illegal value for '"
                    + PropertyNames.MAXIMUM_NETWORK_SIZE_KEY + "' property: "
                    + sz + " is not a valid network size");
        }
        if (encapsulation.equalsIgnoreCase("strongSwan")) {
            networkType = EncapsulationType.StrongSwan;
        } else if (encapsulation.equalsIgnoreCase("gre")) {
            networkType = EncapsulationType.GRE;
        } else if (encapsulation.equalsIgnoreCase("openvpn")) {
            networkType = EncapsulationType.OpenVPN;
        } else {
            throw new VinError("Unsupported network mode '" + encapsulation
                    + "'");
        }

        final Subnet subnet = vinAdministrator.getNextSubnet(sz);
        final ArrayValueRange macAddresses = vinAdministrator
                .getNextMacSubrange(subnet.getAddressCount());
        final String networkId = networkAdministration.createNetwork(
                networkType, subnet, macAddresses, caFactoryTalker, properties);
        agents.sendPrivateNetworkInfo(networkAdministration, networkId,
                transmitter);
        return networkId;
    }

    @Override
    public void removeNetwork(final String networkId) throws VinError {
        agents.sendNetworkRemoval(networkId, transmitter);
        networkAdministration.removeNetwork(networkId, vinAdministrator);
    }

    @Override
    public String addConnection(final String networkId,
            final String virtualMachineId, final Properties properties)
            throws VinError, ConfigurationError {
        InetAddress addr;
        assert virtualMachineId != null;
        try {
            addr = networkAdministration.getNextAddress(networkId);
        } catch (final OutOfEntriesError e) {
            throw new VinError("The pool of local addresses is exhausted");
        }
        return agents.registerMachineConnection(networkAdministration,
                virtualMachineId, networkId, addr, properties, transmitter);
    }

    public void registerConnectionReady(final String networkId,
            final String virtualMachineId, final String connectionId,
            final boolean changed) throws VinError, ConfigurationError {
        agents.registerMachineConnectionReady(networkAdministration,
                virtualMachineId, networkId, connectionId, changed, transmitter);
    }

    @Override
    public void removeConnection(final String networkId,
            final String connectionId) throws VinError {
        networkAdministration.removeConnection(networkId, connectionId, this);
        agents.sendConnectionRemoval(networkId, connectionId, transmitter);
    }

    @Override
    public String getAddress(final String networkId, final String connectionId)
            throws VinError {
        return networkAdministration.getPrivateIPAddressString(networkId,
                connectionId);
    }

    @Override
    public void setStopped() {
        setStopped("API setStopped() invoked", true);
    }

    @Override
    public void setStopped(final String reason, final boolean terminateNormally) {
        setStoppedWithoutWaiting(reason, terminateNormally);
        try {
            join(ImplSettings.ENGINE_SHUTDOWN_TIMEOUT);
        } catch (final InterruptedException e) {
            // ignore.
        }
    }

    private void setStoppedWithoutWaiting(final String reason,
            final boolean terminateNormally) {
        final String threadName = getName();
        logger.debug("Stopping controller engine: '" + reason + "' thread "
                + threadName + " terminateNormally=" + terminateNormally);
        hasTerminatedNormally = terminateNormally;
        stopped.set();
        wakeTransmitterListenerThread();
    }

    @Override
    public String getIbisServerAddress() {
        return localIbisServer.getAddress();
    }

    @Override
    public String getIbisPoolName() {
        return ibisPoolName;
    }

    void returnLocalAddress(final String networkId, final InetAddress addr)
            throws VinError {
        networkAdministration.recycleAddress(networkId, addr);
    }

    @Override
    public boolean getTerminatedNormally() {
        return !isAlive() && hasTerminatedNormally;
    }

}
