package org.ow2.contrail.resource.vin.controller.impl;

import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.Subnet;

interface ControllerServiceAPI {
    public void onControllerTerminated(final int ibisSocketNumber);

    public Subnet getNextSubnet(long sz) throws OutOfEntriesError;

    public void returnInetSubnet(Subnet subnet);

    public ArrayValueRange getNextMacSubrange(long members)
            throws OutOfEntriesError;

    public void returnMACAddresses(ArrayValueRange macAddresses);
}
