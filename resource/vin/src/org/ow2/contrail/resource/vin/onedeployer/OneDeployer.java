package org.ow2.contrail.resource.vin.onedeployer;

import ibis.util.RunProcess;

import java.io.File;
import java.io.IOException;

import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Runs a specified job on a single processor.
 */
public class OneDeployer {
    private static final Logger logger = LoggerFactory
            .getLogger(OneDeployer.class);
    private static final File oneHookScriptsDir = new File(
            "/usr/share/contrail/support/vin/one-scripts");
    private static final String CREATE_VM_SCRIPT = "one-create-vm";

    private static String buildOneTemplate(final String tpl, final String vmId,
            final String serverAddress, final String poolName) {
        assert !vmId.isEmpty();
        assert !serverAddress.isEmpty();
        assert !poolName.isEmpty();
        String res = tpl.replace("$VIN_VM_ID", vmId);
        res = res.replace("$VIN_IBIS_SERVER", serverAddress);
        res = res.replace("$VIN_POOL_NAME", poolName);
        return res;
    }

    /**
     * Given a machine address, launch a Vin agent on that machine.
     * 
     * @param templateFile
     *            The OpenNebula template file to use
     * @param vmId
     *            The identifier that the agent should use to register itself
     *            with the controller.
     * @param serverAddress
     *            The address of the Ibis server to use.
     * @param poolName
     *            The name of the Ibis pool of this application.
     * @return Returns the OpenNebula identifier of the newly started One VM.
     * @throws VinError
     *             Thrown if for some reason there is an inconsistency in the
     *             specified info.
     */
    public static String deployAgent(final File templateFile,
            final String vmId, final String serverAddress, final String poolName)
            throws VinError {
        if (!templateFile.exists()) {
            throw new VinError("Template file '" + templateFile
                    + "' does not exist");
        }
        String rawTpl;
        try {
            rawTpl = Utils.readFile(templateFile);
        } catch (final IOException e1) {
            throw new VinError("Cannot read template file '" + templateFile
                    + "'", e1);
        }
        final String tpl = buildOneTemplate(rawTpl, vmId, serverAddress,
                poolName);
        final File tmpFile;
        try {
            tmpFile = File.createTempFile("vin-vm-", ".one");
            Utils.writeFile(tmpFile, tpl);
        } catch (final IOException e) {
            throw new VinError("Cannot write rewritten template", e);
        }
        try {
            final RunProcess res = Utils.cleanlyExecuteScript(
                    oneHookScriptsDir, CREATE_VM_SCRIPT, "x",
                    tmpFile.getAbsolutePath());
            final String id = new String(res.getStdout());
            final int exitStatus = res.getExitStatus();
            if (exitStatus != 0) {
                throw new VinError(
                        "Cannot create VM using template, exit status "
                                + exitStatus + "\n" + tpl);
            }
            return id;
        } finally {
            tmpFile.delete();
        }
    }

    /**
     * Given the OpenNebula of a VM, delete this VM through OpenNebula.
     * 
     * @param VMid
     *            The identifier of the VM to delete.
     */
    public static void deleteVM(final String VMid) {
        final RunProcess res = Utils.cleanlyExecuteCommand("onevm", "delete",
                VMid);
        if (res.getExitStatus() != 0) {
            logger.error("Could not delete OpenNebula VM " + VMid);
        }
    }
}
