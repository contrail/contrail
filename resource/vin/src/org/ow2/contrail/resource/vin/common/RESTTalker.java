package org.ow2.contrail.resource.vin.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ow2.contrail.resource.vin.annotations.TestAPI;

public class RESTTalker {
    public static final String JSON_MIME = "application/json";
    public static final String XML_MIME = "application/xml";
    public static final String PLAIN_TEXT_MIME = "text/plain";
    private final String user;

    public RESTTalker(final String user) {
        this.user = user;
    }

    @TestAPI
    public String requestGet(final URI uri) throws VinError {
        try {

            final DefaultHttpClient httpClient = new DefaultHttpClient();
            final HttpGet getRequest = new HttpGet(uri);
            getRequest.getParams();
            getRequest.addHeader("accept", JSON_MIME);
            if (user != null) {
                getRequest.addHeader("X-Username", user);
            }
            final HttpResponse response = httpClient.execute(getRequest);
            final StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() != 200) {
                throw new RuntimeException("Failed get request:  " + statusLine);
            }
            final InputStream content = response.getEntity().getContent();
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    content));
            final String body;
            try {
                body = Utils.read(br);
            } finally {
                br.close();
            }
            httpClient.getConnectionManager().shutdown();
            return body;
        } catch (final IOException e) {
            throw new VinError("Cannot connect to server '" + uri + "'", e);
        }
    }

    public String sendPutRequest(final URI uri, final String payload,
            final String payloadType) throws VinError {
        try {
            final DefaultHttpClient httpClient = new DefaultHttpClient();
            final HttpPut putRequest = new HttpPut(uri);
            putRequest.addHeader("accept", JSON_MIME);
            putRequest.addHeader("Content-Type", payloadType);
            if (user != null) {
                putRequest.addHeader("X-Username", user);
            }
            putRequest.setEntity(new StringEntity(payload));
            final HttpResponse response = httpClient.execute(putRequest);
            final StatusLine statusLine = response.getStatusLine();
            final int statusCode = statusLine.getStatusCode();
            if (statusCode != 200 && statusCode != 202) {
                throw new RuntimeException("Failed put request'" + putRequest
                        + "': " + statusLine);
            }
            final InputStream content = response.getEntity().getContent();
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    content));
            final String body;
            try {
                body = Utils.read(br);
            } finally {
                br.close();
            }
            httpClient.getConnectionManager().shutdown();
            return body;
        } catch (final IOException e) {
            throw new VinError("Cannot connect to server '" + uri + "'", e);
        }
    }

    public String sendPostRequest(final URI uri, final String payload,
            final String payloadType) throws VinError {
        try {
            final DefaultHttpClient httpClient = new DefaultHttpClient();
            final HttpPost postRequest = new HttpPost(uri);
            postRequest.addHeader("accept", JSON_MIME);
            postRequest.addHeader("Content-Type", payloadType);
            if (user != null) {
                postRequest.addHeader("X-Username", user);
            }
            postRequest.setEntity(new StringEntity(payload));
            final HttpResponse response = httpClient.execute(postRequest);
            final StatusLine statusLine = response.getStatusLine();
            final int statusCode = statusLine.getStatusCode();
            if (statusCode < 200 || statusCode > 202) {
                throw new RuntimeException("Failed put request'" + postRequest
                        + "': " + statusLine);
            }
            final InputStream content = response.getEntity().getContent();
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    content));
            final String body;
            try {
                body = Utils.read(br);
            } finally {
                br.close();
            }
            httpClient.getConnectionManager().shutdown();
            return body;
        } catch (final IOException e) {
            throw new VinError("Cannot connect to server '" + uri + "'", e);
        }
    }

    public String sendDeleteRequest(final URI uri) throws VinError {
        try {
            final DefaultHttpClient httpClient = new DefaultHttpClient();
            final HttpDelete deleteRequest = new HttpDelete(uri);
            deleteRequest.getParams();
            deleteRequest.addHeader("accept", JSON_MIME);
            if (user != null) {
                deleteRequest.addHeader("X-Username", user);
            }
            final HttpResponse response = httpClient.execute(deleteRequest);
            final StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() != 200) {
                throw new RuntimeException("Failed delete request '"
                        + deleteRequest + "': " + statusLine);
            }
            final InputStream content = response.getEntity().getContent();
            final BufferedReader br = new BufferedReader(new InputStreamReader(
                    content));
            final String body;
            try {
                body = Utils.read(br);
            } finally {
                br.close();
            }
            httpClient.getConnectionManager().shutdown();
            return body;
        } catch (final IOException e) {
            throw new VinError("Cannot connect to server '" + uri + "'", e);
        }
    }

}
