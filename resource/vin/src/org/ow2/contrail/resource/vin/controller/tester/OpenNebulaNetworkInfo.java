package org.ow2.contrail.resource.vin.controller.tester;

import java.util.ArrayList;

import org.ow2.contrail.resource.vin.onedeployer.OneDeployer;

class OpenNebulaNetworkInfo extends VINNetworkInfo {
    private final ArrayList<String> ids = new ArrayList<String>();

    void registerVM(final String id) {
        ids.add(id);
    }

    @Override
    void destroy() {
        for (final String id : ids) {
            OneDeployer.deleteVM(id);
        }
    }
}