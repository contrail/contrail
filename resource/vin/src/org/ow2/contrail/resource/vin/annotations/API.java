package org.ow2.contrail.resource.vin.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Indicates that the given method is part of the API (so don't complain that it
 * is unused).
 * 
 * @author Kees van Reeuwijk
 * 
 */
@Retention(RetentionPolicy.SOURCE)
public @interface API {
    // Nothing
}
