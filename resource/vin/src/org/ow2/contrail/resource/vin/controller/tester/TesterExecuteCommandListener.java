package org.ow2.contrail.resource.vin.controller.tester;

import ibis.util.RunProcess;

import org.ow2.contrail.resource.vin.common.ExecuteCommandListener;
import org.slf4j.Logger;

public class TesterExecuteCommandListener implements ExecuteCommandListener {
    private final Logger logger;

    TesterExecuteCommandListener(final Logger logger) {
        this.logger = logger;
    }

    @Override
    public void onCommandCompleted(final RunProcess p) {
        if (p.getExitStatus() != 0) {
            logger.error("Command " + p.command() + " failed");
        } else {
            logger.debug("Completed command " + p.command());
        }
    }
}
