package org.ow2.contrail.resource.vin.vepdeployer;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.ow2.contrail.resource.vin.common.RESTTalker;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Runs a specified job on a single processor.
 */
public class VEPDeployer {
    private static final Logger logger = LoggerFactory
            .getLogger(VEPDeployer.class);
    private static final URI vepURI = URI.create("http://localhost:10500");

    public static String createVMOvfFragment(final String tpl,
            final String vmId, final String serverAddress, final String poolName)
            throws VinError {
        final File vmTemplateFile = new File(tpl);
        if (!vmTemplateFile.exists()) {
            throw new VinError("Template file '" + vmTemplateFile
                    + "' does not exist");
        }
        String res;
        try {
            res = Utils.readFile(vmTemplateFile);
        } catch (final IOException e) {
            throw new VinError("Cannot read template file '" + vmTemplateFile
                    + "'", e);
        }
        assert !vmId.isEmpty();
        assert !serverAddress.isEmpty();
        assert !poolName.isEmpty();
        logger.debug("Read OVF fragment from file '" + vmTemplateFile + "':["
                + res + "]");
        res = res.replace("$VIN_VM_ID", vmId);
        res = res.replace("$VM_ID", vmId);
        res = res.replace("$VIN_IBIS_SERVER", serverAddress);
        res = res.replace("$VIN_POOL_NAME", poolName);
        logger.debug("Created OVF fragment [" + res + "]");
        return res;
    }

    /**
     * Given a machine address, launch a Vin agent on that machine.
     * 
     * @param user
     *            The user of the deployment.
     * @param appName
     *            The name to use for the application.
     * @param applicationTemplateFile
     *            The OVF template file for constructing an entire application
     *            out of filled in vmTemplateFile fragments.
     * @param payload
     *            The OVF xml fragments describing the individual VMs.
     * @return The identifier that has been assigned to this application.
     * @throws VinError
     *             Thrown if for some reason there is an inconsistency in the
     *             specified info.
     */
    public static String deployNetwork(final String user, final String appName,
            final File applicationTemplateFile, final String payload)
            throws VinError {
        if (!applicationTemplateFile.exists()) {
            throw new VinError("Template file '" + applicationTemplateFile
                    + "' does not exist");
        }
        String rawApplicationTpl;
        try {
            rawApplicationTpl = Utils.readFile(applicationTemplateFile);
        } catch (final IOException e) {
            throw new VinError("Cannot read template file '"
                    + applicationTemplateFile + "'", e);
        }
        final String ovfText = rawApplicationTpl.replace("$VIN_VMS", payload);
        final RESTTalker talker = new RESTTalker(user);
        String res = talker.sendPutRequest(vepURI.resolve("/ovf/" + appName),
                ovfText, RESTTalker.XML_MIME);
        logger.debug("Application OVF submitted. Reply is: [" + res + "]");
        final String ovfId = extractApplicationId(res);
        res = talker.sendPutRequest(
                vepURI.resolve("/ovf/id/" + ovfId + "/action/initialize"), "",
                RESTTalker.XML_MIME);
        logger.debug("Application initialized. Reply is: [" + res + "]");
        res = talker.sendPutRequest(
                vepURI.resolve("/ovf/id/" + ovfId + "/action/deploy"), "",
                RESTTalker.XML_MIME);
        logger.debug("Application deployed. Reply is: [" + res + "]");
        return ovfId;
    }

    private static String extractApplicationId(final String res) {
        try {
            final ObjectMapper m = new ObjectMapper();
            final JsonNode rootNode = m.readValue(res, JsonNode.class);
            final JsonNode snoNode = rootNode.get("sno");
            if (snoNode == null) {
                throw new VinInternalError(
                        "No 'sno' field in VEP reply string [" + res + ']');
            }
            return snoNode.asText();
        } catch (final JsonParseException e) {
            throw new VinInternalError(
                    "Cannot parse json string [" + res + "]", e);
        } catch (final JsonMappingException e) {
            throw new VinInternalError("Mapping exception in json  [" + res
                    + "]", e);
        } catch (final IOException e) {
            throw new VinInternalError("Cannot read json string [" + res + "]",
                    e);
        }
    }

    /**
     * Given the OpenNebula of a VM, delete this VM through OpenNebula.
     * 
     * @param user
     *            The user of the deployment.
     * @param ovfId
     *            The identifier of the application to stop.
     * @throws VinError
     *             Thrown if for some reason there is an inconsistency in the
     *             specified info.
     */
    public static void deleteVM(final String user, final String ovfId)
            throws VinError {
        final RESTTalker talker = new RESTTalker(user);
        final String res = talker.sendPostRequest(
                vepURI.resolve("/ovf/id/" + ovfId + "/action/stop"), "",
                RESTTalker.JSON_MIME);
        logger.debug("Application stopped. Reply is: [" + res + "]");
    }
}
