package org.ow2.contrail.resource.vin.common;

/**
 * A message from an agent to the coordinator, telling it that the given node is
 * now ready to handle membership duties of the given network.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class LocalNetworkMembershipReadyMessage extends Message {
    private static final long serialVersionUID = 1L;

    /** The identifier of the network to which this node is connected. */
    public final String networkId;

    /** The identifier of the connection. */
    public final String connectionId;

    /**
     * Constructs a new message with the given information.
     * 
     * @param networkId
     *            The identifier of the network to which this node is connected.
     * @param connectionId
     *            The identifier of the connection.
     */
    public LocalNetworkMembershipReadyMessage(final String networkId,
            final String connectionId) {
        this.networkId = networkId;
        this.connectionId = connectionId;
    }

    @Override
    public String toString() {
        return LocalNetworkMembershipReadyMessage.class.getName()
                + "[connectionId=" + connectionId + " network=" + networkId
                + "]";
    }

}
