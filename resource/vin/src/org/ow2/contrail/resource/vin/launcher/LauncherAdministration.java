package org.ow2.contrail.resource.vin.launcher;

import ibis.ipl.IbisIdentifier;

import java.util.ArrayList;

import org.ow2.contrail.resource.vin.common.Transmitter;

class LauncherAdministration {
    private final ArrayList<LauncherInfo> launcherInfoList = new ArrayList<LauncherInfo>();

    LauncherInfo getLauncherInfo(final String host) {
        for (int i = 0; i < launcherInfoList.size(); i++) {
            final LauncherInfo e = launcherInfoList.get(i);
            if (e.hostId.equals(host)) {
                return e;
            }
        }
        return null;
    }

    LauncherInfo registerNewHost(final String host) {
        final LauncherInfo info = new LauncherInfo(host, null);
        launcherInfoList.add(info);
        return info;
    }

    void registerLauncher(final IbisIdentifier source, final String hostId,
            final Transmitter transmitter) {
        final LauncherInfo info = getLauncherInfo(hostId);
        if (info == null) {
            final LauncherInfo newInfo = new LauncherInfo(hostId, source);
            launcherInfoList.add(newInfo);
        } else {
            info.transmitBacklog(transmitter);
        }
    }

}
