package org.ow2.contrail.resource.vin.common;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Methods to manipulate byte arrays.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class ArrayValue implements Serializable {
    private static final long serialVersionUID = 1L;
    public final int val[];

    public ArrayValue(final int... v) {
        val = Arrays.copyOf(v, v.length);
    }

    public ArrayValue(final byte... v) {
        val = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            val[i] = (v[i] & 0xFF);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(val);
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ArrayValue)) {
            return false;
        }
        final ArrayValue other = (ArrayValue) obj;
        if (!Arrays.equals(val, other.val)) {
            return false;
        }
        return true;
    }

    /**
     * Given a value <code>v</code>, return <code>true</code> iff <code>v</code>
     * is the next value after this value.
     * 
     * @param v
     *            The value to compare.
     * @return <code>true</code> iff v is the value after this value.
     */
    boolean hasAsNextValue(final ArrayValue v) {
        // TODO: cheaper way to compute hasAsNextValue.
        final ArrayValue tmp = increment();
        final boolean res = tmp.equals(v);
        return res;
    }

    /**
     * Returns 1 if a is larger than b, -1 if a is smaller than b, or 0 if they
     * are equal.
     * 
     * @param av
     *            The first array value to compare.
     * @param bv
     *            The second array value to compare.
     * @return The comparison verdict.
     */
    static int compare(final ArrayValue av, final ArrayValue bv) {
        final int a[] = av.val;
        final int b[] = bv.val;
        if (a.length < b.length) {
            return -1;
        }
        if (a.length > b.length) {
            return 1;
        }
        for (int ix = 0; ix < a.length; ix++) {
            final int vala = a[ix];
            final int valb = b[ix];
            if (vala > valb) {
                return 1;
            }
            if (vala < valb) {
                return -1;
            }
        }
        return 0;
    }

    /**
     * Given two values, and return <code>true</code> iff they are equal.
     * 
     * @param a
     *            The one value to compare.
     * @param b
     *            The other value to compare.
     * @return <code>true</code> iff the two arrays are equal.
     */
    static boolean areEqual(final ArrayValue a, final ArrayValue b) {
        return Arrays.equals(a.val, b.val);
    }

    /**
     * Returns an decremented copy of this value.
     * 
     * @return An decremented copy of this value.
     */
    ArrayValue decrement() {
        final int r[] = Arrays.copyOf(val, val.length);
        int ix = r.length;
        while (ix > 0) {
            ix--;
            final int v = r[ix];
            if (v == 0) {
                // Wrap around, decrement next byte.
                r[ix] = 0xFF;
            } else {
                r[ix] = (v - 1);
                break;
            }
        }
        return new ArrayValue(r);
    }

    /**
     * Returns an incremented copy of this value.
     * 
     * @return An incremented copy of this value.
     */
    public ArrayValue increment() {
        final int r[] = Arrays.copyOf(val, val.length);
        int ix = r.length;
        while (ix > 0) {
            ix--;
            final int v = r[ix];
            if (v == 0xFF) {
                // Wrap around, increment next byte.
                r[ix] = 0;
            } else {
                r[ix] = (v + 1);
                break;
            }
        }
        return new ArrayValue(r);
    }

    /**
     * Given two byte arrays, return <code>true</code> iff the array 'b'
     * represents a value that is less than 'a'.
     * 
     * @param av
     *            The first value to compare.
     * @param bv
     *            The second value to compare.
     * @return <code>true</code> iff the range 'b' represents a value that is
     *         less than 'a'.
     */
    public static boolean isLessThan(final ArrayValue av, final ArrayValue bv) {
        final int a[] = av.val;
        final int b[] = bv.val;
        assert a.length == b.length;
        for (int ix = 0; ix < a.length; ix++) {
            final int va = a[ix] & 0xFF;
            final int vb = b[ix] & 0xFF;
            if (vb > va) {
                return false;
            }
            if (vb < va) {
                return true;
            }
        }
        return false;
    }

    /**
     * Given two byte arrays, return <code>true</code> iff the array 'b'
     * represents a value that is greater than 'a'.
     * 
     * @param av
     *            The first value to compare.
     * @param bv
     *            The second value to compare.
     * @return <code>true</code> iff the range 'b' represents a value that is
     *         greater than 'a'.
     */
    static boolean isGreaterThan(final ArrayValue av, final ArrayValue bv) {
        final int a[] = av.val;
        final int b[] = bv.val;
        assert a.length == b.length;
        for (int ix = 0; ix < a.length; ix++) {
            final int va = a[ix] & 0xFF;
            final int vb = b[ix] & 0xFF;
            if (vb > va) {
                return true;
            }
            if (vb < va) {
                return false;
            }
        }
        return false;
    }

    /**
     * Given two byte arrays, return <code>true</code> iff the array 'b'
     * represents a value that is less than 'a' or equal to 'a'.
     * 
     * @param av
     *            The first value to compare.
     * @param bv
     *            The second value to compare.
     * @return <code>true</code> iff the range 'b' represents a value that is
     *         less than 'a' or equal to 'a'.
     */
    public static boolean isLessOrEqual(final ArrayValue av, final ArrayValue bv) {
        final int a[] = av.val;
        final int b[] = bv.val;
        assert a.length == b.length;
        for (int ix = 0; ix < a.length; ix++) {
            final int va = a[ix] & 0xFF;
            final int vb = b[ix] & 0xFF;
            if (vb > va) {
                return false;
            }
            if (vb < va) {
                return true;
            }
        }
        return true;
    }

    /**
     * Given two byte arrays, return <code>true</code> iff the array 'b'
     * represents a value that is greater than 'a' or equal to 'a'.
     * 
     * @param av
     *            The first value to compare.
     * @param bv
     *            The second value to compare.
     * @return <code>true</code> iff the range 'b' represents a value that is
     *         greater than 'a' or equal to 'a'.
     */
    static boolean isGreaterOrEqual(final ArrayValue av, final ArrayValue bv) {
        final int a[] = av.val;
        final int b[] = bv.val;
        assert a.length == b.length;
        for (int ix = 0; ix < a.length; ix++) {
            final int va = a[ix] & 0xFF;
            final int vb = b[ix] & 0xFF;
            if (vb < va) {
                return false;
            }
            if (vb > va) {
                return true;
            }
        }
        return true;
    }

    /**
     * Given a value, return a representation of that value as an IPv4 address.
     * 
     * @return The IPv4 address string of <code>addr</code>.
     */
    public String makeIP4AddressString() {
        assert val.length == 4;
        final StringBuilder b = new StringBuilder();
        b.append(val[0] & 0xFF);
        b.append('.');
        b.append(val[1] & 0xFF);
        b.append('.');
        b.append(val[2] & 0xFF);
        b.append('.');
        b.append(val[3] & 0xFF);
        return new String(b);
    }

    private static void addSegment(final StringBuilder b, final int v0,
            final int v1) {
        final String s = Integer.toHexString(v0 * 256 + v1);
        b.append(s);
    }

    /**
     * Given a value, return a representation of that value as an IPv6 address.
     * 
     * @return The IPv6 address string of <code>addr</code>.
     */
    public String makeIP6AddressString() {
        assert val.length == 16;
        final StringBuilder b = new StringBuilder();
        addSegment(b, val[0], val[1]);
        b.append(':');
        addSegment(b, val[2], val[3]);
        b.append(':');
        addSegment(b, val[4], val[5]);
        b.append(':');
        addSegment(b, val[6], val[7]);
        b.append(':');
        addSegment(b, val[8], val[9]);
        b.append(':');
        addSegment(b, val[10], val[11]);
        b.append(':');
        addSegment(b, val[12], val[13]);
        b.append(':');
        addSegment(b, val[14], val[15]);
        return new String(b);
    }

    /**
     * Returns a representation of this value as a MAC address.
     * 
     * @return The MAC address string of this value.
     */
    public String buildBuildMACAddressString() {
        final StringBuilder res = new StringBuilder();
        boolean first = true;
        for (final int b : val) {
            if (first) {
                first = false;
            } else {
                res.append(':');
            }
            final int v = b;
            final String hexString = Integer.toHexString(v);
            if (hexString.length() == 1) {
                res.append('0');
            }
            res.append(hexString);
        }
        return res.toString();
    }

    /**
     * Given a value <code>av</code>, calculate the offset that must be added to
     * this value to get the value <code>av</code>. In other words, this code
     * subtracts this value from <code>av</code>, and represents the difference
     * as a long. We assume without checking that the difference can be
     * represented in a long.
     * 
     * @param av
     *            The value to calculate the offset for.
     * @return The offset.
     */
    public long offsetOf(final ArrayValue av) {
        final int[] value = av.val;
        long v = 0;
        assert value.length == val.length;
        for (int ix = 0; ix < value.length; ix++) {
            v *= 256;
            v += value[ix] - val[ix];
        }
        return v;
    }

    /**
     * Add the offset <code>offset</code> to this value, and return a new
     * ArrayValue for the result.
     * 
     * @param offset
     *            The offset to add.
     * @return The offset value.
     */
    public ArrayValue addOffset(final long offset) {
        final int res[] = new int[val.length];
        long v = offset;
        int carry = 0;
        int ix = val.length;
        while (ix > 0) {
            ix--;
            final int v1 = (int) (v & 0xFF);
            final int v2 = val[ix];
            final int n = v2 + v1 + carry;
            res[ix] = n & 0xFF;
            carry = n >> 8;
            v >>= 8;
        }
        return new ArrayValue(res);
    }

    ArrayValue invert() {
        final int res[] = new int[val.length];
        for (int i = 0; i < val.length; i++) {
            res[i] = (~val[i]) & 0xFF;
        }
        return new ArrayValue(res);
    }

    @Override
    public String toString() {
        return Arrays.toString(val);
    }

    int size() {
        return val.length;
    }

    int get(final int i) {
        return val[i];
    }

    /**
     * Given the number of bytes to use, and the number of alignment bits,
     * return a byte array with the alignment mask for the given number of bits.
     * 
     * @param alignmentBits
     *            The number of alignment bits.
     * @param length
     *            The length of the mask in bytes.
     * @return The alignment mask.
     */
    static ArrayValue buildAlignmentMask(final int alignmentBits,
            final int length) {
        assert alignmentBits < length * 8;
        final int res[] = new int[length];
        // First set res[] to 1<<alignmentBits
        final int ix = length - 1 - alignmentBits / 8;
        final int val = 1 << (alignmentBits & 7);
        res[ix] = val;
        // Then subtract one.
        ArrayValue res1 = new ArrayValue(res);
        res1 = res1.decrement();
        return res1;
    }

}
