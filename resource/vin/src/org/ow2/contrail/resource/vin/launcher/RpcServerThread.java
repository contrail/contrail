package org.ow2.contrail.resource.vin.launcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.ow2.contrail.resource.vin.common.GlobalGlobals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thetransactioncompany.jsonrpc2.JSONRPC2ParseException;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.Dispatcher;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;

class RpcServerThread extends Thread {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Dispatcher dispatcher;
    private final Socket socket;

    RpcServerThread(final Dispatcher dispatcher, final Socket socket) {
        super("RpcServerThread");
        this.dispatcher = dispatcher;
        this.socket = socket;
    }

    private static final MessageContext messageContext = null;

    private String processInput(final String theInput) {
        JSONRPC2Request req;
        try {
            req = JSONRPC2Request.parse(theInput);
        } catch (final JSONRPC2ParseException e) {
            return e.toString();
        }
        logger.trace("Request: " + theInput);
        GlobalGlobals.demoLogger.info("SERVER: request: " + theInput);
        final JSONRPC2Response response = dispatcher.process(req,
                messageContext);
        final String res = response.toString();
        logger.trace("Response: " + res);
        GlobalGlobals.demoLogger.info("SERVER: response: " + res);
        return res;
    }

    @Override
    public void run() {
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            String inputLine;
            String outputLine;

            while ((inputLine = in.readLine()) != null) {
                outputLine = processInput(inputLine);
                out.println(outputLine);
            }
        } catch (final IOException e) {
            // FIXME: better error handling.
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
            if (in != null) {
                try {
                    in.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}