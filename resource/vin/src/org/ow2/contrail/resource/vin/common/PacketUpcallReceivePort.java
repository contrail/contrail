package org.ow2.contrail.resource.vin.common;

import ibis.ipl.Ibis;
import ibis.ipl.MessageUpcall;
import ibis.ipl.PortType;
import ibis.ipl.ReadMessage;
import ibis.ipl.ReceivePort;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Receive port for packet reception.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class PacketUpcallReceivePort implements MessageUpcall {
    private final static Logger logger = LoggerFactory
            .getLogger(PacketUpcallReceivePort.class);
    public static final PortType portType = new PortType(
            PortType.COMMUNICATION_RELIABLE, PortType.SERIALIZATION_OBJECT_SUN,
            PortType.CONNECTION_MANY_TO_ONE, PortType.RECEIVE_AUTO_UPCALLS,
            PortType.RECEIVE_EXPLICIT);

    private final ReceivePort port;
    private final ReceivedMessageQueue queue;
    private final Object watcher;

    /**
     * Constructs a new PacketUpcallReceive port.
     * 
     * @param ibis
     *            The Ibis the port will belong to.
     * @param portName
     *            The name of the port.
     * @param queue
     *            The queue to put the received messages on.
     * @param watcher
     *            The object that watches the queue, and should be notified when
     *            a new message arrives.
     * @throws IOException
     *             Thrown iff there is a problem creating the port.
     */
    public PacketUpcallReceivePort(final Ibis ibis, final String portName,
            final ReceivedMessageQueue queue, final Object watcher)
            throws IOException {
        this.queue = queue;
        this.watcher = watcher;
        port = ibis.createReceivePort(portType, portName, this);
    }

    /**
     * Handle the upcall of the ipl port. Only public because the interface
     * requires it.
     * 
     * @param msg
     *            The message to handle.
     * @throws IOException
     *             Thrown if for some reason the given message could not be
     *             read.
     */
    @Override
    public void upcall(final ReadMessage msg) throws IOException {
        Message data;
        try {
            data = (Message) msg.readObject();
        } catch (final ClassNotFoundException e) {
            final Throwable t = new Throwable();
            logger.error(
                    "Internal error: "
                            + "Cannot read message in upcall: class not found: "
                            + e.getLocalizedMessage(), t);
            return;
        }
        // msg.finish();
        data.source = msg.origin().ibisIdentifier();
        queue.add(data);
        Globals.receiverLogger.trace("Added to receive queue: " + data);
        synchronized (watcher) {
            watcher.notifyAll();
        }
    }

    /** Enable this port. */
    public void enable() {
        port.enableMessageUpcalls();
        port.enableConnections();
    }

    public void close() throws IOException {
        port.close();
    }

}
