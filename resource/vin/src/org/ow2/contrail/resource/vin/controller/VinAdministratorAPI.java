package org.ow2.contrail.resource.vin.controller;

import org.ow2.contrail.resource.vin.common.OutOfEntriesError;

/**
 * The interface of the Vin administrator.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public interface VinAdministratorAPI {
    /**
     * Create a new VIN controller. Such a controller should be created for each
     * application.
     * 
     * @return The newly constructed VIN controller.
     * @throws OutOfEntriesError
     *             Thrown if one of the pools has run out of errors.
     */
    public VinAPI createController() throws OutOfEntriesError;

    /**
     * Verify that there are no outstanding resources, such as allocated IP
     * addresses). Obviously, this should only be invoked when there are
     * supposedly no outstanding resources.
     * 
     * @return <code>true</code> iff everything is ok.
     */
    public boolean verifyNoOutstandingResources();
}
