package org.ow2.contrail.resource.vin.agent;

import java.io.File;

import org.ow2.contrail.resource.vin.agent.TunnelPool.TunnelPoolEntry;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.SanityChecks;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A network handler using iproute2 tunnels. This does not provide the strong
 * encryption of an IPSec tunnel, but in this mode much faster networks can be
 * realized.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class Iproute2GuestNetworkHandler implements NetworkHandler {
    private static final String CREATE_ROUTE_SCRIPT_NAME = "add-iproute2-guest-route.sh";
    private static final String REMOVE_ROUTE_SCRIPT_NAME = "remove-iproute2-guest-route.sh";
    private static final String CREATE_TUNNEL_SCRIPT_NAME = "add-iproute2-guest-tunnel.sh";
    private static final String REMOVE_TUNNEL_SCRIPT_NAME = "remove-iproute2-guest-tunnel.sh";
    private final TunnelPool tunnelPool;
    private final File scriptDir;
    private static final Logger logger = LoggerFactory
            .getLogger(Iproute2GuestNetworkHandler.class);

    Iproute2GuestNetworkHandler(final TunnelPool tunnelPool,
            final File scriptDir) {
        this.tunnelPool = tunnelPool;
        this.scriptDir = scriptDir;
    }

    @Override
    public void createNetwork(final AgentNetworkDescriptor network) {
        // Ignore
    }

    @Override
    public void removeNetwork(final AgentNetworkDescriptor network) {
        // Ignore
    }

    @Override
    public void createConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError,
            VinError {
        assert local != null;
        assert remote != null;
        final String hostName = remote.getHostName();
        if (hostName != null) {
            EtcHostsManager.addEntry(hostName, remote.getVINAddressString());
        }
        final String bridgeName = local.network.networkId;
        if (remote.isOnSameHost(local)) {
            logger.debug("connection: local=" + local + " remote=" + remote
                    + " is on same host. bridgeName=" + bridgeName);
        } else {
            final TunnelPoolEntry e = tunnelPool.addConnection(remote);
            final String tunnelName = e.getTunnelName();
            if (e.hasOneEntry()) {
                logger.debug("creating tunnel on host: local=" + local
                        + " remote=" + remote + " tunnelName=" + tunnelName);
                Utils.cleanlyExecuteScript(scriptDir,
                        CREATE_TUNNEL_SCRIPT_NAME, tunnelName,
                        local.getHostAddressString(),
                        remote.getHostAddressString());
                logger.trace("tunnel created: local=" + local + " remote="
                        + remote);
            }
            logger.debug("creating route over tunnel " + tunnelName);
            Utils.cleanlyExecuteScript(scriptDir, CREATE_ROUTE_SCRIPT_NAME,
                    tunnelName, local.getVINAddressString(),
                    remote.getVINAddressString());
            logger.trace("tunnel created: local=" + local + " remote=" + remote);
        }
    }

    @Override
    public void removeConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError,
            VinError {
        logger.debug("removing connection: local=" + local + " remote="
                + remote);
        final String hostName = remote.getHostName();
        if (hostName != null) {
            EtcHostsManager.removeEntry(hostName, remote.getVINAddressString());
        }
        if (remote.isOnSameHost(local)) {
            logger.debug("On same host, no action needed");
        } else {
            final TunnelPoolEntry e = tunnelPool.removeConnection(remote);
            final String tunnelName = e.getTunnelName();
            logger.debug("removing route from host: local=" + local
                    + " remote=" + remote + " tunnelName=" + tunnelName);
            Utils.cleanlyExecuteScript(scriptDir, REMOVE_ROUTE_SCRIPT_NAME,
                    tunnelName, local.getVINAddressString(),
                    remote.getVINAddressString());
            if (e.isEmpty()) {
                logger.debug("removing tunnel from host: local=" + local
                        + " remote=" + remote + " tunnelName=" + tunnelName);
                Utils.cleanlyExecuteScript(scriptDir,
                        REMOVE_TUNNEL_SCRIPT_NAME, tunnelName);
            }
        }
        logger.trace("connection removed: local=" + local + " remote=" + remote);
    }

    @Override
    public void createLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection) {
        // Ignore
    }

    @Override
    public void removeLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection)
            throws ConfigurationError {
        // Ignore
    }

    private boolean didSanityChecks = false;

    @Override
    public void doSanityChecks() throws ConfigurationError {
        if (!didSanityChecks) {
            didSanityChecks = true;
            SanityChecks.ensureFileContentMatches(new File(
                    "/proc/sys/net/ipv4/ip_forward"), "1\n");
            SanityChecks.ensureDirectoryExists(scriptDir, "interface scripts");
            SanityChecks
                    .ensureScriptIsSane(scriptDir, CREATE_ROUTE_SCRIPT_NAME);
            SanityChecks
                    .ensureScriptIsSane(scriptDir, REMOVE_ROUTE_SCRIPT_NAME);
            SanityChecks.ensureScriptIsSane(scriptDir,
                    CREATE_TUNNEL_SCRIPT_NAME);
            SanityChecks.ensureScriptIsSane(scriptDir,
                    REMOVE_TUNNEL_SCRIPT_NAME);
            logger.debug("System passed Iproute2 sanity checks");
        }
    }

    @Override
    public String getFriendlyName() {
        return "guest-iproute2";
    }
}
