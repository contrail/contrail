package org.ow2.contrail.resource.vin.launcher;

import java.util.Properties;

/**
 * An entry in the queue of launches that await the new laucher instance once it
 * has registered itself.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class LaunchQueueEntry {
    final Properties properties;
    final String launchId;

    /**
     * @param launchId
     * @param properties
     */
    LaunchQueueEntry(final String launchId, final Properties properties) {
        this.launchId = launchId;
        this.properties = properties;
    }

}
