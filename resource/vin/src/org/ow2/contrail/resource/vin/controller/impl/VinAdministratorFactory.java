package org.ow2.contrail.resource.vin.controller.impl;

import java.util.Properties;

import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.controller.VinAdministratorAPI;

/**
 * A class that manufactures VinAdministratorAPI instances. Although it is
 * technically possible to create multiple instances, this is rarely necessary.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class VinAdministratorFactory {
    /**
     * Given a set of properties, returns a new instance of a
     * VinAdministratorAPI.
     * 
     * @param properties
     *            The properties of the new VinAdministrator.
     * @return The new VinAdministratorAPI.
     * @throws VinError
     *             Thrown if there is some kind of configuration error.
     */
    public static VinAdministratorAPI createVinAdministrator(
            final Properties properties) throws VinError {
        assert properties != null;
        return new VinAdministrator(properties);
    }
}
