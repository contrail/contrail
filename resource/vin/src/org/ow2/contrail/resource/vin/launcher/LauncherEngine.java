package org.ow2.contrail.resource.vin.launcher;

import ibis.ipl.Ibis;
import ibis.ipl.IbisCapabilities;
import ibis.ipl.IbisFactory;
import ibis.ipl.IbisIdentifier;
import ibis.ipl.Registry;
import ibis.ipl.RegistryEventHandler;
import ibis.util.RunProcess;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.ow2.contrail.resource.vin.common.AgentErrorMessage;
import org.ow2.contrail.resource.vin.common.Defaults;
import org.ow2.contrail.resource.vin.common.Flag;
import org.ow2.contrail.resource.vin.common.GlobalGlobals;
import org.ow2.contrail.resource.vin.common.IbisServer;
import org.ow2.contrail.resource.vin.common.Message;
import org.ow2.contrail.resource.vin.common.PacketSendPort;
import org.ow2.contrail.resource.vin.common.PacketUpcallReceivePort;
import org.ow2.contrail.resource.vin.common.ReceivedMessageQueue;
import org.ow2.contrail.resource.vin.common.Transmitter;
import org.ow2.contrail.resource.vin.common.TransmitterListener;
import org.ow2.contrail.resource.vin.common.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class LauncherEngine extends Thread implements RegistryEventHandler, API,
        TransmitterListener {
    private static final IbisCapabilities ibisCapabilities = new IbisCapabilities(
            IbisCapabilities.MEMBERSHIP_UNRELIABLE,
            IbisCapabilities.ELECTIONS_STRICT);
    private final ReceivedMessageQueue receivedMessageQueue = new ReceivedMessageQueue(
            Settings.MAXIMAL_RECEIVED_MESSAGE_QUEUE_LENGTH);
    private final Flag stopped = new Flag(false);
    private final Transmitter transmitter;
    private final ConcurrentLinkedQueue<IbisIdentifier> deletedNodes = new ConcurrentLinkedQueue<IbisIdentifier>();
    private final PacketUpcallReceivePort receivePort;
    private final IbisIdentifier coordinatorIdentifier;
    private final Ibis localIbis;
    private final static Logger logger = LoggerFactory
            .getLogger(LauncherEngine.class);
    private boolean controllerIsGone = false;
    private final String hostId;
    private final IbisServer localIbisServer;
    private final LauncherAdministration launcherAdministration = new LauncherAdministration();
    private File launchCommand = Defaults.LAUNCH_AGENT_COMMAND_PATH;
    private final LaunchAdministration launchAdministration = new LaunchAdministration();

    /**
     * Creates a new launcher engine.
     * 
     * @param runIbisServer
     *            Should we run an Ibis server?
     * @param runHub
     *            Should we run an Ibis hub?
     * @param ibisPortNumber
     *            The port number the Ibis should run on.
     * @param launchCommand
     *            The launch script to use
     * @param hostId
     *            The identifier of this host.
     * @param verbose
     *            If true, be more verbose during execution.
     * 
     * @throws Exception
     *             Thrown on problems.
     */
    protected LauncherEngine(final boolean runIbisServer, final boolean runHub,
            final int ibisPortNumber, final File launchCommand,
            final String hostId, final boolean verbose) throws Exception {
        super("launcher thread");
        this.hostId = hostId;
        this.launchCommand = launchCommand;
        // FIXME: properly handle pool name.
        final String poolName = "launcherpool" + ibisPortNumber;
        System.setProperty("ibis.pool.name", poolName);
        if (runIbisServer) {
            localIbisServer = new IbisServer(verbose, ibisPortNumber, false);
        } else if (runHub) {
            localIbisServer = new IbisServer(verbose, ibisPortNumber, true);
        } else {
            localIbisServer = null;
        }
        transmitter = new Transmitter(this);
        final Properties ibisProperties = new Properties();
        localIbis = IbisFactory.createIbis(ibisCapabilities, ibisProperties,
                true, this, PacketSendPort.portType,
                PacketUpcallReceivePort.portType);
        final Registry registry = localIbis.registry();
        final IbisIdentifier myIbis = localIbis.identifier();
        if (runIbisServer) {
            coordinatorIdentifier = registry
                    .elect(Globals.COORDINATOR_ELECTION_NAME);
        } else {
            coordinatorIdentifier = registry
                    .getElectionResult(Globals.COORDINATOR_ELECTION_NAME);
        }
        receivePort = new PacketUpcallReceivePort(localIbis,
                GlobalGlobals.receivePortName, receivedMessageQueue, this);
        transmitter.start();
        registry.enableEvents();
        receivePort.enable();
        if (hostId != null) {
            // Since we're a launcher, register with the coordinator.
            final RegisterLauncherMessage msg = new RegisterLauncherMessage(
                    hostId);
            transmitter.addToDataQueue(coordinatorIdentifier, msg);
        }
        logger.info("Created launcher ibis " + myIbis + " "
                + Utils.getPlatformVersion() + " host "
                + Utils.getCanonicalHostname());
    }

    @Override
    public void died(final IbisIdentifier agent) {
        if (agent.equals(localIbis.identifier())) {
            if (!stopped.isSet()) {
                logger.error("This agent has been declared dead, we might as well stop");
                setStopped();
            }
        } else {
            transmitter.deleteNode(agent);
            deletedNodes.add(agent);
        }
        wakeTransmitterListenerThread(); // Something interesting has happened.
    }

    @Override
    public void left(final IbisIdentifier agent) {
        if (agent.equals(localIbis.identifier())) {
            if (!stopped.isSet()) {
                logger.error("This agent has been declared `left', we might as well stop");
                setStopped();
            }
        } else {
            transmitter.deleteNode(agent);
            deletedNodes.add(agent);
        }
        wakeTransmitterListenerThread(); // Something interesting has happened.
    }

    @Override
    public void electionResult(final String arg0, final IbisIdentifier arg1) {
        // Not interesting.
    }

    @Override
    public void gotSignal(final String arg0, final IbisIdentifier arg1) {
        // Not interesting.
    }

    @Override
    public void joined(final IbisIdentifier agent) {
        // Ignore
    }

    /**
     * Register the fact that no new agents can join this pool. Not relevant, so
     * we ignore this.
     */
    @Override
    public void poolClosed() {
        // Not interesting.
    }

    @Override
    public void poolTerminated(final IbisIdentifier arg0) {
        setStopped();
    }

    /**
     * This ibis was reported as 'may be dead'. Try not to communicate with it.
     * 
     * @param node
     *            The node that may be dead.
     */
    @Override
    public void setSuspect(final IbisIdentifier node) {
        logger.info("Node " + node + " may be dead", new Throwable());
        try {
            localIbis.registry().maybeDead(node);
        } catch (final IOException e) {
            // Nothing we can do about it.
        }
    }

    /**
     * Handle any new and deleted nodes that have been registered after the last
     * call.
     * 
     * @return <code>true</code> iff we made any changes to the node state.
     */
    private boolean registerNewAndDeletedNodes() {
        boolean changes = false;
        while (true) {
            final IbisIdentifier node = deletedNodes.poll();
            if (node == null) {
                break;
            }
            registerNodeLeft(node);
            changes = true;
        }
        return changes;
    }

    @Override
    public int setStopped() {
        stopped.set();
        wakeTransmitterListenerThread(); // Something interesting has happened.
        return 0;
    }

    /** Tell the engine thread that something interesting has happened. */
    @Override
    public void wakeTransmitterListenerThread() {
        synchronized (this) {
            notifyAll();
        }
    }

    private void handleSingleMessage(final Message msg) {
        logger.info("Handling message " + msg);
        if (msg instanceof LaunchAgentMessage) {
            final LaunchAgentMessage lam = (LaunchAgentMessage) msg;
            if (hostId != null && hostId.equals(lam.hostId)) {
                launchAgent(lam.launchId, lam.properties);
            }
        } else if (msg instanceof AgentLaunchedMessage) {
            final AgentLaunchedMessage alm = (AgentLaunchedMessage) msg;
            launchAdministration.registerAgentLaunched(alm.launchId);
            if (alm.exitStatus != 0) {
                logger.error("Launch failed: " + alm.msg + " exitStatus="
                        + alm.exitStatus);
                logger.error(" stdout:\n" + alm.stdoutString);
                logger.error(" stderr:'n" + alm.stderrString);
            }
        } else if (msg instanceof RegisterLauncherMessage) {
            final RegisterLauncherMessage lm = (RegisterLauncherMessage) msg;
            logger.debug("Launcher " + lm.hostId + " (" + lm.source
                    + ") is now online");
            launcherAdministration.registerLauncher(lm.source, lm.hostId,
                    transmitter);
        }
    }

    private void launchAgent(final String launchId, final Properties properties) {
        logger.debug("Launching new agent with properties " + properties
                + " launchId=" + launchId);
        final ArrayList<String> parameters = new ArrayList<String>();
        addPropertyParameters(parameters, properties);
        final String pl[] = parameters.toArray(new String[parameters.size()]);
        final RunProcess res = Utils.cleanlyExecuteCommand(launchCommand, pl);
        Message ackMsg = null;
        final int exitStatus = res.getExitStatus();
        if (exitStatus != 0) {
            final String msg = "Launch of agent failed. launchCommand="
                    + launchCommand + " parameters=" + parameters;
            logger.error(msg + " exitStatus=" + exitStatus);
            final String stdoutString = new String(res.getStdout());
            logger.error(" stdout: " + stdoutString);
            final String stderrString = new String(res.getStderr());
            logger.error(" stderr: " + stderrString);
            ackMsg = new AgentLaunchedMessage(launchId, exitStatus, msg,
                    stdoutString, stderrString);
        }
        if (ackMsg == null && launchId != null) {
            ackMsg = new AgentLaunchedMessage(launchId, exitStatus);
        }
        transmitter.addToDataQueue(coordinatorIdentifier, ackMsg);
    }

    private static void addPropertyParameters(
            final ArrayList<String> parameters, final Properties properties) {
        final Set<String> names = properties.stringPropertyNames();
        for (final String name : names) {
            final String val = properties.getProperty(name);
            final String s = "-D" + name + "=" + val;
            parameters.add(s);
        }
    }

    private boolean handleIncomingMessages() {
        boolean progress = false;
        while (true) {
            final Message msg = receivedMessageQueue.getNext();
            if (msg == null) {
                break;
            }
            logger.info("Get incoming message from queue: " + msg);
            handleSingleMessage(msg);
            progress = true;
        }
        return progress;
    }

    private void registerNodeLeft(final IbisIdentifier node) {
        if (coordinatorIdentifier != null && node.equals(coordinatorIdentifier)) {
            controllerIsGone = true;
        }
    }

    @Override
    public Ibis getLocalIbis() {
        return localIbis;
    }

    @Override
    public void run() {
        final int sleepTime = Settings.MAXIMAL_ENGINE_SLEEP_INTERVAL;
        try {
            while (!stopped.isSet()) {
                boolean progress;
                do {
                    // Keep doing bookkeeping chores until all is done.
                    final boolean progressIncoming = handleIncomingMessages();
                    final boolean progressNodeChurn = registerNewAndDeletedNodes();
                    progress = progressIncoming || progressNodeChurn;
                    if (Settings.traceDetailedProgress) {
                        if (progress) {
                            logger.trace("Main loop: progress=true incoming="
                                    + progressIncoming + " churn="
                                    + progressNodeChurn);
                            if (progressIncoming) {
                                logger.info(receivedMessageQueue.formatCounts());
                            }
                        }
                    }
                } while (progress);
                if (controllerIsGone) {
                    logger.info("Setting engine to stopped state");
                    stopped.set();
                    break;
                }
                synchronized (this) {
                    if (receivedMessageQueue.isEmpty() && !stopped.isSet()
                            && deletedNodes.isEmpty()) {
                        try {
                            this.wait(sleepTime);
                        } catch (final InterruptedException e) {
                            // Ignored.
                        }
                    }
                }
            }
        } catch (final Throwable e) {
            final Message msg = new AgentErrorMessage(hostId,
                    "Agent is fatally wounded", e);
            logger.error("Agent is fatally wounded", e);
            transmitter.sendEmergencyMessage(coordinatorIdentifier, msg);
        } finally {
            logger.info("Shutting down transmitter");
            transmitter.setStopped();
            try {
                transmitter.join(Settings.TRANSMITTER_SHUTDOWN_TIMEOUT);
            } catch (final InterruptedException e) {
                // ignore.
            }
            logger.info("Transmitter has shut down");
            try {
                receivePort.close();
                logger.info("Receive port was closed");
                if (localIbisServer != null) {
                    localIbisServer.stop();
                    logger.info("Ibis server/hub was stopped");
                }
                localIbis.end();
            } catch (final IOException x) {
                // Nothing we can do about it.
            }
        }
        transmitter.printStatistics();
        Utils.printThreadStats(logger);
    }

    @Override
    public String requestAgentLaunch(final boolean getIdentifier,
            final String host, final Properties properties) {
        LauncherInfo info = launcherAdministration.getLauncherInfo(host);
        final String launchId;
        if (getIdentifier) {
            launchId = launchAdministration.getLaunchIdentifier();
        } else {
            launchId = null;
        }
        if (info == null) {
            info = launcherAdministration.registerNewHost(host);
        }
        if (info.ibisIdentifier == null) {
            info.registerFutureLaunch(launchId, properties);
        } else {
            final LaunchAgentMessage msg = new LaunchAgentMessage(launchId,
                    host, properties);
            transmitter.addToDataQueue(info.ibisIdentifier, msg);
        }
        return launchId;
    }

    @Override
    public int waitForLaunch(final String id) {
        return launchAdministration.waitForLaunch(id);
    }
}
