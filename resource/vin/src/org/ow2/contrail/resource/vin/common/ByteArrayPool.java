package org.ow2.contrail.resource.vin.common;

import java.util.ArrayList;

import org.ow2.contrail.resource.vin.annotations.TestAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Maintains a pool of multi-byte values that can be requested and returned
 * (`recycled').
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class ByteArrayPool {
    private final ArrayValue startValue;
    private final ArrayValue endValue;
    private ArrayValue nextValue;
    private final ArrayList<ArrayValueRange> recycleBin = new ArrayList<ArrayValueRange>();
    private final String friendlyName;
    private static final Logger logger = LoggerFactory
            .getLogger(ByteArrayPool.class);

    protected ByteArrayPool(final ArrayValue startBytes,
            final ArrayValue endBytes, final String friendlyName) {
        assert startBytes.size() == endBytes.size();
        this.startValue = startBytes;
        this.nextValue = startBytes;
        this.endValue = endBytes;
        this.friendlyName = friendlyName;
    }

    final ArrayValue getNextValue() throws OutOfEntriesError {
        ArrayValue res;
        if (recycleBin.isEmpty()) {
            if (ArrayValue.areEqual(nextValue, endValue)) {
                dumpRecycleBin();
                throw new OutOfEntriesError("No free addresses in pool");
            }
            res = nextValue;
            nextValue = nextValue.increment();
        } else {
            final ArrayValueRange e = recycleBin.get(0);
            if (e.isSingleValue()) {
                res = e.startValue;
                recycleBin.remove(0);
            } else {
                res = e.extractFirstValue();
            }
        }
        return res;
    }

    @TestAPI
    static final int computeNextAlignedValue(final int val,
            final int alignmentBits) {
        final int res = (val + (1 << alignmentBits) - 1) >> alignmentBits;
        return res << alignmentBits;
    }

    private static final ArrayValue computeNextAlignedValue(
            final ArrayValue nextValue, final int alignmentBits,
            final ArrayValue alignmentMask) {
        ArrayValue res = nextValue.addOffset((1 << alignmentBits) - 1);
        res = computePreviousAlignedValue(res, alignmentMask);
        return res;
    }

    private static ArrayValue computePreviousAlignedValue(final ArrayValue v,
            final ArrayValue alignmentMask) {
        assert v.size() == alignmentMask.size();
        final int res[] = new int[v.size()];
        int i = v.size();
        while (i > 0) {
            i--;
            res[i] = v.get(i) & ~alignmentMask.get(i);
        }
        return new ArrayValue(res);
    }

    public final ArrayValueRange getNextSubrange(final long members,
            final int alignmentBits) throws OutOfEntriesError {
        final ArrayValue alignmentMask = ArrayValue.buildAlignmentMask(
                alignmentBits, nextValue.size());
        final ArrayValue rangeStartValue = computeNextAlignedValue(nextValue,
                alignmentBits, alignmentMask);
        final ArrayValueRange unalignedRange;
        logger.debug("getNextSubrange: members=" + members + " alignmentBits="
                + alignmentBits + " alignmentMask=" + alignmentMask
                + " rangeStartValue=" + rangeStartValue);
        if (nextValue.equals(rangeStartValue)) {
            logger.debug("no unaligned range");
            unalignedRange = null;
        } else {
            final ArrayValue endRecycled = rangeStartValue.decrement();
            unalignedRange = new ArrayValueRange(nextValue, endRecycled);
            nextValue = rangeStartValue;
        }
        final ArrayValue rangeEndValue = computeNextAlignedValue(
                nextValue.addOffset(members), alignmentBits, alignmentMask)
                .decrement();
        logger.debug("unalignedRange=" + unalignedRange + " rangeEndValue="
                + rangeEndValue + " endValue=" + endValue);
        if (ArrayValue.isGreaterThan(endValue, rangeEndValue)) {
            dumpRecycleBin();
            throw new OutOfEntriesError("Insufficient free addresses in "
                    + friendlyName);
        }
        nextValue = rangeEndValue.increment();
        if (unalignedRange != null) {
            // Put the range we could not use in the recycle bin.
            addToRecyclebin(unalignedRange);
        }
        return new ArrayValueRange(rangeStartValue, rangeEndValue);
    }

    /**
     * Given a range of values, return the position in the list where this range
     * should be inserted.
     * 
     * @param list
     *            The list to search a position in.
     * @param e
     *            The range to search a position for.
     * @return The position in the list to insert the entry.
     */
    private static int findInsertionPosition(
            final ArrayList<ArrayValueRange> list, final ArrayValueRange e) {
        final int sz = list.size();
        if (sz == 0) {
            return 0;
        }
        int l = 0;
        int h = sz;
        while (l + 1 < h) {
            final int mid = (l + h) / 2;
            final int c = ArrayValue.compare(list.get(mid).startValue,
                    e.startValue);
            if (c > 0) {
                // Value is smaller than the mid-array value.
                h = mid;
            } else {
                l = mid;
            }
        }
        final int c = ArrayValue.compare(list.get(l).startValue, e.startValue);
        return c > 0 ? l : l + 1;
    }

    /**
     * Keep a sorted list of entries, by inserting new entries at the right
     * position.
     * 
     * @param val
     *            The value to return to the recycle bin.
     */
    private void addEntryToRecyclebin(final ArrayValue val) {
        final ArrayValueRange e = new ArrayValueRange(val);
        addToRecyclebin(e);
    }

    public final void addToRecyclebin(ArrayValueRange e) {
        if (e != null) {
            int pos = findInsertionPosition(recycleBin, e);
            recycleBin.add(pos, e);
            if (pos > 0) {
                final ArrayValueRange prev = recycleBin.get(pos - 1);
                final ArrayValueRange m = prev.tryToMergeWith(e);
                if (m != null) {
                    logger.debug("range " + prev + " and " + e
                            + " can be merged into " + m);
                    // Ranges could be merged, do it.
                    recycleBin.set(pos - 1, m);
                    recycleBin.remove(pos);
                    pos--;
                    e = m;
                }
            }
            if (pos + 1 < recycleBin.size()) {
                final ArrayValueRange next = recycleBin.get(pos + 1);
                final ArrayValueRange m = next.tryToMergeWith(e);
                if (m != null) {
                    // Ranges could be merged, do it.
                    recycleBin.set(pos, m);
                    recycleBin.remove(pos + 1);
                }
            }
        }
        cleanupRecyclebin();
    }

    /**
     * Given a value, return <code>true</code> iff the value is one of the
     * outstanding values of this pool.
     * 
     * @param val
     *            The value to examine.
     * @return <code>true</code> iff the value is outstanding.
     */
    private boolean isOutstandingValue(final ArrayValue val) {
        if (ArrayValue.isGreaterOrEqual(nextValue, val)) {
            return false;
        }
        for (final ArrayValueRange e : recycleBin) {
            if (e.contains(val)) {
                return false;
            }
        }
        return true;
    }

    private void cleanupRecyclebin() {
        /*
         * Now try to reduce the size of the recycle bin by decreasing
         * nextValue.
         */
        if (!recycleBin.isEmpty()) {
            final int ix = recycleBin.size() - 1;
            final ArrayValueRange r = recycleBin.get(ix);
            if (r.endValue.hasAsNextValue(nextValue)) {
                logger.debug("We can absort range " + r
                        + " in the implicit range: nextValue=" + nextValue);
                nextValue = r.startValue;
                recycleBin.remove(ix);
                dumpRecycleBin();
            }
        }
    }

    final void recycleValue(final ArrayValue val) {
        if (!isOutstandingValue(val)) {
            logger.error("Not an outstanding value: val=" + val);
            return;
        }
        if (val.hasAsNextValue(nextValue)) {
            logger.debug("We can absort val=" + val
                    + " in the implicit range: nextValue=" + nextValue);
            nextValue = val;
            cleanupRecyclebin();
            return;
        }
        addEntryToRecyclebin(val);
    }

    /**
     * Returns true iff the pool is in pristine state: no entries in the recycle
     * bin, and nextValue is equal to startValue.
     * 
     * This method is used by the unit tests.
     * 
     * @return Whether the pool is in pristine state.
     */
    public final boolean isInPristineState() {
        final boolean res = recycleBin.isEmpty()
                && ArrayValue.areEqual(nextValue, startValue);
        if (!res) {
            dumpRecycleBin();
            logger.debug("startValue=" + (startValue) + " nextValue="
                    + (nextValue));
        }
        return res;
    }

    private void dumpRecycleBin() {
        for (int i = 0; i < recycleBin.size(); i++) {
            logger.debug(" recycleBin[" + i + "]=" + recycleBin.get(i));
        }
    }

    /**
     * Return <code>true</code> iff the pool is in canonical state: the recycle
     * bin has ordered and non-overlapping entries.
     * 
     * @return <code>true</code> iff the pool is in canonical state.
     */
    @TestAPI
    final boolean isInCanonicalState() {
        if (!recycleBin.isEmpty()) {
            ArrayValueRange prev = recycleBin.get(0);
            for (int i = 1; i < recycleBin.size(); i++) {
                final ArrayValueRange e = recycleBin.get(i);
                if (prev.tryToMergeWith(e) != null) {
                    logger.error("Entries in recyclebin could be merged: prev="
                            + prev + " e=" + e + " i=" + i);
                    dumpRecycleBin();
                    return false;
                }
                if (ArrayValue.isLessOrEqual(prev.startValue, e.startValue)) {
                    logger.error("Entries in recyclebin unsorted: prev=" + prev
                            + " e=" + e + " i=" + i);
                    dumpRecycleBin();
                    return false;
                }
                prev = e;
            }
            if (ArrayValue.isGreaterOrEqual(nextValue, prev.endValue)
                    || prev.endValue.hasAsNextValue(nextValue)) {
                logger.error("Recyclebin could be pruned: nextValue="
                        + nextValue);
                dumpRecycleBin();
                return false;
            }
        }
        return true;
    }
}
