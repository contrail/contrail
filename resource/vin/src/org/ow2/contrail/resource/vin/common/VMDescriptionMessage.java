package org.ow2.contrail.resource.vin.common;

import java.util.Properties;

/**
 * A message from a coordinator to an agent, telling it about the virtual
 * machine of that agent.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class VMDescriptionMessage extends Message {
    private static final long serialVersionUID = 1L;

    /** In what context it the agent of this vm run. */
    public final AgentContextType context;

    /**
     * The properties of the VM.
     */
    public final Properties properties;

    public VMDescriptionMessage(final AgentContextType context,
            final Properties properties) {
        this.context = context;
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "VMDescriptionMessage[properties=" + properties + "]";
    }

}
