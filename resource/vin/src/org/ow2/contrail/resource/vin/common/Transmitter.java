package org.ow2.contrail.resource.vin.common;

import ibis.ipl.IbisIdentifier;

/**
 * This class handles the transmission of messages to other nodes in the Ibis
 * network, including retries if necessary.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class Transmitter extends Thread {
    private final SendQueue dataQueue = new SendQueue();
    private final SendQueue retryQueue = new SendQueue();
    private final PacketSendPort sendPort;
    private final TransmitterListener listener;
    private final NodeSet deadNodes = new NodeSet();
    private long idleTime = 0L;
    // private final String role;
    private boolean sentMessages = false;
    private final Flag shuttingDown = new Flag(false);
    private final Flag stopped = new Flag(false);

    /**
     * Constructs a new transmitter with the given listener and role
     * description. The listener is informed by the transmitter about
     * problematic nodes.
     * 
     * @param listener
     *            The listener class.
     */
    public Transmitter(final TransmitterListener listener) {
        super("Transmitter thread");
        setDaemon(true);
        sendPort = new PacketSendPort(listener);
        this.listener = listener;
        setPriority(Thread.NORM_PRIORITY + 1);
    }

    /**
     * Add the given message to the transmission queue, to be transmitted to the
     * given destination/
     * 
     * @param destination
     *            The destination of the message.
     * @param msg
     *            The message to transmit.
     */
    public void addToDataQueue(final IbisIdentifier destination,
            final Message msg) {
        assert destination != null;
        if (shuttingDown.isSet()) {
            Globals.transmitterLogger
                    .error("Someone tries to send messages while we are shutting down",
                            new Throwable());
        } else {
            Globals.transmitterLogger.debug("Transmitter: put on data queue: "
                    + msg);
            dataQueue.add(destination, msg);
            wakeTransmitter();
        }
    }

    public void sendEmergencyMessage(final IbisIdentifier destination,
            final Message msg) {
        // Emergency messages have priory.
        Globals.transmitterLogger
                .debug("Transmitter: put emergency message on data queue: "
                        + msg);
        dataQueue.addFirst(destination, msg);
        // This thread now has maximum priority.
        setPriority(MAX_PRIORITY);
        wakeTransmitter();
    }

    /**
     * The interval between wakes in ms, if the transmitter isn't woken up for
     * work.
     */
    private static final long TRANSMITTER_WAKE_INTERVAL = 5000;

    /**
     * The sleep interval if there are retry entries in the queue.
     */
    private static final long RETRY_WAKE_INTERVAL = 100;

    /**
     * Sends the given queued message.
     * 
     * @param qm
     *            The queued message to send.
     * @return <code>true</code> iff we added to the retry queue.
     */
    private boolean sendMessage(final QueuedMessage qm) {
        if (deadNodes.contains(qm.destination)) {
            Globals.transmitterLogger
                    .warn("Transmitter dropped message to dead node: " + qm);
            return false;
        }
        Globals.transmitterLogger.info("Transmitter: sending: " + qm);
        final Message msg = qm.msg;
        final boolean ok = sendPort.sendMessage(qm.destination, msg);
        if (!ok) {
            qm.retries++;
            if (qm.retries > Settings.MAXIMAL_SEND_RETRIES) {
                Globals.transmitterLogger.warn("Dropped message after "
                        + Settings.MAXIMAL_SEND_RETRIES + " retries");
                listener.setSuspect(qm.destination);
            }
            if (shuttingDown.isSet()) {
                /*
                 * While shutting down, don't retry, but simply ban problematic
                 * nodes.
                 */
                deadNodes.add(qm.destination);
            } else {
                retryQueue.add(qm);
            }
            return true;
        }
        return false;
    }

    /**
     * Delete the given node from our administration.
     * 
     * @param node
     *            The node to delete.
     */
    public void deleteNode(final IbisIdentifier node) {
        deadNodes.add(node);
    }

    private synchronized void wakeTransmitter() {
        notifyAll();
    }

    @Override
    public void run() {
        try {
            while (true) {
                boolean addedToRetriesQueue = false;
                while (true) {
                    // Request messages have top priority.
                    if (!dataQueue.isEmpty()) {
                        // Data messages have priority over retries.
                        final QueuedMessage msg = dataQueue.getNext();
                        addedToRetriesQueue |= sendMessage(msg);
                        sentMessages = true;
                    } else if (!addedToRetriesQueue && !retryQueue.isEmpty()) {
                        /*
                         * Retries have lowest priority. To avoid being stuck in
                         * a retry loop, only handle retry messages if no new
                         * ones have been queued.
                         */
                        final QueuedMessage msg = retryQueue.getNext();
                        addedToRetriesQueue |= sendMessage(msg);
                        sentMessages = true;
                    } else {
                        break;
                    }
                }
                listener.wakeTransmitterListenerThread();
                try {
                    Globals.transmitterLogger.trace("Transmitter: waiting");
                    synchronized (this) {
                        if (dataQueue.isEmpty() && retryQueue.isEmpty()) {
                            final long startWaitTime = System.nanoTime();
                            final long sleepTime = shuttingDown.isSet() ? 10
                                    : retryQueue.isEmpty() ? TRANSMITTER_WAKE_INTERVAL
                                            : RETRY_WAKE_INTERVAL;
                            wait(sleepTime);
                            if (sentMessages) {
                                final long waitTime = System.nanoTime()
                                        - startWaitTime;
                                idleTime += waitTime;
                            }
                        }
                    }
                } catch (final InterruptedException e) {
                    // ignore.
                }
                Globals.transmitterLogger.trace("Transmitter: looping");
            }
        } finally {
            Globals.transmitterLogger.debug("Closing send port");
            sendPort.close();
            Globals.transmitterLogger.debug("Send port is now closed");
        }

    }

    private void waitForQueueToDrain() {
        while (true) {
            synchronized (this) {
                if (dataQueue.isEmpty()) {
                    break;
                }
                try {
                    wait(RETRY_WAKE_INTERVAL);
                } catch (final InterruptedException e) {
                    // Ignore
                }
            }
        }
    }

    private void printQueueStatistics() {
        dataQueue.printStatistics(Globals.transmitterLogger, "data queue");
        retryQueue.printStatistics(Globals.transmitterLogger, "retry queue");
    }

    /**
     * Print a brief state overview to the logger.
     */
    public synchronized void dumpState() {
        Globals.transmitterLogger.trace("Transmitter queues: data="
                + dataQueue.size() + " retries=" + retryQueue.size()
                + " transmitter idle time: "
                + Utils.formatSeconds(idleTime * 1e-9));
        printQueueStatistics();
    }

    /**
     * Print some statistics of this transmitter.
     */
    public synchronized void printStatistics() {
        sendPort.printStatistics(Globals.transmitterLogger, "transmitter");
        Globals.transmitterLogger.debug("Transmitter idle time: "
                + Utils.formatSeconds(idleTime * 1e-9));
        printQueueStatistics();
    }

    /**
     * Remove any pending entries from the queue. Since this obviously involves
     * unrecoverable loss of information, this method is only useful during
     * shutdown of the node.
     */
    public void setStopped() {
        shuttingDown.set();
        retryQueue.clear();
        wakeTransmitter();
        waitForQueueToDrain();
        stopped.set();
        wakeTransmitter();
    }
}
