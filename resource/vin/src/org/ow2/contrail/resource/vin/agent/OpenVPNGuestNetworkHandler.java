package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.SanityChecks;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A network handler using iproute2 tunnels. This does not provide the strong
 * encryption of an IPSec tunnel, but in this mode much faster networks can be
 * realized.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class OpenVPNGuestNetworkHandler implements NetworkHandler {
    private static final String ADD_CONNECTION_SCRIPT_NAME = "add-openvpn-guest-connection";
    private static final String REMOVE_CONNECTION_SCRIPT_NAME = "remove-openvpn-guest-connection";
    private final File scriptDir;
    private final String debugString;
    private static final Logger logger = LoggerFactory
            .getLogger(OpenVPNGuestNetworkHandler.class);
    private static final String PID_FILE_NAME = "pid-filename";

    OpenVPNGuestNetworkHandler(final Properties properties, final File scriptDir)
            throws ConfigurationError {
        this.scriptDir = scriptDir;
        try {
            this.debugString = Utils.getOptionalBooleanProperty(properties,
                    PropertyNames.DEBUG_FLAG, "Debug tracing", false) ? "-"
                    : "dx";
        } catch (final VinError e) {
            throw new ConfigurationError("Invalid debug flag '"
                    + PropertyNames.DEBUG_FLAG + "'", e);
        }
    }

    private static String buildTunnelName(
            final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) {
        final String name = local.network.networkId + remote.connectionId;
        return name;
    }

    private static String createPidFilename() throws VinInternalError {
        try {
            final String pidFilename = File.createTempFile("vin-pid", ".pid")
                    .getAbsolutePath();
            return pidFilename;
        } catch (final IOException e) {
            throw new VinInternalError("Cannot create pid file", e);
        }
    }

    @Override
    public void createNetwork(final AgentNetworkDescriptor network) {
        // Ignore
    }

    @Override
    public void removeNetwork(final AgentNetworkDescriptor network) {
        // Ignore
    }

    @Override
    public void createConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError {
        logger.debug("creating connection: local=" + local + " remote="
                + remote);
        assert local != null;
        assert remote != null;
        final String pidFilename = createPidFilename();
        final String tunnel = buildTunnelName(local, remote);
        // FIXME: also pass a port number.
        Utils.cleanlyExecuteScript(scriptDir, ADD_CONNECTION_SCRIPT_NAME,
                debugString, tunnel, remote.getHostAddressString(),
                local.getVINAddressString(), remote.getVINAddressString(),
                pidFilename);
        logger.trace("connection created: local=" + local + " remote=" + remote);
        remote.setProperty(PID_FILE_NAME, pidFilename);
    }

    @Override
    public void removeConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError {
        logger.debug("removing connection: local=" + local + " remote="
                + remote);
        final String pidFilename = remote.getProperty(PID_FILE_NAME);
        Utils.cleanlyExecuteScript(scriptDir, REMOVE_CONNECTION_SCRIPT_NAME,
                debugString, pidFilename);
        logger.trace("connection destroyed: local=" + local + " remote="
                + remote);
    }

    @Override
    public void createLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection) {
        // Ignore
    }

    @Override
    public void removeLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection)
            throws ConfigurationError {
        // Ignore
    }

    private boolean didSanityChecks = false;

    @Override
    public void doSanityChecks() throws ConfigurationError {
        if (!didSanityChecks) {
            didSanityChecks = true;
            SanityChecks.ensureFileContentMatches(new File(
                    "/proc/sys/net/ipv4/ip_forward"), "1\n");
            SanityChecks.ensureDirectoryExists(scriptDir, "interface scripts");
            SanityChecks.ensureScriptIsSane(scriptDir,
                    ADD_CONNECTION_SCRIPT_NAME);
            SanityChecks.ensureScriptIsSane(scriptDir,
                    REMOVE_CONNECTION_SCRIPT_NAME);
            logger.debug("System passed openVPN sanity checks");
        }
    }

    @Override
    public String getFriendlyName() {
        return "guest-openvpn";
    }
}
