package org.ow2.contrail.resource.vin.controller.tester;

import org.ow2.contrail.resource.vin.common.VinError;

abstract class VINNetworkInfo {
    abstract void destroy() throws VinError;
}