package org.ow2.contrail.resource.vin.common;

/**
 * A message from a coordinator to an agent, telling that a particular agent
 * should be removed.
 * 
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class StopAgentMessage extends Message {
    private static final long serialVersionUID = 1L;
    public final String agentId;

    public StopAgentMessage(final String agentId) {
        this.agentId = agentId;
    }

    @Override
    public String toString() {
        return "StopMessage[networkId=" + agentId + "]";
    }
}
