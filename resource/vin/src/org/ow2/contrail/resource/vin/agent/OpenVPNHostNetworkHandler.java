package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ArrayValue;
import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.SanityChecks;
import org.ow2.contrail.resource.vin.common.Subnet;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A network handler using OpenVPN tunnels.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class OpenVPNHostNetworkHandler implements NetworkHandler {
    private static final String CREATE_LOCAL_CONNECTION_CONFIG_NAME = "add-openvpn-host-guest.conf";
    private static final String CREATE_CONNECTION_CONFIG_NAME = "add-openvpn-host-route.conf";
    private static final String CREATE_NETWORK_CONFIG_NAME = "create-openvpn-host-network.conf";
    private static final String REMOVE_NETWORK_CONFIG_NAME = "remove-openvpn-host-network.conf";
    private static final String REMOVE_LOCAL_CONNECTION_CONFIG_NAME = "remove-openvpn-host-guest.conf";
    private static final String REMOVE_CONNECTION_CONFIG_NAME = "remove-openvpn-host-route.conf";
    private static final String START_VPN_CONFIG_NAME = "start-openvpn.conf";
    private static final String STOP_VPN_CONFIG_NAME = "stop-openvpn.conf";
    private final File configDir;
    private final boolean debug;
    private final ArrayValueRange macAddresses;
    private final File safeconfigPath;
    private final File lockfilePath;
    private static final Logger logger = LoggerFactory
            .getLogger(OpenVPNHostNetworkHandler.class);
    private static final String PID_FILE_NAME = "pid-filename";

    OpenVPNHostNetworkHandler(final Properties properties,
            final File configDir, final File safeconfigPath,
            final ArrayValueRange macAddresses) throws ConfigurationError {
        this.configDir = configDir;
        this.safeconfigPath = safeconfigPath;
        this.macAddresses = macAddresses;
        // FIXME: don't use an open and hard-coded lockfiles directory.
        // Perhaps it is sufficient to own contrail-vin
        this.lockfilePath = new File("/tmp/contrail-vin/lockfiles");
        lockfilePath.mkdirs();
        try {
            this.debug = Utils.getOptionalBooleanProperty(properties,
                    PropertyNames.DEBUG_FLAG, "Debug tracing", false);
        } catch (final VinError e) {
            throw new ConfigurationError("Invalid debug flag '"
                    + PropertyNames.DEBUG_FLAG + "'", e);
        }
    }

    private static String buildTunnelName(
            final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) {
        final String name = local.network.networkId + remote.connectionId;
        return name;
    }

    private static String createPidFilename() throws VinInternalError {
        try {
            final String pidFilename = File.createTempFile("vin-pid", ".pid")
                    .getAbsolutePath();
            return pidFilename;
        } catch (final IOException e) {
            throw new VinInternalError("Cannot create pid file", e);
        }
    }

    private static void writeNetworkXMLFile(final Subnet subnet,
            final String domain, final PrintWriter output,
            final String bridgeName, final ArrayValueRange macAddresses) {
        final String bridgeIpAddress = subnet.getBridgeAddressString();
        final String netMask = subnet.getNetmaskString();
        output.println("<network>");
        output.println(" <name>" + bridgeName + "</name>");
        output.println(" <bridge name='" + bridgeName + "'/>");
        output.println(" <forward mode='route'/>");
        output.println(" <ip address='" + bridgeIpAddress + "' netmask='"
                + netMask + "'>");
        output.println("  <dhcp>");
        output.println("   <range start='" + subnet.getFreeRangeStartString()
                + "' end='" + subnet.getFreeRangeEndString() + "'/>");
        {
            ArrayValue counter = subnet.getStartValue();
            ArrayValue macAddress = macAddresses.startValue;
            int i = 0;
            while (ArrayValue.isLessOrEqual(subnet.getEndValue(), counter)) {
                /*
                 * Skip the two special values in the address range. Since we
                 * allocated MAC addresses to them, it is easier to start
                 * counting at the start, and do this skip.
                 */
                if (i > 1) {
                    final String vmName = "vm" + i;
                    final String name = vmName + "." + domain;
                    final String ipAddress = counter.makeIP4AddressString();
                    final String macAddressString = macAddress
                            .buildBuildMACAddressString();
                    output.println("   <host mac=\"" + macAddressString
                            + "\" ip=\"" + ipAddress + "\" name=\"" + name
                            + "\"/>");
                }
                counter = counter.increment();
                macAddress = macAddress.increment();
                i++;
            }
        }
        output.println("  </dhcp>");
        output.println(" </ip>");
        output.println("</network>");
    }

    private static void writeNetworkXMLFile(final String bridgeName,
            final AgentNetworkDescriptor network,
            final ArrayValueRange macAddresses, final File f)
            throws IOException, VinInternalError {
        final PrintWriter output = new PrintWriter(f);
        try {
            final Subnet subnet = network.subnet;
            writeNetworkXMLFile(subnet, network.domain, output, bridgeName,
                    macAddresses);
        } finally {
            output.close();
        }
    }

    @Override
    public void createLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor connection)
            throws ConfigurationError {
        final String networkId = connection.network.networkId;
        logger.debug("adding connection to VM: connectionId="
                + connection.connectionId + " networkId=" + networkId);
        Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                CREATE_LOCAL_CONNECTION_CONFIG_NAME, networkAdmin.libvirtId,
                networkId, connection.getMacAddress());
        logger.debug("added connection to VM: connectionId="
                + connection.connectionId + " networkId="
                + connection.network.networkId);
    }

    @Override
    public void removeLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor connection)
            throws ConfigurationError {
        logger.debug("removing connection to VM: connectionId="
                + connection.connectionId + " networkId="
                + connection.network.networkId);
        Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                REMOVE_LOCAL_CONNECTION_CONFIG_NAME, networkAdmin.libvirtId,
                connection.getMacAddress());
        logger.debug("removed connection to VM: connectionId="
                + connection.connectionId + " networkId="
                + connection.network.networkId);
    }

    @Override
    public void createConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError {
        assert local != null;
        assert remote != null;
        final String bridgeName = local.network.networkId;
        if (remote.isOnSameHost(local)) {
            logger.debug("connection: local=" + local + " remote=" + remote
                    + " is on same host. bridgeName=" + bridgeName);
        } else {
            final String pidFilename = createPidFilename();
            final String tunnelName = buildTunnelName(local, remote);
            logger.debug("creating route on host: local=" + local + " remote="
                    + remote + " tunnelName=" + tunnelName + " bridgeName="
                    + bridgeName);
            // FIXME: also pass a port number.
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    START_VPN_CONFIG_NAME, tunnelName,
                    remote.getHostAddressString(), local.getVINAddressString(),
                    remote.getVINAddressString(), pidFilename);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    CREATE_CONNECTION_CONFIG_NAME, tunnelName,
                    local.getVINAddressString(), remote.getVINAddressString(),
                    local.network.subnet.getNetmaskBitsString(), bridgeName);
            logger.trace("connection created: local=" + local + " remote="
                    + remote);
            remote.setProperty(PID_FILE_NAME, pidFilename);
        }
    }

    @Override
    public void removeConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError {
        logger.debug("removing connection: local=" + local + " remote="
                + remote);
        if (remote.isOnSameHost(local)) {
            logger.debug("On same host, no action needed");
        } else {
            final String tunnelName = buildTunnelName(local, remote);
            final AgentNetworkDescriptor network = local.network;
            final String bridgeName = network.networkId;
            final String pidFilename = remote.getProperty(PID_FILE_NAME);
            logger.debug("removing route from host: local=" + local
                    + " remote=" + remote + " tunnelName=" + tunnelName);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    REMOVE_CONNECTION_CONFIG_NAME, tunnelName,
                    local.getVINAddressString(), remote.getVINAddressString(),
                    network.subnet.getNetmaskBitsString(), bridgeName);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    STOP_VPN_CONFIG_NAME, pidFilename);
        }
        logger.trace("connection removed: local=" + local + " remote=" + remote);
    }

    @Override
    public void createNetwork(final AgentNetworkDescriptor network)
            throws ConfigurationError {
        final File lockfile = new File(lockfilePath, network.networkId);
        final boolean succeeded;
        try {
            succeeded = lockfile.createNewFile();
            if (succeeded) {
                network.setLockfile(lockfile);
            }
        } catch (final IOException x) {
            throw new VinInternalError(
                    "Trying to create a lockfile causes an exception", x);
        }
        if (succeeded) {
            final String bridgeName = network.networkId;
            File mappingFile;
            try {
                mappingFile = File.createTempFile("vinnetwork", ".xml");
                writeNetworkXMLFile(bridgeName, network, macAddresses,
                        mappingFile);
                logger.debug("Wrote a libvirt network XML file for subnet "
                        + network.subnet + " macAddresses " + macAddresses
                        + " to file " + mappingFile);
            } catch (final IOException e) {
                throw new VinInternalError("Cannot create mapping file", e);
            }
            logger.debug("creating network " + network.networkId);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    CREATE_NETWORK_CONFIG_NAME, mappingFile.getAbsolutePath(),
                    bridgeName, network.getBridgeIPAddress(),
                    network.getNetmask());
            logger.debug("created network " + network.networkId);
        } else {
            logger.debug("Network " + network.networkId
                    + " was already created");
        }
    }

    @Override
    public void removeNetwork(final AgentNetworkDescriptor network)
            throws ConfigurationError {
        final File lockfile = network.getLockfile();
        if (lockfile != null) {
            final String bridgeName = network.networkId;
            logger.debug("removing network " + network.networkId);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    REMOVE_NETWORK_CONFIG_NAME, bridgeName);
            logger.debug("removed network " + network.networkId);
            lockfile.delete();
            network.setLockfile(null);
        }
    }

    private boolean didSanityChecks = false;

    @Override
    public void doSanityChecks() throws ConfigurationError {
        if (!didSanityChecks) {
            didSanityChecks = true;
            SanityChecks.ensureFileContentMatches(new File(
                    "/proc/sys/net/ipv4/ip_forward"), "1\n");
            SanityChecks.ensureDirectoryExists(configDir, "interface configs");
            SanityChecks.ensureConfigsAreSane(configDir,
                    CREATE_NETWORK_CONFIG_NAME, REMOVE_NETWORK_CONFIG_NAME,
                    CREATE_LOCAL_CONNECTION_CONFIG_NAME,
                    REMOVE_LOCAL_CONNECTION_CONFIG_NAME,
                    CREATE_CONNECTION_CONFIG_NAME,
                    REMOVE_CONNECTION_CONFIG_NAME, START_VPN_CONFIG_NAME,
                    STOP_VPN_CONFIG_NAME);
            logger.debug("System passed openVPN sanity checks");
        }
    }

    @Override
    public String getFriendlyName() {
        return "OpenVPN";
    }
}
