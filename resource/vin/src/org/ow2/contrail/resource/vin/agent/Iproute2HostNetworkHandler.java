package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import org.ow2.contrail.resource.vin.agent.TunnelPool.TunnelPoolEntry;
import org.ow2.contrail.resource.vin.common.ArrayValue;
import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.GlobalGlobals;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.SanityChecks;
import org.ow2.contrail.resource.vin.common.Subnet;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A network handler using iproute2 tunnels. This does not provide the strong
 * encryption of an IPSec tunnel, but in this mode much faster networks can be
 * realized.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class Iproute2HostNetworkHandler implements NetworkHandler {
    private static final String CREATE_NETWORK_CONFIG_NAME = "create-iproute2-host-network.conf";
    private static final String REMOVE_NETWORK_CONFIG_NAME = "remove-iproute2-host-network.conf";
    private static final String CREATE_LOCAL_CONNECTION_CONFIG_NAME = "add-iproute2-host-guest.conf";
    private static final String REMOVE_LOCAL_CONNECTION_CONFIG_NAME = "remove-iproute2-host-guest.conf";
    private static final String ADD_ROUTE_CONFIG_NAME = "add-iproute2-host-route.conf";
    private static final String REMOVE_ROUTE_CONFIG_NAME = "remove-iproute2-host-route.conf";
    private static final String CREATE_TUNNEL_CONFIG_NAME = "add-iproute2-host-tunnel.conf";
    private static final String REMOVE_TUNNEL_CONFIG_NAME = "remove-iproute2-host-tunnel.conf";
    private final File configDir;
    private final boolean debug;
    private final ArrayValueRange macAddresses;
    private final File safeconfigPath;
    private final File lockfilePath;
    private final TunnelPool tunnelPool;
    private boolean didSanityChecks = false;
    private final String libvirtUri;

    private static final Logger logger = LoggerFactory
            .getLogger(Iproute2HostNetworkHandler.class);

    Iproute2HostNetworkHandler(final TunnelPool tunnelPool,
            final Properties properties, final File configDir,
            final File safeconfigPath, final ArrayValueRange macAddresses)
            throws ConfigurationError {
        this.tunnelPool = tunnelPool;
        this.configDir = configDir;
        this.safeconfigPath = safeconfigPath;
        this.macAddresses = macAddresses;
        // FIXME: don't use an open and hard-coded lockfiles directory.
        // Perhaps it is sufficient to own contrail-vin
        this.lockfilePath = new File("/tmp/contrail-vin/lockfiles");
        lockfilePath.mkdirs();
        try {
            this.debug = Utils.getOptionalBooleanProperty(properties,
                    PropertyNames.DEBUG_FLAG, "Debug tracing", false);
        } catch (final VinError e) {
            throw new ConfigurationError("Invalid debug flag '"
                    + PropertyNames.DEBUG_FLAG + "'", e);
        }
        String uri = System.getenv(GlobalGlobals.LIBVIRT_URI_ENV_NAME);
        if (uri == null) {
            uri = "qemu:///system";
        }
        libvirtUri = uri;
    }

    static void writeNetworkXMLFile(final Subnet subnet, final String domain,
            final PrintWriter output, final String bridgeName,
            final ArrayValueRange macAddresses) {
        final String bridgeIpAddress = subnet.getBridgeAddressString();
        final String netMask = subnet.getNetmaskString();
        output.println("<network>");
        output.println(" <name>" + bridgeName + "</name>");
        output.println(" <bridge name='" + bridgeName + "'/>");
        output.println(" <forward mode='route'/>");
        output.println(" <ip address='" + bridgeIpAddress + "' netmask='"
                + netMask + "'>");
        output.println("  <dhcp>");
        output.println("   <range start='" + subnet.getFreeRangeStartString()
                + "' end='" + subnet.getFreeRangeEndString() + "'/>");
        {
            ArrayValue counter = subnet.getStartValue();
            ArrayValue macAddress = macAddresses.startValue;
            int i = 0;
            while (ArrayValue.isLessThan(subnet.getEndValue(), counter)) {
                /*
                 * Skip the two special values in the address range. Since we
                 * allocated MAC addresses to them, it is easier to start
                 * counting at the start, and do this skip.
                 */
                if (i > 1) {
                    // FIXME: dig up a name and dynamically change DNS (hah!)
                    final String vmName = "vm" + i;
                    final String name = vmName + "." + domain;
                    final String ipAddress = counter.makeIP4AddressString();
                    final String macAddressString = macAddress
                            .buildBuildMACAddressString();
                    output.println("   <host mac=\"" + macAddressString
                            + "\" ip=\"" + ipAddress + "\" name=\"" + name
                            + "\"/>");
                }
                counter = counter.increment();
                macAddress = macAddress.increment();
                i++;
            }
        }
        output.println("  </dhcp>");
        output.println(" </ip>");
        output.println("</network>");
    }

    private static void writeNetworkXMLFile(final String bridgeName,
            final AgentNetworkDescriptor network,
            final ArrayValueRange macAddresses, final File f)
            throws IOException, VinInternalError {
        final PrintWriter output = new PrintWriter(f);
        try {
            final Subnet subnet = network.subnet;
            writeNetworkXMLFile(subnet, network.domain, output, bridgeName,
                    macAddresses);
        } finally {
            output.close();
        }
    }

    private static void writeDeviceXMLFile(final PrintWriter output,
            final String networkName, final String macAddress) {
        output.println("<interface type='network'>");
        output.println("  <mac address='" + macAddress + "'/>");
        final String deviceType = "virtio";
        // ne2k_pci,i82551,i82557b,i82559er,rtl8139,e1000,pcnet,virtio
        // final String deviceType = "ne2k_pci";
        output.println("  <model type='" + deviceType + "'/>");
        output.println("  <source network='" + networkName + "'/>");
        output.println("</interface>");
    }

    private static void writeDeviceXMLFile(final String bridgeName,
            final String macAddress, final File f) throws IOException,
            VinInternalError {
        final PrintWriter output = new PrintWriter(f);
        try {
            writeDeviceXMLFile(output, bridgeName, macAddress);
        } finally {
            output.close();
        }
    }

    @Override
    public void createLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor connection)
            throws ConfigurationError {
        final String networkId = connection.network.networkId;
        final String macAddress = connection.getMacAddress();
        logger.debug("adding connection to VM: connectionId="
                + connection.connectionId + " networkId=" + networkId);
        File mappingFile;
        try {
            mappingFile = File.createTempFile("vindevice", ".xml");
            writeDeviceXMLFile(networkId, macAddress, mappingFile);
            logger.debug("Wrote a libvirt device XML file for macAddress "
                    + macAddress + " to file " + mappingFile);
        } catch (final IOException e) {
            throw new VinInternalError("Cannot create mapping file", e);
        }
        Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                CREATE_LOCAL_CONNECTION_CONFIG_NAME, libvirtUri,
                networkAdmin.libvirtId, mappingFile.getAbsolutePath());
        logger.debug("added connection to VM: connectionId="
                + connection.connectionId + " networkId="
                + connection.network.networkId);
    }

    @Override
    public void removeLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor connection)
            throws ConfigurationError {
        logger.debug("removing connection to VM: connectionId="
                + connection.connectionId + " networkId="
                + connection.network.networkId);
        Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                REMOVE_LOCAL_CONNECTION_CONFIG_NAME, libvirtUri,
                networkAdmin.libvirtId, connection.getMacAddress());
        logger.debug("removed connection to VM: connectionId="
                + connection.connectionId + " networkId="
                + connection.network.networkId);
    }

    @Override
    public void createConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError {
        assert local != null;
        assert remote != null;
        final String bridgeName = local.network.networkId;
        if (remote.isOnSameHost(local)) {
            logger.debug("connection: local=" + local + " remote=" + remote
                    + " is on same host. bridgeName=" + bridgeName);
        } else {
            final TunnelPoolEntry e = tunnelPool.addConnection(remote);
            final String tunnelName = e.getTunnelName();
            if (e.hasOneEntry()) {
                logger.debug("creating tunnel on host: local=" + local
                        + " remote=" + remote + " tunnelName=" + tunnelName);
                Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                        CREATE_TUNNEL_CONFIG_NAME, tunnelName,
                        local.getHostAddressString(),
                        remote.getHostAddressString());
                logger.trace("tunnel created: local=" + local + " remote="
                        + remote);
            }
            logger.debug("creating route over tunnel " + tunnelName);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    ADD_ROUTE_CONFIG_NAME, tunnelName,
                    local.getVINAddressString(), remote.getVINAddressString(),
                    local.network.subnet.getNetmaskBitsString(), bridgeName);
            logger.trace("tunnel created: local=" + local + " remote=" + remote);
        }
    }

    @Override
    public void removeConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError {
        logger.debug("removing connection: local=" + local + " remote="
                + remote);
        if (remote.isOnSameHost(local)) {
            logger.debug("On same host, no action needed");
        } else {
            final TunnelPoolEntry e = tunnelPool.removeConnection(remote);
            final String tunnelName = e.getTunnelName();
            final String bridgeName = local.network.networkId;
            logger.debug("removing route from host: local=" + local
                    + " remote=" + remote + " tunnelName=" + tunnelName);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    REMOVE_ROUTE_CONFIG_NAME, tunnelName,
                    local.getVINAddressString(), remote.getVINAddressString(),
                    local.network.subnet.getNetmaskBitsString(), bridgeName);
            if (e.isEmpty()) {
                logger.debug("removing tunnel from host: local=" + local
                        + " remote=" + remote + " tunnelName=" + tunnelName);
                Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                        REMOVE_TUNNEL_CONFIG_NAME, tunnelName);
            }
        }
        logger.trace("connection removed: local=" + local + " remote=" + remote);
    }

    @Override
    public void createNetwork(final AgentNetworkDescriptor network)
            throws ConfigurationError {
        final File lockfile = new File(lockfilePath, network.networkId);
        final boolean succeeded;
        try {
            succeeded = lockfile.createNewFile();
            if (succeeded) {
                network.setLockfile(lockfile);
            }
        } catch (final IOException x) {
            throw new VinInternalError(
                    "Trying to create a lockfile causes an exception", x);
        }
        if (succeeded) {
            final String bridgeName = network.networkId;
            File mappingFile;
            try {
                mappingFile = File.createTempFile("vinnetwork", ".xml");
                writeNetworkXMLFile(bridgeName, network, macAddresses,
                        mappingFile);
                logger.debug("Wrote a libvirt network XML file for subnet "
                        + network.subnet + " macAddresses " + macAddresses
                        + " to file " + mappingFile);
            } catch (final IOException e) {
                throw new VinInternalError("Cannot create mapping file", e);
            }
            logger.debug("creating network " + network.networkId);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    CREATE_NETWORK_CONFIG_NAME, mappingFile.getAbsolutePath(),
                    libvirtUri, bridgeName, network.getBridgeIPAddress(),
                    network.getNetmask());
            logger.debug("created network " + network.networkId);
        } else {
            logger.debug("Network " + network.networkId
                    + " was already created");
        }
    }

    @Override
    public void removeNetwork(final AgentNetworkDescriptor network)
            throws ConfigurationError {
        final File lockfile = network.getLockfile();
        if (lockfile != null) {
            final String bridgeName = network.networkId;
            logger.debug("removing network " + network.networkId);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    REMOVE_NETWORK_CONFIG_NAME, libvirtUri, bridgeName);
            logger.debug("removed network " + network.networkId);
            lockfile.delete();
            network.setLockfile(null);
        }
    }

    @Override
    public void doSanityChecks() throws ConfigurationError {
        if (!didSanityChecks) {
            didSanityChecks = true;
            SanityChecks.ensureFileContentMatches(new File(
                    "/proc/sys/net/ipv4/ip_forward"), "1\n");
            SanityChecks.ensureDirectoryExists(configDir, "interface configs");
            SanityChecks.ensureConfigsAreSane(configDir,
                    CREATE_NETWORK_CONFIG_NAME, REMOVE_NETWORK_CONFIG_NAME,
                    CREATE_LOCAL_CONNECTION_CONFIG_NAME,
                    REMOVE_LOCAL_CONNECTION_CONFIG_NAME,
                    CREATE_TUNNEL_CONFIG_NAME, REMOVE_TUNNEL_CONFIG_NAME,
                    REMOVE_ROUTE_CONFIG_NAME, ADD_ROUTE_CONFIG_NAME);
            logger.debug("System passed Iproute2 sanity checks");
        }
    }

    @Override
    public String getFriendlyName() {
        return "iproute2";
    }
}
