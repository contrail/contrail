package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.util.Properties;

import org.ow2.contrail.resource.vin.agent.TunnelPool.TunnelPoolEntry;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.GlobalGlobals;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.SanityChecks;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A network handler using iproute2 tunnels. This does not provide the strong
 * encryption of an IPSec tunnel, but in this mode much faster networks can be
 * realized.
 * 
 * @author Kees van Reeuwijk
 * @author Teodor Crivat
 * 
 */
class Iproute2PhysicalNetworkHandler implements NetworkHandler {
    private static final String ADD_ADDRESS_CONFIG_NAME = "add-iproute2-physical-address.conf";
    private static final String REMOVE_ADDRESS_CONFIG_NAME = "remove-iproute2-physical-address.conf";
    private static final String ADD_ROUTE_CONFIG_NAME = "add-iproute2-physical-route.conf";
    private static final String REMOVE_ROUTE_CONFIG_NAME = "remove-iproute2-physical-route.conf";
    private static final String CREATE_TUNNEL_CONFIG_NAME = "add-iproute2-physical-tunnel.conf";
    private static final String REMOVE_TUNNEL_CONFIG_NAME = "remove-iproute2-physical-tunnel.conf";
    // present
    private final File configDir;
    private final boolean debug;
    private final File safeconfigPath;
    private final TunnelPool tunnelPool;
    private boolean didSanityChecks = false;
    private static final Logger logger = LoggerFactory
            .getLogger(Iproute2PhysicalNetworkHandler.class);

    Iproute2PhysicalNetworkHandler(final TunnelPool tunnelPool,
            final Properties properties, final File configDir,
            final File safeconfigPath) throws ConfigurationError {
        this.tunnelPool = tunnelPool;
        this.configDir = configDir;
        this.safeconfigPath = safeconfigPath;
        try {
            this.debug = Utils.getOptionalBooleanProperty(properties,
                    PropertyNames.DEBUG_FLAG, "Debug tracing", false);
        } catch (final VinError e) {
            throw new ConfigurationError("Invalid debug flag '"
                    + PropertyNames.DEBUG_FLAG + "'", e);
        }
    }

    private static String buildLocalInterfaceName(final String networkId,
            final String vmId) {
        final String id = networkId + "-" + vmId;
        return id;
    }

    @Override
    public void createLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection)
            throws ConfigurationError, VinError {
        final String localInterfaceName = buildLocalInterfaceName(
                localConnection.network.networkId, localConnection.vmId);
        final String localVINAddressString =
                localConnection.getVINAddressString();
        logger.debug("adding local VIN address " + localVINAddressString);
        Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                ADD_ADDRESS_CONFIG_NAME, localInterfaceName,
                localVINAddressString);
        logger.trace("local VIN address " + localVINAddressString + " added");
    }

    @Override
    public void removeLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection)
            throws ConfigurationError, VinError {
        final String localInterfaceName = buildLocalInterfaceName(
                localConnection.network.networkId, localConnection.vmId);
        final String localVINAddressString =
                localConnection.getVINAddressString();
        logger.debug("removing local VIN address " + localVINAddressString);
        Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                REMOVE_ADDRESS_CONFIG_NAME, localInterfaceName,
                localVINAddressString);
        logger.trace("local VIN address " + localVINAddressString + " removed");
    }

    @Override
    public void createConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError {
        assert local != null;
        assert remote != null;
        final String bridgeName = local.network.networkId;
        if (remote.isOnSameHost(local)) {
            logger.debug("connection: local=" + local + " remote=" + remote
                    + " is on same host. bridgeName=" + bridgeName);
        } else {
            final TunnelPoolEntry e = tunnelPool.addConnection(remote);
            final String tunnelName = e.getTunnelName();
            if (e.hasOneEntry()) {
                logger.debug("creating tunnel on host: local=" + local
                        + " remote=" + remote + " tunnelName=" + tunnelName);
                Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                        CREATE_TUNNEL_CONFIG_NAME, tunnelName,
                        local.getHostAddressString(),
                        remote.getHostAddressString());
                logger.trace("tunnel created: local=" + local + " remote="
                        + remote);
            }
            logger.debug("creating route over tunnel " + tunnelName);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    ADD_ROUTE_CONFIG_NAME, tunnelName,
                    local.getVINAddressString(), remote.getVINAddressString());
            logger.trace("tunnel created: local=" + local + " remote=" + remote);
        }
    }

    @Override
    public void removeConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError {
        logger.debug("removing connection: local=" + local + " remote="
                + remote);
        if (remote.isOnSameHost(local)) {
            logger.debug("On same host, no action needed");
        } else {
            final TunnelPoolEntry e = tunnelPool.removeConnection(remote);
            final String tunnelName = e.getTunnelName();
            final String bridgeName = local.network.networkId;
            logger.debug("removing route from host: local=" + local
                    + " remote=" + remote + " tunnelName=" + tunnelName);
            Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                    REMOVE_ROUTE_CONFIG_NAME, tunnelName,
                    local.getVINAddressString(), remote.getVINAddressString());
            if (e.isEmpty()) {
                logger.debug("removing tunnel from host: local=" + local
                        + " remote=" + remote + " tunnelName=" + tunnelName);
                Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                        REMOVE_TUNNEL_CONFIG_NAME, tunnelName);
            }
        }
        logger.trace("connection removed: local=" + local + " remote=" + remote);
    }

    @Override
    public void createNetwork(final AgentNetworkDescriptor network)
            throws ConfigurationError {
        // Nothing
    }

    @Override
    public void removeNetwork(final AgentNetworkDescriptor network)
            throws ConfigurationError {
        // Nothing
    }

    @Override
    public void doSanityChecks() throws ConfigurationError {
        if (!didSanityChecks) {
            didSanityChecks = true;
            SanityChecks.ensureDirectoryExists(configDir, "interface configs");
            SanityChecks.ensureConfigsAreSane(configDir,
                    ADD_ADDRESS_CONFIG_NAME, REMOVE_ADDRESS_CONFIG_NAME,
                    CREATE_TUNNEL_CONFIG_NAME, REMOVE_TUNNEL_CONFIG_NAME,
                    REMOVE_ROUTE_CONFIG_NAME, ADD_ROUTE_CONFIG_NAME);
            logger.debug("System passed Iproute2 sanity checks");
        }
    }

    @Override
    public String getFriendlyName() {
        return "iproute2";
    }
}
