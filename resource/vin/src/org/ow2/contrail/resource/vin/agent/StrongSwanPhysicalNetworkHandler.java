package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.SanityChecks;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tunnel creation handler using StrongSwan.
 *
 * @author Kees van Reeuwijk
 * @author Teodor Crivat
 *
 */
class StrongSwanPhysicalNetworkHandler implements NetworkHandler {
    private static final String ADD_ADDRESS_CONFIG_NAME = "add-strongswan-physical-address.conf";
    private static final String REMOVE_ADDRESS_CONFIG_NAME = "remove-strongswan-physical-address.conf";

    private final StrongSwanDaemonTalker daemonTalker;
    private final String networkId;
    private final File configDir;
    private final boolean debug;
    private final File safeconfigPath;
    private boolean didSanityChecks;
    private static final Logger logger = LoggerFactory
            .getLogger(StrongSwanPhysicalNetworkHandler.class);

    StrongSwanPhysicalNetworkHandler(final StrongSwanDaemonTalker daemonTalker,
            final String networkId, final Properties properties,
            final File configDir, final File safeconfigPath)
            throws ConfigurationError {
        this.daemonTalker = daemonTalker;
        this.networkId = networkId;
        this.configDir = configDir;
        this.safeconfigPath = safeconfigPath;
        try {
            this.debug = Utils.getOptionalBooleanProperty(properties,
                    PropertyNames.DEBUG_FLAG, "Debug tracing", false);
        } catch (final VinError e) {
            throw new ConfigurationError("Invalid debug flag '"
                    + PropertyNames.DEBUG_FLAG + "'", e);
        }
    }

    private static String buildConnectionIdentifier(final String networkId,
            final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) {
        assert local != null;
        assert remote != null;
        final String id = networkId + "-link-" + local.ordinal
                + '-' + remote.ordinal;
        return id;
    }

    private static String buildLocalInterfaceName(final String networkId,
            final String vmId) {
        final String id = networkId + "-" + vmId;
        return id;
    }

    private boolean shouldCreateTunnel(
            final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) {
        /* returns true if the local address is lower */
        return local.getVINAddressString().compareTo(
                remote.getVINAddressString()) < 0;
    }

    @Override
    public void createConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError,
            VinError {
        assert local != null;
        assert remote != null;
        final String bridgeName = local.network.networkId;
        if (remote.isOnSameHost(local)) {
            logger.debug("connection: local=" + local + " remote=" + remote
                    + " is on same host. bridgeName=" + bridgeName);
        } else {
            /* only one end of the tunnel should initiate the connection */
            if (shouldCreateTunnel(local, remote)) {
                final String connectionId = buildConnectionIdentifier(networkId, local,
                        remote);
                logger.debug("creating tunnel on host: local=" + local + " remote="
                        + remote);
                daemonTalker.createConnection(connectionId, local, remote);
                logger.trace("tunnel created: id=" + connectionId + " local="
                        + local + " remote=" + remote);
            }
        }
    }

    @Override
    public void removeConnection(final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws ConfigurationError,
            VinError {
        logger.debug("removing connection: local=" + local + " remote="
                + remote);
        assert local != null;
        assert remote != null;
        if (remote.isOnSameHost(local)) {
            logger.debug("On same host, no action needed");
        } else {
            /* the same end of the tunnel should terminate the connection */
            if (shouldCreateTunnel(local, remote)) {
                final String connectionId = buildConnectionIdentifier(networkId, local,
                        remote);
                logger.debug("removing tunnel from host: local=" + local + " remote="
                        + remote);
                daemonTalker.removeConnection(connectionId);
                logger.trace("tunnel removed: local=" + local + " remote="
                        + remote);
            }
        }
        logger.trace("connection removed: local=" + local + " remote=" + remote);
    }

    @Override
    public void createNetwork(final AgentNetworkDescriptor network) {
        // Nothing
    }

    @Override
    public void removeNetwork(final AgentNetworkDescriptor network) {
        // Nothing
    }

    @Override
    public void createLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection)
            throws ConfigurationError, VinError {
        final String localInterfaceName = buildLocalInterfaceName(networkId,
                localConnection.vmId);
        final String localVINAddressString =
                localConnection.getVINAddressString();
        logger.debug("adding local VIN address " + localVINAddressString);
        Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                ADD_ADDRESS_CONFIG_NAME, localInterfaceName,
                localVINAddressString);
        logger.trace("local VIN address " + localVINAddressString + " added");
        final String connectionId = localConnection.network.networkId;
        logger.debug("creating receiving connection");
        daemonTalker.createReceivingConnection(
                connectionId, localConnection);
        logger.trace("created receiving connection: id=" + connectionId
                + " local=" + localConnection);
    }

    @Override
    public void removeLocalConnection(
            final AgentNetworkAdministration networkAdmin,
            final AgentConnectionDescriptor localConnection)
            throws ConfigurationError, VinError {
        final String localInterfaceName = buildLocalInterfaceName(networkId,
                localConnection.vmId);
        final String localVINAddressString =
                localConnection.getVINAddressString();
        logger.debug("removing local VIN address " + localVINAddressString);
        Utils.cleanlyRunConfig(safeconfigPath, debug, configDir,
                REMOVE_ADDRESS_CONFIG_NAME, localInterfaceName,
                localVINAddressString);
        logger.trace("local VIN address " + localVINAddressString + " removed");
        final String connectionId = localConnection.network.networkId;
        logger.debug("removing receiving connection");
        daemonTalker.removeConnection(connectionId);
        logger.trace("removed receiving connection: id=" + connectionId
                + " local=" + localConnection);
    }

    @Override
    public void doSanityChecks() throws ConfigurationError {
        if (!didSanityChecks) {
            didSanityChecks = true;
            SanityChecks.ensureDirectoryExists(configDir, "interface configs");
            SanityChecks.ensureConfigsAreSane(configDir,
                    ADD_ADDRESS_CONFIG_NAME, REMOVE_ADDRESS_CONFIG_NAME);
            logger.debug("System passed strongSwan sanity checks");
        }
    }

    @Override
    public String getFriendlyName() {
        return "StrongSwan";
    }
}
