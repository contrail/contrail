package org.ow2.contrail.resource.vin.common;

class Settings {
    /** Message transmission timeout in ms on essential communications. */
    static final int COMMUNICATION_TIMEOUT = 5000;

    protected static final int MAXIMAL_SEND_RETRIES = 10;

    /** Do we cache connections? */
    static final boolean CACHE_CONNECTIONS = true;

    /** The number of connections we maximally keep open. */
    static final int CONNECTION_CACHE_SIZE = 50;

    /** How many cache accesses unused before the entry is evicted. */
    static final int CONNECTION_CACHE_MAXIMAL_UNUSED_COUNT = 2000;

}
