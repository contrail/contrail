package org.ow2.contrail.resource.vin.common;

import ibis.ipl.server.ServerProperties;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Server running on the local machine (inside the central controller).
 * 
 */
public class IbisServer {
    private static final Logger logger = LoggerFactory
            .getLogger(IbisServer.class);

    private final ibis.ipl.server.Server server;

    public IbisServer(final boolean verbose, final int port,
            final boolean hubOnly) throws Exception {
        logger.debug("Starting built-in Ibis server");

        final Properties properties = new Properties();
        properties.put(ServerProperties.HUB_ONLY, Boolean.toString(hubOnly));
        properties.put(ServerProperties.PRINT_ERRORS, "true");
        properties.put(ServerProperties.PORT, Integer.toString(port));

        if (verbose) {
            properties.put(ServerProperties.PRINT_EVENTS, "true");
            properties.put(ServerProperties.PRINT_STATS, "true");
        }

        server = new ibis.ipl.server.Server(properties);

        logger.debug(server.toString());
    }

    /**
     * Get the address of this server.
     * 
     * @return the address of this server
     * 
     * 
     */
    public String getAddress() {
        return server.getAddress();
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Local Server @ " + getAddress();
    }

    /*
     * public void addListener(final StateListener listener) { if (listener !=
     * null) { // signal listener we're done deploying.
     * listener.stateUpdated(State.DEPLOYED, null); } }
     * 
     * public State getState() { return State.DEPLOYED; }
     */

    public void stop() {
        server.end(1000);
    }
}
