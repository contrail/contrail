package org.ow2.contrail.resource.vin.launcher;

import java.util.HashMap;

class LaunchAdministration {
    private int launchIdCounter = 0;
    private final HashMap<String, LaunchRequest> pendingLaunches = new HashMap<String, LaunchRequest>();

    /**
     * An object for a particular launch request.
     * 
     * @author Kees van Reeuwijk
     * 
     */
    private static final class LaunchRequest {
        boolean launched = false;

        synchronized void setLaunched() {
            launched = true;
            notifyAll();
        }

        synchronized void waitForLaunch() {
            while (!launched) {
                try {
                    wait();
                } catch (final InterruptedException e) {
                    // Ignore
                }
            }
        }

    }

    @SuppressWarnings("synthetic-access")
    String getLaunchIdentifier() {
        final String id = "id" + launchIdCounter++;
        final LaunchRequest rq = new LaunchRequest();
        pendingLaunches.put(id, rq);
        return id;
    }

    int waitForLaunch(final String launchId) {
        if (launchId != null) {
            final LaunchRequest rq = pendingLaunches.get(launchId);
            rq.waitForLaunch();
            pendingLaunches.remove(launchId);
        }
        return 0;
    }

    void registerAgentLaunched(final String launchId) {
        if (launchId != null) {
            final LaunchRequest rq = pendingLaunches.get(launchId);
            if (rq != null) {
                rq.setLaunched();
            }
        }
    }
}
