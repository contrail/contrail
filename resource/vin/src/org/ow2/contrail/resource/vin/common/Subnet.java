package org.ow2.contrail.resource.vin.common;

public interface Subnet {

    String getBridgeAddressString();

    String getNetmaskBitsString();

    String getNetmaskString();

    ArrayValue getStartValue();

    ArrayValue getEndValue();

    String getFreeRangeStartString();

    String getFreeRangeEndString();

    long getAddressCount();

    IPAddressRangePool createPool(String friendlyName) throws OutOfEntriesError;

    String getSubnetAddressString();

}
