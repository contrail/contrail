package org.ow2.contrail.resource.vin.controller.impl;

import ibis.ipl.IbisIdentifier;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.AgentContextType;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.Message;
import org.ow2.contrail.resource.vin.common.Transmitter;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.controller.Globals;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class UsedHostAdministration {
    private final AddressRanges hostAddressRanges;
    /**
     * A map from host address to host entry.
     */
    private final ArrayList<AgentInfo> agents = new ArrayList<AgentInfo>();
    private static final Logger logger = LoggerFactory
            .getLogger(ControllerEngine.class);

    public UsedHostAdministration(final AddressRanges hostAddressRanges) {
        this.hostAddressRanges = hostAddressRanges;
    }

    /**
     * A list of connections that could not be broadcast yet because the
     * necessary information isn't there.
     */
    private final List<PendingConnection> pendingConnections = new ArrayList<PendingConnection>();

    private static class PendingConnection {
        final String virtualMachineId;
        final String networkId;
        final String connectionId;

        PendingConnection(final String virtualMachineId,
                final String networkId, final String connectionId) {
            this.virtualMachineId = virtualMachineId;
            this.networkId = networkId;
            this.connectionId = connectionId;
        }
    }

    private static int searchAgentsForIbis(final List<AgentInfo> l,
            final IbisIdentifier node) {
        for (int i = 0; i < l.size(); i++) {
            final AgentInfo workerInfo = l.get(i);
            if (workerInfo != null && workerInfo.ibisIdentifier != null
                    && node.equals(workerInfo.ibisIdentifier)) {
                return i;
            }
        }
        return -1;
    }

    private static int searchAgentsForIdentifier(final List<AgentInfo> l,
            final String agentId) {
        for (int i = 0; i < l.size(); i++) {
            final AgentInfo workerInfo = l.get(i);
            if (workerInfo != null && agentId.equals(workerInfo.agentId)) {
                return i;
            }
        }
        return -1;
    }

    void removeIbis(final IbisIdentifier agent) {
        final int ix = searchAgentsForIbis(agents, agent);
        if (ix >= 0) {
            final AgentInfo info = agents.get(ix);
            if (info != null) {
                info.setDeleted();
            }
        }
    }

    private InetAddress selectHostAddress(final InetAddress[] addresses)
            throws VinError {
        // FIXME: selecting the proper host address requires more care
        if (addresses.length == 1) {
            // A choice between one addresses is easy.
            return addresses[0];
        }
        for (final InetAddress a : addresses) {
            if (hostAddressRanges.contains(a)) {
                return a;
            }
        }
        throw new VinError(
                "None of the Agent addresses matches a host address: agent addresses: "
                        + Arrays.deepToString(addresses) + " hostAddresses: "
                        + hostAddressRanges);
    }

    void onAgentIsAvailable(final IbisIdentifier ibis,
            final ControllerNetworkAdministration privateNetworkAdministration,
            final String agentId, final InetAddress[] addresses,
            final Transmitter transmitter) throws VinError {
        final int ix = searchAgentsForIdentifier(agents, agentId);
        if (ix >= 0) {
            Globals.administrationLogger
                    .debug("An agent is now registered: ibis=" + ibis
                            + " agentId=" + agentId);
            final AgentInfo agent = agents.get(ix);
            agent.setIbisIdentifier(ibis);
            final InetAddress hostAddress = selectHostAddress(addresses);
            agent.setAddress(hostAddress);
            // Send all queued messages for this agent now.
            for (final Message msg : agent.localQueue) {
                transmitter.addToDataQueue(ibis, msg);
            }
            agent.localQueue.clear();
            final Iterator<PendingConnection> it = pendingConnections
                    .iterator();
            while (it.hasNext()) {
                final PendingConnection c = it.next();
                if (c.virtualMachineId.equals(agentId)) {
                    broadcastConnection(privateNetworkAdministration,
                            c.networkId, c.connectionId, transmitter);
                    it.remove();
                }
            }
        } else {
            Globals.administrationLogger
                    .error("An unknown agent tried to register: ibis=" + ibis
                            + " agentId=" + agentId);
            Globals.administrationLogger.error("++ Known agents: "
                    + agents.toString());
        }
    }

    void registerMachine(final String vmId, final AgentContextType contextType,
            final Properties properties) {
        final AgentInfo agent = new AgentInfo(vmId, contextType, properties);
        Globals.administrationLogger.debug("Register new machine: " + agent
                + " context=" + contextType + " properties=" + properties);
        agents.add(agent);
    }

    void removeMachine(final String machineId, final Transmitter transmitter) {
        for (final AgentInfo agent : agents) {
            if (agent.isActive()) {
                agent.sendStopMessage(machineId, transmitter);
            }
        }
    }

    void sendPrivateNetworkInfo(
            final ControllerNetworkAdministration privateNetworkAdministration,
            final String networkId, final Transmitter transmitter)
            throws VinError {
        for (final AgentInfo agent : agents) {
            if (agent.isActive()) {
                agent.sendPrivateNetworkDescriptionMessage(
                        privateNetworkAdministration, networkId, transmitter);
            }
        }
    }

    private void broadcastConnection(
            final ControllerNetworkAdministration administration,
            final String networkId, final String connectionId,
            final Transmitter transmitter) throws VinError {
        logger.debug("Broadcasting connection info: networkId=" + networkId
                + " connectionId=" + connectionId);
        for (final AgentInfo agent : agents) {
            if (agent.isActive()) {
                agent.sendPrivateNetworkMembershipMessage(administration,
                        networkId, connectionId, transmitter);
            }
        }
    }

    String registerMachineConnection(
            final ControllerNetworkAdministration privateNetworkAdministration,
            final String virtualMachineId, final String networkId,
            final InetAddress addr, final Properties properties,
            final Transmitter transmitter) throws VinError, ConfigurationError {
        assert virtualMachineId != null;
        final int ix = searchAgentsForIdentifier(agents, virtualMachineId);
        if (ix < 0) {
            throw new ConfigurationError("Unknown virtual machine identifier '"
                    + virtualMachineId + '\'');
        }
        final AgentInfo connectionAgent = agents.get(ix);
        final String connectionId = privateNetworkAdministration.addConnection(
                connectionAgent, networkId, addr, properties);
        if (connectionAgent.address == null) {
            final PendingConnection pc = new PendingConnection(
                    virtualMachineId, networkId, connectionId);
            pendingConnections.add(pc);
        } else {
            broadcastConnection(privateNetworkAdministration, networkId,
                    connectionId, transmitter);
        }
        return connectionId;
    }

    void registerMachineConnectionReady(
            final ControllerNetworkAdministration privateNetworkAdministration,
            final String virtualMachineId, final String networkId,
            final String connectionId, final boolean changed,
            final Transmitter transmitter) throws VinError, ConfigurationError {
        final int ix = searchAgentsForIdentifier(agents, virtualMachineId);
        if (ix < 0) {
            throw new ConfigurationError("Unknown virtual machine identifier '"
                    + virtualMachineId + '\'');
        }
        final AgentInfo connectionAgent = agents.get(ix);
        logger.debug("Connection is ready: vmId=" + virtualMachineId + " nwId="
                + networkId + " conId=" + connectionId + " changed=" + changed);
        if (connectionAgent.address == null) {
            final PendingConnection pc = new PendingConnection(
                    virtualMachineId, networkId, connectionId);
            pendingConnections.add(pc);
        } else if (changed) {
            broadcastConnection(privateNetworkAdministration, networkId,
                    connectionId, transmitter);
        }
    }

    void sendConnectionRemoval(final String networkId,
            final String connectionId, final Transmitter transmitter) {
        for (final AgentInfo agent : agents) {
            if (agent.isActive()) {
                agent.sendNetworkConnectionRemovalMessage(networkId,
                        connectionId, transmitter);
            }
        }
    }

    void sendNetworkRemoval(final String networkId,
            final Transmitter transmitter) {
        for (final AgentInfo agent : agents) {
            if (agent.isActive()) {
                agent.sendNetworkRemovalMessage(networkId, transmitter);
            }
        }
    }

    void stop(final Transmitter transmitter) {
        for (final AgentInfo agent : agents) {
            if (agent.isActive()) {
                agent.sendStopMessage(transmitter);
            }
        }
    }

}
