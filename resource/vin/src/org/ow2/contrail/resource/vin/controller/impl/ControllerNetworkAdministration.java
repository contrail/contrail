package org.ow2.contrail.resource.vin.controller.impl;

import ibis.ipl.IbisIdentifier;

import java.net.InetAddress;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.EncapsulationType;
import org.ow2.contrail.resource.vin.common.Message;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.Subnet;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.controller.Globals;

class ControllerNetworkAdministration {
    private int networkIdCounter = 0;
    private long connectionIdCounter = 0;
    private final HashMap<String, NetworkDescriptor> networks = new HashMap<String, NetworkDescriptor>();
    private final String sessionId;

    ControllerNetworkAdministration(final int ibisPortNumber) {
        this.sessionId = "s" + Integer.toHexString(ibisPortNumber);
    }

    /**
     * Creates a new private network with the given name and encryption
     * algorithm, and returns an identifier for that network.
     * 
     * @param name
     *            The name of the network to create. Only used for tracing and
     *            debugging.
     * @param encapsulationType
     *            The type of packet encapsulation to use.
     * @param encryption
     *            The encryption algorithm to use. Ignored for network type
     *            Open.
     * @param key
     *            The encryption key to use, or <code>null</code> to use an
     *            internally generated key. Ignored for network types Open and
     *            strongSwan.
     * @return The identifier of the new network.
     * @throws OutOfEntriesError
     * @throws VinError
     */
    String createNetwork(final EncapsulationType encapsulationType,
            final Subnet subnet, final ArrayValueRange macAddresses,
            final CAFactoryTalker caFactoryTalker, final Properties properties)
            throws OutOfEntriesError, VinError {
        final String networkId = sessionId + "-vin" + networkIdCounter++;
        final URI ca;
        if (caFactoryTalker == null) {
            ca = null;
        } else {
            ca = caFactoryTalker.createNetworkCA(networkId);
        }

        final boolean connectionsAreAlwaysReady;
        switch (encapsulationType) {
        case GRE:
            connectionsAreAlwaysReady = true;
            break;
        case OpenVPN:
        case StrongSwan:
        default:
            connectionsAreAlwaysReady = false;
        }

        final NetworkDescriptor pn = new NetworkDescriptor(encapsulationType,
                networkId, subnet, macAddresses, ca, connectionsAreAlwaysReady,
                properties);
        networks.put(networkId, pn);
        Globals.administrationLogger
                .debug("Created new private network: networkType="
                        + encapsulationType + " properties=" + properties);
        return networkId;
    }

    String addConnection(final AgentInfo agent, final String networkId,
            final InetAddress addr, final Properties properties)
            throws ConfigurationError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new ConfigurationError("Unknown network: id='" + networkId
                    + '\'');
        }
        final String connectionId = "cn" + connectionIdCounter++;
        nw.addConnection(agent, connectionId, addr, properties);
        Globals.administrationLogger.debug("Added connection: networkID="
                + networkId + " virtualMachine=" + agent.agentId
                + " connectionId=" + connectionId);
        return connectionId;
    }

    boolean registerNetworkMemberReady(final String networkId,
            final String connectionId) throws ConfigurationError, VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new ConfigurationError("Unknown network: id='" + networkId
                    + '\'');
        }
        return nw.registerMemberReady(connectionId);
    }

    String getVmId(final String networkId, final String connectionId)
            throws ConfigurationError, VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new ConfigurationError("Unknown network: id='" + networkId
                    + '\'');
        }
        return nw.getVmId(connectionId);
    }

    void removeConnection(final String networkId, final String connectionId,
            final ControllerEngine personality) throws VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinError("Unknown network: id='" + networkId + '\'');
        }
        nw.ensureConnectionIdExists(connectionId);
        nw.removeConnection(connectionId, personality);
        Globals.administrationLogger.debug("Removed connection: networkID="
                + networkId + " connectionId=" + connectionId);
    }

    void removeNetwork(final String networkId,
            final ControllerServiceAPI vinAdministrator) throws VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinError("Unknown network: id='" + networkId + '\'');
        }
        vinAdministrator.returnInetSubnet(nw.subnet);
        vinAdministrator.returnMACAddresses(nw.macAddresses);
        networks.remove(networkId);
    }

    Message builPrivateNetworkDescriptionMessage(final String networkId)
            throws VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinError("Unknown network: id='" + networkId + '\'');
        }
        final Message msg = nw.buildPrivateNetworkDescriptionMessage();
        Globals.administrationLogger.trace("Built message: " + msg);
        return msg;
    }

    Message builPrivateNetworkMembershipMessage(final String networkId,
            final String connectionId) throws VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinError("Unknown network: id='" + networkId + '\'');
        }
        final Message msg = nw
                .buildPrivateNetworkMembershipMessage(connectionId);
        Globals.administrationLogger.trace("Built message: " + msg);
        return msg;
    }

    String getPrivateIPAddressString(final String networkId,
            final String connectionId) throws VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinError("Unknown network: id='" + networkId + '\'');
        }
        final String addr = nw.getIPAddressString(connectionId);
        Globals.administrationLogger
                .debug("The IP address of connection: networkID=" + networkId
                        + " connectionId=" + connectionId + " is: " + addr);
        return addr;
    }

    void registerNodeLeft(final IbisIdentifier node,
            final ControllerEngine personality) throws VinError {
        final Collection<NetworkDescriptor> c = networks.values();
        for (final NetworkDescriptor nw : c) {
            nw.registerNodeLeft(node, personality);
        }
    }

    public InetAddress getNextAddress(final String networkId)
            throws OutOfEntriesError, VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinError("Unknown network: id='" + networkId + '\'');
        }
        return nw.getNextAddress();
    }

    public void recycleAddress(final String networkId, final InetAddress addr)
            throws VinError {
        final NetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinError("Unknown network: id='" + networkId + '\'');
        }
        nw.recycleAddress(addr);
    }

}
