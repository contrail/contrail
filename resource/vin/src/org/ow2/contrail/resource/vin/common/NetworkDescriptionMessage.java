package org.ow2.contrail.resource.vin.common;

import java.net.URI;
import java.util.Properties;

/**
 * A message from a coordinator to an agent, telling it about a particular
 * network.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class NetworkDescriptionMessage extends Message {
    private static final long serialVersionUID = 1L;
    public final EncapsulationType networkType;
    /** The properties of this network. */
    public final Properties properties;
    /** The identifier of this network. */
    public final String networkId;

    /**
     * The subnet to use for this network.
     */
    public final Subnet subnet;

    /**
     * The MAC addresses to use for this network.
     */
    public final ArrayValueRange macAddresses;

    /**
     * The uri of the CA of this network.
     */
    public URI caUri;

    public NetworkDescriptionMessage(final EncapsulationType mode,
            final String networkId, final Subnet subnetPool,
            final ArrayValueRange macAddresses, final URI caUri,
            final Properties properties) {
        this.networkType = mode;
        this.networkId = networkId;
        this.properties = properties;
        this.subnet = subnetPool;
        this.macAddresses = macAddresses;
        this.caUri = caUri;
    }

    @Override
    public String toString() {
        return NetworkDescriptionMessage.class.getName() + "[mode="
                + networkType + " networkId=" + networkId + " properties="
                + properties + " subnet=" + subnet + ']';
    }

}
