package org.ow2.contrail.resource.vin.common;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Global constants that should be visible to all packages.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class GlobalGlobals {
    /**
     * The name of the single receive port of both the controller and the
     * agents.
     */
    public static final String receivePortName = "receivePort";

    /**
     * The name of the election for the controller of the VIN. Note that this is
     * a very boring election, since there is only one controller, and only that
     * controller is a candidate in the election.
     */
    public static final String CONTROLLER_ELECTION_NAME = "controller-election";

    /** A logger that logs demo tracing text. */
    public final static Logger demoLogger = LoggerFactory
            .getLogger("org.ow2.contrail.resource.vin.common.demo");

    public static final String LIBVIRT_URI_ENV_NAME = "VIRSH_DEFAULT_CONNECT_URI";
}
