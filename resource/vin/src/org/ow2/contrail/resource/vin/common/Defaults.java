package org.ow2.contrail.resource.vin.common;

import java.io.File;

public class Defaults {

    public static final File CONTRAIL_DIR = new File(
            "/usr/share/contrail/support/vin");
    public static final File SAFECONFIG_PATH = new File("/usr/bin/safeconfig");
    public static final File VIN_CONFIG_DIR = new File("/etc/contrail/vin");
    public static final File LAUNCH_AGENT_COMMAND_PATH = new File(CONTRAIL_DIR,
            "scripts/launch-agent");

}
