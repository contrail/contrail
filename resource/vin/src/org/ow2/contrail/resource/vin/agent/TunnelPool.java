package org.ow2.contrail.resource.vin.agent;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

import org.ow2.contrail.resource.vin.common.VinInternalError;

class TunnelPool {
    private int ctr = 0;
    private final String prefix;

    TunnelPool(final String prefix) {
        super();
        this.prefix = prefix;
    }

    static final class TunnelPoolEntry {
        private final ArrayList<AgentConnectionDescriptor> members = new ArrayList<AgentConnectionDescriptor>();
        private final String name;

        private TunnelPoolEntry(final String name) {
            this.name = name;
        }

        private void add(final AgentConnectionDescriptor c) {
            members.add(c);
        }

        private boolean remove(final AgentConnectionDescriptor c) {
            members.remove(c);
            return members.isEmpty();
        }

        String getTunnelName() {
            return name;
        }

        boolean hasOneEntry() {
            return members.size() == 1;
        }

        boolean isEmpty() {
            return members.isEmpty();
        }
    }

    private final HashMap<InetAddress, TunnelPoolEntry> pool = new HashMap<InetAddress, TunnelPool.TunnelPoolEntry>();

    TunnelPoolEntry addConnection(final AgentConnectionDescriptor c) {
        final InetAddress address = c.hostAddress;
        TunnelPoolEntry e = pool.get(address);
        if (e == null) {
            final String name = prefix + ctr;
            ctr++;
            e = new TunnelPoolEntry(name);
            pool.put(address, e);
        }
        e.add(c);
        return e;
    }

    TunnelPoolEntry removeConnection(final AgentConnectionDescriptor c) {
        final TunnelPoolEntry e = pool.get(c.hostAddress);
        if (e == null) {
            throw new VinInternalError("Connection is missing from pool: " + c);
        }
        final boolean isEmpty = e.remove(c);
        if (isEmpty) {
            pool.remove(c.hostAddress);
        }
        return e;
    }

    public boolean isEmpty() {
        return pool.isEmpty();
    }
}
