package org.ow2.contrail.federation.hub

import sbt._
import Keys._

object MainBuild extends Build {

  val project = Project(id="monitoring-hub", base=file(".")) settings (
    name := "monitoring-hub",
    version := "1.0.1-SNAPSHOT",
    organization := "org.ow2.contrail.resource",
    scalaVersion := "2.8.2",
    
    resolvers ++= Seq(
        "twitter.com" at "http://maven.twttr.com/",
        "repo.codahale.com" at "http://repo.codahale.com"
    ),
    libraryDependencies ++= Seq(
        // Finagle
        "com.twitter" % "finagle-core" % "2.0.1",
        "com.twitter" % "finagle-http" % "2.0.1",
        "com.twitter" % "finagle-stream" % "2.0.1",
        "com.twitter" % "finagle-redis" % "2.0.1",
        "com.twitter" % "finagle-kestrel" % "2.0.1",
        
        // JSON
        "org.codehaus.jackson" % "jackson-core-asl"  % "1.8.1",
        "org.codehaus.jackson" % "jackson-mapper-asl" % "1.8.1",
        "com.codahale" % "jerkson_2.8.2" % "0.5.0",
        
        // AMPQ
        "com.rabbitmq" % "amqp-client" % "2.7.1",
        
        // Logging
        "org.slf4j" % "slf4j-simple" % "1.6.3",
        "ch.qos.logback" % "logback-core" % "0.9.30"
        
        // Testing
//        "org.scalatest" % "scalatest_2.8.1" % "1.5.1" % "test"
    ),
    
    // don't append scala version number to artifactId
    crossPaths := false,
    
    pomExtra :=
        <parent>
          <groupId>org.ow2.contrail</groupId>
          <artifactId>contrail</artifactId>
          <version>0.3-SNAPSHOT</version>
        </parent>
        <build>
          <plugins>
            <plugin>
              <groupId>org.scala-tools</groupId>
              <artifactId>maven-scala-plugin</artifactId>
              <executions>
                <execution>
                  <goals>
                    <goal>compile</goal>
                    <goal>testCompile</goal>
                  </goals>
                </execution>
              </executions>
              <version>2.9.1</version>
            </plugin>
            <plugin>
              <artifactId>maven-assembly-plugin</artifactId>
              <executions>
                <execution>
                  <phase>package</phase>
                  <goals>
                    <goal>single</goal>
                  </goals>
                  <configuration>
                    <descriptorRefs>
                      <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                    <archive>
                      <manifest>
                        <mainClass>org.ow2.contrail.monitoring.hub.HubServer</mainClass>
                      </manifest>
                    </archive>
                  </configuration>
                </execution>
                <execution>
                  <id>make-assembly</id>
                  <phase>package</phase>
                  <configuration>
                    <appendAssemblyId>false</appendAssemblyId>
                    <finalName>contrail-monitoring-hub</finalName>
                    <descriptors>
                      <descriptor>src/main/assembly/package.xml</descriptor>
                    </descriptors>
                  </configuration>
                  <goals>
                    <goal>single</goal>
                  </goals>
                </execution>
              </executions>
            </plugin>
          </plugins>
        </build>
  )
}

