package org.ow2.contrail.monitoring.hub

import java.util.concurrent.Executors
import System.getProperty
import scala.util.Random
import org.slf4j.LoggerFactory
import com.twitter.util.FuturePool
import com.twitter.finagle.builder.ClientBuilder
import com.twitter.finagle.redis.Redis
import com.twitter.finagle.kestrel.protocol.Kestrel
import com.twitter.finagle.kestrel.{ReadHandle, Client => KestrelClient}
import org.ow2.contrail.monitoring.hub.util.RedisClient
import com.rabbitmq.client.ConnectionFactory
import com.codahale.jerkson.Json._

object Hub {
  private val log = LoggerFactory.getLogger(getClass)
  
  val config = {
    val configFile = getProperty("hub.config")
    
    try {
      val content = scala.io.Source.fromFile(configFile).mkString
      val cfg = parse[HubConfig](content)
      log.info("Loaded config file {}", configFile)
      cfg
    } catch {
      case err => {
        log.info("Could not read config file {} ({})", configFile, err.toString)
        HubConfig.default
      }
    }
  }
  
  val redisClient = {
    val host = "%s:%d".format(config.redis.host, config.redis.port)
    
    new RedisClient(
      ClientBuilder()
      .hosts(host)
      .hostConnectionLimit(10)
      .codec(new Redis())
      .build()
    )
  }
	
	object kestrel {
	  val host = "%s:%d".format(config.kestrel.host, config.kestrel.port)
	  
	  def clientForReading = {
	    KestrelClient(ClientBuilder()
        .codec(Kestrel())
        .hosts(host)
        .hostConnectionLimit(1)
        .buildFactory())
	  }
	  
	  val client = {
      KestrelClient(ClientBuilder()
        .codec(Kestrel())
        .hosts(host)
        .hostConnectionLimit(10)
        .buildFactory())
	  }
	  
	  def clientForWriting = client
	}
	
	object rabbit {
	  val r = config.rabbit;
	  
	  val factory = if (r.enabled) {
      val fac = new ConnectionFactory
		  
      if (r.username != "") {
		    fac.setUsername(r.username)
		  }
      
		  if (r.password != "") {
		    fac.setPassword(r.password)
		  }
			
		  fac.setVirtualHost(r.virtualHost)
			fac.setRequestedHeartbeat(0)
			fac.setHost(r.host)
			fac.setPort(r.port)
			
			Some(fac)
	  } else {
	    None
	  }
	  
	  val sendPool = FuturePool(Executors.newFixedThreadPool(50))
	  
	  def !(msg: Any) {
	    factory map { f =>
		    sendPool {
		      val sender = new RabbitSender(f, r.exchange)
		      sender ! msg
		      sender.close
		    } onFailure { err =>
		      log.error("Could not send message to RabbitMQ", err)
		    }
	    }
	  }
	}
}