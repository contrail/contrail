package org.ow2.contrail.monitoring.hub.util

import com.twitter.util.Future
import com.twitter.finagle.Service
import com.twitter.finagle.builder.ClientBuilder
import com.twitter.finagle.redis.Client
import com.twitter.finagle.redis.protocol._

class RedisClient(service: Service[Command, Reply]) extends Client(service) {
	def zRem(key: Array[Byte], member: Array[Byte]): Future[Int] =
	  doRequest(ZRem(List(key, member))) {
      case IntegerReply(n) => Future.value(n)
    }
	
	def zRange(key: String, start: Int, stop: Int): Future[Seq[Array[Byte]]] =
	  doRequest(ZRange(key, start, stop)) {
      case MBulkReply(messages) => Future.value(messages)
      case EmptyMBulkReply()    => Future.value(Seq())
    }
	
	private def doRequest[T](cmd: Command)(handler: PartialFunction[Reply, Future[T]]) =
    service(cmd) flatMap (handler orElse {
      case ErrorReply(message)  => Future.exception(new ServerError(message))
      case _                    => Future.exception(new IllegalStateException)
    })
}