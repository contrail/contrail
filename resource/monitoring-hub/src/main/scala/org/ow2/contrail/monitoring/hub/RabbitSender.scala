package org.ow2.contrail.monitoring.hub

import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
import com.rabbitmq.client.ConnectionFactory
import java.nio.charset.Charset

class RabbitSender(cf: ConnectionFactory, exchange: String) {
  val conn = cf.newConnection()
  val channel = conn.createChannel()
  
  channel.exchangeDeclare("hub", "fanout", true)
  
  def send(routingKey: String, msg: String) = {
    channel.basicPublish(exchange, routingKey, null, msg.getBytes("UTF8"))
  }
  
  def !(msg: Any) {
    msg match {
      case Message(routingKey, msg) => send(routingKey, msg)
    }
  }
  
  def close() {
    channel.abort
    conn.abort
  }
}

case class Message(routingKey: String, msg: String)
