package org.ow2.contrail.monitoring.hub

case class ServerConfig(restPort: Int, streamerPort: Int)
case class RedisConfig(host: String, port: Int)
case class KestrelConfig(host: String, port: Int)
case class RabbitConfig(enabled: Boolean, host: String, port: Int,
    										exchange: String, username: String, password: String,
    										virtualHost: String)
case class HubConfig(server:ServerConfig,
    									redis: RedisConfig,
    									kestrel: KestrelConfig,
    									rabbit: RabbitConfig)

object HubConfig {
  val default = HubConfig(
    server = ServerConfig(
      restPort = 8342,
      streamerPort = 8343
    ),
		redis = RedisConfig(
			host = "localhost",
			port = 6379
		),
		kestrel = KestrelConfig(
		  host = "localhost",
		  port = 22133
		),
		rabbit = RabbitConfig(
		  enabled = true,
		  host = "localhost",
      port = 5672,
      exchange = "hub",
      username = "",
      password = "",
      virtualHost = "/"
		)
	)
}
