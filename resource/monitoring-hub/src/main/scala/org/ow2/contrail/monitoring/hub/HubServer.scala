package org.ow2.contrail.monitoring.hub

import java.net.InetSocketAddress
import org.slf4j.LoggerFactory
import org.jboss.netty.handler.codec.http._
import org.jboss.netty.handler.codec.http.HttpResponseStatus._
import org.jboss.netty.handler.codec.http.HttpMethod._
import org.jboss.netty.buffer.ChannelBuffer
import org.jboss.netty.buffer.ChannelBuffers.copiedBuffer
import org.jboss.netty.util.CharsetUtil.UTF_8
import com.twitter.util.Future
import com.twitter.concurrent.{ Channel, ChannelSource, Broker }
import com.twitter.finagle.{ Service, SimpleFilter }
import com.twitter.finagle.builder.{ Server, ServerBuilder }
import com.twitter.finagle.http.{ Http, RichHttp, Request, Response }
import com.twitter.finagle.http.path._
import com.twitter.finagle.stream.{Stream, StreamResponse}
import com.twitter.finagle.kestrel.protocol.Kestrel
import com.twitter.finagle.kestrel.ReadHandle
import com.codahale.jerkson.Json._

object HubServer {
  private val log = LoggerFactory.getLogger(getClass)
  
  class HandleExceptions extends SimpleFilter[Request, Response] {
    def apply(request: Request, service: Service[Request, Response]) = {

      // `handle` asynchronously handles exceptions.
      service(request) handle { case error =>
        val statusCode = error match {
          case _: IllegalArgumentException =>
            FORBIDDEN
          case _ =>
            INTERNAL_SERVER_ERROR
        }
        
        val errorResponse = request.response
        errorResponse.setStatus(statusCode)
        errorResponse.setContent(copiedBuffer("%s: %s".format(error, error.getStackTraceString), UTF_8))

        errorResponse
      }
    }
  }
  
  class Respond extends Service[Request, Response] {
    def apply(request: Request) = {
      log.info("{} {}", request.method, request.path)
      
      val res = (request.method, Path(request.path)) match {
        case PUT -> Root / "offers" / "route" / route =>
          val subscribed = false
          val providerId = request.getHeader("X-Provider-ID") toString
          
          val federRoute = "providerid.%s.%s".format(providerId, route)

          val offer = Offer(providerId, route, federRoute, subscribed)
          
          log.info("Adding {}", offer.id)
          
          Offer.get(offer.id) flatMap { existingOffer =>
            if (existingOffer == null) {
              log.info("Offer does not exist yet. Listening...")
              
              offer.save
            offer.listen flatMap { subscribed =>
              log.info("Subscribed...")
              Future.value(JsonResponse(offer.toJson))
            }
            } else {
              if (existingOffer.sub) {
                log.info("Offer already exist and subscribed")
              Future.value(JsonResponse(offer.toJson))
              } else {
                log.info("Offer already exist. Listening...")
                
                offer.listen flatMap { subscribed =>
                  log.info("Subscribed...")
                  Future.value(JsonResponse(offer.toJson))
                }
              }
            }
          }
        
        case GET -> Root / "offers" / "unsubscribed" =>
          log.info("Getting list of unsubscribed offers")
          
          Offer.unsubscribed map { offers =>
            JsonResponse(generate(offers))
          }

        case GET -> Root / "offers" / "get" / id =>
          log.info("Getting offer {}", id)
          
          Offer.get(id) map { offer =>
            JsonResponse(offer.toJson)
          }

        case PUT -> Root / "offers" / "subscribe" / id =>
          log.info("Subscribing to {}", id)
          
          Offer.get(id) map { offer =>
            offer.subscribe
            val newOffer = Offer(offer.provId, offer.provRoute, offer.federRoute, true, offer.dt)
            newOffer.save
            
            StatusResponse(CREATED)
          }

        case PUT -> Root / "metrics" / "route" / route =>
          val provider = request.getHeader("X-Provider-ID")
          
          log.info("New metric for route {} for provider {}", route, provider)
          
          val providerId = provider toString
          
          Offer.get(providerId, route) flatMap { offer =>
            if (offer == null) {
              Future.value(StatusResponse(BAD_REQUEST))
            } else {
              val metric = Metric(offer.federRoute)
              metric.add(request.content)
              Future.value(StatusResponse(CREATED))
            }
          }
        
        case GET -> Root / "metrics" / route =>
          log.info("Waiting for metrics for route {}", route)
          
          val metric = Metric(route)
          
          metric.getSingle flatMap { content =>
            Future.value(StringResponse(content))
          }
        
        case _ =>
          null
      }
      
      res match {
        case future: Future[_] =>
          future flatMap { futureResponse =>
            futureResponse match {
              case JsonResponse(content) =>
                val response = request.response
                response.setContentTypeJson()
                response.setContent(copiedBuffer(content, UTF_8))
                Future.value(response)
              
              case StatusResponse(status) =>
                val response = request.response
                response.setStatus(status)
                Future.value(response)
              
              case StringResponse(content) =>
                val response = request.response
                response.setContent(copiedBuffer(content, UTF_8))
                Future.value(response)
              
              case _ =>
                log.info("Bad request for future")
                
                val response = request.response
                response.setStatus(BAD_REQUEST)
                Future.value(response)
            }
          }

        case e =>
          log.info("Bad request {}", e)
          
          val response = request.response
          response.setStatus(BAD_REQUEST)
          Future.value(response)
      }
    }
  }
  
  class Streamer extends Service[HttpRequest, StreamResponse] {
    def apply(request: HttpRequest) = {
      val path = request.getUri().split("\\?").head
      
      (request.getMethod(), Path(path)) match {
        case GET -> Root / "metrics" / route / "stream" => {
          log.info("Streaming metrics for route {}", route)
          
          val metric = Metric(route)
          val readHandle = metric.readHandle
          
          val subscriber = new Broker[ChannelBuffer]
	      
	      val response = new StreamResponse {
	        val httpResponse = new DefaultHttpResponse(
	            request.getProtocolVersion, HttpResponseStatus.OK)
	        
	        def messages = subscriber.recv
	        def error = new Broker[Throwable].recv
	        def release() = {
	          readHandle.close()
	          
	          subscriber.recv foreach { _ => () }
	        }
	      }
	      
	      readHandle.messages foreach { msg =>
	        try {
	          subscriber ! msg.bytes
	        } finally {
	          msg.ack()
	        }
	      }
	      
	      Future.value(response)
        }
      }
    }
  }
  
  case class JsonResponse(content: String)
  case class StringResponse(content: String)
  case class StatusResponse(status: HttpResponseStatus)

  def main(args: Array[String]) {
    val handleExceptions = new HandleExceptions
    val respond = new Respond

    val myService: Service[Request, Response] = handleExceptions andThen respond

    val server: Server = ServerBuilder()
      .codec(RichHttp[Request](Http()))
      //.tls("server.crt", "server.key")
      .bindTo(new InetSocketAddress(Hub.config.server.restPort))
      .name("monitoring-hub-rest")
      .build(myService)

    log.info("REST server started on port {}", Hub.config.server.restPort)
    
    val streamer: Service[HttpRequest, StreamResponse] = new Streamer
    
    val streamerServer: Server = ServerBuilder()
      .codec(Stream())
      .bindTo(new InetSocketAddress(Hub.config.server.streamerPort))
      .name("monitoring-hub-streamer")
      .build(streamer)

    log.info("Streamer server started on port {}", Hub.config.server.streamerPort)
  }

}