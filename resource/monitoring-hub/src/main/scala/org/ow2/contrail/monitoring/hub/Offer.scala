package org.ow2.contrail.monitoring.hub

import java.util.Date
import org.slf4j.LoggerFactory
import org.jboss.netty.buffer.ChannelBuffers.copiedBuffer
import org.jboss.netty.util.CharsetUtil.UTF_8
import com.twitter.util.{Future, JavaTimer}
import com.twitter.conversions.time._
import com.twitter.finagle.service.Backoff
import com.codahale.jerkson.Json._

object Offer {
  private val log = LoggerFactory.getLogger(getClass)
  
  val unsubscribedDbKey = "hub:offer:unsubscribed"
    
  def kestrelEscapeKey(key: String) = key.replaceAll("\\.", "\\#")
	
  def genId(provId: String, provRoute: String) = {
    "hub:offer:%s:%s".format(provId, provRoute)
  }
  
  def apply(provId: String, provRoute: String, federRoute: String, sub: Boolean, dt: Date = new Date) = {
    val id = genId(provId, provRoute)
    new Offer(id, provId, provRoute, federRoute, sub, dt)
  }
  
  def get(provId: String, provRoute: String): Future[Offer] = {
    get(genId(provId, provRoute))
  }
  
  def get(id: String): Future[Offer] = {
    Hub.redisClient.get(id) map { res =>
      res match {
        case Some(bytes) =>
          parse[Offer](new String(bytes))
        case None =>
          null
      }
    }
  }
  
  def unsubscribed: Future[Seq[Offer]] = {
    Hub.redisClient.zRange(unsubscribedDbKey, 0, -1) flatMap { keyResults =>
      val res = keyResults map { keyBytes =>
        Offer.get(new String(keyBytes))
      }
      Future.collect(res)
    } handle { case error =>
        log.error("Error: {}", error)
        null
      }
  }
}

case class Offer(id: String, provId: String, provRoute: String, federRoute: String, sub: Boolean, dt: Date) {
  def save = {
    Hub.redisClient.set(id, toJson.getBytes)
    if (sub) subscribe else unsubscribe
  }
  
  def remove = {
    Hub.redisClient.del(Seq(id))
    unsubscribe
  }
  
  def subscribe = {
    Hub.redisClient zRem(Offer.unsubscribedDbKey.getBytes, id.getBytes)
    Hub.kestrel.clientForWriting set(Offer.kestrelEscapeKey(id), copiedBuffer("1", UTF_8))
  }

  def unsubscribe = {
    Hub.redisClient.zAdd(Offer.unsubscribedDbKey.getBytes, 1.0, id.getBytes)
  }
  
  def listen: Future[Boolean] = {
    val client = Hub.kestrel.clientForReading
    
    val readHandle = {
      val timer = new JavaTimer(isDaemon = true)
      val retryBackoffs = Backoff.const(10.milliseconds)
      client.readReliably(Offer.kestrelEscapeKey(id), timer, retryBackoffs)
    }
    
    readHandle.messages() flatMap { msg =>
      try {
        Future.value(true)
      } finally {
        msg.ack()
        readHandle.close
        client.close
      }
    }
  }

  def toJson = {
    generate(this)
  }
}
