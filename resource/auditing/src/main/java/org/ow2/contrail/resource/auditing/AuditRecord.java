package org.ow2.contrail.resource.auditing;

public interface AuditRecord {

    public String getId();
}
