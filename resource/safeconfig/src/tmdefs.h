/* File: tmdefs.h
 *
 * Datastructures and other definitions of Tm: datastructure
 * and definition and manipulation routine generator.
 */

#if defined( GEMDOS ) || defined( __TOS__ )
#define PATHSEPSTR "\\"
#endif
#ifdef MSDOS
#define PATHSEPSTR "\\"
#endif

#ifndef PATHSEPSTR
#define PATHSEPSTR "/"
#endif

#ifndef __cplusplus
typedef short bool;
#endif

/* increment in dynamically growing string buffer size */
#define STRSTEP 20

/* Some character names */
#define OCBRAC '{'
#define CCBRAC '}'
#define ORBRAC '('
#define CRBRAC ')'
#define OSBRAC '['
#define CSBRAC ']'
#define VARCHAR '$'
#define LCOMCHAR '.'
#define DQUOTE '"'

#if __GNUC__
#define ATTRIBUTE_PRINTF(fmt,args) __attribute__((format(printf,fmt,args)))
#else
#define ATTRIBUTE_PRINTF(fmt,args)
#endif
