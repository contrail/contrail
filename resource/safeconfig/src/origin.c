// File: origin.c

#include <stdlib.h>

#include "origin.h"
#include "sgstring.h"
#include "error.h"

origin *new_origin(const char *file, int line)
{
    origin *res = (origin *) malloc(sizeof(struct str_origin));
    if(res == 0){
        fatal_error("Out of memory");
    }
    res->file = new_string(file);
    res->line = line;
    return res;
}

origin *rdup_origin(const origin *org)
{
    return new_origin(org->file, org->line);
}

void rfre_origin(origin *org)
{
    fre_string(org->file);
    free(org);
}
