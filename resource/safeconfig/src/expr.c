/* File: expr.c
 *
 * Evaluate template expressions.
 */

/* Standard UNIX libraries and expressions */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>


/* local definitions */
#include "tmdefs.h"
#include "origin.h"
#include "error.h"
#include "expr.h"
#include "global.h"
#include "sgstring.h"

static const char *evalbool( const origin *org, const char *x, long *vp );

/* integer expression evaluation.
 * All these functions evaluate a set of operators at one
 * priority level, set a value pointed to by a passed pointer to
 * the evaluated value, and return a pointer to the remaining string.
 *
 * Priorities (in order of increasing binding power:
 *
 * & |
 * < <= > >= == !=
 * + -
 * * / %
 * + - ~ ! (unary)
 */

/* Evaluate integer constants and '()'. */
static const char *evalconst( const origin *org, const char *x, long *vp )
{
    char *buf;
    char *bufp;
    const char *s;

    while( isspace( *x ) ){
	x++;
    }
    if( *x == ORBRAC ){
	s = evalbool( org, x+1, vp );
	while( isspace( *s ) ) s++;
	if( *s != CRBRAC ){
	    return x;
	}
	else{
	    return s+1;
	}
    }
    buf = new_string( x );
    bufp = buf;
    while( isdigit( *x ) ){
	*bufp++ = *x++;
    }
    *bufp = '\0';
    if( buf[0] == '\0' ){
	origin_error( org, "bad expression" );
	*vp = 0;
    }
    else{
	*vp = atoi( buf );
    }
    fre_string( buf );
    return x;
}

/* Evaluate unary operators. */
static const char *evalun( const origin *org, const char *x, long *vp )
{
    const char *s;
    long v;

    while( isspace( *x ) ) x++;
    if( *x == '-' ){
	s = evalun( org, x+1, &v );
	*vp = -v;
	return s;
    }
    if( *x == '!' ){
	s = evalun( org, x+1, &v );
	*vp = !v;
	return s;
    }
    if( *x == '+' ){
	return evalun( org, x+1, vp );
    }
    return evalconst( org, x, vp);
}

/* Evaluate product operators. */
static const char *evalprod( const origin *org, const char *x, long *vp )
{
    const char *s;
    long v1;
    long v2;

    while( isspace( *x ) ) x++;
    s = evalun( org, x, &v1 );
    if( s == x ){
	*vp = 0;
	return x;
    }
again:
    while( isspace( *s ) ) s++;
    if( *s == '*' ){
	s = evalun( org, s+1, &v2 );
	v1 = v1*v2;
	goto again;
    }
    if( *s == '/' ){
	s = evalun( org, s+1, &v2 );
	if( v2 == 0 ){
	    origin_error( org, "division by zero" );
	    v1 = 1;
	}
	else {
	    v1 = v1/v2;
	}
	goto again;
    }
    if( *s == '%' ){
	s = evalun( org, s+1, &v2 );
	if( v2 == 0 ){
	    origin_error( org, "modulus by zero" );
	    v1 = 1;
	}
	else {
	    v1 = v1%v2;
	}
	goto again;
    }
    *vp = v1;
    return s;
}

/* Evaluate sum operators. */
static const char *evalsum( const origin *org, const char *x, long *vp )
{
    const char *s;
    long v1;
    long v2;

    while( isspace( *x ) ) x++;
    s = evalprod( org, x, &v1 );
    if( s == x ){
	*vp = 0;
	return x;
    }
again:
    while( isspace( *s ) ) s++;
    if( *s == '+' ){
	s = evalprod( org, s+1, &v2 );
	v1 = v1 + v2;
	goto again;
    }
    if( *s == '-' ){
	s = evalprod( org, s+1, &v2 );
	v1 = v1 - v2;
	goto again;
    }
    *vp = v1;
    return s;
}

/* Evaluate compare operators. */
static const char *evalcmp( const origin *org, const char *x, long *vp )
{
    const char *s;
    long v1;
    long v2;

    while( isspace( *x ) ) x++;
    s = evalsum( org, x, &v1 );
    if( s == x ){
	*vp = 0;
	return x;
    }
    while( isspace( *s ) ) s++;
    if( s[0] == '!' && s[1] == '=' ){
	s = evalsum( org, s+2, &v2 );
	*vp = (long) (v1!=v2);
	return s;
    }
    if( s[0] == '=' && s[1] == '=' ){
	s = evalsum( org, s+2, &v2 );
	*vp = (long) (v1==v2);
	return s;
    }
    if( s[0] == '<' && s[1] == '=' ){
	s = evalsum( org, s+2, &v2 );
	*vp = (long) (v1<=v2);
	return s;
    }
    if( s[0] == '<' ){
	s = evalsum( org, s+1, &v2 );
	*vp = (long) (v1<v2);
	return s;
    }
    if( s[0] == '>' && s[1] == '=' ){
	s = evalsum( org, s+2, &v2 );
	*vp = (long) (v1>=v2);
	return s;
    }
    if( s[0] == '>' ){
	s = evalsum( org, s+1, &v2 );
	*vp = (long) (v1>v2);
	return s;
    }
    *vp = v1;
    return s;
}

/* Evaluate boolean binary operators. */
static const char *evalbool( const origin *org, const char *x, long *vp )
{
    const char *s;
    long v1;
    long v2;

    s = evalcmp( org, x, &v1 );
    while( isspace( *s ) ) s++;
    if( s[0] == '&' ){
	s = evalbool( org, s+1, &v2 );
	*vp = (long) ((v1 != 0) && (v2 != 0));
	return s;
    }
    if( s[0] == '|' ){
	s = evalbool( org, s+1, &v2 );
	*vp = (long) ((v1 != 0) || (v2 != 0));
	return s;
    }
    *vp = v1;
    return s;
}

/* Given a string 'x' containing a numerical expression,
 * evaluate it, and construct a string from the resulting integer.
 */
char *evalexpr( const origin *org, const char *x )
{
    char *buf;
    const char *s;
    long v;

    if( fntracing ){
	fprintf( tracestream, "evaluating expression $[%s]\n", x );
    }
    s =  evalbool( org, x, &v );
    while( isspace( *s ) ) s++;
    if( *s != '\0' ){
	origin_error( org, "bad expression '%s'", s );
    }
    (void) asprintf( &buf, "%ld", v );
    if( fntracing ){
	fprintf( tracestream, "expression value: '%s'\n", buf );
    }
    return buf;
}
