/* File: util.c
 *
 * Various low-level routines.
 */

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>

#include "tmdefs.h"

#include "util.h"
#include "error.h"
#include "sgstring.h"

/* Same as fopen, but give error message if file can't be opened */
FILE *ckfopen( const char *nm, const char *acc )
{
    FILE *hnd = fopen( nm, acc );

    if( NULL == hnd ){
	sys_error( errno, "'%s'", nm );
	exit( EXIT_FAILURE );
    }
    return hnd;
}

/* Similar to freopen, but give error message if file can't be opened.
 * Therefore, file handle need not be returned.
 */
void ckfreopen( const char *nm, const char *acc, FILE *f )
{
    if( freopen( nm, acc, f ) == NULL ){
	sys_error( errno, "'%s'", nm );
	exit( EXIT_FAILURE );
    }
}

struct str_propertyname {
    const char *name;
    enum FileProperty prop;
};

struct str_propertyname propertynames[] = {
    { "exists", FPExists },
    { "readable", FPReadable },
    { "writable", FPWritable },
    { "executable", FPExecutable },
    { "plain", FPPlain },
    { "directory", FPDirectory },
    { "not-exists", FPNotExists },
    { "not-readable", FPNotReadable },
    { "not-writable", FPNotWritable },
    { "not-executable", FPNotExecutable },
    { "not-plain", FPNotPlain },
    { "not-directory", FPNotDirectory },
    { NULL, FPExists }
};


static const char *get_file_property_name( enum FileProperty prop )
{
    struct str_propertyname *p = propertynames;
    while(p->name != NULL){
        if(p->prop == prop){
            return p->name;
        }
        p++;
    }
    return stringNIL;
}

bool get_file_property(enum FileProperty *res, char *s )
{
    struct str_propertyname *p = propertynames;
    while(p->name != NULL){
        if(strcmp(s,p->name) == 0){
            *res = p->prop;
            return true;
        }
        p++;
    }
    return false;
}

void assert_file_property( const origin *org, const char *fnm, enum FileProperty prop )
{
    int flag = 0;
    bool invert = false;
    // For any error message we produce.
    const char *property_name = get_file_property_name(prop);

    switch(prop){
        case FPExists:
            flag = F_OK;
            invert = false;
            break;

        case FPReadable:
            flag = R_OK;
            invert = false;
            break;

        case FPWritable:
            flag = W_OK;
            invert = false;
            break;

        case FPExecutable:
            flag = X_OK;
            invert = false;
            break;

        case FPNotExists:
            flag = F_OK;
            invert = true;
            break;

        case FPNotReadable:
            flag = R_OK;
            invert = true;
            break;

        case FPNotWritable:
            flag = W_OK;
            invert = true;
            break;

        case FPNotExecutable:
            flag = X_OK;
            invert = true;
            break;

        case FPPlain:
        case FPDirectory:
        case FPNotPlain:
        case FPNotDirectory:
        {
            /**
             * We must test these with a separate function.
             */
            struct stat st_buf;

            int status = stat(fnm, &st_buf);
            if (status != 0 && (prop == FPPlain || prop == FPDirectory)) {
                origin_sys_error( org, errno, "Cannot get information about file '%s'", fnm);
                return;
            }

            bool fail = false;
            switch(prop){
                case FPPlain:
                    fail = !S_ISREG (st_buf.st_mode);
                    break;

                case FPDirectory:
                    fail = !S_ISDIR (st_buf.st_mode);
                    break;

                case FPNotPlain:
                    fail = S_ISREG (st_buf.st_mode);
                    break;

                case FPNotDirectory:
                    fail = S_ISDIR (st_buf.st_mode);
                    break;

                default:
                    break;
            }
            if(fail){
                origin_fatal_error( org, "File '%s' fails assertion '%s'", fnm,  property_name);
            }
            return;

        }
    }
    int state = access( fnm, flag );
    if(invert){
        if( state == 0 ){
            origin_fatal_error( org, "File '%s' fails assertion '%s'", fnm,  property_name);
        }
    }
    else {
        if( state != 0 ){
            origin_sys_error( org, errno, "File '%s' fails assertion '%s'", fnm,  property_name);
            exit( EXIT_FAILURE );
        }
    }
}

void assert_file_readable( const origin *org, const char *fnm )
{
    int state = access( fnm, R_OK );
    if( state != 0 ){
	origin_sys_error( org, errno, "%s", fnm );
	exit( EXIT_FAILURE );
    }
}

void assert_file_executable( const origin *org, const char *fnm )
{
    int state = access( fnm, X_OK );
    if( state != 0 ){
	origin_sys_error( org, errno, "file '%s' is not executable", fnm );
	exit( EXIT_FAILURE );
    }
}

/**
 * Given a filename, make sure that the file is not
 * writable by group or other.
 */
void assert_file_not_go_writable( const char *fnm )
{
    struct stat stat_data;
    int res = stat(fnm, &stat_data);
    if(res != 0){
	sys_error( errno, "'%s'", fnm );
	exit( EXIT_FAILURE );
    }
    bool stop = false;
    int mode = stat_data.st_mode;
    if( (mode & S_IWGRP) ){
	error( "configuration file '%s' is writable by group. Please fix!", fnm );
        stop = true;
    }
    if( (mode & S_IWOTH) ){
	error( "configuration file '%s' is writable by others. Please fix!", fnm );
        stop = true;
    }
    if(stop){
        fatal_error("Fix permissions first, goodbye!");
	exit( EXIT_FAILURE );
    }
}

char *read_file(const char *fnm)
{
    long sz;
    char *buffer;

    FILE *fp = ckfopen( fnm, "rb" );

    fseek( fp, 0L , SEEK_END);
    sz = ftell( fp );
    rewind( fp );

    /* allocate memory for entire content */
    buffer = (char *) malloc( sz+1 );
    if( buffer == stringNIL ){
         fclose(fp);
         fatal_error("Cannot allocate memory for %ld bytes", sz+1);
    }


    /* copy the file into the buffer */
    if( 1!=fread( buffer , sz, 1 , fp) ){
        fclose(fp);
        free(buffer);
        fatal_error("Cannot read file '%s'", fnm);
    }
    buffer[sz] = '\0';

    fclose(fp);
    return buffer;
}

void set_caller_privs()
{
    int res = seteuid(getuid());
    if(res != 0){
	sys_error( errno, "cannot set euid to uid");
	exit( EXIT_FAILURE );
    }
    res = setegid(getgid());
    if(res != 0){
	sys_error( errno, "cannot set egid to gid");
	exit( EXIT_FAILURE );
    }
}

extern void set_privs_of_file(const char *fnm)
{
    struct stat stat_data;
    int res = stat(fnm, &stat_data);
    if(res != 0){
	sys_error( errno, "'%s'", fnm );
	exit( EXIT_FAILURE );
    }

    // We may want to change the uid if we originally were root.
    res = seteuid(0);
    if(res != 0){
	// We were not root, we will have to make do with what we have
	return;
    }
    // We're root again. Only use that privilege to
    // honor the ISGID or ISUID bits
    if((stat_data.st_mode & S_ISGID) != 0){
	// Honor the set-group-ID-on-execution permission
	res = setegid(stat_data.st_gid);
	if(res != 0){
	    sys_error( errno, "cannot set egid to %d", stat_data.st_gid);
	    exit( EXIT_FAILURE );
	}
    }
    int uid; 
    if((stat_data.st_mode & S_ISUID) != 0){
	// Honor the set-user-ID-on-execution permission
	uid = stat_data.st_uid;
    }
    else {
	// No special bits, go back to the invoking uid.
	uid = getuid();
    }
    res = seteuid(uid);
    if(res != 0){
	sys_error( errno, "cannot set euid to %d", uid);
	exit( EXIT_FAILURE );
    }
    // If we're still root at this point, it is because
    // set S_ISUID bit was set, and the owner of the file is root,
    // or the program is run as root.
}


char *trim_string(const char *s)
{
    while(isspace(*s)){
        s++;
    }
    char *res = create_string( strlen(s) );
    char *dst = res;
    bool sawSpace = false;
    while(*s != '\0'){
        int c = *s++;
        if(isspace(c)){
            if(!sawSpace){
                *dst++ = ' ';
                sawSpace = true;
            }
        }
        else {
            *dst++ = c;
            sawSpace = false;
        }
        if(*s == '\0'){
            if(sawSpace){
                // Don't let it end with a space.
                dst--;
                *dst = '\0';
            }
        }
    }
    return res;
}
