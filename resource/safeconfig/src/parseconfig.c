/*
 * parseconfig.c
 *
 *  Created on: Mar 13, 2012
 *      Author: Kees van Reeuwijk
 */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "tmdefs.h"
#include "line.h"
#include "word.h"
#include "parseconfig.h"
#include "sgstring.h"
#include "error.h"
#include "origin.h"

static bool matches_command(const char *realWord, const char *refWord)
{
    return strcmp(realWord,refWord) == 0;
}

line *parseline(const char *s, origin *org)
{
    word *l = chopstring(org, s);
    line *res;

    if(l == wordNIL){
        return new_line(Empty, rdup_origin(org), wordNIL);
    }
    enum Command c;
    const char *cmdString = l->string;
    if(matches_command(cmdString,"set")){
        c = Set;
    }
    else if(matches_command(cmdString,"load")){
        c = Load;
    }
    else if(matches_command(cmdString,"print")){
        c = Print;
    }
    else if(matches_command(cmdString,"debugprint")){
        c = DebugPrint;
    }
    else if(matches_command(cmdString,"errorprint")){
        c = DebugPrint;
    }
    else if(matches_command(cmdString,"parameters")){
        c = SetParameters;
    }
    else if(matches_command(cmdString,"run")){
        c = Run;
    }
    else if(matches_command(cmdString,"debugrun")){
        c = DebugRun;
    }
    else if(matches_command(cmdString,"assertmatch")){
        c = AssertMatch;
    }
    else if(matches_command(cmdString,"assertfile")){
        c = AssertFile;
    }
    else {
        origin_error(org, "Unknown command '%s'", cmdString);
        exit(1);
    }
    res = new_line(c, rdup_origin(org), rdup_word_list(l->next));
    rfre_word_list(l);
    return res;
}

bool isEmptyLine(const char *s){
    while(*s != '\0' && *s != '\r' && *s != '\n'){
        if(isspace(*s)){
            s++;
        }
        else if(*s == '#' ){
            return true;
        }
        else {
            break;
        }
    }
    return false;
}

/**
 * Extract a line of text from the string pointed a by 'start'.
 * Return the position after the extracted line, and fill
 * *line with a newly allocated string with the line of text without
 * any terminating character.
 */
static const char *extractLine(const char *start, char **linestr)
{
    const char *s = start;

    while(*s != '\0' && *s != '\r' && *s != '\n'){
        s++;
    }
    *linestr = extract_string(start, s);
    if(*s == '\r'){
        s++;
    }
    if(*s == '\n'){
        s++;
    }
    return s;
}

line *parseConfiguration(const char *config, const char * fnm){
    line *res = lineNIL;
    line **backPtr = &res;
    int lineNumber = 1;
    const char *p = config;

    while(*p != '\0'){
        char *lineStr;
        p = extractLine(p, &lineStr);
        if(!isEmptyLine(lineStr)){
            origin *org = new_origin(fnm, lineNumber);
            line *l = parseline(lineStr, org);
            *backPtr = l;
            backPtr = &l->next;
            rfre_origin(org);
        }
        lineNumber++;
        fre_string(lineStr);
    }
    return res;
}

