// File: run.c

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <regex.h>
#include <sys/wait.h>

#include "run.h"
#include "line.h"
#include "error.h"
#include "word.h"
#include "trans.h"
#include "sgstring.h"
#include "var.h"
#include "util.h"
#include "global.h"
#include "fileproperty.h"

#define ERRBUFSIZE 4000

static char **eval_word_list(const origin *org, const word * const l)
{
    const word *w;
    // First count the number of elements.
    size_t sz = 1; // For NULL ptr
    w = l;
    while(w != wordNIL){
        sz++;
        w = w->next;
    }
    char **res = (char **) calloc(sz+1, sizeof(char *));
    if(res == NULL){
        fatal_error("Out of memory");
        return NULL;
    }
    w = l;
    int ix = 0;
    while(w != wordNIL){
        res[ix++] = evaluate_string(org, w->string);
        w = w->next;
    }
    res[ix] = stringNIL;
    return res;
}

static void rfre_params(char **params)
{
    int ix = 0;
    while(true){
        char *p = params[ix]++;
        if(p == stringNIL){
            break;
        }
        fre_string(p);
        ix++;
    }
    free(params);
}

static void run_print_command(FILE *out, char **params)
{
    int ix = 0;
    while(true){
        const char *txt = params[ix];
        if(txt == stringNIL){
            break;
        }

        if(ix>0){
            putc(' ', out);
        }
        fputs(txt, out);
        ix++;
    }
    putc('\n', out);
    fflush(out);
}

static int child_pid;

static void command_signal_handler(int sig)
{
    if(child_pid>0){
        int res = kill(child_pid, sig);
        if(res != 0){
            sys_error(errno, "Cannot send signal %d to child with pid %d", sig, child_pid);
            exit(EXIT_FAILURE);
        }
    }
    exit(EXIT_SUCCESS);
}

static void run_command(const origin *org, char **params)
{
    if(showrun){
        int ix = 0;
        while(params[ix] != stringNIL){
            if(ix>0){
                putc(' ',stdout);
            }
            fputs(params[ix], stdout);
            ix++;
        }
        putc('\n', stdout);
        fflush(stdout);
    }
    if(!dryrun){
        char *cmd = params[0];
        assert_file_executable(org, cmd);
        struct sigaction new_action;
        struct sigaction old_term_action;
        struct sigaction old_int_action;
        struct sigaction old_quit_action;
        memset(&new_action, 0, sizeof(new_action));
        new_action.sa_handler = command_signal_handler;
        int ok = sigaction(SIGINT, &new_action, &old_int_action);
        if(ok != 0){
            origin_sys_error( org, errno, "Cannot set SIGINT handler");
            exit(EXIT_FAILURE);
            return;
        }
        ok = sigaction(SIGQUIT, &new_action, &old_quit_action);
        if(ok != 0){
            origin_sys_error( org, errno, "Cannot set SIGQUIT handler");
            exit(EXIT_FAILURE);
            return;
        }
        ok = sigaction(SIGTERM, &new_action, &old_term_action);
        if(ok != 0){
            origin_sys_error( org, errno, "Cannot set SIGTERM handler");
            exit(EXIT_FAILURE);
            return;
        }
        int child = vfork();
        if(child<0){
            origin_sys_error( org, errno, "Cannot fork to execute command '%s'", cmd);
            exit(EXIT_FAILURE);
            return;
        }
        if(child == 0){
            int res = execv(cmd, params);
            // If we reach this point, something is wrong.
            origin_sys_error( org, errno, "Cannot execute command '%s', res=%d", cmd, res);
            exit(EXIT_FAILURE);
            return;
        }
        {
            int status;
            child_pid = child;
            wait(&status);
            int ok = sigaction(SIGTERM, &old_term_action, NULL);
            if(ok != 0){
                origin_sys_error( org, errno, "Cannot restore SIGTERM handler");
                exit(EXIT_FAILURE);
                return;
            }
            ok = sigaction(SIGINT, &old_int_action, NULL);
            if(ok != 0){
                origin_sys_error( org, errno, "Cannot restore SIGTERM handler");
                exit(EXIT_FAILURE);
                return;
            }
            ok = sigaction(SIGQUIT, &old_quit_action, NULL);
            if(ok != 0){
                origin_sys_error( org, errno, "Cannot restore SIGQUIT handler");
                exit(EXIT_FAILURE);
                return;
            }
            if(status != 0){
                char *s = flat_string_array(params, 0);
                fprintf(stderr, "Command: [%s]\n", s);
                fre_string(s);
                origin_fatal_error(org, "Execution of command '%s' returned exit code %d", cmd, status);
            }
        }
    }
}

static void run_set(const origin *org, char **params)
{
    if(params[0] == stringNIL){
        origin_fatal_error(org,"No variable to set");
        return;
    }
    char *s = flat_string_array(params, 1);
    set_var_string(params[0], s);
    fre_string(s);
}

static void run_load(const origin *org, char **params)
{
    if(params[0] == stringNIL){
        origin_fatal_error(org,"No variable to load into");
        return;
    }
    if(params[1] == stringNIL){
        origin_fatal_error(org, "No file to load specified");
    }
    if(params[2] != stringNIL){
        origin_fatal_error(org, "Superfluous parameters for load command");
    }
    char *fnm = params[1];
    // FIXME: handle non-existent file better.
    char *s = read_file(fnm);
    char *s1 = trim_string(s);
    fre_string(s);
    set_var_string(params[0], s1);
    fre_string(s1);
}

static void run_assert_match_command(const origin *org, char **params)
{
    const char *rawpat = params[0];
    if(rawpat == stringNIL){
        origin_fatal_error(org, "No pattern specified");
    }
    char *pat;
    // Anchor the pattern.
    asprintf(&pat, "^%s$", rawpat);
    const char *testString = params[1];
    if(testString == stringNIL){
        origin_fatal_error(org, "No test string specified");
    }
    regex_t re;
    int res = regcomp(&re, pat, REG_EXTENDED|REG_NOSUB);
    fre_string(pat);
    if(res != 0){
        if(hide_regex_error){
            origin_fatal_error(org, "Malformed regular expression '%s'", rawpat);
        }
        else {
            char errbuf[ERRBUFSIZE+2];
            regerror(res, &re, errbuf, ERRBUFSIZE);
            origin_fatal_error(org, "Malformed regular expression '%s': %s", rawpat, errbuf);
        }
    }
    res = regexec(&re, testString, 0, NULL, 0);
    regfree(&re);
    if(res != 0){
        // Something is wrong.
        if(res == REG_NOMATCH){
            origin_fatal_error(org, "String '%s' does not match pattern '%s'", testString, rawpat);
        }
        else {
            if(hide_regex_error){
                origin_fatal_error(org, "Could not match string '%s' against pattern '%s'", testString, rawpat);
            }
            else {
                char errbuf[ERRBUFSIZE+2];
                regerror(res, &re, errbuf, ERRBUFSIZE);
                origin_fatal_error(org, "Could not match string '%s' against pattern '%s': %s", testString, rawpat, errbuf);
            }
        }
    }
}

static void run_assert_file_command(const origin *org, char **params)
{
    if(params[0] == stringNIL){
        origin_fatal_error(org,"No file to test");
        return;
    }
    const char *fnm = params[0];
    char **p = params+1;
    while(*p != stringNIL){
        char *propString = *p;
        enum FileProperty fp;
        if(!get_file_property(&fp, propString )){
            origin_fatal_error(org,"Unknown property '%s'", propString);
        }
        assert_file_property(org, fnm, fp);
        p++;
    }
}

static int count_argv(const word * const argv)
{
    int n = 0;
    const word *arg = argv;
    while(arg != wordNIL){
        n++;
        arg = arg->next;
    }
    return n;
}

static int count_variables(char **vl){
    int i = 0;
    while(vl[i] != stringNIL){
        i++;
    }
    return i;
}

static const char *verb_be(int n)
{
    return n==1?"is":"are";
}

static const char *plural_s(int n)
{
    return n==1?"":"s";
}

static void run_set_parameters_command(const origin *org, char **params, const word * const argv)
{
    const word *arg = argv;
    int ix = 0;

    while(arg != wordNIL){
        const char *param = params[ix++];

        if(param == stringNIL){
            int argc = count_argv(argv);
            int n_param = count_variables(params);
            origin_fatal_error(org, "Parameter mismatch: there %s %d parameter%s, but only %d variable%s",
                    verb_be(argc), argc, plural_s(argc),
                    n_param, plural_s(n_param));
        }
        set_var_string(param, arg->string);
        arg = arg->next;
    }
    if(params[ix] != stringNIL){
        int argc = count_argv(argv);
        int n_param = count_variables(params);
        origin_fatal_error(org, "Parameter mismatch: there %s only %d parameter%s, but %d variable%s",
             verb_be(argc), argc, plural_s(argc),
             n_param, plural_s(n_param));
    }

}

static void run_script_line(const line *l, const word *argv)
{
    char **eval_params = eval_word_list(l->org, l->words);
    switch(l->command){
    case AssertMatch:
        run_assert_match_command(l->org, eval_params);
        break;

    case AssertFile:
        run_assert_file_command(l->org, eval_params);
        break;

    case ErrorPrint:
        run_print_command(stderr, eval_params);
        break;

    case Print:
        run_print_command(stdout, eval_params);
        break;

    case DebugPrint:
        if(debug){
            run_print_command(stdout, eval_params);
        }
        break;

    case SetParameters:
        run_set_parameters_command(l->org, eval_params, argv);
        break;

    case Run:
        run_command(l->org, eval_params);
        break;

    case DebugRun:
        if(debug){
            run_command(l->org, eval_params);
        }
        break;

    case Set:
        run_set(l->org, eval_params);
        break;

    case Load:
        run_load(l->org, eval_params);
        break;

    case Empty:
        // Nothing to do.
        break;
    }
    rfre_params(eval_params);
}

void run_script(const line *script, const word *argv)
{
    const line *p = script;

    while(p != lineNIL){
        run_script_line(p, argv);
        p = p->next;
    }
}
