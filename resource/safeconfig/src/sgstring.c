/* File: sgstring.c
 *
 * String manipulation routines.
 */

#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include "tmdefs.h"
#include "origin.h"
#include "sgstring.h"
#include "util.h"
#include "error.h"

#define IS_ESCAPABLE(c) (((c)=='"')||((c)=='\\'))

char *create_string( size_t sz )
{
    char *res = (char *) malloc(sz);
    if(res == 0){
        fatal_error("Out of memory");
    }
    return res;
}


/* Given a pointer to a string 's' and a pointer to a character pointer 'w',
 * scan the string for a 'word'.
 *
 * First, all characters matching isspace() are skipped.
 * After that a word is scanned, a new string is allocated for the word,
 * and assigned to '*w'. If there is no other word in the string, '*w'
 * is set to stringNIL.
 *
 * A word is one of the following regular expressions:
 * [^ \t\n\r\f\0]+: An arbitrary string of nonblanks and non-specials.
 * "[^"\0]*": Arbitrary characters surrounded by "".
 */
const char *scanword( const origin *org, const char *s, char **w )
{
    const char *start;
    const char *end;

    while( isspace( *s ) ){
        s++;
    }
    if( *s == '\0' ){
        *w = stringNIL;
        return s;
    }
    if( *s == DQUOTE ){
        s++;
        start = s;
        while( *s != DQUOTE && *s != '\0' ){
            if( *s == '\\' && IS_ESCAPABLE( s[1] ) ){
                s++;
            }
            s++;
        }
        end = s;
        if( *s != DQUOTE ){
            origin_error( org, "unexpected end of line" );
        }
        else {
            s++;
        }
    }
    else {
        start = s;
        while( *s != '\0' && !isspace( *s ) ){
            s++;
        }
        end = s;
    }
    *w = extract_string(start, end);
    return s;
}

/* Given a string 's', ensure that it does not contain an other
   parameter, or else complain.
 */
static void cknopar( const origin *org, const char *s )
{
    while( isspace( *s ) ) s++;
    if( *s != '\0' ){
        origin_error( org, "excess function parameters: `%s'", s );
    }
}

/* Given a parameter string 'p' and a pointer to a string pointer
   'p1', ensure that 'p' contains exactly one parameter and create
   a copy of the string to put in '*p1'.
 */
void scan1par( const origin *org, const char *pl, char **p1 )
{
    pl = scanword( org, pl, p1 );
    if( *p1 == stringNIL ){
        origin_error( org, "missing parameter" );
        return;
    }
    cknopar( org, pl );
}

/***************************************************************
 *   error handling                                            *
 ***************************************************************/

/* Given a string 's', ensure that it is a correct number,
   or else complain.
 */
bool cknumpar( const origin *org, const char *n )
{
    const char *s = n;

    while( isspace( *s ) ){
        s++;
    }
    if( *s == '-' || *s == '+' ){
        s++;
    }
    while( isdigit( *s ) ){
        s++;
    }
    while( isspace( *s ) ){
        s++;
    }
    if( *s != '\0' ){
        origin_error( org, "malformed number `%s'", n );
        return false;
    }
    return true;
}

/* Return a new "1" or "0" string reflecting the value of boolean 'b'. */
char *newboolstr( bool b )
{
    return new_string( ( b ? "1" : "0" ) );
}

/* Return a new string reflecting the value of int 'n'. */
char *newintstr( int n )
{
    char *buf;

    (void) asprintf( &buf, "%d", n );
    return buf;
}

/* Return a new string reflecting the value of uint 'n'. */
char *newuintstr( unsigned int n )
{
    char *buf;

    (void) asprintf( &buf, "%u", n );
    return buf;
}

/* Given a string 'p', chop it into words using 'scanword' and return
 * a stringlist containing the pieces.
 */
word *chopstring( const origin *org, const char *p )
{
    char *s;
    word *sl = wordNIL;
    word **backPtr = &sl;

    for(;;){
        p = scanword( org, p, &s );
        if( s == stringNIL ){
            break;
        }
        word *w = new_word(s);
        *backPtr = w;
        backPtr = &w->next;
        fre_string(s);
    }
    return sl;
}

/* Given a string_list 'sl' and a separation string 'sep', construct one
 * single (newly allocated) string from all strings in 'sl' separated from
 * each other with a copy of 'sep'.
 *
 * To prevent problems with a fixed buffer, the final length of the
 * string is calculated, and sufficient room is allocated for that
 * string.
 *
 * the total length of the required string is
 *  sum(<all joined strings>) + (n-1)*strlen(sep)
 */
char *sep_string_array( char **sl, int startix, const char *sep )
{
    char *cs;           /* string under construction */
    char *bufp;         /* pointer in string under construction */
    unsigned int len;   /* calculated length of string */
    int ix;

    unsigned int seplen = strlen(sep);
    len = 0;
    ix = startix;
    while(true){
        const char *e = sl[ix];
        if(e == stringNIL){
            break;
        }
        if(ix != startix){
            len += seplen;
        }
        len += (unsigned int) strlen( e );
        ix++;
    }
    cs = create_string( len+1 );
    bufp = cs;
    ix = startix;
    while(true){
        const char *e = sl[ix];
        if(e == stringNIL){
            break;
        }
        if(ix != startix){
            const char *cp = sep;
            while( *cp!='\0' ){
                *bufp++ = *cp++;
            }
        }
        {
            const char *cp = e;
            while( *cp!='\0' ){
                *bufp++ = *cp++;
            }
        }
        ix++;
    }
    *bufp = '\0';
    return cs;
}

/* Given a string_list 'sl', construct one single string from by separating
 * all strings in 'sl' from each other with a ' '.
 */
char *flat_string_array( char **sl, int startix )
{
    return sep_string_array( sl, startix, " " );
}


/* Given a string_list 'sl' and a separation string 'sep', construct one
 * single (newly allocated) string from all strings in 'sl' separated from
 * each other with a copy of 'sep'.
 *
 * To prevent problems with a fixed buffer, the final length of the
 * string is calculated, and sufficient room is allocated for that
 * string.
 *
 * the total length of the required string is
 *  sum(<all joined strings>) + (n-1)*strlen(sep)
 */
char *sepstrings( const word *sl, const char *sep )
{
    const char *cp;	/* pointer in copied strings  */
    char *cs;		/* string under construction */
    char *bufp;		/* pointer in string under construction */
    unsigned int len;	/* calculated length of string */
    const word *wp;
    bool first;

    unsigned int seplen = strlen(sep);
    len = 0;
    wp = sl;
    first = true;
    while(wp != wordNIL){
        if(first){
            first = false;
        }
        else {
            len += seplen;
        }
        len += (unsigned int) strlen( wp->string );
        wp = wp->next;
    }
    cs = create_string( len+1 );
    bufp = cs;
    wp = sl;
    first = true;
    while(wp != wordNIL){
        if(first){
            first = false;
        }
        else {
            cp = sep;
            while( *cp!='\0' ){
                *bufp++ = *cp++;
            }
        }
        cp = wp->string;
        while( *cp!='\0' ){
            *bufp++ = *cp++;
        }
        wp = wp->next;
    }
    *bufp = '\0';
    return cs;
}

/* Given a string_list 'sl', construct one single string from by separating
 * all strings in 'sl' from each other with a ' '.
 */
char *flatstrings( const word *sl )
{
    return sepstrings( sl, " " );
}

/* Return true if this string represents false. */
bool isfalsestr( const char *s )
{
    const char *p = s;

    while( isspace( *p ) ){
        p++;
    }
    if( p[0] != '0' ){
        /* This also covers the empty string. */
        return false;
    }
    return ( isspace( p[1] ) || p[1] == '\0' );
}

/* Return true if this string does not represent FALSE. */
bool istruestr( const char *s )
{
    const char *p = s;

    while( isspace( *p ) ){
        p++;
    }
    if( p[0] != '0' ){
        /* This also covers the empty string. */
        return true;
    }
    return ( !(isspace( p[1] ) || p[1] == '\0') );
}

void fre_string(char *s)
{
    free(s);
}

char *new_string(const char *s)
{
    size_t sz = strlen(s) + 1;
    char *res = create_string( sz );
    strcpy(res, s);
    return res;
}

char *extract_string(const char *start, const char *end)
{
    size_t sz = (end-start);
    char *res = create_string( sz+1 );
    memcpy(res, start, sz);
    res[sz] = '\0';
    return res;
}

char *realloc_string(char *s, unsigned int sz)
{
    char *res = (char *) realloc(s, sz);
    if(res == NULL){
        fatal_error("Out of memory");
    }
    return res;
}
