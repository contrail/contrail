/*
 * line.c
 *
 *  Created on: Mar 13, 2012
 *      Author: Kees van Reeuwijk
 */

#include <stdio.h>
#include <stdlib.h>

#include "line.h"
#include "error.h"

line *new_line(enum Command cmd, const origin *org, const word *words)
{
    line *res = (line *) malloc(sizeof(struct str_line));
    if(res == NULL){
        fatal_error("Out of memory");
        return lineNIL;
    }
    res->command = cmd;
    res->org = rdup_origin(org);
    res->words = rdup_word_list(words);
    res->next = lineNIL;
    return res;
}

void rfre_line(line *l)
{
    rfre_origin(l->org);
    rfre_word_list(l->words);
    free(l);
}

void rfre_line_list(line *l)
{
    while(l != lineNIL){
    	line *n = l->next;
        rfre_line(l);
        l = n;
    }
}
