/* File var.c
 *
 * Variable management.
 */

/* standard UNIX libraries */
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

/* local definitions */
#include "tmdefs.h"
#include "origin.h"
#include "error.h"
#include "sgstring.h"
#include "util.h"
#include "var.h"
#include "global.h"
#include "word.h"

#define variableNIL ((struct str_variable *) 0)

struct str_variable {
    struct str_variable *next;
    char *name;
    char *val;
};

typedef struct str_variable variable;


/******************************************************
 *    Datastructures                                  *
 ******************************************************/

static variable *variables;


static void delete_variable( variable *v )
{
    fre_string(v->name);
    fre_string(v->val);
    free(v);
}

static variable *new_variable(const char *nm, const char *val)
{
    variable *res = (variable *) malloc(sizeof(struct str_variable));
    if(res == NULL){
        fatal_error("Out of memory");
    }
    res->name = new_string(nm);
    res->val = new_string(val);
    res->next = variableNIL;
    return res;
}

/******************************************************
 *    variable management                             *
 ******************************************************/

/* Search in all contexts for variable with name 'nm'. Return pointer to
   struct variable of variable, or variableNIL if not found.
 */
static variable *findvar( const char *nm )
{
    variable *l = variables;

    while( l != variableNIL ){
        if( strcmp( nm, l->name ) == 0 ) return l;
        l = l->next;
    }
    return variableNIL;
}

/* Add a variable 'nm' with value 'v' to the known variables.
   If variable 'nm' exists, overwrite old value, else create a
   new variable with the given value.
 */
void set_var_string( const char *nm, const char *v )
{
    variable *nwvar;

    if( vartr ){
        fprintf( tracestream, "set: %s = %s\n", nm, v );
    }
    nwvar = findvar( nm );
    if( nwvar != variableNIL ){
        fre_string( nwvar->val );
        nwvar->val = new_string( v );
        return;
    }
    nwvar = new_variable( nm, v );
    /* Insert this variable in the linked list. */
    nwvar->next = variables;
    variables = nwvar;
}

/* Search for variable with name 'nm'. Return pointer to the value of
   'nm', or NULL of not found.
 */
const char *getvar( const char *nm )
{
    variable *v = findvar( nm );

    if( v != variableNIL ){
        if( vartr ){
            fprintf( tracestream, "found: %s = %s\n", nm, v->val );
        }
        return v->val;
    }
    if( vartr ) fprintf( tracestream, "not found: %s\n", nm );
    return stringNIL;
}

/******************************************************
 *    initialization                                  *
 ******************************************************/

/* Initialize 'variable' routines. */
void init_var(void )
{
    variables = variableNIL;
}

/* Terminate 'variable' routines. */
void end_var( void )
{
    variable *v = variables;
    while(v != variableNIL){
    	variable *n = v->next;
        delete_variable( v );
        v = n;
    }
    variables = variableNIL;
}
