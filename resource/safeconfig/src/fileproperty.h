/*
 * fileproperty.h
 *
 *  Created on: Apr 19, 2012
 *      Author: Kees van Reeuwijk
 */

#ifndef FILEPROPERTY_H_
#define FILEPROPERTY_H_

enum FileProperty {
	FPExists,
        FPReadable,
	FPWritable,
	FPExecutable,
	FPPlain,
        FPDirectory,
	FPNotExists,
        FPNotReadable,
	FPNotWritable,
	FPNotExecutable,
	FPNotPlain,
        FPNotDirectory
};

#endif /* FILEPROPERTY_H_ */
