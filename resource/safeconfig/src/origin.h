/* File: origin.h
 *
 */

#ifndef _INC_ORIGIN_H
#define _INC_ORIGIN_H

typedef struct str_origin origin;

struct str_origin {
    char *file;
    int line;
};

#define originNIL ((struct str_origin *) 0)

extern origin *rdup_origin(const origin *org);
extern origin *new_origin(const char *file, int line);
extern void rfre_origin(origin *org);

#endif
