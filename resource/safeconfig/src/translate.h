// File: translate.h

#ifndef _INC_TRANSLATE_H
#define _INC_TRANSLATE_H

#include "line.h"

extern void translate_script(const line *script, const char *fnm);

#endif
