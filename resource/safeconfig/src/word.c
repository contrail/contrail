/*
 * word.c
 *
 *  Created on: Mar 14, 2012
 *      Author: Kees van Reeuwijk
 */

#include <stdbool.h>
#include <stdlib.h>
#include "sgstring.h"
#include "word.h"
#include "error.h"

word *new_word(const char *string)
{
    word *res = (word *) malloc(sizeof(struct str_word));
    if(res == 0){
        fatal_error("Out of memory");
    }
    res->string = new_string(string);
    res->next = wordNIL;
    return res;
}

void fre_word(word *w)
{
    fre_string(w->string);
    free(w);
}

void rfre_word_list(word *w)
{
    while(w != wordNIL){
        word *n = w->next;
        fre_word(w);
        w = n;
    }
}

word *rdup_word_list(const word *w)
{
    word *res = wordNIL;
    word **prev = &res;
    while(w != wordNIL){
        word *nw = new_word(w->string);
        *prev = nw;
        prev = &nw->next;
        w = w->next;
    }
    return res;
}

void print_word_list(FILE *f, const word *v)
{
    bool first = true;
    if(v == wordNIL){
        fputs("<empty>", f);
        return;
    }
    while(v != wordNIL){
        if(first){
            first = false;
        } else {
            fputc('"', f);
        }
        fputc('"', f);
        fputs(v->string, f);
        v = v->next;
        fputc('"', f);
    }
}
