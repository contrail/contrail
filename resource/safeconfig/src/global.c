/* File: global.c */

#include <stdbool.h>
#include <stddef.h>
#include "global.h"

bool vartr = false;
bool sevaltr = false;
bool noerrorline = false;
bool fntracing = false;
bool dryrun = false;
bool debug = false;
bool showrun = false;
bool hide_regex_error = false;

FILE *tracestream;
