.TH SAFECONFIG 1 "Vrije Universiteit"
.UC
.SH NAME
safeconfig \- safe system configuration 
.SH SYNOPSIS
.B safeconfig
[
.B \-d<debugflags>
] [
.B \-e <file>
] [
.B \-h
] [
.B \-o <file>
]
[
.B \-s<var>[=<val>]
]
.B <config-file>
.B <parameter>...<parameter>
.br
.SH DESCRIPTION
.I Safeconfig
is a program to safely execute commands that are not normally
executable by an unprivileged user.  The program works on configuration files,
where a configuration file specifies a list of programs to run.
Execution of the programs can be preceded by assertions
that the parameters that are passed to the programs are
valid.
.PP
Although such configuration files may sound like scripts, the language of
.I safeconfig
is so simple that it only allows configuration sequences that are
easy to review for correctness and safety.  There are no loops,
conditions, accesses to environment variables, file inclusions,
search paths, tricky variable expansions, or any other complicated
constructs that can cause bugs or provide opportunities for malicious
users.
.PP
One of the applications of safeconfig is to allow users to do system
administration tasks without giving them full adminstration
privileges.  Examples of such tasks are removing junk files from
/tmp, restarting a troublesome network connection, or creating new
users on the system.
.PP
In contrast to
.IR sudo(1) ,
.I safeconfig
does not necessarily give full access to a command; the configuration
file only allows a specific sequence of commands, and a specific
pattern of parameters to these commands.  In contrast to set-UID
shell scripts, the configuration files of
.I safeconfig
does not offer a full and tricky programming language that makes
it difficult to verify the safety of such scripts. On many systems
the set-UID flag is even ignored for shell scripts since shells are
considered impossible to secure. In contrast to custom-written
executables with set-UID flags, the configurations of
.I safeconfig
are easy to write and read.
.PP
.I Safeconfig
is also designed to be invoked from large programs such as webservers,
mail processors, and interpreters such as those for Java, Perl, and
Python. Such programs are far too large to be safely executed with
elevated privileges, and for the vast majority of the time such
privileges are not necessary. Nevertheless, sometimes elevated
privileges are necessary for small and confined operations such as
a modification to the network configuration, software updates, or
other administration tasks.
.PP
For programs written in C or C++,
.I safeconfig
should be invoked using 
.I execl(2)
or
.IR execv(2) .
It is
.I not
recommended to invoke
.I safeconfig
with 
.I system(3)
or
.IR popen(3) ,
because this will invoke
.I sh(1)
with all its security issues.
For Java, the methods in
.I java.lang.Processbuilder
are recommended.
For Python the
.I call()
function in the
.I subprocess
module is recommended.
For Perl,
it is recommended to use the
.I system()
function with an array of parameters, and
.I not
the variation with a single string for the entire invocation;
the latter uses a shell to execute the command, which is less safe.
.PP
Although
.I safeconfig
is run with root permissions, it first lowers its privileges to that
of the invoking user before reading a configuration file (and fails
if the user cannot read the file). If the configuration file has the
set-user-ID-on-execution permission set, it then changes the user id to
that of the configuration file before it runs any commands.  Similarly,
if the configuration file has the set-group-ID-on-execution permission
set, the group id is changed to that of the configuration file before any
commands are run.  Since Unix only allows root to change file ownership,
as an unprivileged user you cannot escalate your privileges by creating
a configuration file: the configuration file is owned by you, and the
commands in it will be read and executed with the privileges you already
have.  Similarly, you can only change the group of a file to a group
you are a member of, so again no escalation of privileges is possible.
However, the owner of the configuration file can give you permission
to run the configuration file with their privileges by setting the
set-user-ID-on-execution and/or the set-group-ID-on-execution permissions.
As an additional security measure,
.I safeconfig
refuses to run configurations that are group- or world-writable.
.SH OPTIONS
.IP \-d<debugflags>
Set the debugging flags given in
.I <debugflags>.
Use the
.B \-h
option to get a list of debug flags and their meaning.
.IP -D
Run the
.I debugrun
and
.I debugprint
commands in the configuration file.
Without this flag these commands are skipped.
.IP \-e<file>
Write error messages to the given file. By default they are written to stderr.
.IP \-h
Print the options of
.I safeconfig
and some version information.
.IP \-n
Only show the commands that would be run, do not actually run them. Implies the -r option.
.IP \-r
Show the
.B run
commands as they are run.
.IP \-o<file>
Write the output to the given file. By default it is written to stdout.
.IP \-s<var>
Set the configuration variable <var> to the empty string.
.IP \-s<var>=<val>
Set the configuration variable <var> to value <val>.
.SH "CONFIGURATION FILES"
Any ordinary file that is not writable by the group or by others
can be used as a configuration file, but by convention a
configuration file has the file extension \fI.conf\fR.
Empty lines, or lines that have a '#' as the first non-whitespace
character are ignored.
All other lines are split into a sequence of words,
and every word is then evaluated for variable references and
arithmetic expressions. This evaluation never changes the 
number of words in the line, although it may result in
empty words.
.PP
A word consists either of a the longest possible sequence
of non-whitespace characters, or a sequence of arbitrary
characters surrounded by unescaped double-quote ('"') characters.
.PP
A $ character followed by square open bracket ('[') denotes an arithmetic
expression. The expression is terminated by a square close bracket (']').
Arithmetic expressions may contain integer constants, and bracketed
expressions denoting evaluation order.
It may contain the unary operators \fB+\fR,
\fB-\fR, \fB~\fR, and \fB!\fR.
It may contain the binary arithmetic operators \fB+\fR, \fB-\fR,
\fB*\fR, \fB/\fR, and \fB%\fR.
It may contain the comparison operators
\fB<\fR,
\fB<=\fR,
\fB>\fR,
\fB>=\fR,
\fB==\fR, and
\fB!=\fR.
It may contain the binary operators \fB&\fR, and \fB|\fR.
When evaluating boolean values, 0 denotes \fBfalse\fR, and any other
value denotes \fBtrue\fR. When generating boolean values, \fBtrue\fR
is always represented as 1.
For example, '$[1+2]' is replaced by '3', and '$[3<5]' is replaced
by '1'.
.PP
A $ character followed by a round open bracket ('(') denotes a variable
reference. The expression is terminated by a round close bracket (')').
The string between the brackets represents the variable name. For
example, if the variable \fBNAME1\fR has been set to the string 'bilbo',
the expression '$(NAME1)' evaluates to 'bilbo'. Variable references themselves
are also evaluated, so if the variable \fBIX\fR is set to 1, 
the expression '$(NAME$(IX)) also evaluates to 'bilbo'.
.PP
A $ character followed by any character except '[' or '('
denotes a reference to a variable with a single character.
For example, both '$N' and '$(N)' represent a reference to a variable
\fBN\fR.
.PP
As said, each line in a configuration file denotes a command.
The following commands are supported:
.IP "\fBparameters <variable>...<variable>\fR"
Assign the command-line parameters of the
.I safeconfig
invocation to the given variables. The number of parameters given
and the number of variables in the list must must be the same.
.IP "\fBrun <program> <parameter>...<parameter>\fR"
Run the given program with the given parameters.
No search path is maintained, so 
usually an absolute path to the program must
be given.
.IP "\fBdebugrun <program> <parameter> ... <parameter>\fR"
Similar to the
.I run
command, but the program is only run if the
.I -D
commandline flag has been set.
.IP "\fBprint <value>...<value>\fR"
Print the given values to the standard output.
.IP "\fBdebugprint <value>...<value>\fR"
If the -D commandline flag has been set, print the given values
to the standard output.
.IP "\fBset <variable> <value>...<value>\fR"
Set the given variable to the concatenation of the given values.
.IP "\fBload <variable> <filename>\fR"
Set the given variable to the content of the given file.
Any whitespace (spaces, tabs, newlines, carriage returns) at the
beginning and end of the file is removed, and every other sequence
of whitespace characters is replaced by a single space.
.IP "\fBassertmatch <pattern> <expression>\fR"
Assert that the given expression matches the given regular expression.
The regular expresssion uses the syntax and semantics of \fIregex\fR\|(3).
The regular expression is `anchored' to the start and end of the
expression.
.IP "\fBassertfile <file> <property>...<property>\fR"
Assert that the given file meets all of the given properties.
The following properties are supported: \fBexists\fB, \fBreadable\fR, \fBwritable\fR,
\fBexecutable\fR, \fBplain\fR, \fBdirectory\fR.
The property \fBexists\fR means readable or writable. All listed assertions
fail for a non-existent file.
Prefixing the
property with \fBnot-\fR inverses the meaning of the assertion,
so \fBnot-writable\fR asserts that the file is not writable.
.SH "EXAMPLES"
The following configuration file performs a set of operations
to stop the libvirt network with the given name.
.RS
.PP
.nf
# Stops a libvirt network.
#
# Usage:
#   safeconfig stop-network.conf <network-name>
#
# Where <network-name> is the name of the network to stop.

parameters NW_NAME

#parameters sanity check
assertmatch [[:alnum:]_-]+ $(NW_NAME)

#stop libvirt network
run /usr/bin/virsh net-destroy $(NW_NAME)

#remove leases file
run /bin/rm -f /var/lib/libvirt/dnsmasq/$(NW_NAME).leases
#remove hosts file
rm /var/lib/libvirt/dnsmasq/$(NW_NAME).hostsfile
.fi
.RE
.PP
.SH "SEE ALSO"
\&\fIsu\fR\|(1),
\&\fIexecv\fR\|(2),
\&\fIsudo\fR\|(8),
\&\fIregex\fR\|(3)
