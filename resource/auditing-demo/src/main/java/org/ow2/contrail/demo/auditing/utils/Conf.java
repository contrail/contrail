package org.ow2.contrail.demo.auditing.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

public class Conf {
    private static Logger log = LoggerFactory.getLogger(Conf.class);
    private static Properties props;

    private Conf() {
    }

    public static void load(String confFile) throws Exception {
        props = new Properties();
        try {
            props.load(new FileInputStream(confFile));
            log.info("Configuration loaded successfully from file '{}'.", confFile);
        }
        catch (IOException e) {
            throw new Exception(String.format("Failed to load configuration file '%s': %s", confFile, e.getMessage()));
        }
    }

    public static String getUserUUID() {
        return props.getProperty("userUUID");
    }

    public static String getProviderUUID() {
        return props.getProperty("providerUUID");
    }

    public static String getAccountingRequestData() {
        return props.getProperty("accounting.requestData");
    }

    public static URI getOAuthASUri() throws URISyntaxException {
        return new URI(props.getProperty("oauth-as.url"));
    }

    public static URI getOAuthASAdminUri() throws URISyntaxException {
        return new URI(props.getProperty("oauth-as.url")).resolve("admin/");
    }

    public static URI getOAuthASTokenEndpoint() throws URISyntaxException {
        return new URI(props.getProperty("oauth-as.url")).resolve("r/access_token/request");
    }

    public static URI getFederationApiUri() throws URISyntaxException {
        return new URI(props.getProperty("federation-api.url"));
    }

    public static URI getAuditManagerUri() throws URISyntaxException {
        return new URI(props.getProperty("audit-manager.url"));
    }

    public static String getClientKeystoreFile() {
        return props.getProperty("oauthClient.keystore.file");
    }

    public static String getClientKeystorePass() {
        return props.getProperty("oauthClient.keystore.pass");
    }

    public static String getClientTruststoreFile() {
        return props.getProperty("oauthClient.truststore.file");
    }

    public static String getClientTruststorePass() {
        return props.getProperty("oauthClient.truststore.pass");
    }

    public static String getClientID() {
        return props.getProperty("oauthClient.id");
    }

    public static String getClientSecret() {
        return props.getProperty("oauthClient.secret");
    }

}
