*******************************************************************************
                                Auditing Demo
*******************************************************************************

I. Prerequsities

* federation-api
* federation-accounting
* provider-accounting
* audit-manager
* auditing-api
* oauth-as
* storage-manager

II. Installation

mvn clean package

Deploy the package contrail-auditing-demo.tar.gz.

III. Configuration

Configuration files:
* /etc/contrail/auditing-demo/auditing-demo.properties
* /usr/share/contrail/auditing-demo/log4j.properties

IV. Running the application

java -cp "/usr/share/contrail/auditing-demo:/usr/share/contrail/auditing-demo/lib/*" org.ow2.contrail.demo.auditing.AuditingDemo --config /etc/contrail/auditing-demo/auditing-demo.properties

Output is written to the console and to the log file /var/log/contrail/auditing-demo.log (by default log4j
configuration).
