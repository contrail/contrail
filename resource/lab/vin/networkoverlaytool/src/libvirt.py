from cfg.msg import Msg

from fabric.api import run
from fabric.operations import sudo
from fabric.operations import put
from fabric.context_managers import settings

from helper.libvirtdomainxmlparser import virtualmachinetodomainxml

from virtualmachine import VirtualMachine
from virtualclusterexception import VirtualClusterException

import os

##
# The temporary path where the domain XML contents will be stored on the local
# disk. This temporary file will be uploaded to the remote physical machine and
# will help with booting up the remote virtual machine.
#
temporarydomainxmlpath = "domainXMLupload.xml"

##
# Starts a remote virtual machine. The Fabric connection has already been
# established and the details of the virtual machine come as parameters to this
# function. 
#
# @param virtualmachine VirtualMachine The settings of the virtual machine that
# needs to be started. 
# @throws VirtualClusterException if problems occur while tryin to start the
# virtual machine.
#
def startvm(virtualmachine):
  # Get the libvirt domain XML file representation from the virtual machine
  # instance. This will subsequently be used in order to boot up the virtual
  # machine on the remote host.
  domainxml = virtualmachinetodomainxml(virtualmachine)

  # Next step: flush the domain XML contents to disk so that we can subsequently
  # upload it to the remote physical machine.
  FILE = open(temporarydomainxmlpath, "w")
  FILE.write(domainxml)
  FILE.close()

  # First we will upload the domain XML file to the remote machine (physical).
  put(temporarydomainxmlpath, "~/{0}".format(temporarydomainxmlpath))

  # All the prerequisite steps have been made, it is time to instruct the remote
  # libvirt library to bootup our machine by feeding it the uploaded domain XML
  # file.
  out = sudo("virsh create {0}".format(temporarydomainxmlpath))

  if out.failed:
    # Cleanup.
    run("rm ~/{0}".format(temporarydomainxmlpath))
    os.remove(temporarydomainxmlpath)

    raise VirtualClusterException(
          Msg.virtualclusterlibvirtcreatedomain.format(virtualmachine.parentip))

  # Now that we already used the uploaded domain XML file to boot up the
  # machine, we can remove it.
  out = run("rm ~/{0}".format(temporarydomainxmlpath))

  if out.failed:
    # Cleanup.
    os.remove(temporarydomainxmlpath)

    raise VirtualClusterException(
               Msg.virtualclusterlibvirtcleanup.format(virtualmachine.parentip))

  # Time to cleanup the local hard disk for any auxiliary resources that have
  # been used for starting up the virtual machine.
  os.remove(temporarydomainxmlpath)

##
# Stops a remote virtual machine. The Fabric connection has already been
# established and the details of the virtual machine come as parameters to this
# function.
#
# @param virtualmachine VirtualMachine The settings of the virtual machine tha
# needs to be stopped.
#
def stopvm(virtualmachine):
  with settings(warn_only=True):
    # libvirt on the remote machine makes the stopping of the virtual machine
    # virtually only a one line command!
    out = sudo("virsh destroy {0}".format(virtualmachine.name), shell=False)

    if out.failed:
      print Msg.virtualclusterlibvirtdestroydomain



