from cfg.config import Config
from cfg.msg import Msg

from helper.ipconvertors import inttoip
from helper.ipconvertors import iptoint

from virtualclusterexception import VirtualClusterException

import os

class VirtualMachine:

  ##
  # A unique id for the virtual machine.
  #
  uniqueid = 0

  ##
  # The hostname.
  #
  host = ""

  ##
  # The ip address of the virtual machine.
  #
  ip = ""

  ##
  # The ip address that will be given to the virbr virtual interface that will
  # be used as a gateway on the physical machines.
  #
  gwbridgeip = "192.168.0.1"

  gwbridgeid = "0"

  ##
  # The ip address of the parent physical machine.
  #
  parentip = ""

  ##
  # The user name that is used to login to the parent physical machine in order
  # to setup the machine for the Virtual Cluster.
  #
  __parentuser = ""

  ##
  # The password that is used to login to the parent physical machine in order
  # to setup the machine for the Virtual Cluster.
  #
  __parentpass = ""

  ##
  # The name of the virtual machine.
  #
  name = ""

  ##
  # The domain used to bootup the machine. This is related to the Virtual
  # Machine Monitor. Examplex include "xen" and "kvm".
  #
  domain = ""

  ##
  # The memory destined for this virtual machine.
  #
  memory = ""

  ##
  # The number of virtual cpus destined for this virtual machine.
  #
  vcpu = ""

  ##
  # The path to the emulator used for this virtual machine.
  #
  emulator = ""

  ##
  # The path to the image that is to be used as a hard disk for this virtual
  # machine.
  #
  imagepath = ""

  ##
  # The MAC address to be assigned to the virtual machine's network interface.
  #
  mac = ""

  ##
  # The gateway's IP address. Please note that for any virtual machine the
  # gateway will be a virtual interface on the physical machine. Hence, this
  # virtual interface will have the task of forwarding the packets from and to
  # the underlying virtual machine.
  #
  gatewayip = ""

  ##
  # The operating system of this virtual machine in a string representation.
  #
  operatingsystem = ""

  ##
  # The subinterface on the physical machine on which this virtual machine is
  # "attached". The encrypted communication is further sent through this
  # interface.
  #
  subinterface = None

  ##
  # The process id of the racoon instance started on the underlying physical
  # machine.
  #
  racooninstancepid = -1000000

  ##
  # Whether the virtual machine is granted internet access or not.
  #
  internetconnected = False

  ##
  # The name of the site (physical cluster) on which this virtual machine
  # resides.
  #
  site = ""

  ##
  # The script with security associations set to detele. Used for cleanly
  # removing a virtual cluster. Used because merging multiple cluster is a many
  # step process which in could end up combining many levels of cluster. This
  # script keeps track of what needs to be removed for this virtual/physical
  # machine.
  #
  deleteipsecassocscript = ""

  ##
  # The script with ike associations set to detele. Used for cleanly
  # removing a virtual cluster. Used because merging multiple cluster is a many
  # step process which in could end up combining many levels of cluster. This
  # script keeps track of what needs to be removed for this virtual/physical
  # machine.
  #
  deleteikescript = ""

  ##
  # The list of taken used ports on the parent physical machine.
  #
  __ports = None

  ##
  # A dictionary(port, virtualmachine) that says each port to which virtual
  # machine belongs.
  #
  __porttovmmapping = None

  ##
  # The list of used ips on the parent physical machine (virbr interface and
  # virtual machine ips).
  #
  __ips = None

  ##
  # A dictionary(ip, virtualmachine) that says each IP to which virtual machine
  # it belongs.
  #
  __iptovmmapping = None

  __ipethtovmmapping = None
  __ipseth = None

  __gwids = None
  __gwidtovmmapping = None

  ##
  # Each machine comes with an available range of ports that will be used for
  # the IKE tool. This is the minimum value of the range.
  #
  portRangeMin = 500
  
  ##
  # As portRangeMin, but this is the maximum value of the range.
  #
  portRangeMax = 530

  ##
  # Each site comes with an available range of private IP's for their virtual
  # machines. This is the minimum value of the range in integer format.
  #
  ipRangeMin = 0

  ##
  # The maximum value for the range.
  #
  ipRangeMax = 0

  gwidRangeMin = 0

  gwidRangeMax = 9

  ipethRangeMin = 0
  ipethRangeMax = 0

  def __init__(
          self, uniqueid=None, host=None, ip=None, parentip=None,
          parentuser=None, parentpass=None, name=None, domain=None,
          memory=None, vcpu=None, emulator=None, imagepath=None, mac=None,
          gatewayip=None, operatingsystem=None, subinterface=None):
    self.uniqueid = uniqueid
    self.host = host
    self.ip = ip
    self.parentip = parentip
    self.__parentuser = parentuser
    self.__parentpass = parentpass
    self.name = name
    self.domain = domain
    self.memory = memory
    self.vcpu = vcpu
    self.emulator = emulator
    self.imagepath = imagepath
    self.mac = mac
    self.gatewayip = gatewayip
    self.operatingsystem = operatingsystem
    if subinterface != None:
      self.subinterface = subinterface

  ##
  # Getter for the parent username.
  #
  def getparentuser(self):
    return self.__parentuser

  ##
  # Getter for the parent password.
  #
  def getparentpass(self):
    return self.__parentpass

  ##
  # Username member setter.
  #
  # @param username String The new username.
  #
  def setparentuser(self, parentuser):
    self.__parentuser = parentuser

  ##
  # Password member setter.
  #
  # @param password String The new password.
  #
  def setparentpass(self, parentpass):
    self.__parentpass = parentpass

  ##
  # Loads the taken ports from disk.
  #
  def __loadports(self):
    self.__ports = []
    self.__porttovmmapping = {}

    # Directory with port descriptions.
    portdir = "{0}/ports/".format(Config.resourcespath)
    if os.path.exists(portdir) == False:
      return

    portfilepath = "{0}ports_{1}.txt".format(portdir, self.parentip)

    if os.path.exists(portfilepath) == False:
      return

    f = open(portfilepath, "r")
    
    for line in f.readlines():
      listentry = line.split(' ')
      port = int(listentry[0])
      virtualmachineid = int(listentry[1])
      self.__ports.append(port)
      self.__porttovmmapping[port] = virtualmachineid
    f.close()

  ##
  # Loads the taken IPs from disk.
  #
  def __loadips(self):
    self.__ips = []
    self.__iptovmmapping = {}

    # Directory with ip descriptions.
    ipdir = "{0}/ips/".format(Config.resourcespath)
    if os.path.exists(ipdir) == False:
      return

    ipfilepath = "{0}ips_{1}.txt".format(ipdir, self.site)

    if os.path.exists(ipfilepath) == False:
      return

    f = open(ipfilepath, "r")
    for line in f.readlines():
      listentry = line.split(' ')
      ip = int(iptoint(listentry[0]))
      virtualmachineid = int(listentry[1])
      self.__ips.append(ip)
      self.__iptovmmapping[ip] = virtualmachineid
    f.close()

  def __loadipseth(self):
    self.__ipseth = []
    self.__ipethtovmmapping = {}

    # Directory with ip descriptions.
    ipdir = "{0}/ips/".format(Config.resourcespath)
    if os.path.exists(ipdir) == False:
      return

    ipfilepath = "{0}ipseth_{1}.txt".format(ipdir, self.site)

    if os.path.exists(ipfilepath) == False:
      return

    f = open(ipfilepath, "r")
    for line in f.readlines():
      listentry = line.split(' ')
      ipeth = int(iptoint(listentry[0]))
      virtualmachineid = int(listentry[1])
      self.__ipseth.append(ipeth)
      self.__ipethtovmmapping[ipeth] = virtualmachineid
    f.close()
    
  ##
  # Loads the used gateway bridge ids.
  #
  def __loadbridgeids(self):
    self.__gwids = []
    self.__gwidtovmmapping = {}
    gwdir = "{0}/gwbr/".format(Config.resourcespath)
    if os.path.exists(gwdir) == False:
      return

    gwfilepath = "{0}gwids_{1}.txt".format(gwdir, self.parentip)
    if os.path.exists(gwfilepath) == False:
      return

    f = open(gwfilepath, "r")
    for line in f.readlines():
      listentry = line.split(' ')
      gwid = int(listentry[0])
      vmid = int(listentry[1])
      self.__gwids.append(gwid)
      self.__gwidtovmmapping[gwid] = vmid
    f.close()

  def __flushbridgeids(self):
    gwdir = "{0}/gwbr/".format(Config.resourcespath)
    if os.path.exists(gwdir) == False:
      os.mkdir(gwdir)
    gwfilepath = "{0}gwids_{1}.txt".format(gwdir, self.parentip)
    if os.path.exists(gwfilepath) == True:
      os.remove(gwfilepath)

    f = open(gwfilepath, "w")
    for gwid in self.__gwids:
      f.write("{0} {1}\n".format(gwid, self.__gwidtovmmapping[gwid]))
    f.close()

    self.__gwids = None
    self.__gwidtovmmapping = None

  ##
  # Flushes the current potentially updated set of ports to disk.
  #
  def __flushports(self):
    portdir = "{0}/ports/".format(Config.resourcespath)
    if os.path.exists(portdir) == False:
      os.mkdir(portdir)

    portfilepath = "{0}ports_{1}.txt".format(portdir, self.parentip)

    if os.path.exists(portfilepath) == True:
      os.remove(portfilepath)

    f = open(portfilepath, "w")
    for port in self.__ports:
      f.write("{0} {1}\n".format(port, self.__porttovmmapping[port]))
    f.close()

    self.__ports = None
    self.__porttovmmapping = None

  ##
  # Flushes the current potentially updated set of IPs to disk.
  #
  def __fluships(self):
    ipdir = "{0}/ips/".format(Config.resourcespath)
    if os.path.exists(ipdir) == False:
      os.mkdir(ipdir)

    ipfilepath = "{0}ips_{1}.txt".format(ipdir, self.site)

    if os.path.exists(ipfilepath) == True:
      os.remove(ipfilepath)

    f = open(ipfilepath, "w")
    for ip in self.__ips:
      f.write("{0} {1}\n".format(inttoip(ip), self.__iptovmmapping[ip]))
    f.close()

    self.__ips == None
    self.__iptovmmapping = None

  def __flushipseth(self):
    ipdir = "{0}/ips/".format(Config.resourcespath)
    if os.path.exists(ipdir) == False:
      os.mkdir(ipdir)

    ipfilepath = "{0}ipseth_{1}.txt".format(ipdir, self.site)

    if os.path.exists(ipfilepath) == True:
      os.remove(ipfilepath)
    
    f = open(ipfilepath, "w")
    for ipeth in self.__ipseth:
      f.write("{0} {1}\n".format(inttoip(ipeth), self.__ipethtovmmapping[ipeth]))
    f.close()
    
    self.__ipseth == None
    self.__ipethtovmmapping = None


  ##
  # Gets a free port for a racoon instance on the parent physical machine.
  #
  # @returns int A free port.
  #
  def getfreeport(self):
    if self.__ports == None:
      self.__loadports()

    if len(self.__ports) == 0:
      return self.portRangeMin

    self.__ports.sort()

    prev = self.__ports[0]

    if prev - self.portRangeMin > 0:
      return self.portRangeMin

    for i in range(1, len(self.__ports) - 1):
      next = self.__ports[i]
      if next - prev > 1:
        return prev + 1
      prev = next
          
    if self.portRangeMax - prev > 0:
      return prev + 1

    return None

  ##
  # Gets a free id for the gateway bridge.
  #
  def getfreegwid(self):
    self.__loadbridgeids()
    mapper = {}

    if len(self.__gwids) == 0:
      return self.gwidRangeMin

    for i in range(self.gwidRangeMin, self.gwidRangeMax):
      mapper[i] = "Not taken"

    for gwid in self.__gwids:
      mapper[gwid] = "Taken"

    for i in range(self.gwidRangeMin, self.gwidRangeMax):
      if mapper[i] == "Not taken":
        return i

    return None

  ##
  # Gets a free IP on the parent physical site (for the virbr interfaces or
  # for the VMs.
  #
  # @returns string A free IP.
  #
  def getfreeip(self):
    #if self.__ips == None:
    self.__loadips()

    if len(self.__ips) == 0:
      return inttoip(self.ipRangeMin)

    self.__ips.sort()

    prev = self.__ips[0]

    if prev - self.ipRangeMin > 0:
      return inttoip(self.ipRangeMin)

    for i in range(1, len(self.__ips)):
      next = self.__ips[i]
      if next - prev > 1:
        return inttoip(prev + 1)
      prev = next

    if self.ipRangeMax - prev > 0:
      return inttoip(prev + 1)

    return None

  def getfreeipeth(self):
    self.__loadipseth()
 
    if len(self.__ipseth) == 0:
      return inttoip(self.ipethRangeMin)

    self.__ipseth.sort()

    prev = self.__ipseth[0]

    if prev - self.ipethRangeMin > 0:
      return inttoip(self.ipethRangeMin)

    for i in range(1, len(self.__ipseth)):
      next = self.__ipseth[i]
      if next - prev > 1:
        return inttoip(prev + 1)
      prev = next
 
    if self.ipethRangeMax - prev > 0:
      return inttoip(prev + 1)

    return None

  ##
  # Reserves the given port for use. port obtained with getfreeport needs to be
  # provided.
  #
  # @param port int The port to reserve.
  #
  def reserveport(self, port):
    # Ports were already loaded on the getfreeport call.
    self.__ports.append(port)

    self.__porttovmmapping[port] = self.uniqueid

    self.__flushports()

  ##
  # Reserves the five gateway id.
  #
  def reservebridgeid(self, gwid):
    self.__gwids.append(gwid)
    self.__gwidtovmmapping[gwid] = self.uniqueid
    self.__flushbridgeids()

  ##
  # Reserve the given ip for use. IP obtained with getfreeip needs to be
  # provided
  #
  # @param ip int The IP to reserve in string format.
  #
  def reserveip(self, ip):
    # IPs were already loaded on the getfreeip call.
    self.__ips.append(iptoint(ip))

    self.__iptovmmapping[iptoint(ip)] = self.uniqueid

    self.__fluships()

  ##
  # Reserve tha given ip for use for an eth subinterface. Must reserve and
  # assing an IP address to the VM itself beforehand (reserveip call).
  #
  def reserveipeth(self, ipeth):
    self.__ipseth.append(iptoint(ipeth))
    self.__ipethtovmmapping[iptoint(ipeth)] = self.uniqueid
    self.__flushipseth()

  ##
  # Frees the provided ports (the processes that used them no longer do.
  #
  def releaseports(self):

    if self.__ports == None:
      self.__loadports()

    toremove = []

    for port in self.__ports:
      if self.__porttovmmapping[port] == self.uniqueid:
        toremove.append(port)
    for port in toremove:
      self.__ports.remove(port)

    self.__flushports()

  def releasebridgeids(self):
    if self.__gwids == None or self.gwidtovmmapping == None:
      self.__loadbridgeids()

    toremove = []

    for gwid in self.__gwids:
      if self.__gwidtovmmapping[gwid] == self.uniqueid:
        toremove.append(gwid)
    for gwid in toremove:
      self.__gwids.remove(gwid)
    self.__flushbridgeids()

  ##
  # Frees the provided ips.
  #
  def releaseips(self):
    if self.__ips == None or self.__iptovmmapping == None:
      self.__loadips()

    toremove = []

    for ip in self.__ips:
      if self.__iptovmmapping[ip] == self.uniqueid:
        toremove.append(ip)
    for ip in toremove:
      self.__ips.remove(ip)

    self.__fluships()

  def releaseipseth(self):
    if self.__ipseth == None or self.__ipethtovmmapping == None:
      self.__loadipseth()

    toremove = []

    for ipeth in self.__ipseth:
      if self.__ipethtovmmapping[ipeth] == self.uniqueid:
        toremove.append(ipeth)
    for ipeth in toremove:
      self.__ipseth.remove(ipeth)

    self.__flushipseth()

  ##
  # Gets a string representation of the virtual machine.
  #
  def tostring(self):
    return "VIRTUAL MACHINE\nuniqueid={0}\nhost={1}\nip={2}\nparentip={3}\
            \nparentuser={4}\nparentpass={5}\nname={6}\ndomain={7}\nmemory={8}\
            \nvcpu={9}\nemulator={10}\nimagepath={11}\nmac={12}\ngatewayip={13}\
            \noperatingsystem={14}\n".format(self.uniqueid, self.host,
              self.ip, self.parentip, self.__parentuser, self.__parentpass,
              self.name, self.domain, self.memory, self.vcpu, self.emulator,
              self.imagepath, self.mac, self.gatewayip, self.operatingsystem)


