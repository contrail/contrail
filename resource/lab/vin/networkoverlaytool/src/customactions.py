from fabric.api import run
from fabric.operations import sudo
from fabric.operations import put
from fabric.context_managers import settings

from subinterface import SubInterface
from virtualmachine import VirtualMachine


##
# Deletes the associated subinterface of a virtual machine. This command is
# executed remotely via fabric.
#
# @param subinterface SubInterface The subinterface details of the subinterface
# to be removed.
#
def deletesubinterfaces(subinterface):
  sudo("ifconfig {0} {1} down".format(subinterface.getname(), subinterface.ip))

##
# Add the associated subinterface of a virtual machine. This command is
# executed remotely via fabric.
#
# @param subinterface SubInterface The subinterface details of the subinterface
# to be added.
#
def pushsubinterfaces(subinterface):
  sudo("ifconfig {0} {1} up".format(subinterface.getname(), subinterface.ip))

##
# Add the virbr interface as a gateway for the virtual machine.
#
# @param number Int The number associated with the virbr interface (e.g.
# virbr0).
# @param ip String The IP address for the interface in string format.
# 
def pushvirbr(number, ip):
  sudo("brctl addbr virbr{0}".format(number))
  sudo("ifconfig virbr{0} {1} up". format(number, ip))

##
# Delete the virbr interface that worked as a gateway for a virtual machine
# 
# @param number Int The number associated with the virbr interface (e.g.
# virbr0).
#
def deletevirbr(number):
  sudo("ifconfig virbr{0} down".format(number))
  sudo("brctl delbr virbr{0}".format(number))

##
# Grants internet access to a virtual machine.
#
# @param virtualmachine VirtualMachine The virtual machine to which internet
# access needs to be granted.
#
def grantinternetaccess(virtualmachine):
  sudo("iptables -t nat -I POSTROUTING 1 -s {0} \
! -d 192.168.0.0/16 -j MASQUERADE".format(virtualmachine.ip))

##
# Reverts the granted internet access to a virtual machine.
#
# @param virtualmachine VirtualMachine The virtual machine to which internet
# access needs to be reverted.
#
def revertinternetaccess(virtualmachine):
  sudo("iptables -t nat -D POSTROUTING -s {0} \
! -d 192.168.0.0/16 -j MASQUERADE".format(virtualmachine.ip))

##
# Pushes the forwarding firewall rules onto the physical machine such that
# virtual machines are allowed to communicate. These rules might be needed in
# restrictive firewall environments (e.g. DROP policies).
# Note: these could be made more aggressive by specifying both ends. If needed,
# developer could change this function and from where it is called (it might be
# that specifying both ends would require rules between any two endpoints, not
# like we have now, one pair of rules per virtual cluster).
#
# @param virtualmachine VirtualMachine The virtual machine that needs to have
# the firewall rules pushed. 
#
def pushforwardfirewallrules(virtualmachine):
  sudo("iptables -I FORWARD 1 -s {0} -j ACCEPT".format(virtualmachine.ip))
  sudo("iptables -I FORWARD 1 -d {0} -j ACCEPT".format(virtualmachine.ip))

##
# Deletes the forwarding firewall rules created by 'pushforwardfirewallrules'.
#
# @param virtualmachine VirtualMachine The virtual machine for which to delete
# the forwarding firewall rules.
#
def deleteforwardfirewallrules(virtualmachine):
  sudo("iptables -D FORWARD -s {0} -j ACCEPT".format(virtualmachine.ip))
  sudo("iptables -D FORWARD -d {0} -j ACCEPT".format(virtualmachine.ip))

