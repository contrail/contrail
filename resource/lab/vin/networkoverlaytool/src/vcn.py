from cfg.config import Config
from cfg.msg import Msg

from helper.locker import Locker
from helper.encryptionalgorithmvalidator import isencryptionsupported

from namequantitypair import NameQuantityPair

from oss import supportedoss

from schedulernotenoughresourcesexception import SchedulerNotEnoughResourcesException
from schedulerloadexception import SchedulerLoadException

from virtualcluster import VirtualCluster
from virtualclusterexception import VirtualClusterException

import argparse
import os
import pickle
import sys

##
# Parser instance used for command line parsing
#
__parser = None

##
# Creates a new virtual cluster from the list of pairs provided.
#
# @param list list of os name quantity pairs.
# @param site String The name of the physical site on which the new virtual
# cluster is requested.
# @param encalg String The encryption algorithm to use.
# @returns VirtualCluster The VirtualCluster instance or None.
# @throws VirtualClusterException When a problem occurs with the creation of
# the virtual cluster.
# @throws SchedulerLoadException When there are problems parsing the XML
# configuration file.
# @throws SchedulerNotEnoughResourcesException When there are not enough free
# machines to satisfy this virtual cluster creation request.
#
def newvcn(list, site=None, encalg="aes"):

  # Create the virtual cluster.
  vc = VirtualCluster(list, site, encalg)

  # Save the virtual cluster to disk for further operations on it.
  vc.savevirtualclustertodisk()

  return vc


##
# Creates a new virtual cluster from the list of virtual machine instances
# provided.
#
# @param vmlist list of VirtualMachine instances.
# @param encalg String The encryption algorithm to use.
# @param bootvms Boolean Whether to automatically boot up the virtual machines.
# @returns VirtualCluster The VirtualCluster instance.
#
def vcnfromvmlist(vmlist, encalg="aes", bootvms=False):
  vc = VirtualCluster.fromvirtualmachinelist(vmlist, encalg, bootvms)

  # Save the virtual cluster to disk for further operations on it.
  vc.savevirtualclustertodisk()

  return vc

##
# Creates a new virtual cluster network overlay from a list of hostnames.
#
# @param hostlist list of String The list of hostnames that will host the
# virtual machines for the network overlay.
# @param encalg String The encryption algorithm to use for the secure network
# overlay.
#
def vcnfromhostlist(hostlist, encalg="aes"):
  vc = VirtualCluster.fromhostlist(hostlist, encalg)

  # Save the virtual cluster to disk for further operations on it.
  vc.savevirtualclustertodisk()

  return vc


##
# Deletes the provided virtual cluster.
#
# @param virtualcluster VirtualCluster The instance of the virtual cluster to
# delete.
# @param stopvms Boolean Whether to stop the virtual machines or not. Useful when
# virtual cluster is created from existing list of virtual machines.
#
def __delvcnbyreference(virtualcluster, stopvms=False, islibrary=False):
  
  #Just delete the virtual cluster.
  virtualcluster.delete(None, stopvms, islibrary)

##
# Delete a virtual cluster by its id.
#
# @param uniqueid String The unique id of the virtual cluster to delete.
# @param stopvms Boolean Whether to stop the virtual machines or not. Useful when
# virtual cluster is created from existing list of virtual machines.
# @throws VirtualClusterException When a problem occurs with the creation of
# the virtual cluster.
#
def delvcnbyid(uniqueid, stopvms=True, islibrary=True):

  vc = VirtualCluster.loadvirtualclusterfromdisk(uniqueid)

  if vc == None:
    raise VirtualClusterException(Msg.virtualclusterexistance.format(uniqueid))

  # Pass the deserialized instance of the virtual cluster to the deletion by
  # reference function.
  __delvcnbyreference(vc, stopvms, islibrary)

  vc.removevirtualclusterfromdisk()

##
# Handler for the creation of a virtual cluster from the command line arguments.
#
def create(args):

  if Locker.islocked():
    raise VirtualClusterException(Msg.virtualclusterlocked)
  
  Locker.lock()

  site = None
  if args.site != None and len(args.site) > 0:
    site = args.site[0]

  encalg = None
  if args.encalg != None and len(args.encalg) > 0:
    if isencryptionsupported(args.encalg[0]) == False:
      Locker.unlock()
      __parser.error(Msg.commandlineencryptionnotsupported)
    encalg = args.encalg[0]

  if args.spec == None:
    Locker.unlock()
    __parser.error(Msg.commandlineprovidespecshelp)

  if len(args.spec) == 0:
    Locker.unlock()
    __parser.error(Msg.virtualclusteremptycreation)

  # Operating system + quantity pairs need to be provided.
  if len(args.spec) % 3 != 0:
    Locker.unlock()
    __parser.error(Msg.virtualclusterprovidepairs)

  i = 0

  # Create the list of (operating system, quantity) pairs that will define the
  # new virtual cluster. 
  list = []
  while i < len(args.spec):

    # Make sure the requested operating system is supported.
    if args.spec[i] in supportedoss and supportedoss[args.spec[i]] == True:

      # Check whether a valid quantity has been provided for this OS.
      quantity = 0
      howmanyhaveinternet = 0
      try:
        quantity = int(args.spec[i+1])
        howmanyhaveinternet = int(args.spec[i+2])

        if quantity < howmanyhaveinternet:
          __parser.error(Msg.virtualclusterquantityinternetinvalid)

      except ValueError:
        Locker.unlock()
        __parser.error(Msg.virtualclusterquantityinvalid)

      list.append(NameQuantityPair(args.spec[i], quantity, howmanyhaveinternet))
    else:
      Locker.unlock()
      # The operating system is not supported. Warn the user and stop!
      __parser.error(Msg.virtualclusterunsupportedos.format(args.spec[i]))
    i += 3

  try:
    # Create the user's virtual cluster.
    vc = newvcn(list, site, encalg)
  except SchedulerNotEnoughResourcesException, (inst):
    print inst.parameter
    Locker.unlock()
    return
  except SchedulerLoadException, (inst):
    print inst.parameter
    Locker.unlock()
    return
  except VirtualClusterException, (inst):
    print inst.parameter
    Locker.unlock()
    return

  Locker.unlock()

  # This is a command line execution, provide the user with the id for cleanly
  # dismantling the virtual cluster when done using it.
  print Msg.virtualclustersuccesfulcreation.format(vc.getuniqueid())


##
# Handler for the deletion of a virtual cluster from the command line arguments.
#
def delete(args):
  if Locker.islocked():
    raise VirtualClusterException(Msg.virtualclusterlocked)
  
  Locker.lock()

  try:
    # Get the next parameter of the command that needs to provide the id of the
    # virtual cluster the user wants to dismantle.
    delvcnbyid(args.id[0], False)
  except VirtualClusterException, (inst):
    print inst.parameter
    Locker.unlock()
    return

  Locker.unlock()
  print Msg.virtualclustersuccesfuldeletion


##
# Handler for the merge of 2 virtual cluster2 from the command line arguments.
#
def merge(args):
  if Locker.islocked():
    raise VirtualClusterException(Msg.virtualclusterlocked)
  
  Locker.lock()

  encalg = None
  if args.encalg != None and len(args.encalg) > 0:
    if isencryptionsupported(args.encalg[0]) == False:
      Locker.unlock()
      __parser.error(Msg.commandlineencryptionnotsupported)
    encalg = args.encalg[0]

  if args.ids[0] == args.ids[1]:
    parser.error(Msg.virtualclustermergewithself)

  mergedvc = None

  try:
    mergedvc = VirtualCluster.merge(args.ids[0], args.ids[1], encalg)
    
    if mergedvc == None:
      Locker.unlock()
      return
  except VirtualClusterException, (inst):
    Locker.unlock()
    __parser.error(inst.parameter)

  # This is a command line execution, provide the user with the id for cleanly
  # dismantling the virtual cluster when done using it.
  print Msg.virtualclustersuccesfulcreation.format(mergedvc.getuniqueid())

  Locker.unlock()


# Used for easier testing of the library. If directly executed, create a bogus
# virtual cluster and delete it to check if the process works correctly.
if __name__ == "__main__":

  # Create the argument parser for the command line arguments.
  __parser = argparse.ArgumentParser(description=Msg.commandlinedescription)

  subparsers = __parser.add_subparsers()

  parser_create = subparsers.add_parser('create')
  parser_create.add_argument('-spec', nargs='*', help=Msg.commandlinecreatehelp)
  parser_create.add_argument('-site', nargs=1, help=Msg.commandlinesite)
  parser_create.add_argument('-encalg', nargs=1, help=Msg.commandlineencahelp)
  parser_create.set_defaults(func=create)

  parser_delete = subparsers.add_parser('delete')
  parser_delete.add_argument('-id', nargs=1, help=Msg.commandlinedeletehelp)
  parser_delete.set_defaults(func=delete)

  parser_merge = subparsers.add_parser('merge')
  parser_merge.add_argument('-ids', nargs=2, help=Msg.commandlinemergehelp)
  parser_merge.add_argument('-encalg', nargs=1, help=Msg.commandlineencahelp)
  parser_merge.set_defaults(func=merge)

  args = __parser.parse_args()
  args.func(args)

