##
# The representative for a physical site (physical cluster). Each site is
# composed of multiple physical machines that each contain virtual machines 
# (e.g. DAS3, DAS4, ...)
#
class PhysicalSite:

  ##
  # The name of the site.
  #
  name = ""

  ##
  # The collection of physical machines that compose this site
  #
  physicalmachines = None

  ##
  # The start ip of the allocated range for this cluster. Ip in integer format.
  #
  ipstart = 0

  ##
  # The end ip of the allocated range for this cluster. Ip in integer format.
  #
  ipend = 0

  def __init__(self, name, physicalmachines, ips, ipe):
    self.name = name
    self.physicalmachines = physicalmachines
    self.ipstart = ips
    self.ipend = ipe

