##
# Class containing several configuration constants.
#
class Config:

  ##
  # Resources directory path.
  #
  resourcespath = "res/"

  ##
  # Path to the xml that defines all the machines that are available to this
  # virtual cluster configurator.
  #
  machinesxmlpath = "{0}machines.xml".format(resourcespath)

  machinesxmlpathlibrary = "{0}machines_library.xml".format(resourcespath)

  ##
  # The path to the file where the preshared keys will be stored on the remote
  # machines before being used with racoon. The file name ends in the virtual
  # cluster's unique id.
  #
  racoonpresharedkeypath = "/etc/racoon/psk_{0}.txt"

  ##
  # Tje path to the file where the pid of the newly spawned racoon process. This
  # will ne helpful on cleanly dismantling a virtual cluster.
  #
  racoonpidfilepath = "/etc/racoon/pid.txt"

  ##
  # The length of the symmetric keys that racoon will use.
  #
  symmetrickeylength = 16

  ##
  # A debug switch. For extra information on run set this flag to 1.
  #
  DEBUG = 0

  ##
  # The file path used for the serialization of user access.
  #
  lockfile = ".lock"

