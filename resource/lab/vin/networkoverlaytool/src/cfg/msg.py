##
# Constant messages class.
#
class Msg:

  ##
  # Message that gets shown upon deletion of non-existent virtual cluster.
  #
  virtualclusterexistance = "The virtual cluster with id {0} doesn't exist!"

  ##
  # Message for when no parameters are provided to the virtual cluster creation
  # command.
  #
  virtualclusteremptycreation = "Cannot create an empty virtual cluster. Please\
 provide some \n(Operating System, Quantity, Quantity of Internet connected vms\
) pairs"
  
  ##
  # Message for when parameters provided for virtual cluster creation are not
  # operating system quantity pairs.
  #
  virtualclusterprovidepairs = "Please provide (Operating System, Quantity, \
Quantity of Internet connected vms) pairs"

  ##
  # Message for when quantity cannot be parsed.
  #
  virtualclusterquantityinvalid = "Please provide valid integer values for the \
quantities"

  ##
  # Message for when a requested operating system is not supported. 
  #
  virtualclusterunsupportedos = "We currently don't support the {0} operating \
system"

  ##
  # Message for when a cluster was succesfully created.
  #
  virtualclustersuccesfulcreation = "\
The virtual cluster has been created with id: '{0}'.\n\
Please use this id for the safe deletion of this virtual cluster when you are\n\
finished with it!"

  ##
  # Message for when a cluster was succesfully deleted.
  #
  virtualclustersuccesfuldeletion = "The virtual cluster was succesfully \
deleted!\n"

  ##
  # Message for when the user tries to merge a cluster with itself.
  #
  virtualclustermergewithself = "Cannot merge a virtual cluster with itself"

  ##
  # The description shown to the user on the command line.
  #
  commandlinedescription = 'Virtual Cluster management tool. \
Use \"python vcn.py create -h\" for creation options, \
\"python vcn.py merge -h\" for merging options and \
\"python vcn.py delete -h\" for deletion options.'

  ##
  # The help message shown to the command line user for the virtual cluster
  # create operation.
  #  
  commandlinecreatehelp = 'Creates a new\
virtual cluster with the help of the arguments provided. Requires a list of\
(Operating System, Quantity, Internet Connected) pairs \
(e.g \'Linux 2 1 Windows 1 0\')'

  ##
  # The help message shown to the command line user for the virtual cluster
  # delete operation.
  #
  commandlinedeletehelp = 'Deletes an \
instance of an existing virtual cluster. Requires a valid virtual cluster id\
 (e.g. \'1b8fb5db82a240249766d88e6a75421b\'). User needs to be owner of that\
virtual cluster.'

  ##
  # The help message shown to the command line user for the virtual cluster
  # merge operation.
  #
  commandlinemergehelp = 'Merges two\
existing virtual clusters. Requires two valid virtual cluster id\'s (e.g. \'\
1b8fb5db82a240249766d88e6a75421b cd32e92d6a964e6ab398e53d1f07a278\'). User\
 needs to be the owner of both virtual clusters. '

  ##
  # The message for when configuration issues occur. The psk file could not be
  # moved to the appropriate location.
  #
  virtualclustercouldntmovepskfile = "Could not move the IKE preshared key \
file to the appropriate location on remote site {0}"

  ##
  # The message for when configuration issues occur. The psk ownership could not
  # be changed.
  #
  virtualclusterpskownership = "Could not change the owner of the preshared \
key file on remote site {0}"

  ##
  # The message for when configuration issues occur. The psk file permissions
  # could not be changed.
  #
  virtualclusterpskpermission = "Could not change the file permission of the \
preshared key file on remote site {0}"

  ##
  # The message for when configuration issues occur. The racoon instance could
  # not be started.
  #
  virtualclustercantstartracoon = "Could not start a new racoon instance \
remote site {0}"

  ##
  # The message for when configuration issues occur. The racoon configuration
  # file could not be removed.
  #
  virtualclusterracoonconfremove = "Could not remove the temp racoon \
configuration file on remote site {0}"

  ##
  # The message for when configuration issues occur. The racoon pid could not
  # be retrieved.
  #
  virtualclusterracoonpidretrieval = "Could not retrieve the racoon instance\
process id on remote site {0}"

  ##
  # The message for when configuration issues occur. The racoon process could
  # not be killed.
  #
  virtualclusteracoonkill = "Could not kill the racoon instance process.\n"

  ##
  # The message for when configuration issues occur. The racoon psk file coult
  # not be removed.
  #
  virtualclusterpskremove = "Could not remove the racoon preshared key file.\n"

  ##
  # The message for when configuration issues occur. The IPSec script could not
  # be executed on the remote site.
  #
  virtualclusteripsecexecscript = "Could not execute IPSec script on remote \
site {0}"

  ##
  # The message for when configuration issues occur. The IPSec script could not
  # be removed on the remote site.
  #
  virtualclusteripsecremovescript = "Could not cleanup IPSec script on remote \
site {0}"

  ##
  # The message for when configuration issues occur. The libvirt domain could
  # not be created.
  #
  virtualclusterlibvirtcreatedomain = "Could not create libvirt domain on \
remote site {0}"

  ##
  # The message for when configuration issues occur. The libvirt configuration
  # files could not be cleaned up.
  #
  virtualclusterlibvirtcleanup = "Could not cleanup libvirt temps on remote \
site {0}"

  ##
  # The message for when configuration issues occur. The libvirt domain could
  # not be destroyed.
  #
  virtualclusterlibvirtdestroydomain = "Could not execute virsh destroy \
command.\n"

  ##
  # Message for when the configuration XML could not be found.
  #
  schedulerconfigfileexistence = "Could not find configuration XML path! \
Please contact your administrator!"

  ##
  # Message for when the config file could not be parsed.
  #
  schedulerconfigfileparse = "Severe error. Configuration file could not be \
parsed!"

  ##
  # A please check something message.
  #
  pleasecheck = "Please check {0}"

  ##
  # Message shown when XML config file could not be loaded.
  #
  schedulerloadxml = "Could not load XML configuration file! \
Please contact your administrator for support!"

  ##
  # Message for when there are not enough free machines to satisfy a request.
  #
  schedulernotenoughfreemachines = "Not enough free machines to \
satisfy your request. Readjust your request or wait until resources are \
available"

  ##
  # Message shown when some problems were encountered and the execution needs to
  # be reverted.
  #
  virtualclusterproblemsencountered = "Some problems were encountered while \
creating the virtual cluster. Reverted execution!"

  ##
  # Message for when a user provides a number of internet connected virtual
  # machines greater than the requested number of virtual machines.
  #
  virtualclusterquantityinternetinvalid = "Cannot have more virtual machines \
with an internet connection than available machines!"

  ##
  # Message shown when a user tries to merge 2 clusters from the same site.
  #
  virtualclustermergesamesite = "Cannot merge two cluster coming from the same \
site! Invalid operation!"

  ##
  # Message shown when an error occured while trying to merge the provided
  # virtual clusters.
  #
  virtualclustermergegenericerror = "Cannot merge the two clusters, problem \
occured. Please check the input id's!"

  ##
  # Message shown when user tries to merge two virtual clusters that share the
  # same physical node.
  #
  virtualclustermergerepeatednode = "Cannot merge two clusters that share the \
same physical node"

  ##
  # Message for when there are no more free ports available for the ike
  # instances.
  #
  virtualclustermergenomoreports = "No more free ports available! Merge failed!"

  ##
  # Message shown when a user attemps to run the utility but the utility is
  # already running.
  #
  virtualclusterlocked = "The utility is under use, please try again when it is\
not used!"

  ##
  # Message for the help of the virtual cluster creation (site help).
  #
  commandlinesite = "Physical site on which to host the virtual cluster!"

  ##
  # Message for the help with the encryption algorithm.
  #
  commandlineencahelp = "Encryption algorithm to use for this operation!"

  ##
  # Helper message for provision of specs.
  #
  commandlineprovidespecshelp = "Please provide the specs!"

  ##
  # Message to alert the user that the encryption he chose is not supported.
  #
  commandlineencryptionnotsupported = "Specified encryption not supported!"


  ##
  # Message to alert the user that all the allocated IP addresses have been 
  # used.
  #
  virtualclusternomoreips = "There are no more free IPs allocated!"

  virtualclusternomorebridgeids = "The are no more free bridge IDs!"

