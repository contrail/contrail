##
# A representative class for a subinterface on a physical machine. This class
# hold the subinterface's details and tells virtual machines to which
# subinterface they are attached to.
#
class SubInterface:

  ##
  # The name of the physical interface to which this subinterface os attached to
  # (e.g. eth0).
  # 
  attachedto = ""

  ##
  # The number associated with the subinterface (e.g. 1 ... in combination
  # with the attachedto member results the appropriate names like eth0:1.
  #
  number = ""

  ##
  # The ip address of this subinterface.
  #
  ip = ""

  ##
  # Returns the official name of the subinterface. It is obtained by
  # concatenation between the attachedto interface and the number (e.g. eth0:1).
  #
  def getname(self):
    return "{0}:{1}".format(self.attachedto, self.number)

  ##
  # The constructor of the SubInterface class.
  #
  # @param attachedto String The physical interface's name of the interface
  # to which this one is attached to.
  # @param number String The number attached to the subinterface's name. (e.g 0,
  # or 1, such to result in names like eth0:1).
  # @param ip String The ip address of the subinterface
  # 
  def __init__(self, attachedto, number, ip):
    self.attachedto = attachedto
    self.number = number
    self.ip = ip


