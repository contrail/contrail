class PhysicalMachine:
  """A physical machine class."""

  ##
  # A unique id associated with this physical machine.
  #
  uniqueid = 0

  ##
  # The ip address of this physical machine.
  #
  ip = ""

  ##
  # The hostname of this physical machine. 
  #
  name = ""

  ##
  # The username that is used to log into this physical machine.
  #
  __username = ""

  ##
  # The password that is used to log into this physical machine.
  #
  __password = ""

  ##
  # A list of VirtualMachine object instances that represent the virtual
  # machines of this physical machine.
  #
  __virtualmachines = []

  ##
  # Each machine comes with an available range of ports that will be used for
  # the IKE tool. This is the minimum value of the range.
  #
  portRangeMin = 500
  
  ##
  # As portRangeMin, but this is the maximum value of the range.
  #
  portRangeMax = 530

  ##
  # Each machine comes with an available range of IPs that will be used for
  # the virtualmachines. This is the minimum value of the range.
  #
  ipRangeMin = 0

  ##
  # As ipRangeMin, but this is the maximum value of the range.
  #
  ipRangeMax = 0

  ##
  # Physical machine constructor.
  #
  # @param uniqueid int A unique id of this physical machine.
  # @param ip String The ip of the physical machine.
  # @param name String The hostname associated with this physical machine. 
  # @param username String The username used to login to this physical machine.
  # @param password String The password user to login to this physical machine.
  #
  def __init__(
      self, uniqueid=None, ip=None, name=None, username=None, password=None):
    self.uniqueid = uniqueid
    self.ip = ip
    self.name = name
    self.__username = username
    self.__password = password
    self.__virtualmachines = []

  ##
  # Username member setter.
  #
  # @param username String The new username.
  #
  def setusername(self, username):
    self.__username = username

  ##
  # Password member setter.
  #
  # @param password String The new password.
  #
  def setpassword(self, password):
    self.__password = password

  ##
  # Username member getter.
  #
  def getusername(self):
    return self.__username

  ##
  # Password member getter.
  #
  def getpassword(self):
    return self.__password

  ##
  # Adding a virtual machine to the virtual machine collection of this physical
  # machine.
  #
  # @param virtualmachine VirtualMachine The virtual machine to add.
  #
  def addvirtualmachine(self, virtualmachine):
    self.__virtualmachines.append(virtualmachine)

  ##
  # Removing a virtual machine from the virtual machine collection of this
  # physical machine.
  #
  # @param virtualmachine VirtualMachine The virtual machine to remove.
  #
  def removevirtualmachine(self, virtualmachine):
    self.__virtualmachines.remove(virtualmachine)

  ##
  # Pops one of the virtual machines of this physical machine. Useful when
  # the scheduler needs to request a virtual machine from this physical machine.
  # User needs to call the hasvirtualmachines to check if any virtual machines
  # are available first!
  #
  def popvirtualmachine(self):
    return self.__virtualmachines.pop(0)
    

  ##
  # Checks if this physical machine has any virtual machines available.
  #
  def hasvirtualmachines(self):
    if len(self.__virtualmachines) == 0:
      return False
    else:
      return True

  ##
  # Returns the operating system of the virtual machines of this physical
  # machine.
  # User needs to call hasvirtualmachines first to check if this physical
  # machine has any virtual machine available!
  #
  # @returns String The string representation of the OS.
  #
  def getvirtualmachineoperatingsystem(self):
    return self.__virtualmachines[0].operatingsystem

  ##
  # This function needs to be called after initializing the physical machine.
  # It's purpose it to initialize the underlying virtual machines with the help
  # of some values contained by the parent physical machine.
  #
  def completesetup(self):
    for virtualmachine in self.__virtualmachines:
      virtualmachine.setparentuser(self.__username)
      virtualmachine.setparentpass(self.__password)
      virtualmachine.parentip = self.ip
      virtualmachine.host = self.name
      virtualmachine.portRangeMin = self.portRangeMin
      virtualmachine.portRangeMax = self.portRangeMax
      virtualmachine.ipRangeMin = self.ipRangeMin
      virtualmachine.ipRangeMax = self.ipRangeMax

  ##
  # Gets a string representation of the physical machine.
  #
  def tostring(self):
    vmtostring = ""
    for virtualmachine in self.__virtualmachines:
      vmtostring += virtualmachine.tostring()

    return "PHYSICAL MACHINE\nuniqueid={0}\nip={1}\nname={2}\nusername={3}\
            \npassword={4}\n{5}\n".format(self.uniqueid, self.ip, self.name,
                                        self.__username, self.__password,
                                        vmtostring)



