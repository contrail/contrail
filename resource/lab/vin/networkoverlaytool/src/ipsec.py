from cfg.msg import Msg 

from fabric.api import run
from fabric.operations import sudo
from fabric.operations import put
from fabric.context_managers import settings

from helper.ipsectunnelmodeconfigparser import getipsecconfigfooter
from helper.ipsectunnelmodeconfigparser import getipsecconfigheader
from helper.ipsectunnelmodeconfigparser import ipsecconfigscriptfromvirtualmachinepair

from virtualmachine import VirtualMachine
from virtualclusterexception import VirtualClusterException

import os

##
# The path where the temporary security associations script will be stored.
#
temporaryscriptpath = "associationsscript.sh"

##
# Pushes the security policy association to virtualmachine1 in order to enable
# it to securely talk to virtualmachine2.
#
# @param virtualmachine1 VirtualMachine The source virtual machine. This is the
# virtual machine that settings are pushed onto at this point.
# @param virtualmachinelist list The list of virtual machines that will also
# participate to the virtual cluster. This list will now be connected to the
# virtualmachine1.
# @param islocal Boolean Whether the result is local or not. If the result is
# local, the tunnel is done using the private ip address range (10.0.0.0/16),
# but if the result is not local, the public IP addresses need to be used.
# @throws VirtualClusterException if problems occur while tryin to push the
# associations.
#
def pushassociations(virtualmachine1, virtualmachinelist, islocal=True):

  with settings(warn_only=True):

    # Get the appropriate script for adding IPSec associations between the 
    # source virtual machine and the other virtual machines using the templated
    # abstraction function. First step, consider the first part of the script, 
    # the header.
    associationsscript = getipsecconfigheader()
    deleteassocscript = ""

    # Next, add the configurations for each of the endpoints.
    for virtualmachine2 in virtualmachinelist:
      if virtualmachine2 != virtualmachine1:
        associationsscript += ipsecconfigscriptfromvirtualmachinepair(
                           virtualmachine1, virtualmachine2, "add", not islocal)
        deleteassocscript += ipsecconfigscriptfromvirtualmachinepair(
                        virtualmachine1, virtualmachine2, "delete", not islocal)

    # Finally, close the script with it's footer.
    associationsscript += getipsecconfigfooter()

    # Put the content of that script on disk so as to transfer it to the remote
    # physical machine in order to execute it there.
    FILE = open(temporaryscriptpath, "w")
    FILE.write(associationsscript)
    FILE.close()

    # First we will upload the script file to the remote machine (physical).
    put(temporaryscriptpath, "~/{0}".format(temporaryscriptpath))

    # All the prerequisite steps have been made, it is time to execute the 
    # script on the remote host so as to add the appropriate associations on 
    # that physical machine. 
    out = sudo("sh {0}".format(temporaryscriptpath))

    if out.failed:
      # Cleanup.
      os.remove(temporaryscriptpath)
      run("rm ~/{0}".format(temporaryscriptpath))
      raise VirtualClusterException(
             Msg.virtualclusteripsecexecscript.format(virtualmachine1.parentip))

    # Now that we already used the uploaded script file to add the security
    # associations, we can remove it.
    out = run("rm ~/{0}".format(temporaryscriptpath))

    if out.failed:
      # Cleanup.
      os.remove(temporaryscriptpath)
      raise VirtualClusterException(
           Msg.virtualclusteripsecremovescript.format(virtualmachine1.parentip))

    # Time to cleanup the local hard disk for any auxiliary resources that have
    # been used for adding the security associations.
    os.remove(temporaryscriptpath)

    virtualmachine1.deleteipsecassocscript += "\n{0}\n".format(deleteassocscript)

def deletebulkassociations(virtualmachine):
  with settings(warn_only=True):
    deleteipsec = getipsecconfigheader() + virtualmachine.deleteipsecassocscript + getipsecconfigfooter()
    out = sudo(deleteipsec)
    if out.failed:
      print Msg.virtualclusteripsecexecscript.format(virtualmachine.parentip)

##
# Deletes the security policy association from virtualmachine1 in order to
# disable its secure communication with virtualmachine2. This is used upon
# dismantling a virtual cluster.
#
# @param virtualmachine1 VirtualMachine The source virtual machine. This is the
# virtual machine that settings are pushed onto at this point.
# @param virtualmachinelist list The list of virtual machines that will also
# participate to the virtual cluster. This list will now be disconnected from
# the virtualmachine1.
#
def deleteassociations(virtualmachine1, virtualmachinelist):

  with settings(warn_only=True):
    # Get the appropriate script for removing IPSec associations between the 
    # source virtual machine and the other virtual machines using the templated
    # abstraction function. First step, consider the first part of the script, 
    # the header.
    associationsscript = getipsecconfigheader()

    # Next, add the configurations for each of the endpoints.
    for virtualmachine2 in virtualmachinelist:
      if virtualmachine2 != virtualmachine1:
        associationsscript += ipsecconfigscriptfromvirtualmachinepair(
                                     virtualmachine1, virtualmachine2, "delete")

    # Finally, close the script with it's footer.
    associationsscript += getipsecconfigfooter()

    # Put the content of that script on disk so as to transfer it to the remote
    # physical machine in order to execute it there.
    FILE = open(temporaryscriptpath, "w")
    FILE.write(associationsscript)
    FILE.close()

    # First we will upload the script file to the remote machine (physical).
    out = put(temporaryscriptpath, "~/{0}".format(temporaryscriptpath))

    # All the prerequisite steps have been made, it is time to execute the 
    # script on the remote host so as to remove the appropriate associations on 
    # that physical machine. 
    out = sudo("sh {0}".format(temporaryscriptpath))

    if out.failed:
      print Msg.virtualclusteripsecexecscript.format(virtualmachine.parentip)

    # Now that we already used the uploaded script file to remove the security
    # associations, we can remove it.
    out = run("rm ~/{0}".format(temporaryscriptpath))

    if out.failed:
      print Msg.virtualclusteripsecremovescript.format(virtualmachine.parentip)

    # Time to cleanup the local hard disk for any auxiliary resources that have
    # been used for removing the security associations.
    os.remove(temporaryscriptpath)


