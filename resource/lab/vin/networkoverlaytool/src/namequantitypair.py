class NameQuantityPair:
  """A class that stands for a pair of an OS name and a number/quantity"""
  name = "Linux"
  quantity = 0
  quantityofinternetconnected = 0

  def __init__(self, name, quantity, quantityofinternetconnected = 0):
    self.name = name
    self.quantity = quantity
    self.quantityofinternetconnected = quantityofinternetconnected


