from cfg.config import Config
from cfg.msg import Msg

from helper.machinesparser import MachinesParser

from schedulerloadexception import SchedulerLoadException
from physicalsite import PhysicalSite

from xml.sax import saxutils
from xml.sax import make_parser

from xml.sax.handler import feature_namespaces

from virtualmachine import VirtualMachine

import os

##
# The scheduler class is responsible with allocating new machines according to
# some user requests. The mechanism works like this: when a user requests some
# machines in the form of a list of OS quantity pairs, the scheduler class gets
# from the configuration XML what machines are available and allocates, if
# possible, machines from that list to satisfy the user needs.
# For the simplicity of the implementation, we will assume for now that all of
# the virtual machines on a physical machine are of the same type. This way we
# will be able to do the scheduling much easier with no heuristics involved.
#
class Scheduler:
  """ The scheduler class."""

  ##
  # The list of available sites. This list will be used to schedule available
  # machines according to client requests for Virtual Clusters.
  #
  __sites = []

  ##
  # Before anything, we need to load the available machines from the
  # configuration file.
  #
  # @throws SchedulerLoadException If the Scheduler cannot load the XML
  # configuration file.
  #
  @staticmethod
  def __loadmachines(library=True):
    # Create an xml parser
    parser = make_parser()

    # Tell the parser we are not interested in the XML namespaces, but only in
    # the XML content.
    parser.setFeature(feature_namespaces, 0)

    # Create the handler.
    dh = MachinesParser()

    # Tell the parser to use our handler.
    parser.setContentHandler(dh)

    if os.path.exists(Config.machinesxmlpath) == False:
      raise SchedulerLoadException(Msg.schedulerconfigfileexistence)

    if library == False:
      # Parse the input.
      parser.parse(Config.machinesxmlpath)
    else:
      parser.parse(Config.machinesxmlpathlibrary)

    # Get the parse results.
    Scheduler.__sites = dh.sites

    if Config.DEBUG == 1:
      for site in Scheduler.__sites:
        print "SITE:\n"
        for physicalmachine in site:
          print "{0}\n".format(physicalmachine.tostring())


  ##
  # Gets a list of virtual machines from the provided hosts.
  #
  # @param hostlist list of String The list of hostnames from which to get
  # the virtual machines.
  #
  @staticmethod
  def getmachinesfromhosts(hostlist):
    if len(Scheduler.__sites) == 0:
      Scheduler.__loadmachines(True)

    if len(Scheduler.__sites) == 0:
      print Msg.schedulerconfigfileparse
      print Msg.pleasecheck.format(Config.machinesxmlpath)
      raise SchedulerLoadException(Msg.schedulerloadxml)

    resultvirtualmachines = []
    resultphysicalmachines = []
    found = False

    for host in hostlist:
      found = False

      # Try to satisfy the request
      for site in Scheduler.__sites:
        # We now try to satisfy this name quantity pair request.
        for physicalmachine in site.physicalmachines:
          if host == physicalmachine.ip or host == physicalmachine.name:
            # We check if this node can host another virtualmachine
            if physicalmachine.hasvirtualmachines():
              resultphysicalmachines.append(physicalmachine)
              found = True
            
      if found == False:
        break

    if found == False:
      return None
    
    ips = {}

    for physicalmachine in resultphysicalmachines:
      virtualmachine = physicalmachine.popvirtualmachine()
      resultvirtualmachines.append(virtualmachine)

    return resultvirtualmachines


  ##
  # Here we need to get the list of machines that will participate in the
  # Virtual Cluster.
  #
  # @pairs list of Operating System Quantity pairs
  # @returns list of VirtualMachine instances that contain the details of the
  # Scheduled virtual machines to start in order to create the virtual cluster.
  # Returns None if the request cannot be satisfied.
  # @throws SchedulerLoadException If the Scheduler cannot load the XML
  # configuration file.
  #
  @staticmethod
  def getmachines(pairs, sitename=None):

    if len(Scheduler.__sites) == 0:
      Scheduler.__loadmachines(False)

    if len(Scheduler.__sites) == 0:
      print Msg.schedulerconfigfileparse
      print Msg.pleasecheck.format(Config.machinesxmlpath)
      raise SchedulerLoadException(Msg.schedulerloadxml)

    resultvirtualmachines = []

    # Try to satisfy the request 
    for site in Scheduler.__sites:
      if sitename == None or site.name == sitename:

        resultphysicalmachines = []
        resultvirtualmachines = []

        # We take each of the name quantity pairs and try to find that amount of
        # requested virtual machines in the available loaded ones.
        for pair in pairs:
          quantity = pair.quantity

          # We now try to satisfy this name quantity pair request.
          for physicalmachine in site.physicalmachines:
            # If a virtual machine is available and it has the searched OS.
            if physicalmachine.hasvirtualmachines() and physicalmachine.getvirtualmachineoperatingsystem() == pair.name and pair.name != "":

              # Cool, we found a candidate physical machine that contains a good
              # virtual machine for the request. We mark it and go on.
              resultphysicalmachines.append(physicalmachine)

              # We note that we need one less.
              quantity = quantity - 1

              if quantity == 0:
                break

          # Can we satisfy the client request for this pair?
          if quantity != 0:
            # No, we cannot satisfy the request on this site. Try the next.
            # Reset the collection and move to the next site.
            resultphysicalmachines = []
            break        

        if len(resultphysicalmachines) > 0:
          # We managed to satisfy the request on this site.

          for pair in pairs:
            internetconnected = pair.quantityofinternetconnected
            
            for physicalmachine in resultphysicalmachines:
              if pair.name == physicalmachine.getvirtualmachineoperatingsystem():
                virtualmachine = physicalmachine.popvirtualmachine()
                if internetconnected > 0:
                  virtualmachine.internetconnected = True
                  internetconnected = internetconnected - 1

                resultvirtualmachines.append(virtualmachine)    

          if Config.DEBUG == 1:
            for virtualmachine in resultvirtualmachines:
              print "{0}\n".format(virtualmachine.tostring())

          return resultvirtualmachines

    # If we get here it means no site was able to satisfy the request.
    return None


