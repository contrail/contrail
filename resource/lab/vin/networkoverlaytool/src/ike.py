from cfg.config import Config
from cfg.msg import Msg

from fabric.api import run
from fabric.operations import sudo
from fabric.operations import put
from fabric.context_managers import settings

from helper.ikeconfigparser import getracoonheader
from helper.ikeconfigparser import getracoonconf
from helper.ikeconfigparser import getpskconf

from virtualmachine import VirtualMachine
from virtualclusterexception import VirtualClusterException

import os

##
# Temporary racoon conf path.
#
__temporaryracoonconfpath = "tempracoonconf.conf"

##
# Temporary racoon preshared key content.
#
__temporaryracoonpskpath = "temppsk2.txt"

##
# Creates the configuration contents for the ike setup of the remote machine.
#
# @param virtualmachine VirtualMachine The remote site where the configurations
# need to be pushed.
# @param virtualmachinelist list The list of the virtual machines that take part
# in the virtual cluster to be created with these configs.
# @param symmetricsecuritykey String The security key that will be used by
# racoon.
# @param virtualclusterid String The unique id associated with the virtual
# cluster that needs these configurations.
# @param encalg String The encryption algorithm to use. None means the default
# will be used (aes).
# @throws VirtualClusterException When a problem occurs with the creation of
# the virtual cluster.
# @param ports dictionary(virtualmachine, int) which associates a port number to
# use to each of the virtual machines. If none is provided, the private
# subinterface is used with the default port numbers.
# @throws VirtualClusterException if problems occur while tryin to push the
# configs.
#
def pushikeconfigs(
    virtualmachine, virtualmachinelist, symmetricsecuritykey, virtualclusterid,
    encalg=None, ports = None):

  with settings(warn_only=True):
    racoonpresharedkeypath = Config.racoonpresharedkeypath.format(
                                                                virtualclusterid)

    usepublicip = True
    if ports == None:
      usepublicip = False

    # Get the racoon configuration header.
    racoonconf = getracoonheader(racoonpresharedkeypath,
                   Config.racoonpidfilepath, usepublicip, virtualmachine, ports)

    pskconf = ""

    # Add the configurations and the psk's.
    for vm in virtualmachinelist:
      if vm != virtualmachine:
        racoonconf += getracoonconf(virtualmachine, vm, encalg, ports)
        pskconf += getpskconf(virtualmachine, vm, symmetricsecuritykey, ports)

    # Put the content of the racoon configurations in the appropriate file so 
    # that we can transfer it to the remote site.
    FILE = open(__temporaryracoonconfpath, "w")
    FILE.write(racoonconf)
    FILE.close()

    # Put the content of the racoon psks in the appropriate file so that we can
    # transfer it to the remote site.
    FILE = open(__temporaryracoonpskpath, "w")
    FILE.write(pskconf)
    FILE.close()

    # Transfer the configuration files to the remote machine.
    put(__temporaryracoonconfpath, "~/{0}".format(__temporaryracoonconfpath))
    put(__temporaryracoonpskpath, "~/{0}".format(__temporaryracoonpskpath))

    # Place the racoon preshared key file at it's place on the remote machine. 
    out = sudo("mv ~/{0} {1}".format(
                              __temporaryracoonpskpath, racoonpresharedkeypath))

    if out.failed:
      __cleanup(__temporaryracoonconfpath, __temporaryracoonpskpath,
                                           Msg.virtualclustercouldntmovepskfile)

    # Grant the appropriate permissions to the remote preshared key file and
    # appropriately change it's ownership in order to allow racoon execution on
    # it.
    out = sudo("chown root {0}".format(racoonpresharedkeypath))

    if out.failed:
      __cleanup(__temporaryracoonconfpath, __temporaryracoonpskpath,
                                                 Msg.virtualclusterpskownership)


    # Change the file permissions so that racoon accepts it.
    out = sudo("chmod 600 {0}".format(racoonpresharedkeypath))

    if out.failed:
      __cleanup(__temporaryracoonconfpath, __temporaryracoonpskpath,
                                                Msg.virtualclusterpskpermission)


    # Execute racoon using the transferred configuration files.
    out = sudo("racoon -f ~/{0}".format(__temporaryracoonconfpath))

    if out.failed:
      __cleanup(__temporaryracoonconfpath, __temporaryracoonpskpath,
                                              Msg.virtualclustercantstartracoon)


    # Cleanup the remote temporary files
    out = run("rm ~/{0}".format(__temporaryracoonconfpath))

    if out.failed:
     __cleanup(__temporaryracoonconfpath, __temporaryracoonpskpath,
                                            Msg.virtualclusterracoonconfremove)


    # Retrieve the racoon instance process id.
    pidstring = run("cat {0}".format(Config.racoonpidfilepath))

    if pidstring.failed:
      __cleanup(__temporaryracoonconfpath, __temporaryracoonpskpath,
                                           Msg.virtualclusterracoonpidretrieval)

    virtualmachine.racooninstancepid = int(pidstring)

    # Cleanup the temporary files.
    os.remove(__temporaryracoonpskpath)
    os.remove(__temporaryracoonconfpath)

    virtualmachine.deleteikescript += "kill {0};rm {1};".format(
                       virtualmachine.racooninstancepid, racoonpresharedkeypath)

##
# Cleans up in case of error while configuring.
#
# @param tempracoonconfpath String The path for the temporary racoon 
# configuration file.
# @param  tempracoonpskpath String The path for the temporary racoon preshared
# key file.
# @param message String The message to pass to the VirtualClusterException
# thrown by the function.
# @throws VirtualClusterException With the provided message.
#
def __cleanup(tempracoonconfpath, tempracoonpskpath, message):
  with settings(warn_only=True):
    # Cleanup the temporary files.
    run("rm ~/{0}".format(tempracoonconfpath))
    os.remove(tempracoonpskpath)
    os.remove(tempracoonconfpath)

  raise VirtualClusterException(message.format(virtualmachine.parentip))

##
# Executes a bulk cleanup command from the kept log within the virtualmachine.
#
# @param virtualmachine VirtualMachine The virtual machine for which to do the
# operation.
#
def deletebulkike(virtualmachine):
  with settings(warn_only=True):
    out = sudo(virtualmachine.deleteikescript)
    if out.failed:
      print Msg.virtualclusteracoonkill


##
# Deletes the configuration contents for the ike setup of the remote machine.
#
# @param virtualmachine VirtualMachine The remote site where the configurations
# need to be pushed.
# @param racooninstancepid String The process id of the racoon instance that
# needs to be shutdown.
# @param virtualclusterid String The unique id associated with the virtual
# cluster that needs these configurations.
#
def deleteikeconfigs(virtualmachine, virtualclusterid):
  with settings(warn_only=True):
    # Due to the fact that we enable the IKE, racoon, by starting a new racoon
    # instance via 'racoon -f path_to_custom_racoon_conf', we will only need the
    # process id of that racoon instance in order to kill it!

    racoonpresharedkeypath = Config.racoonpresharedkeypath.format(
                                                               virtualclusterid)

    # See first if it was initialized.
    if virtualmachine.racooninstancepid > 0:
      out = sudo("kill {0}".format(virtualmachine.racooninstancepid))

      if out.failed:
        print Msg.virtualclusteracoonkill

      # We can now cleanup the racoon preshared key file
      out = sudo("rm {0}".format(racoonpresharedkeypath))
      
      if out.failed:
        print Msg.virtualclusterpskremove



