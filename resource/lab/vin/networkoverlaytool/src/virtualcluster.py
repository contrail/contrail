from cfg.config import Config
from cfg.msg import Msg

from customactions import deleteforwardfirewallrules
from customactions import deletesubinterfaces
from customactions import grantinternetaccess
from customactions import pushforwardfirewallrules
from customactions import pushsubinterfaces
from customactions import revertinternetaccess
from customactions import pushvirbr
from customactions import deletevirbr

from fabric.api import run
from fabric.context_managers import settings
from fabric.operations import sudo

from helper.ipsectunnelmodeconfigparser import getipsecconfigfooter
from helper.ipsectunnelmodeconfigparser import getipsecconfigheader
from helper.machinesupdater import MachinesUpdater

from ike import deletebulkike
from ike import deleteikeconfigs
from ike import pushikeconfigs

from ipsec import deleteassociations
from ipsec import deletebulkassociations
from ipsec import pushassociations

from libvirt import startvm
from libvirt import stopvm

from scheduler import Scheduler
from schedulerloadexception import SchedulerLoadException
from schedulernotenoughresourcesexception import SchedulerNotEnoughResourcesException
from virtualclusterexception import VirtualClusterException

import os
import pickle
import random
import string
import uuid

##
# The Virtual Cluster class which is responsible for creating, deleting and
# managing Virtual Clusters.
#
class VirtualCluster:

  """The Virtual Cluster class"""

  ##
  # The size of the Virtual Cluster.
  #
  n = 0

  ##
  # A unique id associated with this virtual cluster.
  #
  __uniqueid = ""

  ##
  # A list of virtual machine object instances that will compose the underlying
  # Virtual Cluster.
  #
  nodes = []

  ##
  # The symmetric key that will be used by the racoon instances of this virtual
  # cluster as protection.
  #
  __symmetricracoonkey = ""

  ##
  # Tells us if this virtual cluster is only local to a single site or if it has
  # been merged before with a different virtual cluster from a remote site. By
  # default we will say any cluster is local.
  #
  islocal = True

  ##
  # Returns a random symmetric key of the given length.
  #
  def __getsymmetrickey(self, keylength):
    return "".join(random.sample(string.letters+string.digits, keylength))

  ##
  # Creates a virtual cluster from the using the specified hosts
  # using the provided encryption algorithm.
  #
  # @param hostlist list of String The list of hosts that contribute to the
  # virtual cluster.
  # @param encalg String The encryption algorithm to use.
  # @return VirtualCluster The resulting virtual cluster.
  #
  @staticmethod
  def fromhostlist(hostlist, encalg=None):

    result = VirtualCluster()

    vmlist = []

    try:
      # We translate the host list into a list of VirtualMachine objects that
      # contain the specifications of the virtual machines to start on the
      # physical nodes.
      vmlist = Scheduler.getmachinesfromhosts(hostlist)
    except SchedulerLoadException, (inst):
      raise

    if vmlist == None:
      raise VirtualClusterException(Msg.virtualclusterproblemsencountered)
 
    result.nodes = vmlist

    # Generate a unique id for this virtual cluster.
    result.__uniqueid = uuid.uuid4().hex

    # Generate the symmetric key for the ike.
    result.__symmetricracoonkey = result.__getsymmetrickey(
                                                      Config.symmetrickeylength)

    # Time to update the configuration XML and let it know that the newly
    # reserved machines need to be taken off the grid.
    machinesupdater = MachinesUpdater(Config.machinesxmlpathlibrary)
    machinesupdater.update(result.nodes, True)


    errorsencountered = False

    for virtualmachine in result.nodes:
      # Get free IP for the virtual machine
      ip = virtualmachine.getfreeip()
      if ip == None:
        raise VirtualClusterException(Msg.virtualclusternomoreips)
      virtualmachine.reserveip(ip)
      virtualmachine.ip = ip

      gwid = virtualmachine.getfreegwid()
      if gwid == None:
        raise VirtualClusterException(Msg.virtualclusternomorebridgeids)
      virtualmachine.reservebridgeid(gwid)
      virtualmachine.gwbridgeid = gwid

      # Get free IP for the gateway virtual bridge.
      gwbridgeip = virtualmachine.getfreeip()
      if gwbridgeip == None:
        raise VirtualClusterException(Msg.virtualclusternomoreips)
      virtualmachine.gwbridgeip = gwbridgeip
      virtualmachine.reserveip(gwbridgeip)

      ipeth = virtualmachine.getfreeipeth()
      if ipeth == None:
        raise VirtualClusterException(Msg.virtualclusternomoreips)
      virtualmachine.reserveipeth(ipeth)

      virtualmachine.subinterface.ip = ipeth
      virtualmachine.subinterface.number = gwid
      


    # We need to loop through all the virtual machines and connect to the parent
    # physical machine in order to create the virtual cluster.
    for virtualmachine in result.nodes:
      with settings(host_string=virtualmachine.host,
        user=virtualmachine.getparentuser(),
        password=virtualmachine.getparentpass(), warn_only=True):

        try:

          # First we will push the subinterface on the parent physical machine
          # that will be responsible with the virtual machine's communication.
          pushsubinterfaces(virtualmachine.subinterface)

          pushvirbr(virtualmachine.gwbridgeid, virtualmachine.gwbridgeip)

          # Pushing the ipsec associations onto the physical
          # machines such as to enable the encrypted
          # communication between the virtual machines that
          # will be started and that will compose the virtual
          # cluster.
          pushassociations(virtualmachine, result.nodes)

          # Pushing the IKE configurations (if the case) onto
          # the physical machines.
          pushikeconfigs(virtualmachine, result.nodes, result.__symmetricracoonkey,
                                                        result.__uniqueid, encalg)

          # startvm(virtualmachine)


          # Because the firewall policy on the physical machine might be very
          # restrictive (e.g. DROP policies), we will push the firewall rules
          # needed for the virtual machines to see each other.
          pushforwardfirewallrules(virtualmachine)

        except VirtualClusterException, (inst):

          errorsencountered = True

          # Trigger the cleanup for the node in question.
          result.__cleanupnode(virtualmachine, False)

          break

    if errorsencountered == True:
      # Trigger the cleanup for all the other nodes already configured
      # except the current one.
      result.delete(virtualmachine, False, True)

      # Cleanup the disk due to this forced virtual cluster deletion
      result.removevirtualclusterfromdisk()

      raise VirtualClusterException(Msg.virtualclusterproblemsencountered)

    print Msg.virtualclustersuccesfulcreation.format(result.getuniqueid())

    return result

  ##
  # Constructor.
  #
  # @param pairlist list of OS name quantity pairs.
  # @param site String The name of the physical site on which the new virtual
  # cluster is requested.
  # @param encalg String The encryption algorithm to use.
  # @throws VirtualClusterException When a problem occurs with the creation of
  # the virtual cluster.
  # @throws SchedulerLoadException When there are problems parsing the XML
  # configuration file.
  # @throws SchedulerNotEnoughResourcesException When there are not enough free
  # machines to satisfy this virtual cluster creation request.
  #
  def __init__(self, pairlist = None, site=None, encalg=None):

    # Generate a unique id for this virtual cluster.
    self.__uniqueid = uuid.uuid4().hex

    # Generate the symmetric key for the ike.
    self.__symmetricracoonkey = self.__getsymmetrickey(
                                                      Config.symmetrickeylength)

    if pairlist == None:
      return

    try:
      # We translate the Operating System Quantity pairs
      # into a list of VirtualMachine objects that contain
      # the specifications of the virtual machines to start
      # on the physical nodes.
      self.nodes = Scheduler.getmachines(pairlist, site)
    except SchedulerLoadException, (inst):
      raise
    
    if self.nodes == None:
      raise SchedulerNotEnoughResourcesException(
                                             Msg.schedulernotenoughfreemachines)

    # Time to update the configuration XML and let it know that the newly
    # reserved machines need to be taken off the grid.
    machinesupdater = MachinesUpdater(Config.machinesxmlpath)
    machinesupdater.update(self.nodes, True)

    errorsencountered = False

    # We need to loop through all the virtual machines and connect to the parent
    # physical machine in order to create the virtual cluster.
    for virtualmachine in self.nodes:
      with settings(host_string=virtualmachine.host,
        user=virtualmachine.getparentuser(),
        password=virtualmachine.getparentpass(), warn_only=True):

        try:

          #if virtualmachine.parentip == "130.37.197.189":
          #  raise VirtualClusterException("Some Error message!")

          # First we will push the subinterface on the parent physical machine
          # that will be responsible with the virtual machine's communication.
          pushsubinterfaces(virtualmachine.subinterface)

          pushvirbr(virtualmachine.gwbridgeid, virtualmachine.gwbridgeip)

          # Pushing the ipsec associations onto the physical
          # machines such as to enable the encrypted
          # communication between the virtual machines that
          # will be started and that will compose the virtual
          # cluster.
          pushassociations(virtualmachine, self.nodes)

          # Pushing the IKE configurations (if the case) onto
          # the physical machines.
          pushikeconfigs(virtualmachine, self.nodes, self.__symmetricracoonkey,
                                                        self.__uniqueid, encalg)

          # Once the ipsec security associations have been
          # pushed onto the physical machines, and the IKE
          # configurations have been done, it is time to send
          # the commands to start the physical machines.
          startvm(virtualmachine)

          # Because the firewall policy on the physical machine might be very
          # restrictive (e.g. DROP policies), we will push the firewall rules
          # needed for the virtual machines to see each other.
          pushforwardfirewallrules(virtualmachine)

          # When everything is set up here, we will check if this virtual
          # machine needs an internet connection. If so, the appropriate
          # firewall rules will be pushed onto the physical machine (e.g. 
          # MASQUERADE for the virtual machine's IP).
          if virtualmachine.internetconnected == True:
            grantinternetaccess(virtualmachine)

        except VirtualClusterException, (inst):

          errorsencountered = True

          # Trigger the cleanup for the node in question.
          self.__cleanupnode(virtualmachine)

          break

    if errorsencountered == True:
      # Trigger the cleanup for all the other nodes already configured
      # except the current one.
      self.delete(virtualmachine)

      # Cleanup the disk due to this forced virtual cluster deletion
      self.removevirtualclusterfromdisk()

      raise VirtualClusterException(Msg.virtualclusterproblemsencountered)

  ## 
  # Dismantles the underlying Virtual Cluster and releases all of the used
  # resources.
  #
  # @param vm VirtualMachine The virtual machine at which the
  # creation got suspended. This parameter needs to be used only when the
  # virtual cluster cannot be created due to some exceptions. Used for
  # dismantling.
  # @param stopvms Boolean Whether to stop the virtual machines as part
  # of the deletion process. Might be useful when the overlay was created
  # from a list of images and this list needs to stay up and running.
  #
  def delete(self, vm=None, stopvms=True, islibrary=False):
    # Time to update the configuration XML and let it know that the newly
    # reserved machines need to be put back on the grid.
    machinesupdater = None
    if islibrary == False:
      machinesupdater = MachinesUpdater(Config.machinesxmlpath)
    else:
      machinesupdater = MachinesUpdater(Config.machinesxmlpathlibrary)

    machinesupdater.update(self.nodes, False)

    # We need to loop through all the virtual machines and connect to the parent
    # physical machine in order to cleanly dismantle the virtual cluster.
    for virtualmachine in self.nodes:
      with settings(host_string=virtualmachine.host,
        user=virtualmachine.getparentuser(),
        password=virtualmachine.getparentpass(), warn_only=True):
        
        # We already cleaned up this vm's configs and we already finished the
        # configs for all the other machines.
        if virtualmachine == vm:

          # Cleanup the disk mess.
          self.removevirtualclusterfromdisk()
          return

        # Now that we have the connection established, we cleanup the configs.
        self.__cleanupnode(virtualmachine, stopvms)

        virtualmachine.releaseports()

        virtualmachine.releaseips()

        virtualmachine.releaseipseth()

        virtualmachine.releasebridgeids()

  ##
  # Cleans up the virtual cluster configurations for the provided node.
  # Surrounds each instruction with a try block so that minor errors don't block
  # the dismantling of the configs.
  # 
  # @param virtualmachine VirtualMachine The virtual machine for which to
  # cleanup the configurations.
  # @param stopvms Boolean Whether to stop the virtual machines as part
  # of the deletion process. Might be useful when the overlay was created
  # from a list of images and this list needs to stay up and running.
  #
  def __cleanupnode(self, virtualmachine, stopvms=True):
    try:
      # First we will launch the remote command that attempts to stop the 
      # virtual machine.
      if stopvms == True:          
        stopvm(virtualmachine)
    finally:
      pass

    try:
      # Next, we need to delete the IKE configurations on
      # the remote physical machines.
      #deleteikeconfigs(virtualmachine, self.__uniqueid)
      deletebulkike(virtualmachine)
      pass
    finally:
      pass

    try:
      # It is time to delete the ipsec security
      # association from the remote physical machines
      #deleteassociations(virtualmachine, self.nodes)
      deletebulkassociations(virtualmachine)
      pass
    finally:
      pass

    try:
      # Finally, we need to remove the subinterface on the physical machine
      # that was used for this virtual machine's communication.
      deletesubinterfaces(virtualmachine.subinterface)
    finally:
      pass

    try:
      deletevirbr(virtualmachine.gwbridgeid)
    finally:
      pass

    try:
      # Cleanup the forward rules that were needed to enable the virtual
      # machines to see each other.
      deleteforwardfirewallrules(virtualmachine)
    finally:
      pass

    try:
      # Now, we need to remove internet access from the node, in case it had it.
      if virtualmachine.internetconnected == True:
        revertinternetaccess(virtualmachine)
    finally:
      pass


  ##
  # When a virtual cluster is created, it will be saved to disk such that it can
  # be reloaded later and dismantled (or merged, ...)
  #
  def savevirtualclustertodisk(self):
    # The path to the serialization directory of this user.
    pathtoserialized = "{0}/{1}/".format(Config.resourcespath, os.getlogin())

    # Time to check whether this user has created a virtual cluster previously.
    if os.path.exists(pathtoserialized) == False:
      # Create the path for this user.
      os.mkdir(pathtoserialized)

    # Get the serialization path of this virtual cluster.
    serializedfilepath = "{0}{1}.pk1".format(
                                           pathtoserialized, self.getuniqueid())

    # Serialize this newly created Virtual Cluster and provide a unique id for it
    # that is to be given as output to the user. That id need to be used for the
    # destruction of the virtual cluster.
    pickle.dump(self, open(serializedfilepath, 'wb'))

  ##
  # Removes a virtual cluster's representation from disk.
  #
  def removevirtualclusterfromdisk(self):
    # The path to the serialization directory of this user.
    pathtoserialized = "{0}/{1}/".format(Config.resourcespath, os.getlogin())

    # Time to check whether this user has created a virtual cluster previously.
    if os.path.exists(pathtoserialized) == False:
      # Create the path for this user.
      return

    # Get the serialization path of this virtual cluster.
    serializedfilepath = "{0}{1}.pk1".format(
                                           pathtoserialized, self.getuniqueid())

    if os.path.exists(serializedfilepath) == False:
      return

    # Now we need to clean the just deleted virtual cluster from disk.
    os.remove("{0}".format(serializedfilepath))

  ##
  # Loads a virtual cluster from disk.
  #
  # @param uniqueid String The unique id of the virtual cluster to load.
  # @returns VirtualCluster The loaded virtual cluster or None if it doesn't
  # exist.
  #
  @staticmethod
  def loadvirtualclusterfromdisk(uniqueid):
    # First consider this user's name. Important to consider that any user can
    # only dismantle his own virtual cluster's. We will use his username as an
    # identifier for this.
    username = os.getlogin()

    # The path to the serialization directory of this user.
    pathtoserialized = "{0}/{1}/".format(Config.resourcespath, username)

    # Get the serialization path of this virtual cluster.
    serializedfilepath = "{0}{1}.pk1".format(pathtoserialized, uniqueid)

    # Time to check whether this user has created a virtual cluster previously.
    if os.path.exists(serializedfilepath) == False:
      # Ignore the command, it has no value.
      return None

    # Deserialize the found virtual cluster.
    vc = pickle.load(open(serializedfilepath, 'rb'))

    return vc

  ##
  # Getter for the unique id of the virtual cluster.
  #
  def getuniqueid(self):
    return self.__uniqueid

  ##
  # Return a list of (cluster node hostname, virtual machine ip, gateway bridge ip,
  # virtual bridge on host the vm needs to be linked to).
  # tuples.
  #
  # @returns The list of tuples.
  #
  def getvmipandgwlist(self):

    iplist = []

    for vm in self.nodes:
      if vm.host != "":
        iplist.append((vm.host, vm.ip, vm.gwbridgeip, "virbr{0}".format(vm.gwbridgeid)))
      else:
        iplist.append((vm.parentip, vm.ip, vm.gwbridgeip, "virbr{0}".format(vm.gwbridgeid)))

    return iplist

  ##
  # Getter for the symmetric racoon key for the virtual cluster.
  #
  def getracoonkey(self):
    return self.__symmetricracoonkey


  ##
  # Gets a string representation of the virtual cluster.
  #
  def tostring(self):
    vmtostring = ""
    for virtualmachine in self.nodes:
      vmtostring += virtualmachine.tostring()

    return "VIRTUAL CLUSTER\nuniqueid={0}\n{1}\n".format(
                                                self.getuniqueid(), vmtostring)    

  ##
  # Merges two virtual cluster instances.
  # Warning: The two virtual clusters will be temporarily disabled for their
  # merge operation. Once the merging operation is complete, the two virtual
  # clusters will be removed and the resulting merged cluster will be the new
  # cluster the user can use for his custom operations.
  #
  # @param virtualclusterid1 String The first virtual cluster's id of the
  # merging operation.
  # @param virtualclusterid2 String The second virtual cluster's id of the
  # merging operation.
  # @param encalg String The name of the encryption algorithm to use.
  # @returns VirtualCluster The resulting mergem virtual cluster.
  # @throws VirtualClusterException When a problem occurs with the creation of
  # the virtual cluster.
  #
  @staticmethod
  def merge(virtualclusterid1, virtualclusterid2, encalg=None):

    vc1 = VirtualCluster.loadvirtualclusterfromdisk(virtualclusterid1)
    vc2 = VirtualCluster.loadvirtualclusterfromdisk(virtualclusterid2)

    if vc1 == None or vc2 == None:
      raise VirtualClusterException(Msg.virtualclustermergegenericerror)

    for vm1 in vc1.nodes:
      for vm2 in vc2.nodes:
        if vm1 == vm2:
          raise VirtualClusterException(Msg.virtualclustermergerepeatednode)

    allnodes = []
    for item in vc1.nodes:
      allnodes.append(item)
    for item in vc2.nodes:
      allnodes.append(item)

    result = VirtualCluster()
    result.nodes = allnodes

    if vc1.islocal == False or vc2.islocal == False or vc1.nodes[0].site != vc2.nodes[0].site:
      # Remote merge.
      result.islocal = False
      pass
    else:
      # Local merge.
      result.islocal = True
      pass

    # Get free ports for the racoon instances for all the machines.
    ports = None
    if result.islocal == False:
      ports = {}
      for virtualmachine in vc1.nodes:
        port = virtualmachine.getfreeport()
        if port != None:
          ports[virtualmachine] = port
        else:
          raise VirtualClusterException(Msg.virtualclustermergenomoreports)
      for virtualmachine in vc2.nodes:
        port = virtualmachine.getfreeport()
        if port != None:
          ports[virtualmachine] = port
        else:
          raise VirtualClusterException(Msg.virtualclustermergenomoreports)

    for k, v in ports.iteritems():
      k.reserveport(v)

    # We need to loop through all the virtual machines and connect to the parent
    # physical machine in order to create the virtual cluster.
    for virtualmachine in vc1.nodes:
      with settings(host_string=virtualmachine.host,
        user=virtualmachine.getparentuser(),
        password=virtualmachine.getparentpass(), warn_only=True):

        # Pushing the ipsec associations onto the physical
        # machines such as to enable the encrypted
        # communication between the virtual machines that
        # will be started and that will compose the virtual
        # cluster.
        pushassociations(virtualmachine, vc2.nodes, result.islocal)

        if result.islocal == True:
          deleteikeconfigs(virtualmachine, vc1.getuniqueid())
          # Pushing the IKE configurations (if the case) onto
          # the physical machines.
          pushikeconfigs(virtualmachine, allnodes, result.getracoonkey(),
                                                   result.getuniqueid(), encalg)
        else:
          pushikeconfigs(virtualmachine, vc2.nodes, result.getracoonkey(),
                             result.getuniqueid(), encalg, ports)

    # We need to loop through all the virtual machines and connect to the parent
    # physical machine in order to create the virtual cluster.
    for virtualmachine in vc2.nodes:
      with settings(host_string=virtualmachine.host,
        user=virtualmachine.getparentuser(),
        password=virtualmachine.getparentpass(), warn_only=True):

        # Pushing the ipsec associations onto the physical
        # machines such as to enable the encrypted
        # communication between the virtual machines that
        # will be started and that will compose the virtual
        # cluster.
        pushassociations(virtualmachine, vc1.nodes, result.islocal)

        if result.islocal == True:
          deleteikeconfigs(virtualmachine, vc2.getuniqueid())

          # Pushing the IKE configurations (if the case) onto
          # the physical machines.
          pushikeconfigs(virtualmachine, allnodes, result.getracoonkey(),
                                                   result.getuniqueid(), encalg)
        else: 
          pushikeconfigs(virtualmachine, allnodes, result.getracoonkey(), 
                          result.getuniqueid(), encalg, ports)


    result.savevirtualclustertodisk()

    vc1.removevirtualclusterfromdisk()
    vc2.removevirtualclusterfromdisk()

    return result


