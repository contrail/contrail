import socket, struct

##
# Convert decimal dotted quad string to long integer.
#
# @param ip String IP address in string format.
# @returns long Integer representation of the IP.
#
def iptoint(ip):
  # convert decimal dotted quad string to long integer
  strs = ip.split('.')
  p1 = int(strs[0])
  p2 = int(strs[1])
  p3 = int(strs[2])
  p4 = int(strs[3])
  #print "IPTOINT {0}".format(p1*256*256*256 + p2*256*256 + p3*256 + p4)
  return p1*256*256*256 + p2*256*256 + p3*256 + p4

##
# Convert long in to dotted quad string.
#
# @param n long IP address in longint format.
# @returns String The IP address in string representation.
#
#
def inttoip(n):
  aux = n
  # convert long int to dotted quad string
  p4 = aux % 256
  aux = aux / 256
  p3 = aux % 256
  aux = aux / 256
  p2 = aux % 256
  aux = aux / 256
  p1 = aux
  #print "INTTOIP {0}.{1}.{2}.{3}".format(p1, p2, p3, p4)
  return "{0}.{1}.{2}.{3}".format(p1, p2, p3, p4)

