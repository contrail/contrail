from virtualmachine import VirtualMachine

##
# The domain XML that will be used as a template to start up all the virtual
# machines that will compose the Virtual Cluster using XEN. This template will 
# be filled in by formatting it with the parameters coming from the virtual
# machine object instance.
#
xendomainxml = "<domain type='{0}'>\n\
  <name>{1}</name>\n\
  <memory>{2}</memory>\n\
  <vcpu>{3}</vcpu>\n\
  <!--<bootloader>/usr/lib/xen-3.2-1/bin/pygrub</bootloader>\n\
  <bootloader>/usr/lib/xen-default/bin/pygrub</bootloader> -->\n\
  <os>\n\
    <type>{0}</type>\n\
    <loader>/usr/lib/xen-3.2-1/boot/hvmloader</loader>\n\
    <kernel>/boot/vmlinuz-2.6.26-2-xen-686</kernel>\n\
    <initrd>/boot/initrd.img-2.6.26-2-xen-686</initrd>\n\
    <cmdline>root=/dev/hda</cmdline>\n\
  </os>\n\
    <clock offset='utc'/>\n\
    <on_poweroff>destroy</on_poweroff>\n\
    <on_reboot>restart</on_reboot>\n\
    <on_crash>destroy</on_crash>\n\
    <devices>\n\
      <emulator>{4}</emulator>\n\
      <console type='pty'>\n\
        <target type='virtio'/>\n\
      </console>\n\
      <disk type='file' device='disk'>\n\
        <source file='{5}'/>\n\
        <target dev='hda'/>\n\
      </disk>\n\
      <interface type='network'>\n\
        <source network='default'/>\n\
        <mac address='{6}'/>\n\
        <start mode='onboot'/>\n\
        <protocol family='ipv4'>\n\
          <ip address='{7}' prefix='24'/>\n\
          <route gateway='{8}'/>\n\
        </protocol>\n\
      </interface>\n\
      <graphics type='vnc' port='-1' listen='127.0.0.1'/>\n\
    </devices>\n\
  </domain>";

##
# The domain XML that will be used as a template to start up all the virtual
# machines that will compose the Virtual Cluster using KVM. This template will 
# be filled in by formatting it with the parameters coming from the virtual
# machine object instance.
#
kvmdomainxml = "<domain type='{0}'>\n\
  <name>{1}</name>\n\
<!--  <uuid>009902b1-cf27-32c9-b5cc-9cdca08e75f8</uuid> -->\n\
  <memory>{2}</memory>\n\
  <currentMemory>{2}</currentMemory>\n\
  <vcpu>{3}</vcpu>\n\
  <os>\n\
    <!-- <type arch='i686' machine='pc-0.12'>hvm</type> -->\n\
    <type>hvm</type> \n\
    <boot dev='hd'/>\n\
  </os>\n\
  <features>\n\
    <acpi/>\n\
    <apic/>\n\
    <pae/>\n\
  </features>\n\
  <clock offset='utc'/>\n\
  <on_poweroff>destroy</on_poweroff>\n\
  <on_reboot>restart</on_reboot>\n\
  <on_crash>restart</on_crash>\n\
  <devices>\n\
    <emulator>{4}</emulator>\n\
    <disk type='file' device='disk'>\n\
      <driver name='qemu' type='raw'/>\n\
      <source file='{5}'/>\n\
      <target dev='hda' bus='ide'/>\n\
      <address type='drive' controller='0' bus='0' unit='0'/>\n\
    </disk>\n\
<!--    <disk type='block' device='cdrom'>\n\
      <driver name='qemu' type='raw'/>\n\
      <target dev='hdc' bus='ide'/>\n\
      <readonly/>\n\
      <address type='drive' controller='0' bus='1' unit='0'/>\n\
    </disk> -->\n\
    <controller type='ide' index='0'>\n\
      <address type='pci' domain='0x0000' bus='0x00' slot='0x01' function='0x1'/>\n\
    </controller>\n\
    <interface type='network'>\n\
      <source network='default'/>\n\
      <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>\n\
      <protocol family='ipv4'>\n\
        <ip address='{6}' prefix='24'/>\n\
        <route gateway='{7}'/>\n\
      </protocol>\n\
    </interface>\n\
    <serial type='pty'>\n\
      <target port='0'/>\n\
    </serial>\n\
    <console type='pty'>\n\
      <target type='serial' port='0'/>\n\
    </console>\n\
    <input type='mouse' bus='ps2'/>\n\
    <graphics type='vnc' port='-1' autoport='yes' keymap='en-us'/>\n\
    <video>\n\
      <model type='cirrus' vram='9216' heads='1'/>\n\
      <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>\n\
    </video>\n\
    <memballoon model='virtio'>\n\
      <address type='pci' domain='0x0000' bus='0x00' slot='0x04' function='0x0'/>\n\
    </memballoon>\n\
  </devices>\n\
</domain>\n";


##
# Converts a virtual machine instance into a domain XML representation according
# to http://libvirt.org/formatdomain.html . Thus, it creates a viable domain XML
# that can be used to boot up a virtual machine on a remote host. All the
# required parameter to build the domain XML can be found in the virtual machine
# instance
# @virtualmachine VirtualMachine The virtual machine instance that contains all
# the necessary data for creating a libvirt domain xml file.
# @returns XMl ?
#
def virtualmachinetodomainxml(virtualmachine):
  if virtualmachine.domain == 'xen':
    return xendomainxml.format(
                     virtualmachine.domain, virtualmachine.name,
                     virtualmachine.memory, virtualmachine.vcpu,
                     virtualmachine.emulator, virtualmachine.imagepath,
                     virtualmachine.mac, virtualmachine.ip,
                     virtualmachine.gatewayip)
  elif virtualmachine.domain == 'kvm':
    return kvmdomainxml.format(virtualmachine.domain, virtualmachine.name,
                     virtualmachine.memory, virtualmachine.vcpu,
                     virtualmachine.emulator, virtualmachine.imagepath,
                     virtualmachine.ip, virtualmachine.gatewayip)
    


