from cfg.config import Config

from xml.etree.ElementTree import *

import os

import uuid

##
# The MachinesUpdater class is responsible with modifying the machine XML
# configuration file so as it reflects the state of all virtual machines
# (whether they are being used or not). This will allow new reservations to know
# which virtual machines are available for them.
#
class MachinesUpdater:

  ##
  # The path to the machines XML configuration file. 
  #
  __machinesconfigpath = ""

  ##
  # MachinesUpdater constructor.
  #
  # @param machinesconfigpath String The path to the machines XML configuration
  # file.
  #
  def __init__(self, machinesconfigpath):
    self.__machinesconfigpath = machinesconfigpath

  ##
  # The update method needs to be called when the state of a subset of virtual
  # machines has changed (i.e. reserved or not reserved). This method will be
  # responsible with updating the configuration file accordingly and flushing
  # the changes to disk for persistent storage.
  #
  # @param virtualmachinelist List of VirtualMachine object instances. The list
  # of virtual machines that will be affected by this configuration file update
  # call.
  # @param newreservedvalue Boolean The new value that needs to be assigned to
  # reserved attribute for the provided list of virtual machines.
  #
  def update(self, virtualmachinelist, newreservedvalue):

    # Quick conversion from Boolean to String.
    updatevalue = 'false'
    if newreservedvalue == True:
      updatevalue = 'true'

    # Create the ElementTree from the machines configuration file path.
    tree = ElementTree()
    tree.parse(self.__machinesconfigpath)

    siteselements = tree.findall('site')

    for site in siteselements:
      # We care only about the 'machine' (i.e. physical machines) entries. 
      physicalmachineselements = site.findall('machine')

      # Loop through all the physical machine elements in the XML.
      for physicalmachineelement in physicalmachineselements:
        
        # Find out if this physical machine element is the parent of any of the 
        # provided virtual machines that need a reserved status update.
        for virtualmachine in virtualmachinelist:

          # Is the physical machine the parent of this virtual machine?
          if virtualmachine.parentip == physicalmachineelement.get('ip'):
            
            # Yes, it is his parent, get all it's vm's and start searching for the
            # appropriate one.
            virtualmachineselements = physicalmachineelement.find(
                                          'vms').findall('vm')

            # Loop through all the virtual machine XML elements.
            for virtualmachineelement in virtualmachineselements:

              # Is this element the searched one?
              if virtualmachineelement.get('reserved') != updatevalue:

                # Yes, found it! Update it's reserved status as requested.
                virtualmachineelement.set('reserved', updatevalue)
                break

    # Create a backup of the machines configuration XML file with a uniques
    # name.
    #os.rename(Config.machinesxmlpath,
    #         "{0}backup_{1}.xml".format(Config.resourcespath, uuid.uuid4().hex))

    os.remove(self.__machinesconfigpath)

    # Flush out the new machines configuration XML file.
    tree.write(self.__machinesconfigpath)


