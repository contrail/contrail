__supported = {'aes':True, '3des':True, 'blowfish':True, 'camellia':False, 'rijndael 192':True, 'blowfish 192':True}

##
# Checks whether an encryption algorithm is supported or not.
#
# @param algorithm String The algorithm to check.
# @returns Boolean True if the algorithm is supported. False otherwise.
#
def isencryptionsupported(algorithm):
  if algorithm in __supported and __supported[algorithm] == True:
    return True

  return False


