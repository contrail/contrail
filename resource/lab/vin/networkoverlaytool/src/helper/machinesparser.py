from machine import PhysicalMachine

from physicalsite import PhysicalSite

from subinterface import SubInterface

from xml.sax import saxutils
from xml.sax import handler

from virtualmachine import VirtualMachine

from ipconvertors import iptoint
from incuidgenerator import getuid

##
# A XML parsing class for the machines in the configuratin file. 
#
class MachinesParser(handler.ContentHandler):

  ##
  # A collection containing all the sites with their underlying physical
  # machines. It is a collection of Site instances.
  #
  sites = None

  ##
  # The name of the currently parsed site.
  #
  __currentsitename = ""

  ##
  # A collection containing all the physical machines parsed so far from the
  # configuration file. We leave it public so that it can be retrieved once the
  # parsing of the XML configuration file has been complete.
  #
  __physicalmachines = None

  ##
  # A PhysicalMachine instance object that represents the currently parsed
  # physical machine entry from the configuration file.
  #
  __currentphysicalmachine = None

  ##
  # A VirtualMachine instance object that represents the currently parsed
  # virtual machine entry from the configuration file.
  #
  __currentvirtualmachine = None

  ##
  # The start ip of an allocated range in integer format.
  #
  __ipstart = 0

  ##
  # The end ip of an allocated range in integer format.
  #
  __ipend = 0

  __ipethstart = 0
  __ipethend = 0

  ##
  # A default parameterless constructor.
  #
  def __init__(self):
    return

  ##
  # Overriding of the startElement parsing method. This is the method that
  # automatically gets called whenever a new xml element gets parsed. The
  # XML parsing works as follows: we instantiate and start populating the
  # members when these startElement function calls occur; moreover, we finalize
  # the parsings in the endElement override.
  #
  # @param name String The name of the XML element that caused the callback.
  # @param attrs collection The attributes of the XML element that caused the
  # callback.
  #
  def startElement(self, name, attrs):
    # We are at the beginning of the configuration file, we create the empty
    # physical machine collection.
    if name == 'xml':
      self.sites = []

    elif name == 'site':
      self.__physicalmachines = []
      self.__currentsitename = attrs.get('name', None)
      self.__ipstart = iptoint(attrs.get('ipstart', None))
      self.__ipend = iptoint(attrs.get('ipend', None))
      self.__ipethstart = iptoint(attrs.get('ipethstart', None))
      self.__ipethend = iptoint(attrs.get('ipethend', None))

    # When we reach a new machine entry, we create a new physical machine and
    # get the appropriate attributes from the configuration file.
    elif name == 'machine':
      self.__currentphysicalmachine = PhysicalMachine()
      self.__currentphysicalmachine.uniqueid = getuid()
      self.__currentphysicalmachine.ip = attrs.get('ip', None)
      self.__currentphysicalmachine.name = attrs.get('name', None)
      self.__currentphysicalmachine.ipRangeMin = self.__ipstart
      self.__currentphysicalmachine.ipRangeMax = self.__ipend

    elif name == 'portRange':
      self.__currentphysicalmachine.portRangeMin = int(attrs.get('min', None))
      self.__currentphysicalmachine.portRangeMax = int(attrs.get('max', None))

    # Get the authentication details for the current machine.
    elif name == 'login':
      self.__currentphysicalmachine.setusername(attrs.get('name', None))
      self.__currentphysicalmachine.setpassword(attrs.get('password', None))

    # A new virtual machine entry, instantiate it and get its attributes.
    elif name == 'vm':
      self.__currentvirtualmachine = VirtualMachine()
      self.__currentvirtualmachine.uniqueid = getuid()
      self.__currentvirtualmachine.name = attrs.get('name', None)
      self.__currentvirtualmachine.operatingsystem = attrs.get('os', None)
      self.__currentvirtualmachine.domain = attrs.get('domain', None)
      self.__currentvirtualmachine.memory = attrs.get('memory', None)
      self.__currentvirtualmachine.vcpu = attrs.get('vcpu', None)
      self.__currentvirtualmachine.reserved = attrs.get('reserved', None)
      self.__currentvirtualmachine.site = self.__currentsitename
      self.__currentvirtualmachine.ipRangeMin = self.__ipstart
      self.__currentvirtualmachine.ipRangeMax = self.__ipend
      self.__currentvirtualmachine.ipethRangeMin = self.__ipethstart
      self.__currentvirtualmachine.ipethRangeMax = self.__ipethend

    elif name == 'subinterface':
      self.__currentvirtualmachine.subinterface = SubInterface(attrs.get(
          'attachedto', None), attrs.get('number', None),
          attrs.get('ip', None))

    # Get the current virtual machine's disk image path.
    elif name == 'image':
      self.__currentvirtualmachine.imagepath = attrs.get('path', None)

    # The emulator used for the current virtual machine in the libvirt
    # configurations.
    elif name == 'emulator':
      self.__currentvirtualmachine.emulator = attrs.get('path', None)
    
    # MAC address for the virtual machine.
    elif name == 'mac':
      self.__currentvirtualmachine.mac = attrs.get('address', None)

    # IP address for the virtual machine.
    elif name == 'ip':
      self.__currentvirtualmachine.ip = attrs.get('address', None)

    # Gateway address for this virtual machine.
    elif name == 'route':
      self.__currentvirtualmachine.gatewayip = attrs.get('gateway', None)

  ##
  # Overriding of the endElement parsing method. This is used to finalize the
  # parsing of that particular element.
  #
  # @param name String The name of the XML element that caused the callback.
  #
  def endElement(self, name):
    # If a machine element is finished, let it know that the setup is complete
    # and add it to the collection.
    if name == 'machine':
      self.__currentphysicalmachine.completesetup()
      self.__physicalmachines.append(self.__currentphysicalmachine)

    elif name == 'site':
      self.sites.append(
                  PhysicalSite(self.__currentsitename, self.__physicalmachines,
                               self.__ipstart, self.__ipend))

    # When a virtual machine element is complete, add that virtual machine to
    # the physical machine's collection of virtual machines.
    elif name == 'vm':
      # Consider this virtual machine only if it is not reserved/being used by
      # someone else at this point. 
      if self.__currentvirtualmachine.reserved == "false":
        self.__currentphysicalmachine.addvirtualmachine(
          self.__currentvirtualmachine)
      

