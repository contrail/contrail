import os

from cfg.config import Config

##
# A class designed to manage locking and unlocking of access to the virtual
# cluster application. Practically it naively serializez process access.
#
class Locker:

  ##
  # Checks whether calls can be made to the virtual cluster utility.
  #
  @staticmethod  
  def islocked():

    if os.path.exists(Config.lockfile):
      return True

    return False

  ##
  # Disables access for other users. Users need to call islocked first and if
  # islocked returns false, then this can be called.
  #
  @staticmethod
  def lock():

    f = open(Config.lockfile, "w")
    f.write(os.getlogin())
    f.close()

    
  ##
  # Enables access to other users.
  #
  @staticmethod
  def unlock():

    os.remove(Config.lockfile)


