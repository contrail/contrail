
from virtualmachine import VirtualMachine

##
# The template used to create the racoon configuration file header.
#
ikeheader = "\
path pre_shared_key \"{2}\";\n\
path pidfile \"{3}\";\n\
listen {0}\n\
    isakmp {4}{5};\n\
  {1}\n"

##
# The template used to create a racoon configuration .
#
ikeconfigtemplate = "\
	remote {0}{5} {3}\n\
    nat_traversal off;\n\
    exchange_mode main;\n\
    proposal {3}\n\
      encryption_algorithm rijndael 192;\n\
      hash_algorithm sha1;\n\
      authentication_method pre_shared_key;\n\
      dh_group modp1024;\n\
    {4}\n\
    generate_policy off;\n\
  {4}\n\
  sainfo address {1} any address {2} any {3}\n\
    pfs_group modp768;\n\
    encryption_algorithm {6};\n\
    authentication_algorithm non_auth;\n\
    compression_algorithm deflate;\n\
  {4}\n\
  \n\n"

##
# The template used for a psk entry for the secure communication between two
# endpoints.
#
psktemplate = "\
{0} {1}\n"

##
# Returns the racoon header for a set of racoon configuration file configs.
# It is created from the template header provided above.
#
# @param presharedkeypath String The path of the file containing the preshared
# keys used for the encryption between two endpoints.
# @param pidfilepath String The path where the file that contains the process
# id of the newly spawned racoon process. Useful for elegant dismantling of the
# configurations.
# @param usepublic Boolean Whether to use the public ip.
# @param virtualmachine VirtualMachine The virtual machine for which this header
# needs to be composed.
# @param ports Dictionary(virtualmachine, port). If this is provided, the public
# ip's will be used.
#
def getracoonheader(
               presharedkeypath, pidfilepath, usepublic, virtualmachine, ports):

  ip = virtualmachine.subinterface.ip
  port = ""
  if usepublic == True:
    ip = virtualmachine.parentip
    port = "[{0}]".format(ports[virtualmachine])

  return ikeheader.format("{", "}", presharedkeypath, pidfilepath, ip, port)

##
# Returns the racoon configurations used to encrypt the communication between
# the two virtual machines.
#
# @param virtualmachine1 VirtualMachine One of the endpoints for the secure
# communication that this will enable.
# @param virtualmachine2 VirtualMachine One of the endpoints for the secure
# communication that this will enable.
# @param encalg String The encryption algorithm to use. None means the default
# will be used (aes).
# @param ports Dictionary(virtualmachine, port). If this is provided, the public
# ip's will be used.
#
def getracoonconf(virtualmachine1, virtualmachine2, encalg=None, ports=None):

  usepublicip = False
  if ports != None:
    usepublicip = True

  remoteip = virtualmachine2.subinterface.ip
  localip = virtualmachine1.subinterface.ip
  remoteport = ""
  localport = ""

  if usepublicip == True:
    remoteip = virtualmachine2.parentip
    localip = virtualmachine1.parentip
    remoteport = "[{0}]".format(ports[virtualmachine2])
    localport = "[{0}]".format(ports[virtualmachine1])

  if encalg == None:
    encalg = "aes";

  # Create the racoon config script from the above defined template and the
  # provided parameters.
  return ikeconfigtemplate.format(remoteip, virtualmachine1.ip,
                   virtualmachine2.ip, "{", "}", remoteport, encalg)

##
# Returns the racoon preshared key content needed for secure communication
# between the two provided virtual machines.
#
# @param virtualmachine1 VirtualMachine One of the endpoints for the secure
# communication that this will enable.
# @param virtualmachine2 VirtualMachine One of the endpoints for the secure
# communication that this will enable.
# @param symmetricsecuritykey String The symmetric security key that will
# protect the communication through racoon.
# @param ports Dictionary(virtualmachine, port). If this is provided, the public
# ip's will be used.
#
def getpskconf(
            virtualmachine1, virtualmachine2, symmetricsecuritykey, ports=None):

  usepublicip = False
  if ports != None:
    usepublicip = True

  remoteip = virtualmachine2.subinterface.ip
  if usepublicip == True:
    remoteip = virtualmachine2.parentip

  return psktemplate.format(remoteip, symmetricsecuritykey)


