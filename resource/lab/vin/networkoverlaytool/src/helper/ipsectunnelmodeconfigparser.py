from virtualmachine import VirtualMachine

##
# The template shell script used to create a new security association on the
# remote physical machines. This will be used to link together all of the
# virtual cluster components. Hence, this script is responsible for setting up
# the secure ipsec channel between two endpoints. 
#
ipsecnewtunnelshelltemplate = "\
spd{6} {0} {1} any -P {4} ipsec esp/tunnel/{2}-{3}/require;\n\
spd{6} {1} {0} any -P {5} ipsec esp/tunnel/{3}-{2}/require;\n\
"

##
# Returns the top part of the configuration script for ipsec.
#
def getipsecconfigheader():
  return "#!/bin/sh\n\
sudo setkey -c << EOF\n"

def getipsecconfigfooter():
  return "EOF\n"

##
# Generates an ipsec configuration script using the command setkey based on the
# above described template (check 'man setkey' for more details). This ipsec
# configuration script is generated for two given endpoints (virtual machines)
# using their parent physical machines to do the actual encryption.
#
# @param virtualmachine1 VirtualMachine The first endpoint for the setting up
# of the secure tunnel.
# @param virtualmachine2 VirtualMachine The second endpoint for the setting up
# of the secure tunnel.
# @param spiout int A normal decimal number that represents the Security
# Parameter Index (SPI) for the SAD and SPD entries on ipsec. This is the SPI
# used for the outgoing communication (virtualmachine1 -> virtualmachine2).
# @param spiin int This is the SPI used for the incoming communication
# (virtualmachine2 -> virtualmachine1).
# @param idout int A unique integer id associated with the outgoing SAD rule.
# Please note that values 0-255 are reserved by IANA for future use.
# @param idin int A unique integer id associated with the ingoing SAD rule.
# Please note that values 0-255 are reserved by IANA for future use.
# @param enckey String the encryption key used for these security associations.
# @param usepublicip Boolean Whether to use the public ip for the tunnel.
# @returns String representing the shell script that needs to be executed
#
def ipsecconfigscriptfromvirtualmachinepair(
    virtualmachine1, virtualmachine2, command, usepublicip=False):

  tunnelendp1 = virtualmachine1.subinterface.ip
  tunnelendp2 = virtualmachine2.subinterface.ip

  if usepublicip == True:
    tunnelendp1 = virtualmachine1.parentip
    tunnelendp2 = virtualmachine2.parentip

  return ipsecnewtunnelshelltemplate.format(virtualmachine1.ip,
            virtualmachine2.ip, tunnelendp1, tunnelendp2, "out", "in", command)


