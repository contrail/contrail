from cfg.config import Config

from virtualclusterexception import VirtualClusterException

import os

##
# Gets a new unique id on an incremental basis. Each call increases the previous
# id by 1.
#
def getuid():
  # Directory with uid descriptions.
  uiddir = "{0}/uids/".format(Config.resourcespath)
  uidfilepath = "{0}uids.txt".format(uiddir)
  if os.path.exists(uiddir) == False:
    os.mkdir(uiddir)
    f = open(uidfilepath, "w")
    f.write("1")
    f.close()
    return 1

  if os.path.exists(uidfilepath) == False:
    f = open(uidfilepath, "w")
    f.write("1")
    f.close()
    return 1

  f = open(uidfilepath, "r")  
  uid = int(f.readline()) + 1
  f.close()

  f = open(uidfilepath, "w")
  f.write("{0}".format(uid))
  f.close()

  return uid

