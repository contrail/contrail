\chapter{Virtual Cluster Network configuration}
\label{chap:virtualclusterconfiguration}

As described in the previous sections, two major technologies were considered for establishing a secure communication backbone: OpenVPN and IPsec. On one hand, OpenVPN is known for how easy it is to use and configure to accomplish complicated tasks like site-to-site tunnels, in contrast to IPsec. On the other hand, having security in mind, as sketched in the previous section, IPsec provides a perfectly symmetric connection between two endpoints without either of them having a special role (e.g. server/client) in contrast to OpenVPN which requires one of the peers to act as a server and the other as a client. This inherent asymmetry within OpenVPN complicates the process of automating the management of virtual cluster networks. In addition, in the next section we will provide performance measurements that will fully justify our choice of IPsec over OpenVPN as a secure communication backbone.

In addition to using IPsec, we used the racoon~\cite{Racoon} IKE implementation for security association negotiations. We chose to use an IKE implementation for automatic negociations as opposed to manually pushed configurations because this approach provides and extra layer of automation. Moreover, racoon takes care of periodic key renegotiations. Finally, as shown in the evaluations in the next chapter, this automation comes at little or no cost.

We also used the Python Fabric~\cite{Fabfile} tool as means of pushing configurations to the cluster physical machines that will host virtual machines. An alternative to this approach could have been writing a daemon application on each cluster node that listens for commands to deploy virtual cluster networks. We chose the Python Fabric tool because it provides a secure layer for command pushing and an interface that is easy to understand and use. Moreover, the Fabric solution was much more straightforward and less error-prone.

We have thus provided an easy to use solution for automatically deploying fast, scalable and secure virtual cluster networks using the Python language, based on a routed setup (as presented in the next section) as opposed to a bridged setup, using IPsec for the security layer, racoon for automatic key management and Fabric for pushing configurations to remote machines.

This chapter is structured as follows: Section 1 briefly explains how communication is established and the need of a security layer, Section 2 introduces the need for a master node which is a central point of management in the lifecycle of any virtual cluster that may or may not span across multiple physical sites, Section 3 briefly describes the Python Fabric tool that was used to push configurations from the master node to all participants of a virtual cluster, Section 4 describes the way in which a virtual cluster is created, Section 5 explains the process of merging two virtual clusters and Section 6 describes the process of cleanly deleting a virtual cluster.

\section{Communication and the security layer}

The first important step in creating a virtual cluster is managing to link together all the allocated virtual machines. Our target is to accomplish a setup in which the virtual machines are entirely unaware neither of any network topology nor of the fact that there are any physical machines facilitating the communication between virtual machines. 

\begin{figure}[htb]
\centering
\includegraphics[width=100mm]{res/Communication.png}
\caption{Communication between two virtual machine endpoints.}\label{fig:Communication}
\end{figure}

There are two major options to enable communication between virtual machines on different physical cluster nodes: a bridged setup or a routed setup.

Bridging is a forwarding technique used in packet switched computer networks. It relies on flooding and examination of source addresses in received packets to locate unknown devices and records their locations in tables so as to minimize the need for flooding. Bridging takes place at the OSI Model Layer 2 \cite{Tanenbaum1988} (data link layer), which means that frames are directed according to MAC addresses. This implies that bridging is unable to distinguish networks. Moreover, due to the flooding approach bridging does not scale to large networks.

Routing is the process of selecting paths along which to send traffic in a network. It takes place at the OSI Model Layer 3 \cite{Tanenbaum1988} (network layer), which means that decisions are made based on assigned IP addresses. As a result, routing can distinguish between networks, unlike bridging. In addition, as explained in the following section, this is the needed setup for the security layer.

For examples of how to create a routing setup between two physical machines, each hosting virtual machines, please check Appendix A.


One of the important aspects in creating virtual cluster networks for our purpose is security. Considering that multiple users will be making use of the same physical resources at the same time, multiple virtual clusters will coexist concurrently, most of them having different owners. Assuming that the environment is not entirely secure and that some of the users of virtual clusters or of the underlying physical cluster might be with bad intentions, a security layer needs to be configured on top of the communication backbone such that malicious users cannot interfere with the normal execution of another user's virtual cluster.

Figure~\ref{fig:SecurityLayer} depicts our proposed security overlay on top of the communication layer.

\begin{figure}[htb]
\centering
\includegraphics[width=100mm]{res/SecurityLayer.png}
\caption{Security layer with regards to two communicating endpoints.}\label{fig:SecurityLayer}
\end{figure}

As it can be seen in Figure~\ref{fig:SecurityLayer}, we propose a scheme in which the virtual machines communicate in an unencrypted manner with their parent physical machines, but with communication being encrypted on the segment between the parent physical machines. At a closer look, we can identify this scheme as a tunneled mode where the tunnel endpoints are the physical machines and the virtual machines being part of private networks. In this regards, we can look at the physical machines as being gateways for communication between virtual machines.

There exists a number of technologies that provide this functionality in the industry with the most prevalent being OpenVPN and IPsec, which is why both need to be considered in order to choose the most appropriate performance wise and from an administrative and configuration point of view. We will give a brief introduction for both of these technologies from a theoretical point of view in the following sections.



\section{Master node}

The first step towards setting up and managing a virtual cluster is establishing a way in which configurations are sent and executed onto the physical machines of the participating physical clusters. To this extent we have decided to use a master node that keeps state on all the virtual clusters administered within the participating physical clusters. 

Figure~\ref{fig:masternode} gives a graphical representation of the master node and its role managing multiple physical sites:

\begin{figure}[htb]
\centering
\includegraphics[width=100mm]{res/MasterNode.png}
\caption{Master node's role in managing virtual clusters.}\label{fig:masternode}
\end{figure}

As with a regular cluster that has a central node that accepts job submissions, our master node will have the role of receiving virtual cluster management commands from authorized users and pushing those commands onto the managed physical clusters. Moreover, the master node is responsible for keeping a consistent state and view across all authorized users with regards to all its managed physical resources.



\section{Python Fabric tool}

The next step in order to automatically configure virtual clusters is finding a means to enable the master node to automatically push commands and configurations to its administered sites. One of the best existing options is the Fabric tool which is a Python 2.5 or higher library and command-line tool for streamlining the use of SSH for application deployment or system administration tasks~\cite{Fabfile}.

Fabric offers a suite of library commands that enables local or remote shell command execution, either normal or sudo, together with an api for uploading and downloading files to and from remote locations. Moreover, Fabric also offers more flexible functionality such as prompting for input or aborting execution.

Fabric can be used in multiple ways such as creating a Python module that contains functions to be executed via a fab command-line tool, or importing its modules in other Python code.

Examples and technical details on how to use the Fabric tool to execute a basic command (e.g. \emph{uname -s}) on a remote server can be found in Appendix A.

\section{Creation}

In order to create a fully functional virtual cluster, a number of available physical machines from within the same physical cluster need to be reserved. Once this reservation phase is completed, connectivity and security configurations need to be applied and the virtual machines need to be deployed.

Figure~\ref{fig:creation1} provides a representation of the resources that are used for the creation of virtual clusters.

\begin{figure}[htp]
\centering
\includegraphics[width=90mm]{res/Creation1.png}
\caption{Resources for virtual clusters.}\label{fig:creation1}
\end{figure}

It is important to note that we designed the virtual cluster network creation operation to allocate resources within a single physical cluster. But this restriction can be overcome by the merging operation which will be presented in the next section.

The first step that needs to be done is to make sure the virtual machines can communicate with their hosts. This can be accomplished by setting up a virtual network interface (e.g. virbr0) on the host with a fixed ip address (e.g. 192.168.0.1). All the virtual machines will now receive ip addresses in the 192.168.0.0/16 range that enables them to communicate with their hosts.

The second step is to establish the routes the packets will follow for the communication between two virtual machines. Considering that most IKE daemons (i.e. racoon) require an IP/port pair to listen to for other IKE daemons that want to setup security associations, establishing routes can be accomplished by adding additional private IP addresses to the cluster nodes as subinterfaces. In addition, for each virtual cluster that is created a single racoon instance can be started on each physical machine to handle all the security associations with all the other participants. Figure~\ref{fig:creation2} depicts these steps.

\begin{figure}[htb]
\centering
\includegraphics[width=100mm]{res/Creation2.png}
\caption{IP address assignments and packet routing.}\label{fig:creation2}
\end{figure}

Regarding the communication between two virtual machines, as Figure~\ref{fig:creation2} depicts, packets sent by a virtual machine are received via the \emph{virbr0} virtual network interface card and forwarded via the \emph{eth0:1} virtual network interface. 

From a creation process perspective, we can regard setting up a virtual cluster network as a succession of operations applied to the individual physical machines that make up the network. Applying the following operations to every cluster node that has been reserved will result in a successful configuration:

\begin{enumerate}
  \item create a subinterface (e.g. \emph{eth0:1 10.0.0.1}) used for forwarding traffic to and from virtual machines hosted on different cluster nodes. A subinterface is a division of one physical interface (e.g. \emph{eth0}) into multiple logical interfaces (e.g. \emph{eth0:1}), thus providing the capability of multiple IP addresses for the same interface;
  \item generate and add IPsec security policies with all the other virtual cluster participants;
  \item generate racoon configs and keys and start a racoon instance based on them;
  \item push forwarding firewall rules to the cluster node such that it allows traffic to and from it's virtual machine;
  \item grant internet access to the virtual machine, if requested;
  \item signal that the virtual cluster is up and running;
\end{enumerate}

%\begin{figure}[htb]
%\centering
%\includegraphics[width=90mm]{res/Creation3.png}
%\caption{Virtual cluster creation process on one machine.}\label{fig:creation3}
%\end{figure}

Finally, Figure~\ref{fig:creation4} provides a graphical representation of a resulting virtual cluster network for three virtual machines.

\begin{figure}[htb]
\centering
\includegraphics[width=120mm]{res/Creation4.png}
\caption{Three virtual machines virtual cluster.}\label{fig:creation4}
\end{figure}


The successful virtual cluster network creation is signaled to the user and the IP addresses that have been allocated will be provided. The virtual machines can be booted up using these provided IP addresses and the just deployed virtual cluster network will automatically be used. Moreover, so far the network is available as long as it is needed, until the user requests it's safe deletion. Finally, all the communication of all applications running on the virtual machines uses, by default, the just deployed virtual cluster network, and all the communication will automatically be encrypted, as requested upon creation.


\section{Merging}

In this section we will describe the design of the merge operation applied to multiple virtual clusters. We will note that the library supports virtual clusters that can span across multiple physical clusters which can be accomplished by multiple merge operations. %Moreover, the library supports two types of merging, a local merge operation and a remote merge operation. These operations and their restrictions will be described in the next two subsections.



%\subsection{Local merge}

%The local merge operation addresses the merging of two virtual clusters that reside on the same physical cluster. Some restrictions apply to this local merge operation, most importantly two virtual clusters %to merge should not share a physical cluster node. 

%For the implementation of the local merging operation we considered two major approaches: either make use of the cluster node's real IP addresses, or make use of the subinterfaces added during the creation of each individual virtual cluster. For consistency reasons, we made use of the private IP addresses given to the subinterfaces (i.e. in the 10.0.0.0/16 address range).

%\begin{figure}[htb]
%\centering
%\includegraphics[width=125mm]{res/MergingLocal.png}
%\caption{Local merge.}\label{fig:localmerge}
%\end{figure}

%Figure 4.5 denotes the merging of two local virtual clusters, each of size two, delimited by the thick vertical dotted line. Labels with the network interfaces and IP addresses used as communication endpoints are placed on the communication links and the communication links that need to be added for a successful merge operation are denoted by the thin dotted lines, all of which are IPsec tunnels. In order to setup these new IPsec tunnels, new IPsec security policies need to be created on each machine and a racoon instance needs to handle the creation of new security associations on each machine with the cluster nodes from the other virtual cluster.



%\subsection{Remote merge}

The merge operation addresses the merging of two virtual clusters that reside on different physical clusters. Some restrictions apply to this operation, most importantly all the cluster nodes from both remote clusters need to have public IP addresses. The public IP addresses are used to directly connect remote cluster nodes through IPsec tunnels. An alternative design would be to make use of gateways with public IP addresses that forward traffic between remote clusters, a solution which wouldn't require public IP addresses for the cluster nodes, but would increase the computational demand on the gateways thus creating performance bottlenecks.

An important aspect of merging two virtual clusters is handling the racoon instances in order to maintain high availability:

%\marginpar{\emph{\textbf{Andrei}: Not restart for 2 reasons ... 1) restart temporarily shuts down both virtual clusters and my design decision was towards high availability for remote merging, as stated above; 2) restart means recomputing previously computed config file parts, hence higher burden for large clusters.}}

\begin{enumerate}
  \item considering that once started, a racoon instance cannot be dynamically modified, new racoon instances need to be started for each merge operation. In this way each merge operation can be interpreted as a new layer consisting of new security policies and a new racoon instance. Another considered approach was to restart the racoon instance and append the new settings, but this would not only decrease the availability of the virtual cluster by temporarily disabling the virtual clusters but also induce a performance problem by recreating configuration file parts that were already created;
  \item each racoon instance listens for incoming connections on a dedicated IP address and port number which can be specified in a configuration file. Thus, our design relies on an unused port range provided for each machine by an administrator from which a port number is chosen for new racoon instances.
\end{enumerate}

\begin{figure}[htb]
\centering
\includegraphics[width=125mm]{res/MergingRemote.png}
\caption{Remote merge.}\label{fig:remotemerge}
\end{figure}

Figure~\ref{fig:remotemerge} denotes the merging of two virtual clusters, each of size two, residing on different physical sites. Labels with the network interfaces and IP addresses used as communication endpoints are placed on the communication links and the communication links that need to be added for a successful merge operation are denoted by dashed lines, all of which are new IPsec tunnels that require new security policies and a racoon instance for correct setup.



\section{Deletion}

After creating virtual clusters and using them, users must initiate deletion operations that are responsible with freeing resources and cleaning up configurations on the participating machines. Freeing resources is an important step towards other users that might need them and towards making sure that the physical resources aren't bloated with setups that might slow them down more and more over time.

The successful deletion of a virtual cluster requires keeping track of the configurations pushed onto each machine. These configurations include subinterface names, IP addresses, firewall rules, internet sharing rules, virtual machines deployed, IPsec security policies and most important racoon instance process identifiers. Considering that a virtual cluster might go through several merging operations, support for more than one racoon process identifier is provided.

After keeping track of all the required configurations, the virtual cluster deletion becomes a straight forward single step process: apply all the reverse configurations, in order, on each machine, as a reverse process to the virtual cluster creation.
