\begin{thebibliography}{10}

\bibitem{OpenNebulaVirtualNetworks}
Configuring opennebula virtual networks.
\newblock
  \url{http://www.opennebula.org/documentation:archives:rel1.2:howto_vnet}.

\bibitem{DynamicVirtualClustering}
Dynamic virtual clustering.
\newblock
  \url{http://papers.cluster2007.org/presentations/Cluster2007_Dynamic_Virtual%
_Clustering.pdf}.

\bibitem{ISAKMP}
The internet ip security domain of interpretation for isakmp.
\newblock \url{http://tools.ietf.org/html/rfc2407}.

\bibitem{IKE}
The internet key exchange (ike).
\newblock \url{http://tools.ietf.org/html/rfc2409}.

\bibitem{ISAKMP1}
Internet security association and key management protocol (isakmp).
\newblock \url{http://tools.ietf.org/html/rfc2408}.

\bibitem{AH}
Ip authentication header (ah).
\newblock \url{http://tools.ietf.org/html/rfc4302}.

\bibitem{ESP1}
Ip encapsulating security payload (esp).
\newblock \url{http://tools.ietf.org/html/rfc4303}.

\bibitem{ESP}
Ip encapsulating security payload (esp).
\newblock \url{http://tools.ietf.org/html/rfc2406}.

\bibitem{IPsec}
Ipsec.
\newblock \url{http://en.wikipedia.org/wiki/IPsec}.

\bibitem{LZO}
Lempel-ziv-oberhumer (lzo).
\newblock \url{http://www.oberhumer.com/opensource/lzo/}.

\bibitem{OpenNebulaManagingVirtualNetworks}
Managing virtual networks.
\newblock \url{http://www.opennebula.org/documentation:rel2.2:vgg}.

\bibitem{OpenSSL}
Openssl.
\newblock \url{http://www.openssl.org/}.

\bibitem{OpenVPN}
Openvpn.
\newblock \url{http://en.wikipedia.org/wiki/OpenVPN}.

\bibitem{Racoon}
Racoon.
\newblock \url{http://netbsd.gw.com/cgi-bin/man-cgi?racoon++NetBSD-current}.

\bibitem{SecArchInternetProtocol}
Security architecture for the internet protocol.
\newblock \url{http://tools.ietf.org/html/rfc4301}.

\bibitem{Libvirt}
The virtualization api.
\newblock \url{http://libvirt.org/}.

\bibitem{SotomayorMontero2009}
Sotomayor B., Montero R.S., Llorente I.M., and Foster I.
\newblock Virtual infrastructure management in private and hybrid clouds.
\newblock {\em IEEE Internet Computing 13}, 5:14--22, 2009.

\bibitem{BavierFeamster2006}
A.~Bavier, N.~Feamster, M.~Huang, L.~Peterson, and J.~Rexford.
\newblock In vini veritas: Realistic and controlled network experimentation.
\newblock In {\em In Proc. ACM SIGCOMM}, Pisa, Italy, August 2006.

\bibitem{BhatiaMotiwala2008}
Sapan Bhatia, Murtaza Motiwala, Wolfgang Muhlbauer, Yogesh Mundada, Vytautas
  Valancius, Andy Bavier, Nick Feamster, Larry Peterson, and Jennifer Rexford.
\newblock Trellis: a platform for building flexible, fast virtual networks on
  commodity hardware.
\newblock In {\em In Proceedings of the 2008 ACM CoNEXT Conference.}, page
  72:1–72:6, New York, NY, 2008.

\bibitem{EmenekerStanzione2006}
W.~Emeneker, D.~Jackson, J.~Butikofer, , and D.~Stanzione.
\newblock Dynamic virtual clustering with xen and moab.
\newblock {\em Workshop on XEN in HPC Cluster and Grid Computing Environments
  (XHPC)}, 2006.

\bibitem{EmenekerStanzione2007}
W.~Emeneker and D.~Stanzione.
\newblock Dynamic virtual clustering.
\newblock In {\em In Proc. Cluster}, Austin, TX, September 2007.
  \url{http://www.cca08.org/papers.php}.

\bibitem{Sotomayor2008}
B.~Sotomayor et~al.
\newblock Capacity leasing in cloud systems using the opennebula engine.
\newblock In {\em Proc. Cloud Computing and Applications 2008 (CCA 08)}.
  \url{http://www.cca08.org/papers.php}, 2008.

\bibitem{Fabfile}
Fabfile.
\newblock \url{http://docs.fabfile.org/en/1.0.1/index.html}.

\bibitem{KeaheyFreeman2008}
K.~Keahey and T.~Freeman.
\newblock Contextualization: Providing oneclick virtual clusters.
\newblock {\em eScience 2008}, 2008.

\bibitem{OpenNebula}
OpenNebula.
\newblock \url{http://opennebula.org/}.

\bibitem{RekhterMoskowitz1996}
Y.~Rekhter, B.~Moskowitz, D.~Karrengerg, G.~de~Groot, and E.~Lear.
\newblock Address allocation for private internets.
\newblock {\em RFC1918}, 1996.

\bibitem{AmazonCaseStudies}
Amazon~Case Studies.
\newblock \url{http://aws.amazon.com/solutions/case-studies/}.

\bibitem{FreemanKeahey2008}
Freeman T. and K.~Keahey.
\newblock Flying low: Simple leases with workspace pilot.
\newblock {\em EuroPar 2008}, 2008.

\bibitem{FreemanKeahey2006}
Freeman T., K.~Keahey, I.~Foster, A.~Rana, B.~Sotomayor, and F.~Wuerthwein.
\newblock Division of labor: Tools for growth and scalability of the grids.
\newblock {\em ICSOC}, 2006.

\bibitem{Tanenbaum1988}
Andrew~S. Tanenbaum.
\newblock {\em Computer Networks}.
\newblock Prentice-Hall, second edition, 1988.

\bibitem{TsugawaFortes2006}
M.~Tsugawa and J.~A.~B. Fortes.
\newblock A virtual network (vine) architecture for grid computing.
\newblock In {\em In Proc. 20th Int. Parallel and Distributed Processing
  Symp.}, 2006.

\bibitem{AmazonVPCFAQ}
Amazon VPC.
\newblock \url{http://aws.amazon.com/vpc/faqs/}.

\bibitem{AmazonVPC}
Amazon VPC.
\newblock \url{http://aws.amazon.com/vpc/}.

\end{thebibliography}
