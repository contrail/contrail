\contentsline {chapter}{Abstract}{5}
\contentsline {chapter}{\numberline {1}Introduction}{7}
\contentsline {chapter}{\numberline {2}Virtual cluster networks and related work}{9}
\contentsline {section}{\numberline {2.1}Technologies}{9}
\contentsline {subsection}{\numberline {2.1.1}OpenVPN}{9}
\contentsline {subsection}{\numberline {2.1.2}IPsec}{10}
\contentsline {subsubsection}{\numberline {2.1.2.1}Internet Key Exchange protocol}{11}
\contentsline {section}{\numberline {2.2}Related work}{11}
\contentsline {chapter}{\numberline {3}Virtual Cluster Network configuration}{15}
\contentsline {section}{\numberline {3.1}Communication and the security layer}{16}
\contentsline {section}{\numberline {3.2}Master node}{18}
\contentsline {section}{\numberline {3.3}Python Fabric tool}{19}
\contentsline {section}{\numberline {3.4}Creation}{19}
\contentsline {section}{\numberline {3.5}Merging}{22}
\contentsline {section}{\numberline {3.6}Deletion}{23}
\contentsline {chapter}{\numberline {4}Evaluation}{25}
\contentsline {section}{\numberline {4.1}Implementation performance evaluation}{26}
\contentsline {section}{\numberline {4.2}Transport mode evaluation}{27}
\contentsline {section}{\numberline {4.3}Tunnel mode evaluation}{29}
\contentsline {section}{\numberline {4.4}Scalability evaluation}{31}
\contentsline {chapter}{\numberline {5}Conclusions and future work}{35}
\contentsline {chapter}{\numberline {A}OpenVPN and IPsec configuration details}{41}
\contentsline {section}{\numberline {A.1}OpenVPN}{41}
\contentsline {subsection}{\numberline {A.1.1}The server}{41}
\contentsline {subsection}{\numberline {A.1.2}The client}{42}
\contentsline {section}{\numberline {A.2}IPsec}{44}
\contentsline {subsection}{\numberline {A.2.1}Environment}{44}
\contentsline {subsection}{\numberline {A.2.2}First configuration steps}{44}
\contentsline {subsection}{\numberline {A.2.3}Configuring the environment for IPSec}{46}
\contentsline {section}{\numberline {A.3}Python Fabric tool usage}{49}
\contentsline {chapter}{\numberline {B}Python tool usage}{51}
\contentsline {section}{\numberline {B.1}Creation}{51}
\contentsline {section}{\numberline {B.2}Merging}{52}
\contentsline {section}{\numberline {B.3}Deletion}{52}
\contentsline {chapter}{\numberline {C}Resource management}{53}
\contentsline {section}{\numberline {C.1}Adding a new site}{53}
\contentsline {section}{\numberline {C.2}Removing a site}{53}
\contentsline {section}{\numberline {C.3}Adding a new cluster node to a site}{53}
\contentsline {section}{\numberline {C.4}Removing a cluster node from a site}{55}
