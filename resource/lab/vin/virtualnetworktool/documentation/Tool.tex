\chapter{Secure Virtual Network in VIN}{
Chapter~\ref{chap:svn} and Chapter~\ref{chap:external} explains the technology that can be used to build the secure virtual network in VIN. We use IPSec to build the secure tunnels in the network. The reasons for the choice of IPSec is explained in Section~\ref{sec:tech}. IPSec creates end to end tunnels between the virtual machines. VIN should be able to create, delete and manage these tunnels easily and efficiently. The management of the virtual network can be done by a master node which keeps track of the configuration information and the states of the virtual networks. Master node is a well known node in the VIN which is responsible for configuring other nodes in the virtual cluster consisting of virtual machines existing in one or more cloud service providers. We call these nodes as cluster nodes. When an application needs to make use of a cluster, the Contrail will communicate with the VIN which in turn makes use of master node to configure the network. The communication between the master and the cluster nodes during configuration should be secure. The cluster nodes need not be aware of the existence of the master node. VIN should be able to check the status of the virtual network whenever required. The master node should also make sure that the states of nodes maintained by it is consistent. Since IPSec does not have a client server architecture, there is no order in which configuration of nodes should be done and hence we can configure all the nodes in parallel and later bring the tunnels up. Master node has to manage the phase 1 ISAKMP SA keys of the IPSec tunnels\footnote{For more explanation read Chapter~\ref{chap:svn}}. However the data communication through the tunnel is encrypted by IPSec SA keys which is negotiated by the participating end points and hence there is no risk of a compromised master node.


\begin{figure}[htb]
\includegraphics[scale=.75]{figures/pytoolarchi.pdf}
\caption{Highlevel architecture of VIN}
\label{fig:pytoolarchi}
\end{figure}

\section{Python tool}
We created a tool which can automatically configure virtual network in DAS-4 cluster and external cloud service provider such as Amazon EC2 and Rackspace. The high level architecture of a deployment scenario is shown in Figure~\ref{fig:pytoolarchi}. The tool is written in python and shell script. The master node logs into the cluster node as a root(sudo) user and configure each node to use IPSec. It will then start the IPSec network with the encryption specified in the network definition. The tool will create host to host IPSec tunnel between each pair of nodes in the network. We assume the communication between the nodes in Rackspace and Amazon EC2 is secure and hence the python tool will not create the tunnels between them. This will avoid the overhead of using an IPSec tunnel for communication within the nodes in Rackspace and Amazon EC2. 

The python tool exists in the master node. The tool expects the cluster nodes to be up and ready and requires the following information from the VIN for the configuration of the network tunnels.
\begin{itemize}
\item Node IP address which is accessible from other cluster nodes
\item Encryption information of the IPSec tunnels
\item Username and SSH keys which will allow master node to login to the nodes
\end{itemize}

The VIN should first define the virtual cluster by specifying to the master the list of node IP address in the virtual network and its properties. The cluster nodes should be able to receive the ESP packets used by IPSec. The tools should also have NAT information of the cluster nodes. The properties include the keys required for authentication of master nodes, algorithms to use for encryption and other properties of IPSec mentioned in chapter~\ref{chap:svn} and chapter~\ref{chap:external}.  There are three types of keys used by the tool to establish the secure virtual network.
\begin{itemize}
\item SSH keys - Used by the master node login to cluster nodes for configuration and management. The identity file (private key) of the cluster nodes is copied to master node. Public key exists in the cluster nodes. The master node should be authorized to login to the cluster node with root/sudo permission.
\item ISAKMP SA - Used by IPSec for the authentication of endpoints during key exchange. Currently the python tool uses hard coded shared secrets which copied to the cluster nodes by the master node. This key is only used for key exchanges and hence there is not much of security concern. However the security can be further improved by generating rsa keys for each tunnel endpoints. This is left as a future work
\item IPSec key - Used for the actual encryption of data. This key is managed by the IPSec implementation and is stored in the SPD situated in the participating end points. This key is also refreshed frequently.
\end{itemize}

If the algorithm for encryption is not specified, the tool will use AES which is the fastest among the encryption algorithms. The master node needs root access to configure the cluster nodes. Authentication of master node is done by using SSH keys. In case of a sudoer we need to make sure that the user has password less sudoer access to avoid interaction from user. The keys can be defined in by using the `addkey' parameter. With `addkey' we specify both username and the SSH key associated with the username. This will return a key id. The virtual cluster can be defined using `define' parameter of the of the tool. The key id obtained from the `addkey' should be specified along with the node in the list. The key and the node address should be separated by `::'. For example if the key id is 9342 and the IP address of the node is 10.11.0.1 then the node should be specified as 10.11.0.1::9342. Once the cluster is defined the tool will return the network id. Network id is used by the VIN to start and stop a virtual cluster. To start the virtual cluster VIN makes use of the 'start' parameter of the tool. Configuration includes creating required files for implementing the virtual network using IPSec and executing user space tools to create the SPD and SAD entries. Nodes in the VIN can discover each other by checking the tunnel endpoint information which is stored in the pytool.conf (Default path - $\sim$/.config/) of the nodes.

Python tool uses fabric library~\cite{fabfile}. Fabric is used for streamlining the use of SSH for application deployment or systems administration tasks. Fabric is used to copy some configuration files and executing commands remotely. Fabric tool in the current version is not executed in parallel since the current version doesn't support parallel execution by using api calls. However a recent version allows parallel execution using API by using simple decorators (@run\_once)~\cite{fabric}. Since fabric is based on SSH, the communication between master and the cluster nodes is secure.


Some of the major functions are listed below

\begin{itemize}
\item Define security keys - This refers to the ssh keys that are used for login. eg: key.pem of EC2
\begin{itemize}
\item Example: pytool addkey \textit {keydetails}
\item Output: keyid
\end{itemize}
\item Define a network - This is used to create a network definition. It defines nodes and encrption algorithms to use.
\begin{itemize}
\item Example: pytool define -n \textit {[ip1::keyid,ip2::keyid]} -e \textit{blowfish}
\item Output: Network id
\end{itemize}
\item Create a network - This starts the IPsec network with the nodes and encryption algorithm specified in the definition.
\begin{itemize}
\item Example: pytool start -i \textit{Network id} [--verbose] 
\end{itemize}
\item Test network - This command will test the network it will ping each other and show packet loss %
\begin{itemize}
\item Example: pytool test -i \textit{Network id} [--verbose]
\item Output: Unreachable IPaddress, latency of reachable nodes.
\end{itemize}
\item Stop a network - This command stops the network
\begin{itemize}
\item Example: pytool stop -i \textit{Network id} [--verbose]
\end{itemize}
\item Add a node - This command will add a node to the network (one at a time)
\begin{itemize}
\item Example: pytool addnode -i \textit{Network id} -n \textit{ip1::keyid1} [or -x \textit{ip2::keyid2}] [--verbose]
\end{itemize}
\item Remove a node - This command will remove a node from the network (one at a time). Keyid is optional.
\begin{itemize}
\item Example: pytool remnode -i \textit{Network id} -n \textit{ip1[::keyid1]} [or -x \textit{ip2::keyid2}] [--verbose]
\end{itemize}
\item Network status - This command will check if IPSec is up on the nodes.
\begin{itemize}
\item Example: pytool status -i \textit{Network id} [--verbose]
\item Output: Shows the nodes in which IPSec is not running.
\end{itemize}
\item Remove network - Removes the network definition
\begin{itemize}
\item Example: pytool remove -i \textit{Network id}
\end{itemize}
\item Show network - Shows the encryption algorithm and nodes of the network
\begin{itemize}
\item Example: pytool show -a [or -i \textit{Network id}]
\item Output: Node and encryption algorithm used
\end{itemize}
\end{itemize}

