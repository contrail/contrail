'''
This code is executed by the Master node. Master node if the node which is aware of the
available Virtual Machines and the network configuration. Master nide has the list of
nodes in a network and the functions will create an *all-to-all* IPSec connections.

This code assumes the following
1. The Master has the list of nodes in the network and their public IP address.
2. The Master has password less access to the nodes. This is done by saving public key in the nodes.
   (Steps explained later)
3. The following lines exist in /etc/sudoers if Master has no root access. [TODO: There are more limitations
   like the username should be configurable. Currently all the nodes have same username] 
   user ALL=NOPASSWD: /bin/bash
   user ALL=NOPASSWD: /usr/sbin/racoon
   user ALL=NOPASSWD: /usr/sbin/setkey
   user ALL=NOPASSWD: /usr/bin/nohup
4. Currently this script will only work in GNU/Linux OS. This script will be tested in Debian, Redhat, Ubuntu and CentOS
   To add support for other OS edit config.xml file
'''

#TODO Package installation should be configured for atleast popular GNU/Linux OS

from __future__ import with_statement
from fabric.api import *
from fabric.contrib.console import confirm
from fabric.api import env, run
from fabric.contrib.files import *
from fabric.colors import *

from lxml import etree
import string

env.hosts = ['130.37.31.76','145.116.228.141']#'192.168.122.179']#,'145.116.228.141']
spd_template= "spdadd LOCAL_VPN_ADDRESS REMOTE_VPN_ADDRESS any -P out ipsec\n\
    esp/tunnel/LOCAL_ADDRESS-REMOTE_ADDRESS/require;\n\
    \n\
    spdadd REMOTE_VPN_ADDRESS LOCAL_VPN_ADDRESS any -P in ipsec\n\
    esp/tunnel/REMOTE_ADDRESS-LOCAL_ADDRESS/require;"

psk_template='REMOTE  KEY'

class Deploy:
#def localhost():
    
    __remote_conf_path= '~/config'
    __ipsec_conf='ipsec-tools.conf'
    __racoon_conf='racoon.conf'
    __psk_conf='psk.txt'
    __psk_key='a9993e364706816aba3e' #TODO: Certificates



    #HELPER FUNCTIONS#

    def __getOperatingSystem(self,issue):
        str=string.split(issue)
        return str[0]

    
    def __getCommand(self,command,operating_system):

        tree=etree.parse('config.xml')
        root=tree.getroot()
        #inst=root.find("install")
        #debinst=inst.find("debian")
        operating_system=string.lower(operating_system)
        str="commands/%s/%s"%(command,operating_system)
        return root.find(str).text
        

    def __getRacoonInstall(self,issue):
        return "%sracoon"%(self.__getCommand("install",self.__getOperatingSystem(issue)))

    def __getSetKeyInstall(self,issue):
        return "%sipsec-tools"%(self.__getCommand("install",self.__getOperatingSystem(issue)))
    
    def __getPingCommand(self,issue):
        return self.__getCommand("ping",self.__getOperatingSystem(issue))

    #env.key_filename = ['/home/sajith/.ssh/id_dsa']
    #env.password = ['']


    def __createSPDEntry(self,source,destination,source_vpn,destination_vpn):
        spd_template_cpy=spd_template
        spd_template_cpy=spd_template_cpy.replace("LOCAL_VPN_ADDRESS",source_vpn)
        spd_template_cpy=spd_template_cpy.replace('REMOTE_VPN_ADDRESS',destination_vpn)
        spd_template_cpy=spd_template_cpy.replace('LOCAL_ADDRESS',source_vpn)
        spd_template_cpy=spd_template_cpy.replace('REMOTE_ADDRESS',destination)
        return spd_template_cpy

    def __createPSK(self,remote,key):
        psk_template_cpy=psk_template
        psk_template_cpy=psk_template_cpy.replace("REMOTE",remote)
        psk_template_cpy=psk_template_cpy.replace('KEY',key)
        return psk_template_cpy

    def __configIPSec(self,encryalgo):
    
    #Package installation
        print(green("* Installing required packages"))
        with settings(hide('stdout'),warn_only=True):
            out = sudo('which racoon'); #Or use dpkg -s racoon (for debian). But slower and needs to consider other OS
        if (out.failed):
            print(red("* Racoon not installed. Installing"))
            issue=sudo('cat /etc/issue') #Not an appropriate assumption.
            print(green("* Found Operating System %s"%(self.__getOperatingSystem(issue))))
            sudo(self.__getRacoonInstall(issue))
     
        with settings(hide('stdout'),warn_only=True):
            out = sudo('which setkey'); #Or use dpkg -s ipsec-tools (for debian). But slower and needs to consider other OS
        if (out.failed):
            print(red("* Setkey not installed. Installing"))
            issue=sudo('cat /etc/issue') #Not an appropriate assumption.
            print(green("* Found Operating System %s"%(self.__getOperatingSystem(issue))))
            sudo(self.__getSetKeyInstall(issue))   
        
        #Copying configuration files
        print(green("* Copying configuration files"))
        if exists(os.path.expanduser(self.__remote_conf_path)):
            print(cyan("* Configuration directory exists"))
        else:
            print(red("* Configuration directory does not exist. Creating."))
            run('mkdir %s'%(self.__remote_conf_path)) #handle sudo mkdir
        
        #What if these files already exist??? HANDLE
    
        put(self.__ipsec_conf, os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__ipsec_conf)))
        put(self.__racoon_conf, os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__racoon_conf)))
        put(self.__psk_conf, os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__psk_conf)),True) #TRUE for sudo, psk is owned by root so if it already exist then it cannot be replaced unless it is root

        for destination in env.hosts:
            if(destination!=env.host_string):
                template=self.__createSPDEntry(env.host_string,destination,env.host_string,destination)
                append(os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__ipsec_conf)),template)
                psk_entry=self.__createPSK(destination,self.__psk_key)
                append(os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__psk_conf)),psk_entry)
    
        sed(os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__racoon_conf)),'PATH_TO_PSK',os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__psk_conf)))
        print("Encry %s"%(encryalgo))
        sed(os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__racoon_conf)),'ENCRY_ALGO',encryalgo)

        #Change ownership and permission of pskey (I DONT LIKE THIS)
    
        sudo('chmod 600 %s/%s'%(os.path.expanduser(self.__remote_conf_path),self.__psk_conf))
        sudo('chown root %s/%s'%(os.path.expanduser(self.__remote_conf_path),self.__psk_conf))
    
        #Starting Racoon and Setting up SPD
    
        sudo('setkey -f %s/%s'%(os.path.expanduser(self.__remote_conf_path),self.__ipsec_conf))
        sudo('nohup racoon -f %s/%s'%(os.path.expanduser(self.__remote_conf_path),self.__racoon_conf),shell=False)
    
    def __addIPSec(self,encryalgo):
    
    #Package installation
        print(green("* Installing required packages"))
        with settings(hide('stdout'),warn_only=True):
            out = sudo('which racoon'); #Or use dpkg -s racoon (for debian). But slower and needs to consider other OS
        if (out.failed):
            print(red("* Racoon not installed. Installing"))
            issue=sudo('cat /etc/issue') #Not an appropriate assumption.
            print(green("* Found Operating System %s"%(self.__getOperatingSystem(issue))))
            sudo(self.__getRacoonInstall(issue))
     
        with settings(hide('stdout'),warn_only=True):
            out = sudo('which setkey'); #Or use dpkg -s ipsec-tools (for debian). But slower and needs to consider other OS
        if (out.failed):
            print(red("* Setkey not installed. Installing"))
            issue=sudo('cat /etc/issue') #Not an appropriate assumption.
            print(green("* Found Operating System %s"%(self.__getOperatingSystem(issue))))
            sudo(self.__getSetKeyInstall(issue))   
        
        #Copying configuration files
        print(green("* Copying configuration files"))
        if exists(os.path.expanduser(self.__remote_conf_path)):
            print(cyan("* Configuration directory exists"))
        else:
            print(red("* Configuration directory does not exist. Creating."))
            run('mkdir %s'%(self.__remote_conf_path)) #handle sudo mkdir
        
        #What if these files already exist??? HANDLE
    
        put(self.__ipsec_conf, os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__ipsec_conf)))
        put(self.__racoon_conf, os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__racoon_conf)))
        put(self.__psk_conf, os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__psk_conf)),True) #TRUE for sudo, psk is owned by root so if it already exist then it cannot be replaced unless it is root

        for destination in env.hosts:
            if(destination!=env.host_string):
                template=self.__createSPDEntry(env.host_string,destination,env.host_string,destination)
                append(os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__ipsec_conf)),template)
                psk_entry=self.__createPSK(destination,self.__psk_key)
                append(os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__psk_conf)),psk_entry)
    
        sed(os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__racoon_conf)),'PATH_TO_PSK',os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__psk_conf)))
        print("Encry %s"%(encryalgo))
        sed(os.path.expanduser('%s/%s'%(self.__remote_conf_path,self.__racoon_conf)),'ENCRY_ALGO',encryalgo)

        #Change ownership and permission of pskey (I DONT LIKE THIS)
    
        sudo('chmod 600 %s/%s'%(os.path.expanduser(self.__remote_conf_path),self.__psk_conf))
        sudo('chown root %s/%s'%(os.path.expanduser(self.__remote_conf_path),self.__psk_conf))
    
        #Starting Racoon and Setting up SPD
    
        sudo('setkey -f %s/%s'%(os.path.expanduser(self.__remote_conf_path),self.__ipsec_conf))
        sudo('nohup racoon -f %s/%s'%(os.path.expanduser(self.__remote_conf_path),self.__racoon_conf),shell=False)

    def __stopIPSec(self): #TODO: We should not flush the whole database and kill all the racoon instances
        print(green("* Flusing SPD"))
        sudo('setkey -c << EOF\nspdflush;\nflush;\nEOF')
        #sudo('flush')
        print(green("* Killing Racoon Instances"))#To avoid unnecessary traffic
        sudo('pkill racoon')
        
    #TODO check IPSEC between individual nodes
    def __showStatus(self):
        stat=sudo('setkey -D')
        if stat == "No SAD entries.":
            return False,env.host_string
        return True,env.host_string

    def __testStatus(self):
        issue=sudo('cat /etc/issue')
        result=[]
        for destination in env.hosts:
            if(destination!=env.host_string):
                stat=sudo("%s %s"%(self.__getPingCommand(issue),destination))
                right=stat.rfind('%')
                left=stat.rfind(' ',0,right)
                loss=stat[left:right]
                loss=loss.strip()
                loss=loss.strip('(') #For windows ping. Not tested
                loss=int(loss)
                temp=[env.host_string,destination,loss]
                result.append(temp)
        
        return result
        
    
    def configIPSec(self,encryalgo,verbose):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__configIPSec(encryalgo)
        else:
            return self.__configIPSec(encryalgo)
    
    def stopIPSec(self,verbose):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__stopIPSec()
        else:
            return self.__stopIPSec()
        
    def showStatus(self,verbose):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__showStatus()
        else:
            return self.__showStatus()
    def testStatus(self,verbose):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__testStatus()
        else:
            return self.__testStatus()