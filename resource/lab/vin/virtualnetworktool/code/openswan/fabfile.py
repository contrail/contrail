'''
This code is executed by the Master node. Master node if the node which is aware of the
available Virtual Machines and the network configuration. Master nide has the list of
nodes in a network and the functions will create an *all-to-all* IPSec connections.

This code assumes the following
1. The Master has the list of nodes in the network and their public IP address.
2. The Master has password less access to the nodes. This is done by saving public key in the nodes.
   (Steps explained later)
3. The following lines exist in /etc/sudoers if Master has no root access. [TODO: There are more limitations
   like the username should be configurable. Currently all the nodes have same username] 
   user ALL=NOPASSWD: /bin/bash
   user ALL=NOPASSWD: /usr/sbin/racoon
   user ALL=NOPASSWD: /usr/sbin/setkey
   user ALL=NOPASSWD: /usr/bin/nohup
4. Currently this script will only work in GNU/Linux OS. This script will be tested in Debian, Redhat, Ubuntu and CentOS
   To add support for other OS edit config.xml file
'''

#TODO Package installation should be configured for atleast popular GNU/Linux OS

from __future__ import with_statement
from fabric.api import *
from fabric.contrib.console import confirm
from fabric.api import env, run
from fabric.contrib.files import *
from fabric.colors import *

from lxml import etree
import string

#env.hosts = ['130.37.31.76','145.116.228.141']#'192.168.122.179']#,'145.116.228.141']
ipsec_template= "conn hCONNAME \n\
    left=DEFAULT\n\
    leftsubnet=EC2/32\n\
    leftid=EC2\n\
    leftsourceip= EC2\n\
    #leftrsasigkey=0sAQPAOojQvMkIZKcfohMXzesrEs689XD4DeqK1znd1f8XKjaRqO+EddDmJjcN/pYplFD6RTTK/KBFZPLEzaX7E51NRixYhbYEdCsKKFWMWeoG70PgC/m/TbuNhWHCEEzqid2V4EDs+/ADyWBHLeSrtQkMdG4HtC4l9I6XFSix8tSI0v1xep5f1S10EeGyoXGbCc4B/H/v0RSsXbGZaP1EzrireFrwMiWGoqogQ+QgHw0zQf5ILL9ecBsl0OyNLvnK+ipJvUV/x8HwEd9upHryHba1EnMNc6njs/i46LeKXD1ttWwtfi+HyefEXEo59R1N9ZARzi+yenSPTBh8oKIOl6Slp2x3kNDJ6vwMiTCjRauqU+hR\n\
    leftnexthop=DEFAULT\n\
    \n\
    right=REMOTE # Remote IP address  \n\
    rightid=REMOTE\n\
    rightsourceip=REMOTE\n\
    #rightrsasigkey=0sAQN2l1ABuJnM0Tr5dhtb1paaOki9zyp+XaGwmxcZeiGBqsQcRgsO4WWsRaabFBWkM+b5CQWPR8t6OQv09ch83k9dZxC6xI94d34vWRQ6v0NgYfwg9BK/aVwnMq5hz74vvwGDE0py8zv2DGPBmpW3NDR9MRWmRoc6R3UzoG6Oo5YUiq5N8HZUsXuxhXXsH8l+zPxp0orizfQtjekMwSd7bgym9gYjIQJZKpyDG5hxJcp/h9cHUsxHniqkT/WpjJu5OlHKiKaD3JNpLg63Qd7/jNqVwWn4dK26FZGMDnTChrzpdeHQR1R0ELHdZuuS7gHZkE6ujP5ck6KOUFO5Yu5bo7T2+aIcPs2o7+hXUS8HD8iY6lo3\n\
    rightnexthop=REMOTE\n\
    \n\
    connaddrfamily=ipv4\n\
    ike=aes128\n\
    esp=aes128\n\
    auto=add\n\
    pfs=yes\n\
    forceencaps=yes\n\
    type=tunnel\n\
    authby=secret"
    
spd_template= "spdadd LOCAL_VPN_ADDRESS REMOTE_VPN_ADDRESS any -P out ipsec\n\
    esp/tunnel/LOCAL_ADDRESS-REMOTE_ADDRESS/require;\n\
    \n\
    spdadd REMOTE_VPN_ADDRESS LOCAL_VPN_ADDRESS any -P in ipsec\n\
    esp/tunnel/REMOTE_ADDRESS-LOCAL_ADDRESS/require;"

psk_template='LOCAL %any : PSK \"abc\"'

class Deploy:
#def localhost():
    
    __remote_conf_path= '~/config'
    __remote_conf_path1= '' #Workaround for Bug #323 in fabric
    __pytool_conf= 'pytool.conf'
    __ipsec_conf= 'ipsec.tar'
    __ipsec_conf_file= 'ipsec.conf'
    __ipsec_ec2_conf= 'ipsec-ec2.tar'
    #__racoon_conf= 'racoon.conf'
    __psk_conf='ipsec.secret'
    __psk_conf2='ipsec.secrets'
    #__psk_key='a9993e364706816aba3e' #TODO: Certificates



    #HELPER FUNCTIONS#

    def __getOperatingSystem(self,issue):
        str=string.split(issue)
        return str[0]

    
    def __getCommand(self,command,operating_system):

        tree=etree.parse('config.xml')
        root=tree.getroot()
        #inst=root.find("install")
        #debinst=inst.find("debian")
        operating_system=string.lower(operating_system)
        str="commands/%s/%s"%(command,operating_system)
        return root.find(str).text
        

    def __getPingCommand(self,issue):
        return self.__getCommand("ping",self.__getOperatingSystem(issue))

    #env.key_filename = ['/home/sajith/.ssh/id_dsa']
    #env.password = ['']


    def __createSPDEntry(self,source,destination,source_vpn,destination_vpn):
        spd_template_cpy=spd_template
        spd_template_cpy=spd_template_cpy.replace("LOCAL_VPN_ADDRESS",source_vpn)
        spd_template_cpy=spd_template_cpy.replace('REMOTE_VPN_ADDRESS',destination_vpn)
        spd_template_cpy=spd_template_cpy.replace('LOCAL_ADDRESS',source_vpn)
        spd_template_cpy=spd_template_cpy.replace('REMOTE_ADDRESS',destination)
        return spd_template_cpy
    
    def __createConfEntry(self,left,right,ec2):
        ipsec_template_cpy=ipsec_template
        ipsec_template_cpy=ipsec_template_cpy.replace("EC2",left)
        if ec2==True:
            ipsec_template_cpy=ipsec_template_cpy.replace("DEFAULT","%defaultroute")
        else:
            ipsec_template_cpy=ipsec_template_cpy.replace("DEFAULT",left)
            ipsec_template_cpy=ipsec_template_cpy.replace("leftnexthop=","#leftnexthop=")
        ipsec_template_cpy=ipsec_template_cpy.replace('REMOTE',right)
        conname="%s-%s"%(left,right)
        ipsec_template_cpy=ipsec_template_cpy.replace('CONNAME',conname)
        return ipsec_template_cpy

    def __createPSK(self,local,key):
        psk_template_cpy=psk_template
        psk_template_cpy=psk_template_cpy.replace("LOCAL",local)
        #psk_template_cpy=psk_template_cpy.replace('KEY',key)
        return psk_template_cpy
          

  
    
    def __configIPSec(self,encryalgo,id):
    
    #Package installation
        print(green("* Checking installed packages"))
        with settings(hide('stdout'),warn_only=True):
            which_ipsec = sudo('which ipsec'); #Or use dpkg -s racoon (for debian). But slower and needs to consider other OS
        if (which_ipsec.failed):
            print(red("* Openswan not installed. You should Install Openswan to continue"))
            return

        if self.__remote_conf_path[0]=='~':
            self.__remote_conf_path1="%s%s"%(sudo('pwd'),self.__remote_conf_path[1:])
        else:
            self.__remote_conf_path1=self.__remote_conf_path
        print(green("* Copying configuration files"))
        print(self.__remote_conf_path1)
        if exists(self.__remote_conf_path1):
            print(cyan("* Configuration directory exists"))
        else:
            print(red("* Configuration directory does not exist. Creating."))
            run('mkdir %s'%(self.__remote_conf_path1)) #handle sudo mkdir

        if env.ec2==False:
             #What if these files already exist??? HANDLE
    
            put(self.__ipsec_conf, '%s/%s'%(self.__remote_conf_path1,self.__ipsec_conf))
            #put(self.__racoon_conf,'%s/%s'%(self.__remote_conf_path1,self.__racoon_conf))
            #put(self.__psk_conf, '%s/%s'%(self.__remote_conf_path1,self.__psk_conf),True) #TRUE for sudo, psk is owned by root so if it already exist then it cannot be replaced unless it is root
            sudo('tar -xvf %s/%s -C %s'%(self.__remote_conf_path1,self.__ipsec_conf,self.__remote_conf_path1))
            if not exists("%s/firstrun"%(self.__remote_conf_path1)):
                #TODO NAT CHECKING
                comment("%s/%s"%(self.__remote_conf_path1,self.__ipsec_conf_file), "nat_traversal=yes", use_sudo=True, char='#', backup='.bak')
                sudo('cp %s/%s /etc/'%(self.__remote_conf_path1,self.__ipsec_conf_file))
                 #sudo("/etc/init.d/ipsec stop")
                sudo("nohup /etc/init.d/ipsec restart",shell=False)
                sudo('echo ""> %s/firstrun'%(self.__remote_conf_path1))
            #TODO: NEED TO MODIFY IF WE ARE USING DIFFERENT KEYS
            #sudo('/bin/cp -rf %s/ipsec.d %s/ipsec.secret* /etc/'%(self.__remote_conf_path1,self.__remote_conf_path1))
            #sudo('/bin/cp -rf %s/ipsec.secret /etc/ipsec.d/ipsec.secrets'%(self.__remote_conf_path1))
            
            host_left=True
            for destination in env.hosts:
                if(destination!=env.host_string):
                    if host_left==True:
                        template=self.__createConfEntry(env.host_string,destination,env.ec2)
                        conname='h%s-%s'%(env.host_string,destination)
                    else:
                        template=self.__createConfEntry(destination,env.host_string,env.ec2)
                        conname='h%s-%s'%(destination,env.host_string)
                    conffile='%s/%s'%(self.__remote_conf_path1,self.__ipsec_conf_file)
                    append("%s"%conffile,template,use_sudo=True)
                    append("%s/%s_%s"%(self.__remote_conf_path1,self.__pytool_conf,id),conname)
                    psk=self.__createPSK(env.host_string,"abc")
                    pskfile1="/etc/%s"%(self.__psk_conf)
                    pskfile2="/etc/%s"%(self.__psk_conf2)
                    append("%s"%pskfile1,psk,use_sudo=True)
                    append("%s"%pskfile2,psk,use_sudo=True)
                    sudo("%s/ch.sh"%self.__remote_conf_path1)
                    sudo('nohup %s auto --rereadall'%which_ipsec,shell=False)
                    sudo('nohup %s auto --config %s --replace %s'%(which_ipsec,conffile,conname),shell=False)
                    sudo('nohup %s auto --config %s --asynchronous --up %s'%(which_ipsec,conffile,conname),shell=False)
                    print('ipsec auto --config %s --asynchronous --up %s'%(conffile,conname))
                else:
                    host_left=False
            for destination in env.ec2hosts:
                template=self.__createConfEntry(destination,env.host_string,env.ec2)
                conffile='%s/%s'%(self.__remote_conf_path1,self.__ipsec_conf_file)
                append("%s"%conffile,template,use_sudo=True)
                conname='h%s-%s'%(destination,env.host_string)
                append("%s/%s_%s"%(self.__remote_conf_path1,self.__pytool_conf,id),conname)
                psk=self.__createPSK(env.host_string,"abc")
                pskfile1="/etc/%s"%(self.__psk_conf)
                pskfile2="/etc/%s"%(self.__psk_conf2)
                append("%s"%pskfile1,psk,use_sudo=True)
                append("%s"%pskfile2,psk,use_sudo=True)
                sudo("%s/ch.sh"%self.__remote_conf_path1)
                sudo('nohup %s auto --rereadall'%which_ipsec,shell=False)
                sudo('nohup %s auto --config %s --replace %s'%(which_ipsec,conffile,conname),shell=False)
                sudo('nohup %s auto --config %s --asynchronous --up %s'%(which_ipsec,conffile,conname),shell=False)
                print('ipsec auto --config %s --asynchronous --up %s'%(conffile,conname))
        else:
            put(self.__ipsec_ec2_conf, '%s/%s'%(self.__remote_conf_path1,self.__ipsec_ec2_conf))
            sudo('tar -xvf %s/%s -C %s'%(self.__remote_conf_path1,self.__ipsec_ec2_conf,self.__remote_conf_path1))
            #sudo('/bin/cp -rf %s/ipsec.d %s/ipsec.secret* /etc/'%(self.__remote_conf_path1,self.__remote_conf_path1))
            if not exists("%s/firstrun"%(self.__remote_conf_path1)):
                #TODO NAT CHECKING
                #uncomment("%s/%s"%(self.__remote_conf_path1,self.__ipsec_conf_file), "nat_traversal=yes", use_sudo=True, char='#', backup='.bak')
                sudo('cp %s/%s /etc/'%(self.__remote_conf_path1,self.__ipsec_conf_file))
                #sudo("/etc/init.d/ipsec stop")
                sudo("nohup /etc/init.d/ipsec restart",shell=False)
                sudo('echo "" > %s/firstrun'%(self.__remote_conf_path1))
            for destination in env.hosts:
                template=self.__createConfEntry(env.host_string,destination,env.ec2)
                conffile='%s/%s'%(self.__remote_conf_path1,self.__ipsec_conf_file)
                append("%s"%conffile,template,use_sudo=True)
                conname='h%s-%s'%(env.host_string,destination)
                append("%s/%s_%s"%(self.__remote_conf_path1,self.__pytool_conf,id),conname)
                psk=self.__createPSK(env.host_string,"abc")
                pskfile1="/etc/%s"%(self.__psk_conf)
                pskfile2="/etc/%s"%(self.__psk_conf2)
                append("%s"%pskfile1,psk,use_sudo=True)
                append("%s"%pskfile2,psk,use_sudo=True)
                sudo("%s/ch.sh"%self.__remote_conf_path1)
                sudo('nohup %s auto --rereadall'%which_ipsec,shell=False)
                sudo('nohup %s auto --config %s --replace %s'%(which_ipsec,conffile,conname),shell=False)
                sudo('nohup %s auto --config %s --asynchronous --up %s'%(which_ipsec,conffile,conname),shell=False)
                print('ipsec auto --config %s --asynchronous --up %s'%(conffile,conname))
    
    def __stopIPSec(self,id): #TODO: We should not flush the whole database and kill all the racoon instances
        print(green("* Stopping Ipsec"))
        if self.__remote_conf_path[0]=='~':
            self.__remote_conf_path1="%s%s"%(sudo('pwd'),self.__remote_conf_path[1:])
        else:
            self.__remote_conf_path1=self.__remote_conf_path
        file="%s/%s_%s"%(self.__remote_conf_path1,self.__pytool_conf,id)
        conffile='%s/%s'%(self.__remote_conf_path1,self.__ipsec_conf_file)
        
        which_ipsec=sudo('which ipsec')
        while exists(file):
            conname=run("%s/read.sh %s"%(self.__remote_conf_path1,file))
            sudo('%s auto --config %s --asynchronous --down %s'%(which_ipsec,conffile,conname))
            sudo('%s auto --config %s --asynchronous --delete %s'%(which_ipsec,conffile,conname))
 
    def __removeNode(self,node,id):
        if self.__remote_conf_path[0]=='~':
            self.__remote_conf_path1="%s%s"%(sudo('pwd'),self.__remote_conf_path[1:])
        else:
            self.__remote_conf_path1=self.__remote_conf_path
        file="%s/%s_%s"%(self.__remote_conf_path1,self.__pytool_conf,id)
        conffile='%s/%s'%(self.__remote_conf_path1,self.__ipsec_conf_file)
        sudo('cat %s |grep -E %s> %s_stop'%(file,node,file))
        sudo('cat %s |grep -Ev %s >%s.tmp'%(file,node,file))
        sudo('mv -f %s.tmp %s'%(file,file))
        which_ipsec=sudo('which ipsec')
        while exists("%s_stop"%(file)):
            conname=run("%s/read.sh %s_stop"%(self.__remote_conf_path1,file))
            sudo('%s auto --config %s --asynchronous --down %s'%(which_ipsec,conffile,conname))
            sudo('%s auto --config %s --asynchronous --delete %s'%(which_ipsec,conffile,conname))

    #TODO check IPSEC between individual nodes
    def __showStatus(self,id):
        #stat=sudo('setkey -D')
        down=[]
        if self.__remote_conf_path[0]=='~':
            self.__remote_conf_path1="%s%s"%(sudo('pwd'),self.__remote_conf_path[1:])
        else:
            self.__remote_conf_path1=self.__remote_conf_path
        file="%s/%s_%s"%(self.__remote_conf_path1,self.__pytool_conf,id)
        sudo("cp -fr %s %s_temp"%(file,file))
        file="%s/%s_%s_temp"%(self.__remote_conf_path1,self.__pytool_conf,id)
        conffile='%s/%s'%(self.__remote_conf_path1,self.__ipsec_conf_file)
        which_ipsec=sudo('which ipsec')
        while exists(file):
            connname=run("%s/read.sh %s"%(self.__remote_conf_path1,file))
            stat=sudo("%s auto --config %s --status|grep \"IPsec SA established\" |grep %s"%(which_ipsec,conffile,connname))
            if stat == "":
                down.append(connname)
            
        #if stat == "No SAD entries.":
        #    return False,env.host_string
        return down,env.host_string

    def __testStatus(self,othernodes):
        issue=sudo('cat /etc/issue')
        result=[]
        for destination in env.hosts:
            if(destination!=env.host_string):
                #stat=sudo("%s %s"%(self.__getPingCommand(issue),destination))
                stat=sudo("`which ping` -w 3 -c 4  -i .1 %s"%destination)
                right=stat.rfind('%')
                left=stat.rfind(' ',0,right)
                loss=stat[left:right]
                loss=loss.strip()
                loss=loss.strip('(') #For windows ping. Not tested
                loss=int(loss)
                temp=[env.host_string,destination,loss]
                result.append(temp)
        for destination in othernodes:
            if(destination!=env.host_string):
                #stat=sudo("%s %s"%(self.__getPingCommand(issue),destination))
                stat=sudo("`which ping` -w 3 -c 4  -i .1 %s"%destination)
                right=stat.rfind('%')
                left=stat.rfind(' ',0,right)
                loss=stat[left:right]
                loss=loss.strip()
                loss=loss.strip('(') #For windows ping. Not tested
                loss=int(loss)
                temp=[env.host_string,destination,loss]
                result.append(temp)
        
        return result
        
    
    def configIPSec(self,encryalgo,verbose,id):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__configIPSec(encryalgo,id)
        else:
            return self.__configIPSec(encryalgo,id)
    
    def stopIPSec(self,id,verbose):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__stopIPSec(id)
        else:
            return self.__stopIPSec(id)
        
    def showStatus(self,id,verbose):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__showStatus(id)
        else:
            return self.__showStatus(id)
    def testStatus(self,othernodes,verbose):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__testStatus(othernodes)
        else:
            return self.__testStatus(othernodes)
        
    def removeNode(self,node,id,verbose):
        if not verbose:
            with settings(hide('running', 'stdout', 'stderr')):
                return self.__removeNode(node,id)
        else:
            return self.__removeNode(node,id)
   
